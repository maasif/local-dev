package com.healthslate.patientapp.controller.filter;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.healthslate.patientapp.model.dao.BaseDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.ServiceFilterLog;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.CommonUtils;
import com.healthslate.patientapp.util.JsonUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PreMatching
public class RESTAuthFilter implements javax.ws.rs.container.ContainerRequestFilter {
	static final Logger LOG = LoggerFactory.getLogger(RESTAuthFilter.class);
	@Context
	private HttpServletRequest servletRequest;

	@Override
	public void filter(ContainerRequestContext containerRequest) throws IOException {
		System.out.println("Filter "+containerRequest.getUriInfo().getPath());
		long patientId = 0;
		String status = AppConstants.ACCESS_DENIED;
		String creds = "";
		String endPointUri = containerRequest.getUriInfo().getPath();
		String versionNo = ObjectUtils.nullSafe(containerRequest.getHeaderString(AppConstants.UserInfo.VERSION.getValue()));
		String macAddress = ObjectUtils.nullSafe(containerRequest.getHeaderString(AppConstants.UserInfo.MAC_ADDRESS.getValue()));

		if(AppConstants.ExcludedServices.isExcluded(endPointUri)) {
			status = AppConstants.AUTHENTICATION_NOT_REQ;
		} else {
			creds = containerRequest.getHeaderString(AppConstants.UserInfo.AUTHORIZATION.getValue());
			Map<String, Object> resultMap = CommonUtils.authenticateUser(creds);
			status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));
			patientId = (Long) resultMap.get(AppConstants.JsonConstants.DATA.name());
		}

		try {
			//updateVersionNumber(versionNo, patientId);
			ServiceFilterLog logging = new ServiceFilterLog();
			logging.setService(endPointUri);
			logging.setStatus(status);
			logging.setTimestamp(System.currentTimeMillis());
			logging.setIpAddress(servletRequest.getRemoteAddr());
			logging.setPatientId(patientId);
			logging.setVersionNo(versionNo);
			logging.setMacAddress(macAddress);
			new BaseDAO().save(logging);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("filter: Exception: "+e.getMessage());
		}

		try {
			if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {
                String response = AppConstants.ACCESS_DENIED;
                //for auth/login check if returns ACCESS_DENIED then show custom message
                if(endPointUri.equalsIgnoreCase("auth/login")){
                    Map<String, Object> returnValues = new LinkedHashMap<String, Object>();
                    returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                    returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL_PASSWORD);
                    response = JsonUtil.toJsonExcludedNull(returnValues);
                    LOG.info("filter (auth/login): Web Service: "+endPointUri+"\nSTATUS: "+response+"\nipAddress: "+servletRequest.getRemoteAddr()+ "\nPatientId: "+patientId+"\nAppVersion: "+versionNo+"\nAuthorization Header: "+creds+"\nmacAddress: "+macAddress);
                }

				//stop further request and send response to mobile app, __oz
				containerRequest.abortWith(Response.status(Status.FORBIDDEN).entity(response).build());
            }else{
                LOG.info("filter: Web Service: "+endPointUri+"\nSTATUS: "+status+"\nipAddress: "+servletRequest.getRemoteAddr()+ "\nPatientId: "+patientId+"\nAppVersion: "+versionNo+"\nAuthorization Header: "+creds+"\nmacAddress: "+macAddress);
            }
		} catch (Exception e) {
			LOG.info("filter: Exception: "+e.getMessage());
		}
	}

	public HttpServletRequest getServletRequest() {
		return servletRequest;
	}

	public void setServletRequest(HttpServletRequest servletRequest) {
		this.servletRequest = servletRequest;
	}
}
