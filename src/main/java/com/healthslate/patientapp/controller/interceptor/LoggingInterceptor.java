package com.healthslate.patientapp.controller.interceptor;

import java.nio.file.AccessDeniedException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Context;

import com.amazonaws.services.opsworks.model.App;
import com.healthslate.patientapp.model.dao.AppAccessLogDAO;
import com.healthslate.patientapp.model.dao.BaseDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.BrowserDetectUtil;
import com.healthslate.patientapp.util.CommonUtils;
import com.healthslate.patientapp.util.ObjectUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import com.healthslate.patientapp.model.dao.ServerLogDAO;
import com.healthslate.patientapp.util.AppConstants;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/*
* ======= FILE CHANGE HISTORY =======
* [2/11/2015]: added check to verify the patient assigned to provider, __oz
* [2/12/2015]: logged access denied entry in DB, __oz
* ===================================
 */

@SuppressWarnings("serial")
public class LoggingInterceptor extends AbstractInterceptor {

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
        final ActionContext context = invocation.getInvocationContext();
        HttpServletRequest request = (HttpServletRequest) context.get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
        HttpSession session = request.getSession(true);

        //[2/11/2015]: added by __oz
        Provider provider = (Provider)session.getAttribute(AppConstants.SessionKeys.PROVIDER.name());
        if(provider != null){
            Object patIdFromRequest = request.getParameter(AppConstants.JsonKeys.PATIENT_ID.getValue());
            Object logIdFromRequest = request.getParameter(AppConstants.JsonKeys.LOG_ID.getValue());
            long patientId = 0;
            if(logIdFromRequest != null){
                if(patIdFromRequest != null){
                    patientId = Long.parseLong(patIdFromRequest.toString());
                }
                if(!new PatientDAO().doesPatientAssignToProvider(provider.getProviderId(), patientId, ObjectUtils.nullSafe(logIdFromRequest))){
                    //[2/12/2015]: logged access denied entry in DB, __oz
                    CommonUtils.saveAccessDeniedLog(request, patientId);
                    throw new AccessDeniedException(AppConstants.ACCESS_DENIED);
                }
            }else{
                if(patIdFromRequest != null){
                    patientId = Long.parseLong(patIdFromRequest.toString());

                    if(!new PatientDAO().doesPatientAssignToProvider(provider.getProviderId(), patientId)){
                        //[2/12/2015]: logged access denied entry in DB, __oz
                        CommonUtils.saveAccessDeniedLog(request, patientId);
                        throw new AccessDeniedException(AppConstants.ACCESS_DENIED);
                    }
                }
            }
        }

        String returnValue = invocation.invoke();
		return returnValue;
	}
}
