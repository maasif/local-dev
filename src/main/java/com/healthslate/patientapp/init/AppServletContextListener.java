package com.healthslate.patientapp.init;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.healthslate.patientapp.util.CacheUtils;
import com.healthslate.patientapp.ws.EmailScheduler;
import com.healthslate.patientapp.ws.ReminderScheduler;

public class AppServletContextListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	
		if ( CacheUtils.manager != null ) {
			CacheUtils.cache.flush();			
			CacheUtils.manager.shutdown();
			CacheUtils.cacheUtils = null;
		}
	}

    public void runAllSchedulersAtOnce(){

        //SMS scheduler for medication reminders
        //will run every 1 minute
        ReminderScheduler.schedulerMedicationReminders();

        //SMS scheduler for goal assessment reminder
        //will run every fifteenth minute
        ReminderScheduler.schedulerGoalAssessmentReminders();

        //SMS scheduler for 1:1 session reminder for 24 Hours delay
        //will run every twenty minute
        ReminderScheduler.schedulerOneToOneSessionReminders24Hours();

        //SMS scheduler for 1:1 session reminder for today
        //will run every fifth minute
        ReminderScheduler.schedulerOneToOneSessionRemindersToday();

        //Email scheduler for sending coaches on third day after member joined
        //will run every 3rd hour
        EmailScheduler.schedulerReminders3DaysAfterNewMemberJoined();

        //Email scheduler for sending coaches on seventh day after member joined
        //will run every fourth hour
        EmailScheduler.schedulerReminders7DaysAfterNewMemberJoined();
    }

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
        try {
            runAllSchedulersAtOnce();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
