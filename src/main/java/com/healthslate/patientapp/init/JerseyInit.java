package com.healthslate.patientapp.init;

import javax.servlet.http.HttpServlet;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import com.healthslate.patientapp.ws.UploadServices;

@SuppressWarnings("serial")
public class JerseyInit extends HttpServlet {
	static {    
		final ResourceConfig resourceConfig = new ResourceConfig(UploadServices.class);
		resourceConfig.register(MultiPartFeature.class);     
	}
}