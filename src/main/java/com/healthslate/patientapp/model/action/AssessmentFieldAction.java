package com.healthslate.patientapp.model.action;

import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.amazonaws.services.datapipeline.model.Field;
import com.google.gson.GsonBuilder;
import com.healthslate.patientapp.model.dao.FieldDAO;
import com.opensymphony.xwork2.ActionSupport;

@Results ({ 
	@Result (name=ActionSupport.SUCCESS, location="/patientforms/short_assessment_form.jsp"),
 	@Result(name="ajax", type="json", params={"root", "jsonString"}),
})
public class AssessmentFieldAction extends ActionSupport{

private static final long serialVersionUID = 1L;
	
	FieldDAO fieldDAO = new FieldDAO();
	public String description;

	private String jsonString;

	@Action("getAssessmentFields")
	public String getAssessmentFields() {
		
		List<Field> fields = fieldDAO.list();
		
		if(fields.size() > 0){
			jsonString = StringEscapeUtils.escapeJava(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create().toJson(fields));
		}
		description = "Get assesment field successfully";
		return SUCCESS;
	}
	
	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


}
