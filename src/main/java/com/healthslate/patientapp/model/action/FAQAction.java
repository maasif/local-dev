package com.healthslate.patientapp.model.action;

import com.healthslate.patientapp.model.dao.PreferencesDAO;
import com.healthslate.patientapp.util.AppConstants;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Results({
   @Result(name=ActionSupport.SUCCESS, type="tiles", location="faqs.tiles"),
   @Result(name="howtos", type="tiles", location="howtos.tiles"),
   @Result(name="quick-start", type="tiles", location="quickStart.tiles")
})
public class FAQAction extends ActionSupport implements SessionAware, ServletRequestAware {

    private Map<String, Object> session;
    private HttpServletRequest request;
    private String token;
    public String description;

    @Action(value="faqs")
    public String showFaqsPage() {
        description = "Displayed FAQs page";
        return ActionSupport.SUCCESS;
    }

    @Action(value="howtos")
    public String showHowTosPage() {
        description = "Displayed Member App How To Videos page";
        token = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.WISTIA_API_TOKEN.getValue());
        return "howtos";
    }

    @Action(value="quickStart")
    public String showQuickStartGuidePage() {
        description = "Displayed Quick Start Guide page";
        return "quick-start";
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    public Map<String, Object> getSession(){
        return this.session;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
