package com.healthslate.patientapp.model.action;

import com.amazonaws.util.json.JSONObject;
import com.healthslate.patientapp.model.dao.BaseDAO;
import com.healthslate.patientapp.model.dao.FacilityDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.dao.PreferencesDAO;
import com.healthslate.patientapp.model.entity.Facility;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.*;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.io.IOUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.activation.DataHandler;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by omursaleen on 3/10/2015.
 */

@Results({
    @Result(name=ActionSupport.SUCCESS, type="tiles", location="recruitment/healthslateProgram.tiles"),
    @Result(name="evergreen", type="tiles", location="recruitment/healthslateProgramEvergreen.tiles"),
    @Result(name="joinHealthSlateProgram", type="tiles", location="recruitment/joinHealthslateProgram.tiles"),
    @Result(name="thankyou", type="tiles", location="recruitment/thankyou.tiles"),
    @Result(name="termsOfService", type="tiles", location="recruitment/termsOfService.tiles"),
    @Result(name="getHealthSlateApp", type="tiles", location="recruitment/getHealthSlateApp.tiles"),
    @Result(name="alreadyEnrolled", type="tiles", location="recruitment/alreadyEnrolled.tiles"),
    @Result(name="requestDenied", type="tiles", location="recruitment/requestDenied.tiles"),
    @Result(name="ajax", type="json", params={"root", "jsonData"})
})

public class RecruitmentAction extends ActionSupport implements SessionAware, ServletRequestAware {
    private String jsonData;
    public String description;

    private Map<String, Object> session;
    private HttpServletRequest request;
    private String token;
    private String userData;
    private Long patientId;
    public Patient patient;

    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String uuid;
    private String appLinkAndroid;
    private String appLinkIOS;
    private String assignedFacility = "";
    private String facilityName = "";

    private static final String DENIED_PARAMETER = "2";

    private boolean shouldSkipConsent;
    private String consentFormFile;
    private String otherData;
    public Facility facility;

    @Action(value="HealthSlateProgram")
    public String showIndexPage() {
        description = "Displayed HealthSlate Program page";
        facility = CommonUtils.getRecruitmentFacility();
        if(!ObjectUtils.isEmpty(assignedFacility)){
            facility = getDecryptedFacilityAndFromDB(assignedFacility);
            shouldSkipConsent = ObjectUtils.nullSafe(facility.getIsSkipConsent());
            if(facility != null){
                facilityName = facility.getName();
            }
        }
        return ActionSupport.SUCCESS;
    }

    @Action(value="EnrollInHealthSlateProgram")
    public String showEnrollInHealthSlateProgram() {
        uuid = token;
        if(!ObjectUtils.isEmpty(assignedFacility)){
            facility = getDecryptedFacilityAndFromDB(assignedFacility);
            shouldSkipConsent = ObjectUtils.nullSafe(facility.getIsSkipConsent());
            if(facility != null){
                facilityName = facility.getName();
            }
        }

        if(!ObjectUtils.isEmpty(token)){
            if(token != null){
                patient = new PatientDAO().getPatientByUUID(token);
                if(patient != null){
                    if(patient.getIsConsentAccepted() != null && patient.getIsConsentAccepted()) {
                        return "alreadyEnrolled";
                    }else if(patient.getIsApproved() != null && patient.getIsApproved().equalsIgnoreCase(DENIED_PARAMETER)) {
                        return "requestDenied";
                    }
                }
            }
        }

        description = "Displayed Enroll In HealthSlate Program page";

        //this check is for evergreen facility
        if(facility != null
                && !ObjectUtils.isEmpty(facility.getFacilityLogo())
                && facility.getFacilityLogo().equalsIgnoreCase("logo-evergreen.png")){

            return "evergreen";
        }

        return ActionSupport.SUCCESS;
    }

    @Action(value="JoinHealthSlateProgram")
    public String showJoinHealthSlateProgramPage() {

        if(!ObjectUtils.isEmpty(token)){
            patient = new PatientDAO().getPatientByUUID(token);
            if(patient != null){
                if(patient.getIsConsentAccepted() != null && patient.getIsConsentAccepted()) {
                    return "alreadyEnrolled";
                }else if(patient.getIsApproved() != null && patient.getIsApproved().equalsIgnoreCase(DENIED_PARAMETER)){
                    return "requestDenied";
                }else{
                    User user = patient.getUser();
                    patientId = patient.getPatientId();
                    if(user != null){
                        email = user.getEmail();
                    }
                }
            }
        }

        if(!ObjectUtils.isEmpty(assignedFacility)){
            facility = getDecryptedFacilityAndFromDB(assignedFacility);
            if(facility != null){
                facilityName = facility.getName();
                shouldSkipConsent = ObjectUtils.nullSafe(facility.getIsSkipConsent());
            }
        }

        description = "Displayed Join HealthSlate Program page";
        return "joinHealthSlateProgram";
    }

    @Action(value="ThankyouForRequest")
    public String showThankyouPage() {
        description = "Displayed Thank you for request joining HealthSlate program page";
        facility = getDecryptedFacilityAndFromDB(assignedFacility);
        return "thankyou";
    }

    @Action(value="emailTOS")
    public String emailTOS() {
        Map<String, String> prefMap = new PreferencesDAO().map();
        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        facility = getDecryptedFacilityAndFromDB(assignedFacility);
        try {
            if(facility != null){
                String htmlFileName = facility.getConsentFormFile();
                htmlFileName = htmlFileName.replace(".html", ".pdf");
                String consentFileName = request.getServletContext().getRealPath("/consentForms/"+htmlFileName);
                File consentFile = new File(consentFileName);
                if(consentFile.exists()){
                    // Creating inputStream to ByteArray

                    InputStream fileInputStream = new FileInputStream(consentFile);

                    byte[] bytes = IOUtils.toByteArray(fileInputStream);
                    InputStream myInputStream = new ByteArrayInputStream(bytes);
                    String fileName = "TermsOfService.pdf";

                    // Body Part of the Email, First Body Part Attachment File
                    MimeBodyPart bp = new MimeBodyPart();
                    List<MimeBodyPart> bodyParts = new ArrayList<MimeBodyPart>();
                    ByteArrayDataSource ds = new ByteArrayDataSource(myInputStream, "application/pdf");
                    bp.setDataHandler(new DataHandler(ds));
                    bp.setFileName(fileName);

                    // Adding body part into BodyParts List
                    bodyParts.add(bp);

                    // 2nd Body part Plain Text in email
                    MimeBodyPart mbp = new MimeBodyPart();

                    StringBuffer emailBody = new StringBuffer();
                    emailBody.append("Hi "+ firstName + ", <br/>")
                        .append("Please see attached informed consent.")
                        .append("<br/><br/>")
                        .append("Regards,")
                        .append("<br/>")
                        .append("The HealthSlate Team.");

                    mbp.setContent(emailBody.toString(), "text/html");

                    // Adding body part into BodyParts List
                    bodyParts.add(mbp);

                    InternetAddress[] ccEmails = null;

                    // Sending Email
                    boolean isSentEmail = EmailUtils.sendEmailWithAttachment("Please get the attached TOS", otherData,
                            prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                            "HealthSlate Informed Consent", //TODO: remember to put in AppConstants.java
                            fileName,
                            ccEmails,
                            bodyParts);

                    if(isSentEmail){
                        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnDataMap);
        return "ajax";
    }

    @Action(value="GetHealthSlateApp")
    public String showHealthSlateAppPage() {
        facility = getDecryptedFacilityAndFromDB(assignedFacility);
        if(!ObjectUtils.isEmpty(token)){
            if(token != null){
                patient = new PatientDAO().getPatientByUUID(token);
                if(patient != null){
                    setConsentAccepted(patient);
                    appLinkAndroid = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.PLAYSTORE_APP_URL.getValue());
                    appLinkIOS = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.APPLESTORE_APP_URL.getValue());
                    CommonUtils.sendAppLinks(patient);
                }
            }
        }

        description = "Displayed Get HealthSlate App page";
        return "getHealthSlateApp";
    }

    @Action(value="RedirectToEnrollForm")
    public String redirectToEnrollForm() {
        Boolean isFromEnrollPage = (Boolean)session.get(AppConstants.SessionKeys.ENROLL_PROGRAM.name());
        if(isFromEnrollPage != null && isFromEnrollPage){
             description = "Redirecting to Enroll HealthSlate Program Page";
             return showJoinHealthSlateProgramPage();
        }

        description = "Redirecting to HealthSlate Program Page";
        return showIndexPage();
    }

    @Action(value="TermsOfService")
    public String showTOS() {

        facility = getDecryptedFacilityAndFromDB(assignedFacility);

        if(!ObjectUtils.isEmpty(token)){
            try {

                if(token != null){
                    patient = new PatientDAO().getPatientByUUID(token);
                    facility = patient.getFacilities().get(0);
                    consentFormFile = ObjectUtils.nullSafe(facility.getConsentFormFile());
                    facilityName = facility.getName();

                    if(patient != null){
                        if(patient.getIsConsentAccepted() != null && patient.getIsConsentAccepted()){
                            return "alreadyEnrolled";
                        }else if(patient.getIsApproved() != null && patient.getIsApproved().equalsIgnoreCase(DENIED_PARAMETER)){
                            return "requestDenied";
                        }else{
                            User user = patient.getUser();
                            patientId = patient.getPatientId();
                            if(user != null){
                                firstName = user.getFirstName();
                                lastName = user.getLastName();
                                email = user.getEmail();
                                phone = user.getPhone();
                            }
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                try {
                    throw new AccessDeniedException(AppConstants.ACCESS_DENIED);
                } catch (AccessDeniedException e1) {
                    e1.printStackTrace();
                }
            }
        }

        description = "Displayed Terms of Service of HealthSlate program page";
        return "termsOfService";
    }

    @Action(value="saveParticipant")
    public String saveParticipant() {
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        try {

            JSONObject jsonObject = new JSONObject(userData);
            patientId = jsonObject.getLong("patientId");

            if(patientId != null){
                patient = new PatientDAO().getPatientById(patientId);
                patient.setIsConsentAccepted(true);

                User user = patient.getUser();

                firstName = jsonObject.getString("firstName");
                lastName = jsonObject.getString("lastName");
                email = jsonObject.getString("email");
                phone = jsonObject.getString("phone");

                String newName = CommonUtils.formatPatientName(firstName, lastName, "");

                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setDisplayName(newName);

                user.setEmail(email);
                user.setPhone(CommonUtils.formatPhoneNumber(phone));

                new BaseDAO().save(user);

                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        description = "Saving patient info on terms of service page";
        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    public void setConsentAccepted(Patient patient){
        try {
            List<Facility> facilityAssigned = patient.getFacilities();
            if(facilityAssigned != null && facilityAssigned.size() > 0){
                Facility facility = facilityAssigned.get(0);
                if(facility.getIsSkipConsent()){
                    patient.setIsConsentAccepted(true);
                    new BaseDAO().save(patient);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getDecryptedFacilityName(String encryptedFacilityName){
        String decryptedFacilityName = encryptedFacilityName;
        try {
            decryptedFacilityName = new EncryptionUtils().decrypt(encryptedFacilityName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return decryptedFacilityName;
    }

    public Facility getDecryptedFacilityAndFromDB(String encryptedFacilityName){
        try {
            String decryptedFacilityName = getDecryptedFacilityName(encryptedFacilityName);
            facility = new FacilityDAO().getFacilityByName(decryptedFacilityName);
            return facility;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    public Map<String, Object> getSession(){
        return this.session;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }


    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserData() {
        return userData;
    }

    public void setUserData(String userData) {
        this.userData = userData;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAppLinkAndroid() {
        return appLinkAndroid;
    }

    public void setAppLinkAndroid(String appLinkAndroid) {
        this.appLinkAndroid = appLinkAndroid;
    }

    public String getAssignedFacility() {
        return assignedFacility;
    }

    public void setAssignedFacility(String assignedFacility) {
        this.assignedFacility = assignedFacility;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public boolean isShouldSkipConsent() {
        return shouldSkipConsent;
    }

    public void setShouldSkipConsent(boolean shouldSkipConsent) {
        this.shouldSkipConsent = shouldSkipConsent;
    }

    public String getConsentFormFile() {
        return consentFormFile;
    }

    public void setConsentFormFile(String consentFormFile) {
        this.consentFormFile = consentFormFile;
    }

    public String getAppLinkIOS() {
        return appLinkIOS;
    }

    public void setAppLinkIOS(String appLinkIOS) {
        this.appLinkIOS = appLinkIOS;
    }

    public String getOtherData() {
        return otherData;
    }

    public void setOtherData(String otherData) {
        this.otherData = otherData;
    }

    public Facility getFacility() {
        return facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }
}
