package com.healthslate.patientapp.model.action;

import com.amazonaws.util.json.JSONObject;
import com.healthslate.patientapp.model.dao.BaseDAO;
import com.healthslate.patientapp.model.dao.FacilityDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.dao.PreferencesDAO;
import com.healthslate.patientapp.model.entity.Facility;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.*;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.AccessDeniedException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by omursaleen on 3/10/2015.
 */

@Results({
        @Result(name=ActionSupport.SUCCESS, type="tiles", location="recruitment/healthslateProgram.tiles"),
        @Result(name="evergreen", type="tiles", location="recruitment/healthslateProgramEvergreen.tiles"),
        @Result(name="ajax", type="json", params={"root", "jsonData"})
})
public class RecruitmentRedirectAction extends ActionSupport implements SessionAware, ServletRequestAware {
    private String jsonData;
    public String description;

    private Map<String, Object> session;
    private HttpServletRequest request;
    private String token;
    private Long patientId;
    public Patient patient;
    private String uuid;
    private String appLink;
    private String assignedFacility = "";
    private String facilityName = "";
    private boolean shouldSkipConsent;
    public Facility facility;

    //---------------------------------------------- start hstage -----------------------------------------------

    @Action(value="Developer")
    public String showDeveloperPage() {
        description = "Displayed Developer recruitment page";
        loadFacility(16l);
        return ActionSupport.SUCCESS;
    }

    @Action(value="Providence")
    public String showProvidencePage() {
        description = "Displayed Providence recruitment page";
        loadFacility(22l);
        return ActionSupport.SUCCESS;
    }

    @Action(value="ProvidenceTest")
    public String showProvidenceTestPage() {
        description = "Displayed Providence Test recruitment page";
        loadFacility(34l);
        return ActionSupport.SUCCESS;
    }

    @Action(value="Franciscan")
    public String showFranciscanPage() {
        description = "Displayed CHI Franciscan recruitment page";
        loadFacility(24l);
        return ActionSupport.SUCCESS;
    }

    @Action(value="FranciscanTest")
    public String showFranciscanTestPage() {
        description = "Displayed CHI Franciscan Test recruitment page";
        loadFacility(28l);
        return ActionSupport.SUCCESS;
    }

    @Action(value="QA")
    public String showQAPage() {
        description = "Displayed CHI Franciscan recruitment page";
        loadFacility(18l);
        return ActionSupport.SUCCESS;
    }

    @Action(value="TSQA")
    public String showTSQAPage() {
        description = "Displayed TSQA recruitment page";
        loadFacility(26l);
        return ActionSupport.SUCCESS;
    }

    @Action(value="EvergreenTest")
    public String showEvergreenTestPage() {
        description = "Displayed CHI Evergreen Test recruitment page";
        loadFacility(23l);
        return "evergreen";
    }

    //---------------------------------------------- end hstage -----------------------------------------------

    //---------------------------------------------- start prod -----------------------------------------------

    @Action(value="qliance")
    public String showQliancePage() {
        description = "Displayed qliance recruitment page";
        loadFacility(18l);
        return ActionSupport.SUCCESS;
    }

    @Action(value="HSTEST")
    public String showHSTESTPage() {
        description = "Displayed HSTEST recruitment page";
        loadFacility(14l);
        return ActionSupport.SUCCESS;
    }

    @Action(value="evergreen")
    public String showEvergreenPage() {
        description = "Displayed Evergreen recruitment page";
        loadFacility(20l);
        return "evergreen";
    }

    @Action(value="franciscan")
    public String showFranciscanProdPage() {
        description = "Displayed Franciscan recruitment page";
        loadFacility(21l);
        return ActionSupport.SUCCESS;
    }

    //---------------------------------------------- end prod -----------------------------------------------

    //---------------------------------------------- start tprod -----------------------------------------------

    @Action(value="providence")
    public String showProvidenceFacilityPage() {
        description = "Displayed providence recruitment page";
        loadFacility(6l);
        return ActionSupport.SUCCESS;
    }

    //---------------------------------------------- end tprod -----------------------------------------------

    public void loadFacility(long facId){
        facility = new FacilityDAO().getFacilityWithId(facId);
        try {
            assignedFacility = new EncryptionUtils().encrypt(facility.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        shouldSkipConsent = ObjectUtils.nullSafe(facility.getIsSkipConsent());
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    public Map<String, Object> getSession(){
        return this.session;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAppLink() {
        return appLink;
    }

    public void setAppLink(String appLink) {
        this.appLink = appLink;
    }

    public String getAssignedFacility() {
        return assignedFacility;
    }

    public void setAssignedFacility(String assignedFacility) {
        this.assignedFacility = assignedFacility;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public boolean isShouldSkipConsent() {
        return shouldSkipConsent;
    }

    public void setShouldSkipConsent(boolean shouldSkipConsent) {
        this.shouldSkipConsent = shouldSkipConsent;
    }

    public Facility getFacility() {
        return facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }
}
