package com.healthslate.patientapp.model.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.dao.ProviderDAO;
import com.healthslate.patientapp.model.dao.UserDAO;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.opensymphony.xwork2.ActionSupport;

@Results({
	@Result(name="[ROLE_PROVIDER]", type="redirectAction", params={"actionName", "educator/fetchAllLogs"}),
	@Result(name="[ROLE_PATIENT]", location="/login.jsp"),
	@Result(name=ActionSupport.ERROR, location="/login.jsp"),
	/*@Result(name="[ROLE_PATIENT]", type="redirectAction", params={"actionName", "patient/viewPatientLogsTablet"}),*/
})
@SuppressWarnings("serial")
public class RoleRedirectAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private Map<String, Object> session;

	HttpServletRequest request;
	private long providerId;

	public String description;

	@Action(value="role-redirect")
	public String roleRedirect() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = new UserDAO().getUserByUsername(auth.getName());
		session.put(AppConstants.SessionKeys.USER.name(), user);
		
		if (auth.getAuthorities().toString().equals("[ROLE_PROVIDER]")) {
			Provider provider = new ProviderDAO().getProviderByUserId(user.getUserId()); 
			session.put(AppConstants.SessionKeys.PROVIDER.name(), provider);
			description = "Displayed Patients Logs to Provider Id: "+provider.getProviderId();
		} else if (auth.getAuthorities().toString().equals("[ROLE_PATIENT]")) {
			Patient patient = new PatientDAO().getPatientByUserId(user.getUserId()); 
			session.put(AppConstants.SessionKeys.PATIENT.name(), patient);
			description = "Displayed Patient Logs for Patient Id: "+patient.getPatientId();
		} else if (auth.getAuthorities().toString().equals("[ROLE_FACILITY_ADMIN]")) {
			Provider provider = new ProviderDAO().getProviderByUserId(user.getUserId());
			session.put(AppConstants.SessionKeys.PROVIDER.name(), provider);
			description = "Displayed Dashboard for Facility Admin : " +provider.getProviderId();
		}

		return auth.getAuthorities().toString();
	}

	@Action(value="login")
	 public String login() {
		description = "Redirect to login screen";
		return ERROR;
	}
	
	@Action(value="loginFailed")
	public String loginFailed() {
		description = "Login failed, Redirect to login screen";
		return ERROR;
	}
	
	@Action(value="logout")
	public String logout() {
		description = "Logout, Redirect to login screen";
		return ERROR;
	}

    @Action(value="refreshSession")
    public String refreshSession() {
        description = "Session restarted";
        return null;
    }

	@Override
	public void setSession(Map<String, Object> arg0) {
		session = arg0;
	}

	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		request = arg0;
	}

	public long getProviderId() {
		return providerId;
	}
	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
