package com.healthslate.patientapp.model.action.app;

/*
* ======= FILE CHANGE HISTORY =======
* [2/3/2015]: Created by __oz
* ===================================
 */

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.codec.Base64;

import com.opensymphony.xwork2.ActionSupport;

@Results({
    @Result(name= ActionSupport.SUCCESS, location="/resetPassword.jsp"),
    @Result (name=ActionSupport.ERROR, location="/error.jsp"),
    @Result(name="resetPasswordForced", location="/resetPasswordForced.jsp"),
    @Result(name="ajax", type="json", params={"root", "jsonData"})
})
@SuppressWarnings("serial")
public class ChangePasswordAction extends ActionSupport implements SessionAware, ServletRequestAware {
	private static final String WRONG_PASSWORD = "Wrong password";
	private static final String USER_NOT_EXIST = "User doesn't available";
//    private static final String SERVER_ERROR = "Server error";

	private String AJAX = "ajax";
    private String jsonData;
    private String userName;
    private String token;
    private String error;
    private String password;
    private String oldPassword;
    private String newPassword;
    private String pinCode;

    private UserDAO userDAO = new UserDAO();
    private BaseDAO baseDAO = new BaseDAO();
    private EmailTokenDAO emailTokenDAO = new EmailTokenDAO();
    private PreferencesDAO preferencesDAO = new PreferencesDAO();
    private EmailNotificationMessageDAO emailNotificationMessageDAO = new EmailNotificationMessageDAO();
    private Map<String, Object> session;
    private HttpServletRequest request;

    public String description;

    @Action("sendResetEmail")
    public String sendResetEmail() {

        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        try {

            User user = userDAO.getUserByEmail(userName);

            if(user != null){

                boolean isSentEmail = EmailUtils.sendResetPasswordEmail(user, request, true);

                if(isSentEmail) {
                    returnMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                    description = "Reset password email sent to " + userName;
                }else{
                    returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
                    returnMap.put(AppConstants.JsonConstants.REASON.name(), "Unable to send email at the moment.");
                    description = "Error in sending reset email, Unable to send email at the moment to " + userName;
                }
            }else{
                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
                returnMap.put(AppConstants.JsonConstants.REASON.name(), "Invalid email address.");
                description = "Error in sending reset email, Invalid email/No user with given email: " + userName;
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return AJAX;
    }

    @Action("resetPassword")
    public String resetPassword() {
        String returnValue = ERROR;
        EmailToken emailToken = emailTokenDAO.getByToken(token);
        if (emailToken != null) {
            token = emailToken.getToken();
            userName = emailToken.getUsername();
            returnValue = SUCCESS;
            description = "Reset password successfully for " + userName;
        } else {
            error = "Your email link has been expired.";
            description = "Error in resetting password, email link has been expired";
        }
        return returnValue;
    }

    @Action("setPassword")
    public String setPassword() {
        User user = userDAO.getUserByEmail(userName);
        EmailToken emailToken = emailTokenDAO.getByToken(token);

        user.setPassword(new BCryptPasswordEncoder().encode(password));
        user.setPasswordModifiedTime(System.currentTimeMillis());

        Patient patient = user.getPatient();
        if(patient != null){
            patient.setPasswordExpiryTime(DateUtils.addDayInTime(System.currentTimeMillis(), 60));
        }
        baseDAO.save(user);

        Map<String, String> returnValues = new LinkedHashMap<String, String>();
        returnValues.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
        returnValues.put(AppConstants.JsonConstants.DATA.name(), user.getUserType());

        emailToken.setUsed(true);
        emailTokenDAO.saveEmailToken(emailToken);
        jsonData = JsonUtil.toJson(returnValues);
        description = "Set password successfully for " + userName + " User id" +user.getUserId();
        return AJAX;
    }

    @Action("ResetPassword")
    public String resetPasswordForced() {
        String returnValue = ERROR;
        EmailNotificationMessage emailToken = emailNotificationMessageDAO.getByToken(token);
        if (emailToken != null) {
            boolean isExpired = ObjectUtils.nullSafe(emailToken.getIsExpired());
            if(emailToken.getTokenExpiry() < System.currentTimeMillis() || isExpired){
                error = "Your email link has been expired.";
                description = "Error in resetting password, email link has been expired";
            }else{
                token = emailToken.getToken();
                userName = emailToken.getUser().getEmail();
                returnValue = "resetPasswordForced";
                description = "Reset password successfully for " + userName;
            }
        }
        return returnValue;
    }

    @Action("setPasswordForced")
    public String setPasswordForced() {
        User user = userDAO.getUserByEmail(userName);
        EmailNotificationMessage emailToken = emailNotificationMessageDAO.getByToken(token);

        Map<String, String> returnValues = new LinkedHashMap<String, String>();
        returnValues.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        if(emailToken != null){

            if(emailToken.getPinCode().equalsIgnoreCase(pinCode)){
                user.setPassword(new BCryptPasswordEncoder().encode(password));
                user.setPasswordModifiedTime(System.currentTimeMillis());
                baseDAO.save(user);
                emailToken.setIsExpired(true);
                baseDAO.save(emailToken);
                returnValues.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            }else{
                returnValues.put(AppConstants.JsonConstants.REASON.name(), "Pin code does not match.");
            }
        }else{
            returnValues.put(AppConstants.JsonConstants.REASON.name(), "Token is invalid.");
        }

        jsonData = JsonUtil.toJson(returnValues);
        description = "Set password successfully for " + userName + " User id" +user.getUserId();
        return AJAX;
    }

    @Action("changePassword1")
    public String changePassword() {
    	Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
    	Provider provider = (Provider) session.get(AppConstants.SessionKeys.PROVIDER.name());
    	if(provider != null) {
    		Long userId = provider.getUser().getUserId();
        	User user = new UserDAO().getUserById(userId);
        	if(user != null) {
        		user.setPassword(new BCryptPasswordEncoder().encode(newPassword));
                user.setPasswordModifiedTime(System.currentTimeMillis());
        		boolean isSaved = new BaseDAO().save(user);
        		if(isSaved) {
        			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
        	        description = "Password changed successfully of user id" + user.getUserId();
        		} else {
        	        returnMap.put(AppConstants.JsonConstants.REASON.name(), AppConstants.UBABLE_TO_CHANGE_PASSWORD);
        	        description = "Error while changing password, Server error";
        		}
        	} else {
        		returnMap.put(AppConstants.JsonConstants.REASON.name(), USER_NOT_EXIST);
        		description = "Error while changing password, User not exist";
        	}
    	} else {	    		
    		returnMap.put(AppConstants.JsonConstants.REASON.name(), AppConstants.UBABLE_TO_CHANGE_PASSWORD);
    		description = "Error while changing password, Provider not found in session";
    	}
    	
    	jsonData = JsonUtil.toJson(returnMap);
    	return AJAX;
    }
    
    @Action("verifyPassword1")
    public String verifyPassword() {
    	Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
         
    	Provider provider = (Provider) session.get(AppConstants.SessionKeys.PROVIDER.name());
    	if(provider != null) {
	    	Long userId = provider.getUser().getUserId();
	    	User user = new UserDAO().getUserById(userId);
	    	if(user != null) {
	    		if(BCrypt.checkpw(oldPassword, user.getPassword())) {
	    			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
	    			description = "Password verified for user" + userId;
	    		} else {
	    			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
	    	        returnMap.put(AppConstants.JsonConstants.REASON.name(), WRONG_PASSWORD);		
	    	        description = "Error while verifying password" + WRONG_PASSWORD;
	    		}
	    	} else {
	    		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
	            returnMap.put(AppConstants.JsonConstants.REASON.name(), USER_NOT_EXIST);
	            description = "Error while verifying password, User not exist";
	    	}
    	} else {
    		returnMap.put(AppConstants.JsonConstants.REASON.name(), AppConstants.UBABLE_TO_VERIFY_PASSWORD);
    		description = "Error while verifying password, Provider not found in session";
    	}
    	jsonData = JsonUtil.toJson(returnMap);
    	return AJAX;
    }
    
    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    public Map<String, Object> getSession(){
        return this.session;
    }

    public String getError() {
        return error;
    }
    public void setError(String error) {
        this.error = error;
    }

    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }
}
