package com.healthslate.patientapp.model.action.app;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.healthslate.patientapp.model.dao.BaseDAO;
import com.healthslate.patientapp.model.dao.FacilityDAO;
import com.healthslate.patientapp.model.dto.SignUpDTO;
import com.healthslate.patientapp.util.JsonUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import com.opensymphony.xwork2.ActionSupport;

/*
* ======= FILE CHANGE HISTORY =======
* [2/3/2015]: Created by __oz
* ===================================
 */

@Results({
	@Result(name=ActionSupport.SUCCESS, location="/signUpPatient.jsp"),
	@Result(name="ajax", type="json", params={"root", "jsonData"})
})

@SuppressWarnings("serial")
public class PatientSignupAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private String jsonData;
	private String signupData;
	private String facilitiesListString;

	private BaseDAO baseDAO = new BaseDAO();
	private SignUpDTO signUp;
	private Map<String, Object> session;
	private HttpServletRequest request;

	public String description;
	public String baseUrl;

	@Action(value="signupMember")
	public String signupMember() {
		facilitiesListString = StringEscapeUtils.escapeJava(JsonUtil.toJsonExcludedNull(new FacilityDAO().listRaw(true)));
		baseUrl = ObjectUtils.getApplicationUrl(request);
		description = "Patient sign up request";
		return ActionSupport.SUCCESS;
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFacilitiesListString() {
		return facilitiesListString;
	}

	public void setFacilitiesListString(String facilitiesListString) {
		this.facilitiesListString = facilitiesListString;
	}

	public String getSignupData() {
		return signupData;
	}

	public void setSignupData(String signupData) {
		this.signupData = signupData;
	}

	public SignUpDTO getSignUp() {
		return signUp;
	}

	public void setSignUp(SignUpDTO signUp) {
		this.signUp = signUp;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
}
