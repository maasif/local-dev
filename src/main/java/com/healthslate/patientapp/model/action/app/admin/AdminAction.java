package com.healthslate.patientapp.model.action.app.admin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.validation.SkipValidation;

import sun.audio.AudioStream;

import com.contentful.java.api.CDAClient;
import com.contentful.java.model.CDAArray;
import com.contentful.java.model.CDAEntry;
import com.contentful.java.model.CDAResource;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.healthslate.patientapp.model.dao.UserDAO;
import com.healthslate.patientapp.model.dto.GroupsDTO;
import com.healthslate.patientapp.model.dto.MessageDTO;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.CacheUtils;
import com.healthslate.patientapp.util.CommonUtils;
import com.healthslate.patientapp.util.SocialServicesUtils;
import com.opensymphony.xwork2.ActionSupport;

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="admin/adminDashboard.tiles"),
	@Result(name=AppConstants.GROUPS, type="tiles", location="admin/socialGroups.tiles"),
	@Result(name=AppConstants.MESSAGES, type="tiles", location="admin/Messages.tiles"),
	@Result(name=AppConstants.AJAX_USER_LIST, type="json", params={"root", "userListJson"}),
    /*@Result(name = "input", type="tiles", location="admin/Messages.tiles"),*/
})

@SuppressWarnings("serial")
public class AdminAction extends ActionSupport implements SessionAware{
	private Map<Integer, String> groupMap = new LinkedHashMap<Integer, String>();
	private Map<String, String> UserMap = new LinkedHashMap<String, String>();
	String userListJson;
	String reciverUserNameKey;
	
	String messageContents;
	String sendTo ;
	String sendBy ;
	private UserDAO userDAO = new UserDAO();
	
	private Map<String, Object> session;
	
	private List<GroupsDTO> groupsDTOs;
	private List<MessageDTO> messageDTOs ;
	
	private String groupName;
	private String groupDescription;
	private String statusMessage = "";
	int groupIdTogetUser ;
	public String description;
	private AudioStream as;
	
	@SuppressWarnings("unchecked")
	@Action(value="reloadFacts")
	public String reloadFacts() {
		// creates the "C:/DYK Facts" directory if doesn't exist
		File file = new File("C:\\DYK Facts");
		if (!file.exists()) {
			try {
				file.mkdir();
			} 
			catch (Exception e) {
			}
		}
		
		String elementKey = "facts";				
				
		// makes a client with valid credentials.
		CDAClient client = new CDAClient.Builder()
		.setSpaceKey("1ovf50zlp7i1")
		.setAccessToken("6d923a09338aebc75ffc94a0ba9510844eebaee314427beae32a31b7eed23894")
		.build();
		
		// mapping for getting data from contentful 
		Map<String, String> queryMap = new LinkedHashMap<String, String>();
		queryMap.put("content_type", "5WdclSkh2w84SCaCc2Y0Cs");
		
		// fetches data and we transform this data to a list of maps (fact, source, tag and so on)		
		CDAArray cdaArray = client.fetchEntriesMatchingBlocking(queryMap);
		List<CDAResource> resources = cdaArray.getItems();
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		
		for (CDAResource resource: resources) {
			
			CDAEntry entry = (CDAEntry)resource;			
			Map<String, String> map = entry.getFields();
			list.add(map);
		}
		
		// convert list of maps to json
		Gson gson = new Gson();
		String json = gson.toJson(list);
					
		// insert that json in cache with key "facts". this key "facts" will be used to retireve it from cache.
		CacheUtils.insertElement(elementKey, json);
		
		description = "Reloaded DYK facts and cached them";
		
		return SUCCESS;
	}
	
	@Action(value="clearFacts")
	public String clearFacts(){
				
		// clears the cache element that matches its key with "facts"
		CacheUtils.removeElement("facts");
		description = "cleared the cache element that matches its key with 'facts'";
		return SUCCESS;		
	}
	
	@Action(value="readFacts")
	public String readFacts(){
		System.out.println(CacheUtils.getElement("facts").getObjectValue().toString());
		description = "Read facts from cache that matches with the key 'facts'";
		return SUCCESS;		
	}
	
	@Action(value="viewDashBoard")
	public String viewDashBoard() throws IOException {
		description = "View Admin Dashboard";
		return SUCCESS;
	}
	
	@Action(value="addNewGroup")
	public String addNewGroup() throws ClientProtocolException, IOException {
		
		int ownerId = createUserInSocial();
		if(ownerId != 0){
			
			Map<String, String> dataMap = new LinkedHashMap<String, String>();
			
			dataMap.put(AppConstants.SOCIALKEYS.NAME.getValue(), groupName);
			dataMap.put(AppConstants.SOCIALKEYS.DESCRIPTION.getValue(), groupDescription);
			dataMap.put(AppConstants.SOCIALKEYS.OWNER_GUID.getValue(), String.valueOf(ownerId));
			
			String jsonString = new Gson().toJson(dataMap);
			String response = SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.CREATE_GROUP.getValue());
			
			JsonElement jelement = new JsonParser().parse(response);
	        JsonObject jobject = jelement.getAsJsonObject();
	        
	        int statusFromElgg = jobject.get(AppConstants.SOCIALKEYS.STATUS.getValue()).getAsInt();
	        
	        if(statusFromElgg != -1){
	        	int groupId = jobject.get(AppConstants.SOCIALKEYS.RESULT.getValue()).getAsInt();
	        	
	            if(groupId != 0) {
	            	statusMessage = "New Group Created Successfully";
	            	description = "Add new group, group name: "+groupName;
	            } else{
		        	statusMessage = "Error in creating group";
		        	description = statusMessage + ", Group Name: " + groupName;
		        }
	        } else {
	        	statusMessage = "Error in creating group";
	        	description = statusMessage + ", Group Name: " + groupName;
	        }
		}
		return viewSocialGroups();
	}
	
	
	@SuppressWarnings("rawtypes")
	@Action(value="SendMessage")
	@SkipValidation
	public String SendMeesage() throws ClientProtocolException, IOException {
		System.out.println(groupIdTogetUser);	
		populateUserMap(groupIdTogetUser);
		System.out.println(sendTo);
		System.out.println(reciverUserNameKey);
		System.out.println(messageContents);
		
		User Sessionuser =(User) session.get(AppConstants.SessionKeys.USER.name());
		sendBy = Sessionuser.getEmail();
		
		
		if(sendTo.equals("All")) {
			Iterator it = UserMap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry)it.next();
				if((String) pairs.getKey()!="All"){
				sendAMessage((String) pairs.getKey());
				}
			}
		} else {
		sendAMessage(sendTo);
	}
	
	return viewMessages();
	}
	
	private void sendAMessage(String RecEmail)  throws ClientProtocolException, IOException{
		sendTo=RecEmail ;
		Map<String, String> dataMap = new LinkedHashMap<String, String>();
		dataMap.put(AppConstants.SOCIALKEYS.MESSAGE.getValue(), messageContents);
		dataMap.put(AppConstants.SOCIALKEYS.SEND_TO.getValue(), sendTo);
		dataMap.put(AppConstants.SOCIALKEYS.SEND_BY.getValue(), sendBy);
		String jsonString = new Gson().toJson(dataMap);
		String response = SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.MESSAGE_SEND.getValue());
		
		

		JsonElement jelement = new JsonParser().parse(response);
        JsonObject jobject = jelement.getAsJsonObject();
        int statusFromElgg = jobject.get(AppConstants.SOCIALKEYS.STATUS.getValue()).getAsInt();
        
        if(statusFromElgg != -1){
        	int msgId = jobject.get(AppConstants.SOCIALKEYS.RESULT.getValue()).getAsInt();
        	
            if(msgId != 0){
            	statusMessage = "Message Sent Succesfuly";
            	description = "Add new group, group name: "+messageContents;
            }
            else{
	        	statusMessage = "Error Sending Message";
	        	description = statusMessage + ", Message: " + messageContents;
	        }
        }
        else{
        	statusMessage = "Error Sending Message";
        	description = statusMessage + ", Group Name: " + messageContents;
        }
		
	}
	private int createUserInSocial() throws ClientProtocolException, IOException {
		
		int userSocialId = 0;
		User user = (User) session.get(AppConstants.SessionKeys.USER.name());
		
		if(user != null && user.getSocialId() == 0){
			
			Map<String, String> dataMap = new LinkedHashMap<String, String>();
			
			dataMap.put(AppConstants.SOCIALKEYS.USERNAME.getValue(), user.getEmail());
			dataMap.put(AppConstants.SOCIALKEYS.NAME.getValue(), user.getFirstName() + " " + user.getLastName());
			dataMap.put(AppConstants.SOCIALKEYS.EMAIL.getValue(), user.getEmail());
			dataMap.put(AppConstants.SOCIALKEYS.PASSWORD.getValue(), user.getPassword());
			
			String jsonString = new Gson().toJson(dataMap);
			String response = SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.CREATE_USER.getValue());
			
			JsonElement jelement = new JsonParser().parse(response);
	        JsonObject jobject = jelement.getAsJsonObject();
	        
	        int statusFromElgg = jobject.get(AppConstants.SOCIALKEYS.STATUS.getValue()).getAsInt();
	        
	        if(statusFromElgg != -1){
	        	jelement = jobject.get(AppConstants.SOCIALKEYS.RESULT.getValue());
	            JsonObject object = jelement.getAsJsonObject();
	            
	            boolean isSuccess = object.get(AppConstants.SOCIALKEYS.SUCCESS.getValue()).getAsBoolean();
	            if(isSuccess){
	            	userSocialId = object.get(AppConstants.SOCIALKEYS.GUID.getValue()).getAsInt();
	            	
	            	user.setSocialId(userSocialId);
	            	userDAO.save(user);
	            }
	        }
		}
		else{
			if(user != null){
				userSocialId = user.getSocialId();
			}
		}
		
		return userSocialId;
	}
	
	@Action(value="viewSocialGroups")
	public String viewSocialGroups() throws ClientProtocolException, IOException {
		
		String response = SocialServicesUtils.getSocialData(AppConstants.SOCIALKEYS.GET_GROUPS_FULL_VIEW.getValue());
		JsonElement jelement = new JsonParser().parse(response);
        JsonObject jobject = jelement.getAsJsonObject();
        
        jelement = jobject.get("result");
        JsonArray jsonArray = jelement.getAsJsonArray();
        
        groupsDTOs = new ArrayList<GroupsDTO>();
        for(JsonElement element: jsonArray){
        	JsonObject object = element.getAsJsonObject();
        	
        	GroupsDTO groupsDTO = new GroupsDTO();
        	groupsDTO.setGroupId(object.get(AppConstants.SOCIALKEYS.GUID.getValue()).getAsInt());
        	groupsDTO.setGroupName(object.get(AppConstants.SOCIALKEYS.NAME.getValue()).getAsString());
        	
        	if(!object.get(AppConstants.SOCIALKEYS.DESCRIPTION.getValue()).equals(JsonNull.INSTANCE)){
        		groupsDTO.setDescription(object.get(AppConstants.SOCIALKEYS.DESCRIPTION.getValue()).getAsString());
        	}
        	
        	groupsDTO.setGroupMembers(object.get(AppConstants.SOCIALKEYS.MEMBERS.getValue()).getAsInt());
        	groupsDTO.setLikes(object.get(AppConstants.SOCIALKEYS.LIKES.getValue()).getAsInt());
        	groupsDTO.setAvatarPath(object.get(AppConstants.SOCIALKEYS.AVATAR_URL.getValue()).getAsString());
        	
        	groupsDTOs.add(groupsDTO);
        }
        description = "View social groups to admin";
        
		return AppConstants.GROUPS;
	}
	@Action(value="viewMessages")
	public String viewMessages() throws ClientProtocolException, IOException {
		populateGroupMap();
		//populateUserMap();
		
	
		User Sessionuser =(User) session.get(AppConstants.SessionKeys.USER.name());
		String userName = Sessionuser.getEmail();
	
		String response = SocialServicesUtils.getSocialData(AppConstants.SOCIALKEYS.MESSAGE_SENT.getValue()+"&"+AppConstants.SOCIALKEYS.USER.getValue()+"="+userName);
		JsonElement jelement = new JsonParser().parse(response);
        JsonObject jobject = jelement.getAsJsonObject();
        
        jelement = jobject.get("result");
        JsonArray jsonArray = jelement.getAsJsonArray();
        
        messageDTOs = new ArrayList<MessageDTO>();
        for(JsonElement element: jsonArray){
        	JsonObject user;
        	JsonObject object = element.getAsJsonObject();
        	
        	MessageDTO messagesDTO = new MessageDTO();
        	user =object.getAsJsonObject("user");
        	messagesDTO.setMessage(object.get(AppConstants.SOCIALKEYS.MESSAGE.getValue()).getAsString());
        	
        	messagesDTO.setGroupName(object.get(AppConstants.SOCIALKEYS.GROUP_NAME.getValue()).getAsString());
        	
        	messagesDTO.setTime(CommonUtils.gmtToest(object.get(AppConstants.SOCIALKEYS.TIME_STAMP.getValue()).getAsLong()*1000));
        	messagesDTO.setUserDisplayName(user.get(AppConstants.SOCIALKEYS.USER_DISPLAY_NAME.getValue()).getAsString());
        
        	messageDTOs.add(messagesDTO);
        }
        description = "View messages to admin";
       
		return AppConstants.MESSAGES;
	}
	
	private void populateGroupMap() {
		
		try{
			String response = SocialServicesUtils.getSocialData(AppConstants.SOCIALKEYS.LIST_GROUPS.getValue());
			
			JsonElement jelement = new JsonParser().parse(response);
	        JsonObject jobject = jelement.getAsJsonObject();
	        
            jelement = jobject.get("result");
            JsonArray jsonArray = jelement.getAsJsonArray();
            
            groupMap.put(0, AppConstants.SELECT);
            for(JsonElement element: jsonArray){
            	
            	JsonObject object = element.getAsJsonObject();
            	groupMap.put(object.get("guid").getAsInt(), object.get("name").getAsString());
            }
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private String populateUserMap(int GroupID) {
		try{
			String response = SocialServicesUtils.getSocialData(AppConstants.SOCIALKEYS.LIST_USER.getValue()+"&"+AppConstants.SOCIALKEYS.GROUP_GUID.getValue()+"="+GroupID);
			JsonElement jelement = new JsonParser().parse(response);
	        JsonObject jobject = jelement.getAsJsonObject();
	        
	        int statusFromElgg = jobject.get(AppConstants.SOCIALKEYS.STATUS.getValue()).getAsInt();
            
	        if(statusFromElgg == -1) {
	        	UserMap.put("All", AppConstants.SELECT);
	        } else {
	        	UserMap.put("All", AppConstants.ALL);
	        	jelement = jobject.get("result");
	        	JsonArray jsonArray = jelement.getAsJsonArray();
	        	for(JsonElement element: jsonArray){
	        		JsonObject object = element.getAsJsonObject();
	        		UserMap.put(object.get("email").getAsString(), object.get("name").getAsString());
	        	}
	        }
		}
		catch(Exception e){
			e.printStackTrace();
		}
		userListJson = new Gson().toJson(UserMap);
		return userListJson;
	}
	
	@Action(value="PopulateUserSelector")
	public String PopulateUserSelector() throws ClientProtocolException, IOException {
		System.out.println(groupIdTogetUser);	
		populateUserMap(groupIdTogetUser);
		description = "Populating user map for group id "+groupIdTogetUser;
		return AppConstants.AJAX_USER_LIST;
	}
	
	@Override
	public void setSession(Map<String, Object> arg0) {
		session = arg0;
	}

	public List<GroupsDTO> getGroupsDTOs() {
		return groupsDTOs;
	}

	public void setGroupsDTOs(List<GroupsDTO> groupsDTOs) {
		this.groupsDTOs = groupsDTOs;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public AudioStream getAs() {
		return as;
	}

	public void setAs(AudioStream as) {
		this.as = as;
	}
	public Map<Integer, String> getGroupMap() {
		return groupMap;
	}

	public void setGroupMap(Map<Integer, String> groupMap) {
		this.groupMap = groupMap;
	}

	public Map<String, String> getUserMap() {
		return UserMap;
	}

	public void setUserMap(Map<String, String> userMap) {
		UserMap = userMap;
	}

	public int getGroupIdTogetUser() {
		return groupIdTogetUser;
	}

	public void setGroupIdTogetUser(int groupIdTogetUser) {
		this.groupIdTogetUser = groupIdTogetUser;
	}

	public String getUserListJson() {
		return userListJson;
	}

	public void setUserListJson(String userListJson) {
		this.userListJson = userListJson;
	}

	public List<MessageDTO> getMessageDTOs() {
		return messageDTOs;
	}

	public void setMessageDTOs(List<MessageDTO> messageDTOs) {
		this.messageDTOs = messageDTOs;
	}

	

	public String getMessageContents() {
		return messageContents;
	}

	public void setMessageContents(String messageContents) {
		this.messageContents = messageContents;
	}

	public String getReciverUserNameKey() {
		return reciverUserNameKey;
	}

	public void setReciverUserNameKey(String reciverUserNameKey) {
		this.reciverUserNameKey = reciverUserNameKey;
	}

	public String getSendTo() {
		return sendTo;
	}

	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}

	public String getSendBy() {
		return sendBy;
	}

	public void setSendBy(String sendBy) {
		this.sendBy = sendBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
