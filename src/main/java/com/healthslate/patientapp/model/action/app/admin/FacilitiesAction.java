package com.healthslate.patientapp.model.action.app.admin;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.FacilityDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="admin/facility.tiles"),
    @Result(name="facilities", type="tiles", location="admin/facilities.tiles"),
    @Result(name="ajaxTable", type="stream", params={"contentType", "application/Json", "inputName", "is"}),
	@Result(name="ajax", type="json", params={"root", "jsonData"})
})

@SuppressWarnings("serial")
public class FacilitiesAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private long facilityId;
	private String jsonData;
	private String facilityData;
	private Facility facility;
	private String facilitiesListString;
	private String facilityAdminListString;
    private String coachesListString;

	private BaseDAO baseDAO = new BaseDAO();
	private FacilityDAO facilityDAO = new FacilityDAO();

	private Map<String, Object> session;
	private HttpServletRequest request;

	public String description;
	private String facilityAdminEmail;
	private String facilityAdminPhone;
	private String facilityAdminFirstName;
	private String facilityAdminLastName;

    private List<FacilityDTO> facilities;

    private String sEcho;
    private InputStream is;
    private Long iTotalRecords = null;
    private Integer iTotalDisplayRecords = null;
    private Map<Integer, String> fieldLabelMap;

    @Action(value="allFacilities")
    public String showFacilitiesPage() {
        description = "Displayed all facilities including atoz";
        return "facilities";
    }

    @Action(value="fetchFacilitiesData")
    public String fetchFacilitiesData() {
        JQueryDataTableParamModel param = DataTablesParamUtility.getParam(request);
        sEcho = param.sEcho;
        Long iTotalRecordsInt; // total number of records (un-filtered)
        int iTotalDisplayRecordsInt;
        populateFieldLabelsMap();
        String sortIndex = fieldLabelMap.get(param.iSortColumnIndex);

        facilities = facilityDAO.getFacilitiesList(param.iDisplayStart, param.iDisplayLength, param.sSearch, sortIndex, param.sSortDirection);
        iTotalRecordsInt =  facilityDAO.getFacilitiesListCount(param.sSearch);

        sEcho = param.sEcho;
        iTotalDisplayRecordsInt = facilities.size();
        iTotalRecords = iTotalRecordsInt;
        iTotalDisplayRecords = iTotalDisplayRecordsInt;

        //iTotalRecordsInt =
        JsonObject jsonResponse = new JsonObject();
        jsonResponse.addProperty("sEcho", sEcho);
        jsonResponse.addProperty("iTotalRecords", iTotalRecords);
        jsonResponse.addProperty("iTotalDisplayRecords", iTotalRecords);

        Gson gson = new Gson();
        jsonResponse.add("aaData", gson.toJsonTree(facilities));
        is = new ByteArrayInputStream(jsonResponse.toString().getBytes());

        description = "Fetched Facilities Data";
        return "ajaxTable";
    }

    private void populateFieldLabelsMap() {
        fieldLabelMap = new LinkedHashMap<Integer, String>();
        fieldLabelMap.put(0, "f.facility_id");
        fieldLabelMap.put(1, "f.`name`");
        fieldLabelMap.put(2, "f.contact_person_name");
        fieldLabelMap.put(3, "f.contact_info");
        fieldLabelMap.put(6, "innerQuery.fullName");
        fieldLabelMap.put(7, "innerQuery.pEmail");
    }


    @Action(value="facility")
    public String showFacilityPage() {
        description = "Displayed all facilities including atoz";
        facilitiesListString = StringEscapeUtils.escapeJava(JsonUtil.toJsonExcludedNull(facilityDAO.getFacilityDetailsById(facilityId)));
        coachesListString = StringEscapeUtils.escapeJava(JsonUtil.toJsonExcludedNull(new ProviderDAO().getCoachesByFacility(request, facilityId)));
        return ActionSupport.SUCCESS;
    }

	@Action(value="saveFacility")
	public String saveFacility() {
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

		//newly created
		try {
			facility = (Facility) JsonUtil.fromJson(facilityData, Facility.class);

            String logo = "logowhite.png";
            String slogan = "Diabetes education and support services are provided by HealthSlate LLC";
            String emailTextName = "HealthSlate LLC";
            String consentFileName = "default_consent.html";
            long leadCoach = 0;
            int groupId = 0;

            //setting default groupId to facility when newly created, __oz
            if(facility.getFacilityId() == null || facility.getFacilityId() == 0){
                String gId = (String) new PreferencesDAO().getPreference(AppConstants.PreferencesNames.SOCIAL_GROUP_ID.getValue());
                try {
                    groupId = Integer.parseInt(gId);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
                    returnMap.put(AppConstants.JsonConstants.REASON.name(), "Social group Id not set in preferences.");
                    jsonData = JsonUtil.toJson(returnMap);
                    return "ajax";
                }
                facility.setGroupId(groupId);
            }else{
                FacilityDTO facilityFromDB = facilityDAO.getFacilityDetailsById(facility.getFacilityId());
                logo = facilityFromDB.getLogo();
                slogan = facilityFromDB.getSlogan();
                emailTextName = facilityFromDB.getEmailTextName();
                groupId = facilityFromDB.getGroupId();
                consentFileName = ObjectUtils.isEmpty(facilityFromDB.getConsentFileName()) ? consentFileName : facilityFromDB.getConsentFileName();
            }

            facility.setFacilityLogo(logo);
            facility.setSlogan(slogan);
            facility.setEmailTextName(emailTextName);
            facility.setGroupId(groupId);
            facility.setConsentFormFile(consentFileName);
            facility.setTimezoneOffsetMillis(DateUtils.getTimezoneInMillisecondsByOffset(facility.getTimezoneOffset()));

			boolean shouldCallSave = saveAsFacilityAdmin(facility);
            if(shouldCallSave){
                baseDAO.save(facility);
                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
                description = "Saved facility "+facility.getName();
            }else{
                returnMap.put(AppConstants.JsonConstants.REASON.name(), "This email address is already registered.");
            }
		} catch (Exception e) {
			e.printStackTrace();
			description = "Error in saving facility details";
		}

		jsonData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

	private boolean saveAsFacilityAdmin(Facility facility) {
        boolean shouldSaved = false;
		if(!ObjectUtils.isEmpty(facilityAdminEmail) && !ObjectUtils.isEmpty(facilityAdminPhone)){
			try {
				User user = new UserDAO().getUserByEmail(facilityAdminEmail);
				if(user == null){
                    user = new User();
                    user.setEmail(facilityAdminEmail);
                    user.setPhone("+1 "+facilityAdminPhone);
					user.setFirstName(facilityAdminFirstName);
					user.setLastName(facilityAdminLastName);
					user.setIsEnabled(true);
					user.setUserType(AppConstants.Roles.ROLE_FACILITY_ADMIN.name());
					user.setRegistrationDate(new Date());

                    if(facility.getFacilityId() == 0){
                        facility.setFacilityId(null);
                    }

                    baseDAO.save(facility);
                    Provider provider = new Provider();
                    provider.setFacility(new Facility(facility.getFacilityId()));
                    provider.setType(AppConstants.PROVIDERTYPE.FACILITY_ADMIN.getValue());
					provider.setIsEmailEnabled(false);
					provider.setIsSMSEnabled(false);

                    user.setProvider(provider);
                    provider.setUser(user);

                    baseDAO.save(user);

                    UserRole userRole = new UserRoleDAO().getUserRoleById(user.getUserId());
                    if(userRole == null){
                        userRole = new UserRole();
                    }
                    userRole.setUser(user);
                    userRole.setAuthority(AppConstants.Roles.ROLE_FACILITY_ADMIN.name());
                    baseDAO.save(userRole);

                    EmailUtils.sendResetPasswordEmail(user, request, false);
                    shouldSaved = true;
                }else{
                    String type = user.getUserType();
                    if(type.equalsIgnoreCase(AppConstants.Roles.ROLE_FACILITY_ADMIN.name())){
                        user.setEmail(facilityAdminEmail);
                        user.setPhone("+1 "+facilityAdminPhone);
                        user.setFirstName(facilityAdminFirstName);
                        user.setLastName(facilityAdminLastName);
                        baseDAO.save(user);
                        shouldSaved = true;
                    }
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

        return shouldSaved;
	}

	@Action(value="getFacilityList")
	public String getFacilityList() {
		try {
			HashMap<String, String> returnMap = new LinkedHashMap<String, String>();
			returnMap.put("FACILITIES", JsonUtil.toJsonExcludedNull(new FacilityDAO().list(false)));
			returnMap.put("FACILITY_ADMINS", JsonUtil.toJson(facilityDAO.listFacilityAdmins(0)));
			jsonData = JsonUtil.toJson(returnMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		description = "Displayed all facilities including atoz";
		return "ajax";
	}

    @Action(value="resendEmailPinCode")
    public String resendEmailPinCode() {
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        User user = null;
        try {
            user = new UserDAO().getUserByEmail(facilityAdminEmail);
            if(user != null){
                EmailUtils.sendResetPasswordEmail(user, request, false);
                description = "Resending Email/PinCode to facility admin, email: " + user.getEmail() + ", pinCode sent to: "+ user.getPhone();
            }else{
                returnMap.put(AppConstants.JsonConstants.REASON.name(), "The email is not registered.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFacilityData() {
		return facilityData;
	}

	public void setFacilityData(String facilityData) {
		this.facilityData = facilityData;
	}

	public Facility getFacility() {
		return facility;
	}

	public void setFacility(Facility facility) {
		this.facility = facility;
	}

	public long getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(long facilityId) {
		this.facilityId = facilityId;
	}

	public String getFacilitiesListString() {
		return facilitiesListString;
	}

	public void setFacilitiesListString(String facilitiesListString) {
		this.facilitiesListString = facilitiesListString;
	}

	public String getFacilityAdminEmail() {
		return facilityAdminEmail;
	}

	public void setFacilityAdminEmail(String facilityAdminEmail) {
		this.facilityAdminEmail = facilityAdminEmail;
	}

	public String getFacilityAdminPhone() {
		return facilityAdminPhone;
	}

	public void setFacilityAdminPhone(String facilityAdminPhone) {
		this.facilityAdminPhone = facilityAdminPhone;
	}

	public String getFacilityAdminFirstName() {
		return facilityAdminFirstName;
	}

	public void setFacilityAdminFirstName(String facilityAdminFirstName) {
		this.facilityAdminFirstName = facilityAdminFirstName;
	}

	public String getFacilityAdminLastName() {
		return facilityAdminLastName;
	}

	public void setFacilityAdminLastName(String facilityAdminLastName) {
		this.facilityAdminLastName = facilityAdminLastName;
	}

	public String getFacilityAdminListString() {
		return facilityAdminListString;
	}

	public void setFacilityAdminListString(String facilityAdminListString) {
		this.facilityAdminListString = facilityAdminListString;
	}

    public String getsEcho() {
        return sEcho;
    }

    public void setsEcho(String sEcho) {
        this.sEcho = sEcho;
    }

    public InputStream getIs() {
        return is;
    }

    public void setIs(InputStream is) {
        this.is = is;
    }

    public Long getiTotalRecords() {
        return iTotalRecords;
    }

    public void setiTotalRecords(Long iTotalRecords) {
        this.iTotalRecords = iTotalRecords;
    }

    public Integer getiTotalDisplayRecords() {
        return iTotalDisplayRecords;
    }

    public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
        this.iTotalDisplayRecords = iTotalDisplayRecords;
    }

    public Map<Integer, String> getFieldLabelMap() {
        return fieldLabelMap;
    }

    public void setFieldLabelMap(Map<Integer, String> fieldLabelMap) {
        this.fieldLabelMap = fieldLabelMap;
    }

    public List<FacilityDTO> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<FacilityDTO> facilities) {
        this.facilities = facilities;
    }

    public String getCoachesListString() {
        return coachesListString;
    }

    public void setCoachesListString(String coachesListString) {
        this.coachesListString = coachesListString;
    }
}
