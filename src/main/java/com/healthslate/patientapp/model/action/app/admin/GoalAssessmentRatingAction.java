package com.healthslate.patientapp.model.action.app.admin;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.google.gson.Gson;
import com.healthslate.patientapp.model.dao.CoachGoalAssessmentDAO;
import com.healthslate.patientapp.model.dao.FilledFormDAO;
import com.healthslate.patientapp.model.dao.FormDataDAO;
import com.healthslate.patientapp.model.dao.GoalAssessmentRatingDAO;
import com.healthslate.patientapp.model.dao.GoalCategoryDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.entity.CoachGoalAssessment;
import com.healthslate.patientapp.model.entity.FilledForm;
import com.healthslate.patientapp.model.entity.GoalAssessmentRating;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.opensymphony.xwork2.ActionSupport;

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/goals.tiles"),
	@Result(name="create_goal", type="tiles", location="educator/createGoal.tiles"),
	@Result(name="ajax", type="json", params={"root", "ratingJsonData"})
})
public class GoalAssessmentRatingAction extends ActionSupport implements SessionAware, ServletRequestAware{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long filledFormId;
	private String ratingJsonData;
	private Long patientId;
	
	private Long formId;
	private int rating;
	private String remindString;
	
	private String notes;
	private String status;
	
	private GoalAssessmentRatingDAO assessmentRatingDAO = new GoalAssessmentRatingDAO();
	private PatientDAO patientDAO = new PatientDAO();
	private FilledFormDAO filledFormDAO = new FilledFormDAO();
	private CoachGoalAssessmentDAO coachGoalAssessmentDAO = new CoachGoalAssessmentDAO();
	private String record;
	private Map<String, Object> session;
	private HttpServletRequest request;
	
	public String description;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		setRequest(arg0);
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		session  = arg0;
	}
	
	@Action(value="saveRating")
	public String saveRating() {
		
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
	
		GoalAssessmentRating goalAssessmentRating = assessmentRatingDAO.getRatingByFormById(formId);	
		
		//new record
		if(goalAssessmentRating == null) {	
			goalAssessmentRating = new GoalAssessmentRating();
			goalAssessmentRating.setFilledForm(new FilledForm(formId));
			description = "Save "; 
		} else {
			description = "Update ";
		}
		
		goalAssessmentRating.setRatedOn(new Date());
		goalAssessmentRating.setRating(rating);
		Long id = assessmentRatingDAO.saveRating(goalAssessmentRating);
		
		if(id > 0) {
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());	
		}
		
		ratingJsonData = new Gson().toJson(returnMap); 
		description += "rating for goal assessment for patient id "+ patientId + returnMap.get(AppConstants.JsonConstants.STATUS.name());
		return "ajax";
	}

	@Action(value="saveReminder")
	public String saveReminder() {
		
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
	
		FilledForm filledForm = filledFormDAO.getFormById(formId);
		long id = 0;
		if(filledForm != null){
			boolean shouldRemind = false;			
			if(remindString.equalsIgnoreCase(AppConstants.REMINDER)){
				shouldRemind = true;
			}
			filledForm.setShouldRemind(shouldRemind);
			id = filledFormDAO.saveForm(filledForm);
		}
		
		if(id > 0) {
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());	
		}
		
		ratingJsonData = new Gson().toJson(returnMap); 
		description = "Save reminder for patient id "+ patientId + returnMap.get(AppConstants.JsonConstants.STATUS.name());
		return "ajax";
	}
	
	@Action(value="saveCoachAssessment")
	public String saveCoachAssessment() {
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		FilledForm filledForm = filledFormDAO.getFormById(formId);
		long id = 0;
		if(filledForm != null){					
			User user = (User) session.get(AppConstants.SessionKeys.USER.name());			
			CoachGoalAssessment coachGoalAssessment = new CoachGoalAssessment(status, filledForm, System.currentTimeMillis(), notes, user);
			id = coachGoalAssessmentDAO.saveCoachAssessment(coachGoalAssessment);
		}
		if(id > 0) {
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());	
		}
		ratingJsonData = new Gson().toJson(returnMap);
		description = "Save Coach Assessment  for patient id "+ patientId + returnMap.get(AppConstants.JsonConstants.STATUS.name());
		return "ajax";
	}
	
	
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public Long getFormId() {
		return formId;
	}

	public int getRating() {
		return rating;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public Long getFilledFormId() {
		return filledFormId;
	}

	public void setFilledFormId(Long filledFormId) {
		this.filledFormId = filledFormId;
	}

	public String getRatingJsonData() {
		return ratingJsonData;
	}

	public void setRatingJsonData(String ratingJsonData) {
		this.ratingJsonData = ratingJsonData;
	}

	public String getRemindString() {
		return remindString;
	}

	public void setRemindString(String remindString) {
		this.remindString = remindString;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
