package com.healthslate.patientapp.model.action.app.admin;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.healthslate.patientapp.model.dao.ServerLogDAO;
import com.healthslate.patientapp.model.dto.ServerLogsDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.DataTablesParamUtility;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/*
* ======= FILE CHANGE HISTORY =======
* [2/12/2015]: Created by __oz
* ===================================
 */

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="admin/hippaLogs.tiles"),
    @Result(name="ajax", type="stream", params={"contentType", "application/Json", "inputName", "is"})
})

@SuppressWarnings("serial")
public class HIPPALogsAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private String jsonData;
	private Map<String, Object> session;
	private HttpServletRequest request;
    private InputStream is;
    private String jsonString;
    private JsonObject jsonResponse;
    public List<ServerLogsDTO> aaData;
    public List<ServerLogsDTO> serverLogs;

    public String description;

    private String sEcho;
    private Long iTotalRecords = null;
    private Integer iTotalDisplayRecords = null;
    private Map<Integer, String> fieldLabelMap;
    private ServerLogDAO serverLogDAO = new ServerLogDAO();

	@Action(value="hippaLogs")
	public String showHippaLogsPage() {
        description = "Viewed HIPPA logs page";
		return ActionSupport.SUCCESS;
	}

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Action(value="fetchHippaLogs")
    public String fetchHippaLogs() throws IOException {

        aaData = new ArrayList<ServerLogsDTO>();

        JQueryDataTableParamModel param = DataTablesParamUtility.getParam(request);
        sEcho = param.sEcho;
        Long iTotalRecordsInt; // total number of records (un-filtered)
        int iTotalDisplayRecordsInt;
        populateFieldLabelsMap();
        String sortIndex = fieldLabelMap.get(param.iSortColumnIndex);

        int maxCount = serverLogDAO.getCount();
        serverLogs = serverLogDAO.list(param.iDisplayStart, param.iDisplayLength);
        iTotalRecordsInt = Long.parseLong(maxCount + "");

        sEcho = param.sEcho;
        aaData = serverLogs;
        iTotalDisplayRecordsInt = aaData.size();
        iTotalRecords = iTotalRecordsInt;
        iTotalDisplayRecords = iTotalDisplayRecordsInt;

        //iTotalRecordsInt =
        JsonObject jsonResponse = new JsonObject();
        jsonResponse.addProperty("sEcho", sEcho);
        jsonResponse.addProperty("iTotalRecords", iTotalRecords);
        jsonResponse.addProperty("iTotalDisplayRecords", iTotalRecords);

        Gson gson = new Gson();
        jsonResponse.add("aaData", gson.toJsonTree(aaData));
        is = new ByteArrayInputStream(jsonResponse.toString().getBytes());

        description = "Fetched HIPPA logs";

        return "ajax";
    }

    private void populateFieldLabelsMap(){
        fieldLabelMap = new LinkedHashMap<Integer, String>();
        fieldLabelMap.put(0, "actionName");
        fieldLabelMap.put(1, "description");
        fieldLabelMap.put(2, "loggedDate");
        fieldLabelMap.put(3, "loggedUserName");
        fieldLabelMap.put(4, "userRole");
    }

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

    public InputStream getIs() {
        return is;
    }

    public void setIs(InputStream is) {
        this.is = is;
    }

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }

    public JsonObject getJsonResponse() {
        return jsonResponse;
    }

    public void setJsonResponse(JsonObject jsonResponse) {
        this.jsonResponse = jsonResponse;
    }
}
