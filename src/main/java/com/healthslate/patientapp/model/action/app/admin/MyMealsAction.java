package com.healthslate.patientapp.model.action.app.admin;

import com.google.gson.Gson;
import com.healthslate.patientapp.model.dao.LogDAO;
import com.healthslate.patientapp.model.dto.RecentMealDTO;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.JsonUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/myMeals.tiles"),	
	@Result(name="ajax", type="json", params={"root", "ajaxData"})
})
public class MyMealsAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private boolean isProcessed;
	private String date;
	private String mealType;
	private Long patientId;
	private String jsonData;
	private String ajaxData;
	private LogDAO logDAO = new LogDAO();
	private Map<String, Object> session;
	private HttpServletRequest request;
	public String description;
	public String appPath;
	private Map<Integer, String> logTypeMap = new LinkedHashMap<Integer, String>();
	
	@Action(value="myMealsPage")
	public String openMyMealsPage(){
		appPath = this.request.getContextPath();
		if(!ObjectUtils.isEmpty(mealType) && patientId != null){
			List<RecentMealDTO> recentMealDTOs = logDAO.getAllRecentMeals(patientId, mealType, appPath + AppConstants.LOG_IMAGE_FOLDER, date, isProcessed);
			if(recentMealDTOs != null && recentMealDTOs.size() > 0){
				jsonData = StringEscapeUtils.escapeJava(new Gson().toJson(recentMealDTOs));
			}
			description = "Displayed My Meal Page for meal type "+mealType+" and patient id "+patientId;
		} else {
			description = "Displaying My Meal Page failed for meal type "+mealType+" and patient id "+patientId;
		}
		return ActionSupport.SUCCESS;
	}
	
	@Action(value="getMealsByType")
	public String getMealsByType(){
		appPath = this.request.getContextPath();
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		
		if(!ObjectUtils.isEmpty(mealType) && patientId != null){			
			List<RecentMealDTO> recentMealDTOs = logDAO.getAllRecentMeals(patientId, mealType, appPath + AppConstants.LOG_IMAGE_FOLDER, date, isProcessed);
			if(recentMealDTOs != null && recentMealDTOs.size() > 0){
				ajaxData = JsonUtil.toJson(recentMealDTOs);
				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
				returnMap.put(AppConstants.JsonConstants.DATA.name(), new Gson().toJson(recentMealDTOs));
			}
			description = "Displayed Meals for meal type "+mealType+" and patient id "+patientId;
		} else {
			description = "Displaying Meals failed for meal type "+mealType+" and patient id "+patientId;
		}
		
		ajaxData = JsonUtil.toJson(returnMap);
		return "ajax";
	}
	
	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getMealType() {
		return mealType;
	}

	public void setMealType(String mealType) {
		this.mealType = mealType;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public Map<Integer, String> getLogTypeMap() {
		return AppConstants.MEAL_TYPES;
	}

	public void setLogTypeMap(Map<Integer, String> logTypeMap) {
		this.logTypeMap = logTypeMap;
	}

	public String getAjaxData() {
		return ajaxData;
	}

	public void setAjaxData(String ajaxData) {
		this.ajaxData = ajaxData;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public boolean isProcessed() {
		return isProcessed;
	}

	public void setProcessed(boolean isProcessed) {
		this.isProcessed = isProcessed;
	}
}
