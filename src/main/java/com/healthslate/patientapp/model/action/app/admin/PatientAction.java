package com.healthslate.patientapp.model.action.app.admin;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.healthslate.patientapp.model.dao.CohortDAO;
import com.healthslate.patientapp.model.dao.DiabetesTypeDAO;
import com.healthslate.patientapp.model.dao.InsulinTypeDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.dao.ProviderDAO;
import com.healthslate.patientapp.model.entity.Cohort;
import com.healthslate.patientapp.model.entity.DiabetesType;
import com.healthslate.patientapp.model.entity.InsulinType;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.CommonUtils;
import com.healthslate.patientapp.util.SocialServicesUtils;
import com.opensymphony.xwork2.ActionSupport;

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/patientDetail.tiles"),
})

public class PatientAction extends ActionSupport implements SessionAware{

private static final long serialVersionUID = 1L;
	
	DiabetesTypeDAO diabetesTypeDAO = new DiabetesTypeDAO();
	InsulinTypeDAO insulinTypeDAO = new InsulinTypeDAO();
	ProviderDAO providerDAO = new ProviderDAO();
	PatientDAO patientDAO = new PatientDAO(); 
	CohortDAO cohortDAO = new CohortDAO();
	
	private Map<Long, String> diabetesTypesMap = new LinkedHashMap<Long, String>();
	private Map<Long, String> insulinTypesMap = new LinkedHashMap<Long, String>();
	private Map<Boolean, String> pregnancyMap = new LinkedHashMap<Boolean, String>();
	private Map<Integer, String> groupMap = new LinkedHashMap<Integer, String>();
	@SuppressWarnings("unused")
	private Map<String, Object> session;
	
	private List<String> genders;
	
	private Patient patient;
	private Long patientId;
	private Long providerId;
	
	private int groupId;
	
	public String description;
	
	@Action(value="fetchPatientDetail")
	public String fetchPatientDetail() throws ParseException {
		
		description = "Displayed Patient Details ";
		if( patientId != null && patientId != 0){
			patient = patientDAO.getPatientById(patientId);
			
			if(patient != null){
				patient.setDob(CommonUtils.getFormatedDateOnly(patient.getDob()));
				patient.setDateFirstInsulin(CommonUtils.getFormatedDateOnly(patient.getDateFirstInsulin()));
				patient.getUser().setRegistrationDate(CommonUtils.getFormatedDateOnly(patient.getUser().getRegistrationDate()));
				
				description = description + "for Patient Id: "+patient.getPatientId();
			}
			
		}
		initializeCollections();
		
		return SUCCESS;
	}
	
	private void initializeCollections() {
		makeGendersList();
		populateMaps();
	}
	
	public void populateMaps(){
		
		List<DiabetesType> diabetesTypes = diabetesTypeDAO.getDiabetesTypes();
		diabetesTypesMap.put(null, AppConstants.SELECT);
		for(DiabetesType diabetesType: diabetesTypes){
			diabetesTypesMap.put(diabetesType.getDiabetesTypeId(), diabetesType.getName());
		}
		
		List<InsulinType> insulinTypes = insulinTypeDAO.getInsulinTypes();
		insulinTypesMap.put(null, AppConstants.SELECT);
		for(InsulinType insulinType: insulinTypes){
			insulinTypesMap.put(insulinType.getInsulinTypeId(), insulinType.getName());
		}
		
		pregnancyMap.put(false, AppConstants.NO);
		pregnancyMap.put(true, AppConstants.YES);
		
		populateGroupMap();
	}
	
	private void populateGroupMap() {
		
		try{
			String response = SocialServicesUtils.getSocialData(AppConstants.SOCIALKEYS.LIST_GROUPS.getValue());
			
			JsonElement jelement = new JsonParser().parse(response);
	        JsonObject jobject = jelement.getAsJsonObject();
	        
            jelement = jobject.get("result");
            JsonArray jsonArray = jelement.getAsJsonArray();
            
            groupMap.put(0, AppConstants.SELECT);
            for(JsonElement element: jsonArray){
            	
            	JsonObject object = element.getAsJsonObject();
            	groupMap.put(object.get("guid").getAsInt(), object.get("name").getAsString());
            }
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void makeGendersList(){
		genders = new ArrayList<String>();
		genders.add("Male");
		genders.add("Female");
	}
	
	@Action(value="updatePatientDetail")
	public String updatePatientDetail() throws ClientProtocolException, IOException {
		
		populateMaps();
		if(patient != null){
			
			Patient pat = patientDAO.getPatientById(patient.getPatientId());
			patient.setProviders(pat.getProviders());
			
			Cohort cohort = cohortDAO.getCohortById(patient.getCohort().getCohortId());
			patient.setCohort(cohort);
			
			/*patient.getDiabetesType().setName(diabetesTypesMap.get(patient.getDiabetesType().getDiabetesTypeId()));
			patient.getInsulinType().setName(insulinTypesMap.get(patient.getInsulinType().getInsulinTypeId()));
			*/
			int id = SocialServicesUtils.createPatientInSocialGroup(patient.getUser());
			if(id != 0){
				patient.getUser().setSocialId(id);
			}
				
			if(patient.getUser().getGroupId() != 0){
				SocialServicesUtils.patientJoinGroup(patient, patient.getUser().getGroupId());
			}
			
			patientDAO.save(patient);
			addActionMessage("Patient Record Update Successfully");
		}
		initializeCollections();
		
		description = "Updated Patient Details for Patient Id: "+patient.getPatientId();
		return SUCCESS;
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		session = arg0;
	}
	
	public Patient getPatient() {
		return patient;
	}
	
	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	public List<String> getGenders() {
		return genders;
	}

	public void setGenders(List<String> genders) {
		this.genders = genders;
	}

	public Map<Long, String> getDiabetesTypesMap() {
		return diabetesTypesMap;
	}

	public void setDiabetesTypesMap(Map<Long, String> diabetesTypesMap) {
		this.diabetesTypesMap = diabetesTypesMap;
	}

	public Map<Long, String> getInsulinTypesMap() {
		return insulinTypesMap;
	}

	public void setInsulinTypesMap(Map<Long, String> insulinTypesMap) {
		this.insulinTypesMap = insulinTypesMap;
	}

	public Map<Boolean, String> getPregnancyMap() {
		return pregnancyMap;
	}

	public void setPregnancyMap(Map<Boolean, String> pregnancyMap) {
		this.pregnancyMap = pregnancyMap;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public Map<Integer, String> getGroupMap() {
		return groupMap;
	}

	public void setGroupMap(Map<Integer, String> groupMap) {
		this.groupMap = groupMap;
	}
}
