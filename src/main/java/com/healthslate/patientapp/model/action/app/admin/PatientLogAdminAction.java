package com.healthslate.patientapp.model.action.app.admin;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.healthslate.patientapp.model.dao.FoodLogDetailDAO;
import com.healthslate.patientapp.model.dao.FoodMasterDAO;
import com.healthslate.patientapp.model.dao.LogDAO;
import com.healthslate.patientapp.model.dao.MessageDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.dao.ProviderDAO;
import com.healthslate.patientapp.model.dao.ReportErrorDAO;
import com.healthslate.patientapp.model.dao.UserDAO;
import com.healthslate.patientapp.model.dto.PatientDTO;
import com.healthslate.patientapp.model.entity.FoodAudio;
import com.healthslate.patientapp.model.entity.Log;
import com.healthslate.patientapp.model.entity.Message;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.CommonUtils;
import com.healthslate.patientapp.util.ObjectUtils;
import com.opensymphony.xwork2.ActionSupport;

@Results({
	@Result(name=AppConstants.REPORT_ERROR, type="tiles", location="educator/reportError.tiles"),
	@Result(name=AppConstants.LOG, type="tiles", location="educator/patientLogs.tiles"),
	@Result(name=AppConstants.LOG_DETAIL, type="tiles", location="educator/patientLogDetail.tiles"),
    @Result(name=AppConstants.NEW_LOG, type="json", params={"root", "maxCount"}),
    
})

public class PatientLogAdminAction extends ActionSupport implements SessionAware,ServletRequestAware{

	private static final long serialVersionUID = 1L;
	private String AppPath; 
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy");
	
	FoodLogDetailDAO foodLogDetailDAO = new FoodLogDetailDAO();
	FoodMasterDAO foodMasterDAO = new FoodMasterDAO();
	ReportErrorDAO errorDAO = new ReportErrorDAO();
	ProviderDAO providerDAO = new ProviderDAO();
	PatientDAO patientDAO = new PatientDAO();
	MessageDAO messageDAO = new MessageDAO();
	UserDAO userDAO = new UserDAO();
	LogDAO logDAO = new LogDAO();

	private Map<String, Object> session;
	@SuppressWarnings("unused")
	private Map<String, String> servingUnitMap = new LinkedHashMap<String, String>();
	@SuppressWarnings("unused")
	private Map<Integer, String> statusMap = new LinkedHashMap<Integer, String>();
	private static Map<String, Integer> logMap = new LinkedHashMap<String, Integer>();

	private List<String> foodAudioLocations;
	private List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();
	private List<Log> unprocessedPatientLogs;
	private List<Log> processedPatientLogs;
	private List<Object> errorObjsForLogId;
	private List<Log> patientLogs;
	private List<Object> errorObjs;
	private HttpServletRequest request;
	private Log log;

	private Boolean logProcessStatus;
	private Boolean isAdvSearch;
	private Boolean isRoleAdmin = false;
	
	private String isFromError;
	private String errorDescription;
	private String logsJson;
	private String foodImageLocation;
	private String foodImageInputStream;
	private String nutritionInfoJson;
	private String newMessagesJson;
	private String actionMessage;
	private String statusMessage = AppConstants.ServicesConstants.DATA.name();
	private String message;
	
	private String foodName;
	private String servingSize;
	private String servingUnit;
	private String foodJson;
	
	private String totalCarbs = "0";
	private String totalCalories = "0";
	private String totalFats = "0";
	private String totalProtein = "0";
	private String logId;
	private String maxTime;

	private Integer foodCalories;
	private int status = 1;
	private int maxCount;
	
	private float foodCarbs;
	private float foodFats;
	private float foodProtein;
	
	private Date dateFrom = ObjectUtils.getSevenDaysBeforeDate();
	private Date createdOn;
	//private Date dateFrom = new Date();
	private Date dateTo = new Date();
	
	private Long foodId;
	private Long selFoodId;
	private long patientId = 0l;
	private long errorId;
	
	public String description;
	
	@Action(value="viewReportErrors")
	public String viewReportErrors() {
		errorObjs = errorDAO.getErrorsByCoachId(0);
		description = "View Report Error Page to Admin";
		return AppConstants.REPORT_ERROR;
	}

	@Action(value="findNewLogs")
	public String findNewLogs(){
		
		int count = logDAO.getLogsCount(patientId);
		
		if(maxCount < count){
			maxCount = count;
		}
		
		return AppConstants.NEW_LOG;
	}
	
	@Action(value="fetchPatientLogs")
	public String fetchPatientLogs(){
		
		if((Date) session.get(AppConstants.SessionKeys.DATE_FROM.name()) != null){
			dateFrom = (Date) session.get(AppConstants.SessionKeys.DATE_FROM.name());
		}
		
		if((Date) session.get(AppConstants.SessionKeys.DATE_TO.name()) != null){
			dateTo = (Date) session.get(AppConstants.SessionKeys.DATE_TO.name());
		}
		
		return fetchLogs();
	}
	
	@Action(value="fetchLogs")
	public String fetchLogs(){
		
		patientLogs = logDAO.getLogsByStatusAndDate(patientId, 0, dateFrom, dateTo);
		maxCount = logDAO.getLogsCount(patientId);
		setDates();
		
		//get processed and unprocessed logs
		processedPatientLogs = new ArrayList<Log>();
		unprocessedPatientLogs = new ArrayList<Log>();
		
		for(Log log: patientLogs){
			if(log.getIsProcessed()){
				processedPatientLogs.add(log);
			}
			else{
				unprocessedPatientLogs.add(log);
			}
		}
				
		//get unread messages count for each log
		populateLogMap(patientLogs, logMap);

		User user = (User) session.get(AppConstants.SessionKeys.ADMIN.name());
		description = "Patient Logs Found and displayed to admin "+user.getUserId();
		return AppConstants.LOG;
	}
	
	@Action(value="setDates")
	public void setDates(){
		session.put(AppConstants.SessionKeys.DATE_FROM.name(), dateFrom);
		session.put(AppConstants.SessionKeys.DATE_TO.name(), dateTo);
	}
	
	@Action(value="setSearchStatus")
	public void setSearchStatus(){
		session.put(AppConstants.SessionKeys.ADV_SEARCH.name(), isAdvSearch);
	}
	
	private void populateLogMap(List<Log> patientLogs, Map<String, Integer> logMap) {
		
		for(Log l: patientLogs){
			if(l.getFoodLogSummary() != null && l.getFoodLogSummary().getMessages() != null){
				
				int unreadMessageCount = 0;
				for(Message m: l.getFoodLogSummary().getMessages()){
					
					if(m.getReadStatus() == null || m.getReadStatus() == false){
						unreadMessageCount += 1;
					}
				}
				
				logMap.put(l.getLogId(), unreadMessageCount);
			}
		}
	}
	
	public static int getunreadMessagesCount(String logId) {
		
		if(logMap != null && logMap.containsKey(logId)){
			return logMap.get(logId);
		}
		return 0;
	}

	@Action(value="fetchLogDetail")
	public String fetchLogDetail() throws IOException{
		AppPath =this.request.getContextPath();
		List<Log> logs = new ArrayList<Log>();
		addActionError(actionMessage);
		
		if(!ObjectUtils.isEmpty(logId)){
			
			log = logDAO.getLogById(logId);
			if(log != null){
			
				//get unique log child
				logs.add(log);
				logs = CommonUtils.getUniqueResults(logs);
				log = logs.get(0);
			
				logProcessStatus = log.getIsProcessed();
				createdOn = new Date(log.getLogTime());
				
				if(log.getFoodLogSummary() != null){
					
					//change status of log un read messages to read
					if(log.getFoodLogSummary() != null && log.getFoodLogSummary().getMessages().size() > 0){
						
						Long messageMaxTime = null;
						for(Message msg: log.getFoodLogSummary().getMessages()){
							
							msg.setReadStatus(true);
							messageDAO.save(msg);
							
							if(messageMaxTime == null){
								messageMaxTime = msg.getTimestamp();
							}
							else if(msg.getTimestamp() != null && messageMaxTime.compareTo(msg.getTimestamp()) < 0){
								messageMaxTime = msg.getTimestamp();
							}
						}
						
						maxTime = CommonUtils.gmtToest(messageMaxTime);
						
					}
					
					//get totals 
					//Map<String, Float> calcualtedValues = FoodLogUtils.calculateNutritionSum(log.getFoodLogSummary().getFoodLogDetails());
					totalCarbs = CommonUtils.getCalculatedSize(String.valueOf(log.getFoodLogSummary().getCarbs()), "");
					totalFats = CommonUtils.getCalculatedSize(String.valueOf(log.getFoodLogSummary().getFats()), "");
					totalProtein = CommonUtils.getCalculatedSize(String.valueOf(log.getFoodLogSummary().getProtein()), "");
					totalCalories = CommonUtils.getCalculatedSize(String.valueOf(log.getFoodLogSummary().getCalories()), "");
					
					//get food image
					if(log.getFoodLogSummary() != null && 
							log.getFoodLogSummary().getHasImage() != null &&
							log.getFoodLogSummary().getHasImage() &&
							log.getFoodLogSummary().getFoodImage() != null &&
							log.getFoodLogSummary().getFoodImage().getImageName() != null){
						
						try {
							foodImageLocation = AppPath + AppConstants.LOG_IMAGE_FOLDER + log.getFoodLogSummary().getFoodImage().getImageName();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					//get food audio
					if(log.getFoodLogSummary() != null && 
							log.getFoodLogSummary().getFoodAudios() != null &&
							log.getFoodLogSummary().getFoodAudios().size() > 0){
						
						foodAudioLocations = new ArrayList<String>();
						for(FoodAudio audio: log.getFoodLogSummary().getFoodAudios()){
							
							try {
								foodAudioLocations.add(AppPath + AppConstants.LOG_AUDIO_FOLDER + audio.getAudioName());
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
				
				if(log.getPatient() != null){
					int years = getYear(log.getPatient().getDob());
					if(years > 0){
						log.getPatient().setAge(years + " yrs");
					}
					else {
						log.getPatient().setAge("N/A");
					}
				}
			}
			
			errorObjsForLogId = errorDAO.getErrorsByLogId(logId);
		}
		
		User user = (User) session.get(AppConstants.SessionKeys.ADMIN.name());
		description = "Fetch Log Detail of Log Id: "+log.getLogId() + " and displayed to admin "+user.getUserId();
		isRoleAdmin = true;
		
		return AppConstants.LOG_DETAIL;
	}

	private int getYear(Date dob){ 
		SimpleDateFormat simpleDateformat=new SimpleDateFormat("yyyy");
      
		if(dob != null){
			int yearOfDateOfBirth = Integer.parseInt(simpleDateformat.format(dob));
			int yearOfTodaysDate = Integer.parseInt(simpleDateformat.format(new Date()));
			return yearOfTodaysDate - yearOfDateOfBirth;
		}
		
		return 0;
    }
	
	@Override
	public void setSession(Map<String, Object> arg0) {
		session = arg0;
	}
	
	public List<Log> getPatientLogs() {
		return patientLogs;
	}

	public void setPatientLogs(List<Log> patientLogs) {
		this.patientLogs = patientLogs;
	}

	public String getLogId() {
		return logId;
	}

	public void setLogId(String logId) {
		this.logId = logId;
	}

	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}

	public Boolean getLogProcessStatus() {
		return logProcessStatus;
	}

	public void setLogProcessStatus(Boolean logProcessStatus) {
		this.logProcessStatus = logProcessStatus;
	}

	public String getFoodName() {
		return foodName;
	}

	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}

	public Long getFoodId() {
		return foodId;
	}

	public void setFoodId(Long foodId) {
		this.foodId = foodId;
	}

	public Integer getFoodCalories() {
		return foodCalories;
	}

	public void setFoodCalories(Integer foodCalories) {
		this.foodCalories = foodCalories;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getFoodJson() {
		return foodJson;
	}

	public void setFoodJson(String foodJson) {
		this.foodJson = foodJson;
	}

	public Map<Integer, String> getStatusMap() {
		return AppConstants.LOG_STATUS_MAP;
	}

	public void setStatusMap(Map<Integer, String> statusMap) {
		this.statusMap = statusMap;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getNutritionInfoJson() {
		return nutritionInfoJson;
	}

	public void setNutritionInfoJson(String nutritionInfoJson) {
		this.nutritionInfoJson = nutritionInfoJson;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Map<String, String> getServingUnitMap() {
		return AppConstants.SERVING_UNIT_MAP;
	}

	public void setServingUnitMap(Map<String, String> servingUnitMap) {
		this.servingUnitMap = servingUnitMap;
	}

	public String getServingSize() {
		return servingSize;
	}

	public void setServingSize(String servingSize) {
		this.servingSize = servingSize;
	}

	public String getServingUnit() {
		return servingUnit;
	}

	public void setServingUnit(String servingUnit) {
		this.servingUnit = servingUnit;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getFoodImageInputStream() {
		return foodImageInputStream;
	}

	public void setFoodImageInputStream(String foodImageInputStream) {
		this.foodImageInputStream = foodImageInputStream;
	}

	public String getActionMessage() {
		return actionMessage;
	}

	public void setActionMessage(String actionMessage) {
		this.actionMessage = actionMessage;
	}

	public String getTotalCarbs() {
		return totalCarbs;
	}

	public void setTotalCarbs(String totalCarbs) {
		this.totalCarbs = totalCarbs;
	}

	public String getTotalCalories() {
		return totalCalories;
	}

	public void setTotalCalories(String totalCalories) {
		this.totalCalories = totalCalories;
	}

	public String getTotalFats() {
		return totalFats;
	}

	public void setTotalFats(String totalFats) {
		this.totalFats = totalFats;
	}

	public String getTotalProtein() {
		return totalProtein;
	}

	public void setTotalProtein(String totalProtein) {
		this.totalProtein = totalProtein;
	}

	public Map<String, Integer> getLogMap() {
		return logMap;
	}

	public String getNewMessagesJson() {
		return newMessagesJson;
	}

	public void setNewMessagesJson(String newMessagesJson) {
		this.newMessagesJson = newMessagesJson;
	}

	public String getMaxTime() {
		return maxTime;
	}

	public void setMaxTime(String maxTime) {
		this.maxTime = maxTime;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public float getFoodCarbs() {
		return foodCarbs;
	}

	public void setFoodCarbs(float foodCarbs) {
		this.foodCarbs = foodCarbs;
	}

	public float getFoodFats() {
		return foodFats;
	}

	public void setFoodFats(float foodFats) {
		this.foodFats = foodFats;
	}

	public float getFoodProtein() {
		return foodProtein;
	}

	public void setFoodProtein(float foodProtein) {
		this.foodProtein = foodProtein;
	}

	public Long getSelFoodId() {
		return selFoodId;
	}

	public void setSelFoodId(Long selFoodId) {
		this.selFoodId = selFoodId;
	}

	public int getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(int maxCount) {
		this.maxCount = maxCount;
	}

	public String getLogsJson() {
		return logsJson;
	}

	public void setLogsJson(String logsJson) {
		this.logsJson = logsJson;
	}

	public List<Log> getUnprocessedPatientLogs() {
		return unprocessedPatientLogs;
	}

	public void setUnprocessedPatientLogs(List<Log> unprocessedPatientLogs) {
		this.unprocessedPatientLogs = unprocessedPatientLogs;
	}

	public List<Log> getProcessedPatientLogs() {
		return processedPatientLogs;
	}

	public void setProcessedPatientLogs(List<Log> processedPatientLogs) {
		this.processedPatientLogs = processedPatientLogs;
	}

	public Boolean getIsAdvSearch() {
		return isAdvSearch;
	}

	public void setIsAdvSearch(Boolean isAdvSearch) {
		this.isAdvSearch = isAdvSearch;
	}

	public Boolean getIsRoleAdmin() {
		return isRoleAdmin;
	}

	public void setIsRoleAdmin(Boolean isRoleAdmin) {
		this.isRoleAdmin = isRoleAdmin;
	}

	public List<PatientDTO> getPatientDTOs() {
		return patientDTOs;
	}

	public void setPatientDTOs(List<PatientDTO> patientDTOs) {
		this.patientDTOs = patientDTOs;
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public String getFoodImageLocation() {
		return foodImageLocation;
	}

	public void setFoodImageLocation(String foodImageLocation) {
		this.foodImageLocation = foodImageLocation;
	}

	public List<Object> getErrorObjs() {
		return errorObjs;
	}

	public void setErrorObjs(List<Object> errorObjs) {
		this.errorObjs = errorObjs;
	}

	public String getIsFromError() {
		return isFromError;
	}

	public void setIsFromError(String isFromError) {
		this.isFromError = isFromError;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public long getErrorId() {
		return errorId;
	}

	public void setErrorId(long errorId) {
		this.errorId = errorId;
	}

	public List<Object> getErrorObjsForLogId() {
		return errorObjsForLogId;
	}

	public void setErrorObjsForLogId(List<Object> errorObjsForLogId) {
		this.errorObjsForLogId = errorObjsForLogId;
	}

	public List<String> getFoodAudioLocations() {
		return foodAudioLocations;
	}

	public void setFoodAudioLocations(List<String> foodAudioLocations) {
		this.foodAudioLocations = foodAudioLocations;
	}

	public String getAppPath() {
		return AppPath;
	}

	public void setAppPath(String appPath) {
		AppPath = appPath;
	}

	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		this.request=arg0 ;
		
	}
}
