package com.healthslate.patientapp.model.action.app.admin;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.MembersDataDTO;
import com.healthslate.patientapp.model.dto.PatientDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import com.healthslate.patientapp.ws.SocialGroupServices;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;

/*
* ======= FILE CHANGE HISTORY =======
* [2/2/2015]: Created by __oz
* [2/23/2015]: send GCM to patients when assign lead coach, __oz
* ===================================
 */

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="admin/providerPatient.tiles"),
	@Result(name="ajax", type="json", params={"root", "jsonData"}),
	@Result(name="ajaxTable", type="stream", params={"contentType", "application/Json", "inputName", "is"})
})

@SuppressWarnings("serial")
public class ProviderPatientMapAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private String jsonData;
	private String patientListString;
	private String providerListString;
	private String sEcho;
	private long patientId;
	private long providerId;
	PatientDTO patientDTO;
	private Long iTotalRecords = null;
	private Integer iTotalDisplayRecords = null;
	private BaseDAO baseDAO = new BaseDAO();
	private PatientDAO patientDAO = new PatientDAO();
	private UserDAO userDAO = new UserDAO();
	private InputStream is;
	private Map<Integer, String> fieldLabelMap;
	private Map<String, Object> session;
	private HttpServletRequest request;
    private String email;
	public String description;
    private String uuid;

	@Action(value="membersData")
	public String showProviderPatientPage() {
		description = "Displayed Feedback logs page";
		return ActionSupport.SUCCESS;
	}

	@Action(value="fetchMembersData")
	public String fetchMembersData() {
		PatientDAO patientDAO = new PatientDAO();
		List<MembersDataDTO> membersDataDTOs = new ArrayList<MembersDataDTO>();

		JQueryDataTableParamModel param = DataTablesParamUtility.getParam(request);
		sEcho = param.sEcho;
		Long iTotalRecordsInt; // total number of records (un-filtered)
		int iTotalDisplayRecordsInt;

		iTotalRecordsInt = patientDAO.getMembersDataListCount(param.sSearch, 0);

		populateFieldLabelsMap();
		String sortIndex = fieldLabelMap.get(param.iSortColumnIndex);

		membersDataDTOs = patientDAO.getMembersDataList(param.iDisplayStart, param.iDisplayLength, param.sSearch, sortIndex, param.sSortDirection, 0);
//		int maxCount = patientDAO.count();
//		iTotalRecordsInt = Long.parseLong(maxCount + "");

		sEcho = param.sEcho;
		iTotalDisplayRecordsInt = membersDataDTOs.size();
		iTotalRecords = iTotalRecordsInt;
		iTotalDisplayRecords = iTotalDisplayRecordsInt;

		//iTotalRecordsInt =
		JsonObject jsonResponse = new JsonObject();
		jsonResponse.addProperty("sEcho", sEcho);
		jsonResponse.addProperty("iTotalRecords", iTotalRecords);
		jsonResponse.addProperty("iTotalDisplayRecords", iTotalRecords);

		populateVersionNoFromFilterLogs(membersDataDTOs);
		Gson gson = new Gson();
		jsonResponse.add("aaData", gson.toJsonTree(membersDataDTOs));
		is = new ByteArrayInputStream(jsonResponse.toString().getBytes());

		description = "Fetched Members Data";
		return "ajaxTable";
	}

	private void populateFieldLabelsMap() {
		fieldLabelMap = new LinkedHashMap<Integer, String>();
		fieldLabelMap.put(0, "p.patient_id");
        fieldLabelMap.put(1, "us.first_name");
        fieldLabelMap.put(2, "us.email");
        fieldLabelMap.put(3, "us.registration_date");
        fieldLabelMap.put(4, "p.device_mac_address");
		fieldLabelMap.put(6, "f.`name`");
        fieldLabelMap.put(7, "total_logs");
	}

	@Action(value="saveLeadCoach")
	public String saveLeadCoach() {

		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

		//newly created
		try {
			Patient patient = patientDAO.getPatientById(patientId);
			if(patient != null){
				patient.setLeadCoachId(providerId);
                patient.setIsApproved("3");
			}

			baseDAO.save(patient);

            //[2/23/2015]: send GCM to patients when assign lead coach __oz
            Provider provider = new ProviderDAO().getProviderById(providerId);
            NotifyUtils.notifyPatientsOfNewCoach(provider, request);

			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
		} catch (Exception e) {
			e.printStackTrace();
		}
		description = "Saved lead coach for patient id "+patientId+" "+returnMap.get(AppConstants.JsonConstants.STATUS.name());
		jsonData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

	@Action(value="savePrimaryFoodCoach")
	public String savePrimaryFoodCoach() {

		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

		//newly created
		try {
			Patient patient = patientDAO.getPatientById(patientId);
			if(patient != null){
				patient.setPrimaryFoodCoachId(providerId);
                patient.setIsApproved("3");
			}

			baseDAO.save(patient);

			//[2/23/2015]: send GCM to patients when assign lead coach __oz
			Provider provider = new ProviderDAO().getProviderById(providerId);
			NotifyUtils.notifyPatientsOfNewCoach(provider, request);

			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
		} catch (Exception e) {
			e.printStackTrace();
		}
		description = "Saved Primary Food Coach for patient id "+patientId+" "+returnMap.get(AppConstants.JsonConstants.STATUS.name());
		jsonData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

    //gets user activity count from elgg service
    @Action(value="getUserActivityCount")
    public String getUserActivityCount(){
        Map<String, Object> returnMap = new LinkedHashMap<String, Object>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {
            UserActivity userActivity = new SocialGroupServices().getUserActivityCounts(email);
            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.JsonConstants.DATA.name(), userActivity);
        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

	@Action(value="getProviderListByPatientId")
	public String getProviderListByPatientId() {
		jsonData = JsonUtil.toJsonExcludedNull(userDAO.getCoachesByPatientId(patientId, true));
		description = "Displayed providers list by patient id "+patientId;
		return "ajax";
	}

	private void populateVersionNoFromFilterLogs(List<MembersDataDTO> members) {
		ServiceFilterLogDAO serverFilterLogDAO =  new ServiceFilterLogDAO();
		for (MembersDataDTO member: members) {
			String versionNo = serverFilterLogDAO.getLatestVersionNo(member.getPatientId());
            String deviceAndVersion = "N/A";
            if(!versionNo.equalsIgnoreCase("N/A") && member != null){
                deviceAndVersion = member.getDeviceType() + " / " + versionNo;
            }
			member.setVersionNo(deviceAndVersion.trim());

			UserActivity userActivity = null;
			member.setUserActivity(userActivity);
		}
	}

    @Action(value="removeMember")
    public String removeMember() {

        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        try {
            if(!ObjectUtils.isEmpty(uuid)){
                Patient patient = new PatientDAO().getPatientByUUID(uuid);
                if(patient != null){
                    User user = patient.getUser();
                    String emailNew = CommonUtils.flushEmail(user.getEmail());
					SocialServicesUtils.removeUserFromElgg(user.getEmail());
                    patient.setIsDeleted(true);
                    patient.setDeletedOn(new Date());
                    user.setEmail(emailNew);
                    new BaseDAO().save(user);
                    returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        description = "Removing Member with uuid: " + uuid;
        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	public String getPatientListString() {
		return patientListString;
	}

	public void setPatientListString(String patientListString) {
		this.patientListString = patientListString;
	}

	public String getProviderListString() {
		return providerListString;
	}

	public void setProviderListString(String providerListString) {
		this.providerListString = providerListString;
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public long getProviderId() {
		return providerId;
	}

	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}

	public PatientDTO getPatientDTO() {
		return patientDTO;
	}

	public void setPatientDTO(PatientDTO patientDTO) {
		this.patientDTO = patientDTO;
	}

	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}

	public BaseDAO getBaseDAO() {
		return baseDAO;
	}

	public void setBaseDAO(BaseDAO baseDAO) {
		this.baseDAO = baseDAO;
	}

	public PatientDAO getPatientDAO() {
		return patientDAO;
	}

	public void setPatientDAO(PatientDAO patientDAO) {
		this.patientDAO = patientDAO;
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public Map<Integer, String> getFieldLabelMap() {
		return fieldLabelMap;
	}

	public void setFieldLabelMap(Map<Integer, String> fieldLabelMap) {
		this.fieldLabelMap = fieldLabelMap;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public Long getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(Long iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public InputStream getIs() {
		return is;
	}

	public void setIs(InputStream is) {
		this.is = is;
	}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
