package com.healthslate.patientapp.model.action.app.admin;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.CoachDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Hibernate;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;

/*
* ======= FILE CHANGE HISTORY =======
* [1/29/2015]: Created by, __oz
* [2/13/2015]: getFacility groupId and assign to user and post on social, __oz
* [2/23/2015]: send GCM to patients when assign lead coach, __oz
* ===================================
 */

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="admin/coach.tiles"),
    @Result(name="coaches", type="tiles", location="admin/coaches.tiles"),
    @Result(name="ajaxTable", type="stream", params={"contentType", "application/Json", "inputName", "is"}),
	@Result(name="ajax", type="json", params={"root", "jsonData"})
})

@SuppressWarnings("serial")
public class ProvidersAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private long providerId;
	private String jsonData;
	private String userData;
	private User user;
	private String facilitiesListString;
	private String providerListString;
    private String oldPassword;

	private long facilityId;
	private String designation;
    private int sortType;
    private long coachId;

    private boolean isEmailEnabled;
    private boolean isSMSEnabled;
    private boolean isAccessAll;

	private BaseDAO baseDAO = new BaseDAO();
	private ProviderDAO providerDAO = new ProviderDAO();
	private UserDAO userDAO = new UserDAO();

	private Map<String, Object> session;
	private HttpServletRequest request;

	public String description;

    private String sEcho;
    private InputStream is;
    private Long iTotalRecords = null;
    private Integer iTotalDisplayRecords = null;
    private Map<Integer, String> fieldLabelMap;

    private long facilityIdPref;

    @Action(value="coaches")
    public String showCoachesPage() {
        description = "Display Coaches Page";
        return "coaches";
    }

    @Action(value="fetchCoachesData")
    public String fetchCoachesData() {
        List<CoachDTO> coachDTOs = new ArrayList<CoachDTO>();

        JQueryDataTableParamModel param = DataTablesParamUtility.getParam(request);
        sEcho = param.sEcho;
        Long iTotalRecordsInt; // total number of records (un-filtered)
        int iTotalDisplayRecordsInt;
        populateFieldLabelsMap();
        String sortIndex = fieldLabelMap.get(param.iSortColumnIndex);

        coachDTOs = providerDAO.getCoachesDataList(param.iDisplayStart, param.iDisplayLength, param.sSearch, sortIndex, param.sSortDirection, 0, request);
        iTotalRecordsInt = providerDAO.getCoachesDataListCount(param.sSearch,0);

        sEcho = param.sEcho;
        iTotalDisplayRecordsInt = coachDTOs.size();
        iTotalRecords = iTotalRecordsInt;
        iTotalDisplayRecords = iTotalDisplayRecordsInt;

        //iTotalRecordsInt =
        JsonObject jsonResponse = new JsonObject();
        jsonResponse.addProperty("sEcho", sEcho);
        jsonResponse.addProperty("iTotalRecords", iTotalRecords);
        jsonResponse.addProperty("iTotalDisplayRecords", iTotalRecords);

        Gson gson = new Gson();
        jsonResponse.add("aaData", gson.toJsonTree(coachDTOs));
        is = new ByteArrayInputStream(jsonResponse.toString().getBytes());

        description = "Fetched Members Data";
        return "ajaxTable";
    }

    private void populateFieldLabelsMap() {
        fieldLabelMap = new LinkedHashMap<Integer, String>();
        fieldLabelMap.put(0, "p.provider_id");
        fieldLabelMap.put(1, "us.first_name");
        fieldLabelMap.put(2, "us.email");
        fieldLabelMap.put(3, "us.phone");
        fieldLabelMap.put(4, "us.user_type");
        fieldLabelMap.put(5, "f.`name`");
    }

	@Action(value="coach")
	public String showProviderPage() {
		facilitiesListString = StringEscapeUtils.escapeJava(JsonUtil.toJsonExcludedNull(new FacilityDAO().listRaw(false)));
		providerListString = StringEscapeUtils.escapeJava(JsonUtil.toJsonExcludedNull(providerDAO.getCoachDetailsById(coachId)));
        facilityIdPref = Long.parseLong(new PreferencesDAO().getPreference(AppConstants.PreferencesNames.HEALTHSLATE_FACILITY_ID.getValue()));
		description = "Display Coach Page";
		return ActionSupport.SUCCESS;
	}

	@Action(value="saveProvider")
	public String saveProvider() {

		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        FacilityDAO facilityDAO = new FacilityDAO();
		//newly created
		try {
			user = (User) JsonUtil.fromJson(userData, User.class);
			Provider provider = null;
			user.setProvider(provider);
            Facility facilityFromDb = facilityDAO.getFacilityWithIdAndPatients(facilityId);
            facilityIdPref = Long.parseLong(new PreferencesDAO().getPreference(AppConstants.PreferencesNames.HEALTHSLATE_FACILITY_ID.getValue()));

            if(facilityFromDb == null){
                facilityFromDb = facilityDAO.getFacilityById(facilityId);
            }

			boolean isNewCoach = false;
            int groupId = facilityFromDb.getGroupId();

            if(facilityId != facilityIdPref){
                isAccessAll = false;
            }

            //update scenario
            if(user.getUserId() != 0l){

                //check for provider
                provider = providerDAO.getProviderByUser(user.getUserId());
                oldPassword = provider.getUser().getPassword();
                if(provider != null){
                    user.setRegistrationDate(provider.getUser().getRegistrationDate());
                    user.setPassword(oldPassword);
                    provider.setUser(user);
                    provider.setFacility(null);
                    provider.setType(user.getUserType());
                    provider.setSortType(sortType);
                }
                description = "Updated ";
            } else {
                User userFromDB = userDAO.getUserByEmail(user.getEmail());
                if(userFromDB == null){
                    //new create provider and user with it
                    provider = createNewProvider(user);
                    user.setRegistrationDate(new Date());
                    description = "Saved ";

					isNewCoach = true;

                } else {
                    description = "Coach already existed ";
                    returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
                    returnMap.put(AppConstants.JsonConstants.REASON.name(), "The email address is registered to another user.");
                    jsonData = JsonUtil.toJson(returnMap);
                    return "ajax";
                }
            }

            //mean its atoz facility
            if(facilityIdPref == facilityId){
                provider.setUser(null);
                List<Patient> allPatients = new PatientDAO().list();
                provider.setPatients(allPatients);
                provider.setUser(user);
            }else{
                List<Patient> patientsList = facilityFromDb.getPatients();
                for(Patient patient: patientsList) {
                    List<Provider> providers = new ArrayList<Provider>();
                    providers.add(provider);
                    patient.setProviders(providers);
                }
                provider.setPatients(patientsList);
            }

            user.setIsEnabled(true);

            if(provider != null){
                provider.setFacility(new Facility(facilityId));
                provider.setDesignation(designation);
            }

            provider.setIsAccessAll(isAccessAll);
            provider.setIsEmailEnabled(isEmailEnabled);
            provider.setIsSMSEnabled(isSMSEnabled);
			user.setProvider(provider);

            //getFacility groupId and assign to user, __oz
			user.setGroupId(groupId);
            user.setPassword(oldPassword);
            user.setPhone(CommonUtils.formatPhoneNumber(user.getPhone()));
            baseDAO.save(user);

			UserRole userRole = new UserRoleDAO().getUserRoleById(user.getUserId());
			if(userRole == null){
				userRole = new UserRole();
			}
			userRole.setUser(user);
			userRole.setAuthority(AppConstants.Roles.ROLE_PROVIDER.name());

			baseDAO.save(userRole);

			if(isNewCoach){
				boolean isSentEmail = EmailUtils.sendResetPasswordEmail(user, request, false);
				LOG.info("saveProvider action: sending reset password email to "+ user.getEmail() + ", isEmailSent: "+ isSentEmail);
			}

			SocialServicesUtils.createPatientInSocialGroup(user);
			SocialServicesUtils.patientJoinGroup(user, facilityFromDb.getGroupId());

            //[2/23/2015]: end GCM to patients when assign lead coach, __oz
            NotifyUtils.notifyPatientsOfNewCoach(provider, request);

			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			description += "provider against user id "+user.getUserId()+" and provider id "+provider.getProviderId();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		jsonData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

	@Action(value="getProviderList")
	public String getProviderList() {
		jsonData = JsonUtil.toJson(userDAO.onlyProviders(request));
		description = "Displayed providers list";
		return "ajax";
	}

	private Provider createNewProvider(User user){
		Provider provider = new Provider();
		provider.setType(user.getUserType());
		provider.setUser(user);
        provider.setSortType(sortType);
		return provider;
	}

    @Action(value="removeCoach")
    public String removeCoach() {

        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        try {

            Provider provider = new ProviderDAO().getProviderById(providerId);
            if(provider != null){
                User user = provider.getUser();
                String emailNew = CommonUtils.flushEmail(user.getEmail());
                SocialServicesUtils.removeUserFromElgg(user.getEmail());
                provider.setIsDeleted(true);
                provider.setDeletedOn(new Date());
                user.setEmail(emailNew);
                new BaseDAO().save(user);

                new PatientDAO().removeCoachAssignmentFromMember(provider.getProviderId());

                //[6/01/2015]: when remove a coach, __oz
                NotifyUtils.notifyPatientsOfNewCoach(provider, request);

                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        description = "Removing Coach with id: " + providerId;
        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUserData() {
		return userData;
	}

	public void setUserData(String userData) {
		this.userData = userData;
	}

	public String getFacilitiesListString() {
		return facilitiesListString;
	}

	public void setFacilitiesListString(String facilitiesListString) {
		this.facilitiesListString = facilitiesListString;
	}

	public String getProviderListString() {
		return providerListString;
	}

	public void setProviderListString(String providerListString) {
		this.providerListString = providerListString;
	}

	public long getProviderId() {
		return providerId;
	}

	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(long facilityId) {
		this.facilityId = facilityId;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

    public int getSortType() {
        return sortType;
    }

    public void setSortType(int sortType) {
        this.sortType = sortType;
    }

	public boolean getIsEmailEnabled() {
		return isEmailEnabled;
	}

	public void setIsEmailEnabled(boolean isEmailEnabled) {
		this.isEmailEnabled = isEmailEnabled;
	}

	public boolean getIsSMSEnabled() {
		return isSMSEnabled;
	}

	public void setIsSMSEnabled(boolean isSMSEnabled) {
		this.isSMSEnabled = isSMSEnabled;
	}

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getsEcho() {
        return sEcho;
    }

    public void setsEcho(String sEcho) {
        this.sEcho = sEcho;
    }

    public InputStream getIs() {
        return is;
    }

    public void setIs(InputStream is) {
        this.is = is;
    }

    public Long getiTotalRecords() {
        return iTotalRecords;
    }

    public void setiTotalRecords(Long iTotalRecords) {
        this.iTotalRecords = iTotalRecords;
    }

    public Integer getiTotalDisplayRecords() {
        return iTotalDisplayRecords;
    }

    public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
        this.iTotalDisplayRecords = iTotalDisplayRecords;
    }

    public Map<Integer, String> getFieldLabelMap() {
        return fieldLabelMap;
    }

    public void setFieldLabelMap(Map<Integer, String> fieldLabelMap) {
        this.fieldLabelMap = fieldLabelMap;
    }

    public long getCoachId() {
        return coachId;
    }

    public void setCoachId(long coachId) {
        this.coachId = coachId;
    }

    public boolean getIsAccessAll() {
        return isAccessAll;
    }

    public void setIsAccessAll(boolean isAccessAll) {
        this.isAccessAll = isAccessAll;
    }

    public long getFacilityIdPref() {
        return facilityIdPref;
    }

    public void setFacilityIdPref(long facilityIdPref) {
        this.facilityIdPref = facilityIdPref;
    }
}
