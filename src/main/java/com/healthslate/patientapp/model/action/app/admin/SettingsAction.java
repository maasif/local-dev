package com.healthslate.patientapp.model.action.app.admin;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.healthslate.patientapp.util.PushNotificationManager;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.healthslate.patientapp.model.dao.BaseDAO;
import com.healthslate.patientapp.model.dao.DeviceDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.dao.TargetDAO;
import com.healthslate.patientapp.model.entity.Device;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.Reminder;
import com.healthslate.patientapp.model.entity.Target;
import com.healthslate.patientapp.model.entity.TargetGlucose;
import com.healthslate.patientapp.model.entity.TargetMeal;
import com.healthslate.patientapp.model.entity.TargetMedication;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.GCMUtils;
import com.opensymphony.xwork2.ActionSupport;

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/settings.tiles"),	
	@Result(name="ajax", type="json", params={"root", "jsonData"})
})

@SuppressWarnings("serial")
public class SettingsAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private String jsonData;
	private String mealTargets;
	private String glucoseTimes;
	private String reminderJson;
	private String medTimes;
	private String settingsLoadString;
	private String settingsSaveString;
	private String mealIsSigned;

	private Long patientId;
	private Map<String, Object> session;
	private HttpServletRequest request;
	
	private PatientDAO patientDAO = new PatientDAO();
	private TargetDAO targetDAO = new TargetDAO();
	
	public String description;
	private Target target;
	
	public Patient getPatientFromSession() {
		Patient patient = null;
		if(patientId != null && patientId != 0){
			patient = patientDAO.getPatientById(patientId);
		} else{
			patient = (Patient) session.get(AppConstants.SessionKeys.ACTIVE_PATIENT.name());
		}
		
		patient.setTarget(null);
		if(patient != null){			
			//session.put(AppConstants.SessionKeys.ACTIVE_PATIENT.name(), patient);
		}
		
		return patient;
	}
	
	@Action(value="settingsPage")
	public String showSettingsPage() {	
		settingsLoadString = getPatientSettings();
		description = "Displayed settings of patient id" + patientId;
		return ActionSupport.SUCCESS;
	}

	@Action(value="saveSettings")
	public String saveSettings() {	
		Patient patient = getPatientFromSession();
		BaseDAO baseDAO = new BaseDAO();

		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		
		if(target != null) {
			
			Target targetFromDB = targetDAO.getTargetByPatientId(patient.getPatientId());
			
			if(targetFromDB != null) {
				target.setTargetId(targetFromDB.getTargetId());				
				
				//delete all records first
				targetDAO.deleteOldRemindersByTargetId(targetFromDB.getTargetId());
				targetDAO.deleteOldMealTargetsByTargetId(targetFromDB.getTargetId());
				targetDAO.deleteOldGlucoseTargetsByTargetId(targetFromDB.getTargetId());
				targetDAO.deleteOldMedicationTargetsByTargetId(targetFromDB.getTargetId());				
			}
			
			//setting patient to target
			target.setPatient(patient);
			
			Reminder reminder = new Reminder();
			List<TargetMeal> meals = new ArrayList<TargetMeal>();
			List<TargetGlucose> glucoses = new ArrayList<TargetGlucose>();
			List<TargetMedication> medications = new ArrayList<TargetMedication>();
			
			try {
				//parse Reminders
				reminder = new Gson().fromJson(reminderJson, Reminder.class);
				reminder.setTarget(target);
				
				//parse mealTargets list
				meals = new Gson().fromJson(mealTargets, new TypeToken<List<TargetMeal>>(){}.getType());				
				for (TargetMeal targetMeal : meals) {
					targetMeal.setTarget(target);
				}
				
				//parse glucoses times list
				glucoses = new Gson().fromJson(glucoseTimes, new TypeToken<List<TargetGlucose>>(){}.getType());				
				for (TargetGlucose targetGlucose : glucoses) {
					targetGlucose.setTarget(target);
				}
				
				//parse medication times list
				medications = new Gson().fromJson(medTimes, new TypeToken<List<TargetMedication>>(){}.getType());				
				for (TargetMedication medication : medications) {
					medication.setTarget(target);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//set reminder in target
			target.setReminder(reminder);
			
			//set meal target list to target
			target.setMealTargets(meals);
			
			//set glucoses time list to target
			target.setGlucoseTargets(glucoses);
			
			//set medications time list to target
			target.setMedTargets(medications);
						
			User user = (User)session.get(AppConstants.SessionKeys.USER.name());
			String dietitianName = AppConstants.TARGETSETTINGS.DIETITIAN.getValue();
			if(user != null){
				dietitianName = user.getLastName() + ", " + user.getFirstName();
			}
			
			//when save from web save 'Dietitian' to source
			target.setSource(dietitianName);
			
			//save to db
			baseDAO.save(target);

			//save is signed value to patient table
			patient.setIsSigned( ( mealIsSigned.equalsIgnoreCase("Yes") ) ? true : false );
			baseDAO.save(patient);

			sendTargetViaGCM(target);
			
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
		}
		description = "Saving settings of patient id" + patientId;
		jsonData = new Gson().toJson(returnMap); 		
		return "ajax";
	}
		
	public void sendTargetViaGCM(Target target) {
		Patient patient = getPatientFromSession();		
		if(patient != null) {			
			String macAddress = patientDAO.getMacAddressByPatientId(patient.getPatientId());
			if(macAddress != null) {				
				Device device = new DeviceDAO().getDeviceByMac(macAddress);				
				if(device != null) {
                    List<Device> devices = new ArrayList<Device>();
                    devices.add(device);
                    String payLoad = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create().toJson(target);
                    String result = PushNotificationManager.pushMessage(devices, AppConstants.GCMKEYS.TARGET.getValue(), payLoad, request);

					/*List<String> androidTargets = new ArrayList<String>();
					androidTargets.add(device.getRegistrationId());
					String result = GCMUtils.sendGCM(androidTargets, AppConstants.GCMKEYS.TARGET.getValue(), new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create().toJson(target));
					if(result != null){
						String[] resultParts = result.split(",");
						String[] failMessageResultParts = resultParts[3].split("=");

						if(failMessageResultParts[0].equalsIgnoreCase("failure") && failMessageResultParts[1].equalsIgnoreCase("1")){
							//resultMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
						} else{
							//resultMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
						}
					}*/

					/*List<Device> devices = new ArrayList<Device>();
                    devices.add(device);
                    String payLoad = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create().toJson(target);
                    PushNotificationManager.pushMessage(devices, AppConstants.GCMKEYS.TARGET.getValue(), payLoad, request);*/
				}//else device
				
			}//else mac address
			
		}//else patient		
	}
	
	public String getPatientSettings() {
		
		Patient patient = getPatientFromSession();
		
		settingsLoadString = "";
		
		if(patient != null) {
			
			target = targetDAO.getTargetByPatientId(patient.getPatientId());
			if(target != null) {
				target.getReminder();
			}

			mealIsSigned = "No";
			if(patient.getIsSigned() != null && patient.getIsSigned()){
				mealIsSigned = "Yes";
			}

			if(target != null) {				
				settingsLoadString = StringEscapeUtils.escapeJava(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create().toJson(target));				
			}
		}
		
		return settingsLoadString;
	}
	
	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSettingsSaveString() {
		return settingsSaveString;
	}

	public void setSettingsSaveString(String settingsSaveString) {
		this.settingsSaveString = settingsSaveString;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public String getSettingsLoadString() {
		return settingsLoadString;
	}

	public void setSettingsLoadString(String settingsLoadString) {
		this.settingsLoadString = settingsLoadString;
	}

	public Target getTarget() {
		return target;
	}

	public void setTarget(Target target) {
		this.target = target;
	}

	public String getMealTargets() {
		return mealTargets;
	}

	public void setMealTargets(String mealTargets) {
		this.mealTargets = mealTargets;
	}

	public String getGlucoseTimes() {
		return glucoseTimes;
	}

	public void setGlucoseTimes(String glucoseTimes) {
		this.glucoseTimes = glucoseTimes;
	}

	/**
	 * @return the medTimes
	 */
	public String getMedTimes() {
		return medTimes;
	}

	/**
	 * @param medTimes the medTimes to set
	 */
	public void setMedTimes(String medTimes) {
		this.medTimes = medTimes;
	}

	public String getReminderJson() {
		return reminderJson;
	}

	public void setReminderJson(String reminderJson) {
		this.reminderJson = reminderJson;
	}


	public String getMealIsSigned() {
		return this.mealIsSigned;
	}

	public void setMealIsSigned(String mealIsSigned) {
		this.mealIsSigned = mealIsSigned;
	}
}
