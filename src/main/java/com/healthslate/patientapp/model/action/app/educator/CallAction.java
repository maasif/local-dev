package com.healthslate.patientapp.model.action.app.educator;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.healthslate.patientapp.model.dao.DeviceDAO;
import com.healthslate.patientapp.model.entity.Device;
import com.healthslate.patientapp.util.CommonUtils;
import com.healthslate.patientapp.util.HSSessionUtils;
import com.healthslate.patientapp.util.ObjectUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.dao.PreferencesDAO;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/caller.tiles")
})
public class CallAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private Map<String, Object> session;
	private HttpServletRequest request;

	private Long patientId;
	private String callAppId;
	private String providerAddress;
	public String description;
    public String baseUrl;
	private String phoneNumber;
	private String uuid;
    private String appVersion;
    private String memberDeviceType;

    Patient patient;

	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		setRequest(arg0);
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		session  = arg0;
	}
	
	@Action(value="caller")
	public String openCallingPage(){
		callAppId = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.SIGHT_CALL_APP_ID.getValue());
        baseUrl = ObjectUtils.getApplicationUrl(request);
		patient = new PatientDAO().getPatientById(patientId);
		if(patient != null){
            //the mobile app version the patient currently using
            appVersion = CommonUtils.getMobileAppVersionOfPatient(patientId);
            patientId = patient.getPatientId();
            uuid = patient.getUuid();
            Device device = new DeviceDAO().getDeviceByMac(patient.getDeviceMacAddress());
			if(device != null){
				memberDeviceType = device.getDeviceType();
			}

            User user = patient.getUser();
            if(user != null){
				phoneNumber = user.getPhone();
			}
		}else{
			description = "Error while opening calling page" + AppConstants.LOGGING.PATIENT_NOT_FOUND_IN_SESSION.getValue();
		}
		
		Provider provider = HSSessionUtils.getProviderSession(session);
		if(provider != null){
			User user = provider.getUser();			
			if(user != null){
				providerAddress = user.getEmail();
			}
		}else{
			description = "Error while opening calling page Provider not found for patient id " + patientId;
		}

		description = "Displayed caller page of patient with patient id" + patientId;
		return ActionSupport.SUCCESS;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public String getCallAppId() {
		return callAppId;
	}

	public void setCallAppId(String callAppId) {
		this.callAppId = callAppId;
	}

	public String getProviderAddress() {
		return providerAddress;
	}

	public void setProviderAddress(String providerAddress) {
		this.providerAddress = providerAddress;
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getMemberDeviceType() {
        return memberDeviceType;
    }

    public void setMemberDeviceType(String memberDeviceType) {
        this.memberDeviceType = memberDeviceType;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }
}
