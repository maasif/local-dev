package com.healthslate.patientapp.model.action.app.educator;

import com.amazonaws.services.opsworks.model.App;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.PatientDTO;
import com.healthslate.patientapp.model.entity.CoachSessionPreference;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.DateUtils;
import com.healthslate.patientapp.util.HSSessionUtils;
import com.healthslate.patientapp.util.JsonUtil;
import com.healthslate.patientapp.ws.SocialGroupServices;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/*
* ======= FILE CHANGE HISTORY =======
* [9/04/2015]: Created by __oz
* ===================================
 */

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/coachDashboard.tiles"),
	@Result(name="ajax", type="json", params={"root", "jsonData"})
})

@SuppressWarnings("serial")
public class CoachDashboardAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private String jsonData;	
	private Long patientId;
	private Map<String, Object> session;
	private HttpServletRequest request;
    public Patient patient;
    private String token;
    private String uuid;
	private Long providerId;
	private String sessionDays;
	private String sessionTimes;

    private PatientDAO patientDAO = new PatientDAO();
	private ProviderDAO providerDAO = new ProviderDAO();
	private BaseDAO baseDAO = new BaseDAO();

	public String description;

	@Action(value="coachesDashboard")
	public String showCoachDashboard() {
        return ActionSupport.SUCCESS;
	}

	//gets my members list
	@Action(value="getCoachDashboardMyMembers")
	public String getCoachDashboardMyMembers(){
		Map<String, Object> returnMap = new LinkedHashMap<String, Object>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		try {
			Provider loggedInCoach = HSSessionUtils.getProviderSession(session);
			String primaryOrLeadFilter = (loggedInCoach.getType().equalsIgnoreCase(AppConstants.CoachTypes.FOOD_COACH.getValue())) ? AppConstants.PRIMARY_FOOD_COACH : "";
			List<PatientDTO> myMembers = patientDAO.getMyMembers(loggedInCoach.getProviderId(), primaryOrLeadFilter);
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			returnMap.put(AppConstants.JsonConstants.DATA.name(), myMembers);
		} catch (Exception e) {
			e.printStackTrace();
		}

		jsonData = JsonUtil.toJsonExcludedNull(returnMap);

		return "ajax";
	}

	//gets inactive members list
	@Action(value="getCoachDashboardInactiveMembers")
	public String getCoachDashboardInactiveMembers(){
		Map<String, Object> returnMap = new LinkedHashMap<String, Object>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		try {
			Provider loggedInCoach = HSSessionUtils.getProviderSession(session);
			List<PatientDTO> myMembers = patientDAO.getInactiveMembers(loggedInCoach.getProviderId(), 14);
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			returnMap.put(AppConstants.JsonConstants.DATA.name(), myMembers);
		} catch (Exception e) {
			e.printStackTrace();
		}

		jsonData = JsonUtil.toJsonExcludedNull(returnMap);
		return "ajax";
	}

	//gets upcoming sessions list
	@Action(value="getCoachDashboardUpcomingSessions")
	public String getCoachDashboardUpcomingSessions(){
		Map<String, Object> returnMap = new LinkedHashMap<String, Object>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		try {
			Provider loggedInCoach = HSSessionUtils.getProviderSession(session);
			List<PatientDTO> myMembers = patientDAO.getUpcomingOneToOneSessions(loggedInCoach.getProviderId());
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			returnMap.put(AppConstants.JsonConstants.DATA.name(), myMembers);
		} catch (Exception e) {
			e.printStackTrace();
		}

		jsonData = JsonUtil.toJsonExcludedNull(returnMap);
		return "ajax";
	}

	//gets coach messages list
	@Action(value="getCoachDashboardCoachMessages")
	public String getCoachDashboardCoachMessages(){
		Map<String, Object> returnMap = new LinkedHashMap<String, Object>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		try {
			Provider loggedInCoach = HSSessionUtils.getProviderSession(session);
			List<PatientDTO> myMembers = patientDAO.getCoachMessages(loggedInCoach.getProviderId());
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			returnMap.put(AppConstants.JsonConstants.DATA.name(), myMembers);
		} catch (Exception e) {
			e.printStackTrace();
		}

		jsonData = JsonUtil.toJsonExcludedNull(returnMap);
		return "ajax";
	}

	//gets coach messages list
	@Action(value="getCoachDashboardMemberMessages")
	public String getCoachDashboardMemberMessages(){
		Map<String, Object> returnMap = new LinkedHashMap<String, Object>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		List<PatientDTO> myMembers;
		try {
			Provider loggedInCoach = HSSessionUtils.getProviderSession(session);
			if(loggedInCoach.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())){
				myMembers = new TechSupportMessageDAO().getAllMessages();
			} else {
				myMembers = patientDAO.getMemberMessages(loggedInCoach);
			}
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			returnMap.put(AppConstants.JsonConstants.DATA.name(), myMembers);
		} catch (Exception e) {
			e.printStackTrace();
		}

		jsonData = JsonUtil.toJsonExcludedNull(returnMap);
		return "ajax";
	}

	//gets coach messages list
	@Action(value="getCoachDashboardMemberLastContactOn")
	public String getCoachDashboardMemberLastContactOn(){
		Map<String, Object> returnMap = new LinkedHashMap<String, Object>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		List<PatientDTO> myMembers = new ArrayList<PatientDTO>();
		try {
			Provider loggedInCoach = HSSessionUtils.getProviderSession(session);
			if(loggedInCoach.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())){
				myMembers = new TechSupportMessageDAO().getPatientsLastContactOn(loggedInCoach);
			} else {
				myMembers = patientDAO.getPatientsLastContactOn(loggedInCoach);
			}
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			returnMap.put(AppConstants.JsonConstants.DATA.name(), myMembers);
		} catch (Exception e) {
			e.printStackTrace();
		}

		jsonData = JsonUtil.toJsonExcludedNull(returnMap);
		return "ajax";
	}

	@Action(value = "saveCoachSchedulingPreferences")
	public String saveCoachSchedulingPreferences() {
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		try {
			Provider provider = providerDAO.getProviderById(providerId);

			CoachSessionPreference coachSessionPreference = new CoachSessionPreferenceDAO().getSessionPreferenceByProviderId(provider.getProviderId());
			if (coachSessionPreference == null) {
				coachSessionPreference = new CoachSessionPreference();
			}

			coachSessionPreference.setDays(sessionDays);
			coachSessionPreference.setTimes(sessionTimes);

			provider.setCoachSessionPreference(coachSessionPreference);
			coachSessionPreference.setProvider(provider);

			baseDAO.save(coachSessionPreference);
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());

		} catch (Exception e) {
			e.printStackTrace();
		}

		jsonData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

	@Action(value="getCoachSchedulingPreferences")
	public String getCoachSchedulingPreferences(){
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		try {
			Provider provider = providerDAO.getProviderById(providerId);
			jsonData = JsonUtil.toJsonExcludedNull(provider.getCoachSessionPreference());

			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			returnMap.put(AppConstants.JsonConstants.DATA.name(), jsonData);

		} catch (Exception e) {
			e.printStackTrace();
		}

		jsonData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

	@Action(value="getCoachPreferenceDataSet")
	public String getCoachPreferenceDataSet(){
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		try {
			Provider provider = providerDAO.getProviderById(providerId);
			returnMap.put(AppConstants.CoachesArea.COACH_SESSION_PREFERENCE.name(), JsonUtil.toJsonExcludedNull(provider.getCoachSessionPreference()));
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());

		} catch (Exception e) {
			e.printStackTrace();
		}

		jsonData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	public String getSessionDays() {
		return sessionDays;
	}

	public void setSessionDays(String sessionDays) {
		this.sessionDays = sessionDays;
	}

	public String getSessionTimes() {
		return sessionTimes;
	}

	public void setSessionTimes(String sessionTimes) {
		this.sessionTimes = sessionTimes;
	}
}
