package com.healthslate.patientapp.model.action.app.educator;

import com.amazonaws.services.opsworks.model.App;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.MessageGcmDTO;
import com.healthslate.patientapp.model.dto.PatientDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import com.healthslate.patientapp.ws.LogServices;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/*
* ======= FILE CHANGE HISTORY =======
* [2/4/2015]: Created by __oz
* [2/23/2015]: Added uploadMessageImage action for image uploading, __oz
* ===================================
 */

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/coachPatientMessages.tiles"),
	@Result(name=AppConstants.ALL_MESSAGES, type="tiles", location="educator/coachPatientMessagesAll.tiles"),
	@Result(name="ajax", type="json", params={"root", "jsonData"})
})

@SuppressWarnings("serial")
public class CoachMessagesAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private String jsonData;	
	private Long patientId;
	private Long userId;
	private Map<String, Object> session;
	private HttpServletRequest request;
	private MessageGcmDTO message;

	private String messageString;
	private String coachMessagesList;

    private long latestMessageTime;

	private BaseDAO baseDAO = new BaseDAO();
	PatientDAO patientDAO = new PatientDAO();
	Patient patient;
	public String description;
	private String smsTemplateList;

	public static String COACH_MESSAGE_TEXT = "";

    private File uploadFile;
    private String fileName;

	private long messagesStartDate;
	private long messagesEndDate;

    private boolean shouldRead;
	private String videoLink;
	private boolean broadcastMsg;

	@Action(value="coachPatientMessages")
	public String showCoachPatientMessagesPage() {
        patient = getActivePatientFromSession();
        description = "Displayed Coach Patient Messages page for patient id "+patientId;
		//gets sms templates from DB for Twilio SMS dropdown
		smsTemplateList = StringEscapeUtils.escapeJava(JsonUtil.toJson(new SmsTemplateDAO().getAllSmsTemplates()));
		return ActionSupport.SUCCESS;
	}

	@Action(value="coachPatientMessagesAll")
	public String showCoachPatientAllMessagesPage() {
		patient = getActivePatientFromSession();
		//coachMessagesList = StringEscapeUtils.escapeJava(JsonUtil.toJsonExcludedNull(new MessageDAO().getAllMessagesOfAllCoaches(patientId)));
		description = "Displayed Coach Patient Messages page for patient id "+patientId;
		return AppConstants.ALL_MESSAGES;
	}

	@Action(value="sendMessageToDevice")
	public String sendMessageToDevice() {
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        LOG.info("In-App message action: sending message started");

		try {
            message = (MessageGcmDTO) JsonUtil.fromJson(messageString, MessageGcmDTO.class);
            COACH_MESSAGE_TEXT = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.COACH_MESSAGE_TEXT.getValue());

           if(message != null){

               LOG.info("In-App message action: sending message: " + message.toString());
               Provider provider = HSSessionUtils.getProviderSession(session);
			   Patient patient = patientDAO.getPatientById(patientId);

               if(patient != null){

                   User userPro = provider.getUser();

                   LOG.info("In-App message action: message sent from coach: " + userPro.getLastName() + ", " + userPro.getFirstName() + " to member uuid: " + patient.getUuid());

                   if(provider.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())){
					   message.setOwner(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue());
				   } else {
					   message.setOwner(AppConstants.SessionKeys.PROVIDER.name());
				   }
                   message.setTimeStamp(System.currentTimeMillis());
                   message.setIsRead(false);
                   message.setProviderId(provider.getProviderId());
                   message.setPatientId(patientId);

                   DeviceDAO deviceDao = new DeviceDAO();
                   Device device = deviceDao.getDeviceByMac(patient.getDeviceMacAddress());

                   if(device != null && !ObjectUtils.isEmpty(device.getDeviceMacAddress()) && !ObjectUtils.isEmpty(device.getRegistrationId())){

					   if(provider.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())){

						   Provider defaultTechSupport = new ProviderDAO().getDefaultTechSupport();

						   TechSupportMessage tsm = new TechSupportMessage();
						   tsm.setPatient(patient);
						   //For single thread message under Tech Support on Mobile device
						   tsm.setUser(defaultTechSupport.getUser());
						   tsm.setRepliedBy(userPro);
						   tsm.setMessage(message.getMessageText());
						   tsm.setOwner(message.getOwner());
						   tsm.setReadStatus(true);
						   tsm.setImagePath(message.getImagePath());
						   tsm.setTimestamp(message.getTimeStamp());
						   LOG.info("In-App message action: saved to DB: " + tsm.toString());

						   baseDAO.save(tsm);

						   //override the provider with default when sending message from techsupport
						   provider = defaultTechSupport;
					   } else {
						   //save message to DB
						   Message messageDB = new Message();
						   messageDB.setPatient(patient);
						   messageDB.setUser(new UserDAO().getUserById(userPro.getUserId()));
						   messageDB.setMessage(message.getMessageText());
						   messageDB.setOwner(message.getOwner());
						   messageDB.setReadStatus(true);
						   messageDB.setImagePath(message.getImagePath());
						   messageDB.setTimestamp(message.getTimeStamp());
						   messageDB.setIsError(false);
						   LOG.info("In-App message action: saved to DB: " + messageDB.toString());

						   baseDAO.save(messageDB);

						   if(broadcastMsg){
							   List<Patient> patients = patientDAO.getPatientsByLeadCoachIdAndPrimaryFoodCoachId(provider.getProviderId(), patient.getPatientId());
							   List<String> macAddresses = new ArrayList<String>();
							   List<Device> devices;
							   List<String> emails = new ArrayList<String>();

							   for(Patient p : patients){
								   Message broadMsg = new Message();
								   broadMsg.setUser(userPro);
								   broadMsg.setMessage(message.getMessageText());
								   broadMsg.setOwner(message.getOwner());
								   broadMsg.setReadStatus(true);
								   broadMsg.setImagePath(message.getImagePath());
								   broadMsg.setTimestamp(message.getTimeStamp());
								   broadMsg.setIsError(false);
								   broadMsg.setPatient(p);

								   if(!ObjectUtils.isEmpty(p.getDeviceMacAddress())){
									   macAddresses.add(p.getDeviceMacAddress());
								   }

								   emails.add(p.getUser().getEmail());
								   baseDAO.save(broadMsg);
							   }

							   //posting messages on each member me-feed
							   LogServices.buildMessagePost(provider, emails, message.getMessageText());

							   if(macAddresses != null && macAddresses.size() > 0){
								   LOG.info("In-App message action: mac addresses list: " + macAddresses);
								   devices = new DeviceDAO().getDevicesByPatientDeviceMacAddresses(macAddresses);
								   LOG.info("In-App message action: devices list: " + devices);
								   String result = PushNotificationManager.pushMessage(devices, AppConstants.GCMKEYS.MESSAGE.getValue(), JsonUtil.toJson(message), request);
								   LOG.info("In-App message action: GCM sent results: " + result);
							   }else{
								   returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
								   returnMap.put(AppConstants.JsonConstants.REASON.name(), "No devices found for member(s)");
							   }
						   }
					   }

					   LOG.info("In-App message action: sending message to device: macAddress: " + device.getDeviceMacAddress() + ", registrationId: " + device.getRegistrationId());

					   List<Device> devices = new ArrayList<Device>();
					   devices.add(device);
					   String result = PushNotificationManager.pushMessage(devices, AppConstants.GCMKEYS.MESSAGE.getValue(), JsonUtil.toJson(message), request);

					   LOG.info("In-App message action: GCM sent results: " + result);

                       //check here if last message sent time is over 2 minutes then send SMS
                       long[] times = DateUtils.getElapsedTimes(latestMessageTime, message.getTimeStamp());
                       if(times[2] >= 2l){
                           String phoneNumber = patient.getUser().getPhone();
                           COACH_MESSAGE_TEXT = COACH_MESSAGE_TEXT.replace("$$", provider.getProviderId()+"");
                           if(device.getDeviceType().equalsIgnoreCase(AppConstants.DeviceTypes.IOS.getValue())){
                               COACH_MESSAGE_TEXT = COACH_MESSAGE_TEXT.replace(AppConstants.HTTP_KEY, AppConstants.IOS_SMS_KEY);
                           }
                           LOG.info("In-App message action: sending SMS to: " + phoneNumber + ", messageText: " + COACH_MESSAGE_TEXT);
                           TwilioMessageUtils.sendSMS(phoneNumber, COACH_MESSAGE_TEXT);
                       }else{
                           LOG.info("In-App message action: no SMS sent because last SMS was sent less than 2 minutes ago");
                       }

                       LOG.info("In-App message action: sending message to elgg: coach : " + userPro.getLastName() + ", " + userPro.getFirstName() +
                               ", patient: " + patient.getPatientId() +
                               ", messageText: " + message.getMessageText());

					   if(ObjectUtils.isEmpty(videoLink)){
						   LogServices.buildMessagePost(provider, patient, message.getMessageText());
					   }else{
						   LogServices.buildMessagePost(provider, patient, message.getMessageText(), videoLink);
					   }

                       returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());

                   }else{
                       LOG.info("In-App message action: GCM sent results: Member device not registered, he/she hasn't logged in member app yet");
                       returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
                       returnMap.put(AppConstants.JsonConstants.REASON.name(), "Member device not registered, he/she hasn't logged in member app yet.");
                   }
               }else{
                    LOG.info("In-App message action: GCM sent results: Unable to get member");
                    returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
                    returnMap.put(AppConstants.JsonConstants.REASON.name(), "Unable to get member.");
               }
           } else {
               LOG.info("In-App message action: GCM sent results: Message can't be empty");
               returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
               returnMap.put(AppConstants.JsonConstants.REASON.name(), "Message can't be empty.");
           }
		} catch (Exception e) {
			e.printStackTrace();
			description = "Sending message(s) failed due to server error.";
			returnMap.put(AppConstants.JsonConstants.REASON.name(), description);
			returnMap.put(AppConstants.JsonConstants.CUSTOM_DATA.name(), e.getMessage());
		}

		jsonData = JsonUtil.toJson(returnMap);
        LOG.info("In-App message action: returned data : " + jsonData);
		return "ajax";
	}
		
	@Action(value="getAllCoachPatientMessages")
	public String getAllCoachPatientMessages() {
		Provider provider = HSSessionUtils.getProviderSession(session);

		if(userId != null && userId > 0){
			provider = new ProviderDAO().getProviderByUser(userId);
		}

        boolean setRead = (userId == null) ? true : false;

        if(!shouldRead && userId == null){
            setRead = false;
        }
		if(provider.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())){
			jsonData = JsonUtil.toJsonExcludedNull(new TechSupportMessageDAO().getAllMessagesDTO(patientId, setRead));
		} else {
			jsonData = JsonUtil.toJsonExcludedNull(new MessageDAO().getAllMessages(provider, patientId, setRead));
		}

		description = "Display all Coach Patient messages";
		return "ajax";
	}

	@Action(value="getAllCoachesPatientMessages")
	public String getAllCoachesPatientMessages() {

		jsonData = JsonUtil.toJsonExcludedNull(new MessageDAO().getAllMessagesOfAllCoachesByDateRange(patientId, messagesStartDate, messagesEndDate));

		description = "Display all Coach Patient messages";
		return "ajax";
	}

    public Patient getActivePatientFromSession() {
        return patientDAO.getPatientById(patientId);
    }

    //[2/23/2015]: Added uploadMessageImage action for image uploading, __oz
    @Action(value="uploadImageMessage")
    public String uploadMessageImage(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try{
            if(uploadFile != null){
                String logImagesFolderPath = CommonUtils.getProjectPath() + AppConstants.LOG_IMAGE_FOLDER;
                String fileName = ObjectUtils.getImageName();
                File destinationFile = new File(logImagesFolderPath, fileName);
                FileUtils.copyFile(uploadFile, destinationFile);

                LOG.info("In-App message action: image uploaded: " + destinationFile.getName());

                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
                returnMap.put(AppConstants.JsonConstants.DATA.name(), AppConstants.LOG_IMAGE_FOLDER + fileName);
            }
        }catch(IOException e){
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

	@Action(value="getAllProvidersByPatientId")
	public String getAllProvidersByPatientId() {
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		Provider provider = HSSessionUtils.getProviderSession(session);
		try{
			List<User> users = new MessageDAO().getAllProvidersByPatientId(patientId);
			if(provider.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())){
				users.add(provider.getUser());
			}

			String usersData = JsonUtil.toJsonExcludedNull(users);
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			returnMap.put(AppConstants.JsonConstants.DATA.name(), usersData);
		}catch (Exception e){
			e.printStackTrace();
		}
		description = "Display all providers by Patient";
		jsonData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public String getMessageString() {
		return messageString;
	}

	public void setMessageString(String messageString) {
		this.messageString = messageString;
	}

	public String getCoachMessagesList() {
		return coachMessagesList;
	}

	public void setCoachMessagesList(String coachMessagesList) {
		this.coachMessagesList = coachMessagesList;
	}

    public long getLatestMessageTime() {
        return latestMessageTime;
    }

    public void setLatestMessageTime(long latestMessageTime) {
        this.latestMessageTime = latestMessageTime;
    }

    public File getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(File uploadFile) {
        this.uploadFile = uploadFile;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String getSmsTemplateList() {
		return smsTemplateList;
	}

	public void setSmsTemplateList(String smsTemplateList) {
		this.smsTemplateList = smsTemplateList;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public long getMessagesStartDate() {
		return messagesStartDate;
	}

	public void setMessagesStartDate(long messagesStartDate) {
		this.messagesStartDate = messagesStartDate;
	}

	public long getMessagesEndDate() {
		return messagesEndDate;
	}

	public void setMessagesEndDate(long messagesEndDate) {
		this.messagesEndDate = messagesEndDate;
	}

    public boolean isShouldRead() {
        return shouldRead;
    }

    public void setShouldRead(boolean shouldRead) {
        this.shouldRead = shouldRead;
    }

	public boolean getIsBroadcastMsg() {
		return broadcastMsg;
	}

	public void setBroadcastMsg(boolean broadcastMsg) {
		this.broadcastMsg = broadcastMsg;
	}
}
