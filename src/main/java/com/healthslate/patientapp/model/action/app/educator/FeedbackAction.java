package com.healthslate.patientapp.model.action.app.educator;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.healthslate.patientapp.model.dao.FeedbackDAO;
import com.healthslate.patientapp.model.dto.FeedbackLogsDTO;
import com.healthslate.patientapp.model.entity.JQueryDataTableParamModel;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.util.DataTablesParamUtility;
import com.healthslate.patientapp.util.HSSessionUtils;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/*
* ======= FILE CHANGE HISTORY =======
* [2/17/2015]: Created by __ad
* ===================================
 */

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/feedback.tiles"),
    @Result(name="ajax", type="stream", params={"contentType", "application/Json", "inputName", "is"})
})

@SuppressWarnings("serial")
public class FeedbackAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private String jsonData;
	private Map<String, Object> session;
	private HttpServletRequest request;
    private InputStream is;
    private String jsonString;
    private JsonObject jsonResponse;
    public List<FeedbackLogsDTO> aaData;
    public List<FeedbackLogsDTO> feedbackLogs;

    public String description;

    private String sEcho;
    private Long iTotalRecords = null;
    private Integer iTotalDisplayRecords = null;
    private Map<Integer, String> fieldLabelMap;
    private FeedbackDAO feedbackDAO = new FeedbackDAO();

	@Action(value="Feedback")
	public String showFeedBackPage() {
        description = "Displayed Feedback logs page";
		return ActionSupport.SUCCESS;
	}

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Action(value="fetchFeedbackLogs")
    public String fetchFeedbackLogs() throws IOException {

        aaData = new ArrayList<FeedbackLogsDTO>();

        Provider provider = HSSessionUtils.getProviderSession(session);

        JQueryDataTableParamModel param = DataTablesParamUtility.getParam(request);
        sEcho = param.sEcho;
        Long iTotalRecordsInt; // total number of records (un-filtered)
        int iTotalDisplayRecordsInt;
        populateFieldLabelsMap();
        String sortIndex = fieldLabelMap.get(param.iSortColumnIndex);

        int maxCount = feedbackDAO.countByCoach(provider);
        feedbackLogs = feedbackDAO.listByCoach(param.iDisplayStart, param.iDisplayLength, param.sSearch, sortIndex, param.sSortDirection, provider);
        iTotalRecordsInt = Long.parseLong(maxCount + "");

        sEcho = param.sEcho;
        aaData = feedbackLogs;
        iTotalDisplayRecordsInt = aaData.size();
        iTotalRecords = iTotalRecordsInt;
        iTotalDisplayRecords = iTotalDisplayRecordsInt;

        //iTotalRecordsInt =
        JsonObject jsonResponse = new JsonObject();
        jsonResponse.addProperty("sEcho", sEcho);
        jsonResponse.addProperty("iTotalRecords", iTotalRecords);
        jsonResponse.addProperty("iTotalDisplayRecords", iTotalRecords);

        Gson gson = new Gson();
        jsonResponse.add("aaData", gson.toJsonTree(aaData));
        is = new ByteArrayInputStream(jsonResponse.toString().getBytes());

        description = "Fetched Feedback";

        return "ajax";
    }

    private void populateFieldLabelsMap() {
        fieldLabelMap = new LinkedHashMap<Integer, String>();
        fieldLabelMap.put(0, "f.patient_id");
        fieldLabelMap.put(1, "u.first_name");
        fieldLabelMap.put(2, "f.description");
        fieldLabelMap.put(3, "f.timestamp");
        fieldLabelMap.put(4, "f.type");
    }

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

    public InputStream getIs() {
        return is;
    }

    public void setIs(InputStream is) {
        this.is = is;
    }

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }

    public JsonObject getJsonResponse() {
        return jsonResponse;
    }

    public void setJsonResponse(JsonObject jsonResponse) {
        this.jsonResponse = jsonResponse;
    }

	public List<FeedbackLogsDTO> getAaData() {
		return aaData;
	}

	public void setAaData(List<FeedbackLogsDTO> aaData) {
		this.aaData = aaData;
	}

	public List<FeedbackLogsDTO> getFeedbackLogs() {
		return feedbackLogs;
	}

	public void setFeedbackLogs(List<FeedbackLogsDTO> feedbackLogs) {
		this.feedbackLogs = feedbackLogs;
	}

	public Map<Integer, String> getFieldLabelMap() {
		return fieldLabelMap;
	}

	public void setFieldLabelMap(Map<Integer, String> fieldLabelMap) {
		this.fieldLabelMap = fieldLabelMap;
	}

	public FeedbackDAO getFeedbackDAO() {
		return feedbackDAO;
	}

	public void setFeedbackDAO(FeedbackDAO feedbackDAO) {
		this.feedbackDAO = feedbackDAO;
	}
}
