package com.healthslate.patientapp.model.action.app.educator;

import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.LogDTO;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.util.*;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Results({
    @Result(name = AppConstants.GLUCOSE_LOG_BOOK, type = "tiles", location = "educator/glucoseLogbook.tiles"),
    @Result(name=AppConstants.AJAX, type="json", params={"root", "jsonData"}),
})
public class GlucoseLogAction extends ActionSupport implements SessionAware, ServletRequestAware {

    private static final long serialVersionUID = 1L;
    private Map<String, Object> session;
    private HttpServletRequest request;
    private String weeklyGlucoseReportDateString;
    private String weeklyLogsDateString;
    private long patientId = 0l;
    private String targetString;
    private String jsonData;

    Patient patient;

    PatientDAO patientDAO = new PatientDAO();
    LogDAO logDAO = new LogDAO();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String description;

    public void storeActivePatientInSession() {
        session.put(AppConstants.SessionKeys.ACTIVE_PATIENT.name(), null);
        if (patientId != 0l) {
            patient = patientDAO.getPatientById(patientId);
        }
    }

    @Action(value = "fetchGlucoseLogbook")
    public String fetchGlucoseLogbook() {
        storeActivePatientInSession();
        weeklyGlucoseReportDateString = DateUtils.getLastWeekDate(new Date()) + " - " + new SimpleDateFormat("MM/dd/yyyy").format(new Date());
        targetString = StringEscapeUtils.escapeJava(JsonUtil.toJsonExcludedNull(new TargetDAO().getTargetByPatientId(patientId)));
        return AppConstants.GLUCOSE_LOG_BOOK;
    }

    @Action(value = "fetchWeeklyGlucoseLogsData")
    public String fetchWeeklyGlucoseLogsData() throws ParseException {
        Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
        if (weeklyLogsDateString != null && !weeklyLogsDateString.isEmpty()) {
            String[] dateParts = weeklyLogsDateString.split("-");
            List<LogDTO> objGlucoseLogs = logDAO.getGlucoseLogbookDataByPatientId(patientId, dateParts[0], dateParts[1]);
            dataMap.put(AppConstants.LOGS.Glucose_LOGS.getValue(), objGlucoseLogs);
            jsonData = JsonUtil.toJson(dataMap);
        }
        return AppConstants.AJAX;
    }

    @Override
    public void setServletRequest(HttpServletRequest arg0) {
        request = arg0;
    }

    @Override
    public void setSession(Map<String, Object> arg0) {
        session = arg0;
    }

    public String getGlucoseWeeklyReportDateString() {
        return weeklyGlucoseReportDateString;
    }

    public void setGlucoseWeeklyReportDateString(String weeklyGlucoseReportDateString) {
        this.weeklyGlucoseReportDateString = weeklyGlucoseReportDateString;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getTargetString() {
        return targetString;
    }

    public void setTargetString(String targetString) {
        this.targetString = targetString;
    }

    public String getWeeklyLogsDateString() {
        return weeklyLogsDateString;
    }

    public void setWeeklyLogsDateString(String weeklyLogsDateString) {
        this.weeklyLogsDateString = weeklyLogsDateString;
    }

    public String getWeeklyGlucoseReportDateString() {
        return weeklyGlucoseReportDateString;
    }

    public void setWeeklyGlucoseReportDateString(String weeklyGlucoseReportDateString) {
        this.weeklyGlucoseReportDateString = weeklyGlucoseReportDateString;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }
}