package com.healthslate.patientapp.model.action.app.educator;

import com.amazonaws.util.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.*;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.DataTablesParamUtility;
import com.healthslate.patientapp.util.JsonUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.json.JSONUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;


@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/goals.tiles"),
	@Result(name="create_goal", type="tiles", location="educator/createGoal.tiles"),
	@Result(name="create_habit", type="tiles", location="educator/createHabit.tiles"),	
	@Result(name="goal_summary", type="tiles", location="educator/goalSummary.tiles"),
	@Result(name="goals_habits_listing", type="tiles", location="educator/goalsHabitsListing.tiles"),
	@Result(name="goal_progress", type="tiles", location="educator/goalProgress.tiles"),	
	@Result(name="goals_habits_dashboard", type="tiles", location="educator/goalsAndHabitsDashboard.tiles"),
	@Result(name="ajax", type="json", params={"root", "jsonData"}),
	@Result(name="ajax_table", type="stream", params={"contentType", "application/Json", "inputName", "is"}),
})

@SuppressWarnings("serial")
public class GoalActions extends ActionSupport implements SessionAware, ServletRequestAware{

	private String jsonString;
	private Long filledFormId;
	private String jsonData;	
	private String habitsJsonData;
	private String actionPlanString;
	private String formType;
	private String goalStatus = AppConstants.StatusConstants.NOTACHIEVED.getValue();
	private Long patientId;
	private int goalCategory = -1; //default first that is "All"
	
	private GoalCategoryDAO goalCategoryDAO = new GoalCategoryDAO();
	private FilledFormDAO filledFormDAO = new FilledFormDAO();
	private FormDataDAO formDataDAO = new FormDataDAO();
	private PatientDAO patientDAO = new PatientDAO();
	private GoalAssessmentRatingDAO assessmentRatingDAO = new GoalAssessmentRatingDAO();
	
	private Map<String, List<String>> dataMap;	
	private Map<String, Object> session;
	private HttpServletRequest request;
	private List<GoalDetailsDTO> goalDetails;
	private List<HabitDetailsDTO> habitDetails;
	
	public String description;
	private String goalHabitsData;
	private InputStream is;
	
	private String sEcho;
	private Long iTotalRecords = null;
	private Integer iTotalDisplayRecords = null;
	
	public List<GoalDashboardDTO> aaData;
	private Map<Integer, String> feildLableMap;
	List<GoalDashboardDTO> goalDashboardDTOList;
    Patient patient;
    private Map<Integer, String> fieldLabelMap;
    private String goalRatingsHistory;

	@Override
	public void setSession(Map<String, Object> arg0) { 
		session = arg0;
	}

	@Override
	public void setServletRequest(HttpServletRequest arg0) {	
		request = arg0;
	}
	
	//shows goals/habits old listing page for server
	@Action(value="goals")
	public String showGoalsListing() {
		patient = getPatientFromSession();
		formType = AppConstants.GOAL;
		setSwitchState(formType);
		
		if(patient != null){						
			jsonData = getGoals(patient, goalCategory);
			habitsJsonData = getHabits(patient);
			description = "Displayed goals and habits list for patient id "+patient.getPatientId();
		} else {
			description = "Diplaying goals failed because "+AppConstants.LOGGING.PATIENT_NOT_FOUND_IN_SESSION.getValue();
		}
		
		jsonString = getCategories();		
		return ActionSupport.SUCCESS;
	}
	
	//shows goals/habits old listing page for server
	@Action(value="goalsHabits")
	public String goalsHabits() {
		Patient patient = getPatientFromSession();
		
		formType = (String)session.get(AppConstants.SessionKeys.MODULE_TYPE.name());
		setSwitchState(formType);
		
		if(patient != null){						
			jsonData = getGoals(patient, goalCategory);
			habitsJsonData = getHabits(patient);
			description = "Displayed goals and habits list for patient id "+patient.getPatientId();
		} else {
			description = "Displaying goals/habits failed because "+AppConstants.LOGGING.PATIENT_NOT_FOUND_IN_SESSION.getValue();
		}
		
		jsonString = getCategories();					
		return ActionSupport.SUCCESS;
	}
	
	public void setSwitchState(String formType) {
		//store in session on which module user right now to save the state of it,
		//and when the page refreshes switch to that module, __oz		
		if(ObjectUtils.isEmpty(formType)) {
			formType = AppConstants.GOAL;
		}
		session.put(AppConstants.SessionKeys.MODULE_TYPE.name(), formType.toLowerCase());		
	}
	
	//gets goals by category via ajax
	@Action(value="goalsByCategory")
	public String getGoalsByCategory() {
		Patient patient = getPatientFromSession();		
		if(patient != null){						
			jsonData = getGoalsWithoutEscape(patient, goalCategory);
			description = "Displayed goals list for patient id "+patient.getPatientId()+" and category Id "+goalCategory;
		} else {
			description = "Displaying goals by category failed because "+AppConstants.LOGGING.PATIENT_NOT_FOUND_IN_SESSION.getValue();
		}
		return "ajax";
	}
	
	//gets achieved goals via AJAX
	@Action(value="getAchievedGoals")
	public String getAchievedGoals() {
		Patient patient = getPatientFromSession();		
		if(patient != null){	
			goalDetails = filledFormDAO.getGoalsByPatientId(patient.getPatientId(), -1, AppConstants.StatusConstants.ACHIEVED.getValue());
			if(goalDetails.size() > 0){
				jsonData = new Gson().toJson(goalDetails);
				description = "Displayed achievable goals for patient id "+patient.getPatientId();
			} else {
				description = "Displaying achievable goals failed because "+AppConstants.LOGGING.PATIENT_NOT_FOUND_IN_SESSION.getValue();
			}
		}				
		return "ajax";
	}
	
	public String getGoalsWithoutEscape(Patient patient, int catId){
		goalDetails = filledFormDAO.getGoalsByPatientId(patient.getPatientId(), catId, "");
		if(goalDetails.size() > 0){
			jsonData = new Gson().toJson(goalDetails);
		}
		
		return jsonData;
	}

	public String getGoals(Patient patient, int catId){
		goalDetails = filledFormDAO.getGoalsByPatientId(patient.getPatientId(), catId, "");
		if(goalDetails.size() > 0){
			jsonData = StringEscapeUtils.escapeJava(new Gson().toJson(goalDetails));
		}
		
		return jsonData;
	}
	
	public String getCategories(){
		
		List<GoalCategory> goalCategories = goalCategoryDAO.getAllCategories();
		
		if(goalCategories != null && goalCategories.size() > 0){
			jsonString = StringEscapeUtils.escapeJava(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create().toJson(goalCategories));
		}
		
		return jsonString;
	}
	
	public String getHabits(Patient patient) {
		habitDetails = filledFormDAO.getHabitsByPatientId(patient.getPatientId());
		if(habitDetails != null && habitDetails.size() > 0){
			habitsJsonData = StringEscapeUtils.escapeJava(new Gson().toJson(habitDetails));
		}
		
		return habitsJsonData;		
	}
	
	public Patient getPatientFromSession() {
		if(patientId != null && patientId != 0){
			patient = patientDAO.getPatientById(patientId);
		} else{
			patient = (Patient) session.get(AppConstants.SessionKeys.ACTIVE_PATIENT.name());
		}
		
		if(patient != null){			
			//session.put(AppConstants.SessionKeys.ACTIVE_PATIENT.name(), patient);
		}
		
		return patient;
	}
	
	public String getAllGoalsAndHabits(){
		
		if(patientId == null){
			Patient patient = getPatientFromSession();			
			if(patient != null){
				patientId = patient.getPatientId();
			}
		}
		
		List<GoalSummaryDTO> goalSummaryList = filledFormDAO.getGoalsByPatientId(patientId, -1);
		List<HabitDetailsDTO> habits = filledFormDAO.getHabitsByPatientId(patientId);
		
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		
		if(goalSummaryList != null && goalSummaryList.size() > 0){
			
			for (GoalSummaryDTO goalSummary : goalSummaryList) {
							
				//gets formData list from DB
				List<FormData> dataList = formDataDAO.getFormWithFilledFormId(goalSummary.getFilledFormId());
									
				for (FormData formData : dataList) {
					String fieldName = formData.getFieldName();					
					if(fieldName.equalsIgnoreCase(AppConstants.ACTION_PLAN)){
						goalSummary.setActionPlan(new ActionPlanDAO().getActionPlanNameById(Long.parseLong(formData.getFieldValue())));
					}else if(fieldName.equalsIgnoreCase(AppConstants.GOAL_OPTION)){
						goalSummary.setGoalName(new GoalDAO().getGoalNameNyId(Long.parseLong(formData.getFieldValue())));						
					}else if(fieldName.equalsIgnoreCase(AppConstants.GOAL_CATEGORY)){
						goalSummary.setCategoryName(new GoalCategoryDAO().getGoalCategoryNameById(Long.parseLong(formData.getFieldValue())));						
					}
				}
				
				goalSummary.setFormDataList(dataList);
			}
			
			returnMap.put(AppConstants.JsonConstants.GOALS.name(), new Gson().toJson(goalSummaryList));
		}
		
		if(habits != null && habits.size() > 0){
			returnMap.put(AppConstants.JsonConstants.HABITS.name(), new Gson().toJson(habits));
		}
		
		return new Gson().toJson(returnMap);
	}
	
	//shows goals/habits new listing page, for mobile
	@Action(value="goalsHabitsListing")
	public String showGoalsHabitsListing() {
		String goalsAndhabits = getAllGoalsAndHabits();
		jsonString = StringEscapeUtils.escapeJava(goalsAndhabits);
		description = "Displayed all goals and habits list";
		return "goals_habits_listing";
	}
	
	public Map<String, String> deleteHabitOrGoal(String formType){
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		
		if(patientId == null){
			Patient patient = getPatientFromSession();			
			if(patient != null){
				patientId = patient.getPatientId();
			}
		}
		
		boolean isChildDeleted = formDataDAO.deleteFormDataByFilledFormId(filledFormId);
		if(isChildDeleted){
			
			//in case of GOAL also delete its Rating
			if(formType.equalsIgnoreCase(AppConstants.GOAL)){
				new GoalAssessmentRatingDAO().deleteRatingByFilledFormId(filledFormId);				
			}
						
			boolean isParentDeleted = filledFormDAO.deleteFilledFormByType(filledFormId, formType);			
			if(isParentDeleted){
				//String goalsAndhabits = getAllGoalsAndHabits();				
				//String updatedGoalsHabits = StringEscapeUtils.escapeJava(goalsAndhabits);				
				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
				//returnMap.put(AppConstants.JsonConstants.DATA.name(), updatedGoalsHabits);
			}
		}
		
		return returnMap;
	}
	
	@Action(value="goalProgress")
	public String goalProgress(){
		if(patientId == null){
			Patient patient = getPatientFromSession();			
			if(patient != null){
				patientId = patient.getPatientId();
			}
		}
		
		List<GoalSummaryDTO> goalSummaryList = filledFormDAO.getGoalsByPatientId(patientId, filledFormId);
		if(goalSummaryList != null && goalSummaryList.size() > 0){
			
			GoalSummaryDTO goalSummary = goalSummaryList.get(0);
			
			//gets formData list from DB
			List<FormData> dataList = formDataDAO.getFormWithFilledFormId(goalSummary.getFilledFormId());
			
			List<ActionPlan> actionPlans = null;
			
			for (FormData formData : dataList) {
				if(formData.getFieldName().equalsIgnoreCase(AppConstants.GOAL_OPTION)){					
					actionPlans = new ActionPlanDAO().getActionPlansByGoalId(Long.parseLong(formData.getFieldValue()));
					goalSummary.setGoalName(new GoalDAO().getGoalNameNyId(Long.parseLong(formData.getFieldValue())));
				}else if(formData.getFieldName().equalsIgnoreCase(AppConstants.GOAL_CATEGORY)){
					goalSummary.setCategoryName(new GoalCategoryDAO().getGoalCategoryNameById(Long.parseLong(formData.getFieldValue())));
				}
			}
			
			goalSummary.setFormDataList(dataList);
			
			jsonString = new Gson().toJson(goalSummaryList);
			actionPlanString = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(actionPlans);
		}		

		description = "Displayed goals progress for patient id "+patientId;
		//rating, goalOption, createdDate, all action plans , myChallenge, overcoming, reward, shouldRemind
		return "goal_progress";
	}
	
	
	//delete HABIT
	@Action(value="deleteHabit")
	public String deleteHabit() {
		Map<String, String> returnMap = deleteHabitOrGoal(AppConstants.HABIT);		
		jsonData = new Gson().toJson(returnMap);		
		description = "Deleted habit for patient Id "+patientId;
		return "ajax";
	}
	
	//delete GOAL
	@Action(value="deleteGoal")
	public String deleteGoal() {
		Map<String, String> returnMap = deleteHabitOrGoal(AppConstants.GOAL);		
		jsonData = new Gson().toJson(returnMap);
		description = "Deleted habit for patient Id "+patientId;
		return "ajax";
	}
			
	//shows create/edit goal form
	@Action(value="showGoalForm")
	public String showGoalForm() {
		setSwitchState(AppConstants.GOAL);
		List<FormData> formDataList = formDataDAO.getFormWithFilledFormId(filledFormId);
		
		FilledForm filledForm = new FilledForm();
		filledForm.setFilledFormId(filledFormId);
		
		jsonString = getCategories();
		
		jsonData = fetchFormData(formDataList, filledForm);		
		description = "Displayed goal form for form id "+filledFormId+ " and patient Id "+patientId;
		return "create_goal";
	}
		
	//shows single goal summary form
	@Action(value="goalSummary")
	public String goalSummary() {
		setSwitchState(AppConstants.GOAL);
		
		List<FormData> formDataList = formDataDAO.getFormWithFilledFormId(filledFormId);
		
		FilledForm filledForm = new FilledForm();
		filledForm.setFilledFormId(filledFormId);
		
		jsonData = fetchFormData(formDataList, filledForm);	
		goalStatus = getGoalStatusFromServer();
		description = "Displayed single goal summary form for form id "+filledFormId+ " and patient Id "+patientId;
		return "goal_summary";
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String fetchFormData(List<FormData> formDataList, FilledForm filledForm) {

		List jsonList = new ArrayList<BasicNameValuePair>();
		
		for (FormData fd: formDataList){
			jsonList.add(new BasicNameValuePair(fd.getFieldName(), fd.getFieldValue()));
		}
		
		jsonList.add(new BasicNameValuePair("filledFormId", ""+filledForm.getFilledFormId()));
		
		// Creating Json String from Form Data List
		Gson gson = new Gson();
		String jsonString = gson.toJson(jsonList);
		
		//Escaping Json String to be used in JavaScript
		return  StringEscapeUtils.escapeJava(jsonString);
	}
	
	//opens goals create/edit page
	@Action("createGoal")
	public String getGoals() {
		setSwitchState(AppConstants.GOAL);
		jsonString = getCategories();
		jsonData = "null";		
		description = "Displayed Goal create/edit page for patient Id "+patientId;
		return "create_goal";
	}

	//opens habits create/edit page
	@Action("createHabit")
	public String createHabit() {
		setSwitchState(AppConstants.HABIT);
		if(filledFormId != null){
			List<FormData> formDataList = formDataDAO.getFormWithFilledFormId(filledFormId);		
			FilledForm filledForm = new FilledForm();
			filledForm.setFilledFormId(filledFormId);		
			jsonData = fetchFormData(formDataList, filledForm);
			description = "Displayed Habit create/edit page for form id" +filledFormId+" and patient Id "+patientId;
		} else {
			description = "Displayed Habit create/edit page failed because form not found in db for form id" +filledFormId+" and patient Id "+patientId;
		}
		return "create_habit";
	}
	
	//save goals/habits formdata via ajax
	@Action(value="saveForm")
	public String saveForm() {
		setSwitchState(formType);
		
		FilledForm filledForm = new FilledForm();
		List<FormData> formDataList = new ArrayList<FormData>();
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		session.put(AppConstants.SessionKeys.MODULE_TYPE.name(), formType.toLowerCase());
		if(filledFormId == null){
			
			if(session.containsKey(AppConstants.SessionKeys.ACTIVE_PATIENT)){
				Patient patient = (Patient) session.get(AppConstants.SessionKeys.ACTIVE_PATIENT.name());
				
				filledForm.setPatient(patient);
				filledForm.setType(formType);
				
				Date date = new Date();
				filledForm.setCreatedDate(date);
				filledForm.setLastModifiedDate(date);
				boolean shouldRemind = getReminderStatusFromFormData(filledForm);
				filledForm.setShouldRemind(shouldRemind);
				filledFormDAO.saveForm(filledForm);
				
				//save form data
				saveFormData(filledForm);
				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
				description = "Saved goal/habits form data with form Id "+filledFormId+" and patient id "+patientId;
			}
		} else{
			//update modified date
			filledForm = filledFormDAO.getFormById(filledFormId);
			if(filledForm != null) {			
				Date date = new Date();
				
				//in case of goal, when save form called from Goal Progress GUI, save rating and reminder explicitly to
				//respective tables
				if(formType.equalsIgnoreCase(AppConstants.GOAL)){
					boolean shouldRemind = getReminderStatusFromFormData(filledForm);
					filledForm.setShouldRemind(shouldRemind);					
					int goalRating = getGoalRatingFromFormData(filledForm);
					long formId = filledForm.getFilledFormId();
					
					GoalAssessmentRating goalAssessmentRating = assessmentRatingDAO.getRatingByFormById(formId);	
					
					//new record
					if(goalAssessmentRating == null) {	
						goalAssessmentRating = new GoalAssessmentRating();
						goalAssessmentRating.setFilledForm(new FilledForm(formId));				
					}
					
					goalAssessmentRating.setRatedOn(new Date());
					goalAssessmentRating.setRating(goalRating);
					assessmentRatingDAO.saveRating(goalAssessmentRating);					
				}
				
				filledForm.setLastModifiedDate(date);
				filledForm.setFormDataList(formDataList);
				filledFormDAO.saveForm(filledForm);
				
				//delete all previous form data
				formDataList = formDataDAO.getFormWithFilledFormId(filledFormId);
				formDataDAO.deleteFormData(formDataList);
				
				//save new form data
				saveFormData(filledForm);
				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());	
				description = "Updated goal/habits form data with form Id "+filledFormId+" and patient id "+patientId;
			}else {
				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
				returnMap.put(AppConstants.JsonConstants.REASON.name(), "Something went wrong. Please contact your Administrator.");
				description = "Updating goal/habits failed because filled form not found in db for form id " +filledFormId+" and patient id "+patientId;
			}
		}
		jsonData = new Gson().toJson(returnMap); 
		return "ajax";
	}
	
	//sets filledForm to FormData and save all values
	public void saveFormData(FilledForm filledForm){
		try {
			List<FormData> formDataList = jsonToListFormData(filledForm);
			// saving formData in DB
			formDataDAO.saveformDataList(formDataList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//converts from json to List FormData
	@SuppressWarnings("rawtypes")
	public List<FormData> jsonToListFormData(FilledForm filledForm){
		
		List<FormData> formDataList = new ArrayList<FormData>();
		
		try {
			
			JSONObject jsonObject = new JSONObject(goalHabitsData);
			
			for(Iterator iterator = jsonObject.keys(); iterator.hasNext();) {
			    String key = (String) iterator.next();			    
			    if(!key.equalsIgnoreCase("filledFormId") || !key.equalsIgnoreCase(AppConstants.REMINDER)){				
					FormData formData = new FormData();
					formData.setFieldName(key);					
					formData.setFieldValue(jsonObject.get(key).toString());
					formData.setFilledForm(filledForm);
					formDataList.add(formData); 
				}			    
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return formDataList;
	}
	
	//gets the reminder status from FormData List
	public boolean getReminderStatusFromFormData(FilledForm filledForm){
		boolean shouldRemind = false;
		try {
			
			List<FormData> formDataList = jsonToListFormData(filledForm);
			for (FormData formData : formDataList) {
				if(formData.getFieldName().equalsIgnoreCase(AppConstants.REMINDER)){
					if(formData.getFieldValue().equalsIgnoreCase(AppConstants.REMINDER)){
						shouldRemind = true;
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return shouldRemind;
	}
	
	//gets goal Rating from FormData List
	public int getGoalRatingFromFormData(FilledForm filledForm){
		int goalRating = -1;
		try {
			
			List<FormData> formDataList = jsonToListFormData(filledForm);
			for (FormData formData : formDataList) {
				if(formData.getFieldName().equalsIgnoreCase(AppConstants.GOAL_RATING)){
					goalRating = Integer.parseInt(formData.getFieldValue());
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return goalRating;
	}
		
	//save goals/habits switch state
	@Action("saveSwitchState")
	public void saveSwitchState() {		
		setSwitchState(formType);	
		description = "Switching state of goals/habits to "+formType+" and patiend id "+patientId;
	}
	
	//saves goals status via ajax
	@Action("saveGoalStatus")
	public String saveGoalStatus() {		
		
		FilledForm filledForm = filledFormDAO.getFormById(filledFormId);
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		
		if(filledForm != null) {			
			filledForm.setDateAchieved(new Date());
			if(goalStatus.equalsIgnoreCase(AppConstants.StatusConstants.NOTACHIEVED.getValue())) {
				filledForm.setDateAchieved(null);
			}			
			filledFormDAO.saveForm(filledForm);
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());	
			description = "Saved goal status for form id "+filledForm.getFilledFormId()+" and patint Id "+patientId;
		} else {
			description = "Saving goal status failed for patient id "+patientId+" because filled form not found in db for form id "+filledFormId;
		}
		jsonData = new Gson().toJson(returnMap); 
		return "ajax";
	}
	
	@Action("goalsHabitsDashboard")
	public String showGoalsAndHabitsDashboard() {	
		getPatientFromSession();
		description = "Displayed goals habits on dashboard for patient id "+patientId;
        goalRatingsHistory = JsonUtil.toJson(filledFormDAO.getGoalRatingsByPatient(patientId));
		return "goals_habits_dashboard";
	}
	
	@Action(value="fetchGoalsDashboardData")
	public String fetchGoalsDashboardData() throws IOException {
		if(patientId == null){
			Patient patient = getPatientFromSession();			
			if(patient != null){
				patientId = patient.getPatientId();
			}
		}
		
		aaData = new ArrayList<GoalDashboardDTO>();
		
		JQueryDataTableParamModel param = DataTablesParamUtility.getParam(request);
		sEcho = param.sEcho;
		Long iTotalRecordsInt; // total number of records (un-filtered)
		int iTotalDisplayRecordsInt;
        populateFieldLabelsMap();
        String sortIndex = fieldLabelMap.get(param.iSortColumnIndex);

		goalDashboardDTOList = filledFormDAO.getGoalsForDashboard(patientId, param.iDisplayStart, param.iDisplayLength, param.sSearch, sortIndex, param.sSortDirection);
		
		iTotalRecordsInt = 0l;
		if(goalDashboardDTOList != null && goalDashboardDTOList.size() > 0){
			iTotalRecordsInt = (long) goalDashboardDTOList.size();
		}
		
		sEcho = param.sEcho;
		aaData = goalDashboardDTOList;
		iTotalDisplayRecordsInt = aaData.size();
		iTotalRecords = iTotalRecordsInt;
		setiTotalDisplayRecords(iTotalDisplayRecordsInt);

		//iTotalRecordsInt = 
		JsonObject jsonResponse = new JsonObject();	
		jsonResponse.addProperty("sEcho", sEcho);
		jsonResponse.addProperty("iTotalRecords", iTotalRecords);
		jsonResponse.addProperty("iTotalDisplayRecords", iTotalRecords);	
		
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		
		jsonResponse.add("aaData", gson.toJsonTree(aaData));
		setIs(new ByteArrayInputStream(jsonResponse.toString().getBytes()));
		description = "Displayed goals dashboard data for patient id "+patientId;
		return "ajax_table"; 
	}

    private void populateFieldLabelsMap() {
        fieldLabelMap = new LinkedHashMap<Integer, String>();
        fieldLabelMap.put(4, "ff.created_date");
        fieldLabelMap.put(6, "gar.rated_on");
    }

	public String getGoalStatusFromServer() {
		FilledForm filledForm = filledFormDAO.getFormById(filledFormId);
		String status = AppConstants.StatusConstants.NOTACHIEVED.getValue();
		if(filledForm != null) {						
			if(filledForm.getDateAchieved() != null) {
				status = AppConstants.StatusConstants.ACHIEVED.getValue();
			}
		}
		
		return status;
	}
	
	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public Map<String, List<String>> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, List<String>> dataMap) {
		this.dataMap = dataMap;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	public Long getFilledFormId() {
		return filledFormId;
	}

	public void setFilledFormId(Long filledFormId) {
		this.filledFormId = filledFormId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getGoalCategory() {
		return goalCategory;
	}

	public void setGoalCategory(int goalCategory) {
		this.goalCategory = goalCategory;
	}

	public List<GoalDetailsDTO> getGoalDetails() {
		return goalDetails;
	}

	public void setGoalDetails(List<GoalDetailsDTO> goalDetails) {
		this.goalDetails = goalDetails;
	}

	public List<HabitDetailsDTO> getHabitDetails() {
		return habitDetails;
	}

	public void setHabitDetails(List<HabitDetailsDTO> habitDetails) {
		this.habitDetails = habitDetails;
	}

	public String getHabitsJsonData() {
		return habitsJsonData;
	}

	public void setHabitsJsonData(String habitsJsonData) {
		this.habitsJsonData = habitsJsonData;
	}

	public String getGoalHabitsData() {
		return goalHabitsData;
	}

	public void setGoalHabitsData(String goalHabitsData) {
		this.goalHabitsData = goalHabitsData;
	}

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}

	public String getGoalStatus() {
		return goalStatus;
	}

	public void setGoalStatus(String goalStatus) {
		this.goalStatus = goalStatus;
	}

	public String getActionPlanString() {
		return actionPlanString;
	}

	public void setActionPlanString(String actionPlanString) {
		this.actionPlanString = actionPlanString;
	}

	public InputStream getIs() {
		return is;
	}

	public void setIs(InputStream is) {
		this.is = is;
	}

	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Map<Integer, String> getFeildLableMap() {
        return feildLableMap;
    }

    public void setFeildLableMap(Map<Integer, String> feildLableMap) {
        this.feildLableMap = feildLableMap;
    }

    public String getGoalRatingsHistory() {
        return goalRatingsHistory;
    }

    public void setGoalRatingsHistory(String goalRatingsHistory) {
        this.goalRatingsHistory = goalRatingsHistory;
    }
}
