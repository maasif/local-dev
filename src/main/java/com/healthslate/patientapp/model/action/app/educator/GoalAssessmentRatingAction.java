package com.healthslate.patientapp.model.action.app.educator;

import com.google.gson.Gson;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/goals.tiles"),
	@Result(name="create_goal", type="tiles", location="educator/createGoal.tiles"),
	@Result(name="ajax", type="json", params={"root", "ratingJsonData"})
})
public class GoalAssessmentRatingAction extends ActionSupport implements SessionAware, ServletRequestAware{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long filledFormId;
	private String ratingJsonData;
	private Long patientId;
	
	private Long formId;
	private int rating;
	private String remindString;
	
	private String notes;
	private String status;
	
	private GoalAssessmentRatingDAO assessmentRatingDAO = new GoalAssessmentRatingDAO();
	private PatientDAO patientDAO = new PatientDAO();
	private FilledFormDAO filledFormDAO = new FilledFormDAO();
	private CoachGoalAssessmentDAO coachGoalAssessmentDAO = new CoachGoalAssessmentDAO();
	private String record;
	private Map<String, Object> session;
	private HttpServletRequest request;
	
	public String description;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		setRequest(arg0);
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		session  = arg0;
	}
	
	@Action(value="saveRating")
	public String saveRating() {
		
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
	
		GoalAssessmentRating goalAssessmentRating = assessmentRatingDAO.getRatingByFormById(formId);	
		
		//new record
		if(goalAssessmentRating == null) {	
			goalAssessmentRating = new GoalAssessmentRating();
			goalAssessmentRating.setFilledForm(new FilledForm(formId));
			description = "Save "; 
		} else {
			description = "Update ";
		}
		
		goalAssessmentRating.setRatedOn(new Date());
		goalAssessmentRating.setRating(rating);
		Long id = assessmentRatingDAO.saveRating(goalAssessmentRating);
		
		if(id > 0) {
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());	
		}
		
		ratingJsonData = JsonUtil.toJson(returnMap);
		description += "rating for goal assessment for patient id "+ patientId + returnMap.get(AppConstants.JsonConstants.STATUS.name());
		return "ajax";
	}

	@Action(value="saveReminder")
	public String saveReminder() {
		
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
	
		FilledForm filledForm = filledFormDAO.getFormById(formId);
		long id = 0;
		if(filledForm != null){
			boolean shouldRemind = false;			
			if(remindString.equalsIgnoreCase(AppConstants.REMINDER)){
				shouldRemind = true;
			}
			filledForm.setShouldRemind(shouldRemind);
			
			id = filledFormDAO.saveForm(filledForm);
		}
		
		if(id > 0) {
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());	
		}
		
		ratingJsonData = JsonUtil.toJson(returnMap);
		description = "Save reminder for patient id "+ patientId + returnMap.get(AppConstants.JsonConstants.STATUS.name());
		return "ajax";
	}
	
	@Action(value="saveCoachAssessment")
	public String saveCoachAssessment() {
		
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
	
		FilledForm filledForm = filledFormDAO.getFormById(formId);
		Provider provider = HSSessionUtils.getProviderSession(session);
		long id = 0;
		if(filledForm != null){
			CoachGoalAssessment coachGoalAssessment = new CoachGoalAssessment(status, filledForm, System.currentTimeMillis(), notes, provider.getUser());
			id = coachGoalAssessmentDAO.saveCoachAssessment(coachGoalAssessment);
		}
		try {
			if(id > 0) {
                Map<Long, String> allGoals = CommonUtils.listToMap(new GoalDAO().getAllGoals());
                List<FormData> formDataList = new FormDataDAO().getFormWithFilledFormId(formId);
                String goalName = "";
                for (FormData formData : formDataList) {
                    String fieldName = formData.getFieldName();
                    String fieldValue = formData.getFieldValue();
                    if (fieldName.equalsIgnoreCase(AppConstants.GOAL_OPTION)) {
                        goalName = allGoals.get(Long.parseLong(fieldValue));
                    }
                }

				Patient patient = new PatientDAO().getPatientById(patientId);
				String coachNotesString = "<b>Goal</b>: " + goalName + "<br/><b>Goal Achieved</b>: "+ status+"<br/>"+notes;
				CoachNotes coachNotes = new CoachNotes(coachNotesString, provider, patient);
				coachNotes.setNotesDate(System.currentTimeMillis());
				new BaseDAO().save(coachNotes);
				List<Provider> coachesList = CommonUtils.getCoachesToNotify(provider, patient);
				NotifyUtils.notifyCoachesOfNewNoteAndAssessment(request, patientId, coachesList, true);
				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		ratingJsonData = JsonUtil.toJson(returnMap);
		description = "Save Coach Assessment  for patient id "+ patientId + returnMap.get(AppConstants.JsonConstants.STATUS.name());
		
		return "ajax";
	}
	
	
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public Long getFormId() {
		return formId;
	}

	public int getRating() {
		return rating;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public Long getFilledFormId() {
		return filledFormId;
	}

	public void setFilledFormId(Long filledFormId) {
		this.filledFormId = filledFormId;
	}

	public String getRatingJsonData() {
		return ratingJsonData;
	}

	public void setRatingJsonData(String ratingJsonData) {
		this.ratingJsonData = ratingJsonData;
	}

	public String getRemindString() {
		return remindString;
	}

	public void setRemindString(String remindString) {
		this.remindString = remindString;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
