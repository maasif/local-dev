package com.healthslate.patientapp.model.action.app.educator;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.DykDTO;
import com.healthslate.patientapp.model.dto.ShareableMealDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.json.JSONUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/*
* ======= FILE CHANGE HISTORY =======
* [9/04/2015]: Created by __oz
* ===================================
 */

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/DYK.tiles"),
	@Result(name=AppConstants.AJAX_DYK_SEND, type="json", params={"root", "adviceSentStatus"}),
	@Result(name="ajaxTable", type="stream", params={"contentType", "application/Json", "inputName", "is"}),
	@Result(name="ajax", type="json", params={"root", "jsonData"})
})

@SuppressWarnings("serial")
public class GroupPostAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private String jsonData;
	private Map<String, Object> session;
	private HttpServletRequest request;
	public String description;
	private List<DykDTO> dykDTOs = new ArrayList<DykDTO>();
	private Long adviceIdToSend;
	private int adviceSentStatus;
	private Map<Integer, String> groupMap = new LinkedHashMap<Integer, String>();
	private String videoLink;
	private String videoDescription;
	private String videoThumbnail;
	private String videoEmbedUrl;
	private String logId;

	BaseDAO baseDAO = new BaseDAO();
	DykDAO dykDAO = new DykDAO();
	GroupPostsDAO groupPostsDAO = new GroupPostsDAO();
	List<DYKAdvice> groupPostsList;
	private String memberPermission;
	private String token;


	public List<GroupPosts> aaData;
	List<GroupPosts> groupPostDTO;
	private String sEcho;
	private InputStream is;
	private Long iTotalRecords = null;
	private Integer iTotalDisplayRecords = null;
	private Map<Integer, String> fieldLabelMap;

	private String hyperlink;
	private String hyperlinkDescription;

	@Action(value="groupPosts")
	public String groupPosts() {
		groupPostsList = dykDAO.groupPosts();
		token = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.WISTIA_API_TOKEN.getValue());
		description = "View Advices";
		return ActionSupport.SUCCESS;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Action(value="fetchGroupPosts")
	public String fetchGroupPosts()throws IOException {

		Provider provider = (Provider) session.get(AppConstants.SessionKeys.PROVIDER.name());

		aaData = new ArrayList();

		JQueryDataTableParamModel param = DataTablesParamUtility.getParam(request);
		sEcho = param.sEcho;
		Long iTotalRecordsInt; // total number of records (un-filtered)
		int iTotalDisplayRecordsInt;

		populateGroupPostsFieldMap();
		String sortIndex= fieldLabelMap.get(param.iSortColumnIndex);

		groupPostDTO = groupPostsDAO.getGroupPostsList(provider.getProviderId(), param.sSearch, param.iDisplayStart, param.iDisplayLength, sortIndex, param.sSortDirection);

		sEcho = param.sEcho;
		aaData = groupPostDTO;
		iTotalDisplayRecordsInt = aaData.size();
		iTotalRecordsInt = groupPostsDAO.getGroupPostsListCount(provider.getProviderId(), param.sSearch);
		iTotalRecords = iTotalRecordsInt;
		iTotalDisplayRecords = iTotalDisplayRecordsInt;

		//iTotalRecordsInt =
		JsonObject jsonResponse = new JsonObject();
		jsonResponse.addProperty("sEcho", sEcho);
		jsonResponse.addProperty("iTotalRecords", iTotalRecords);
		jsonResponse.addProperty("iTotalDisplayRecords", iTotalRecords);

		Gson gson = new Gson();
		jsonResponse.add("aaData", gson.toJsonTree(aaData));
		is = new ByteArrayInputStream(jsonResponse.toString().getBytes());

		return "ajaxTable";
	}

	private void populateGroupPostsFieldMap(){
		fieldLabelMap = new LinkedHashMap<Integer, String>();
		fieldLabelMap.put(0, "g.description");
		fieldLabelMap.put(1, "g.post_type");
		fieldLabelMap.put(2, "g.video_link");
		fieldLabelMap.put(3, "coachName");
		fieldLabelMap.put(4, "g.post_date");
	}

	@Action(value="sendGroupPost")
	public String sendGroupPost() {

		String response = null;
		adviceSentStatus = -1;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		User sessionUser = (User) session.get(AppConstants.SessionKeys.USER.name());
		String userEmail = sessionUser.getEmail();
		DYKAdvice groupPost = dykDAO.getAdviceById(adviceIdToSend);

		//if post button is clicked from table for re-post it
		if(!ObjectUtils.isEmpty(token)){
			GroupPosts gPost = groupPostsDAO.getGroupPostByLinkId(Long.parseLong(token));
			if(gPost != null){
				groupPost = new DYKAdvice();
				groupPost.setAdviceContent(gPost.getDescription());
				gPost.setPostDate(System.currentTimeMillis());
				gPost.setProvider(HSSessionUtils.getProviderSession(session));
				baseDAO.save(gPost);
			}
		}else{
			GroupPosts groupPosts = new GroupPosts(groupPost.getAdviceContent(), HSSessionUtils.getProviderSession(session), System.currentTimeMillis(), AppConstants.NEWSFEED.DYK.getValue(), "");
			groupPosts.setLinkId(adviceIdToSend);
			baseDAO.saveNew(groupPosts);
		}

		nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.USERNAME.getValue(), userEmail));
		nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.POST_TYPE.getValue(), AppConstants.NEWSFEED.DYK.getValue()));
		nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.PUBLIC.getValue()));
		nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.DESCRIPTION.getValue(), groupPost.getAdviceContent()));

		String method = AppConstants.SOCIALKEYS.UPLOADPOST.getValue();
		String url = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.ELGG_WEB_SERVICE_PATH.getValue()) + method;

		try {
			response = NetworkUtils.postRequest(url, nameValuePairs);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			LOG.info("sendGroupPost: URL: " + url + " from USER: " + userEmail, ", elgg response: " + response);
			JSONObject jsonObject = new JSONObject(response);
			int statusFromElgg = jsonObject.getInt(AppConstants.SOCIALKEYS.STATUS.getValue());
			LOG.info("sendGroupPost: parsed elgg response: " + statusFromElgg);
			adviceSentStatus = statusFromElgg;
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("sendGroupPost: Exception: " + e.getMessage());
		}

		return AppConstants.AJAX_DYK_SEND;
	}

	@Action(value="sendGroupPostVideo")
	public String sendGroupPostVideo() {
		String response = null;
		adviceSentStatus = -1;
		User sessionUser = (User) session.get(AppConstants.SessionKeys.USER.name());
		String userEmail = sessionUser.getEmail();
		GroupPosts groupPost = null;

		//if post button is clicked from table for re-post it
		if(!ObjectUtils.isEmpty(token)){
			groupPost = groupPostsDAO.getGroupPostById(Long.parseLong(token));
			if(groupPost != null){
				videoEmbedUrl = groupPost.getVideoEmbedUrl();
				videoThumbnail = groupPost.getVideoThumbnail();
				videoDescription = groupPost.getDescription();

				groupPost.setProvider(HSSessionUtils.getProviderSession(session));
				groupPost.setPostDate(System.currentTimeMillis());
				baseDAO.save(groupPost);
			}
		}else{
			groupPost = new GroupPosts(videoDescription, HSSessionUtils.getProviderSession(session), System.currentTimeMillis(), AppConstants.NEWSFEED.VIDEOURL.getValue(), videoLink);
			groupPost.setVideoThumbnail(videoThumbnail);
			groupPost.setVideoEmbedUrl(videoEmbedUrl);
			baseDAO.saveNew(groupPost);
		}

		Map<String, String> dataMap = new LinkedHashMap<String, String>();
		dataMap.put(AppConstants.NEWSFEED.USERNAME.getValue(), userEmail);
		dataMap.put(AppConstants.NEWSFEED.POST_TYPE.getValue(), AppConstants.NEWSFEED.VIDEOURL.getValue());
		dataMap.put(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.PUBLIC.getValue());
		dataMap.put(AppConstants.NEWSFEED.FILE_URL.getValue(), videoEmbedUrl);
		dataMap.put(AppConstants.NEWSFEED.FILE_ICON.getValue(), videoThumbnail);
		dataMap.put(AppConstants.NEWSFEED._FILE_ICON.getValue(), videoThumbnail);
		dataMap.put(AppConstants.NEWSFEED.TAGS.getValue(), videoThumbnail);
		dataMap.put(AppConstants.NEWSFEED.DESCRIPTION.getValue(), videoDescription);

		LOG.info("sendGroupPostVideo: sending data: " + JsonUtil.toJson(dataMap));

		try {
			response = SocialServicesUtils.postSocialData(JsonUtil.toJson(dataMap), AppConstants.SOCIALKEYS.UPLOADPOST.getValue());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			LOG.info("sendGroupPostVideo: from USER: " + userEmail, ", elgg response: " + response);
			JSONObject jsonObject = new JSONObject(response);
			int statusFromElgg = jsonObject.getInt(AppConstants.SOCIALKEYS.STATUS.getValue());
			LOG.info("sendGroupPostVideo: parsed elgg response: " + statusFromElgg);
			adviceSentStatus = statusFromElgg;

			//broadcasting to all members in the system that a video is shared by coach
			PushNotificationManager.broadcast(sessionUser, AppConstants.GCMKEYS.SHARED_VIDEO.getValue(), groupPost.getGroupPostId()+"", request);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("sendGroupPostVideo: Exception: " + e.getMessage());
		}

		return AppConstants.AJAX_DYK_SEND;
	}

	@Action(value="sendGroupPostHyperlink")
	public String sendGroupPostHyperlink() {
		String response = null;
		adviceSentStatus = -1;
		User sessionUser = (User) session.get(AppConstants.SessionKeys.USER.name());
		String userEmail = sessionUser.getEmail();
		GroupPosts groupPost = null;

		//if post button is clicked from table for re-post it
		if(!ObjectUtils.isEmpty(token)){
			groupPost = groupPostsDAO.getGroupPostById(Long.parseLong(token));
			if(groupPost != null){
				hyperlink = groupPost.getVideoLink();
				hyperlinkDescription = groupPost.getDescription();

				groupPost.setProvider(HSSessionUtils.getProviderSession(session));
				groupPost.setPostDate(System.currentTimeMillis());
				baseDAO.save(groupPost);
			}
		}else{
			groupPost = new GroupPosts(hyperlinkDescription, HSSessionUtils.getProviderSession(session), System.currentTimeMillis(), AppConstants.NEWSFEED.HYPERLINK.getValue(), hyperlink);
			baseDAO.saveNew(groupPost);
		}

		Map<String, String> dataMap = new LinkedHashMap<String, String>();
		dataMap.put(AppConstants.NEWSFEED.USERNAME.getValue(), userEmail);
		dataMap.put(AppConstants.NEWSFEED.POST_TYPE.getValue(), AppConstants.NEWSFEED.HYPERLINK.getValue());
		dataMap.put(AppConstants.NEWSFEED.FILE_URL.getValue(), hyperlink);
		dataMap.put(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.PUBLIC.getValue());
		dataMap.put(AppConstants.NEWSFEED.DESCRIPTION.getValue(), hyperlinkDescription);

		LOG.info("sendGroupPostHyperlink: sending data: " + JsonUtil.toJson(dataMap));

		try {
			response = SocialServicesUtils.postSocialData(JsonUtil.toJson(dataMap), AppConstants.SOCIALKEYS.UPLOADPOST.getValue());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			LOG.info("sendGroupPostHyperlink: from USER: " + userEmail, ", elgg response: " + response);
			JSONObject jsonObject = new JSONObject(response);
			int statusFromElgg = jsonObject.getInt(AppConstants.SOCIALKEYS.STATUS.getValue());
			LOG.info("sendGroupPostHyperlink: parsed elgg response: " + statusFromElgg);
			adviceSentStatus = statusFromElgg;

		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("sendGroupPostHyperlink: Exception: " + e.getMessage());
		}

		return AppConstants.AJAX_DYK_SEND;
	}

	@Action(value="askMemberPermission")
	public String askMemberPermission() throws ParseException {
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

		try {
			BaseDAO baseDAO = new BaseDAO();
			Log log = new LogDAO().getLogById(logId);
			Patient patient = log.getPatient();
			ShareMealPermissions smp = new ShareMealPermissionsDAO().getShareMealPermissionsByLogId(log.getLogId());

			if(smp == null){
				smp = new ShareMealPermissions();
				smp.setProviderId(HSSessionUtils.getProviderSession(session).getProviderId());
				smp.setPermissionsAskedDate(System.currentTimeMillis());
				smp.setLogId(log.getLogId());
				baseDAO.save(smp);
				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			}else{
				smp.setPermissionsAskedDate(System.currentTimeMillis());

				smp.setProviderId(HSSessionUtils.getProviderSession(session).getProviderId());
				int requestSentCount = ObjectUtils.nullSafeInteger(smp.getRequestSentCount());
				requestSentCount++;
				smp.setRequestSentCount(requestSentCount);

				baseDAO.save(smp);
				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
				returnMap.put(AppConstants.JsonConstants.REASON.name(), "REQUEST_SENT_ALREADY");
			}

			//send push notification to device
			sendMemberPermissionRequest(patient, log);
		} catch (Exception e) {
			e.printStackTrace();
		}

		description = "Asking member permission for meal share";
		jsonData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

	@Action(value="generateNewsFeedPost")
	public String generateNewsFeedPost() throws ParseException {
		String response = null;
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		Map<String, String> dataMap = new LinkedHashMap<String, String>();
		Map<String, String> descriptionDataMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

		try {
			Provider provider = HSSessionUtils.getProviderSession(session);

			LogDAO logDAO = new LogDAO();
			Log log = logDAO.getLogById(logId);
			ShareMealPermissionsDAO shareMealPermissionsDAO = new ShareMealPermissionsDAO();
			ShareMealPermissions smp = shareMealPermissionsDAO.getShareMealPermissionsByLogId(log.getLogId());

			String method = AppConstants.SOCIALKEYS.POST_BY_CUSTOM_ID.getValue();
			String url = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.ELGG_WEB_SERVICE_PATH.getValue()) + method +
					"&" + AppConstants.NEWSFEED.CUSTOM_ID.getValue() + "=" + log.getLogId() +
					"&" + AppConstants.NEWSFEED.USERNAME.getValue() + "=" + log.getPatient().getUser().getEmail();

			response = NetworkUtils.getRequest(url);

			LOG.info("generateNewsFeedPost: response getPostById: " + response);
			if (smp.getPermissions().equalsIgnoreCase(AppConstants.ShareMealConstants.SHARE.name())) {
				JSONObject jsonObject = new JSONObject(response);
				JSONObject jsonObjectResult = jsonObject.getJSONObject("result");
				JSONObject ownerObject = jsonObjectResult.getJSONObject("owner");

				descriptionDataMap.put(AppConstants.NEWSFEED.OWNER_NAME.getValue(), ownerObject.getString(AppConstants.NEWSFEED.OWNER_NAME.getValue()));
				descriptionDataMap.put(AppConstants.NEWSFEED.OWNER_AVATAR_URL.getValue(), ownerObject.getString(AppConstants.NEWSFEED.OWNER_AVATAR_URL.getValue()));
				descriptionDataMap.put(AppConstants.NEWSFEED.OWNER_USERNAME.getValue(), ownerObject.getString(AppConstants.NEWSFEED.OWNER_USERNAME.getValue()));

				dataMap.put(AppConstants.NEWSFEED.USERNAME.getValue(), provider.getUser().getEmail());
				dataMap.put(AppConstants.NEWSFEED.TITLE.getValue(), log.getFoodLogSummary().getType());
				dataMap.put(AppConstants.NEWSFEED.POST_TYPE.getValue(), AppConstants.NEWSFEED.COACH_MEAL.getValue());
				dataMap.put(AppConstants.NEWSFEED.LOG_TIME.getValue(), log.getCreatedOn().toString());
				dataMap.put(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.PUBLIC.getValue());
				dataMap.put(AppConstants.NEWSFEED.ACCESS_ID.getValue(), jsonObjectResult.getInt(AppConstants.NEWSFEED.ACCESS_ID.getValue())+"");
				dataMap.put(AppConstants.NEWSFEED.HAS_MISSING_FOOD.getValue(), jsonObjectResult.getBoolean(AppConstants.NEWSFEED.HAS_MISSING_FOOD.getValue())+"");
				dataMap.put(AppConstants.NEWSFEED.HAS_SHARED_EVER.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED.HAS_SHARED_EVER.getValue()));
				dataMap.put(AppConstants.NEWSFEED.CUSTOM_ID.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED.CUSTOM_ID.getValue()));
				dataMap.put(AppConstants.NEWSFEED.MEAL_NAME.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED.MEAL_NAME.getValue()));
				dataMap.put(AppConstants.NEWSFEED.MIME_TYPE.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED.MIME_TYPE.getValue()));

				try {
					dataMap.put(AppConstants.NEWSFEED._FILE_ICON.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED._FILE_ICON.getValue()));
					dataMap.put(AppConstants.NEWSFEED.FILE_ICON.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED._FILE_ICON.getValue()));
					dataMap.put(AppConstants.NEWSFEED.TAGS.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED._FILE_ICON.getValue()));

					Map<String, String> otherData = new LinkedHashMap<String, String>();
					otherData.put(AppConstants.NEWSFEED.NOTES.getValue(), ObjectUtils.nullSafe(token));
					dataMap.put(AppConstants.NEWSFEED.OTHER_DATA.getValue(), JsonUtil.toJson(otherData));

					if(jsonObjectResult.getString(AppConstants.NEWSFEED.TAGS.getValue()) != null){
						dataMap.put(AppConstants.NEWSFEED.TAGS.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED.TAGS.getValue()));
					}
				} catch (JSONException e) {

				}

				dataMap.put(AppConstants.NEWSFEED._FILE_ICON.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED._FILE_ICON.getValue()));
				dataMap.put(AppConstants.NEWSFEED.CARBS.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED.CARBS.getValue()));
				dataMap.put(AppConstants.NEWSFEED.GUESS_CARBS.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED.GUESS_CARBS.getValue()));
				dataMap.put(AppConstants.NEWSFEED.DESCRIPTION.getValue(), JsonUtil.toJson(descriptionDataMap));

				response = SocialServicesUtils.postSocialData(JsonUtil.toJson(dataMap), AppConstants.SOCIALKEYS.UPLOADPOST.getValue());
				LOG.info("generateNewsFeedPost: SHARE meal response from elgg: " + response);
				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());

			} else if (smp.getPermissions().equalsIgnoreCase(AppConstants.ShareMealConstants.SHARE_ANONYMOUSLY.name())) {

				JSONObject jsonObject = new JSONObject(response);
				JSONObject jsonObjectResult = jsonObject.getJSONObject("result");

				dataMap.put(AppConstants.NEWSFEED.USERNAME.getValue(), provider.getUser().getEmail());
				dataMap.put(AppConstants.NEWSFEED.TITLE.getValue(), log.getFoodLogSummary().getType());
				dataMap.put(AppConstants.NEWSFEED.POST_TYPE.getValue(), AppConstants.NEWSFEED.COACH_MEAL.getValue());
				dataMap.put(AppConstants.NEWSFEED.LOG_TIME.getValue(), log.getCreatedOn().toString());
				dataMap.put(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.PUBLIC.getValue());
				dataMap.put(AppConstants.NEWSFEED.ACCESS_ID.getValue(), jsonObjectResult.getInt(AppConstants.NEWSFEED.ACCESS_ID.getValue())+"");
				dataMap.put(AppConstants.NEWSFEED.HAS_MISSING_FOOD.getValue(), jsonObjectResult.getBoolean(AppConstants.NEWSFEED.HAS_MISSING_FOOD.getValue())+"");
				dataMap.put(AppConstants.NEWSFEED.HAS_SHARED_EVER.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED.HAS_SHARED_EVER.getValue()));
				dataMap.put(AppConstants.NEWSFEED.CUSTOM_ID.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED.CUSTOM_ID.getValue()));
				dataMap.put(AppConstants.NEWSFEED.MEAL_NAME.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED.MEAL_NAME.getValue()));
				dataMap.put(AppConstants.NEWSFEED.MIME_TYPE.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED.MIME_TYPE.getValue()));

				try {

					dataMap.put(AppConstants.NEWSFEED._FILE_ICON.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED._FILE_ICON.getValue()));
					dataMap.put(AppConstants.NEWSFEED.FILE_ICON.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED._FILE_ICON.getValue()));
					dataMap.put(AppConstants.NEWSFEED.TAGS.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED._FILE_ICON.getValue()));

					Map<String, String> otherData = new LinkedHashMap<String, String>();
					otherData.put(AppConstants.NEWSFEED.NOTES.getValue(), ObjectUtils.nullSafe(token));
					dataMap.put(AppConstants.NEWSFEED.OTHER_DATA.getValue(), JsonUtil.toJson(otherData));

					if(jsonObjectResult.getString(AppConstants.NEWSFEED.TAGS.getValue()) != null){
						dataMap.put(AppConstants.NEWSFEED.TAGS.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED.TAGS.getValue()));
					}
				} catch (JSONException e) {

				}

				dataMap.put(AppConstants.NEWSFEED.CARBS.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED.CARBS.getValue()));
				dataMap.put(AppConstants.NEWSFEED.GUESS_CARBS.getValue(), jsonObjectResult.getString(AppConstants.NEWSFEED.GUESS_CARBS.getValue()));
				dataMap.put(AppConstants.NEWSFEED.DESCRIPTION.getValue(), "");

				response = SocialServicesUtils.postSocialData(JsonUtil.toJson(dataMap), AppConstants.SOCIALKEYS.UPLOADPOST.getValue());

				LOG.info("generateNewsFeedPost: SHARE_ANONYMOUSLY meal response from elgg: " + response);
				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			}

			smp.setMealSharedBy(provider.getProviderId());
			smp.setMealSharedDate(System.currentTimeMillis());
			int mealSharedCount = ObjectUtils.nullSafeInteger(smp.getMealSharedCount());
			mealSharedCount++;
			smp.setMealSharedCount(mealSharedCount);
			smp.setNotes(token);
			baseDAO.save(smp);

		} catch (Exception e) {
			e.printStackTrace();
		}

		description = "Generating newsfeed on elgg";
		jsonData = JsonUtil.toJson(returnMap);
		LOG.info("generateNewsFeedPost: returning response: " + jsonData);
		return "ajax";
	}

	private void sendMemberPermissionRequest(Patient patient, Log log){
		DeviceDAO deviceDAO = new DeviceDAO();
		try {
			Device device = deviceDAO.getDeviceByMac(patient.getDeviceMacAddress());
			if(device != null){
				List<Device> devices = new ArrayList<Device>();
				devices.add(device);
				String payLoad = log.getLogId();
				String result = PushNotificationManager.pushMessage(devices, AppConstants.GCMKEYS.SHARE_MEAL_PERMISSION.getValue(), payLoad, request);
			}else{
				LOG.info("generateNewsFeedPost: device not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<DykDTO> getDykDTOs() {
		return dykDTOs;
	}

	public void setDykDTOs(List<DykDTO> dykDTOs) {
		this.dykDTOs = dykDTOs;
	}

	public Map<Integer, String> getGroupMap() {
		return groupMap;
	}

	public void setGroupMap(Map<Integer, String> groupMap) {
		this.groupMap = groupMap;
	}

	public Long getAdviceIdToSend() {
		return adviceIdToSend;
	}

	public void setAdviceIdToSend(Long adviceIdToSend) {
		this.adviceIdToSend = adviceIdToSend;
	}

	public int getAdviceSentStatus() {
		return adviceSentStatus;
	}

	public void setAdviceSentStatus(int adviceSentStatus) {
		this.adviceSentStatus = adviceSentStatus;
	}

	public String getVideoLink() {
		return videoLink;
	}

	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}

	public String getVideoDescription() {
		return videoDescription;
	}

	public void setVideoDescription(String videoDescription) {
		this.videoDescription = videoDescription;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public List<DYKAdvice> getGroupPostsList() {
		return groupPostsList;
	}

	public void setGroupPostsList(List<DYKAdvice> groupPostsList) {
		this.groupPostsList = groupPostsList;
	}

	public String getVideoThumbnail() {
		return videoThumbnail;
	}

	public void setVideoThumbnail(String videoThumbnail) {
		this.videoThumbnail = videoThumbnail;
	}

	public String getVideoEmbedUrl() {
		return videoEmbedUrl;
	}

	public void setVideoEmbedUrl(String videoEmbedUrl) {
		this.videoEmbedUrl = videoEmbedUrl;
	}

	public String getLogId() {
		return logId;
	}

	public void setLogId(String logId) {
		this.logId = logId;
	}

	public String getMemberPermission() {
		return memberPermission;
	}

	public void setMemberPermission(String memberPermission) {
		this.memberPermission = memberPermission;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public List<GroupPosts> getAaData() {
		return aaData;
	}

	public void setAaData(List<GroupPosts> aaData) {
		this.aaData = aaData;
	}

	public List<GroupPosts> getGroupPostDTO() {
		return groupPostDTO;
	}

	public void setPatientLogsDto(List<GroupPosts> groupPostDTO) {
		this.groupPostDTO = groupPostDTO;
	}

	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}

	public InputStream getIs() {
		return is;
	}

	public void setIs(InputStream is) {
		this.is = is;
	}

	public Long getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(Long iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public Map<Integer, String> getFieldLabelMap() {
		return fieldLabelMap;
	}

	public void setFieldLabelMap(Map<Integer, String> fieldLabelMap) {
		this.fieldLabelMap = fieldLabelMap;
	}

	public String getHyperlink() {
		return hyperlink;
	}

	public void setHyperlink(String hyperlink) {
		this.hyperlink = hyperlink;
	}

	public String getHyperlinkDescription() {
		return hyperlinkDescription;
	}

	public void setHyperlinkDescription(String hyperlinkDescription) {
		this.hyperlinkDescription = hyperlinkDescription;
	}
}
