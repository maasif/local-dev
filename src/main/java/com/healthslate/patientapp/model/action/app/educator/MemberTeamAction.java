package com.healthslate.patientapp.model.action.app.educator;

import com.healthslate.patientapp.model.dao.BaseDAO;
import com.healthslate.patientapp.model.dao.FacilityDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.dao.ProviderDAO;
import com.healthslate.patientapp.model.dto.CoachDTO;
import com.healthslate.patientapp.model.entity.Facility;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.CommonUtils;
import com.healthslate.patientapp.util.JsonUtil;
import com.healthslate.patientapp.util.NotifyUtils;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Irfan Nasim on 09-Sep-15.
 */

@Results({
        @Result(name = "ajax", type = "json", params = {"root", "jsonData"})
})
public class MemberTeamAction extends ActionSupport implements SessionAware, ServletRequestAware {

    private HttpServletRequest request;
    private Map<String, Object> session;
    private long patientId = 0l;
    private String providerListString;
    private String jsonData;


    @Action(value = "getMemberTeamList")
    public String getMemberTeamList() {
        Map<String, Object> providers = new LinkedHashMap<String, Object>();
        Patient patient = new PatientDAO().getPatientById(patientId);
        long facilityId = patient.getFacilities().get(0).getFacilityId();

        List<Provider> patientProviders;
        patientProviders = CommonUtils.getExcludedSuperFacilityProviders(patient.getProviders(), false);
        List<CoachDTO> patientProvidersDTOs = getCoachesDTOs(patientProviders);
        List<CoachDTO> facilityProvidersDTOs = new ProviderDAO().getAllCoaches(facilityId);

        Provider leadCoach = CommonUtils.getLeadCoach(patient);
        Provider primaryFoodCoach = CommonUtils.getPrimaryFoodCoach(patient);

        providers.put(AppConstants.CoachesArea.PATIENT_COACHES.name(), patientProvidersDTOs);
        providers.put(AppConstants.CoachesArea.COACHES_LISTING.name(), facilityProvidersDTOs);
        providers.put(AppConstants.CoachesArea.LEAD_COACH_ID.name(), (leadCoach != null) ? leadCoach.getUser(): null);
        providers.put(AppConstants.CoachesArea.PRIMARY_FOOD_COACH_ID.name(), (primaryFoodCoach != null) ? primaryFoodCoach.getUser(): null);

        jsonData = JsonUtil.toJsonExcludedNull(providers);

        return "ajax";
    }

    @Action(value = "updateMemberTeam")
    public String updateMemberTeam() {
        Map<String, Object> returnMap = new LinkedHashMap<String, Object>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {
            PatientDAO patientDAO = new PatientDAO();
            Patient patient = patientDAO.getPatientById(patientId);
            List<Provider> selectedProviders;

            selectedProviders = CommonUtils.getProvidersFromString(providerListString);

            List<Long> patientProvidersIds = getProviderIds(CommonUtils.getExcludedSuperFacilityProviders(patient.getProviders(), false));
            List<Long> selectedProvidersIds = getProviderIds(selectedProviders);

            patientDAO.deleteAssignedProvidersFromPatient(patientProvidersIds, patient);
            patientDAO.updateAssignedProvidersFromPatient(selectedProvidersIds, patient);

            //sending GCM to member to refresh my team
            NotifyUtils.notifyCoachOfUpdatedMemberTeam(patient, request);

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    private List<CoachDTO> getCoachesDTOs(List<Provider> coachList) {
        List<CoachDTO> primaryCoachList = new ArrayList<CoachDTO>();
        for (Provider p : coachList) {
            CoachDTO c = new CoachDTO();
            c.setUserId(p.getUser().getUserId());
            c.setProviderId(p.getProviderId());
            c.setFirstName(p.getUser().getFirstName());
            c.setLastName(p.getUser().getLastName());
            c.setType(p.getType());
            c.setPhone(p.getPhone());
            c.setEmail(p.getEmail());
            c.setFacilityId(p.getFacility().getFacilityId());
            c.setFacilityName(p.getFacility().getName());
            c.setDesignation(p.getDesignation());
            c.setSMSEnabled(p.getIsSMSEnabled());
//            c.setEmailEnabled(p.getIsEmailEnabled());

            primaryCoachList.add(c);
        }
        return primaryCoachList;
    }

    private List<Long> getProviderIds(List<Provider> providers) {
        List<Long> ids = new ArrayList<Long>();
        for (Provider p : providers) {
            ids.add(p.getProviderId());
        }
        return ids;
    }

    @Override
    public void setSession(Map<String, Object> arg0) {
        session = arg0;
    }

    @Override
    public void setServletRequest(HttpServletRequest arg0) {
        request = arg0;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getProviderListString() {
        return providerListString;
    }

    public void setProviderListString(String providerListString) {
        this.providerListString = providerListString;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }
}
