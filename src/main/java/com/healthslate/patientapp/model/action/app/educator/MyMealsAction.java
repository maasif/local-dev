package com.healthslate.patientapp.model.action.app.educator;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.LogDTO;
import com.healthslate.patientapp.model.dto.RecentMealDTO;
import com.healthslate.patientapp.model.dto.ShareableMealDTO;
import com.healthslate.patientapp.model.dto.SuggestedMealDTO;
import com.healthslate.patientapp.model.entity.JQueryDataTableParamModel;
import com.healthslate.patientapp.model.entity.Log;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.util.*;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/myMeals.tiles"),
	@Result(name="shareableMeals", type="tiles", location="educator/shareableMeals.tiles"),
	@Result(name="ajaxTable", type="stream", params={"contentType", "application/Json", "inputName", "is"}),
	@Result(name="ajax", type="json", params={"root", "ajaxData"})
})
public class MyMealsAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private boolean processed;
	private String date;
	private String mealType;
	private Long patientId;
	private String jsonData;
    private String suggestedMealsData;
	private String ajaxData;
	private LogDAO logDAO = new LogDAO();
	private ShareMealPermissionsDAO shareMealPermissionsDAO = new ShareMealPermissionsDAO();

    private SuggestedLogDAO suggestedLogDAO = new SuggestedLogDAO();
	private Map<String, Object> session;
	private HttpServletRequest request;
	public String description;
	public String appPath;
    private String logId;
	private Map<Integer, String> logTypeMap = new LinkedHashMap<Integer, String>();
	private Patient patient;

	public List<ShareableMealDTO> aaData;
	List<ShareableMealDTO> patientLogsDto;
	private String sEcho;
	private InputStream is;
	private Long iTotalRecords = null;
	private Integer iTotalDisplayRecords = null;
	private Map<Integer, String> fieldLabelMap;

	@Action(value="myMealsPage")
	public String openMyMealsPage(){
		appPath = this.request.getContextPath();
		patient = new PatientDAO().getPatientById(patientId);
		return ActionSupport.SUCCESS;
	}
	
	@Action(value="getMealsByType")
	public String getMealsByType(){
		appPath = this.request.getContextPath();
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		
		if(!ObjectUtils.isEmpty(mealType) && patientId != null){			
			List<RecentMealDTO> recentMealDTOs = logDAO.getAllRecentMeals(patientId, mealType, appPath + AppConstants.LOG_IMAGE_FOLDER, date, processed);
            List<SuggestedMealDTO> suggestedMealDTOs = suggestedLogDAO.getAllSuggestedMeals(patientId, mealType, appPath + AppConstants.LOG_IMAGE_FOLDER);
			List<RecentMealDTO> favoriteMealDTOs = logDAO.getAllFavoriteLogsDTOsByPatient(patientId, mealType, appPath + AppConstants.LOG_IMAGE_FOLDER);

			if(recentMealDTOs != null && recentMealDTOs.size() > 0){
				ajaxData = JsonUtil.toJson(recentMealDTOs);
				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
				returnMap.put(AppConstants.JsonConstants.DATA.name(), JsonUtil.toJson(recentMealDTOs));
                returnMap.put(AppConstants.JsonConstants.SUGGESTED_MEALS.name(), JsonUtil.toJson(suggestedMealDTOs));
				returnMap.put(AppConstants.JsonConstants.FAVORITE_MEALS.name(), JsonUtil.toJson(favoriteMealDTOs));
			}
			description = "Displayed Meals for meal type "+mealType+" and patient id "+patientId;
		} else {
			description = "Displaying Meals failed for meal type "+mealType+" and patient id "+patientId;
		}
		
		ajaxData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

    @Action(value="removeSuggestedMeal")
    public String removeSuggestedMeal(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        if(!ObjectUtils.isEmpty(logId)){
            appPath = this.request.getContextPath();
            Log log = logDAO.getLogById(logId);
            if(log != null){
                log.setIsRemoved(true);
                new BaseDAO().save(log);
                List<SuggestedMealDTO> suggestedMealDTOs = suggestedLogDAO.getAllSuggestedMeals(patientId, mealType, appPath + AppConstants.LOG_IMAGE_FOLDER);
                NotifyUtils.notifyPatientOfSuggestedMeals(log.getPatient(), request);
                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
                returnMap.put(AppConstants.JsonConstants.DATA.name(), JsonUtil.toJson(suggestedMealDTOs));
            }
            description = "Removed suggested log, logId: "+logId;
        }

        ajaxData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

	@Action(value="removeFavoriteMeal")
	public String removeFavoriteMeal(){
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

		if(!ObjectUtils.isEmpty(logId)){
			appPath = this.request.getContextPath();
			Log log = logDAO.getLogById(logId);
			if(log != null){
				log.setIsFavorite(false);
				new BaseDAO().save(log);
				List<RecentMealDTO> favoriteMealDTOs = logDAO.getAllFavoriteLogsDTOsByPatient(patientId, mealType, appPath + AppConstants.LOG_IMAGE_FOLDER);
				// NotifyUtils.notifyPatientOfSuggestedMeals(log.getPatient(), request);
				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
				returnMap.put(AppConstants.JsonConstants.DATA.name(), JsonUtil.toJson(favoriteMealDTOs));
			}
			description = "Removed Favorite log, logId: "+logId;
		}

		ajaxData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

	@Action(value="shareableMeals")
	public String shareableMealsPage() {
		return "shareableMeals";
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Action(value="fetchShareableMeals")
	public String fetchShareableMeals()throws IOException {

		Provider provider = (Provider) session.get(AppConstants.SessionKeys.PROVIDER.name());

		aaData = new ArrayList();

		JQueryDataTableParamModel param = DataTablesParamUtility.getParam(request);
		sEcho = param.sEcho;
		Long iTotalRecordsInt; // total number of records (un-filtered)
		int iTotalDisplayRecordsInt;

		populateShareableMealsFieldMap();
		String sortIndex= fieldLabelMap.get(param.iSortColumnIndex);

		patientLogsDto = shareMealPermissionsDAO.getShareableMealsList(provider.getProviderId(), param.sSearch, param.iDisplayStart, param.iDisplayLength, sortIndex, param.sSortDirection);

		sEcho = param.sEcho;
		aaData = patientLogsDto;
		iTotalDisplayRecordsInt = aaData.size();
		iTotalRecordsInt = shareMealPermissionsDAO.getShareableMealsListCount(provider.getProviderId(), param.sSearch);
		iTotalRecords = iTotalRecordsInt;
		iTotalDisplayRecords = iTotalDisplayRecordsInt;

		//iTotalRecordsInt =
		JsonObject jsonResponse = new JsonObject();
		jsonResponse.addProperty("sEcho", sEcho);
		jsonResponse.addProperty("iTotalRecords", iTotalRecords);
		jsonResponse.addProperty("iTotalDisplayRecords", iTotalRecords);

		Gson gson = new Gson();
		jsonResponse.add("aaData", gson.toJsonTree(aaData));
		is = new ByteArrayInputStream(jsonResponse.toString().getBytes());

		return "ajaxTable";
	}

	private void populateShareableMealsFieldMap(){
		fieldLabelMap = new LinkedHashMap<Integer, String>();
		fieldLabelMap.put(0, "u.display_name");
		fieldLabelMap.put(1, "fls.meal_name");
		fieldLabelMap.put(2, "fls.type");
		fieldLabelMap.put(3, "smp.permissions");
		fieldLabelMap.put(4, "smp.permission_asked_date");
		fieldLabelMap.put(5, "smp.permission_granted_date");
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getMealType() {
		return mealType;
	}

	public void setMealType(String mealType) {
		this.mealType = mealType;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public Map<Integer, String> getLogTypeMap() {
		return AppConstants.MEAL_TYPES;
	}

	public void setLogTypeMap(Map<Integer, String> logTypeMap) {
		this.logTypeMap = logTypeMap;
	}

	public String getAjaxData() {
		return ajaxData;
	}

	public void setAjaxData(String ajaxData) {
		this.ajaxData = ajaxData;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


    public String getSuggestedMealsData() {
        return suggestedMealsData;
    }

    public void setSuggestedMealsData(String suggestedMealsData) {
        this.suggestedMealsData = suggestedMealsData;
    }

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public boolean isProcessed() {
		return processed;
	}

	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

	public List<ShareableMealDTO> getAaData() {
		return aaData;
	}

	public void setAaData(List<ShareableMealDTO> aaData) {
		this.aaData = aaData;
	}

	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}

	public InputStream getIs() {
		return is;
	}

	public void setIs(InputStream is) {
		this.is = is;
	}

	public Long getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(Long iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public Map<Integer, String> getFieldLabelMap() {
		return fieldLabelMap;
	}

	public void setFieldLabelMap(Map<Integer, String> fieldLabelMap) {
		this.fieldLabelMap = fieldLabelMap;
	}

	public List<ShareableMealDTO> getPatientLogsDto() {
		return patientLogsDto;
	}

	public void setPatientLogsDto(List<ShareableMealDTO> patientLogsDto) {
		this.patientLogsDto = patientLogsDto;
	}
}
