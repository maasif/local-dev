package com.healthslate.patientapp.model.action.app.educator;

import com.amazonaws.services.opsworks.model.App;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.PatientDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.components.Push;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/patients.tiles"),
	@Result(name="ajax", type="json", params={"root", "jsonData"}),
	@Result(name="ajaxTable", type="stream", params={"contentType", "application/Json", "inputName", "is"})
})

@SuppressWarnings("serial")
public class MyMembersAction extends ActionSupport implements SessionAware, ServletRequestAware {

	private long patientId = 0l;
	private String jsonData;

	private String sEcho;
	public String description;
	private Map<String, Object> session;
	private InputStream is;
	private Long iTotalRecords = null;
	private Integer iTotalDisplayRecords = null;
	private Map<Integer, String> fieldLabelMap;
	PatientDAO patientDAO = new PatientDAO();
	private List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();
	private HttpServletRequest request;

    private String filterMyMembers;
	private String cannedMessageTemplate;

	@Action(value="members")
	public String myMembers() {
		description = "Displayed Feedback logs page";
		cannedMessageTemplate = new SmsTemplateDAO().getSmsTemplateByName(AppConstants.SmsTemplates.CANNED_EMAIL.getValue());
		return ActionSupport.SUCCESS;
	}

	@Action(value="fetchMyMembersData")
	public String fetchMyMembersData() {
		PatientDAO patientDAO = new PatientDAO();
		JQueryDataTableParamModel param = DataTablesParamUtility.getParam(request);
		sEcho = param.sEcho;
		Long iTotalRecordsInt; // total number of records (un-filtered)
		int iTotalDisplayRecordsInt;
		populateFieldLabelsMap();
		String sortIndex = fieldLabelMap.get(param.iSortColumnIndex);

		Provider provider = HSSessionUtils.getProviderSession(session);
		patientDTOs = patientDAO.getPatientListByCoach(param.iDisplayStart, param.iDisplayLength, param.sSearch, sortIndex, param.sSortDirection, provider, filterMyMembers);
		iTotalRecordsInt =  patientDAO.getPatientListCountByCoach(param.sSearch, provider, filterMyMembers);

		sEcho = param.sEcho;
		iTotalDisplayRecordsInt = patientDTOs.size();
		iTotalRecords = iTotalRecordsInt;
		iTotalDisplayRecords = iTotalDisplayRecordsInt;

		//iTotalRecordsInt =
		JsonObject jsonResponse = new JsonObject();
		jsonResponse.addProperty("sEcho", sEcho);
		jsonResponse.addProperty("iTotalRecords", iTotalRecords);
		jsonResponse.addProperty("iTotalDisplayRecords", iTotalRecords);

		Gson gson = new Gson();
		jsonResponse.add("aaData", gson.toJsonTree(patientDTOs));
		is = new ByteArrayInputStream(jsonResponse.toString().getBytes());

		description = "Fetched Members Data";
		return "ajaxTable";
	}

	private void populateFieldLabelsMap() {
		fieldLabelMap = new LinkedHashMap<Integer, String>();
		fieldLabelMap.put(0, "p.patient_id");
		fieldLabelMap.put(1, "u.first_name");
		fieldLabelMap.put(2, "u.email");
		fieldLabelMap.put(3, "u.phone");
		/*fieldLabelMap.put(4, "innerQuery2.sessionTime");*/
		//fieldLabelMap.put(4, "innerQuery5.notesCoachName");
		fieldLabelMap.put(4, "innerQueryLastContact.lastContactDate");
		fieldLabelMap.put(5, "innerQueryLastContactReceived.lastContactReceivedDate");
		/*fieldLabelMap.put(5, "innerQuery.unreadCount");
        fieldLabelMap.put(6, "innerQuery5.time");*/
		fieldLabelMap.put(6, "registeredWeeks");
		fieldLabelMap.put(7, "innerQuery4.lastLogTime");
        /*fieldLabelMap.put(7, "innerQuery4.lastLogTime");*/
		/*fieldLabelMap.put(6, "us.first_name");*/
	}

	@Action(value="sendMemberCannedMessage")
	public String sendMemberCannedMessage() throws ParseException {
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		try {
			Patient patient = patientDAO.getPatientById(patientId);
			Device device = new DeviceDAO().getDeviceByMac(patient.getDeviceMacAddress());
			Provider provider = HSSessionUtils.getProviderSession(session);
			String providerFirstName = provider.getUser().getFirstName();

			if(device != null) {
				List<Device> devices = new ArrayList<Device>();
				devices.add(device);
				String cannedMessageTitle = new SmsTemplateDAO().getSmsTemplateByName(AppConstants.SmsTemplates.CANNED_EMAIL_TITLE.getValue());

				if(provider.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())){
					provider = new ProviderDAO().getDefaultTechSupport();
					providerFirstName = AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue();
				}

				cannedMessageTitle = cannedMessageTitle.replace(AppConstants.StringReplaceChunks.F_NAME.getValue(), providerFirstName);
				String messageForElgg = cannedMessageTemplate;
				cannedMessageTemplate += AppConstants.StringReplaceChunks.HASH.getValue()+cannedMessageTitle;

				String result = PushNotificationManager.pushMessage(devices, AppConstants.GCMKEYS.CANNED_MESSAGE.getValue(), cannedMessageTemplate, request);

				NotifyUtils.sendInAppMessage(messageForElgg,
						provider,
						patient,
						AppConstants.PROVIDER,
						request,
						true);

				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		description = "Sending canned message to member";
		jsonData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public List<PatientDTO> getPatientDTOs() {
		return patientDTOs;
	}

	public void setPatientDTOs(List<PatientDTO> patientDTOs) {
		this.patientDTOs = patientDTOs;
	}

	public PatientDAO getPatientDAO() {
		return patientDAO;
	}

	public void setPatientDAO(PatientDAO patientDAO) {
		this.patientDAO = patientDAO;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}

	public InputStream getIs() {
		return is;
	}

	public void setIs(InputStream is) {
		this.is = is;
	}

	public Map<Integer, String> getFieldLabelMap() {
		return fieldLabelMap;
	}

	public void setFieldLabelMap(Map<Integer, String> fieldLabelMap) {
		this.fieldLabelMap = fieldLabelMap;
	}

	public Long getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(Long iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	@Override
	public void setServletRequest(HttpServletRequest httpServletRequest) {
		this.request = httpServletRequest;
	}

    public String getFilterMyMembers() {
        return filterMyMembers;
    }

    public void setFilterMyMembers(String filterMyMembers) {
        this.filterMyMembers = filterMyMembers;
    }

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getCannedMessageTemplate() {
		return cannedMessageTemplate;
	}

	public void setCannedMessageTemplate(String cannedMessageTemplate) {
		this.cannedMessageTemplate = cannedMessageTemplate;
	}
}