package com.healthslate.patientapp.model.action.app.educator;

import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.GoalRatingDTO;
import com.healthslate.patientapp.model.dto.GoalSummaryDTO;
import com.healthslate.patientapp.model.dto.HabitDetailsDTO;
import com.healthslate.patientapp.model.dto.PatientDetailDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import com.healthslate.patientapp.ws.SocialGroupServices;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.StringType;
import net.sf.ehcache.hibernate.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Hibernate;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.AccessDeniedException;
import java.text.SimpleDateFormat;
import java.util.*;

/*
* ======= FILE CHANGE HISTORY =======
* [2/24/2015]: Created by __oz
* ===================================
 */

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/patientDashboard.tiles"),
    @Result(name="topics-overview", type="tiles", location="educator/topicsOverview.tiles"),
    @Result(name="access-denied", location="/accessDeniedPage.jsp"),
	@Result(name="ajax", type="json", params={"root", "jsonData"})
})

@SuppressWarnings("serial")
public class PatientDashboardAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private String jsonData;	
	private Long patientId;
	private Map<String, Object> session;
	private HttpServletRequest request;
    private String smsTemplateList;
    private String email;

    public Patient patient;
    public PatientMotivationImage patientMotivationImage;
    private long messagesCount;
    private String dashboardDateString;
    private String formattedDateString;
    private String oneToOneSessionListString;
    private String coachNotesString;
    private String coachesListString;
    private String myCoachesString;
    private String dobFormatted;
    private String diagnosisDateFormatted;
    private String medicationsListing;
    private String medicationString;
    private String token;
    private String uuid;
    private String week;

	private BaseDAO baseDAO = new BaseDAO();
    private LogDAO logDAO = new LogDAO();
    private PatientDAO patientDAO = new PatientDAO();
    private FilledFormDAO filledFormDAO = new FilledFormDAO();
    private FormDataDAO formDataDAO = new FormDataDAO();
    private SmsTemplateDAO smsTemplateDAO = new SmsTemplateDAO();

    private UserActivity userActivity;

    private long logEntriesCount;
    private long targetSteps;
    private long averageSteps;
    private Integer misfitBatteryStatus;

    private String goalsList;
    private String habitsList;

    private String coachNotesAJAX;
    private String sessionDateAJAX;
    private String notifyCoachesListAJAX;
    private String curriculumStringAJAX;
    private String dietaryNotesAJAX;
    private long chosenCoachIdAJAX;
    private String medicationNameAJAX;
    private boolean medicationConfirmedAJAX;
    private String dosageAJAX;
    private long medicationMasterIdAJAX;
    private String sessionTimeAJAX;
    private long customIdAJAX;
    private String customSMSAJAX;
    private long customDataNumber;
    private long HBloggingDate;

    private String customDataAJAX;
    private String sessionDays;
    private String sessionTimes;

	public String description;

    private int currentWeight;
    private int startingWeight;
    private int goalWeight;

    private String CURRICULUM_API_ADDRESS;
    private long id;
    private String memberDOB;
    private String phoneVersion;
    private String secureLink;

    public String httpOrHttps;
    public String meterSerialNumber;

	@Action(value="dashboard")
	public String showPatientDashboard() {

        Provider provider = HSSessionUtils.getProviderSession(session);

        httpOrHttps = request.getScheme();

        if (!ObjectUtils.isEmpty(token)) {
            try {
                //token here is UUID
                patient = patientDAO.getPatientByUUID(token);
                patientId = patient.getPatientId();
                if(!new PatientDAO().doesPatientAssignToProvider(provider.getProviderId(), patientId)){
                    //[2/12/2015]: logged access denied entry in DB, __oz
                    CommonUtils.saveAccessDeniedLog(request, patientId);
                    return "access-denied";
                }
            } catch (Exception e) {
                e.printStackTrace();
                //fall back to encryption utils token if opened link is of older version than the UUID change
                try {
                    token = new EncryptionUtils().decrypt(token);
                    patientId = Long.parseLong(token);
                    if(!new PatientDAO().doesPatientAssignToProvider(provider.getProviderId(), patientId)){
                        CommonUtils.saveAccessDeniedLog(request, patientId);
                        return "access-denied";
                    }
                } catch (Exception e1) {
                    try {
                        throw new AccessDeniedException(AppConstants.ACCESS_DENIED);
                    } catch (AccessDeniedException ade) {
                        ade.printStackTrace();
                    }
                }
            }
        }

        if(patientId != null && patientId > 0){
            //in case if this is not open via secure link then get patient from DB
            if(patient ==  null){
                patient = patientDAO.getPatientById(patientId);
            }
            CURRICULUM_API_ADDRESS = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.CURRICULUM_API.getValue());
            if (patient != null) {

                //gets uuid of patient
                uuid = patient.getUuid();

                memberDOB = "N/A";

                if(patient.getDob() != null){
                    memberDOB = DateUtils.getFormattedDate(patient.getDob().getTime(), DateUtils.DATE_FORMAT);
                }

                meterSerialNumber = "N/A";

                if(!ObjectUtils.isEmpty(patient.getMeterSerialNumber())){
                    meterSerialNumber = patient.getMeterSerialNumber();
                }

                //gets weekdate from now
                dashboardDateString = DateUtils.getLastWeekDate(new Date()) + " - " + new SimpleDateFormat("MM/dd/yyyy").format(new Date());
                formattedDateString = DateUtils.getFormattedDate(DateUtils.getLastWeekDateFromCurrent(new Date()).getTime(), DateUtils.DATE_FORMAT_DASHBOARD) + " - " + DateUtils.getFormattedDate(System.currentTimeMillis(), DateUtils.DATE_FORMAT_DASHBOARD);

                phoneVersion = CommonUtils.getDeviceTypeOfPatient(patientId, patient.getDeviceMacAddress()) + " / " + CommonUtils.getMobileAppVersionOfPatient(patientId);
                secureLink = CommonUtils.getMemberDashboardLink(request, patient.getUuid());

                //gets activity summary target/average steps
                Target target = patient.getTarget();
                if (target != null) {
                    targetSteps = ObjectUtils.nullSafe(target.getActivitySteps());
                    startingWeight = Math.round(ObjectUtils.nullSafe(target.getStartingWeight()));
                    goalWeight = Math.round(ObjectUtils.nullSafe(target.getTargetWeight()));
                }

                //gets patient motivation from DB
                patientMotivationImage = patient.getPatientMotivationImage();
            }

            description = "Displayed member dashboard page for member id: "+patientId;
            return ActionSupport.SUCCESS;
        }

        //[6/22/2015]: logged access denied entry in DB, __oz
        ServiceFilterLog logging = new ServiceFilterLog();
        logging.setService(ActionContext.getContext().getName() + ": Unable to open member dashboard because member is null.");
        logging.setStatus("Member not found");
        logging.setTimestamp(System.currentTimeMillis());
        logging.setIpAddress(request.getRemoteAddr());
        logging.setPatientId(null);
        new BaseDAO().save(logging);
        return "access-denied";
	}

    @Action(value="topicsOverview")
    public String showTopicsOverviewPage() {

        Provider provider = HSSessionUtils.getProviderSession(session);
        patient = patientDAO.getPatientById(patientId);
        CURRICULUM_API_ADDRESS = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.CURRICULUM_API.getValue());

        if (patient != null) {

            //gets uuid of patient
            uuid = patient.getUuid();

            memberDOB = "N/A";

            if(patient.getDob() != null){
                memberDOB = DateUtils.getFormattedDate(patient.getDob().getTime(), DateUtils.DATE_FORMAT);
            }

            meterSerialNumber = "N/A";

            if(!ObjectUtils.isEmpty(patient.getMeterSerialNumber())){
                meterSerialNumber = patient.getMeterSerialNumber();
            }

            secureLink = CommonUtils.getMemberDashboardLink(request, patient.getUuid());
            phoneVersion = CommonUtils.getDeviceTypeOfPatient(patientId, patient.getDeviceMacAddress()) + " / " + CommonUtils.getMobileAppVersionOfPatient(patientId);
        }

        description = "Displayed topic overview page for member id: "+patientId;
        return "topics-overview";
    }

    //gets user activity count from elgg service
    @Action(value="getUserActivityCount")
    public String getUserActivityCount(){
        Map<String, Object> returnMap = new LinkedHashMap<String, Object>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {
            long lastWeekMillis = DateUtils.getLastWeekDateInMillis(new Date());
            lastWeekMillis = DateUtils.getStartOfDayTime(lastWeekMillis);

            long nowMillis = System.currentTimeMillis();
            nowMillis = DateUtils.getEndOfDayTime(nowMillis);

            userActivity = new SocialGroupServices().getUserActivityCounts(email, lastWeekMillis, nowMillis);
            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.JsonConstants.DATA.name(), userActivity);
        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="getUnreadMessagesCount")
    public String getUnreadMessagesOfPatient(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {
            Provider provider = HSSessionUtils.getProviderSession(session);
            if(provider.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())){
                messagesCount = new TechSupportMessageDAO().getUnreadMessageOfTechSupport(patientId, provider.getProviderId());
            } else {
                messagesCount = new LogDAO().getUnreadMessageOfCoach(patientId, provider.getUser().getUserId());
            }

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.JsonConstants.DATA.name(), messagesCount+"");

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="getUnreadCoachesMessageCount")
    public String getUnreadCoachesMessageCount(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {
            Patient patient = new PatientDAO().getPatientById(patientId);
            Provider provider = HSSessionUtils.getProviderSession(session);
            messagesCount = new LogDAO().getUnreadMessageOfCoach(patient.getPatientId(), provider.getUser().getUserId());
            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.JsonConstants.DATA.name(), messagesCount+"");

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="addCoachNotes")
    public String addCoachNotes(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {
            patient = new PatientDAO().getPatientById(patientId);
            Provider provider = HSSessionUtils.getProviderSession(session);
            List<Provider> leadOrPrimaryFoodCoach = CommonUtils.getCoachesToNotify(provider, patient);
            String notifiedCoaches = "";
            CoachNotes coachNotes = new CoachNotes(coachNotesAJAX, provider, patient, customDataNumber);

            for(Provider prov: leadOrPrimaryFoodCoach){
                notifiedCoaches += prov.getProviderId() + ",";
            }

            String comma = ObjectUtils.isEmpty(notifyCoachesListAJAX) ? "" : ",";

            coachNotes.setNotifiedCoaches((ObjectUtils.isEmpty(notifyCoachesListAJAX)?"":notifyCoachesListAJAX) + comma + notifiedCoaches.replaceAll(",$", ""));
            baseDAO.save(coachNotes);

            //send notify email to selected coaches
            if(!ObjectUtils.isEmpty(notifyCoachesListAJAX)){

                String[] commaSeparated = notifyCoachesListAJAX.split(",");
                List<Provider> coachesList = new ArrayList<Provider>();

                Map<Long, Object> allProviders = CommonUtils.listToMapObjects(new ProviderDAO().list());

                for(String proId: commaSeparated){
                    long providerId = Long.parseLong(proId);
                    if(allProviders.containsKey(providerId)){
                        coachesList.add((Provider)allProviders.get(providerId));
                    }
                }

                if(coachesList.size() > 0){
                    NotifyUtils.notifyCoachesOfNewNoteAndAssessment(request, patientId, coachesList, true);
                }
            }

            NotifyUtils.notifyCoachesOfNewNoteAndAssessment(request, patientId, leadOrPrimaryFoodCoach, true);
            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="addDietaryNotes")
    public String addDietaryNotes(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            patient = new PatientDAO().getPatientById(patientId);

            DietaryNotes dietaryNotes = new DietaryNotes(dietaryNotesAJAX, HSSessionUtils.getProviderSession(session), patient, customDataNumber);
            baseDAO.save(dietaryNotes);

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="addMemberKeyFacts")
    public String addMemberKeyFacts(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            patient = new PatientDAO().getPatientById(patientId);

            MemberKeyFacts memberKeyFacts = new MemberKeyFacts(dietaryNotesAJAX, HSSessionUtils.getProviderSession(session), patient, customDataNumber);
            baseDAO.save(memberKeyFacts);

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="addCoachLogs")
    public String addCoachLogs(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {
            patient = new PatientDAO().getPatientById(patientId);
            Provider provider = HSSessionUtils.getProviderSession(session);

            CoachHBLogging coachLog = new CoachHBLogging(customDataAJAX, customDataNumber, provider, patient, HBloggingDate);
            baseDAO.save(coachLog);

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="getAllCoachLogs")
    public String getAllCoachLogs(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            jsonData = JsonUtil.toJsonExcludedNull(new CoachHBLoggingDAO().getAllHBLogsByPatient(patientId));

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.JsonConstants.DATA.name(), jsonData);

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="getAllDietaryNotes")
    public String getAllDietaryNotes(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            jsonData = JsonUtil.toJsonExcludedNull(new DietaryNotesDAO().getAllNotesByPatient(patientId));

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.JsonConstants.DATA.name(), jsonData);

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="getAllMemberKeyFacts")
    public String getAllMemberKeyFacts(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            jsonData = JsonUtil.toJsonExcludedNull(new MemberKeyFactsDAO().getAllMemberKeyFactsByPatient(patientId));

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.JsonConstants.DATA.name(), jsonData);

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="getAllCoachNotes")
    public String getAllCoachNotes(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            jsonData = JsonUtil.toJsonExcludedNull(new CoachNotesDAO().getAllNotesByPatient(patientId));

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.JsonConstants.DATA.name(), jsonData);

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="addOneToOneSessionWithPatient")
    public String addOneToOneSession(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            OneToOneSession oneToOneSession = new OneToOneSession();
            Provider provider = HSSessionUtils.getProviderSession(session);
            patient = new PatientDAO().getPatientById(patientId);
            Device device = new DeviceDAO().getDeviceByMac(patient.getDeviceMacAddress());

            User patientUser = patient.getUser();
            long offsetTime = CommonUtils.getPatientFacilityTimezoneOffset(patient);

            long millisInUserOffset = customDataNumber;
            oneToOneSession.setSessionTime(millisInUserOffset);
            oneToOneSession.setPatient(patient);

            Provider providerChosen = new Provider();
            providerChosen.setProviderId(chosenCoachIdAJAX);

            oneToOneSession.setProvider(providerChosen);
            oneToOneSession.setSessionCreatedBy(provider.getProviderId());

            //sending member Scheduled SMS notification as soon as Session is added, __oz
            String patientPhoneNumber = patientUser.getPhone();
            String smsTemplateInstant = smsTemplateDAO.getSmsTemplateByName(AppConstants.SmsTemplates.VIDEO_CALL_INSTANT_MSG.getValue());
            smsTemplateInstant = smsTemplateInstant.replace(AppConstants.StringReplaceChunks.TIME.getValue(), DateUtils.getFormattedDate(millisInUserOffset + offsetTime, DateUtils.TIME_FORMAT));
            smsTemplateInstant = smsTemplateInstant.replace(AppConstants.StringReplaceChunks.DATE.getValue(), DateUtils.getFormattedDate(millisInUserOffset + offsetTime, DateUtils.DATE_FORMAT_ONLY_MONTH));
            smsTemplateInstant = smsTemplateInstant.replace(AppConstants.StringReplaceChunks.DATE_TIME.getValue(), millisInUserOffset+"");
            if(device != null && device.getDeviceType().equalsIgnoreCase(AppConstants.DeviceTypes.IOS.getValue())){
                smsTemplateInstant = smsTemplateInstant.replace(AppConstants.HTTP_KEY, AppConstants.IOS_SMS_KEY);
            }

            //EmailUtils.sendOneToOneSessionEmail(patientUser.getEmail(), "New 1:1 Session", smsTemplateInstant, false);
            TwilioMessageUtils.sendSMS(patientPhoneNumber, smsTemplateInstant);
            baseDAO.save(oneToOneSession);

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="rescheduleOneToOneSessionWithPatient")
    public String rescheduleOneToOneSessionWithPatient(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            OneToOneSession oneToOneSession = new OneToOneSessionsDAO().getOneToOneSessionsById(customIdAJAX);

            Provider provider = HSSessionUtils.getProviderSession(session);
            patient = new PatientDAO().getPatientById(patientId);

            String combinedDateWithTime = sessionDateAJAX + " " + sessionTimeAJAX;
            User patientUser = patient.getUser();
            String userOffsetKey = patientUser.getOffsetKey();
            long offsetTime = CommonUtils.getPatientFacilityTimezoneOffset(patient);
            /*long selectedTime = new Date(combinedDateWithTime).getTime();
            long millisInUserOffset = DateUtils.getMillisecondsByUserOffset(selectedTime, userOffsetKey, DateUtils.DATE_FORMAT_WITH_TIME);*/

            long millisInUserOffset = customDataNumber;
            oneToOneSession.setPatient(patient);

            //sending member Scheduled SMS notification as soon as Session is added, __oz
            String patientPhoneNumber = patientUser.getPhone();
            String smsTemplateRescheduled = smsTemplateDAO.getSmsTemplateByName(AppConstants.SmsTemplates.VIDEO_CALL_RESCHEDULED_MESSAGE.getValue());

            //if no change then, no notifying the member
            if(customDataNumber == oneToOneSession.getSessionTime() && chosenCoachIdAJAX == oneToOneSession.getProvider().getProviderId()) {

                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());

            }else{

                boolean isDateTimeChanged = false;

                //if datetime of schedule 1:1 session changes, then build sms template and send it
                if(customDataNumber != oneToOneSession.getSessionTime()){
                    smsTemplateRescheduled = smsTemplateRescheduled.replace(AppConstants.StringReplaceChunks.TIME.getValue(), DateUtils.getFormattedDate(oneToOneSession.getSessionTime() + offsetTime, DateUtils.TIME_FORMAT));
                    smsTemplateRescheduled = smsTemplateRescheduled.replace(AppConstants.StringReplaceChunks.DATE.getValue(), DateUtils.getFormattedDate(oneToOneSession.getSessionTime() + offsetTime, DateUtils.DATE_FORMAT_ONLY_MONTH));
                    smsTemplateRescheduled = smsTemplateRescheduled.replace(AppConstants.StringReplaceChunks.NEW_TIME.getValue(), DateUtils.getFormattedDate(customDataNumber + offsetTime, DateUtils.TIME_FORMAT));
                    smsTemplateRescheduled = smsTemplateRescheduled.replace(AppConstants.StringReplaceChunks.NEW_DATE.getValue(), DateUtils.getFormattedDate(customDataNumber + offsetTime, DateUtils.DATE_FORMAT_ONLY_MONTH));
                    isDateTimeChanged = true;
                }

                oneToOneSession.setProvider(null);
                Provider providerChosen = new Provider();
                providerChosen.setProviderId(chosenCoachIdAJAX);
                oneToOneSession.setProvider(providerChosen);
                oneToOneSession.setSessionCreatedBy(provider.getProviderId());

                if(isDateTimeChanged){
                    //EmailUtils.sendOneToOneSessionEmail(patientUser.getEmail(), "1:1 Session Rescheduled", smsTemplateRescheduled, false);
                    TwilioMessageUtils.sendSMS(patientPhoneNumber, smsTemplateRescheduled);
                }
                oneToOneSession.setSessionTime(customDataNumber);
                baseDAO.save(oneToOneSession);

                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
                returnMap.put(AppConstants.JsonConstants.REASON.name(), "Changed");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="deleteOneToOneSession")
    public String deleteOneToOneSession(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            OneToOneSession oneToOneSession = new OneToOneSessionsDAO().getOneToOneSessionsById(customIdAJAX);
            patient = oneToOneSession.getPatient();
            User patientUser = patient.getUser();
            String patientPhoneNumber = patientUser.getPhone();
            long offsetTime = CommonUtils.getPatientFacilityTimezoneOffset(patient);

            if(ObjectUtils.isEmpty(customSMSAJAX)){
                String smsTemplateCanceled = smsTemplateDAO.getSmsTemplateByName(AppConstants.SmsTemplates.VIDEO_CALL_CANCELLED_MESSAGE.getValue());
                smsTemplateCanceled = smsTemplateCanceled.replace(AppConstants.StringReplaceChunks.DATE.getValue(), DateUtils.getFormattedDate(oneToOneSession.getSessionTime() + offsetTime, DateUtils.DATE_FORMAT_ONLY_MONTH));
                smsTemplateCanceled = smsTemplateCanceled.replace(AppConstants.StringReplaceChunks.TIME.getValue(), DateUtils.getFormattedDate(oneToOneSession.getSessionTime() + offsetTime, DateUtils.TIME_FORMAT));
                TwilioMessageUtils.sendSMS(patientPhoneNumber, smsTemplateCanceled);
                //EmailUtils.sendOneToOneSessionEmail(patientUser.getEmail(), "1:1 Session Cancelled", smsTemplateCanceled, false);
            }

            baseDAO.delete(oneToOneSession);
            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="saveMedication")
    public String saveMedication(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            patient = new PatientDAO().getPatientById(patientId);
            Provider provider = HSSessionUtils.getProviderSession(session);

            PatientMedication patientMedication = new PatientMedicationDAO().getPatientMedicationById(medicationMasterIdAJAX);
            if(patientMedication == null){
                patientMedication = new PatientMedication();
                patientMedication.setTimestamp(System.currentTimeMillis());
            }

            patientMedication.setPatient(patient);
            patientMedication.setProvider(provider);

            patientMedication.setDosage(dosageAJAX);
            patientMedication.setMedicationsName(medicationNameAJAX);
            patientMedication.setIsConfirmed(medicationConfirmedAJAX);

            baseDAO.save(patientMedication);

            medicationString = JsonUtil.toJsonExcludedNull(patient.getMedication());
            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.CoachesArea.PATIENT_MEDICATIONS.name(), medicationString);

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="deleteMedication")
    public String deleteMedication(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            boolean result = new PatientDiabDietMedDAO().removeMedicationById(id);
            if(result){
                patient = new PatientDAO().getPatientById(patientId);
                medicationString = JsonUtil.toJsonExcludedNull(patient.getMedication());
                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
                returnMap.put(AppConstants.CoachesArea.PATIENT_MEDICATIONS.name(), medicationString);
            }else{
                returnMap.put(AppConstants.JsonConstants.REASON.name(), "Unable to remove medication. Please try again later.");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="getAllOneToOneSessionsByPatient")
    public String getAllOneToOneSessions(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            jsonData = JsonUtil.toJsonExcludedNull(new OneToOneSessionsDAO().getAllOneToOneSessionsByPatient(patientId));
            String latestOneToOneSession = JsonUtil.toJsonExcludedNull(new OneToOneSessionsDAO().getLatestOneToOneSessionByPatient(patientId));

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.JsonConstants.DATA.name(), jsonData);
            returnMap.put(AppConstants.CoachesArea.LATEST_ONE_TO_ONE_SESSION.name(), latestOneToOneSession);

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="getMyCoaches")
    public String getMyCoaches(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            patient = patientDAO.getPatientById(patientId);

            jsonData = JsonUtil.toJsonExcludedNull(patient.getProviders());

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.JsonConstants.DATA.name(), jsonData);

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="getAllGoalsAndHabits")
    public String getAllGoalsAndHabits(){

        List<GoalSummaryDTO> goalSummaryList = filledFormDAO.getGoalsByPatientId(patientId, -1);
        List<HabitDetailsDTO> habits = filledFormDAO.getHabitsByPatientId(patientId);

        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        //get all lists into id/name pair
        Map<Long, String> allGoalCategories = CommonUtils.listToMap(new GoalCategoryDAO().getAllCategories());
        Map<Long, String> allGoals = CommonUtils.listToMap(new GoalDAO().getAllGoals());
        Map<Long, String> allActionPlans = CommonUtils.listToMap(new ActionPlanDAO().getAllActionPlans());

        List<GoalRatingDTO> ratingDTOs = filledFormDAO.getGoalRatingsByPatient(patientId);

        if(goalSummaryList != null && goalSummaryList.size() > 0){

            for (GoalSummaryDTO goalSummary : goalSummaryList) {

                //gets formData list from DB
                List<FormData> dataList = formDataDAO.getFormWithFilledFormId(goalSummary.getFilledFormId());

                for (FormData formData : dataList) {
                    String fieldName = formData.getFieldName();
                    String fieldValue = formData.getFieldValue();

                    if(fieldName.equalsIgnoreCase(AppConstants.ACTION_PLAN)){
                        goalSummary.setActionPlan(allActionPlans.get(Long.parseLong(fieldValue)));
                    }else if(fieldName.equalsIgnoreCase(AppConstants.GOAL_OPTION)){
                        goalSummary.setGoalName(allGoals.get(Long.parseLong(fieldValue)));
                    }else if(fieldName.equalsIgnoreCase(AppConstants.GOAL_CATEGORY)){
                        goalSummary.setCategoryName(allGoalCategories.get(Long.parseLong(fieldValue)));
                    }
                }

                goalSummary.setFormDataList(dataList);
            }

            if(goalSummaryList != null && goalSummaryList.size() > 0){
                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
                returnMap.put(AppConstants.GOAL, JsonUtil.toJson(goalSummaryList));
                returnMap.put(AppConstants.GOAL_RATINGS, JsonUtil.toJson(ratingDTOs));
            }
        }

        if(habits != null && habits.size() > 0){
            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.HABIT, JsonUtil.toJson(habits));
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="getCoachesAreaDataSet")
    public String getCoachesAreaDataSet(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {
            Provider loggedInProvider = HSSessionUtils.getProviderSession(session);
            patient = patientDAO.getPatientById(patientId);
            coachesListString = JsonUtil.toJsonExcludedNull(new ProviderDAO().list());
            CURRICULUM_API_ADDRESS = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.CURRICULUM_API.getValue());

            ProviderDAO providerDAO = new ProviderDAO();
            Long leadCoachId = ObjectUtils.nullSafeLong(patient.getLeadCoachId());
            Long primaryFoodCoachId = ObjectUtils.nullSafeLong(patient.getPrimaryFoodCoachId());
            //Provider leadCoachProvider = providerDAO.getProviderById(leadCoachId);
            //Provider foodCoachProvider = providerDAO.getProviderById(primaryFoodCoachId);

            List<Provider> filterDeletedCoaches = new ArrayList<Provider>();
            for (Provider provider: patient.getProviders()){
                if(!ObjectUtils.nullSafeBoolean(provider.getIsDeleted())
                        && !ObjectUtils.nullSafeBoolean(provider.getIsAccessAll())
                        && !provider.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.FACILITY_ADMIN.getValue())){
                    filterDeletedCoaches.add(provider);
                }
            }

            String myCoaches = JsonUtil.toJsonExcludedNull(filterDeletedCoaches);
            String oneToOneSessions = JsonUtil.toJsonExcludedNull(new OneToOneSessionsDAO().getAllOneToOneSessionsByPatient(patientId));
            String latestOneToOneSession = JsonUtil.toJsonExcludedNull(new OneToOneSessionsDAO().getLatestOneToOneSessionByPatient(patientId));
            String coachNotes = JsonUtil.toJsonExcludedNull(new CoachNotesDAO().getAllNotesByPatient(patientId));
            String coachLogs = JsonUtil.toJsonExcludedNull(new CoachHBLoggingDAO().getAllHBLogsByPatient(patientId));
            String dietaryNotes = JsonUtil.toJsonExcludedNull(new DietaryNotesDAO().getAllNotesByPatient(patientId));
            String keyFacts = JsonUtil.toJsonExcludedNull(new MemberKeyFactsDAO().getAllMemberKeyFactsByPatient(patientId));
            String patientDetails = JsonUtil.toJson(CommonUtils.convertToPatientDTO(patient));
            medicationsListing = JsonUtil.toJson(new MedicationMasterDAO().list());
            medicationString = JsonUtil.toJsonExcludedNull(patient.getMedication());

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.CoachesArea.COACH_NOTES.name(), coachNotes);
            returnMap.put(AppConstants.CoachesArea.COACH_LOGS.name(), coachLogs);
            returnMap.put(AppConstants.CoachesArea.DIETARY_NOTES.name(), dietaryNotes);
            returnMap.put(AppConstants.CoachesArea.ONE_TO_ONE_SESSIONS.name(), oneToOneSessions);
            returnMap.put(AppConstants.CoachesArea.LATEST_ONE_TO_ONE_SESSION.name(), latestOneToOneSession);
            returnMap.put(AppConstants.CoachesArea.PATIENT_COACHES.name(), myCoaches);
            returnMap.put(AppConstants.CoachesArea.PATIENT_DETAILS.name(), patientDetails);
            returnMap.put(AppConstants.CoachesArea.UUID.name(), patient.getUuid());
            returnMap.put(AppConstants.CoachesArea.CURRICULUM_API_ADDRESS.name(), CURRICULUM_API_ADDRESS);
            returnMap.put(AppConstants.CoachesArea.LEAD_COACH_ID.name(), ObjectUtils.nullSafe(patient.getLeadCoachId()));
            returnMap.put(AppConstants.CoachesArea.PRIMARY_FOOD_COACH_ID.name(), ObjectUtils.nullSafe(patient.getPrimaryFoodCoachId()));
            returnMap.put(AppConstants.CoachesArea.COACH_TYPE.name(), loggedInProvider.getType());
            returnMap.put(AppConstants.CoachesArea.MEDICATIONS_LISTING.name(), medicationsListing);
            returnMap.put(AppConstants.CoachesArea.PATIENT_MEDICATIONS.name(), medicationString);
            returnMap.put(AppConstants.CoachesArea.MEMBER_KEY_FACTS.name(), keyFacts);
            returnMap.put(AppConstants.CoachesArea.PATIENT_SESSION_PREFERENCE.name(), JsonUtil.toJsonExcludedNull(patient.getPatientSessionPreference()));

            long weekNumber = 1;
            PatientCurriculum curriculum = patient.getCurriculum();
            if(curriculum != null){
                weekNumber = curriculum.getWeek();
            }
            returnMap.put(AppConstants.CoachesArea.CURRICULUM_WEEK.name(), weekNumber+"");

            List<Provider> onlyAssignedCoaches = CommonUtils.excludedLeadOrPrimaryFoodCoach(loggedInProvider, patient);

            returnMap.put(AppConstants.CoachesArea.COACHES_LISTING.name(), myCoaches);
            returnMap.put(AppConstants.CoachesArea.EXCLUDED_ME_COACH.name(), JsonUtil.toJsonExcludedNull(onlyAssignedCoaches));

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="assignMemberToMe")
    public String assignMemberToMe() {

        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        try {
            Patient patient = patientDAO.getPatientById(patientId);
            Provider provider = HSSessionUtils.getProviderSession(session);

            if(patient != null){
                patient.setPrimaryFoodCoachId(provider.getProviderId());
            }

            new BaseDAO().save(patient);

            provider = new ProviderDAO().getProviderById(provider.getProviderId());
            NotifyUtils.notifyPatientsOfNewCoach(provider, request);

            coachesListString = JsonUtil.toJsonExcludedNull(new ProviderDAO().list());
            String myCoaches = JsonUtil.toJsonExcludedNull(patient.getProviders());
            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.CoachesArea.PATIENT_COACHES.name(), myCoaches);
            returnMap.put(AppConstants.CoachesArea.LEAD_COACH_ID.name(), ObjectUtils.nullSafe(patient.getLeadCoachId()));
            returnMap.put(AppConstants.CoachesArea.PRIMARY_FOOD_COACH_ID.name(), ObjectUtils.nullSafe(patient.getPrimaryFoodCoachId()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        description = "Saved Primary Food Coach for patient id "+patientId+" "+returnMap.get(AppConstants.JsonConstants.STATUS.name());
        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="saveCurriculum")
    public String saveCurriculum(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            patient = patientDAO.getPatientById(patientId);
            PatientCurriculum patientCurriculum = (PatientCurriculum) JsonUtil.fromJson(curriculumStringAJAX, PatientCurriculum.class);
            patient.setCurriculum(null);
            if(patientCurriculum != null){
                new PatientCurriculumDAO().removeCurriculumByPatientId(patientId);
                patient.setCurriculum(patientCurriculum);
                patientCurriculum.setPatient(patient);
                baseDAO.save(patientCurriculum);
                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="saveMemberSchedulingPreferences")
    public String saveMemberSchedulingPreferences(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            patient = patientDAO.getPatientById(patientId);

            PatientSessionPreference patientSessionPreference = new PatientSessionPreferenceDAO().getSessionPreferenceByPatientId(patient.getPatientId());
            if(patientSessionPreference == null){
                patientSessionPreference = new PatientSessionPreference();
            }

            patientSessionPreference.setDays(sessionDays);
            patientSessionPreference.setTimes(sessionTimes);

            patient.setPatientSessionPreference(patientSessionPreference);
            patientSessionPreference.setPatient(patient);
            baseDAO.save(patientSessionPreference);
            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="getMemberSchedulingPreferences")
    public String getMemberSchedulingPreferences(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            patient = patientDAO.getPatientById(patientId);
            jsonData = JsonUtil.toJsonExcludedNull(patient.getPatientSessionPreference());

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.JsonConstants.DATA.name(), jsonData);

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="getLogEntriesCountAndCurrentWeight")
    public String getLogEntriesCountAndCurrentWeight(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            //gets weekdate from now
            dashboardDateString = DateUtils.getLastWeekDate(new Date()) + " - " + new SimpleDateFormat("MM/dd/yyyy").format(new Date());
            formattedDateString = DateUtils.getFormattedDate(DateUtils.getLastWeekDateFromCurrent(new Date()).getTime(), DateUtils.DATE_FORMAT_DASHBOARD) + " - " + DateUtils.getFormattedDate(System.currentTimeMillis(), DateUtils.DATE_FORMAT_DASHBOARD);

            //log book entries count
            String[] dateParts = dashboardDateString.split("-");
            logEntriesCount = logDAO.getLogbookCount(patientId, dateParts[0], dateParts[1]);
            currentWeight = Math.round(logDAO.getCurrentWeightOfPatient(patientId));

            Map<String, String> dataMap = new LinkedHashMap<String, String>();

            dataMap.put(AppConstants.MiscConstants.LOG_ENTRIES_COUNT.name(), logEntriesCount+"");
            dataMap.put(AppConstants.MiscConstants.CURRENT_WEIGHT.name(), currentWeight+"");
            dataMap.put(AppConstants.MiscConstants.DATE_SPAN.name(), formattedDateString);

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.JsonConstants.DATA.name(), JsonUtil.toJson(dataMap));

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="getMisfitAverageStepsAndBatteryStatus")
    public String getMisfitAverageStepsAndBatteryStatus(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try {

            //gets weekdate from now
            dashboardDateString = DateUtils.getLastWeekDate(new Date()) + " - " + new SimpleDateFormat("MM/dd/yyyy").format(new Date());

            //log book entries count
            String[] dateParts = dashboardDateString.split("-");

            averageSteps = logDAO.getAverageActivityStepsByDate(patientId, dateParts[0], dateParts[1]);
            misfitBatteryStatus = logDAO.getMisfitBatteryStatusByPatient(patientId);

            Map<String, String> dataMap = new LinkedHashMap<String, String>();

            dataMap.put(AppConstants.MiscConstants.AVG_STEPS.name(), averageSteps+"");
            dataMap.put(AppConstants.MiscConstants.MISFIT_BATTERY_STATUS.name(), (misfitBatteryStatus == null)? "" : misfitBatteryStatus+"");

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            returnMap.put(AppConstants.JsonConstants.DATA.name(), JsonUtil.toJson(dataMap));

        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}


    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }


    public String getSmsTemplateList() {
        return smsTemplateList;
    }

    public void setSmsTemplateList(String smsTemplateList) {
        this.smsTemplateList = smsTemplateList;
    }

    public long getMessagesCount() {
        return messagesCount;
    }

    public void setMessagesCount(long messagesCount) {
        this.messagesCount = messagesCount;
    }

    public String getDashboardDateString() {
        return dashboardDateString;
    }

    public void setDashboardDateString(String dashboardDateString) {
        this.dashboardDateString = dashboardDateString;
    }

    public String getFormattedDateString() {
        return formattedDateString;
    }

    public void setFormattedDateString(String formattedDateString) {
        this.formattedDateString = formattedDateString;
    }

    public long getLogEntriesCount() {
        return logEntriesCount;
    }

    public void setLogEntriesCount(long logEntriesCount) {
        this.logEntriesCount = logEntriesCount;
    }

    public long getTargetSteps() {
        return targetSteps;
    }

    public void setTargetSteps(long targetSteps) {
        this.targetSteps = targetSteps;
    }

    public long getAverageSteps() {
        return averageSteps;
    }

    public void setAverageSteps(long averageSteps) {
        this.averageSteps = averageSteps;
    }

    public String getGoalsList() {
        return goalsList;
    }

    public void setGoalsList(String goalsList) {
        this.goalsList = goalsList;
    }

    public String getHabitsList() {
        return habitsList;
    }

    public void setHabitsList(String habitsList) {
        this.habitsList = habitsList;
    }

    public UserActivity getUserActivity() {
        return userActivity;
    }

    public void setUserActivity(UserActivity userActivity) {
        this.userActivity = userActivity;
    }

    public String getOneToOneSessionListString() {
        return oneToOneSessionListString;
    }

    public void setOneToOneSessionListString(String oneToOneSessionListString) {
        this.oneToOneSessionListString = oneToOneSessionListString;
    }

    public String getCoachNotesString() {
        return coachNotesString;
    }

    public void setCoachNotesString(String coachNotesString) {
        this.coachNotesString = coachNotesString;
    }

    public String getCoachNotesAJAX() {
        return coachNotesAJAX;
    }

    public void setCoachNotesAJAX(String coachNotesAJAX) {
        this.coachNotesAJAX = coachNotesAJAX;
    }

    public String getCoachesListString() {
        return coachesListString;
    }

    public void setCoachesListString(String coachesListString) {
        this.coachesListString = coachesListString;
    }

    public String getSessionDateAJAX() {
        return sessionDateAJAX;
    }

    public void setSessionDateAJAX(String sessionDateAJAX) {
        this.sessionDateAJAX = sessionDateAJAX;
    }

    public long getChosenCoachIdAJAX() {
        return chosenCoachIdAJAX;
    }

    public void setChosenCoachIdAJAX(long chosenCoachIdAJAX) {
        this.chosenCoachIdAJAX = chosenCoachIdAJAX;
    }

    public String getMyCoachesString() {
        return myCoachesString;
    }

    public void setMyCoachesString(String myCoachesString) {
        this.myCoachesString = myCoachesString;
    }

    public String getDobFormatted() {
        return dobFormatted;
    }

    public void setDobFormatted(String dobFormatted) {
        this.dobFormatted = dobFormatted;
    }

    public String getDiagnosisDateFormatted() {
        return diagnosisDateFormatted;
    }

    public void setDiagnosisDateFormatted(String diagnosisDateFormatted) {
        this.diagnosisDateFormatted = diagnosisDateFormatted;
    }

    public String getMedicationString() {
        return medicationString;
    }

    public void setMedicationString(String medicationString) {
        this.medicationString = medicationString;
    }

    public int getCurrentWeight() {
        return currentWeight;
    }

    public void setCurrentWeight(int currentWeight) {
        this.currentWeight = currentWeight;
    }

    public int getStartingWeight() {
        return startingWeight;
    }

    public void setStartingWeight(int startingWeight) {
        this.startingWeight = startingWeight;
    }

    public int getGoalWeight() {
        return goalWeight;
    }

    public void setGoalWeight(int goalWeight) {
        this.goalWeight = goalWeight;
    }

    public String getNotifyCoachesListAJAX() {
        return notifyCoachesListAJAX;
    }

    public void setNotifyCoachesListAJAX(String notifyCoachesListAJAX) {
        this.notifyCoachesListAJAX = notifyCoachesListAJAX;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getCurriculumStringAJAX() {
        return curriculumStringAJAX;
    }

    public void setCurriculumStringAJAX(String curriculumStringAJAX) {
        this.curriculumStringAJAX = curriculumStringAJAX;
    }

    public String getDietaryNotesAJAX() {
        return dietaryNotesAJAX;
    }

    public void setDietaryNotesAJAX(String dietaryNotesAJAX) {
        this.dietaryNotesAJAX = dietaryNotesAJAX;
    }

    public String getCURRICULUM_API_ADDRESS() {
        return CURRICULUM_API_ADDRESS;
    }

    public void setCURRICULUM_API_ADDRESS(String CURRICULUM_API_ADDRESS) {
        this.CURRICULUM_API_ADDRESS = CURRICULUM_API_ADDRESS;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public PatientMotivationImage getPatientMotivationImage() {
        return patientMotivationImage;
    }

    public void setPatientMotivationImage(PatientMotivationImage patientMotivationImage) {
        this.patientMotivationImage = patientMotivationImage;
    }

    public String getMedicationsListing() {
        return medicationsListing;
    }

    public void setMedicationsListing(String medicationsListing) {
        this.medicationsListing = medicationsListing;
    }

    public String getMedicationNameAJAX() {
        return medicationNameAJAX;
    }

    public void setMedicationNameAJAX(String medicationNameAJAX) {
        this.medicationNameAJAX = medicationNameAJAX;
    }

    public String getDosageAJAX() {
        return dosageAJAX;
    }

    public void setDosageAJAX(String dosageAJAX) {
        this.dosageAJAX = dosageAJAX;
    }

    public long getMedicationMasterIdAJAX() {
        return medicationMasterIdAJAX;
    }

    public void setMedicationMasterIdAJAX(long medicationMasterIdAJAX) {
        this.medicationMasterIdAJAX = medicationMasterIdAJAX;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean getMedicationConfirmedAJAX() {
        return medicationConfirmedAJAX;
    }

    public void setMedicationConfirmedAJAX(boolean medicationConfirmedAJAX) {
        this.medicationConfirmedAJAX = medicationConfirmedAJAX;
    }

    public String getSessionTimeAJAX() {
        return sessionTimeAJAX;
    }

    public void setSessionTimeAJAX(String sessionTimeAJAX) {
        this.sessionTimeAJAX = sessionTimeAJAX;
    }

    public long getCustomIdAJAX() {
        return customIdAJAX;
    }

    public void setCustomIdAJAX(long customIdAJAX) {
        this.customIdAJAX = customIdAJAX;
    }

    public String getMemberDOB() {
        return memberDOB;
    }

    public void setMemberDOB(String memberDOB) {
        this.memberDOB = memberDOB;
    }

    public String getCustomSMSAJAX() {
        return customSMSAJAX;
    }

    public void setCustomSMSAJAX(String customSMSAJAX) {
        this.customSMSAJAX = customSMSAJAX;
    }


    public String getCustomDataAJAX() {
        return customDataAJAX;
    }

    public void setCustomDataAJAX(String customDataAJAX) {
        this.customDataAJAX = customDataAJAX;
    }

    public long getCustomDataNumber() {
        return customDataNumber;
    }

    public void setCustomDataNumber(long customDataNumber) {
        this.customDataNumber = customDataNumber;
    }

    public String getPhoneVersion() {
        return phoneVersion;
    }

    public void setPhoneVersion(String phoneVersion) {
        this.phoneVersion = phoneVersion;
    }

    public String getSecureLink() {
        return secureLink;
    }

    public void setSecureLink(String secureLink) {
        this.secureLink = secureLink;
    }

    public long getHBloggingDate() {
        return HBloggingDate;
    }

    public void setHBloggingDate(long HBloggingDate) {
        this.HBloggingDate = HBloggingDate;
    }

    public String getSessionDays() {
        return sessionDays;
    }

    public void setSessionDays(String sessionDays) {
        this.sessionDays = sessionDays;
    }

    public String getSessionTimes() {
        return sessionTimes;
    }

    public void setSessionTimes(String sessionTimes) {
        this.sessionTimes = sessionTimes;
    }

    public String getHttpOrHttps() {
        return httpOrHttps;
    }

    public void setHttpOrHttps(String httpOrHttps) {
        this.httpOrHttps = httpOrHttps;
    }

    public String getMeterSerialNumber() {
        return meterSerialNumber;
    }

    public void setMeterSerialNumber(String meterSerialNumber) {
        this.meterSerialNumber = meterSerialNumber;
    }

    public Integer getMisfitBatteryStatus() {
        return misfitBatteryStatus;
    }

    public void setMisfitBatteryStatus(Integer misfitBatteryStatus) {
        this.misfitBatteryStatus = misfitBatteryStatus;
    }
}
