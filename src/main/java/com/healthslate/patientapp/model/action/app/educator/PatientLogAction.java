package com.healthslate.patientapp.model.action.app.educator;

import com.amazonaws.util.json.JSONObject;
import com.google.gson.*;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.DykDTO;
import com.healthslate.patientapp.model.dto.FoodDetailDTO;
import com.healthslate.patientapp.model.dto.LogDTO;
import com.healthslate.patientapp.model.dto.PatientDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import com.healthslate.patientapp.ws.LogServices;
import com.healthslate.patientapp.ws.SocialGroupServices;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import freemarker.template.utility.DateUtil;
import javassist.bytecode.stackmap.BasicBlock;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.AccessDeniedException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Results({
		@Result(name="unProcessedMeals", type="tiles", location="educator/unProcessedMeals.tiles"),
		@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/patients.tiles"),
		@Result(name=AppConstants.REPORT_ERROR, type="tiles", location="educator/reportError.tiles"),
		@Result(name=AppConstants.LOG, type="tiles", location="educator/patientLogs.tiles"),
		@Result(name=AppConstants.LOG_DETAIL, type="tiles", location="educator/patientLogDetail.tiles"),
		@Result(name=AppConstants.LOG_REPORT, type="tiles", location="educator/logReport.tiles"),
		@Result(name=AppConstants.LOG_BOOK, type="tiles", location="educator/logbook.tiles"),
		@Result(name=AppConstants.PATIENT_POPULATION, type="tiles", location="educator/patientPopulation.tiles"),
		@Result(name=AppConstants.AJAX, type="json", params={"root", "nutritionInfoJson"}),
		@Result(name=AppConstants.AJAX_FOOD_MASTER, type="json", params={"root", "foodLogDetailJson"}),
		@Result(name=AppConstants.AJAX_MESSAGE, type="json", params={"root", "actionMessage"}),
		@Result(name=AppConstants.AJAX_NEW_MESSAGES, type="json", params={"root", "newMessagesJson"}),
		@Result(name=AppConstants.AJAX_NUTRI_INFO, type="json", params={"root", "selFoodId"}),
		@Result(name=AppConstants.AJAX_SUCCESS, type="json", params={"root", "logNameUpdateStatus"}),
		@Result(name=AppConstants.NEW_LOG, type="json", params={"root", "maxCount"}),
		@Result(name=AppConstants.AJAX_QUESTION_LIST, type="json", params={"root", "questionListJson"}),
		@Result(name=AppConstants.LOG_REPORT_DATA, type="json", params={"root", "logReportData"}),

		@Result(name="myMeals", type="redirectAction",
				params = {"actionName" , "myMealsPage",
						"patientId", "%{patientId}",
						"mealType","%{log.foodLogSummary.type}"}),

		@Result(name=AppConstants.REDIRECT, type="redirectAction",
				params = {"actionName" , "fetchLogDetail",
						"logId", "%{logId}",
						"nutritionAction","%{nutritionAction}",
						"actionMessage","%{actionMessage}",
						"statusMessage","%{statusMessage}"}),
		@Result(name=AppConstants.REDIRECT_LOGS, type="redirectAction",
				params = {"actionName" , "fetchPatientLogs",
						"logId", "%{logId}",
						"maxCount","%{maxCount}",
						"isAdvSearch","%{isAdvSearch}",
						"patientId","%{patientId}"}),

		@Result(name="ajax", type="stream", params={"contentType", "application/Json", "inputName", "is"}),
		@Result(name="clear", type="tiles", location="educator/patients.tiles"),

		@Result(name="inAppMessages", type="redirectAction",
				params = {"actionName" , "coachPatientMessages",
						"patientId", "%{patientId}"}),
		@Result(name="sharedMeals", type="redirectAction",
		params = {"actionName" , "shareableMeals"})
})

public class PatientLogAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private static final long serialVersionUID = 1L;
	private String AppPath;
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat format2 = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy");

	FoodLogSummaryDAO logSummaryDAO = new FoodLogSummaryDAO();
	FoodLogDetailDAO foodLogDetailDAO = new FoodLogDetailDAO();
	FoodMasterDAO foodMasterDAO = new FoodMasterDAO();
	ReportErrorDAO errorDAO = new ReportErrorDAO();
	ProviderDAO providerDAO = new ProviderDAO();
	PatientDAO patientDAO = new PatientDAO();
	MessageDAO messageDAO = new MessageDAO();
	UserDAO userDAO = new UserDAO();
	LogDAO logDAO = new LogDAO();

	private Map<Integer, String> groupMap = new LinkedHashMap<Integer, String>();
	private Map<String, String> UserMap = new LinkedHashMap<String, String>();
	int GroupIdTogetUser ;
	Long adviceIdToSend;
	float glucoseMin = 0f;
	float glucoseMax = 0f;

	private Map<String, Object> session;
	@SuppressWarnings("unused")
	private Map<String, String> servingUnitMap = new LinkedHashMap<String, String>();
	@SuppressWarnings("unused")
	private Map<Integer, String> statusMap = new LinkedHashMap<Integer, String>();
	@SuppressWarnings("unused")
	private Map<Integer, String> LogTypeMap = new LinkedHashMap<Integer, String>();
	private static Map<String, Integer> logMap = new LinkedHashMap<String, Integer>();

	private List<String> favoriteQuestions;
	private List<String> foodAudioLocations;
	private List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();
	private List<Log> unprocessedPatientLogs;
	private List<Log> processedPatientLogs;
	private List<ReportError> reportErrors;
	private List<Object> errorObjsForLogId;
	private List<Log> patientLogs;
	private List<Object> errorObjs;
	private Log log;

	private Boolean logProcessStatus;
	private Boolean isAdvSearch;
	private Boolean isRoleAdmin = false;
	private Boolean hasMissingFood = false;
	private Boolean favoriteButton = false;

	private String isFromError;
	private String errorDescription;
	private String logsJson;
	private String foodImageLocation;
	private String redirectToPage;

	private String foodImageInputStream;
	private String nutritionInfoJson;
	private String newMessagesJson;
	private String actionMessage;
	private String statusMessage = AppConstants.ServicesConstants.DATA.name();
	private String message;

	private String foodName;
	private String servingSize;
	private String servingUnit;
	private String foodJson;
	private String mealJson ;
	private String foodLogDetailJson ;

	private String totalCarbs = "0";
	private String totalCalories = "0";
	private String totalFats = "0";
	private String totalProtein = "0";
	private String logId;
	private String maxTime;

	private Integer foodCalories;
	private int status = 1;
	private int maxCount;

	private float foodCarbs;
	private float foodFats;
	private float foodProtein;

	private Date dateFrom = ObjectUtils.getSevenDaysBeforeDate();
	private Long createdOn;
	//private Date dateFrom = new Date();
	private Date dateTo = new Date();

	private Long foodId;
	private Long selFoodId;
	private long patientId = 0l;
	private long errorId;

	private String sEcho;
	private Long iTotalRecords = null;
	private Integer iTotalDisplayRecords = null;

	private int basicstatus ;
	private int advstatus  ;
	private int logstatus  ;
	private InputStream is;
	private String jsonString;
	private JsonObject jsonResponse;
	public List<LogDTO> aaData;
	List<DykDTO> dykDTOs =new ArrayList<DykDTO>() ;
	private HttpServletRequest request;
	List<LogDTO> patientlogdto;
	private boolean isAdvanceSearch ;
	private Map<Integer, String> feildLableMap;
	@SuppressWarnings("rawtypes")
	private Map<String ,List > questionMap =new LinkedHashMap<String, List>();
	String questionListJson ;

	int adviceSentStatus ;
	String favoriteQuestion ;

	//Log Name
	private String logName ;
	private String logNameUpdateStatus = "";
	private String foodLogName;
	private String logNameFromFood;
	private Long mealSummaryId ;

	//Log Report
	private String logReportData;
	private Date dailyReportDate = new Date();
	private String dailyReportDateString;
	private String weeklyReportDateString;
	private String monthlyReportDateString;
	private String weeklyLogsDateString;
	private String weeklyTrendDateString;
	private String monthlyTrendDateString;
	private String threeDaysDateString;
	private String fourteenDaysReportDateString;
	private String customDateString;
	private Map<String, Integer> mealCeilings;

	public String description;

	//set total messages variable, __oz
	private int messagesTotalCount;
	private String coachMessagesList;
	private String onFoodImageName;
	private String smsTemplateList;
	private String monthlyReportString;
	private String targetString;

	private String isFromLogbook;
	public String httpOrHttps;
	public int noOfDays;
	private Long startDate;
	private Long endDate;

	private PatientEatingPreference patientEatingPreference;
	Patient patient;
	public String token;
	private String memberPermission;
	private String lastRequestDate;
	private String lastSharedDate;
	private String shareMealNotes;

	public void storeActivePatientInSession() {
		session.put(AppConstants.SessionKeys.ACTIVE_PATIENT.name(), null);
		if(patientId != 0l){
			patient = patientDAO.getPatientById(patientId);
		}
	}


	@Action(value="viewReportErrors")
	public String viewReportErrors() {

		//store Active Patient in session, __oz
		storeActivePatientInSession();
		Provider provider = (Provider) session.get(AppConstants.SessionKeys.PROVIDER.name());
		errorObjs = errorDAO.getErrorsByCoachId(provider.getProviderId());
		description = "View Report Error Page";
		return AppConstants.REPORT_ERROR;
	}

	@Action(value="assignMeAsPrimaryFoodCoach")
	public String assignMeAsPrimaryFoodCoach() {

		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

		//newly created
		try {
			Patient patient = patientDAO.getPatientById(patientId);
			Provider provider = HSSessionUtils.getProviderSession(session);

			if(patient != null){
				patient.setPrimaryFoodCoachId(provider.getProviderId());
			}

			new BaseDAO().save(patient);

			provider = new ProviderDAO().getProviderById(provider.getProviderId());
			NotifyUtils.notifyPatientsOfNewCoach(provider, request);

			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
		} catch (Exception e) {
			e.printStackTrace();
		}
		description = "Saved Primary Food Coach for patient id "+patientId+" "+returnMap.get(AppConstants.JsonConstants.STATUS.name());
		nutritionInfoJson = JsonUtil.toJson(returnMap);
		return AppConstants.AJAX;
	}

	@SuppressWarnings("unused")
	private void populateUserMap(int GroupID) {

		try{
			String response = SocialServicesUtils.getSocialData(AppConstants.SOCIALKEYS.LIST_USER.getValue()+"&"+AppConstants.SOCIALKEYS.GROUP_GUID.getValue()+"="+GroupID);

			JsonElement jelement = new JsonParser().parse(response);
			JsonObject jobject = jelement.getAsJsonObject();

			int statusFromElgg = jobject.get(AppConstants.SOCIALKEYS.STATUS.getValue()).getAsInt();

			if(statusFromElgg == -1)
			{
				UserMap.put("All", AppConstants.SELECT);
			}
			else
			{

				jelement = jobject.get("result");
				JsonArray jsonArray = jelement.getAsJsonArray();
				for(JsonElement element: jsonArray){
					JsonObject object = element.getAsJsonObject();
					UserMap.put(object.get("email").getAsString(), object.get("name").getAsString());
				}
			}

		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value="fetchPatientPopulation")
	public String fetchPatientPopulation() throws ParseException{

		//store Active Patient in session, __oz
		storeActivePatientInSession();

		return AppConstants.PATIENT_POPULATION;
	}

	@Action(value="fetchLogbook")
	public String fetchLogbook() throws ParseException {

		//store Active Patient in session, __oz
		storeActivePatientInSession();

		if(startDate != null && endDate != null){
//			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(DateUtils.getFormattedDate(startTime, "yyyy-MM-dd"));
			weeklyReportDateString = DateUtils.getFormattedDate(startDate, "MM/dd/yyyy")
										+ " - " +
									DateUtils.getFormattedDate(endDate, "MM/dd/yyyy");
		} else {
			weeklyReportDateString = DateUtils.getLastWeekDate(new Date()) + " - " + new SimpleDateFormat("MM/dd/yyyy").format(new Date());
		}

		targetString = StringEscapeUtils.escapeJava(JsonUtil.toJsonExcludedNull(new TargetDAO().getTargetByPatientId(patientId)));
		return AppConstants.LOG_BOOK;
	}

	@Action(value="fetchReportPage")
	public String fetchReportPage() throws ParseException{
		//store Active Patient in session, __ad
		storeActivePatientInSession();
		weeklyTrendDateString   = DateUtils.getLastWeekDate(new Date()) + " - " + new SimpleDateFormat("MM/dd/yyyy").format(new Date());
		weeklyReportDateString  = DateUtils.getLastWeekDate(new Date()) + " - " + new SimpleDateFormat("MM/dd/yyyy").format(new Date());
		monthlyReportDateString = DateUtils.getPreviousMonthDate(new Date()) + " - " + new SimpleDateFormat("MM/dd/yyyy").format(new Date());
		threeDaysDateString = DateUtils.getLastThreeDays(new Date()) + " - "+ new SimpleDateFormat("MM/dd/yyyy").format(new Date());
		fourteenDaysReportDateString = DateUtils.getLastFourteenDays(new Date()) + " - "+ new SimpleDateFormat("MM/dd/yyyy").format(new Date());
		monthlyTrendDateString  = new SimpleDateFormat("M/d/yyyy").format(new Date(DateUtils.getMonthStartTime(System.currentTimeMillis())));
		httpOrHttps = request.getScheme();
		return AppConstants.LOG_REPORT;
	}

	@SuppressWarnings({"rawtypes" })
	@Action(value="populateGlucoseChart")
	public String populateGlucoseChart() throws ParseException{
		/*List & Map for data*/
		Map<String, List> data = new LinkedHashMap<String, List>();
		List<List> beforeMealGlucoseTargets = new ArrayList<List>();

		List<List> glucoseUnderTarget = new ArrayList<List>() ;
		List<List> glucoseAboveTarget = new ArrayList<List>() ;
		List<List> glucoseAtTarget = new ArrayList<List>();
		List<Double> combineGlucoseTarget = new ArrayList<Double>();
		List<List> afterGlucoseTargets = new ArrayList<List>() ;

		Target target = new TargetDAO().getTargetByPatientId(patientId);
		float afterMealGlucoseMin = 0f;
		float afterMealGlucoseMax = 0f;

		if(target != null) {
			glucoseMin = target.getGlucoseMin2();
			glucoseMax = target.getGlucoseMax2();
			afterMealGlucoseMin = target.getGlucoseMin();
			afterMealGlucoseMax = target.getGlucoseMax();
		}

		/*Set MinMax Range*/
		if(!ObjectUtils.isEmpty(dailyReportDateString)){
			String[] dateParts = dailyReportDateString.split("-");
			Long startTime = Long.parseLong(dateParts[0]);
			Long endTime = Long.parseLong(dateParts[1]);
			List<Integer> totalTests = new ArrayList<Integer>();
			List<Integer> avgBGL = new ArrayList<Integer>();
			Integer totalBeforeMealBGL = 0, totalAfterMealBGL = 0;

			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(DateUtils.getFormattedDate(startTime, "yyyy-MM-dd"));
//			List<Object> minMaxRang = getRange(dateToGetChart);
			List<Long> minMaxRang = getRange(dateToGetChart, 2);

			List<Object> glucoseValueList = logDAO.getGlucoseValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> glucoseBeforeMealValueList = logDAO.getGlucoseBeforeMealValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> glucoseAfterMealValueList = logDAO.getGlucoseAfterMealValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> stdBeforeMeal = logDAO.getGlucoseBeforeMealSTDPerDayByPatientId(patientId, startTime, endTime);
			List<Object> stdAfterMeal = logDAO.getGlucoseAfterMealSTDPerDayByPatientId(patientId, startTime, endTime);
			totalBeforeMealBGL = getTotalBGLFromList(glucoseBeforeMealValueList);
			totalAfterMealBGL = getTotalBGLFromList(glucoseAfterMealValueList);

			// Further Calculations / List due to json map get list
			totalTests.add(glucoseBeforeMealValueList.size());
			totalTests.add(glucoseAfterMealValueList.size());
			avgBGL.add(totalBeforeMealBGL / (totalTests.get(0) != 0 ? totalTests.get(0) : 1));
			avgBGL.add(totalAfterMealBGL / (totalTests.get(1) != 0 ? totalTests.get(1) : 1));
			List<Double> avgPrePostList = getAveragePrePostBGL(glucoseValueList);

			for(Object obj: glucoseValueList){
				List<Object> valuePair = new ArrayList<Object>() ;
				Object[] objArray = (Object[]) obj;
				Log log = (Log) objArray[0];
				long logCreatedTime = log.getCreatedOn();
				double GlucoseFloatValue = Double.parseDouble((String) objArray[1]);
				String glucoseMealTime = objArray[4].toString();
				long glTimeStamp = Long.parseLong(objArray[2].toString());

				valuePair.add(logCreatedTime);
				valuePair.add(GlucoseFloatValue) ;
				valuePair.add(glucoseMealTime);
				valuePair.add(log.getLogId());
				valuePair.add(glTimeStamp);

				String glucoseTime = ObjectUtils.nullSafe(objArray[4]);
				if(ObjectUtils.isEmpty(glucoseTime) || glucoseTime.equalsIgnoreCase(AppConstants.GLUCOSE_BEFORE_MEAL)) {
					if(GlucoseFloatValue < glucoseMin) {
						glucoseUnderTarget.add(valuePair);
					} else if(GlucoseFloatValue > glucoseMax) {
						glucoseAboveTarget.add(valuePair) ;
					} else {
						glucoseAtTarget.add(valuePair) ;
					}
				} else {
					if(GlucoseFloatValue < afterMealGlucoseMin) {
						glucoseUnderTarget.add(valuePair);
					} else if(GlucoseFloatValue > afterMealGlucoseMax) {
						glucoseAboveTarget.add(valuePair) ;
					} else {
						glucoseAtTarget.add(valuePair) ;
					}
				}
				combineGlucoseTarget.add(GlucoseFloatValue);
			}

			List<Object> beforeGlucoseTargetsObjects = new ArrayList<Object>();
			beforeGlucoseTargetsObjects.add(glucoseMin);
			beforeGlucoseTargetsObjects.add(glucoseMax);
			beforeMealGlucoseTargets.add(beforeGlucoseTargetsObjects);

			List<Object> afterMealeTargetsObjects = new ArrayList<Object>();
			afterMealeTargetsObjects.add(afterMealGlucoseMin);
			afterMealeTargetsObjects.add(afterMealGlucoseMax);
			afterGlucoseTargets.add(afterMealeTargetsObjects);

			data.put("High", glucoseAboveTarget) ;
			data.put("Low", glucoseUnderTarget) ;
			data.put("Normal", glucoseAtTarget) ;
			data.put("beforeMealGlucoseTargets", beforeMealGlucoseTargets);
			data.put("afterMealGlucoseTargets", afterGlucoseTargets);
			data.put("maxYAxis", combineGlucoseTarget);
			data.put("MinMax", minMaxRang);
			data.put("TotalTests", totalTests);
			data.put("AvgBGL", avgBGL);
			data.put("AvgPrePostBGL", avgPrePostList);
			data.put("stdBeforMeal", stdBeforeMeal);
			data.put("stdAfterMeal", stdAfterMeal);
			logReportData = JsonUtil.toJson(data);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	public List<Object> getRange(Date dateToGetChart) throws ParseException{
		List<Object> MinMaxRang = new ArrayList<Object>() ;
		MinMaxRang.add(dateToGetChart.getTime() + CommonUtils.getOffset()) ;
		Date NextDay =new Date();
		NextDay.setTime(dateToGetChart.getTime()  + AppConstants.MILLI_SECOND_IN_DAY + CommonUtils.getOffset()) ;
		MinMaxRang.add(NextDay.getTime()) ;

		return MinMaxRang;
	}

	public List<Long> getRange(Date dateToGetChart, int days) throws ParseException{
		List<Long> minMaxRang = new ArrayList<Long>() ;
		minMaxRang.add(dateToGetChart.getTime()) ; //first date
		Date nextDay = dateToGetChart;
		for(int index = 1; index < days; index++){
			nextDay.setTime(nextDay.getTime()  + AppConstants.MILLI_SECOND_IN_DAY) ;
			minMaxRang.add(nextDay.getTime()) ;
		}
		return minMaxRang;
	}

	public List<Long> getRange(Date dateToGetChart, int days, long patientId) throws ParseException{
		Patient patient = new PatientDAO().getPatientById(patientId);
		long offset = CommonUtils.getPatientFacilityTimezoneOffset(patient);
		List<Long> minMaxRang = new ArrayList<Long>() ;
		minMaxRang.add(dateToGetChart.getTime()) ; //first date
		Date nextDay = dateToGetChart;
		for(int index = 1; index < days; index++){
			nextDay.setTime(nextDay.getTime()  + AppConstants.MILLI_SECOND_IN_DAY) ;
			minMaxRang.add(nextDay.getTime()) ;
		}
		return minMaxRang;
	}

	@SuppressWarnings("rawtypes")
	@Action(value="populateCarbsChart")
	public String populateCarbsChart() throws ParseException{
		Float snackCarbTarget = 0f;
		Float breakfastCarbTarget = 0f;
		Float lunchCarbTarget = 0f;
		Float dinnerCarbTarget = 0f;
		Float beverageCarbTarget = 0f;

		/*List & Map for data*/
		List<List> carbsNormal = new ArrayList<List>();
		List<List> carbsAboveCeiling = new ArrayList<List>();
		List<List> hasMissingFoodSeries = new ArrayList<List>();
		Target target = new TargetDAO().getTargetByPatientId(patientId);
		if(target != null) {
			for (TargetMeal targetMeal : target.getMealTargets()) {
				if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.BREAKFAST.getValue())) {
					breakfastCarbTarget = targetMeal.getCarbTarget();
				} else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.SNACK.getValue())) {
					snackCarbTarget = targetMeal.getCarbTarget();
				} else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.LUNCH.getValue())) {
					lunchCarbTarget = targetMeal.getCarbTarget();
				} else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.DINNER.getValue())) {
					dinnerCarbTarget = targetMeal.getCarbTarget();
				} else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.BEVERAGE.getValue())) {
					beverageCarbTarget = targetMeal.getCarbTarget();
				}
			}
		}

		Map<String, List> data = new LinkedHashMap<String, List>();

		/*Set MinMax Range*/
		if(dailyReportDateString != null && !dailyReportDateString.isEmpty()){

			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(CommonUtils.getFormatedString(dailyReportDateString));

			List<Object> MinMaxRang = getRange(dateToGetChart);

			Long startTime = null;
			Long endTime = null;

			if(dateToGetChart != null) {
				startTime = DateUtils.getStartOfDayTime(dateToGetChart).getTime();
				endTime = DateUtils.getEndOfDayTime(dateToGetChart).getTime();
			}

			/*get Records from DB*/
			List<Object> carbsValueList = logDAO.getCarbsValuesPerDayByPatientId(patientId, startTime, endTime) ;

			for(Object obj: carbsValueList){
				List<Object> valuePair = new ArrayList<Object>() ;
				Object[] objArray = (Object[]) obj;

				boolean hasMissingFood = (Boolean)objArray[4] == null? false: (Boolean)objArray[4];

				int carbs = 0;

				try {
					carbs = ((Float) objArray[1]).intValue();
				} catch (Exception e) {
					e.printStackTrace();
				}
				Long offset = ((Log)objArray[0]).getLogTimeOffset();
				valuePair.add((Long)objArray[3] + offset);
				valuePair.add(carbs);

				if(hasMissingFood) {
					valuePair.add(carbs + "+") ;
					hasMissingFoodSeries.add(valuePair);
				}

				if(carbs != 0){
					if(String.valueOf(objArray[2]).equalsIgnoreCase(AppConstants.TYPEKEYS.SNACK.getValue())) {
						if(carbs < snackCarbTarget)
						{
							if(!hasMissingFood) {
								carbsNormal.add(valuePair) ;
							}
						}
						else if (carbs >= snackCarbTarget)
						{
							if(!hasMissingFood) {
								carbsAboveCeiling.add(valuePair) ;
							}
						}
					} else if(String.valueOf(objArray[2]).equalsIgnoreCase(AppConstants.TYPEKEYS.BREAKFAST.getValue())) {
						if(carbs < breakfastCarbTarget)
						{
							if(!hasMissingFood) {
								carbsNormal.add(valuePair) ;
							}
						}
						else if (carbs >= breakfastCarbTarget)
						{
							if(!hasMissingFood) {
								carbsAboveCeiling.add(valuePair) ;
							}
						}
					} else if(String.valueOf(objArray[2]).equalsIgnoreCase(AppConstants.TYPEKEYS.LUNCH.getValue())) {
						if(carbs < lunchCarbTarget)
						{
							if(!hasMissingFood) {
								carbsNormal.add(valuePair) ;
							}
						}
						else if (carbs >= lunchCarbTarget)
						{
							if(!hasMissingFood) {
								carbsAboveCeiling.add(valuePair) ;
							}
						}
					} else if(String.valueOf(objArray[2]).equalsIgnoreCase(AppConstants.TYPEKEYS.DINNER.getValue())) {
						if(carbs < dinnerCarbTarget)
						{
							if(!hasMissingFood) {
								carbsNormal.add(valuePair) ;
							}
						}
						else if (carbs >= dinnerCarbTarget)
						{
							if(!hasMissingFood) {
								carbsAboveCeiling.add(valuePair) ;
							}
						}
					} else if(String.valueOf(objArray[2]).equalsIgnoreCase(AppConstants.TYPEKEYS.BEVERAGE.getValue())) {
						if(carbs < beverageCarbTarget)
						{
							if(!hasMissingFood) {
								carbsNormal.add(valuePair) ;
							}
						}
						else if (carbs >= beverageCarbTarget)
						{
							if(!hasMissingFood) {
								carbsAboveCeiling.add(valuePair) ;
							}
						}
					}
				}
			}

			/*set dataMap*/
			data.put("High", carbsAboveCeiling);
			data.put("Normal", carbsNormal);
			data.put("HasMissingFood", hasMissingFoodSeries);
			data.put("MinMax", MinMaxRang);

			//[2/9/2015]: added max targets to
			// bar in charts, __oz
			List<Float> bList = new ArrayList<Float>();
			bList.add(breakfastCarbTarget);

			List<Float> lList = new ArrayList<Float>();
			lList.add(lunchCarbTarget);

			List<Float> sList = new ArrayList<Float>();
			sList.add(snackCarbTarget);

			List<Float> dList = new ArrayList<Float>();
			dList.add(dinnerCarbTarget);

			data.put(AppConstants.TYPEKEYS.BREAKFAST.getValue(), bList);
			data.put(AppConstants.TYPEKEYS.LUNCH.getValue(), lList);
			data.put(AppConstants.TYPEKEYS.SNACK.getValue(), sList);
			data.put(AppConstants.TYPEKEYS.DINNER.getValue(), dList);

			logReportData = JsonUtil.toJson(data);
		}

		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value = "fetchDailyCombineActivityData")
	public String fetchDailyCombineActivityData() throws ParseException {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(!ObjectUtils.isEmpty(dailyReportDateString)) {
			String[] dateParts = dailyReportDateString.split("-");
			Long dateFrom = Long.parseLong(dateParts[0]);
			Long dateTo = Long.parseLong(dateParts[1]);
			String formattedDateString = DateUtils.getFormattedDate(dateFrom, "yyyy-MM-dd");
			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(formattedDateString);
			List<Long> minMaxRang = getRange(dateToGetChart, 2);

			List<Object> misfitHourlyLogs = logDAO.getMisfitActivityHourlyLogsByPatientId(patientId, dateFrom, dateTo);
//			misfitHourlyLogs = CommonUtils.addFacilityTimeOffset(misfitHourlyLogs, patientId);
			List<Object> misfitHourlySumLogs = logDAO.getMisfitActivityHourlyLogsSumByPatientId(patientId, dateFrom, dateTo);
//			misfitHourlySumLogs = CommonUtils.addFacilityTimeOffset(misfitHourlySumLogs, patientId);

			if(misfitHourlyLogs != null) {
				misfitHourlyLogs = getActivityLogsDataListDailyThreeDays(misfitHourlyLogs, patientId);
			}

			dataMap.put(AppConstants.ServicesConstants.DATA.name(), misfitHourlyLogs);
			dataMap.put(AppConstants.ChartServiceConstants.STEPS.name(), misfitHourlySumLogs);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMaxRang);
			dataMap.put(AppConstants.ServicesConstants.MAXYAXIS.name(), getMaxXAxisForChart(misfitHourlyLogs, "daily"));
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value = "fetchDailyCombineActivityMinutesData")
	public String fetchDailyCombineActivityMinutesData() throws ParseException {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(!ObjectUtils.isEmpty(dailyReportDateString)) {
			String[] dateParts = dailyReportDateString.split("-");
			Long dateFrom = Long.parseLong(dateParts[0]);
			Long dateTo = Long.parseLong(dateParts[1]);
			String formattedDateString = DateUtils.getFormattedDate(dateFrom, "yyyy-MM-dd");
			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(formattedDateString);
			List<Long> minMaxRang = getRange(dateToGetChart, 2);

			List<ActivityLog> activityLogList = logDAO.getMinutesActivityDetailData(patientId, dateFrom, dateTo);
//			activityLogList = CommonUtils.addFacilityTimeOffsetInActivityLog(activityLogList, patientId);

			dataMap.put(AppConstants.ServicesConstants.DATA.name(), activityLogList);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMaxRang);
			logReportData = JsonUtil.toJsonExcludedNull(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="findNewLogs")
	public String findNewLogs(){

		int count = logDAO.getLogsCount(patientId);

		if(maxCount < count){
			maxCount = count;
		}

		return AppConstants.NEW_LOG;
	}

	@Action(value="findNewUnProcessedMeals1")
	public String findNewUnProcessedMeals1(){
		//exception comed here ,get excuted when app runs first time
		Provider provider = (Provider) session.get(AppConstants.SessionKeys.PROVIDER.name());
		provider = providerDAO.getProviderById(provider.getProviderId());

		int count = logDAO.getLogsCountofLastFourHours(provider.getProviderId());

		if(maxCount < count){
			maxCount = count;
		}

		return AppConstants.NEW_LOG;
	}

	@Action(value="fetchPatientLogs")
	public String fetchPatientLogs() throws ParseException {
		dateFrom = (Date) session.get(AppConstants.SessionKeys.DATE_FROM.name());
		dateTo = (Date) session.get(AppConstants.SessionKeys.DATE_TO.name());
		//store Active Patient in session, __oz
		storeActivePatientInSession();

		if(!ObjectUtils.isEmpty(redirectToPage) && redirectToPage.equalsIgnoreCase("myMeals")){

			Object logIdFromRequest = request.getParameter(AppConstants.JsonKeys.LOG_ID.getValue());
			log = logDAO.getLogById(logIdFromRequest+"");

			return "myMeals";
		}else if(!ObjectUtils.isEmpty(redirectToPage) && redirectToPage.equalsIgnoreCase("inAppMessages")){
			return "inAppMessages";
		}else if(!ObjectUtils.isEmpty(redirectToPage) && redirectToPage.equalsIgnoreCase("shareableMeals")){
			return "sharedMeals";
		}

		return fetchLogs();
	}

	@Action(value="fetchLogs")
	public String fetchLogs() throws ParseException {

		//store Active Patient in session, __oz
		storeActivePatientInSession();

		/*setDates();*/
		maxCount = logDAO.getLogsCount(patientId);

		basicstatus = 1;
		advstatus = 1;
		description = "View Logs";

		if(!ObjectUtils.isEmpty(redirectToPage) && redirectToPage.equalsIgnoreCase("fetchLogbook")){
			return fetchLogbook();
		}

		return redirectToUnprocessedOrOtherPage(AppConstants.LOG);
	}

	public int getUnprocessedMealsCount() {
		Provider provider = (Provider) session.get(AppConstants.SessionKeys.PROVIDER.name());
		provider = providerDAO.getProviderById(provider.getProviderId());
		int count = logDAO.getLogsCountofLastFourHours(provider.getProviderId());
		return count;
	}

	@Action(value="unProcessedMeals")
	public String unProcessedMeals() {
		session.put(AppConstants.SessionKeys.ACTIVE_PATIENT.name(), null);
		return "unProcessedMeals";
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Action(value="fetchUnProcessedMeals")
	public String fetchUnProcessedMeals()throws IOException {

		Provider provider = (Provider) session.get(AppConstants.SessionKeys.PROVIDER.name());
		provider = providerDAO.getProviderById(provider.getProviderId());

		aaData = new ArrayList();

		JQueryDataTableParamModel param = DataTablesParamUtility.getParam(request);
		sEcho = param.sEcho;
		Long iTotalRecordsInt; // total number of records (un-filtered)
		int iTotalDisplayRecordsInt;

		populateUnProcessedFieldLabelMap();
		String sortIndex=feildLableMap.get(param.iSortColumnIndex);

		patientlogdto = logDAO.getUnProcessedMealsList(provider.getProviderId(),param.sSearch,param.iDisplayStart,param.iDisplayLength,sortIndex,param.sSortDirection);

		sEcho = param.sEcho;
		aaData = patientlogdto;
		iTotalDisplayRecordsInt = aaData.size();
		iTotalRecordsInt =  logDAO.getUnProcessedMealsListCount(provider.getProviderId() ,param.sSearch);
		iTotalRecords = iTotalRecordsInt;
		iTotalDisplayRecords = iTotalDisplayRecordsInt;

		//iTotalRecordsInt =
		JsonObject jsonResponse = new JsonObject();
		jsonResponse.addProperty("sEcho", sEcho);
		jsonResponse.addProperty("iTotalRecords", iTotalRecords);
		jsonResponse.addProperty("iTotalDisplayRecords", iTotalRecords);

		Gson gson = new Gson();
		jsonResponse.add("aaData", gson.toJsonTree(aaData));
		is = new ByteArrayInputStream(jsonResponse.toString().getBytes());

		return "ajax";
	}

	private void populateUnProcessedFieldLabelMap(){
		feildLableMap = new LinkedHashMap<Integer, String>();
		feildLableMap.put(0, "l.log_time");
		feildLableMap.put(1, "u.display_name");
		feildLableMap.put(2, "fls.type");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Action(value="fetchLogsfilter")
	public String fetchLogByfilters()throws IOException {
		/*session.put(AppConstants.SessionKeys.DATE_FROM.name(), dateFrom);
		session.put(AppConstants.SessionKeys.DATE_TO.name(), dateTo);*/
		setDates();
		aaData = new ArrayList();
		maxCount = logDAO.getLogsCount(patientId);

		JQueryDataTableParamModel param = DataTablesParamUtility.getParam(request);
		sEcho = param.sEcho;
		Long iTotalRecordsInt; // total number of records (un-filtered)
		int iTotalDisplayRecordsInt;

		iTotalRecordsInt = logDAO.getLogsCountByStatusAndDate(patientId,isAdvSearch,advstatus,basicstatus,logstatus,param.sSearch,dateFrom,dateTo);

		populateFeildLableMap();
		String sortIndex=feildLableMap.get(param.iSortColumnIndex);

		patientlogdto = logDAO.getLogsByFilters(patientId,isAdvSearch,advstatus,basicstatus,logstatus,param.sSearch,param.iDisplayStart,param.iDisplayLength,sortIndex,param.sSortDirection,dateFrom,dateTo);

		sEcho = param.sEcho;
		aaData = patientlogdto;
		iTotalDisplayRecordsInt = aaData.size();
		iTotalRecords = iTotalRecordsInt;
		iTotalDisplayRecords = iTotalDisplayRecordsInt;

		//iTotalRecordsInt =
		JsonObject jsonResponse = new JsonObject();
		jsonResponse.addProperty("sEcho", sEcho);
		jsonResponse.addProperty("iTotalRecords", iTotalRecords);
		jsonResponse.addProperty("iTotalDisplayRecords", iTotalRecords);

		Gson gson = new Gson();
		jsonResponse.add("aaData", gson.toJsonTree(aaData));
		is = new ByteArrayInputStream(jsonResponse.toString().getBytes());

		return "ajax";
	}


	private void populateFeildLableMap(){

		feildLableMap = new LinkedHashMap<Integer, String>();

		feildLableMap.put(0, "l.logId");
		feildLableMap.put(1, "l.logTime");
		feildLableMap.put(2, "m.weight");
		feildLableMap.put(3, "m.bloodPressureSystolic");
		feildLableMap.put(4, "el.type");
		feildLableMap.put(5, "el.type");
		feildLableMap.put(6, "fs.carbs");
		feildLableMap.put(7, "fs.type");
		feildLableMap.put(8, "l.isProcessed");
	}

	@Action(value="setDates")
	public void setDates(){
		session.put(AppConstants.SessionKeys.DATE_FROM.name(), dateFrom);
		session.put(AppConstants.SessionKeys.DATE_TO.name(), dateTo);
	}

	@Action(value="setSearchStatus")
	public void setSearchStatus(){
		session.put(AppConstants.SessionKeys.ADV_SEARCH.name(), isAdvSearch);
	}

	public static int getunreadMessagesCount(String logId) {

		if(logMap != null && logMap.containsKey(logId)){
			return logMap.get(logId);
		}
		return 0;
	}

	//get the messages count by logID, __oz
	private int getMessageCount(String logID) {
		int count = 0;
		List<Object> messages = logDAO.getNewMessages(logID);
		if(messages != null && messages.size() > 0) {
			count = messages.size();
		}

		return count;
	}

	@Action(value="fetchLogDetail")    //////////////////////////////////////
	public String fetchLogDetail() throws IOException{

		//its for, when open meal error detail from email then decrypt token to member/patient Id
		if(!ObjectUtils.isEmpty(token)){
			try {
				token = new EncryptionUtils().decrypt(token);
				patientId = Long.parseLong(token);
			} catch (Exception e) {
				e.printStackTrace();
				try {
					throw new AccessDeniedException(AppConstants.ACCESS_DENIED);
				} catch (AccessDeniedException e1) {
					e1.printStackTrace();
				}
			}
		}

		//store Active Patient in session, __oz
		storeActivePatientInSession();

		//put date in session here
		populateProcessedMealName();

		AppPath = this.request.getContextPath();
		List<Log> logs = new ArrayList<Log>();

		if(!ObjectUtils.isEmpty(logId)){

			Target target = new TargetDAO().getTargetByPatientId(patientId);
			mealCeilings = CommonUtils.getMealTargetsWithKeys(target);
			targetString = JsonUtil.toJson(mealCeilings);

			log = logDAO.getLogById(logId);

			ShareMealPermissions shareMealPermissions = new ShareMealPermissionsDAO().getShareMealPermissionsByLogId(log.getLogId());
			if(shareMealPermissions != null){
				memberPermission = shareMealPermissions.getPermissions();
				lastRequestDate = ObjectUtils.nullSafe(shareMealPermissions.getPermissionsAskedDate());
				lastSharedDate = ObjectUtils.nullSafe(shareMealPermissions.getMealSharedDate());
				shareMealNotes = ObjectUtils.nullSafe(shareMealPermissions.getNotes());
			}

			patientEatingPreference = log.getPatient().getPatientEatingPreference();

			//	logNameFromFood = log.getLogName();
			if(log != null && log.getIsRemoved() == false){

				//get unique log child
				logs.add(log);
				logs = CommonUtils.getUniqueResults(logs);
				log = logs.get(0);

				logProcessStatus = log.getIsProcessed();
				createdOn = log.getLogTime();
				FoodLogSummary foodLogSummary = log.getFoodLogSummary();

				if(foodLogSummary != null){

					//get totals
					totalCarbs = CommonUtils.getCalculatedSize(String.valueOf(foodLogSummary.getCarbs()), "");
					totalFats = CommonUtils.getCalculatedSize(String.valueOf(foodLogSummary.getFats()), "");
					totalProtein = CommonUtils.getCalculatedSize(String.valueOf(foodLogSummary.getProtein()), "");
					totalCalories = CommonUtils.getCalculatedSize(String.valueOf(foodLogSummary.getCalories()), "");

					//get food image
					if(foodLogSummary != null &&
							foodLogSummary.getHasImage() != null &&
							foodLogSummary.getHasImage() &&
							foodLogSummary.getFoodImage() != null &&
							foodLogSummary.getFoodImage().getImageName() != null){

						try {
							foodImageLocation = AppPath + AppConstants.LOG_IMAGE_FOLDER + foodLogSummary.getFoodImage().getImageName();
							onFoodImageName = foodLogSummary.getFoodImage().getImageName();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					//get food audio
					if(foodLogSummary != null &&
							foodLogSummary.getFoodAudios() != null &&
							foodLogSummary.getFoodAudios().size() > 0){

						foodAudioLocations = new ArrayList<String>();
						for(FoodAudio audio: foodLogSummary.getFoodAudios()){
							try {
								foodAudioLocations.add(AppPath + AppConstants.LOG_AUDIO_FOLDER + audio.getAudioName());
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
					logNameFromFood = foodLogSummary.getMealName();

					List<FoodLogDetail> foodLogDetail = log.getFoodLogSummary().getFoodLogDetails();
					if(foodLogDetail != null && foodLogDetail.size() > 0){
						for(FoodLogDetail detail: foodLogDetail){
							Hibernate.initialize(detail);
							FoodMaster detailedFm = detail.getFoodMaster();
							Hibernate.initialize(detailedFm);
							if(detailedFm instanceof HibernateProxy) {
								HibernateProxy proxy = (HibernateProxy)detailedFm;
								FoodMaster master = (FoodMaster)proxy.getHibernateLazyInitializer().getImplementation();
								detail.setFoodMaster(master);
							}
						}
					}
				}

				if(log.getPatient() != null){
					int years = getYear(log.getPatient().getDob());
					if(years > 0){
						log.getPatient().setAge(years + " yrs");
					} else {
						log.getPatient().setAge("N/A");
					}
				}
			}
			errorObjsForLogId = errorDAO.getErrorsByLogId(logId);
		}

		populateFoodNames();

		Provider provider = (Provider) session.get(AppConstants.SessionKeys.PROVIDER.name());
		favoriteQuestions = messageDAO.getFavoriteQuestions(provider.getProviderId());
		if ( favoriteQuestions.size() > 0 ) {
			favoriteButton = true;
		}

		return AppConstants.LOG_DETAIL;
	}

	//make json of existing foods
	private void populateFoodNames() {
		Map<Long, String> foodNamesMap = new LinkedHashMap<Long, String>();
		List<FoodMaster> foodMasters = foodMasterDAO.getFoodMasters();
		for(FoodMaster foodMaster: foodMasters){
			foodNamesMap.put(foodMaster.getFoodMasterId(), foodMaster.getFoodName());
		}

		foodJson = JsonUtil.toJson(foodNamesMap);
	}
	/*make jason of meal name of processed log*/
	private void populateProcessedMealName() {
		Map<Long, String> mealNameMap = new LinkedHashMap<Long, String>();
		Provider provider = (Provider) session.get(AppConstants.SessionKeys.PROVIDER.name());
		provider = providerDAO.getProviderById(provider.getProviderId());
		List<Object> countObjects = logDAO.getMealNameOfProcessedMealLogs(provider.getProviderId());
		for(Object obj: countObjects){
			Object[] objArray = (Object[]) obj;
			mealNameMap.put(((BigInteger)objArray[3]).longValue(), (String)objArray[4]);
		}
		mealJson = JsonUtil.toJson(mealNameMap);
	}

	private int getYear(Date dob){
		SimpleDateFormat simpleDateformat=new SimpleDateFormat("yyyy");

		if(dob != null){
			int yearOfDateOfBirth = Integer.parseInt(simpleDateformat.format(dob));
			int yearOfTodaysDate = Integer.parseInt(simpleDateformat.format(new Date()));
			return yearOfTodaysDate - yearOfDateOfBirth;
		}

		return 0;
	}
	//make json for food selected from suggestion in add food dialog
	@Action(value="getFoodNutritionInfo")
	public String getFoodNutritionInfo(){

		if(foodId != null && foodId != 0){
			FoodMaster foodMaster = foodMasterDAO.getFoodMasterById(foodId);

			if(foodMaster != null){
				Map<String, String> nutritionInfoMap = new LinkedHashMap<String, String>();
				nutritionInfoMap.put(AppConstants.NutritionInfoKeys.CARBS.name(), String.valueOf(foodMaster.getCarbs()));
				nutritionInfoMap.put(AppConstants.NutritionInfoKeys.FATS.name(), String.valueOf(foodMaster.getFats()));
				nutritionInfoMap.put(AppConstants.NutritionInfoKeys.PROTEIN.name(), String.valueOf(foodMaster.getProtein()));
				nutritionInfoMap.put(AppConstants.NutritionInfoKeys.FOOD_UNIT.name(), foodMaster.getServingSizeUnit());

				nutritionInfoJson = new Gson().toJson(nutritionInfoMap);
			}
		}

		return AppConstants.AJAX;
	}

	@Action(value="getFoodNutritionInfoEdit")
	public String getFoodNutritionInfoEdit(){

		if(foodId != null && foodId != 0){
			FoodMaster foodMaster = foodMasterDAO.getFoodMasterById(foodId);
			Float noOfServings = foodLogDetailDAO.getDetailByIdAndFoodMasterId(logId, foodId);

			if(foodMaster != null){
				Map<String, String> nutritionInfoMap = new LinkedHashMap<String, String>();
				nutritionInfoMap.put(AppConstants.NutritionInfoKeys.CARBS.name(), String.valueOf(foodMaster.getCarbs()));
				nutritionInfoMap.put(AppConstants.NutritionInfoKeys.FATS.name(), String.valueOf(foodMaster.getFats()));
				nutritionInfoMap.put(AppConstants.NutritionInfoKeys.PROTEIN.name(), String.valueOf(foodMaster.getProtein()));
				nutritionInfoMap.put(AppConstants.NutritionInfoKeys.FOOD_UNIT.name(), foodMaster.getServingSizeUnit());
				nutritionInfoMap.put(AppConstants.NutritionInfoKeys.SERVING_SIZE.name(), ((noOfServings % 1) > 0 ? noOfServings+"" : noOfServings.intValue()+""));

				nutritionInfoJson = new Gson().toJson(nutritionInfoMap);
			}
		}

		return AppConstants.AJAX;
	}

	//send nutrition info to patient and change status of log to processed
	@Action(value="sendFoodNutritionInfo")
	public String sendFoodNutritionInfo() throws ParseException {
		description = "Send Nutrition Info ";
		statusMessage = AppConstants.ServicesConstants.DATA.name();

		if(!ObjectUtils.isEmpty(logId)){

			log = logDAO.getLogById(logId);
			Patient loggedPatient = log.getPatient();

			if(processLog(log)){

				FoodLogSummary foodLogSummary = log.getFoodLogSummary();

				if(log != null
						&& foodLogSummary != null
						&& foodLogSummary.getFoodLogDetails() != null
						&& foodLogSummary.getFoodLogDetails().size() > 0) {

					FoodLogSummary summary = logSummaryDAO.getSummaryById(foodLogSummary.getFoodLogSummaryId());

					for(FoodLogDetail detail: summary.getFoodLogDetails()){
						Hibernate.initialize(detail);
						FoodMaster detailedFm = detail.getFoodMaster();
						Hibernate.initialize(detailedFm);
						if(detailedFm instanceof HibernateProxy) {
							HibernateProxy proxy = (HibernateProxy)detailedFm;
							FoodMaster master = (FoodMaster)proxy.getHibernateLazyInitializer().getImplementation();
							detail.setFoodMaster(master);
						}
					}

					summary.setMealName(foodLogName);
					summary.setHasMissingFood(hasMissingFood);

					Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
					dataMap.put(AppConstants.GCMKEYS.DATA.getValue(), summary);
					dataMap.put(AppConstants.GCMKEYS.LOG_ID.getValue(), log.getLogId());

					DeviceDAO deviceDao = new DeviceDAO();

					String macAddress = log.getDeviceMacAddress();
					if(ObjectUtils.isEmpty(macAddress)){
						macAddress = loggedPatient.getDeviceMacAddress();
					}
					Device device = deviceDao.getDeviceByMac(macAddress);
					if(device != null && device.getRegistrationId() != null){
						//This Variable is Introduced to Parse Jason String
						String summaryJason = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(dataMap);
						//this will replace \\u0026(endoded &) with &
						summaryJason = summaryJason.replace("\\u0026", "&");
						Map<String, String> resultMap = sendGCM(device, AppConstants.GCMKEYS.NUTRITIONAL_INFO.getValue(), summaryJason );
						description = resultMap.get(AppConstants.ServicesConstants.DATA.name());
						statusMessage = resultMap.get(AppConstants.ServicesConstants.STATUS.name()) + "_INFO";
					}
				}

				if(statusMessage.equalsIgnoreCase(AppConstants.ServicesConstants.DATA.name())){
					statusMessage = "LOG_SUCCESS";
				} else{
					statusMessage = statusMessage +" LOG_SUCCESS";
				}
			}

			if(log != null){
				description = description + " of log of Log Id: "+log.getLogId();
			}
		}

		addServerLog("sendFoodNutritionInfo");
		if(isFromError.equalsIgnoreCase("yes")){
			return viewReportErrors();
		}else if(!ObjectUtils.isEmpty(redirectToPage) && redirectToPage.equalsIgnoreCase("fetchLogbook")){
			return fetchLogbook();
		} else if(!ObjectUtils.isEmpty(redirectToPage) && redirectToPage.equalsIgnoreCase("myMeals")){
			return "myMeals";
		} else if(!ObjectUtils.isEmpty(redirectToPage) && redirectToPage.equalsIgnoreCase("inAppMessages")){
			return "inAppMessages";
		} else if(!ObjectUtils.isEmpty(redirectToPage) && redirectToPage.equalsIgnoreCase("shareableMeals")){
			return "sharedMeals";
		}

		return redirectToUnprocessedOrOtherPage(AppConstants.REDIRECT_LOGS);
	}

	//if redirectToPage flag contains 'unprocessedMeals' then call its action, __oz
	public String redirectToUnprocessedOrOtherPage(String toOtherPage) {
		if(!ObjectUtils.isEmpty(redirectToPage) && redirectToPage.equalsIgnoreCase("unprocessedmeals")) {
			//remove active patient from session
			session.put(AppConstants.SessionKeys.ACTIVE_PATIENT.name(), null);
			return unProcessedMeals();
		}

		return toOtherPage;
	}

	@Action(value="archive")
	public String archiveLog() throws ParseException {

		Log logToArchive = logDAO.getLogById(logId);
		logToArchive.setIsArchive(true);
		logDAO.save(logToArchive);

		if(!ObjectUtils.isEmpty(redirectToPage) && redirectToPage.equalsIgnoreCase("fetchLogbook")){
			return fetchLogbook();
		}else if(!ObjectUtils.isEmpty(redirectToPage) && redirectToPage.equalsIgnoreCase("myMeals")){
			log = logToArchive;
			return "myMeals";
		}else if(!ObjectUtils.isEmpty(redirectToPage) && redirectToPage.equalsIgnoreCase("inAppMessages")){
			return "inAppMessages";
		}else if(!ObjectUtils.isEmpty(redirectToPage) && redirectToPage.equalsIgnoreCase("shareableMeals")){
			return "sharedMeals";
		}

		return redirectToUnprocessedOrOtherPage(AppConstants.REDIRECT_LOGS);
	}

	@Action(value ="getFoodLogSummaryDetail")
	public String getFoodLogSummaryDetail() {
		List<FoodDetailDTO> foodLogDetailList = new ArrayList<FoodDetailDTO>();

		try {

			log = logDAO.getLogById(logId);
			FoodLogSummary foodLogSummary = log.getFoodLogSummary();

			List<FoodLogDetail> foodLogSummaryDetails = new ArrayList<FoodLogDetail>();
			FoodLogSummary summary = logSummaryDAO.getSummaryById(mealSummaryId);

			if(summary != null && summary.getFoodLogDetails().size() > 0){
				for(FoodLogDetail detail: summary.getFoodLogDetails()){
					FoodLogDetail foodLogSummaryDetail = new FoodLogDetail();

					if(detail.getFoodMaster()!=null){

						FoodDetailDTO fooddetailDTO = new FoodDetailDTO();
						FoodMaster foodmaster = detail.getFoodMaster();

						String foodName = foodmaster.getFoodName();
						if(!ObjectUtils.isEmpty(summary.getMealName())){
							foodName = summary.getMealName();
						}
						fooddetailDTO.setFoodName(foodName);

						float fCal = 0, fCarbs = 0, fFats = 0, fProteins = 0;
						if(summary != null && !foodName.equalsIgnoreCase(foodmaster.getFoodName())){
							fCal = ObjectUtils.nullSafeFloat(summary.getCalories());
							fCarbs = ObjectUtils.nullSafeFloat(summary.getCarbs());
							fFats = ObjectUtils.nullSafeFloat(summary.getFats());
							fProteins = ObjectUtils.nullSafeFloat(summary.getProtein());

							fooddetailDTO.setFats(fFats);
							fooddetailDTO.setCarbs(fCarbs);
							fooddetailDTO.setProtein(fProteins);

							FoodMaster newFoodMaster = CommonUtils.addNewFoodMaster(foodName, fCarbs, fFats, fProteins, foodmaster.getServingSizeUnit());

							foodLogSummaryDetail.setFoodMaster(newFoodMaster);
							fooddetailDTO.setFoodMasterId(newFoodMaster.getFoodMasterId());

						}else{
							foodLogSummaryDetail.setFoodMaster(detail.getFoodMaster());
							fooddetailDTO.setFoodMasterId(foodmaster.getFoodMasterId());
							fooddetailDTO.setFats(foodmaster.getFats());
							fooddetailDTO.setCarbs(foodmaster.getCarbs());
							fooddetailDTO.setProtein(foodmaster.getProtein());
						}

						fooddetailDTO.setNumberOfServings(detail.getNumberOfServings());
						fooddetailDTO.setServingSizeUnit(foodmaster.getServingSizeUnit());
						foodLogDetailList.add(fooddetailDTO);

						foodLogSummaryDetail.setNumberOfServings(detail.getNumberOfServings());
						foodLogSummaryDetail.setFoodLogSummary(foodLogSummary);
						foodLogSummaryDetails.add(foodLogSummaryDetail);
					}
				}

				foodLogSummaryDetails = CommonUtils.removeDuplicateFoodDetails(foodLogSummaryDetails);

				float fCal = 0, fCarbs = 0, fFats = 0, fProteins = 0;

				if(foodLogSummary != null){
					fCal = ObjectUtils.nullSafeFloat(foodLogSummary.getCalories());
					fCarbs = ObjectUtils.nullSafeFloat(foodLogSummary.getCarbs());
					fFats = ObjectUtils.nullSafeFloat(foodLogSummary.getFats());
					fProteins = ObjectUtils.nullSafeFloat(foodLogSummary.getProtein());
				}

				float sCal = ObjectUtils.nullSafeFloat(summary.getCalories());
				float sCarbs = ObjectUtils.nullSafeFloat(summary.getCarbs());
				float sFats = ObjectUtils.nullSafeFloat(summary.getFats());
				float sProteins = ObjectUtils.nullSafeFloat(summary.getProtein());

				if(foodLogSummary == null){
					foodLogSummary = new FoodLogSummary();
					foodLogSummary.setLog(log);
				}

				if(fCal != sCal){
					foodLogSummary.setCalories(sCal + fCal);
				}

				if(fCarbs != sCarbs){
					foodLogSummary.setCarbs(sCarbs + fCarbs);
				}

				if(fFats != sFats){
					foodLogSummary.setFats(sFats + fFats);
				}

				if(fProteins != sProteins){
					foodLogSummary.setProtein(sProteins + fProteins);
				}

				foodLogSummary.setFoodLogDetails(foodLogSummaryDetails);

				logSummaryDAO.save(foodLogSummary);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		foodLogDetailJson = JsonUtil.toJson(foodLogDetailList);
		return AppConstants.AJAX_FOOD_MASTER;
	}

	@Action(value="saveFoodLogSummaryTotals")
	public String saveFoodLogSummaryTotals(){

		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

		try {

			log = logDAO.getLogById(logId);

			if(log != null){

				FoodLogSummary foodLogSummary = log.getFoodLogSummary();

				if(foodLogSummary != null){
					foodLogSummary.setCarbs(Float.parseFloat(totalCarbs));
					foodLogSummary.setFats(Float.parseFloat(totalFats));
					foodLogSummary.setProtein(Float.parseFloat(totalProtein));
					foodLogSummary.setCalories(Float.parseFloat(totalCalories));

					logSummaryDAO.save(foodLogSummary);
					returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
				}
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		nutritionInfoJson = JsonUtil.toJson(returnMap);
		return AppConstants.AJAX;
	}

	@Action(value="processLog")
	public boolean processLog(Log log){
		boolean result = false;
		if(log != null){
			log.setIsProcessed(true);
			log.setProcessDate(new Date());
			log.getFoodLogSummary().setHasMissingFood(hasMissingFood);
			log.getFoodLogSummary().setMealName(foodLogName);
			log.setLogProcessedTime(System.currentTimeMillis());
			log.setLastModified(System.currentTimeMillis());

			//save processed time and userId who has processed the log, __oz [01/5/2015]
			User loggedInUser = (User)session.get(AppConstants.SessionKeys.USER.name());
			log.setProcessedTime(System.currentTimeMillis());
			if(loggedInUser != null){
				log.setUserId(loggedInUser.getUserId());
			}

			//System.out.println(foodLogName);
			logDAO.save(log);
			sendPost(log);
			description = "Process Log for Log Id: "+log.getLogId();
			result = true;
		}
		addServerLog("processLog");
		return result;
	}

	private void sendPost(Log log) {

		try {

			FoodLogSummary foodLogSummary = log.getFoodLogSummary();

			if(foodLogSummary != null){

				Map<String, String> dataMap = new LinkedHashMap<String, String>();

				dataMap.put(AppConstants.NEWSFEED.Q_CUSTOM_ID.getValue(), log.getLogId());
				LOG.info("sendPost: member uuid: "+log.getPatient().getUuid());

				dataMap.put(AppConstants.NEWSFEED.USERNAME.getValue(), log.getPatient().getUser().getEmail());
				//dataMap.put(AppConstants.NEWSFEED.POST_ID.getValue(), String.valueOf(log.getPostId()));

				if(foodLogSummary.getMealName() != null){
					dataMap.put(AppConstants.NEWSFEED.MEAL_NAME.getValue(), foodLogSummary.getMealName());
				}

				if(foodLogSummary.getCarbs() != null){
					LOG.info("sendPost: meal carbs: "+foodLogSummary.getCarbs());
					dataMap.put(AppConstants.NEWSFEED.CARBS.getValue(), String.valueOf(foodLogSummary.getCarbs()));
				}

				if(foodLogSummary.getHasMissingFood() != null && foodLogSummary.getHasMissingFood())	{
					dataMap.put(AppConstants.NEWSFEED.HAS_MISSING_FOOD.getValue(), AppConstants.TRUE);
				}else{
					dataMap.put(AppConstants.NEWSFEED.HAS_MISSING_FOOD.getValue(), AppConstants.FALSE);
				}

				dataMap.put(AppConstants.NEWSFEED.LOG_TIME.getValue(), log.getCreatedOn().toString());
				dataMap.put(AppConstants.NEWSFEED.TITLE.getValue(), foodLogSummary.getType());

				String jsonString = JsonUtil.toJson(dataMap);
				String response = SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.UPDATEPOST.getValue());
				LOG.info("sendPost: response from elgg: "+response);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//Find new msg from patient
	@Action(value="findNewMessages")
	public String findNewMessages(){

		newMessagesJson = "";
		if(logId != null){
			List<Object> messages = logDAO.getNewMessages(logId);
			if(messages != null && messages.size() > 0){
				Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
				dataMap.put(AppConstants.ServicesConstants.DATA.name(), messages);
				dataMap.put(AppConstants.ServicesConstants.COUNT.name(), messages.size());

				//if new count is greater than already counted messages, then set a flag to refresh threaded view, __oz
				if(messages.size() > messagesTotalCount) {
					messagesTotalCount = messages.size();
					dataMap.put(AppConstants.ServicesConstants.REFRESH.name(), true);
					dataMap.put(AppConstants.ServicesConstants.COUNT.name(), messagesTotalCount);
				}
				newMessagesJson = new Gson().toJson(dataMap);
			}
		}

		return AppConstants.AJAX_NEW_MESSAGES;
	}

	@Action(value="updateMealName")
	public String updateMealName(){
		log = logDAO.getLogById(logId);
		if(log != null && log.getFoodLogSummary() != null){
			log.getFoodLogSummary().setMealName(logName);
			logDAO.save(log);
		}

		logNameUpdateStatus = logName;
		return AppConstants.AJAX_SUCCESS;
	}

	//send Question to patient
	@Action(value="sendMessage")
	public String sendMessage(){

		if(!ObjectUtils.isEmpty(logId)){
			log = logDAO.getLogById(logId);

			if(log != null){

				Message msg = new Message();

				if(log.getFoodLogSummary() != null){
					msg.setFoodLogSummary(log.getFoodLogSummary());
				}

				msg.setMessage(message);
				msg.setOwner(AppConstants.SessionKeys.PROVIDER.name());
				msg.setPatient(log.getPatient());

				Date date = new Date();

				/*msg.setTimestamp(CommonUtils.GTMtoESTInLong(date.getTime()));*///
				msg.setTimestamp(date.getTime());

				msg.setReadStatus(true);
				msg.setIsError(false);

				Provider provider = (Provider) session.get(AppConstants.SessionKeys.PROVIDER.name());
				provider = providerDAO.getProviderById(provider.getProviderId());
				User user = userDAO.getUserById(provider.getUser().getUserId());
				msg.setUser(user);

				Map<String, Object> gcmDataMap = new LinkedHashMap<String, Object>();
				gcmDataMap.put(AppConstants.GCMKEYS.LOG_ID.getValue(), log.getLogId());
				gcmDataMap.put(AppConstants.GCMKEYS.DATA.getValue(), message);
				gcmDataMap.put(AppConstants.GCMKEYS.PROVIDER_NAME.getValue(), user.getFirstName()+" "+user.getLastName());

				DeviceDAO deviceDao = new DeviceDAO();
				Device device = deviceDao.getDeviceByMac(log.getDeviceMacAddress());
				Map<String, String> resultMap = sendGCM(device, AppConstants.GCMKEYS.QUESTION.getValue(), new Gson().toJson(gcmDataMap));
				description = resultMap.get(AppConstants.ServicesConstants.DATA.name());

				if(resultMap.get(AppConstants.ServicesConstants.STATUS.name()).equalsIgnoreCase(AppConstants.ServicesConstants.SUCCESS.name())){
					messageDAO.save(msg);

					// Sending Dietition message to Elgg
					LogServices.buildMiscLogForSendingToElgg(log, message, "", AppConstants.NEWSFEED.MESSAGE.getValue(), false);

					description = description + " | Save message for logId: "+logId;

					actionMessage =  CommonUtils.gmtToest(date.getTime());
				}
			}
		}

		addServerLog("sendMessage");
		return AppConstants.AJAX_MESSAGE;
	}

	public Map<String, String> sendGCM(Device device, String action, String payload){

		Map<String, String> resultMap = new LinkedHashMap<String, String>();
		if(description == null){
			description = "Send GCM, action "+action+", payload: "+payload;
		}else{
			description = description + "Send GCM, action "+action+", payload: "+payload;
		}

		List<Device> devices = new ArrayList<Device>();
		devices.add(device);
		String result = PushNotificationManager.pushMessage(devices, action, payload, request);
		description = description + ", GCM Result: "+result;
		resultMap.put(AppConstants.ServicesConstants.DATA.name(), description);
		return resultMap;
	}

	@Action(value="addMealFood")
	public String addMealFoodAjax(){

		FoodLogDetail foodLogDetail = new FoodLogDetail();
		List<FoodMaster> updateFoodMasters = new ArrayList<FoodMaster>();
		FoodMaster foodMaster = new FoodMaster();
		FoodLogSummary foodLogSummary = new FoodLogSummary();
		selFoodId = 0l;

		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

		if(foodId == null){

			if(!ObjectUtils.isEmpty(foodName)){
				foodMaster.setFoodName(foodName);
			}

			foodMaster.setCarbs(foodCarbs);
			foodMaster.setFats(foodFats);
			foodMaster.setProtein(foodProtein);
			foodMaster.setServingSizeUnit(servingUnit);
			foodMasterDAO.save(foodMaster);
			foodId = foodMasterDAO.getLastInsertedRecordId();
		}

		if(!ObjectUtils.isEmpty(logId)) {
			log = logDAO.getLogById(logId);

			if (log != null) {

				if (log.getFoodLogSummary() != null) {
					foodLogSummary = log.getFoodLogSummary();
				}

				List<FoodLogDetail> foodLogDetails = new ArrayList<FoodLogDetail>();
				List<FoodLogDetail> updateFoodLogDetails = new ArrayList<FoodLogDetail>();

				if (foodLogSummary != null) {
					foodLogDetails = foodLogSummary.getFoodLogDetails();
					foodLogSummary.setCarbs(0f);
					foodLogSummary.setFats(0f);
					foodLogSummary.setProtein(0f);
				}

				//change in food which is already added in food table
				if (foodId != null && foodId != 0) {

					selFoodId = foodId;
					foodMaster = foodMasterDAO.getFoodMasterById(foodId);

					if (foodLogDetails != null) {
						for (FoodLogDetail detail : foodLogDetails) {
							if (detail.getFoodMaster() != null) {
								if (detail.getFoodMaster().getFoodMasterId().equals(foodMaster.getFoodMasterId())) {
									foodLogDetail = detail;
								} else {
									updateFoodLogDetails.add(detail);
								}
							}
						}
					}

					float totalCalories = 0;

					for (FoodLogDetail detail : updateFoodLogDetails) {
						if (detail.getFoodMaster() != null && foodLogSummary != null) {
							if (detail.getFoodMaster().getCarbs() != 0) {
								float carbs = detail.getNumberOfServings() * detail.getFoodMaster().getCarbs();
								foodLogSummary.setCarbs(foodLogSummary.getCarbs() + carbs);
								totalCalories += carbs * 4;
							}
							if (detail.getFoodMaster().getFats() != 0) {
								float fats = detail.getNumberOfServings() * detail.getFoodMaster().getFats();
								foodLogSummary.setFats(foodLogSummary.getFats() + fats);
								totalCalories += fats * 9;
							}
							if (detail.getFoodMaster().getProtein() != 0) {
								float protein = detail.getNumberOfServings() * detail.getFoodMaster().getProtein();
								foodLogSummary.setProtein(foodLogSummary.getProtein() + protein);
								totalCalories += protein * 4;
							}
						}
					}

					if (foodLogSummary != null) {
						foodLogSummary.setCalories(totalCalories);
					}
				}

				if (foodName != null && !foodName.equalsIgnoreCase("")) {

					foodMaster.setFoodName(foodName);

					if (servingSize != null && !servingSize.equalsIgnoreCase("")) {
						foodLogDetail.setNumberOfServings(Float.parseFloat(servingSize));
					}

					foodMaster.setCarbs(foodCarbs);
					foodMaster.setFats(foodFats);
					foodMaster.setProtein(foodProtein);
					foodMaster.setServingSizeUnit(servingUnit);

					float totalCalories = 0;
					if (foodCarbs != 0 && foodLogSummary != null) {
						float carbs = foodLogDetail.getNumberOfServings() * foodCarbs;
						foodLogSummary.setCarbs(foodLogSummary.getCarbs() + carbs);
						totalCalories += carbs * 4;
					}
					if (foodFats != 0 && foodLogSummary != null) {
						float fats = foodLogDetail.getNumberOfServings() * foodFats;
						foodLogSummary.setFats(foodLogSummary.getFats() + fats);
						totalCalories += fats * 9;
					}
					if (foodProtein != 0 && foodLogSummary != null) {
						float protein = foodLogDetail.getNumberOfServings() * foodProtein;
						foodLogSummary.setProtein(foodLogSummary.getProtein() + protein);
						totalCalories += protein * 4;
					}

					if (foodLogSummary != null) {
						foodLogSummary.setCalories(foodLogSummary.getCalories() + totalCalories);
					}

					updateFoodMasters.add(foodMaster);
				}

				foodMasterDAO.save(foodMaster);
				foodLogDetail.setFoodMaster(foodMaster);
				foodLogDetail.setFoodLogSummary(foodLogSummary);
				updateFoodLogDetails.add(foodLogDetail);

				if (foodLogSummary != null) {
					foodLogSummary.setFoodLogDetails(updateFoodLogDetails);
				}
				foodLogSummary.setMealName(logNameFromFood);
				log.setFoodLogSummary(foodLogSummary);

				logDAO.save(log);
				selFoodId = foodMaster.getFoodMasterId();
			}
		}

		description = "Add Food for Log Id: "+log.getLogId() ;
		addServerLog("addMealFood");

		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
		returnMap.put(AppConstants.JsonConstants.DATA.name(), selFoodId+"");
		returnMap.put(AppConstants.JsonConstants.CUSTOM_DATA.name(), JsonUtil.toJson(foodMaster));

		nutritionInfoJson = JsonUtil.toJson(returnMap);
		return AppConstants.AJAX;
	}

	@Action(value="deleteMealFood")
	public String deleteMealFood(){

		if(!ObjectUtils.isEmpty(logId)){
			log = logDAO.getLogById(logId);

			if(log != null){

				FoodLogDetail foodLogDetail = new FoodLogDetail();
				FoodMaster foodMaster = new FoodMaster();

				FoodLogSummary foodLogSummary = new FoodLogSummary();
				if(log.getFoodLogSummary() != null){
					foodLogSummary = log.getFoodLogSummary();
				}

				List<FoodLogDetail> foodLogDetails = new ArrayList<FoodLogDetail>();
				List<FoodLogDetail> updateFoodLogDetails = new ArrayList<FoodLogDetail>();
				if(foodLogSummary != null){
					foodLogDetails = foodLogSummary.getFoodLogDetails();
				}

				if(foodId != null && foodId != 0){

					if(foodLogDetails != null){
						for(FoodLogDetail detail: foodLogDetails){

							if(detail.getFoodMaster() != null){
								if(detail.getFoodMaster().getFoodMasterId().equals(foodId)){
									foodLogDetail = detail;
									foodMaster = detail.getFoodMaster();
								}
								else{
									updateFoodLogDetails.add(detail);
								}
							}
						}
					}

					if (foodLogSummary!= null && foodMaster != null && foodLogDetail != null) {

						float carbs = foodLogDetail.getNumberOfServings()*foodMaster.getCarbs();
						float fats = foodLogDetail.getNumberOfServings()*foodMaster.getFats();
						float protein = foodLogDetail.getNumberOfServings()*foodMaster.getProtein();

						foodLogSummary.setCarbs(foodLogSummary.getCarbs()-carbs);
						foodLogSummary.setFats(foodLogSummary.getFats()-fats);
						foodLogSummary.setProtein(foodLogSummary.getProtein()-protein);

						foodLogSummary.setCalories(foodLogSummary.getCalories()-(carbs*4+fats*9+protein*4));
					}

					if (foodLogSummary!= null){
						foodLogSummary.setFoodLogDetails(updateFoodLogDetails);
					}

					log.setFoodLogSummary(foodLogSummary);

					logDAO.save(log);

					if(foodLogDetail != null){
						foodLogDetail.setFoodMaster(null);
						foodLogDetailDAO.delete(foodLogDetail);
					}

					selFoodId = foodId;
				}
			}
		}

		description = "Delete Food for Log Id: "+log.getLogId() ;
		addServerLog("deleteMealFood");
		return AppConstants.AJAX_NUTRI_INFO;
	}

	@Action(value="populateQuestionSelector")
	public String populateQuestionSelector(){
		questionListJson =  PopulateQuestionMap();
		return AppConstants.AJAX_QUESTION_LIST;
	}

	public void addServerLog(String actionName){

		try {
			ServerLog serverLog = new ServerLog();

			User user = (User)session.get(AppConstants.SessionKeys.USER.name());
			if (user != null) {
				serverLog.setLoggedInUserId(user.getUserId());
				serverLog.setRole(AppConstants.Roles.ROLE_PROVIDER.name());
			}

			serverLog.setActionName(actionName);
			serverLog.setDescription(description);
			serverLog.setLogTime(new Date());

			//new ServerLogDAO().saveServerLog(serverLog);

		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public String PopulateQuestionMap(){
		for(AppConstants.Questions question : AppConstants.Questions.values()) {
			if (questionMap.get(question.getCategory()) == null) {
				questionMap.put(question.getCategory(), new ArrayList<String>());
				questionMap.get(question.getCategory()).add(question.getQuestion());
			} else {
				questionMap.get(question.getCategory()).add(question.getQuestion());
			}
		}
		questionListJson =  new Gson().toJson(questionMap);
		return questionListJson;
	}

	/*---------------------Weekly ACTIONS-------------------------------*/

	@Action(value="fetchWeeklyLogsData")
	public String fetchWeeklyLogsData() throws ParseException{
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(weeklyLogsDateString != null && !weeklyLogsDateString.isEmpty()){
			String[] dateParts = weeklyLogsDateString.split("-");
			List<LogDTO> objMealLogs = logDAO.getLogbookDataByPatientId(patientId, dateParts[0], dateParts[1], logName);
			dataMap.put(AppConstants.LOGS.MEAL_LOGS.getValue(), objMealLogs);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchWeeklyReportData")
	public String fetchWeeklyReportData(){
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(weeklyReportDateString != null && !weeklyReportDateString.isEmpty()){
			String[] dateParts = weeklyReportDateString.split("-");

			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			Object glucoseDataAfterMeal = logDAO.getGlucoseSummaryAfterMealByPatientId(patientId, dateFrom, dateTo);
			Object glucoseDataBeforeMeal = logDAO.getGlucoseSummaryBeforeMealByPatientId(patientId, dateFrom, dateTo);
			Object objGlucose = logDAO.getGlucoseSummaryByPatientId(patientId, dateFrom, dateTo);
			Object objMealPercentage = logDAO.getPercentageOfMealsByPatientId(patientId, dateFrom, dateTo);
			Object objMealAverage = logDAO.getAverageOfMealsByPatientId(patientId, dateFrom, dateTo);
			List<Object> medObjects = logDAO.getTookMedsDataByPatientId(patientId, dateFrom, dateTo);
			medObjects = addMissingDatesData(medObjects, dateFrom, dateTo);
			medObjects = getTookMedList(medObjects);

			List<Object> minMax = new ArrayList<Object>();
			minMax.add(dateFrom + CommonUtils.getOffset());
			minMax.add(dateTo + CommonUtils.getOffset());
			dataMap.put(AppConstants.LOG_TYPE_MEDICATION, medObjects);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);

			Target target = new TargetDAO().getTargetByPatientId(patientId);

			dataMap.put(AppConstants.LOGS.GLUCOSE.getValue(), objGlucose);
			dataMap.put(AppConstants.LOGS.MEAL_AVG.getValue(), objMealAverage);
			dataMap.put(AppConstants.LOGS.MEAL_PERCENTAGE.getValue(), objMealPercentage);
			dataMap.put(AppConstants.LOGS.TARGET.getValue(), CommonUtils.getMealTargets(target));
			dataMap.put(AppConstants.LOG_TYPE_MEDICATION, medObjects);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			dataMap.put(AppConstants.LOGS.AFTER_MEAL_GLUCOSE.getValue(), glucoseDataAfterMeal);
			dataMap.put(AppConstants.LOGS.NOT_AFTER_MEAL_GLUCOSE.getValue(), glucoseDataBeforeMeal);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchWeeklyMealsData")
	public String fetchWeeklyMealsData() throws ParseException{
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(weeklyTrendDateString != null && !weeklyTrendDateString.isEmpty()){
			String[] dateParts = weeklyTrendDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0].trim()));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1].trim()));
			Object objWeeklyTrend = logDAO.getTrendsByPatientId(patientId, dateFrom, dateTo);
			Object objAverageTrend = logDAO.getAverageOfEachMealSummaryChart(patientId, dateFrom, dateTo);
			Target target = new TargetDAO().getTargetByPatientId(patientId);
			float mealTargets[] = CommonUtils.getMealTargets(target);
			dataMap.put(AppConstants.LOGS.WEEKLY_MEAL.getValue(), objWeeklyTrend);
			dataMap.put(AppConstants.LOGS.MEAL_AVG.getValue(), objAverageTrend);
			dataMap.put(AppConstants.LOGS.TARGET.getValue(), mealTargets);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchWeeklyMedicationData")
	public String fetchWeeklyMedicationData() throws ParseException{
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(weeklyTrendDateString != null && !weeklyTrendDateString.isEmpty()){
			String[] dateParts = weeklyTrendDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			List<Object> tookMedsDetailList = logDAO.getTookMedsDataByPatientId(patientId, dateFrom, dateTo);
			tookMedsDetailList = addMissingDatesData(tookMedsDetailList, dateFrom, dateTo);
			tookMedsDetailList = getTookMedList(tookMedsDetailList);
			List<Object> minMax = new ArrayList<Object>();
			minMax.add(dateFrom + CommonUtils.getOffset());
			minMax.add(dateTo + CommonUtils.getOffset());
			dataMap.put(AppConstants.LOG_TYPE_MEDICATION, tookMedsDetailList);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value = "fetchWeeklyActivityChart")
	public String fetchWeeklyActivityChart() throws ParseException {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(!ObjectUtils.isEmpty(weeklyReportDateString)) {
			String[] dateParts	 = weeklyReportDateString.split("-");
			Long dateFrom = Long.parseLong(dateParts[0]);
			Long dateTo = Long.parseLong(dateParts[1]);
			String formattedDateString = DateUtils.getFormattedDate(dateFrom, "yyyy-MM-dd");
			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(formattedDateString);
			List<Long> minMax = getRange(dateToGetChart, 8, patientId);

			List<Object> misfitHourlyLogs = logDAO.getMisfitActivityHourlyLogsByPatientId(patientId, dateFrom, dateTo);
//			misfitHourlyLogs = CommonUtils.addFacilityTimeOffset(misfitHourlyLogs, patientId);
			List<Object> misfitHourlySumLogs = logDAO.getMisfitActivityHourlyLogsSumByPatientId(patientId, dateFrom, dateTo);
//			misfitHourlySumLogs = CommonUtils.addFacilityTimeOffset(misfitHourlySumLogs, patientId);

			if(misfitHourlyLogs != null) {
				misfitHourlyLogs = getActivityLogsDataList(misfitHourlyLogs, patientId);
			}

			dataMap.put(AppConstants.ServicesConstants.DATA.name(), misfitHourlyLogs);
			dataMap.put(AppConstants.ChartServiceConstants.STEPS.name(), misfitHourlySumLogs);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			dataMap.put(AppConstants.ServicesConstants.MAXYAXIS.name(), getMaxXAxisForChart(misfitHourlySumLogs, "weekly"));
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value = "fetchWeeklyCombineActivityMinutesData")
	public String fetchWeeklyCombineActivityMinutesData() throws ParseException {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(!ObjectUtils.isEmpty(weeklyReportDateString)) {
			String[] dateParts = weeklyReportDateString.split("-");
			Long dateFrom = Long.parseLong(dateParts[0]);
			Long dateTo = Long.parseLong(dateParts[1]);
			String formattedDateString = DateUtils.getFormattedDate(dateFrom, "yyyy-MM-dd");
			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(formattedDateString);
			List<Long> minMax = getRange(dateToGetChart, 8, patientId);

			List<Object> activityLogList = logDAO.getActivityDetailDataObjects(patientId, dateFrom, dateTo);
			activityLogList = addMissingDatesDataActivityMinutes(activityLogList, dateFrom, dateTo);
			activityLogList = getActivityMinutesDataListFacilityTimeOffset(activityLogList, patientId);

			dataMap.put(AppConstants.ServicesConstants.DATA.name(), activityLogList);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
//			dataMap.put(AppConstants.ServicesConstants.MAXYAXIS.name(), getMaxXAxisForChart(activityLogList, "weekly-minutes"));
			logReportData = JsonUtil.toJsonExcludedNull(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchWeeklyGlucoseScatterGlucoseData")
	public String fetchWeeklyDaysScatterGlucoseData() throws ParseException {
		/*List & Map for data*/
		Map<String, List> data = new LinkedHashMap<String, List>();
		List<List> beforeMealGlucoseTargets = new ArrayList<List>();

		List<List> glucoseUnderTarget = new ArrayList<List>() ;
		List<List> glucoseAboveTarget = new ArrayList<List>() ;
		List<List> glucoseAtTarget = new ArrayList<List>();
		List<Double> combineGlucoseTarget = new ArrayList<Double>();
		List<List> afterGlucoseTargets = new ArrayList<List>() ;

		Target target = new TargetDAO().getTargetByPatientId(patientId);
		float afterMealGlucoseMin = 0f;
		float afterMealGlucoseMax = 0f;

		if(target != null) {
			glucoseMin = target.getGlucoseMin2();
			glucoseMax = target.getGlucoseMax2();
			afterMealGlucoseMin = target.getGlucoseMin();
			afterMealGlucoseMax = target.getGlucoseMax();
		}

		/*Set MinMax Range*/
		if(!ObjectUtils.isEmpty(weeklyReportDateString)){
			String[] dateParts = weeklyReportDateString.split("-");
			Long startTime = Long.parseLong(dateParts[0]);
			Long endTime = Long.parseLong(dateParts[1]);
			List<Integer> totalTests = new ArrayList<Integer>();
			List<Integer> avgBGL = new ArrayList<Integer>();
			Integer totalBeforeMealBGL = 0, totalAfterMealBGL = 0;

			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(DateUtils.getFormattedDate(startTime, "yyyy-MM-dd"));
			List<Long> minMaxRang = getRange(dateToGetChart, 7);

			List<Object> glucoseValueList = logDAO.getAvgGlucoseValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> glucoseBeforeMealValueList = logDAO.getAvgGlucoseBeforeMealValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> glucoseAfterMealValueList = logDAO.getAvgGlucoseAfterMealValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> stdBeforeMeal = logDAO.getAvgGlucoseSTDBeforerMealValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> stdAfterMeal = logDAO.getAvgGlucoseSTDAfterMealValuesPerDayByPatientId(patientId, startTime, endTime);
			glucoseValueList = adjustedGlucoseLogValueListByTime(glucoseValueList, minMaxRang, patientId);
			totalBeforeMealBGL = getTotalBGLFromList(glucoseBeforeMealValueList);
			totalAfterMealBGL = getTotalBGLFromList(glucoseAfterMealValueList);

			// Further Calculations / List due to json map get list
			totalTests.add(glucoseBeforeMealValueList.size());
			totalTests.add(glucoseAfterMealValueList.size());
			avgBGL.add(totalBeforeMealBGL / (totalTests.get(0) != 0 ? totalTests.get(0) : 1));
			avgBGL.add(totalAfterMealBGL / (totalTests.get(1) != 0 ? totalTests.get(1) : 1));
			List<Double> avgPrePostList = getAveragePrePostBGL(glucoseValueList);

			for(Object obj: glucoseValueList){
				List<Object> valuePair = new ArrayList<Object>() ;
				Object[] objArray = (Object[]) obj;

				double GlucoseFloatValue = Double.parseDouble(objArray[1].toString());
				String glucoseMealTime = objArray[2].toString();
				valuePair.add(objArray[0]);
				valuePair.add(GlucoseFloatValue) ;
				valuePair.add(glucoseMealTime);
				valuePair.add(objArray[6]);
				valuePair.add(objArray[7]); // printing time in tooltip

				String glucoseTime = ObjectUtils.nullSafe(objArray[2]);
				if(ObjectUtils.isEmpty(glucoseTime) || glucoseTime.equalsIgnoreCase(AppConstants.GLUCOSE_BEFORE_MEAL)) {
					if(GlucoseFloatValue < glucoseMin) {
						glucoseUnderTarget.add(valuePair);
					} else if(GlucoseFloatValue > glucoseMax) {
						glucoseAboveTarget.add(valuePair) ;
					} else {
						glucoseAtTarget.add(valuePair) ;
					}
				} else {
					if(GlucoseFloatValue < afterMealGlucoseMin) {
						glucoseUnderTarget.add(valuePair);
					} else if(GlucoseFloatValue > afterMealGlucoseMax) {
						glucoseAboveTarget.add(valuePair) ;
					} else {
						glucoseAtTarget.add(valuePair) ;
					}
				}
				combineGlucoseTarget.add(GlucoseFloatValue);
			}

			List<Object> beforeGlucoseTargetsObjects = new ArrayList<Object>();
			beforeGlucoseTargetsObjects.add(glucoseMin);
			beforeGlucoseTargetsObjects.add(glucoseMax);
			beforeMealGlucoseTargets.add(beforeGlucoseTargetsObjects);

			List<Object> afterMealeTargetsObjects = new ArrayList<Object>();
			afterMealeTargetsObjects.add(afterMealGlucoseMin);
			afterMealeTargetsObjects.add(afterMealGlucoseMax);
			afterGlucoseTargets.add(afterMealeTargetsObjects);

			data.put("High", glucoseAboveTarget) ;
			data.put("Low", glucoseUnderTarget) ;
			data.put("Normal", glucoseAtTarget) ;
			data.put("beforeMealGlucoseTargets", beforeMealGlucoseTargets);
			data.put("afterMealGlucoseTargets", afterGlucoseTargets);
			data.put("MinMax", minMaxRang);
			data.put("maxYAxis", combineGlucoseTarget);
			data.put("TotalTests", totalTests);
			data.put("AvgBGL", avgBGL);
			data.put("AvgPrePostBGL", avgPrePostList);
			data.put("stdBeforMeal", stdBeforeMeal);
			data.put("stdAfterMeal", stdAfterMeal);
			logReportData = JsonUtil.toJson(data);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	/*------------------------------------------------------------------*/
	/*---------------------MONTHLY ACTIONS ----------------------------*/

	@Action(value="fetchMonthlyMealsData")
	public String fetchMonthlyMealsData() throws ParseException{

		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();

		if(monthlyTrendDateString != null && !monthlyTrendDateString.isEmpty()){
			String[] dateParts = monthlyTrendDateString.split("-");
			Long dateFrom  = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo  = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			Object objTrend = logDAO.getTrendsByPatientId(patientId, dateFrom, dateTo);
			Object objAverageTrend = logDAO.getAverageOfEachMealSummaryChart(patientId, dateFrom, dateTo);
			Target target = new TargetDAO().getTargetByPatientId(patientId);
			float mealTargets[] = CommonUtils.getMealTargets(target);
			dataMap.put(AppConstants.LOGS.MONTHLY_MEAL.getValue(), objTrend);
			dataMap.put(AppConstants.LOGS.MEAL_AVG.getValue(), objAverageTrend);
			dataMap.put(AppConstants.LOGS.TARGET.getValue(), mealTargets);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchMonthlyMedicationData")
	public String fetchMonthlyMedicationData() throws ParseException{

		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();

		if(monthlyTrendDateString != null && !monthlyTrendDateString.isEmpty()){
			String[] dateParts = monthlyTrendDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			List<Object> tookMedsDetailList = logDAO.getTookMedsDataByPatientId(patientId, dateFrom, dateTo);
			tookMedsDetailList = addMissingDatesData(tookMedsDetailList, dateFrom, dateTo);
			tookMedsDetailList = getTookMedList(tookMedsDetailList);
			List<Object> minMax = new ArrayList<Object>();
			minMax.add(dateFrom + CommonUtils.getOffset());
			minMax.add(dateTo + CommonUtils.getOffset());
			dataMap.put(AppConstants.LOG_TYPE_MEDICATION, tookMedsDetailList);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchMonthlyGlucoseData")
	public String fetchMonthlyGlucoseData(){
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(monthlyReportDateString != null && !monthlyReportDateString.isEmpty()){
			String[] dateParts = monthlyReportDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			Object objGlucose = logDAO.getGlucoseSummaryByPatientId(patientId, dateFrom, dateTo);
			Object glucoseDataAfterMeal = logDAO.getGlucoseSummaryAfterMealByPatientId(patientId, dateFrom, dateTo);
			Object glucoseDataBeforeMeal = logDAO.getGlucoseSummaryBeforeMealByPatientId(patientId, dateFrom, dateTo);
			Target target = new TargetDAO().getTargetByPatientId(patientId);
			int beforeTargetMin = 80;
			int beforeTargetMax = 130;
			int afterTargetMin = 80;
			int afterTargetMax = 180;
			if(target != null) {
				beforeTargetMin = target.getGlucoseMin2() == null? 80: target.getGlucoseMin2();
				beforeTargetMax = target.getGlucoseMax2() == null? 130: target.getGlucoseMax2();
				afterTargetMin = target.getGlucoseMin() == null? 80: target.getGlucoseMin();
				afterTargetMax = target.getGlucoseMax() == null? 180: target.getGlucoseMax();
			}
			dataMap.put(AppConstants.LOGS.GLUCOSE.getValue(), objGlucose);
			dataMap.put("beforeMinTarget", beforeTargetMin);
			dataMap.put("beforeMaxTarget", beforeTargetMax);
			dataMap.put("afterMinTarget", afterTargetMin);
			dataMap.put("afterMaxTarget", afterTargetMax);
			dataMap.put(AppConstants.LOGS.AFTER_MEAL_GLUCOSE.getValue(), glucoseDataAfterMeal);
			dataMap.put(AppConstants.LOGS.NOT_AFTER_MEAL_GLUCOSE.getValue(), glucoseDataBeforeMeal);
			dataMap.put(AppConstants.LOGS.GLUCOSE.getValue(), objGlucose);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchMonthlyActivityChart")
	public String fetchMonthlyActivityChart() throws ParseException{
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(monthlyTrendDateString != null && !monthlyTrendDateString.isEmpty()){
			String[] dateParts = monthlyTrendDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			List<Object> activityDetialList = new LogDAO().getActivityDetailData(patientId, dateFrom, dateTo);
			activityDetialList = addMissingDatesData(activityDetialList, dateFrom, dateTo);
			activityDetialList = getActivityDataList(activityDetialList);
			List<Object> minMax = new ArrayList<Object>();
			minMax.add(dateFrom + CommonUtils.getOffset());
			minMax.add(dateTo + CommonUtils.getOffset());
			dataMap.put(AppConstants.ServicesConstants.DATA.name(), activityDetialList);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value = "fetchMonthlyCombineActivityMinutesData")
	public String fetchMonthlyCombineActivityMinutesData() throws ParseException {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(!ObjectUtils.isEmpty(monthlyTrendDateString)) {
			String[] dateParts = monthlyTrendDateString.split("-");
			Long dateFrom = Long.parseLong(dateParts[0]);
			Long dateTo = Long.parseLong(dateParts[1]);
			String formattedDateString = DateUtils.getFormattedDate(dateFrom, "yyyy-MM-dd");
			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(formattedDateString);
			List<Long> minMaxRang = getRange(dateToGetChart, 30, patientId);
			List<Object> activityLogList = logDAO.getAvgActivityDetailDataObjects(patientId, dateFrom, dateTo, 30);
			activityLogList = adjustedLogValueListByTime(activityLogList, minMaxRang, patientId);

			dataMap.put(AppConstants.ServicesConstants.DATA.name(), activityLogList);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMaxRang);
			logReportData = JsonUtil.toJsonExcludedNull(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchMonthlyScatterGlucoseData")
	public String fetchMonthlyScatterGlucoseData() throws ParseException {
		/*List & Map for data*/
		Map<String, List> data = new LinkedHashMap<String, List>();
		List<List> beforeMealGlucoseTargets = new ArrayList<List>();

		List<List> glucoseUnderTarget = new ArrayList<List>() ;
		List<List> glucoseAboveTarget = new ArrayList<List>() ;
		List<List> glucoseAtTarget = new ArrayList<List>();
		List<Double> combineGlucoseTarget = new ArrayList<Double>();
		List<List> afterGlucoseTargets = new ArrayList<List>() ;

		Target target = new TargetDAO().getTargetByPatientId(patientId);
		float afterMealGlucoseMin = 0f;
		float afterMealGlucoseMax = 0f;

		if(target != null) {
			glucoseMin = target.getGlucoseMin2();
			glucoseMax = target.getGlucoseMax2();
			afterMealGlucoseMin = target.getGlucoseMin();
			afterMealGlucoseMax = target.getGlucoseMax();
		}

		/*Set MinMax Range*/
		if(!ObjectUtils.isEmpty(monthlyReportDateString)){
			String[] dateParts = monthlyReportDateString.split("-");
			Long startTime = Long.parseLong(dateParts[0]);
			Long endTime = Long.parseLong(dateParts[1]);
			List<Integer> totalTests = new ArrayList<Integer>();
			List<Integer> avgBGL = new ArrayList<Integer>();
			Integer totalBeforeMealBGL = 0, totalAfterMealBGL = 0;

			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(DateUtils.getFormattedDate(startTime, "yyyy-MM-dd"));
			List<Long> minMaxRang = getRange(dateToGetChart, 30);

			List<Object> glucoseValueList = logDAO.getAvgGlucoseValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> glucoseBeforeMealValueList = logDAO.getAvgGlucoseBeforeMealValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> glucoseAfterMealValueList = logDAO.getAvgGlucoseAfterMealValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> stdBeforeMeal = logDAO.getAvgGlucoseSTDBeforerMealValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> stdAfterMeal = logDAO.getAvgGlucoseSTDAfterMealValuesPerDayByPatientId(patientId, startTime, endTime);
			glucoseValueList = adjustedGlucoseLogValueListByTime(glucoseValueList, minMaxRang, patientId);
			totalBeforeMealBGL = getTotalBGLFromList(glucoseBeforeMealValueList);
			totalAfterMealBGL = getTotalBGLFromList(glucoseAfterMealValueList);

			// Further Calculations / List due to json map get list
			totalTests.add(glucoseBeforeMealValueList.size());
			totalTests.add(glucoseAfterMealValueList.size());
			avgBGL.add(totalBeforeMealBGL / (totalTests.get(0) != 0 ? totalTests.get(0) : 1));
			avgBGL.add(totalAfterMealBGL / (totalTests.get(1) != 0 ? totalTests.get(1) : 1));
			List<Double> avgPrePostList = getAveragePrePostBGL(glucoseValueList);

			for(Object obj: glucoseValueList){
				List<Object> valuePair = new ArrayList<Object>() ;
				Object[] objArray = (Object[]) obj;
				double GlucoseFloatValue = Double.parseDouble(objArray[1].toString());
				String glucoseMealTime = objArray[2].toString();

				valuePair.add(objArray[0]);
				valuePair.add(GlucoseFloatValue);
				valuePair.add(glucoseMealTime);
				valuePair.add(objArray[6]);
				valuePair.add(objArray[7]); // printing time in tooltip

				String glucoseTime = ObjectUtils.nullSafe(objArray[2]);
				if(ObjectUtils.isEmpty(glucoseTime) || glucoseTime.equalsIgnoreCase(AppConstants.GLUCOSE_BEFORE_MEAL)) {
					if(GlucoseFloatValue < glucoseMin) {
						glucoseUnderTarget.add(valuePair);
					} else if(GlucoseFloatValue > glucoseMax) {
						glucoseAboveTarget.add(valuePair) ;
					} else {
						glucoseAtTarget.add(valuePair) ;
					}
				} else {
					if(GlucoseFloatValue < afterMealGlucoseMin) {
						glucoseUnderTarget.add(valuePair);
					} else if(GlucoseFloatValue > afterMealGlucoseMax) {
						glucoseAboveTarget.add(valuePair) ;
					} else {
						glucoseAtTarget.add(valuePair) ;
					}
				}
				combineGlucoseTarget.add(GlucoseFloatValue);
			}

			List<Object> beforeGlucoseTargetsObjects = new ArrayList<Object>();
			beforeGlucoseTargetsObjects.add(glucoseMin);
			beforeGlucoseTargetsObjects.add(glucoseMax);
			beforeMealGlucoseTargets.add(beforeGlucoseTargetsObjects);

			List<Object> afterMealeTargetsObjects = new ArrayList<Object>();
			afterMealeTargetsObjects.add(afterMealGlucoseMin);
			afterMealeTargetsObjects.add(afterMealGlucoseMax);
			afterGlucoseTargets.add(afterMealeTargetsObjects);

			data.put("High", glucoseAboveTarget) ;
			data.put("Low", glucoseUnderTarget) ;
			data.put("Normal", glucoseAtTarget) ;
			data.put("beforeMealGlucoseTargets", beforeMealGlucoseTargets);
			data.put("afterMealGlucoseTargets", afterGlucoseTargets);
			data.put("MinMax", minMaxRang);
			data.put("maxYAxis", combineGlucoseTarget);
			data.put("TotalTests", totalTests);
			data.put("AvgBGL", avgBGL);
			data.put("AvgPrePostBGL", avgPrePostList);
			data.put("stdBeforMeal", stdBeforeMeal);
			data.put("stdAfterMeal", stdAfterMeal);
			logReportData = JsonUtil.toJson(data);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	private List<Object> addMissingDatesData(List<Object> list, Long dateFrom, Long dateTo) {
		List<Object> completeList  = new ArrayList<Object>();
		int totalDayRecord = DateUtils.getDaysBetween(dateFrom, dateTo) + 1; // +1 means including today date
		if(list.size() >= totalDayRecord) {
			return list;
		} else {
			for (int index = 0; index < totalDayRecord; index++) {
				Long recordDay = (index * DateUtils.ONE_DAY_MILLIS + dateFrom);
				if(list.size() != 0) {
					Object [] objArray = (Object []) list.get(0);
					Long timeToCheck = ((BigInteger) objArray[1]).longValue();
					if(DateUtils.isExistInRDay(timeToCheck, recordDay)) {
						completeList.add(objArray);
						list.remove(0);
					} else {
						Object[] newObjArray = new Object[]{0, recordDay, 0};
						completeList.add(newObjArray);
					}
				} else {
					Object[] newObjArray = new Object[]{0, recordDay, 0};
					completeList.add(newObjArray);
				}
			}
		}
		return completeList;
	}

	private List<Object> addMissingDatesDataActivityMinutes(List<Object> list, Long dateFrom, Long dateTo) {
		List<Object> completeList  = new ArrayList<Object>();
		int totalDayRecord = DateUtils.getDaysBetween(dateFrom, dateTo) + 1; // +1 means including today date
		if(list.size() >= totalDayRecord) {
			return list;
		}

		for (int index = 0; index < totalDayRecord; index++) {
			Long recordDay = (index * DateUtils.ONE_DAY_MILLIS + dateFrom);

			if (list.size() != 0) {
				Object[] objArray = (Object[]) (list.size() > index ? list.get(index) : null);
				if (objArray != null) {
					completeList.add(objArray);
				} else {
					Object[] newObjArray = new Object[]{null, recordDay, null, null, null};
					completeList.add(newObjArray);
				}
			}
		}
		return completeList;
	}

	private List<Object> addMissingDatesDataActivities(List<Object> list, Long dateFrom, Long dateTo) throws ParseException {
		List<Object> completeList = new ArrayList<Object>();

		String date1, date2, date3;
		date1 = DateUtils.getFormattedDate(dateFrom, "yyyy-MM-dd");
		date2 = DateUtils.getFormattedDate(dateFrom + DateUtils.ONE_DAY_MILLIS, "yyyy-MM-dd");
		date3 = DateUtils.getFormattedDate(dateFrom + (DateUtils.ONE_DAY_MILLIS * 2), "yyyy-MM-dd");
		List<String> dateList = new ArrayList<String>();
		dateList.add(date1);
		dateList.add(date2);
		dateList.add(date3);

		Map<String, Object> map = getMapFromList(list);
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();

		for (int index = 0; index < dateList.size(); index++) {
			if(map.get(dateList.get(index)) == null){
				dataMap.put(dateList.get(index),map.get(dateList.get(index)));
			} else {
				dataMap.put(dateList.get(index),map.get(dateList.get(index)));
			}
		}
		completeList = getListFromMap(dataMap);

		return completeList;
	}

	private List<Object> getActivityDataList(List<Object> activityDataList) {
		List<Object> finalActivityDataList = new ArrayList<Object>();
		for (Object object : activityDataList) {
			Object[] objArray = (Object[]) object;
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			map.put(AppConstants.ChartServiceConstants.STEPS_PER_DAY.getValue(), objArray[0] == null ? 0 : objArray[0]);
			Long timestamp = 0L;
			if (objArray[1] instanceof Long) {
				timestamp = (Long) objArray[1];
			} else if (objArray[1] instanceof BigInteger) {
				timestamp = ((BigInteger) objArray[1]).longValue();
			}
			timestamp = DateUtils.getStartOfDayTime(timestamp) + CommonUtils.getOffset();
			map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), timestamp);
			map.put(AppConstants.ChartServiceConstants.DATE.getValue(), objArray[2] == null ? 0 : objArray[2]);
			finalActivityDataList.add(map);
		}
		return finalActivityDataList;
	}

	private List<Object> getActivityMinutesDataListFacilityTimeOffset(List<Object> activityDataList, long patientId) {
		List<Object> finalActivityDataList = new ArrayList<Object>();
		for (Object object : activityDataList) {
			Object [] objArray = (Object [])object;
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			map.put(AppConstants.ChartServiceConstants.ACTIVITY_MINUTES.getValue(), objArray[0] == null? null: objArray[0]);
			Long objTimeStamp = 0L;
			try {
				if (objArray[1] instanceof Long) {
					objTimeStamp = (Long) objArray[1];
				} else if (objArray[1] instanceof BigInteger) {
					objTimeStamp = ((BigInteger) objArray[1]).longValue();
				}
			} catch(Exception e){

			}

//			map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), DateUtils.getStartOfDayTime(objTimeStamp));
			map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), objTimeStamp);
			map.put(AppConstants.ChartServiceConstants.DATE.getValue(), objArray[2] == null? null: objArray[2]);
			map.put(AppConstants.ChartServiceConstants.ACTIVITY_MINUTES_TYPE.getValue(), objArray[3]);
			map.put(AppConstants.ChartServiceConstants.ACTIVITY_MINUTES_TYPES.getValue(), objArray[4]);
			finalActivityDataList.add(map);
		}
		return finalActivityDataList;
	}

	private List<Object> getTookMedsDataList(List<Object> medsDataList) {
		List<Object> finalMedsDataList = new ArrayList<Object>();
		for (Object object : medsDataList) {
			Object [] objArray = (Object [])object;
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			map.put(AppConstants.ChartServiceConstants.TOOK_MEDS_PERCENTAGE.getValue(), objArray[0] == null ? 0: objArray[0]);
			map.put(AppConstants.ChartServiceConstants.TOTAL.getValue(), objArray[1] == null ? 0: objArray[1]);
			map.put(AppConstants.ChartServiceConstants.MEDS_TAKEN.getValue(), objArray[2] == null ? 0: objArray[2]);
			Long timestamp = 0L;
			if(objArray[3] instanceof Long) {
				timestamp = (Long)objArray[1];
			} else if (objArray[3] instanceof BigInteger) {
				timestamp = ((BigInteger)objArray[3]).longValue();
			}
			timestamp = DateUtils.getStartOfDayTime(timestamp) + CommonUtils.getOffset();
			map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), timestamp);
			map.put(AppConstants.ChartServiceConstants.DATE.getValue(), objArray[4] == null? 0: objArray[4]);
			finalMedsDataList.add(map);
		}
		return finalMedsDataList;
	}

	@Action(value="fetchMonthlyWeightChart")
	public String fetchMonthlyWeightChart() throws ParseException{
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(monthlyTrendDateString != null && !monthlyTrendDateString.isEmpty()){
			String[] dateParts = monthlyTrendDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			List<Object> weightDetialList = new LogDAO().getWeightDataForChart(patientId, dateFrom, dateTo);
			//weightDetialList = addMissingDatesData(weightDetialList, dateFrom, dateTo);
			weightDetialList = getWeightDataList(weightDetialList);
			List<Object> minMax = new ArrayList<Object>();
			minMax.add(dateFrom + CommonUtils.getOffset());
			minMax.add(dateTo + CommonUtils.getOffset());
			dataMap.put(AppConstants.ServicesConstants.DATA.name(), weightDetialList);
			Target target = new TargetDAO().getTargetByPatientId(patientId);
			dataMap.put(AppConstants.ServicesConstants.STARTING_WEIGHT.name(), target.getStartingWeight());
			dataMap.put(AppConstants.ServicesConstants.TARGET_WEIGHT.name(), target.getTargetWeight());
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	private List<Object> getWeightDataList(List<Object> weightDetialList) {
		List<Object> finalWeightDetialList = new ArrayList<Object>();
		for (Object object : weightDetialList) {
			Object [] objArray = (Object [])object;
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			float weight = 0f;
			if(objArray[0] instanceof Integer) {
				weight = ((Integer)objArray[0]).floatValue();
			} else if(objArray[0] instanceof Double){
				weight = ((Double)objArray[0]).floatValue();
			}
			weight = Math.round(weight);
			map.put(AppConstants.ChartServiceConstants.WEIGHT_LOST.getValue(), objArray[0] == null? 0: weight);
			Long timestamp = 0L;
			if(objArray[1] instanceof Long) {
				timestamp = (Long)objArray[1];
			} else if (objArray[1] instanceof BigInteger) {
				timestamp = ((BigInteger)objArray[1]).longValue();
			}
			timestamp = DateUtils.getStartOfDayTime(timestamp) + CommonUtils.getOffset();

			map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), timestamp);
			map.put(AppConstants.ChartServiceConstants.DATE.getValue(), objArray[2] == null? 0: objArray[2]);
			finalWeightDetialList.add(map);
		}
		return finalWeightDetialList;
	}

	/*-------------------------------------------------------------------*/

	/*---------------------THREE DAYS ACTIONS ----------------------------*/

	@Action(value="fetchThreeDaysMealsData")
	public String fetchThreeDaysMealsData() throws ParseException{

		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();

		if(threeDaysDateString != null && !threeDaysDateString.isEmpty()){
			String[] dateParts = threeDaysDateString.split("-");
			Long dateFrom  = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo  = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			Object objTrend = logDAO.getTrendsByPatientId(patientId, dateFrom, dateTo);
			Object objAverageTrend = logDAO.getAverageOfEachMealSummaryChart(patientId, dateFrom, dateTo);
			Target target = new TargetDAO().getTargetByPatientId(patientId);
			float mealTargets[] = CommonUtils.getMealTargets(target);
			dataMap.put(AppConstants.LOGS.THREE_DAYS_MEAL.getValue(), objTrend);
			dataMap.put(AppConstants.LOGS.MEAL_AVG.getValue(), objAverageTrend);
			dataMap.put(AppConstants.LOGS.TARGET.getValue(), mealTargets);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchThreeDaysMedicationData")
	public String fetchThreeDaysMedicationData() throws ParseException{
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(threeDaysDateString != null && !threeDaysDateString.isEmpty()){
			String[] dateParts = threeDaysDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			List<Object> tookMedsDetailList = logDAO.getTookMedsDataByPatientId(patientId, dateFrom, dateTo);
			tookMedsDetailList = addMissingDatesData(tookMedsDetailList, dateFrom, dateTo);
			tookMedsDetailList = getTookMedList(tookMedsDetailList);
			List<Object> minMax = new ArrayList<Object>();
			minMax.add(dateFrom + CommonUtils.getOffset());
			minMax.add(dateTo + CommonUtils.getOffset());
			dataMap.put(AppConstants.LOG_TYPE_MEDICATION, tookMedsDetailList);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchThreeDaysGlucoseData")
	public String fetchThreeDaysGlucoseData(){
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(threeDaysDateString != null && !threeDaysDateString.isEmpty()){
			String[] dateParts = threeDaysDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			Object glucoseDataAfterMeal = logDAO.getGlucoseSummaryAfterMealByPatientId(patientId, dateFrom, dateTo);
			Object glucoseDataBeforeMeal = logDAO.getGlucoseSummaryBeforeMealByPatientId(patientId, dateFrom, dateTo);
			Object objGlucose = logDAO.getGlucoseSummaryByPatientId(patientId, dateFrom, dateTo);
			Target target = new TargetDAO().getTargetByPatientId(patientId);
			int beforeTargetMin = 80;
			int beforeTargetMax = 130;
			int afterTargetMin = 80;
			int afterTargetMax = 180;
			if(target != null) {
				beforeTargetMin = target.getGlucoseMin2() == null? 80: target.getGlucoseMin2();
				beforeTargetMax = target.getGlucoseMax2() == null? 130: target.getGlucoseMax2();
				afterTargetMin = target.getGlucoseMin() == null? 80: target.getGlucoseMin();
				afterTargetMax = target.getGlucoseMax() == null? 180: target.getGlucoseMax();
			}
			dataMap.put(AppConstants.LOGS.GLUCOSE.getValue(), objGlucose);
			dataMap.put("beforeMinTarget", beforeTargetMin);
			dataMap.put("beforeMaxTarget", beforeTargetMax);
			dataMap.put("afterMinTarget", afterTargetMin);
			dataMap.put("afterMaxTarget", afterTargetMax);
			dataMap.put(AppConstants.LOGS.AFTER_MEAL_GLUCOSE.getValue(), glucoseDataAfterMeal);
			dataMap.put(AppConstants.LOGS.NOT_AFTER_MEAL_GLUCOSE.getValue(), glucoseDataBeforeMeal);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchThreeDaysScatterGlucoseData")
	public String fetchThreeDaysScatterGlucoseData() throws ParseException {
		/*List & Map for data*/
		Map<String, List> data = new LinkedHashMap<String, List>();
		List<List> beforeMealGlucoseTargets = new ArrayList<List>();

		List<List> glucoseUnderTarget = new ArrayList<List>() ;
		List<List> glucoseAboveTarget = new ArrayList<List>() ;
		List<List> glucoseAtTarget = new ArrayList<List>();
		List<Double> combineGlucoseTarget = new ArrayList<Double>();
		List<List> afterGlucoseTargets = new ArrayList<List>() ;

		Target target = new TargetDAO().getTargetByPatientId(patientId);
		float afterMealGlucoseMin = 0f;
		float afterMealGlucoseMax = 0f;

		if(target != null) {
			glucoseMin = target.getGlucoseMin2();
			glucoseMax = target.getGlucoseMax2();
			afterMealGlucoseMin = target.getGlucoseMin();
			afterMealGlucoseMax = target.getGlucoseMax();
		}

		/*Set MinMax Range*/
		if(!ObjectUtils.isEmpty(threeDaysDateString)){
			String[] dateParts = threeDaysDateString.split("-");
			Long startTime = Long.parseLong(dateParts[0]);
			Long endTime = Long.parseLong(dateParts[1]);
			List<Integer> totalTests = new ArrayList<Integer>();
			List<Integer> avgBGL = new ArrayList<Integer>();
			Integer totalBeforeMealBGL = 0, totalAfterMealBGL = 0;

			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(DateUtils.getFormattedDate(startTime, "yyyy-MM-dd"));
			List<Long> minMaxRang = getRange(dateToGetChart, 3);

			List<Object> glucoseValueList = logDAO.getAvgGlucoseValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> glucoseBeforeMealValueList = logDAO.getAvgGlucoseBeforeMealValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> glucoseAfterMealValueList = logDAO.getAvgGlucoseAfterMealValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> stdBeforeMeal = logDAO.getAvgGlucoseSTDBeforerMealValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> stdAfterMeal = logDAO.getAvgGlucoseSTDAfterMealValuesPerDayByPatientId(patientId, startTime, endTime);
			glucoseValueList = adjustedGlucoseLogValueListByTime(glucoseValueList, minMaxRang, patientId);
			totalBeforeMealBGL = getTotalBGLFromList(glucoseBeforeMealValueList);
			totalAfterMealBGL = getTotalBGLFromList(glucoseAfterMealValueList);

			// Further Calculations / List due to json map get list
			totalTests.add(glucoseBeforeMealValueList.size());
			totalTests.add(glucoseAfterMealValueList.size());
			avgBGL.add(totalBeforeMealBGL / (totalTests.get(0) != 0 ? totalTests.get(0) : 1));
			avgBGL.add(totalAfterMealBGL / (totalTests.get(1) != 0 ? totalTests.get(1) : 1));
			List<Double> avgPrePostList = getAveragePrePostBGL(glucoseValueList);

			for(Object obj: glucoseValueList){
				List<Object> valuePair = new ArrayList<Object>() ;
				Object[] objArray = (Object[]) obj;

				double GlucoseFloatValue = Double.parseDouble(objArray[1].toString());
				String glucoseMealTime = objArray[2].toString();
				valuePair.add(objArray[0]);
				valuePair.add(GlucoseFloatValue) ;
				valuePair.add(glucoseMealTime);
				valuePair.add(objArray[6]);
				valuePair.add(objArray[7]); // printing time in tooltip

				String glucoseTime = ObjectUtils.nullSafe(objArray[2]);
				if(ObjectUtils.isEmpty(glucoseTime) || glucoseTime.equalsIgnoreCase(AppConstants.GLUCOSE_BEFORE_MEAL)) {
					if(GlucoseFloatValue < glucoseMin) {
						glucoseUnderTarget.add(valuePair);
					} else if(GlucoseFloatValue > glucoseMax) {
						glucoseAboveTarget.add(valuePair) ;
					} else {
						glucoseAtTarget.add(valuePair) ;
					}
				} else {
					if(GlucoseFloatValue < afterMealGlucoseMin) {
						glucoseUnderTarget.add(valuePair);
					} else if(GlucoseFloatValue > afterMealGlucoseMax) {
						glucoseAboveTarget.add(valuePair) ;
					} else {
						glucoseAtTarget.add(valuePair) ;
					}

				}
				combineGlucoseTarget.add(GlucoseFloatValue);
			}

			List<Object> beforeGlucoseTargetsObjects = new ArrayList<Object>();
			beforeGlucoseTargetsObjects.add(glucoseMin);
			beforeGlucoseTargetsObjects.add(glucoseMax);
			beforeMealGlucoseTargets.add(beforeGlucoseTargetsObjects);

			List<Object> afterMealeTargetsObjects = new ArrayList<Object>();
			afterMealeTargetsObjects.add(afterMealGlucoseMin);
			afterMealeTargetsObjects.add(afterMealGlucoseMax);
			afterGlucoseTargets.add(afterMealeTargetsObjects);

			data.put("High", glucoseAboveTarget) ;
			data.put("Low", glucoseUnderTarget) ;
			data.put("Normal", glucoseAtTarget) ;
			data.put("beforeMealGlucoseTargets", beforeMealGlucoseTargets);
			data.put("afterMealGlucoseTargets", afterGlucoseTargets);
			data.put("MinMax", minMaxRang);
			data.put("maxYAxis", combineGlucoseTarget);
			data.put("TotalTests", totalTests);
			data.put("AvgBGL", avgBGL);
			data.put("AvgPrePostBGL", avgPrePostList);
			data.put("stdBeforMeal", stdBeforeMeal);
			data.put("stdAfterMeal", stdAfterMeal);
			logReportData = JsonUtil.toJson(data);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchThreeDaysActivityChart")
	public String fetchThreeDaysActivityChart() throws ParseException{
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(threeDaysDateString != null && !threeDaysDateString.isEmpty()){
			String[] dateParts = threeDaysDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			List<Object> activityDetialList = new LogDAO().getActivityDetailData(patientId, dateFrom, dateTo);
			activityDetialList = addMissingDatesData(activityDetialList, dateFrom, dateTo);
			activityDetialList = getActivityDataList(activityDetialList);
			List<Object> minMax = new ArrayList<Object>();
			minMax.add(dateFrom);
			minMax.add(dateTo);
			dataMap.put(AppConstants.ServicesConstants.DATA.name(), activityDetialList);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchThreeDaysWeightChart")
	public String fetchThreeDaysWeightChart() throws ParseException{
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(threeDaysDateString != null && !threeDaysDateString.isEmpty()){
			String[] dateParts = threeDaysDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			List<Object> weightDetialList = new LogDAO().getWeightDataForChart(patientId, dateFrom, dateTo);
			//weightDetialList = addMissingDatesData(weightDetialList, dateFrom, dateTo);
			weightDetialList = getWeightDataList(weightDetialList);
			List<Object> minMax = new ArrayList<Object>();
			minMax.add(dateFrom + CommonUtils.getOffset());
			minMax.add(dateTo + CommonUtils.getOffset());
			dataMap.put(AppConstants.ServicesConstants.DATA.name(), weightDetialList);
			Target target = new TargetDAO().getTargetByPatientId(patientId);
			dataMap.put(AppConstants.ServicesConstants.STARTING_WEIGHT.name(), target.getStartingWeight());
			dataMap.put(AppConstants.ServicesConstants.TARGET_WEIGHT.name(), target.getTargetWeight());
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchThreeDaysCombineGlucoseData")
	public String fetchThreeDaysCombineGlucoseData() throws ParseException{
		/*List & Map for data*/
		Map<String, List> data = new LinkedHashMap<String, List>();
		List<List> afterMealGlucoseTargets = new ArrayList<List>();

		List<List> glucoseUnderTarget = new ArrayList<List>() ;
		List<List> glucoseAboveTarget = new ArrayList<List>() ;
		List<List> glucoseAtTarget = new ArrayList<List>();
		List<List> glucoseTargets = new ArrayList<List>() ;

		Target target = new TargetDAO().getTargetByPatientId(patientId);
		float beforeMealGlucoseMin = 0f;
		float beforeMealGlucoseMax = 0f;

		if(target != null) {
			glucoseMin = target.getGlucoseMin2();
			glucoseMax = target.getGlucoseMax2();
			beforeMealGlucoseMin = target.getGlucoseMin();
			beforeMealGlucoseMax = target.getGlucoseMax();
		}

		/*Set MinMax Range*/
		if(threeDaysDateString != null && !threeDaysDateString.isEmpty()){
			String[] dateParts = threeDaysDateString.split("-");
			Long startTime = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long endTime = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));

			List<Object> MinMaxRang = new ArrayList<Object>() ;
			MinMaxRang.add(startTime + CommonUtils.getOffset());
			MinMaxRang.add(endTime + CommonUtils.getOffset());

			List<Object> glucoseValueList = logDAO.getGlucoseValuesPerDayByPatientId(patientId, startTime, endTime) ;
			for(Object obj: glucoseValueList){
				List<Object> valuePair = new ArrayList<Object>() ;
				Object[] objArray = (Object[]) obj;

				valuePair.add((Long)objArray[2] + CommonUtils.getOffset()) ;
				float GlucoseFloatValue = Float.parseFloat((String) objArray[1]);
				valuePair.add(GlucoseFloatValue) ;

				String glucoseTime = ObjectUtils.nullSafe(objArray[4]);
				if(ObjectUtils.isEmpty(glucoseTime) || glucoseTime.equalsIgnoreCase(AppConstants.GLUCOSE_BEFORE_MEAL)) {
					if(GlucoseFloatValue < glucoseMin) {
						glucoseUnderTarget.add(valuePair);
					} else if(GlucoseFloatValue > glucoseMax) {
						glucoseAboveTarget.add(valuePair) ;
					} else {
						glucoseAtTarget.add(valuePair) ;
					}
				} else {
					if(GlucoseFloatValue < beforeMealGlucoseMin) {
						glucoseUnderTarget.add(valuePair);
					} else if(GlucoseFloatValue > beforeMealGlucoseMax) {
						glucoseAboveTarget.add(valuePair) ;
					} else {
						glucoseAtTarget.add(valuePair) ;
					}

				}
			}

			List<Object> glucoseTargetsObjects = new ArrayList<Object>();
			glucoseTargetsObjects.add(glucoseMin);
			glucoseTargetsObjects.add(glucoseMax);
			afterMealGlucoseTargets.add(glucoseTargetsObjects);

			List<Object> beforeMealeTargetsObjects = new ArrayList<Object>();
			beforeMealeTargetsObjects.add(beforeMealGlucoseMin);
			beforeMealeTargetsObjects.add(beforeMealGlucoseMax);
			glucoseTargets.add(beforeMealeTargetsObjects);

			data.put("High", glucoseAboveTarget) ;
			data.put("Low", glucoseUnderTarget) ;
			data.put("Normal", glucoseAtTarget) ;
			data.put("afterMealGlucoseTargets", afterMealGlucoseTargets);
			data.put("beforeMealGlucoseTargets", glucoseTargets);
			data.put("MinMax",MinMaxRang ) ;
			logReportData = JsonUtil.toJson(data);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value = "fetchThreeDaysCombineActivityData")
	public String fetchThreeDaysCombineActivityData() throws ParseException {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(!ObjectUtils.isEmpty(threeDaysDateString)) {
			String[] dateParts	 = threeDaysDateString.split("-");
			Long dateFrom = Long.parseLong(dateParts[0]);
			Long dateTo = Long.parseLong(dateParts[1]);
			String formattedDateString = DateUtils.getFormattedDate(dateFrom, "yyyy-MM-dd");
			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(formattedDateString);
			List<Long> minMax = getRange(dateToGetChart, 4, patientId);

			List<Object> misfitHourlyLogs = logDAO.getMisfitActivityHourlyLogsByPatientId(patientId, dateFrom, dateTo);
//			misfitHourlyLogs = CommonUtils.addFacilityTimeOffset(misfitHourlyLogs, patientId);
			List<Object> misfitHourlySumLogs = logDAO.getMisfitActivityHourlyLogsSumByPatientId(patientId, dateFrom, dateTo);
//			misfitHourlySumLogs = CommonUtils.addFacilityTimeOffset(misfitHourlySumLogs, patientId);

			if (noOfDays == 3){
				misfitHourlySumLogs = addMissingDatesDataActivities(misfitHourlySumLogs, dateFrom, dateTo);
			}

			if(misfitHourlyLogs != null) {
				misfitHourlyLogs = getActivityLogsDataListDailyThreeDays(misfitHourlyLogs, patientId);
			}

			dataMap.put(AppConstants.ServicesConstants.DATA.name(), misfitHourlyLogs);
			dataMap.put(AppConstants.ChartServiceConstants.STEPS.name(), misfitHourlySumLogs);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			dataMap.put(AppConstants.ServicesConstants.MAXYAXIS.name(), getMaxXAxisForChart(misfitHourlyLogs, "threeDays"));
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value = "fetchThreeDaysCombineActivityMinutesData")
	public String fetchThreeDaysCombineActivityMinutesData() throws ParseException {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(!ObjectUtils.isEmpty(threeDaysDateString)) {
			String[] dateParts = threeDaysDateString.split("-");
			Long dateFrom = Long.parseLong(dateParts[0]);
			Long dateTo = Long.parseLong(dateParts[1]);
			String formattedDateString = DateUtils.getFormattedDate(dateFrom, "yyyy-MM-dd");
			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(formattedDateString);
			List<Long> minMax = getRange(dateToGetChart, 4, patientId);
			List<ActivityLog> activityLogList = logDAO.getMinutesActivityDetailData(patientId, dateFrom, dateTo);
//			activityLogList = CommonUtils.addFacilityTimeOffsetInActivityLog(activityLogList, patientId);

			dataMap.put(AppConstants.ServicesConstants.DATA.name(), activityLogList);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			logReportData = JsonUtil.toJsonExcludedNull(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	private List<Object> getActivityLogsDataList(List<Object> misfitLogDataList, long patientId) {
		List<Object> finalMisfitLogsDataList = new ArrayList<Object>();
		for (Object object : misfitLogDataList) {
			Object [] objArray = (Object [])object;
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			Long objTimestamp = 0L;
			if(objArray[1] instanceof Long) {
				objTimestamp = (Long)objArray[1];
			} else if (objArray[1] instanceof BigInteger) {
				objTimestamp = ((BigInteger)objArray[1]).longValue();
			}
			map.put(AppConstants.ChartServiceConstants.STEPS.getValue(), objArray[0] == null? 0: objArray[0]);
			map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), objArray[1] == null? 0: DateUtils.getStartOfDayTime(objTimestamp));
			finalMisfitLogsDataList.add(map);
		}
		return finalMisfitLogsDataList;
	}

	private List<Object> getActivityLogsDataListDailyThreeDays(List<Object> misfitLogDataList, long patientId) {
		List<Object> finalMisfitLogsDataList = new ArrayList<Object>();
		for (Object object : misfitLogDataList) {
			Object [] objArray = (Object [])object;
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			Long objTimestamp = 0L;
			if(objArray[1] instanceof Long) {
				objTimestamp = (Long)objArray[1];
			} else if (objArray[1] instanceof BigInteger) {
				objTimestamp = ((BigInteger)objArray[1]).longValue();
			}
			map.put(AppConstants.ChartServiceConstants.STEPS.getValue(), objArray[0] == null? 0: objArray[0]);
			map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), objArray[1] == null? 0: objTimestamp);
			finalMisfitLogsDataList.add(map);
		}
		return finalMisfitLogsDataList;
	}

	@Action(value = "fetchThreeDaysCombineCarbsData")
	public String fetchThreeDaysCombineCarbsData() {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(threeDaysDateString != null && !threeDaysDateString.isEmpty()) {
			String[] dateParts	 = threeDaysDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			List<Object> carbsLevelData = logDAO.getCarbsValuesPerDayByPatientId(patientId, dateFrom, dateTo);
			if(carbsLevelData != null) {
				carbsLevelData = getCarbsLevelDataList(carbsLevelData);
			}
			List<Object> minMax = new ArrayList<Object>();
			minMax.add(dateFrom + CommonUtils.getOffset());
			minMax.add(dateTo + CommonUtils.getOffset());
			dataMap.put(AppConstants.ServicesConstants.DATA.name(), carbsLevelData);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	private List<Object> getCarbsLevelDataList(List<Object> carbsLevelData) {
		List<Object> finalCarbsLevelData = new ArrayList<Object>();
		for (Object object : carbsLevelData) {
			Object [] objArray = (Object [])object;
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			Long timestamp = 0L;
			if(objArray[3] instanceof Long) {
				timestamp = (Long)objArray[3];
			} else if (objArray[3] instanceof BigInteger) {
				timestamp = ((BigInteger)objArray[3]).longValue();
			}
			timestamp = timestamp + CommonUtils.getOffset();

			map.put(AppConstants.ChartServiceConstants.CARBS.getValue(), objArray[1] == null? 0: Math.round(Double.parseDouble(objArray[1].toString())));
			map.put(AppConstants.ChartServiceConstants.TYPE.getValue(), objArray[2] == null? 0: objArray[2]);
			map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), objArray[3] == null? 0: timestamp);
			map.put(AppConstants.ChartServiceConstants.HAS_MISSING_FOOD.getValue(), objArray[4] == null? 0: objArray[4]);
			map.put(AppConstants.ChartServiceConstants.ID.getValue(), objArray[5] == null? 0: objArray[5]);
			finalCarbsLevelData.add(map);
		}
		return finalCarbsLevelData;
	}

	@Action(value = "fetchThreeDaysCombineMedsData")
	public String fetchThreeDaysCombineMedsData() {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(threeDaysDateString != null && !threeDaysDateString.isEmpty()) {
			String[] dateParts	 = threeDaysDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			List<Object> medsTimeData = logDAO.getMedsTimeByPatientId(patientId, dateFrom, dateTo);
			if(medsTimeData != null) {
				medsTimeData = getMedTimeDataList(medsTimeData);
			}
			String appPath = ObjectUtils.nullSafe(new PreferencesDAO().getPreference(AppConstants.PreferencesNames.APPLICATION_PATH.getValue()));
			List<Object> minMax = new ArrayList<Object>();
			minMax.add(dateFrom + CommonUtils.getOffset());
			minMax.add(dateTo + CommonUtils.getOffset());
			dataMap.put(AppConstants.ServicesConstants.DATA.name(), medsTimeData);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			dataMap.put(AppConstants.PreferencesNames.APPLICATION_PATH.getValue(), appPath);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	public List<Object> getMedTimeDataList(List<Object> objects) {
		List<Object> finalMedsData = new ArrayList<Object>();
		for (Object object : objects) {
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			Long timestamp = 0L;
			if(object instanceof Long) {
				timestamp = (Long)object;
			} else if (object instanceof BigInteger) {
				timestamp = ((BigInteger)object).longValue();
			}
			timestamp = timestamp + CommonUtils.getOffset();
			map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), object == null? 0: timestamp);
			finalMedsData.add(map);
		}
		return finalMedsData;
	}
	/*-------------------------------------------------------------------*/

	/*---------------------FOURTEEN DAYS ACTIONS ----------------------------*/

	@Action(value="fetchFourteenDaysMealsData")
	public String fetchFourteenDaysMealsData() throws ParseException{

		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();

		if(fourteenDaysReportDateString != null && !fourteenDaysReportDateString.isEmpty()){
			String[] dateParts = fourteenDaysReportDateString.split("-");
			Long dateFrom  = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo  = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			Object objTrend = logDAO.getTrendsByPatientId(patientId, dateFrom, dateTo);
			Object objAverageTrend = logDAO.getAverageOfEachMealSummaryChart(patientId, dateFrom, dateTo);
			Target target = new TargetDAO().getTargetByPatientId(patientId);
			float mealTargets[] = CommonUtils.getMealTargets(target);
			dataMap.put(AppConstants.LOGS.FOURTEEN_DAYS_MEAL.getValue(), objTrend);
			dataMap.put(AppConstants.LOGS.MEAL_AVG.getValue(), objAverageTrend);
			dataMap.put(AppConstants.LOGS.TARGET.getValue(), mealTargets);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchFourteenDaysMedicationData")
	public String fetchFourteenDaysMedicationData() throws ParseException{
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(fourteenDaysReportDateString != null && !fourteenDaysReportDateString.isEmpty()){
			String[] dateParts = fourteenDaysReportDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			List<Object> tookMedsDetailList = logDAO.getTookMedsDataByPatientId(patientId, dateFrom, dateTo);
			tookMedsDetailList = addMissingDatesData(tookMedsDetailList, dateFrom, dateTo);
			tookMedsDetailList = getTookMedList(tookMedsDetailList);
			List<Object> minMax = new ArrayList<Object>();
			minMax.add(dateFrom + CommonUtils.getOffset());
			minMax.add(dateTo + CommonUtils.getOffset());
			dataMap.put(AppConstants.LOG_TYPE_MEDICATION, tookMedsDetailList);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	private List<Object> getTookMedList(List<Object> tookMedDataList) {
		List<Object> finalActivityDataList = new ArrayList<Object>();
		for (Object object : tookMedDataList) {
			Object [] objArray = (Object [])object;
			Long timestamp = 0L;
			if(objArray[1] instanceof Long) {
				timestamp = (Long)objArray[1];
			} else if (objArray[1] instanceof BigInteger) {
				timestamp = ((BigInteger)objArray[1]).longValue();
			}
			timestamp = DateUtils.getStartOfDayTime(timestamp) + CommonUtils.getOffset();
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			map.put(AppConstants.ChartServiceConstants.MEDS_TAKEN.getValue(), objArray[0] == null? 0: objArray[0]);
			map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), timestamp);
			finalActivityDataList.add(map);
		}
		return finalActivityDataList;
	}

	@Action(value="fetchFourteenDaysGlucoseData")
	public String fetchFourteenDaysGlucoseData(){
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(fourteenDaysReportDateString != null && !fourteenDaysReportDateString.isEmpty()){
			String[] dateParts = fourteenDaysReportDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			Object glucoseDataAfterMeal = logDAO.getGlucoseSummaryAfterMealByPatientId(patientId, dateFrom, dateTo);
			Object glucoseDataBeforeMeal = logDAO.getGlucoseSummaryBeforeMealByPatientId(patientId, dateFrom, dateTo);
			Object objGlucose = logDAO.getGlucoseSummaryByPatientId(patientId, dateFrom, dateTo);
			Target target = new TargetDAO().getTargetByPatientId(patientId);
			int beforeTargetMin = 80;
			int beforeTargetMax = 130;
			int afterTargetMin = 80;
			int afterTargetMax = 180;
			if(target != null) {
				beforeTargetMin = target.getGlucoseMin2() == null? 80: target.getGlucoseMin2();
				beforeTargetMax = target.getGlucoseMax2() == null? 130: target.getGlucoseMax2();
				afterTargetMin = target.getGlucoseMin() == null? 80: target.getGlucoseMin();
				afterTargetMax = target.getGlucoseMax() == null? 180: target.getGlucoseMax();
			}
			dataMap.put(AppConstants.LOGS.GLUCOSE.getValue(), objGlucose);
			dataMap.put("beforeMinTarget", beforeTargetMin);
			dataMap.put("beforeMaxTarget", beforeTargetMax);
			dataMap.put("afterMinTarget", afterTargetMin);
			dataMap.put("afterMaxTarget", afterTargetMax);
			dataMap.put(AppConstants.LOGS.AFTER_MEAL_GLUCOSE.getValue(), glucoseDataAfterMeal);
			dataMap.put(AppConstants.LOGS.NOT_AFTER_MEAL_GLUCOSE.getValue(), glucoseDataBeforeMeal);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchFourteenDaysActivityChart")
	public String fetchFourteenDaysActivityChart() throws ParseException{
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(fourteenDaysReportDateString != null && !fourteenDaysReportDateString.isEmpty()){
			String[] dateParts = fourteenDaysReportDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			List<Object> activityDetialList = new LogDAO().getActivityDetailData(patientId, dateFrom, dateTo);
			activityDetialList = addMissingDatesData(activityDetialList, dateFrom, dateTo);
			activityDetialList = getActivityDataList(activityDetialList);
			List<Object> minMax = new ArrayList<Object>();
			minMax.add(dateFrom + CommonUtils.getOffset());
			minMax.add(dateTo + CommonUtils.getOffset());
			dataMap.put(AppConstants.ServicesConstants.DATA.name(), activityDetialList);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value = "fetchFouteenDaysCombineActivityMinutesData")
	public String fetchFouteenDaysCombineActivityMinutesData() throws ParseException {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(!ObjectUtils.isEmpty(fourteenDaysReportDateString)) {
			String[] dateParts = fourteenDaysReportDateString.split("-");
			Long dateFrom = Long.parseLong(dateParts[0]);
			Long dateTo = Long.parseLong(dateParts[1]);
			String formattedDateString = DateUtils.getFormattedDate(dateFrom, "yyyy-MM-dd");
			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(formattedDateString);
			List<Long> minMaxRang = getRange(dateToGetChart, 14, patientId);
			List<Object> activityLogList = logDAO.getAvgActivityDetailDataObjects(patientId, dateFrom, dateTo, 14);
			activityLogList = adjustedLogValueListByTime(activityLogList, minMaxRang, patientId);

			dataMap.put(AppConstants.ServicesConstants.DATA.name(), activityLogList);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMaxRang);
			logReportData = JsonUtil.toJsonExcludedNull(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchFourteenDaysWeightChart")
	public String fetchFourteenDaysWeightChart() throws ParseException{
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(fourteenDaysReportDateString != null && !fourteenDaysReportDateString.isEmpty()){
			String[] dateParts = fourteenDaysReportDateString.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			List<Object> weightDetialList = new LogDAO().getWeightDataForChart(patientId, dateFrom, dateTo);
			//weightDetialList = addMissingDatesData(weightDetialList, dateFrom, dateTo);
			weightDetialList = getWeightDataList(weightDetialList);
			List<Object> minMax = new ArrayList<Object>();
			minMax.add(dateFrom + CommonUtils.getOffset());
			minMax.add(dateTo + CommonUtils.getOffset());
			dataMap.put(AppConstants.ServicesConstants.DATA.name(), weightDetialList);
			Target target = new TargetDAO().getTargetByPatientId(patientId);
			dataMap.put(AppConstants.ServicesConstants.STARTING_WEIGHT.name(), target.getStartingWeight());
			dataMap.put(AppConstants.ServicesConstants.TARGET_WEIGHT.name(), target.getTargetWeight());
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	@Action(value="fetchFourteenDaysScatterGlucoseData")
	public String fetchFourteenDaysScatterGlucoseData() throws ParseException {
		/*List & Map for data*/
		Map<String, List> data = new LinkedHashMap<String, List>();
		List<List> beforeMealGlucoseTargets = new ArrayList<List>();

		List<List> glucoseUnderTarget = new ArrayList<List>() ;
		List<List> glucoseAboveTarget = new ArrayList<List>() ;
		List<List> glucoseAtTarget = new ArrayList<List>();
		List<Double> combineGlucoseTarget = new ArrayList<Double>();
		List<List> afterGlucoseTargets = new ArrayList<List>() ;

		Target target = new TargetDAO().getTargetByPatientId(patientId);
		float afterMealGlucoseMin = 0f;
		float afterMealGlucoseMax = 0f;

		if(target != null) {
			glucoseMin = target.getGlucoseMin2();
			glucoseMax = target.getGlucoseMax2();
			afterMealGlucoseMin = target.getGlucoseMin();
			afterMealGlucoseMax = target.getGlucoseMax();
		}

		/*Set MinMax Range*/
		if(!ObjectUtils.isEmpty(fourteenDaysReportDateString)){
			String[] dateParts = fourteenDaysReportDateString.split("-");
			Long startTime = Long.parseLong(dateParts[0]);
			Long endTime = Long.parseLong(dateParts[1]);
			List<Integer> totalTests = new ArrayList<Integer>();
			List<Integer> avgBGL = new ArrayList<Integer>();
			Integer totalBeforeMealBGL = 0, totalAfterMealBGL = 0;

			String formattedDateString = DateUtils.getFormattedDate(startTime, "yyyy-MM-dd");

			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(formattedDateString);
			List<Long> minMaxRang = getRange(dateToGetChart, 14);

			List<Object> glucoseValueList = logDAO.getAvgGlucoseValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> glucoseBeforeMealValueList = logDAO.getAvgGlucoseBeforeMealValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> glucoseAfterMealValueList = logDAO.getAvgGlucoseAfterMealValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> stdBeforeMeal = logDAO.getAvgGlucoseSTDBeforerMealValuesPerDayByPatientId(patientId, startTime, endTime);
			List<Object> stdAfterMeal = logDAO.getAvgGlucoseSTDAfterMealValuesPerDayByPatientId(patientId, startTime, endTime);
			glucoseValueList = adjustedGlucoseLogValueListByTime(glucoseValueList, minMaxRang, patientId);
			totalBeforeMealBGL = getTotalBGLFromList(glucoseBeforeMealValueList);
			totalAfterMealBGL = getTotalBGLFromList(glucoseAfterMealValueList);

			// Further Calculations / List due to json map get list
			totalTests.add(glucoseBeforeMealValueList.size());
			totalTests.add(glucoseAfterMealValueList.size());
			avgBGL.add(totalBeforeMealBGL / (totalTests.get(0) != 0 ? totalTests.get(0) : 1));
			avgBGL.add(totalAfterMealBGL / (totalTests.get(1) != 0 ? totalTests.get(1) : 1));
			List<Double> avgPrePostList = getAveragePrePostBGL(glucoseValueList);

			for(Object obj: glucoseValueList){
				List<Object> valuePair = new ArrayList<Object>();
				Object[] objArray = (Object[]) obj;
				double GlucoseFloatValue = Double.parseDouble(objArray[1].toString());
				String glucoseMealTime = objArray[2].toString();

				valuePair.add(objArray[0]);
				valuePair.add(GlucoseFloatValue);
				valuePair.add(glucoseMealTime);
				valuePair.add(objArray[6]);
				valuePair.add(objArray[7]); // printing time in tooltip

				String glucoseTime = ObjectUtils.nullSafe(objArray[2]);

				if(ObjectUtils.isEmpty(glucoseTime) || glucoseTime.equalsIgnoreCase(AppConstants.GLUCOSE_BEFORE_MEAL)) {
					if(GlucoseFloatValue < glucoseMin) {
						glucoseUnderTarget.add(valuePair);
					} else if(GlucoseFloatValue > glucoseMax) {
						glucoseAboveTarget.add(valuePair);
					} else {
						glucoseAtTarget.add(valuePair);
					}
				} else {
					if(GlucoseFloatValue < afterMealGlucoseMin) {
						glucoseUnderTarget.add(valuePair);
					} else if(GlucoseFloatValue > afterMealGlucoseMax) {
						glucoseAboveTarget.add(valuePair);
					} else {
						glucoseAtTarget.add(valuePair);
					}
				}
				combineGlucoseTarget.add(GlucoseFloatValue);
			}

			List<Object> beforeGlucoseTargetsObjects = new ArrayList<Object>();
			beforeGlucoseTargetsObjects.add(glucoseMin);
			beforeGlucoseTargetsObjects.add(glucoseMax);
			beforeMealGlucoseTargets.add(beforeGlucoseTargetsObjects);

			List<Object> afterMealeTargetsObjects = new ArrayList<Object>();
			afterMealeTargetsObjects.add(afterMealGlucoseMin);
			afterMealeTargetsObjects.add(afterMealGlucoseMax);
			afterGlucoseTargets.add(afterMealeTargetsObjects);

			data.put("High", glucoseAboveTarget);
			data.put("Low", glucoseUnderTarget);
			data.put("Normal", glucoseAtTarget);
			data.put("beforeMealGlucoseTargets", beforeMealGlucoseTargets);
			data.put("afterMealGlucoseTargets", afterGlucoseTargets);
			data.put("MinMax", minMaxRang);
			data.put("maxYAxis", combineGlucoseTarget);
			data.put("TotalTests", totalTests);
			data.put("AvgBGL", avgBGL);
			data.put("AvgPrePostBGL", avgPrePostList);
			data.put("stdBeforMeal", stdBeforeMeal);
			data.put("stdAfterMeal", stdAfterMeal);
			logReportData = JsonUtil.toJson(data);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	private List getMaxTargetForChart(List glucoseAboveTarge){
		List<Long> maxValueList = new ArrayList<Long>();
		long maxValue = 0l;
		for(Object obj : glucoseAboveTarge){
			List<Object> list1= (List<Object>) obj;
			Double value = (Double) list1.get(1);
			if (maxValue < value.longValue()){
				maxValue = value.longValue();
			}
		}
		if(maxValue == 0){
			maxValue = 200l;
		}
		maxValueList.add(maxValue);
		return maxValueList;
	}

	private List<Object> adjustedLogValueListByTime(List<Object> logList, List<Long> minMaxRang, long patientId) {
		List<Object> avgGlucoseLogList = new ArrayList<Object>();
		long firstDateMilli = 0l;

		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(minMaxRang.get(0));
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
//		firstDate = c.getTime();
		firstDateMilli = c.getTimeInMillis();

//		Patient patient = new PatientDAO().getPatientById(patientId);
//		long offsetTime = CommonUtils.getPatientFacilityTimezoneOffset(patient);

		for (Object obj : logList) {
			Object[] objArray = (Object[]) obj;
			long objOffset = 0l;

			try {
				objOffset = ((BigInteger) objArray[3]).longValue();
			} catch (Exception e){

			}

			long timeIncluded = ((BigInteger) objArray[0]).longValue() - objOffset;
//			timeIncluded = timeIncluded + offsetTime;

			Calendar c1 = Calendar.getInstance();
			c1.setTimeInMillis(((BigInteger) objArray[0]).longValue());
			c1.set(Calendar.HOUR_OF_DAY, 0);
			c1.set(Calendar.MINUTE, 0);
			c1.set(Calendar.SECOND, 0);
			c1.set(Calendar.MILLISECOND, 0);

			long objTime = timeIncluded - c1.getTimeInMillis();

			objArray[0] = firstDateMilli + objTime;
			obj = (Object) objArray;
			avgGlucoseLogList.add(obj);
		}

		return avgGlucoseLogList;
	}

	private List<Object> adjustedGlucoseLogValueListByTime(List<Object> logList, List<Long> minMaxRang, long patientId) {
		List<Object> avgGlucoseLogList = new ArrayList<Object>();
		long firstDateMilli = 0l;

		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(minMaxRang.get(0));
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
//		firstDate = c.getTime();
		firstDateMilli = c.getTimeInMillis();

//		Patient patient = new PatientDAO().getPatientById(patientId);
//		long offsetTime = CommonUtils.getPatientFacilityTimezoneOffset(patient);

		for (Object obj : logList) {
			Object[] objArray = (Object[]) obj;
			long logCreatedTime = ((BigInteger) objArray[3]).longValue();
			//logCreatedTime = logCreatedTime - ((BigInteger) objArray[5]).longValue();

			long timeIncluded = logCreatedTime; // + offsetTime;

			Calendar c1 = Calendar.getInstance();
			c1.setTimeInMillis(((BigInteger) objArray[0]).longValue());
			c1.set(Calendar.HOUR_OF_DAY, 0);
			c1.set(Calendar.MINUTE, 0);
			c1.set(Calendar.SECOND, 0);
			c1.set(Calendar.MILLISECOND, 0);

			long objTime = timeIncluded - c1.getTimeInMillis();

			objArray[0] = firstDateMilli + objTime;
			obj = (Object) objArray;
			avgGlucoseLogList.add(obj);
		}

		return avgGlucoseLogList;
	}

	private List getMaxXAxisForChart(List activityList, String chartDayType){
		List<Long> maxValueList = new ArrayList<Long>();
		long maxValue = 0l;
		if(chartDayType.equalsIgnoreCase("generic")){
			for(Object obj : activityList){
				Object[] objArray = (Object[]) obj;
				Double value = Double.parseDouble(objArray[1].toString());
				if (maxValue < value.longValue()){
					maxValue = value.longValue();
				}
			}
		} else if(chartDayType.equalsIgnoreCase("daily") || chartDayType.equalsIgnoreCase("threeDays")) {
			for (Object obj : activityList) {
				Double value = Double.parseDouble(((LinkedHashMap) obj).get("steps").toString());
				if (maxValue < value.longValue()) {
					maxValue = value.longValue();
				}
			}
		} else if (chartDayType.equalsIgnoreCase("weekly")){
			for(Object obj : activityList){
				Object[] objArray = (Object[]) obj;
				Double value = Double.parseDouble(objArray[0].toString());
				if (maxValue < value.longValue()){
					maxValue = value.longValue();
				}
			}
		} else if (chartDayType.equalsIgnoreCase("weekly-minutes")){
			for(Object obj : activityList){
				Map map = (Map) obj;
				if(map.get("activityMinutes") != null){
					BigDecimal value = (BigDecimal) map.get("activityMinutes");
					if (maxValue < value.longValue()){
						maxValue = value.longValue();
					}
				}
			}
		}

		if(maxValue == 0){
			maxValue = 200l;
		} else {
			maxValue = maxValue + 10; // Label is hiding so, avoiding
		}
		maxValueList.add(maxValue);
		return maxValueList;
	}

	@Action(value = "fetchCombineActivityData")
	public String fetchCombineActivityData() throws ParseException {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		if(!ObjectUtils.isEmpty(customDateString)) {
			String[] dateParts	 = customDateString.split("-");
			Long dateFrom = Long.parseLong(dateParts[0]);
			Long dateTo = Long.parseLong(dateParts[1]);;
			List<Object> misfitHourlyLogs = logDAO.getAvgMisfitActivityHourlyLogsByPatientId(patientId, dateFrom, dateTo, noOfDays);
			String formattedDateString = DateUtils.getFormattedDate(dateFrom, "yyyy-MM-dd");
			Date dateToGetChart = new SimpleDateFormat("yyyy-MM-dd").parse(formattedDateString);
			List<Long> minMax = getRange(dateToGetChart, noOfDays, patientId);
			misfitHourlyLogs = adjustedLogValueListByTime(misfitHourlyLogs, minMax, patientId);

			dataMap.put(AppConstants.ServicesConstants.DATA.name(), misfitHourlyLogs);
			dataMap.put(AppConstants.ServicesConstants.MINMAX.name(), minMax);
			dataMap.put(AppConstants.ServicesConstants.MAXYAXIS.name(), getMaxXAxisForChart(misfitHourlyLogs, "generic"));
			logReportData = JsonUtil.toJson(dataMap);
		}
		return AppConstants.LOG_REPORT_DATA;
	}

	private Map getMapFromList(List list){
		Map dataMap = new HashMap();
		for(Object obj : list){
			Object[] objArray = (Object[]) (obj);
			String objDate = DateUtils.getFormattedDate(Long.parseLong(objArray[1].toString()), "yyyy-MM-dd");
			dataMap.put(objDate, objArray[0]);
		}
		return dataMap;
	}

	private List getListFromMap(Map<String, Object> map) throws ParseException {
		List list = new ArrayList();

		for(String key : map.keySet()){
			Object[] objArray = new Object[3];
			Long dateInMilli = DateUtils.getMillisecondsFromDateString(key, "yyyy-MM-dd");
			objArray[0] = map.get(key) != null ? map.get(key) : 0;
//			objArray[1] = map.get(key) != null ? map.get(key) : 0;
			objArray[1] = dateInMilli;

			list.add(objArray);
		}
		return list;
	}

	private Integer getTotalBGLFromList(List<Object> glucoseValueList){
		Integer totalBGL = 0;

		for(Object obj : glucoseValueList){
			Object[] objArray = (Object[]) obj;
			totalBGL += Integer.parseInt(objArray[1].toString());
		}
		return totalBGL;
	}

	private List<Double> getAveragePrePostBGL(List<Object> glucoseValueList){
		List<Double> avgPrePostBGL = new ArrayList<Double>();
		Double avgPreBGL, avgPostBGL;
		Integer totalPreBGL, totalPostBGL, totalPreBGLCount, totalPostBGLCount;
		totalPreBGL = totalPostBGL = 0;
		totalPreBGLCount = totalPostBGLCount = 0;
		avgPreBGL = avgPostBGL = 0.0;

		for(Object obj : glucoseValueList){
			Object[] objArray = (Object[]) obj;
			if(objArray[2].toString().equalsIgnoreCase("before meal")){
				totalPreBGL += Integer.parseInt(objArray[1]+"");
				totalPreBGLCount++;
			} else {
				totalPostBGL += Integer.parseInt(objArray[1]+"");
				totalPostBGLCount++;
			}
		}
		if(totalPreBGLCount != 0){
			avgPreBGL = Double.parseDouble((totalPreBGL / totalPreBGLCount)+"");
		}
		if (totalPostBGLCount != 0){
			avgPostBGL = Double.parseDouble((totalPostBGL / totalPostBGLCount)+"");
		}
		avgPrePostBGL.add(avgPreBGL);
		avgPrePostBGL.add(avgPostBGL);

		return avgPrePostBGL;
	}
	/*-------------------------------------------------------------------*/

	@Override
	public void setSession(Map<String, Object> arg0) {
		session = arg0;
	}

	public List<Log> getPatientLogs() {
		return patientLogs;
	}

	public void setPatientLogs(List<Log> patientLogs) {
		this.patientLogs = patientLogs;
	}

	public String getLogId() {
		return logId;
	}

	public void setLogId(String logId) {
		this.logId = logId;
	}

	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}

	public Boolean getLogProcessStatus() {
		return logProcessStatus;
	}

	public void setLogProcessStatus(Boolean logProcessStatus) {
		this.logProcessStatus = logProcessStatus;
	}

	public String getFoodName() {
		return foodName;
	}

	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}

	public Long getFoodId() {
		return foodId;
	}

	public void setFoodId(Long foodId) {
		this.foodId = foodId;
	}

	public Integer getFoodCalories() {
		return foodCalories;
	}

	public void setFoodCalories(Integer foodCalories) {
		this.foodCalories = foodCalories;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getFoodJson() {
		return foodJson;
	}

	public void setFoodJson(String foodJson) {
		this.foodJson = foodJson;
	}

	public Map<Integer, String> getStatusMap() {
		return AppConstants.LOG_STATUS_MAP;
	}

	public void setStatusMap(Map<Integer, String> statusMap) {
		this.statusMap = statusMap;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getNutritionInfoJson() {
		return nutritionInfoJson;
	}

	public void setNutritionInfoJson(String nutritionInfoJson) {
		this.nutritionInfoJson = nutritionInfoJson;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Long getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Long createdOn) {
		this.createdOn = createdOn;
	}

	public Map<String, String> getServingUnitMap() {
		return AppConstants.SERVING_UNIT_MAP;
	}

	public void setServingUnitMap(Map<String, String> servingUnitMap) {
		this.servingUnitMap = servingUnitMap;
	}

	public String getServingSize() {
		return servingSize;
	}

	public void setServingSize(String servingSize) {
		this.servingSize = servingSize;
	}

	public String getServingUnit() {
		return servingUnit;
	}

	public void setServingUnit(String servingUnit) {
		this.servingUnit = servingUnit;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getFoodImageInputStream() {
		return foodImageInputStream;
	}

	public void setFoodImageInputStream(String foodImageInputStream) {
		this.foodImageInputStream = foodImageInputStream;
	}

	public String getActionMessage() {
		return actionMessage;
	}

	public void setActionMessage(String actionMessage) {
		this.actionMessage = actionMessage;
	}

	public String getTotalCarbs() {
		return totalCarbs;
	}

	public void setTotalCarbs(String totalCarbs) {
		this.totalCarbs = totalCarbs;
	}

	public String getTotalCalories() {
		return totalCalories;
	}

	public void setTotalCalories(String totalCalories) {
		this.totalCalories = totalCalories;
	}

	public String getTotalFats() {
		return totalFats;
	}

	public void setTotalFats(String totalFats) {
		this.totalFats = totalFats;
	}

	public String getTotalProtein() {
		return totalProtein;
	}

	public void setTotalProtein(String totalProtein) {
		this.totalProtein = totalProtein;
	}

	public Map<String, Integer> getLogMap() {
		return logMap;
	}

	public String getNewMessagesJson() {
		return newMessagesJson;
	}

	public void setNewMessagesJson(String newMessagesJson) {
		this.newMessagesJson = newMessagesJson;
	}

	public String getMaxTime() {
		return maxTime;
	}

	public void setMaxTime(String maxTime) {
		this.maxTime = maxTime;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public float getFoodCarbs() {
		return foodCarbs;
	}

	public void setFoodCarbs(float foodCarbs) {
		this.foodCarbs = foodCarbs;
	}

	public float getFoodFats() {
		return foodFats;
	}

	public void setFoodFats(float foodFats) {
		this.foodFats = foodFats;
	}

	public float getFoodProtein() {
		return foodProtein;
	}

	public void setFoodProtein(float foodProtein) {
		this.foodProtein = foodProtein;
	}

	public Long getSelFoodId() {
		return selFoodId;
	}

	public void setSelFoodId(Long selFoodId) {
		this.selFoodId = selFoodId;
	}

	public int getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(int maxCount) {
		this.maxCount = maxCount;
	}

	public String getLogsJson() {
		return logsJson;
	}

	public void setLogsJson(String logsJson) {
		this.logsJson = logsJson;
	}

	public List<Log> getUnprocessedPatientLogs() {
		return unprocessedPatientLogs;
	}

	public void setUnprocessedPatientLogs(List<Log> unprocessedPatientLogs) {
		this.unprocessedPatientLogs = unprocessedPatientLogs;
	}

	public List<Log> getProcessedPatientLogs() {
		return processedPatientLogs;
	}

	public void setProcessedPatientLogs(List<Log> processedPatientLogs) {
		this.processedPatientLogs = processedPatientLogs;
	}

	public Boolean getIsAdvSearch() {
		return isAdvSearch;
	}

	public void setIsAdvSearch(Boolean isAdvSearch) {
		this.isAdvSearch = isAdvSearch;
	}

	public Boolean getIsRoleAdmin() {
		return isRoleAdmin;
	}

	public void setIsRoleAdmin(Boolean isRoleAdmin) {
		this.isRoleAdmin = isRoleAdmin;
	}

	public List<PatientDTO> getPatientDTOs() {
		return patientDTOs;
	}

	public void setPatientDTOs(List<PatientDTO> patientDTOs) {
		this.patientDTOs = patientDTOs;
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}
	public String getFoodImageLocation() {
		return foodImageLocation;
	}

	public void setFoodImageLocation(String foodImageLocation) {
		this.foodImageLocation = foodImageLocation;
	}



	public List<ReportError> getReportErrors() {
		return reportErrors;
	}

	public void setReportErrors(List<ReportError> reportErrors) {
		this.reportErrors = reportErrors;
	}

	public List<Object> getErrorObjs() {
		return errorObjs;
	}

	public void setErrorObjs(List<Object> errorObjs) {
		this.errorObjs = errorObjs;
	}

	public String getIsFromError() {
		return isFromError;
	}

	public void setIsFromError(String isFromError) {
		this.isFromError = isFromError;
	}

	public long getErrorId() {
		return errorId;
	}

	public void setErrorId(long errorId) {
		this.errorId = errorId;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public List<Object> getErrorObjsForLogId() {
		return errorObjsForLogId;
	}

	public void setErrorObjsForLogId(List<Object> errorObjsForLogId) {
		this.errorObjsForLogId = errorObjsForLogId;
	}

	public Boolean getHasMissingFood() {
		return hasMissingFood;
	}

	public void setHasMissingFood(Boolean hasMissingFood) {
		this.hasMissingFood = hasMissingFood;
	}

	public SimpleDateFormat getDateformat() {
		return dateformat;
	}

	public void setDateformat(SimpleDateFormat dateformat) {
		this.dateformat = dateformat;
	}

	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}

	public Long getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(Long iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public int getBasicstatus() {
		return basicstatus;
	}

	public void setBasicstatus(int basicstatus) {
		this.basicstatus = basicstatus;
	}

	public int getAdvstatus() {
		return advstatus;
	}

	public void setAdvstatus(int advstatus) {
		this.advstatus = advstatus;
	}

	public int getLogstatus() {
		return logstatus;
	}

	public void setLogstatus(int logstatus) {
		this.logstatus = logstatus;
	}

	public InputStream getIs() {
		return is;
	}

	public void setIs(InputStream is) {
		this.is = is;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	public JsonObject getJsonResponse() {
		return jsonResponse;
	}

	public void setJsonResponse(JsonObject jsonResponse) {
		this.jsonResponse = jsonResponse;
	}

	public List<LogDTO> getAaData() {
		return aaData;
	}

	public void setAaData(List<LogDTO> aaData) {
		this.aaData = aaData;
	}

	public List<LogDTO> getPatientlogdto() {
		return patientlogdto;
	}

	public void setPatientlogdto(List<LogDTO> patientlogdto) {
		this.patientlogdto = patientlogdto;
	}

	public boolean isAdvanceSearch() {
		return isAdvanceSearch;
	}

	public void setAdvanceSearch(boolean isAdvanceSearch) {
		this.isAdvanceSearch = isAdvanceSearch;
	}

	public Map<Integer, String> getFeildLableMap() {
		return feildLableMap;
	}

	public void setFeildLableMap(Map<Integer, String> feildLableMap) {
		this.feildLableMap = feildLableMap;
	}

	public static void setLogMap(Map<String, Integer> logMap) {
		PatientLogAction.logMap = logMap;
	}

	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		request = arg0;
	}

	public Map<Integer, String> getLogTypeMap() {
		return AppConstants.LOG_TYPE_MAP;
	}

	public void setLogTypeMap(Map<Integer, String> logTypeMap) {
		LogTypeMap = logTypeMap;
	}

	public String getLogName() {
		return logName;
	}

	public void setLogName(String logName) {
		this.logName = logName;
	}

	public String getLogNameUpdateStatus() {
		return logNameUpdateStatus;
	}

	public void setLogNameUpdateStatus(String logNameUpdateStatus) {
		this.logNameUpdateStatus = logNameUpdateStatus;
	}
	public String getFoodLogName() {
		return foodLogName;
	}

	public void setFoodLogName(String foodLogName) {
		this.foodLogName = foodLogName;
	}

	public String getLogNameFromFood() {
		return logNameFromFood;
	}

	public void setLogNameFromFood(String logNameFromFood) {
		this.logNameFromFood = logNameFromFood;
	}

	public List<String> getFoodAudioLocations() {
		return foodAudioLocations;
	}

	public void setFoodAudioLocations(List<String> foodAudioLocations) {
		this.foodAudioLocations = foodAudioLocations;
	}
	public String getQuestionListJson() {
		return questionListJson;
	}

	public void setQuestionListJson(String questionListJson) {
		this.questionListJson = questionListJson;
	}

	public String getMealJson() {
		return mealJson;
	}

	public void setMealJson(String mealJson) {
		this.mealJson = mealJson;
	}

	public Long getMealSummaryId() {
		return mealSummaryId;
	}

	public void setMealSummaryId(Long mealSummaryId) {
		this.mealSummaryId = mealSummaryId;
	}

	public String getFoodLogDetailJson() {
		return foodLogDetailJson;
	}

	public void setFoodLogDetailJson(String foodLogDetailJson) {
		this.foodLogDetailJson = foodLogDetailJson;
	}

	public List<DykDTO> getDykDTOs() {
		return dykDTOs;
	}

	public void setDykDTOs(List<DykDTO> dykDTOs) {
		this.dykDTOs = dykDTOs;
	}

	public Map<Integer, String> getGroupMap() {
		return groupMap;
	}

	public void setGroupMap(Map<Integer, String> groupMap) {
		this.groupMap = groupMap;
	}

	public Map<String, String> getUserMap() {
		return UserMap;
	}

	public void setUserMap(Map<String, String> userMap) {
		UserMap = userMap;
	}

	public int getGroupIdTogetUser() {
		return GroupIdTogetUser;
	}

	public void setGroupIdTogetUser(int groupIdTogetUser) {
		GroupIdTogetUser = groupIdTogetUser;
	}

	public Long getAdviceIdToSend() {
		return adviceIdToSend;
	}

	public void setAdviceIdToSend(Long adviceIdToSend) {
		this.adviceIdToSend = adviceIdToSend;
	}

	public int getAdviceSentStatus() {
		return adviceSentStatus;
	}

	public void setAdviceSentStatus(int adviceSentStatus) {
		this.adviceSentStatus = adviceSentStatus;
	}

	public String getLogReportData() {
		return logReportData;
	}

	public void setLogReportData(String logReportData) {
		this.logReportData = logReportData;
	}

	public Date getDailyReportDate() {
		return dailyReportDate;
	}

	public void setDailyReportDate(Date dailyReportDate) {
		this.dailyReportDate = dailyReportDate;
	}

	public String getDailyReportDateString() {
		return dailyReportDateString;
	}

	public void setDailyReportDateString(String dailyReportDateString) {
		this.dailyReportDateString = dailyReportDateString;
	}

	public String getWeeklyReportDateString() {
		return weeklyReportDateString;
	}

	public void setWeeklyReportDateString(String weeklyReportDateString) {
		this.weeklyReportDateString = weeklyReportDateString;
	}

	public String getWeeklyLogsDateString() {
		return weeklyLogsDateString;
	}

	public void setWeeklyLogsDateString(String weeklyLogsDateString) {
		this.weeklyLogsDateString = weeklyLogsDateString;
	}

	public String getWeeklyTrendDateString() {
		return weeklyTrendDateString;
	}

	public void setWeeklyTrendDateString(String weeklyTrendDateString) {
		this.weeklyTrendDateString = weeklyTrendDateString;
	}

	public int getMessagesTotalCount() {
		return messagesTotalCount;
	}

	public void setMessagesTotalCount(int messagesTotalCount) {
		this.messagesTotalCount = messagesTotalCount;
	}

	public List<String> getFavoriteQuestions() {
		return favoriteQuestions;
	}

	public void setFavoriteQuestions(List<String> favoriteQuestions) {
		this.favoriteQuestions = favoriteQuestions;
	}

	public Boolean getFavoriteButton() {
		return favoriteButton;
	}

	public void setFavoriteButton(Boolean favoriteButton) {
		this.favoriteButton = favoriteButton;
	}

	public String getRedirectToPage() {
		return redirectToPage;
	}

	public void setRedirectToPage(String redirectToPage) {
		this.redirectToPage = redirectToPage;
	}

	public float getGlucoseMin() {
		return glucoseMin;
	}

	public void setGlucoseMin(float glucoseMin) {
		this.glucoseMin = glucoseMin;
	}

	public float getGlucoseMax() {
		return glucoseMax;
	}

	public void setGlucoseMax(float glucoseMax) {
		this.glucoseMax = glucoseMax;
	}


	public String getCoachMessagesList() {
		return coachMessagesList;
	}

	public void setCoachMessagesList(String coachMessagesList) {
		this.coachMessagesList = coachMessagesList;
	}


	public String getOnFoodImageName() {
		return onFoodImageName;
	}

	public void setOnFoodImageName(String onFoodImageName) {
		this.onFoodImageName = onFoodImageName;
	}


	public String getSmsTemplateList() {
		return smsTemplateList;
	}

	public void setSmsTemplateList(String smsTemplateList) {
		this.smsTemplateList = smsTemplateList;
	}

	public String getMonthlyReportString() {
		return monthlyReportString;
	}

	public void setMonthlyReportString(String monthlyReportString) {
		this.monthlyReportString = monthlyReportString;
	}

	public String getTargetString() {
		return targetString;
	}

	public void setTargetString(String targetString) {
		this.targetString = targetString;
	}


	public String getIsFromLogbook() {
		return isFromLogbook;
	}

	public void setIsFromLogbook(String isFromLogbook) {
		this.isFromLogbook = isFromLogbook;
	}


	public String getMonthlyReportDateString() {
		return monthlyReportDateString;
	}


	public void setMonthlyReportDateString(String monthlyReportDateString) {
		this.monthlyReportDateString = monthlyReportDateString;
	}


	public String getMonthlyTrendDateString() {
		return monthlyTrendDateString;
	}


	public void setMonthlyTrendDateString(String monthlyTrendDateString) {
		this.monthlyTrendDateString = monthlyTrendDateString;
	}

	public String getThreeDaysDateString() {
		return threeDaysDateString;
	}

	public void setThreeDaysDateString(String threeDaysDateString) {
		this.threeDaysDateString = threeDaysDateString;
	}

	public String getFourteenDaysReportDateString() {
		return fourteenDaysReportDateString;
	}

	public void setFourteenDaysReportDateString(String fourteenDaysReportDateString) {
		this.fourteenDaysReportDateString = fourteenDaysReportDateString;
	}

	public Map<String, Integer> getMealCeilings() {
		return mealCeilings;
	}

	public void setMealCeilings(Map<String, Integer> mealCeilings) {
		this.mealCeilings = mealCeilings;
	}

	public PatientEatingPreference getPatientEatingPreference() {
		return patientEatingPreference;
	}

	public void setPatientEatingPreference(PatientEatingPreference patientEatingPreference) {
		this.patientEatingPreference = patientEatingPreference;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String getHttpOrHttps() {
		return httpOrHttps;
	}

	public void setHttpOrHttps(String httpOrHttps) {
		this.httpOrHttps = httpOrHttps;
	}

	public String getCustomDateString() {
		return customDateString;
	}

	public void setCustomDateString(String customDateString) {
		this.customDateString = customDateString;
	}

	public int getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(int noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getMemberPermission() {
		return memberPermission;
	}

	public void setMemberPermission(String memberPermission) {
		this.memberPermission = memberPermission;
	}

	public String getLastRequestDate() {
		return lastRequestDate;
	}

	public void setLastRequestDate(String lastRequestDate) {
		this.lastRequestDate = lastRequestDate;
	}

	public String getLastSharedDate() {
		return lastSharedDate;
	}

	public void setLastSharedDate(String lastSharedDate) {
		this.lastSharedDate = lastSharedDate;
	}

	public String getShareMealNotes() {
		return shareMealNotes;
	}

	public void setShareMealNotes(String shareMealNotes) {
		this.shareMealNotes = shareMealNotes;
	}
}
