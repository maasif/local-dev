package com.healthslate.patientapp.model.action.app.educator;

import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.entity.Device;
import com.healthslate.patientapp.model.entity.Facility;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.JsonUtil;
import com.healthslate.patientapp.util.NotifyUtils;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/*
* ======= FILE CHANGE HISTORY =======
* [2/2/2015]: Created by __oz
* [2/23/2015]: send GCM to patients when assign lead coach, __oz
* ===================================
 */

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="facilityadmin/providerPatient.tiles"),
	@Result(name="ajax", type="json", params={"root", "jsonData"})
})

@SuppressWarnings("serial")
public class ProviderPatientMapAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private String jsonData;
	private String patientListString;
	private String providerListString;
	private long patientId;
	private long providerId;

	private BaseDAO baseDAO = new BaseDAO();
	private PatientDAO patientDAO = new PatientDAO();
	private UserDAO userDAO = new UserDAO();

	private Map<String, Object> session;
	private HttpServletRequest request;

	public String description;

	@Action(value="membersData")
	public String showProviderPatientPage() {
		Facility facility = (Facility)session.get(AppConstants.SessionKeys.FACILITY.name());
		List<Patient> patients = new PatientDAO().list();
		if(patients != null && patients.size() > 0) {
			patients = populateVersionNoFromFilterLogs(patients);
		}
        patientListString = StringEscapeUtils.escapeJava(JsonUtil.toJsonExcludedNull(patients));
		description = "Displayed all patients in Provider patient page";
		return ActionSupport.SUCCESS;
	}

	@Action(value="saveLeadCoach")
	public String saveLeadCoach() {

		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

		//newly created
		try {
			Patient patient = patientDAO.getPatientById(patientId);
			if(patient != null){
				patient.setLeadCoachId(providerId);
                patient.setIsApproved("3");
			}

			baseDAO.save(patient);

            //[2/23/2015]: send GCM to patients when assign lead coach __oz
            Provider provider = new ProviderDAO().getProviderById(providerId);
            NotifyUtils.notifyPatientsOfNewCoach(provider, request);

			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
		} catch (Exception e) {
			e.printStackTrace();
		}
		description = "Saved lead coach for patient id "+patientId+" "+returnMap.get(AppConstants.JsonConstants.STATUS.name());
		jsonData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

	@Action(value="savePrimaryFoodCoach")
	public String savePrimaryFoodCoach() {

		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

		//newly created
		try {
			Patient patient = patientDAO.getPatientById(patientId);
			if(patient != null){
				patient.setPrimaryFoodCoachId(providerId);
                patient.setIsApproved("3");
			}

			baseDAO.save(patient);

			//[2/23/2015]: send GCM to patients when assign lead coach __oz
			Provider provider = new ProviderDAO().getProviderById(providerId);
			NotifyUtils.notifyPatientsOfNewCoach(provider, request);

			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
		} catch (Exception e) {
			e.printStackTrace();
		}
		description = "Saved Primary Food Coach for patient id "+patientId+" "+returnMap.get(AppConstants.JsonConstants.STATUS.name());
		jsonData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

	@Action(value="getProviderListByPatientId")
	public String getProviderListByPatientId() {
		jsonData = JsonUtil.toJsonExcludedNull(userDAO.getCoachesByPatientId(patientId, true));
		description = "Displayed providers list by patient id "+patientId;
		return "ajax";
	}


	private List<Patient> populateVersionNoFromFilterLogs(List<Patient> patients) {
		ServiceFilterLogDAO serverFilterLogDAO =  new ServiceFilterLogDAO();
		List<Patient> newPatientList = new ArrayList<Patient>();
		for (Patient patient: patients) {
			List<Facility> facilities = patient.getFacilities();
			Facility facilityAdmin = (Facility)session.get(AppConstants.SessionKeys.FACILITY.name());
            if(facilities != null && facilities.size() > 0){
                Facility userFacility = facilities.get(0);
                if(userFacility != null && (userFacility.getFacilityId() == facilityAdmin.getFacilityId())){
                    String versionNo = serverFilterLogDAO.getLatestVersionNo(patient.getPatientId());
                    Device device = new DeviceDAO().getDeviceByMac(patient.getDeviceMacAddress());
                    String deviceAndVersion = "N/A";
                    if(!versionNo.equalsIgnoreCase("N/A") && device != null){
                        deviceAndVersion = device.getDeviceType() + " " + versionNo;
                    }
                    patient.setPatientAppVersionNo(deviceAndVersion.trim());
                    newPatientList.add(patient);
                }
            }
		}

		return newPatientList;
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	public String getPatientListString() {
		return patientListString;
	}

	public void setPatientListString(String patientListString) {
		this.patientListString = patientListString;
	}

	public String getProviderListString() {
		return providerListString;
	}

	public void setProviderListString(String providerListString) {
		this.providerListString = providerListString;
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public long getProviderId() {
		return providerId;
	}

	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}
}
