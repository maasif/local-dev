package com.healthslate.patientapp.model.action.app.educator;

import com.google.gson.Gson;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.FoodDetailDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import com.healthslate.patientapp.ws.LogServices;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.proxy.HibernateProxy;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

@Results({
    @Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/myMeals.tiles"),
    @Result(name=AppConstants.LOG_DETAIL, type="tiles", location="educator/suggestedMealLogDetails.tiles"),
    @Result(name=AppConstants.AJAX, type="json", params={"root", "nutritionInfoJson"}),
    @Result(name=AppConstants.AJAX_FOOD_MASTER, type="json", params={"root", "foodLogDetailJson"}),
    @Result(name=AppConstants.AJAX_NUTRI_INFO, type="json", params={"root", "selFoodId"}),
    @Result(name="ajax", type="stream", params={"contentType", "application/Json", "inputName", "is"}),
    @Result(name=AppConstants.REDIRECT, type="redirectAction", params = {"actionName" , "myMealsPage",
                        "patientId", "%{patientId}",
                        "mealType","%{log.foodLogSummary.type}"}),
    @Result(name=AppConstants.REDIRECT_SUGGESTED_MEAL, type="redirectAction", params = {"actionName" , "suggestedMealDetails",
            "patientId", "%{patientId}",
            "redirectToPage", "myMeals", "suggestedLogId", "%{newSuggestedLogId}"})
})

public class SuggestedMealLogAction extends ActionSupport implements SessionAware, ServletRequestAware{

    private Map<String, Object> session;
    private HttpServletRequest request;

    public String description;
    private Log log;
    private String appPath;

    private String foodName;
    private String servingSize;
    private String servingUnit;
    private String foodJson;
    private String mealJson;
    private String foodLogDetailJson;
    private InputStream is;
    private Boolean logProcessStatus;
    private String userFoodNotes;

    private Map<String, String> servingUnitMap = new LinkedHashMap<String, String>();
    private String totalCarbs = "0";
    private String totalCalories = "0";
    private String totalFats = "0";
    private String totalProtein = "0";
    private String logId;
    private String maxTime;

    private Integer foodCalories;
    private int status = 1;
    private int maxCount;
    private Long createdOn;

    private Long foodId;
    private Long selFoodId;
    private long patientId = 0l;
    private long errorId;

    private float foodCarbs;
    private float foodFats;
    private float foodProtein;

    private String logName;
    private String logNameUpdateStatus = "";
    private String foodLogName;
    private String logNameFromFood;
    private Long mealSummaryId;

    private String foodImageInputStream;
    private String nutritionInfoJson;
    private String newMessagesJson;
    private String actionMessage;
    private String statusMessage = AppConstants.ServicesConstants.DATA.name();
    private String message;

    private Boolean hasMissingFood = false;
    private String foodImageLocation;
    private String onFoodImageName;
    private List<String> foodAudioLocations;

    private long suggestedLogId;
    private SuggestedLog suggestedLog;
    private long newSuggestedLogId;

    private String isFromError = "no";

    FoodLogSummaryDAO logSummaryDAO = new FoodLogSummaryDAO();
    FoodLogDetailDAO foodLogDetailDAO = new FoodLogDetailDAO();
    FoodMasterDAO foodMasterDAO = new FoodMasterDAO();
    PatientDAO patientDAO = new PatientDAO();
    SuggestedLogDAO suggestedLogDAO = new SuggestedLogDAO();
    UserDAO userDAO = new UserDAO();
    LogDAO logDAO = new LogDAO();
    BaseDAO baseDAO = new BaseDAO();

    private String logType = "Meal";
    private String minorFoodName = "";
    private File uploadFile;
    private String fileName;

    private Map<String, Integer> mealCeilings;
    private String targetString;

    private boolean isDrafted;
    private boolean isDraftedOrSendToMember;

    private String lastTimeSentOn;
    Patient patient;

    public void storeActivePatientInSession() {
        session.put(AppConstants.SessionKeys.ACTIVE_PATIENT.name(), null);
        if(patientId != 0l){
            patient = patientDAO.getPatientById(patientId);
        }
    }

    @Action(value="suggestedMealDetails")    //////////////////////////////////////
    public String suggestedMealDetails() throws IOException {

        storeActivePatientInSession();

        //put date in session here
        populateProcessedMealName();
        appPath = this.request.getContextPath();

        if(suggestedLogId != 0){

            suggestedLog = suggestedLogDAO.getSuggestedLogById(suggestedLogId);

            isDrafted = ObjectUtils.nullSafe(suggestedLog.getIsDrafted());

            lastTimeSentOn = DateUtils.getFormattedDate(suggestedLog.getLastModified(), DateUtils.DATE_FORMAT_WITH_TIME);

            //	logNameFromFood = log.getLogName();
            if(suggestedLog != null){

                createdOn = suggestedLog.getLastModified();

                Log logFromDB = suggestedLog.getLog();

                log = suggestedLog.getLog();
                logId = log.getLogId();

                Target target = new TargetDAO().getTargetByPatientId(patientId);
                mealCeilings = CommonUtils.getMealTargetsWithKeys(target);
                targetString = JsonUtil.toJson(mealCeilings);

                if(logFromDB != null){

                    FoodLogSummary foodLogSummary = logFromDB.getFoodLogSummary();

                    minorFoodName = foodLogSummary.getMinorName();
                    userFoodNotes = foodLogSummary.getNotes();

                    if(foodLogSummary != null){
					/*Set value of text field here*/

                        //get totals
                        //Map<String, Float> calcualtedValues = FoodLogUtils.calculateNutritionSum(log.getFoodLogSummary().getFoodLogDetails());
                        totalCarbs = CommonUtils.getCalculatedSize(String.valueOf(foodLogSummary.getCarbs()), "");
                        totalFats = CommonUtils.getCalculatedSize(String.valueOf(foodLogSummary.getFats()), "");
                        totalProtein = CommonUtils.getCalculatedSize(String.valueOf(foodLogSummary.getProtein()), "");
                        totalCalories = CommonUtils.getCalculatedSize(String.valueOf(foodLogSummary.getCalories()), "");

                        //get food image
                        if(foodLogSummary != null &&
                                foodLogSummary.getHasImage() != null &&
                                foodLogSummary.getHasImage() &&
                                foodLogSummary.getFoodImage() != null &&
                                foodLogSummary.getFoodImage().getImageName() != null){

                            try {
                                foodImageLocation = appPath + AppConstants.LOG_IMAGE_FOLDER + foodLogSummary.getFoodImage().getImageName();
                                onFoodImageName = foodLogSummary.getFoodImage().getImageName();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        //get food audio
                        if(foodLogSummary != null &&
                                foodLogSummary.getFoodAudios() != null &&
                                foodLogSummary.getFoodAudios().size() > 0){

                            foodAudioLocations = new ArrayList<String>();
                            for(FoodAudio audio: foodLogSummary.getFoodAudios()){
                                try {
                                    foodAudioLocations.add(appPath + AppConstants.LOG_AUDIO_FOLDER + audio.getAudioName());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        logNameFromFood = foodLogSummary.getMealName();
                    }
                    if(logFromDB.getPatient() != null){
                        int years = getYear(logFromDB.getPatient().getDob());
                        if(years > 0){
                            logFromDB.getPatient().setAge(years + " yrs");
                        } else {
                            logFromDB.getPatient().setAge("N/A");
                        }
                    }
                }
            }
        }

        populateFoodNames();
        return AppConstants.LOG_DETAIL;
    }

    @Action(value="saveSuggestedLog")
    public String saveSuggestedLog(){

        description = "Save suggested meal log";
        SuggestedLogDAO suggestedLogDAO = new SuggestedLogDAO();

        try {
            if(suggestedLogId != 0){

                SuggestedLog suggestedLog = suggestedLogDAO.getSuggestedLogById(suggestedLogId);

                if(suggestedLog != null){

                    log = suggestedLog.getLog();

                    FoodLogSummary foodLogSummary = log.getFoodLogSummary();

                    if(foodLogSummary != null
                            && foodLogSummary.getFoodLogDetails() != null
                            && foodLogSummary.getFoodLogDetails().size() > 0) {

                        for(FoodLogDetail detail: foodLogSummary.getFoodLogDetails()){
                            Hibernate.initialize(detail);
                            FoodMaster detailedFm = detail.getFoodMaster();
                            Hibernate.initialize(detailedFm);
                            if(detailedFm instanceof HibernateProxy) {
                                HibernateProxy proxy = (HibernateProxy)detailedFm;
                                FoodMaster master = (FoodMaster)proxy.getHibernateLazyInitializer().getImplementation();
                                detail.setFoodMaster(master);
                            }
                        }

                        foodLogSummary.setMealName(foodLogName);
                        foodLogSummary.setMinorName(minorFoodName);
                        foodLogSummary.setHasMissingFood(false);

                        if(!ObjectUtils.isEmpty(userFoodNotes)){
                            foodLogSummary.setNotes(userFoodNotes);
                        }

                        log.setLastModified(System.currentTimeMillis());
                        baseDAO.save(log);

                        suggestedLog.setLastModified(System.currentTimeMillis());
                        suggestedLog.setCreatedBy(HSSessionUtils.getProviderSession(session));
                        suggestedLog.setIsDrafted(isDraftedOrSendToMember);
                        baseDAO.save(suggestedLog);

                        if(isDraftedOrSendToMember){
                            NotifyUtils.notifyPatientOfSuggestedMeals(log.getPatient(), request);
                            /*LogServices logServices = new LogServices();
                            logServices.sendNewPost(log);*/
                            NotifyUtils.notifyPatientOfSuggestedMealByMsgPushNotification(log, HSSessionUtils.getProviderSession(session), request);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return AppConstants.REDIRECT;
    }

    @Action(value="saveAsSuggestedMealLog")
    public String saveAsSuggestedMealLog(){

        description = "Save as suggested meal log";
        try {
            if(!ObjectUtils.isEmpty(logId)){

                log = logDAO.getLogById(logId);

                if(log != null){

                    log.setLogId(ObjectUtils.generateLogId());
                    log.setIsSuggested(true);

                    FoodLogSummary foodLogSummary = log.getFoodLogSummary();

                    if(foodLogSummary != null){

                        List<FoodLogDetail> foodLogDetails = foodLogSummary.getFoodLogDetails();

                        if(foodLogDetails != null && foodLogDetails.size() > 0) {

                            for(FoodLogDetail detail: foodLogSummary.getFoodLogDetails()){
                                Hibernate.initialize(detail);
                                FoodMaster detailedFm = detail.getFoodMaster();
                                Hibernate.initialize(detailedFm);
                                if(detailedFm instanceof HibernateProxy) {
                                    HibernateProxy proxy = (HibernateProxy)detailedFm;
                                    FoodMaster master = (FoodMaster)proxy.getHibernateLazyInitializer().getImplementation();
                                    detail.setFoodMaster(master);
                                    detail.setFoodLogDetailId(null);
                                }else{
                                    if(detail != null){
                                        detail.setFoodLogDetailId(null);
                                    }
                                }
                            }

                            foodLogSummary.setMealName(foodLogName);
                            foodLogSummary.setMinorName(minorFoodName);
                        }

                        foodLogSummary.setFoodLogSummaryId(null);
                        foodLogSummary.setNotes("");

                        FoodImage foodImage = foodLogSummary.getFoodImage();
                        if(foodImage != null){
                            foodImage.setFoodImageId(null);
                        }

                        foodLogSummary.setHasMissingFood(false);
                        foodLogSummary.setFoodAudios(null);
                        log.setCreatedOn(System.currentTimeMillis());
                        log.setIsFavorite(false);
                        baseDAO.saveNew(log);
                        suggestedLog = new SuggestedLog();
                        suggestedLog.setCreatedOn(System.currentTimeMillis());
                        suggestedLog.setLastModified(System.currentTimeMillis());

                        suggestedLog.setLog(log);
                        suggestedLog.setCreatedBy(HSSessionUtils.getProviderSession(session));

                        baseDAO.saveNew(suggestedLog);

                        newSuggestedLogId = suggestedLog.getSuggestedLogId();

                        //NotifyUtils.notifyPatientOfSuggestedMeals(log.getPatient(), request);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return AppConstants.REDIRECT_SUGGESTED_MEAL;
    }

    @Action(value="getSuggestedFoodNutritionInfo")
    public String getSuggestedFoodNutritionInfo(){

        if(foodId != null && foodId != 0){
            FoodMaster foodMaster = foodMasterDAO.getFoodMasterById(foodId);

            if(foodMaster != null){
                Map<String, String> nutritionInfoMap = new LinkedHashMap<String, String>();
                nutritionInfoMap.put(AppConstants.NutritionInfoKeys.CARBS.name(), String.valueOf(foodMaster.getCarbs()));
                nutritionInfoMap.put(AppConstants.NutritionInfoKeys.FATS.name(), String.valueOf(foodMaster.getFats()));
                nutritionInfoMap.put(AppConstants.NutritionInfoKeys.PROTEIN.name(), String.valueOf(foodMaster.getProtein()));
                nutritionInfoMap.put(AppConstants.NutritionInfoKeys.FOOD_UNIT.name(), foodMaster.getServingSizeUnit());

                nutritionInfoJson = JsonUtil.toJson(nutritionInfoMap);
            }
        }

        return AppConstants.AJAX;
    }

    @Action(value="getSuggestedFoodNutritionInfoEdit")
    public String getSuggestedFoodNutritionInfoEdit(){

        if(foodId != null && foodId != 0){
            suggestedLog = suggestedLogDAO.getSuggestedLogById(suggestedLogId);

            FoodMaster foodMaster = foodMasterDAO.getFoodMasterById(foodId);
            Float noOfServings = foodLogDetailDAO.getDetailByIdAndFoodMasterId(suggestedLog.getLog().getLogId(), foodId);

            if(foodMaster != null){
                Map<String, String> nutritionInfoMap = new LinkedHashMap<String, String>();
                nutritionInfoMap.put(AppConstants.NutritionInfoKeys.CARBS.name(), String.valueOf(foodMaster.getCarbs()));
                nutritionInfoMap.put(AppConstants.NutritionInfoKeys.FATS.name(), String.valueOf(foodMaster.getFats()));
                nutritionInfoMap.put(AppConstants.NutritionInfoKeys.PROTEIN.name(), String.valueOf(foodMaster.getProtein()));
                nutritionInfoMap.put(AppConstants.NutritionInfoKeys.FOOD_UNIT.name(), foodMaster.getServingSizeUnit());
                nutritionInfoMap.put(AppConstants.NutritionInfoKeys.SERVING_SIZE.name(), ((noOfServings % 1) > 0 ? noOfServings+"" : noOfServings.intValue()+""));

                nutritionInfoJson = new Gson().toJson(nutritionInfoMap);
            }
        }

        return AppConstants.AJAX;
    }

    @Action(value ="getSuggestedFoodLogSummaryDetail")
    public String getSuggestedFoodLogSummaryDetail(){

        List<FoodDetailDTO> foodLogDetailList = new ArrayList<FoodDetailDTO>();

        try {
            suggestedLog = suggestedLogDAO.getSuggestedLogById(suggestedLogId);

            if(suggestedLog != null){

                log = suggestedLog.getLog();

                if(log != null){

                    FoodLogSummary foodLogSummary = log.getFoodLogSummary();
                    List<FoodLogDetail> foodLogSummaryDetails = new ArrayList<FoodLogDetail>();
                    FoodLogSummary summary = logSummaryDAO.getSummaryById(mealSummaryId);

                    if(summary != null && summary.getFoodLogDetails().size() > 0){
                        for(FoodLogDetail detail: summary.getFoodLogDetails()){
                            FoodLogDetail foodLogSummaryDetail = new FoodLogDetail();

                            if(detail.getFoodMaster()!=null){

                                FoodDetailDTO fooddetailDTO = new FoodDetailDTO();
                                FoodMaster foodmaster = detail.getFoodMaster();

                                String foodName = foodmaster.getFoodName();
                                if(!ObjectUtils.isEmpty(summary.getMealName())){
                                    foodName = summary.getMealName();
                                }
                                fooddetailDTO.setFoodName(foodName);

                                float fCal = 0, fCarbs = 0, fFats = 0, fProteins = 0;
                                if(summary != null && !foodName.equalsIgnoreCase(foodmaster.getFoodName())){
                                    fCal = ObjectUtils.nullSafeFloat(summary.getCalories());
                                    fCarbs = ObjectUtils.nullSafeFloat(summary.getCarbs());
                                    fFats = ObjectUtils.nullSafeFloat(summary.getFats());
                                    fProteins = ObjectUtils.nullSafeFloat(summary.getProtein());

                                    fooddetailDTO.setFats(fFats);
                                    fooddetailDTO.setCarbs(fCarbs);
                                    fooddetailDTO.setProtein(fProteins);

                                    FoodMaster newFoodMaster = CommonUtils.addNewFoodMaster(foodName, fCarbs, fFats, fProteins, foodmaster.getServingSizeUnit());

                                    foodLogSummaryDetail.setFoodMaster(newFoodMaster);
                                    fooddetailDTO.setFoodMasterId(newFoodMaster.getFoodMasterId());

                                }else{
                                    foodLogSummaryDetail.setFoodMaster(detail.getFoodMaster());
                                    fooddetailDTO.setFoodMasterId(foodmaster.getFoodMasterId());
                                    fooddetailDTO.setFats(foodmaster.getFats());
                                    fooddetailDTO.setCarbs(foodmaster.getCarbs());
                                    fooddetailDTO.setProtein(foodmaster.getProtein());
                                }

                                fooddetailDTO.setNumberOfServings(detail.getNumberOfServings());
                                fooddetailDTO.setServingSizeUnit(foodmaster.getServingSizeUnit());
                                foodLogDetailList.add(fooddetailDTO);

                                foodLogSummaryDetail.setNumberOfServings(detail.getNumberOfServings());
                                foodLogSummaryDetail.setFoodLogSummary(foodLogSummary);
                                foodLogSummaryDetails.add(foodLogSummaryDetail);
                            }
                        }

                        foodLogSummaryDetails = CommonUtils.removeDuplicateFoodDetails(foodLogSummaryDetails);

                        float fCal = 0, fCarbs = 0, fFats = 0, fProteins = 0;

                        if(foodLogSummary != null){
                            fCal = ObjectUtils.nullSafeFloat(foodLogSummary.getCalories());
                            fCarbs = ObjectUtils.nullSafeFloat(foodLogSummary.getCarbs());
                            fFats = ObjectUtils.nullSafeFloat(foodLogSummary.getFats());
                            fProteins = ObjectUtils.nullSafeFloat(foodLogSummary.getProtein());
                        }

                        float sCal = ObjectUtils.nullSafeFloat(summary.getCalories());
                        float sCarbs = ObjectUtils.nullSafeFloat(summary.getCarbs());
                        float sFats = ObjectUtils.nullSafeFloat(summary.getFats());
                        float sProteins = ObjectUtils.nullSafeFloat(summary.getProtein());

                        if(foodLogSummary == null){
                            foodLogSummary = new FoodLogSummary();
                            foodLogSummary.setLog(log);
                        }

                        if(fCal != sCal){
                            foodLogSummary.setCalories(sCal + fCal);
                        }

                        if(fCarbs != sCarbs){
                            foodLogSummary.setCarbs(sCarbs + fCarbs);
                        }

                        if(fFats != sFats){
                            foodLogSummary.setFats(sFats + fFats);
                        }

                        if(fProteins != sProteins){
                            foodLogSummary.setProtein(sProteins + fProteins);
                        }

                        foodLogSummary.setFoodLogDetails(foodLogSummaryDetails);

                        logSummaryDAO.save(foodLogSummary);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        foodLogDetailJson = JsonUtil.toJson(foodLogDetailList);
        return AppConstants.AJAX_FOOD_MASTER;
    }

    @Action(value="addSuggestedMealFood")
    public String addMealFoodAjax(){

        FoodLogDetail foodLogDetail = new FoodLogDetail();
        List<FoodMaster> updateFoodMasters = new ArrayList<FoodMaster>();
        FoodMaster foodMaster = new FoodMaster();
        FoodLogSummary foodLogSummary = new FoodLogSummary();
        selFoodId = 0l;

        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        if(foodId == null){

            if(foodName != null && !foodName.equalsIgnoreCase("")){
                foodMaster.setFoodName(foodName);
            }
            foodMaster.setCarbs(foodCarbs);
            foodMaster.setFats(foodFats);
            foodMaster.setProtein(foodProtein);
            foodMaster.setServingSizeUnit(servingUnit);
            foodMasterDAO.save(foodMaster);
            foodId=foodMasterDAO.getLastInsertedRecordId();
        }

        if(!ObjectUtils.isEmpty(logId)){
            log = logDAO.getLogById(logId);

            if(log != null){

                if(log.getFoodLogSummary() != null){
                    foodLogSummary = log.getFoodLogSummary();

                }

                List<FoodLogDetail> foodLogDetails = new ArrayList<FoodLogDetail>();
                List<FoodLogDetail> updateFoodLogDetails = new ArrayList<FoodLogDetail>();
                if(foodLogSummary != null){
                    foodLogDetails = foodLogSummary.getFoodLogDetails();

                    foodLogSummary.setCarbs(0f);
                    foodLogSummary.setFats(0f);
                    foodLogSummary.setProtein(0f);
                }

                //change in food which is already added in food table
                if(foodId != null && foodId != 0){

                    selFoodId = foodId;
                    foodMaster = foodMasterDAO.getFoodMasterById(foodId);

                    if(foodLogDetails != null){
                        for(FoodLogDetail detail: foodLogDetails){

                            if(detail.getFoodMaster() != null){
                                if(detail.getFoodMaster().getFoodMasterId().equals(foodMaster.getFoodMasterId())){
                                    foodLogDetail = detail;
                                }
                                else{
                                    updateFoodLogDetails.add(detail);
                                }
                            }
                        }
                    }

                    float totalCalories = 0;

                    for(FoodLogDetail detail: updateFoodLogDetails){
                        if(detail.getFoodMaster() != null && foodLogSummary != null){
                            if (detail.getFoodMaster().getCarbs() != 0){
                                float carbs = detail.getNumberOfServings()*detail.getFoodMaster().getCarbs();
                                foodLogSummary.setCarbs(foodLogSummary.getCarbs()+carbs);
                                totalCalories += carbs*4;
                            }
                            if (detail.getFoodMaster().getFats() != 0){
                                float fats = detail.getNumberOfServings()*detail.getFoodMaster().getFats();
                                foodLogSummary.setFats(foodLogSummary.getFats()+fats);
                                totalCalories += fats*9;
                            }
                            if (detail.getFoodMaster().getProtein() != 0){
                                float protein = detail.getNumberOfServings()*detail.getFoodMaster().getProtein();
                                foodLogSummary.setProtein(foodLogSummary.getProtein()+protein);
                                totalCalories += protein*4;
                            }
                        }
                    }

                    if(foodLogSummary != null){
                        foodLogSummary.setCalories(totalCalories);
                    }
                }

                if(foodName != null && !foodName.equalsIgnoreCase("")){

                    foodMaster.setFoodName(foodName);

                    if(servingSize != null && !servingSize.equalsIgnoreCase("")){
                        foodLogDetail.setNumberOfServings(Float.parseFloat(servingSize));
                    }

                    foodMaster.setCarbs(foodCarbs);
                    foodMaster.setFats(foodFats);
                    foodMaster.setProtein(foodProtein);
                    foodMaster.setServingSizeUnit(servingUnit);

                    float totalCalories = 0;
                    if (foodCarbs != 0 && foodLogSummary != null){
                        float carbs = foodLogDetail.getNumberOfServings()*foodCarbs;
                        foodLogSummary.setCarbs(foodLogSummary.getCarbs()+carbs);
                        totalCalories += carbs*4;
                    }
                    if (foodFats != 0 && foodLogSummary != null){
                        float fats = foodLogDetail.getNumberOfServings()*foodFats;
                        foodLogSummary.setFats(foodLogSummary.getFats()+fats);
                        totalCalories += fats*9;
                    }
                    if (foodProtein != 0 && foodLogSummary != null){
                        float protein = foodLogDetail.getNumberOfServings()*foodProtein;
                        foodLogSummary.setProtein(foodLogSummary.getProtein()+protein);
                        totalCalories += protein*4;
                    }

                    if(foodLogSummary != null){
                        foodLogSummary.setCalories(foodLogSummary.getCalories() + totalCalories);
                    }

                    updateFoodMasters.add(foodMaster);
                }

                foodMasterDAO.save(foodMaster);
                foodLogDetail.setFoodMaster(foodMaster);
                foodLogDetail.setFoodLogSummary(foodLogSummary);
                updateFoodLogDetails.add(foodLogDetail);

                if(foodLogSummary != null){
                    foodLogSummary.setFoodLogDetails(updateFoodLogDetails);
                }
                foodLogSummary.setMealName(logNameFromFood);
                log.setFoodLogSummary(foodLogSummary);

                logDAO.save(log);
                selFoodId = foodMaster.getFoodMasterId();
            }
        }
        description = "Add Food for Log Id: "+log.getLogId() ;
        addServerLog("addMealFood");

        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
        returnMap.put(AppConstants.JsonConstants.DATA.name(), selFoodId+"");
        returnMap.put(AppConstants.JsonConstants.CUSTOM_DATA.name(), JsonUtil.toJson(foodMaster));

        nutritionInfoJson = JsonUtil.toJson(returnMap);
        return AppConstants.AJAX;
    }

    @Action(value="deleteSuggestedMealFood")
    public String deleteSuggestedMealFood(){

        if(suggestedLogId != 0){
            suggestedLog = suggestedLogDAO.getSuggestedLogById(suggestedLogId);

            if(suggestedLog != null){

                FoodLogDetail foodLogDetail = new FoodLogDetail();
                FoodMaster foodMaster = new FoodMaster();

                FoodLogSummary foodLogSummary = new FoodLogSummary();

                log = suggestedLog.getLog();

                if(log.getFoodLogSummary() != null){
                    foodLogSummary = log.getFoodLogSummary();
                }

                List<FoodLogDetail> foodLogDetails = new ArrayList<FoodLogDetail>();
                List<FoodLogDetail> updateFoodLogDetails = new ArrayList<FoodLogDetail>();
                if(foodLogSummary != null){
                    foodLogDetails = foodLogSummary.getFoodLogDetails();
                }

                if(foodId != null && foodId != 0){

                    if(foodLogDetails != null){
                        for(FoodLogDetail detail: foodLogDetails){

                            if(detail.getFoodMaster() != null){
                                if(detail.getFoodMaster().getFoodMasterId().equals(foodId)){
                                    foodLogDetail = detail;
                                    foodMaster = detail.getFoodMaster();
                                } else{
                                    updateFoodLogDetails.add(detail);
                                }
                            }
                        }
                    }

                    if (foodLogSummary!= null && foodMaster != null && foodLogDetail != null) {

                        float carbs = foodLogDetail.getNumberOfServings()*foodMaster.getCarbs();
                        float fats = foodLogDetail.getNumberOfServings()*foodMaster.getFats();
                        float protein = foodLogDetail.getNumberOfServings()*foodMaster.getProtein();

                        foodLogSummary.setCarbs(foodLogSummary.getCarbs()-carbs);
                        foodLogSummary.setFats(foodLogSummary.getFats()-fats);
                        foodLogSummary.setProtein(foodLogSummary.getProtein()-protein);

                        foodLogSummary.setCalories(foodLogSummary.getCalories()-(carbs*4+fats*9+protein*4));
                    }

                    if (foodLogSummary!= null){
                        foodLogSummary.setFoodLogDetails(updateFoodLogDetails);
                    }

                    new BaseDAO().save(log);

                    if(foodLogDetail != null){
                        foodLogDetail.setFoodMaster(null);
                        foodLogDetailDAO.delete(foodLogDetail);
                    }

                    selFoodId = foodId;
                }
            }
        }

        description = "Delete Food for Suggested Log Id: "+log.getLogId();
        addServerLog("deleteSuggestedMealFood");
        return AppConstants.AJAX_NUTRI_INFO;
    }

    @Action(value="saveFoodLogSummaryTotalsSuggested")
    public String saveFoodLogSummaryTotalsSuggested(){

        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        try {

            suggestedLog = suggestedLogDAO.getSuggestedLogById(suggestedLogId);
            log = suggestedLog.getLog();

            if(log != null){

                FoodLogSummary foodLogSummary = log.getFoodLogSummary();

                if(foodLogSummary != null){
                    foodLogSummary.setCarbs(Float.parseFloat(totalCarbs));
                    foodLogSummary.setFats(Float.parseFloat(totalFats));
                    foodLogSummary.setProtein(Float.parseFloat(totalProtein));
                    foodLogSummary.setCalories(Float.parseFloat(totalCalories));

                    logSummaryDAO.save(foodLogSummary);
                    returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        nutritionInfoJson = JsonUtil.toJson(returnMap);
        return AppConstants.AJAX;
    }

    @Action(value="uploadSuggestedMealLogImage")
    public String uploadSuggestedMealLogImage(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try{
            log = logDAO.getLogById(logId);
            if(log != null && uploadFile != null){
                String profileImagesFolder = CommonUtils.getProjectPath() + AppConstants.LOG_IMAGE_FOLDER;
                String fileName = ObjectUtils.getImageName();
                File destinationFile = new File(profileImagesFolder, fileName);
                FileUtils.copyFile(uploadFile, destinationFile);

                FoodLogSummary foodLogSummary = log.getFoodLogSummary();
                if(foodLogSummary != null){
                    FoodImage foodImage = foodLogSummary.getFoodImage();
                    if(foodImage == null){
                        foodImage = new FoodImage();
                        foodImage.setFoodLogSummary(foodLogSummary);
                        foodLogSummary.setFoodImage(foodImage);
                    }
                    foodImage.setImageName(fileName);
                    foodLogSummary.setHasImage(true);
                }

                baseDAO.save(foodLogSummary);
                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            }
        }catch(IOException e){
            e.printStackTrace();
        }

        foodLogDetailJson = JsonUtil.toJson(returnMap);
        return AppConstants.AJAX_FOOD_MASTER;
    }

    private int getYear(Date dob){
        SimpleDateFormat simpleDateformat=new SimpleDateFormat("yyyy");

        if(dob != null){
            int yearOfDateOfBirth = Integer.parseInt(simpleDateformat.format(dob));
            int yearOfTodaysDate = Integer.parseInt(simpleDateformat.format(new Date()));
            return yearOfTodaysDate - yearOfDateOfBirth;
        }

        return 0;
    }

    //make json of existing foods
    private void populateFoodNames() {
        Map<Long, String> foodNamesMap = new LinkedHashMap<Long, String>();
        List<FoodMaster> foodMasters = foodMasterDAO.getFoodMasters();
        for(FoodMaster foodMaster: foodMasters){
            foodNamesMap.put(foodMaster.getFoodMasterId(), foodMaster.getFoodName());
        }

        foodJson = JsonUtil.toJson(foodNamesMap);
    }

    /*make jason of meal name of processed log*/
    private void populateProcessedMealName() {
        Map<Long, String> mealNameMap = new LinkedHashMap<Long, String>();
        Provider provider = (Provider) session.get(AppConstants.SessionKeys.PROVIDER.name());
        List<Object> countObjects = logDAO.getMealNameOfProcessedMealLogs(provider.getProviderId());
        for(Object obj: countObjects){
            Object[] objArray = (Object[]) obj;
            mealNameMap.put(((BigInteger)objArray[3]).longValue(), (String)objArray[4]);
        }
        mealJson = JsonUtil.toJson(mealNameMap);
    }

    public void addServerLog(String actionName){

        try {
            ServerLog serverLog = new ServerLog();

            User user = (User)session.get(AppConstants.SessionKeys.USER.name());
            if (user != null) {
                serverLog.setLoggedInUserId(user.getUserId());
                serverLog.setRole(AppConstants.Roles.ROLE_PROVIDER.name());
            }

            serverLog.setActionName(actionName);
            serverLog.setDescription(description);
            serverLog.setLogTime(new Date());

            new ServerLogDAO().saveServerLog(serverLog);

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void setServletRequest(HttpServletRequest arg0) {
        request = arg0;
    }

    @Override
    public void setSession(Map<String, Object> arg0) {
        session = arg0;
    }

    public Map<String, Object> getSession() {
        return session;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Log getLog() {
        return log;
    }

    public void setLog(Log log) {
        this.log = log;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getServingSize() {
        return servingSize;
    }

    public void setServingSize(String servingSize) {
        this.servingSize = servingSize;
    }

    public String getServingUnit() {
        return servingUnit;
    }

    public void setServingUnit(String servingUnit) {
        this.servingUnit = servingUnit;
    }

    public String getFoodJson() {
        return foodJson;
    }

    public void setFoodJson(String foodJson) {
        this.foodJson = foodJson;
    }

    public String getMealJson() {
        return mealJson;
    }

    public void setMealJson(String mealJson) {
        this.mealJson = mealJson;
    }

    public String getFoodLogDetailJson() {
        return foodLogDetailJson;
    }

    public void setFoodLogDetailJson(String foodLogDetailJson) {
        this.foodLogDetailJson = foodLogDetailJson;
    }

    public String getTotalCarbs() {
        return totalCarbs;
    }

    public void setTotalCarbs(String totalCarbs) {
        this.totalCarbs = totalCarbs;
    }

    public String getTotalCalories() {
        return totalCalories;
    }

    public void setTotalCalories(String totalCalories) {
        this.totalCalories = totalCalories;
    }

    public String getTotalFats() {
        return totalFats;
    }

    public void setTotalFats(String totalFats) {
        this.totalFats = totalFats;
    }

    public String getTotalProtein() {
        return totalProtein;
    }

    public void setTotalProtein(String totalProtein) {
        this.totalProtein = totalProtein;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(String maxTime) {
        this.maxTime = maxTime;
    }

    public Integer getFoodCalories() {
        return foodCalories;
    }

    public void setFoodCalories(Integer foodCalories) {
        this.foodCalories = foodCalories;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public Long getFoodId() {
        return foodId;
    }

    public void setFoodId(Long foodId) {
        this.foodId = foodId;
    }

    public Long getSelFoodId() {
        return selFoodId;
    }

    public void setSelFoodId(Long selFoodId) {
        this.selFoodId = selFoodId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public long getErrorId() {
        return errorId;
    }

    public void setErrorId(long errorId) {
        this.errorId = errorId;
    }

    public float getFoodCarbs() {
        return foodCarbs;
    }

    public void setFoodCarbs(float foodCarbs) {
        this.foodCarbs = foodCarbs;
    }

    public float getFoodFats() {
        return foodFats;
    }

    public void setFoodFats(float foodFats) {
        this.foodFats = foodFats;
    }

    public float getFoodProtein() {
        return foodProtein;
    }

    public void setFoodProtein(float foodProtein) {
        this.foodProtein = foodProtein;
    }

    public String getLogName() {
        return logName;
    }

    public void setLogName(String logName) {
        this.logName = logName;
    }

    public String getLogNameUpdateStatus() {
        return logNameUpdateStatus;
    }

    public void setLogNameUpdateStatus(String logNameUpdateStatus) {
        this.logNameUpdateStatus = logNameUpdateStatus;
    }

    public String getFoodLogName() {
        return foodLogName;
    }

    public void setFoodLogName(String foodLogName) {
        this.foodLogName = foodLogName;
    }

    public String getLogNameFromFood() {
        return logNameFromFood;
    }

    public void setLogNameFromFood(String logNameFromFood) {
        this.logNameFromFood = logNameFromFood;
    }

    public Long getMealSummaryId() {
        return mealSummaryId;
    }

    public void setMealSummaryId(Long mealSummaryId) {
        this.mealSummaryId = mealSummaryId;
    }

    public String getFoodImageInputStream() {
        return foodImageInputStream;
    }

    public void setFoodImageInputStream(String foodImageInputStream) {
        this.foodImageInputStream = foodImageInputStream;
    }

    public String getNutritionInfoJson() {
        return nutritionInfoJson;
    }

    public void setNutritionInfoJson(String nutritionInfoJson) {
        this.nutritionInfoJson = nutritionInfoJson;
    }

    public String getNewMessagesJson() {
        return newMessagesJson;
    }

    public void setNewMessagesJson(String newMessagesJson) {
        this.newMessagesJson = newMessagesJson;
    }

    public String getActionMessage() {
        return actionMessage;
    }

    public void setActionMessage(String actionMessage) {
        this.actionMessage = actionMessage;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getHasMissingFood() {
        return hasMissingFood;
    }

    public void setHasMissingFood(Boolean hasMissingFood) {
        this.hasMissingFood = hasMissingFood;
    }

    public InputStream getIs() {
        return is;
    }

    public void setIs(InputStream is) {
        this.is = is;
    }

    public Boolean getLogProcessStatus() {
        return logProcessStatus;
    }

    public void setLogProcessStatus(Boolean logProcessStatus) {
        this.logProcessStatus = logProcessStatus;
    }

    public Long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Long createdOn) {
        this.createdOn = createdOn;
    }

    public String getFoodImageLocation() {
        return foodImageLocation;
    }

    public void setFoodImageLocation(String foodImageLocation) {
        this.foodImageLocation = foodImageLocation;
    }

    public String getOnFoodImageName() {
        return onFoodImageName;
    }

    public void setOnFoodImageName(String onFoodImageName) {
        this.onFoodImageName = onFoodImageName;
    }

    public String getAppPath() {
        return appPath;
    }

    public void setAppPath(String appPath) {
        this.appPath = appPath;
    }

    public long getSuggestedLogId() {
        return suggestedLogId;
    }

    public void setSuggestedLogId(long suggestedLogId) {
        this.suggestedLogId = suggestedLogId;
    }

    public SuggestedLog getSuggestedLog() {
        return suggestedLog;
    }

    public void setSuggestedLog(SuggestedLog suggestedLog) {
        this.suggestedLog = suggestedLog;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public String getIsFromError() {
        return isFromError;
    }

    public void setIsFromError(String isFromError) {
        this.isFromError = isFromError;
    }

    public Map<String, String> getServingUnitMap() {
        return AppConstants.SERVING_UNIT_MAP;
    }

    public void setServingUnitMap(Map<String, String> servingUnitMap) {
        this.servingUnitMap = servingUnitMap;
    }

    public String getMinorFoodName() {
        return minorFoodName;
    }

    public void setMinorFoodName(String minorFoodName) {
        this.minorFoodName = minorFoodName;
    }

    public String getUserFoodNotes() {
        return userFoodNotes;
    }

    public void setUserFoodNotes(String userFoodNotes) {
        this.userFoodNotes = userFoodNotes;
    }

    public void setUploadFile(File uploadFile) {
        this.uploadFile = uploadFile;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Map<String, Integer> getMealCeilings() {
        return mealCeilings;
    }

    public void setMealCeilings(Map<String, Integer> mealCeilings) {
        this.mealCeilings = mealCeilings;
    }

    public String getTargetString() {
        return targetString;
    }

    public void setTargetString(String targetString) {
        this.targetString = targetString;
    }

    public long getNewSuggestedLogId() {
        return newSuggestedLogId;
    }

    public void setNewSuggestedLogId(long newSuggestedLogId) {
        this.newSuggestedLogId = newSuggestedLogId;
    }

    public boolean isDrafted() {
        return isDrafted;
    }

    public void setDrafted(boolean isDrafted) {
        this.isDrafted = isDrafted;
    }

    public boolean getIsDraftedOrSendToMember() {
        return isDraftedOrSendToMember;
    }

    public void setIsDraftedOrSendToMember(boolean isDraftedOrSendToMember) {
        this.isDraftedOrSendToMember = isDraftedOrSendToMember;
    }

    public String getLastTimeSentOn() {
        return lastTimeSentOn;
    }

    public void setLastTimeSentOn(String lastTimeSentOn) {
        this.lastTimeSentOn = lastTimeSentOn;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
