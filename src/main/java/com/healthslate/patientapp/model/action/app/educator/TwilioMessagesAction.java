package com.healthslate.patientapp.model.action.app.educator;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.healthslate.patientapp.model.dao.DeviceDAO;
import com.healthslate.patientapp.model.entity.Device;
import com.healthslate.patientapp.util.JsonUtil;
import org.apache.commons.collections.ListUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.dto.TwilioMessageDTO;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.ObjectUtils;
import com.healthslate.patientapp.util.TwilioMessageUtils;
import com.opensymphony.xwork2.ActionSupport;

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/twilioMessages.tiles"),	
	@Result(name="ajax", type="json", params={"root", "jsonData"})
})

@SuppressWarnings("serial")
public class TwilioMessagesAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private String jsonData;	
	private Long patientId;
	private Map<String, Object> session;
	private HttpServletRequest request;
	
	private String messageTo;
	private String messageFrom;
	private String messageBody;
	private String smsList;
	private String templateHyperlink;

	private PatientDAO patientDAO = new PatientDAO();
	
	public String description;
    Patient patient;

	public Patient getPatientFromSession() {
		if(patientId != null && patientId != 0){
			patient = patientDAO.getPatientById(patientId);
		} else{
			patient = (Patient) session.get(AppConstants.SessionKeys.ACTIVE_PATIENT.name());
		}
		
		if(patient != null){			
			//session.put(AppConstants.SessionKeys.ACTIVE_PATIENT.name(), patient);
		}
		
		return patient;
	}
	
	@Action(value="sendMessageToPhone")
	public String sendMessageToPhone() {	
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
				
		if(!ObjectUtils.isEmpty(messageTo) && !ObjectUtils.isEmpty(messageBody)) {

			if(!ObjectUtils.isEmpty(templateHyperlink)){
				messageBody = templateHyperlink;
			}

            Patient patient = new PatientDAO().getPatientById(patientId);
            Device device = new DeviceDAO().getDeviceByMac(patient.getDeviceMacAddress());

            if(device != null && device.getDeviceType().equalsIgnoreCase(AppConstants.DeviceTypes.IOS.getValue())){
                messageBody = messageBody.replace(AppConstants.HTTP_KEY, AppConstants.IOS_SMS_KEY);
            }

			Map<String, String> result = TwilioMessageUtils.sendSMS(messageTo, messageBody);
			String status = result.get(AppConstants.JsonConstants.STATUS.name());
			if(status.equals(AppConstants.JsonConstants.SUCCESS.name())){
				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			}

            LOG.info("Twillio message action: sending message: " + messageTo + " " + status + " to patient Id: " + patientId + ", isMessageSent: " + status);
			description = "Twillio message sent to " + messageTo + " " + status + " of patient id: " + patientId;
		}

        jsonData = JsonUtil.toJson(returnMap);
        LOG.info("Twillio message action: returned data: " + jsonData);
        return "ajax";
	}
		
	@SuppressWarnings("unchecked")
	@Action(value="getAllMessages")
	public String getAllSmsByNumber() {

		//getting list of messages to twilio from number
		List<TwilioMessageDTO> listReceived = TwilioMessageUtils.getTwilioSMSList(AppConstants.TwillioConstants.TWILIO_NUM.getValue(), messageTo, "");
		
		//getting list of messages to the number only
		List<TwilioMessageDTO> listSent = TwilioMessageUtils.getTwilioSMSList(messageTo, "", "");
		
		//combine both lists
		List<TwilioMessageDTO> list = ListUtils.union(listReceived, listSent);

        LOG.info("Twillio message action: all messages of patient Id: " + patientId + ", phoneNumber: " + messageTo + " , count: " + list.size());

		//sort the list by date
		Collections.sort(list, new Comparator<TwilioMessageDTO>() {
		  public int compare(TwilioMessageDTO o1, TwilioMessageDTO o2) {
			  return new Date(o1.getDateSent()).compareTo(new Date(o2.getDateSent()));
		  }
		});
	
		jsonData = "";
		if(list != null && list.size() > 0) {
			jsonData = JsonUtil.toJson(list);
		}
		description = "Fetching twilio message logs  of patient id" + patientId;
		return "ajax";
	}
	
	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public String getMessageTo() {
		return messageTo;
	}

	public void setMessageTo(String messageTo) {
		this.messageTo = messageTo;
	}

	public String getMessageFrom() {
		return messageFrom;
	}
	
	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}
	
	public String getMessageBody() {
		return messageBody;
	}
	
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	
	public String getSmsList() {
		return smsList;
	}
	
	public void setSmsList(String smsList) {
		this.smsList = smsList;
	}

	public String getTemplateHyperlink() {
		return templateHyperlink;
	}

	public void setTemplateHyperlink(String templateHyperlink) {
		this.templateHyperlink = templateHyperlink;
	}

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
