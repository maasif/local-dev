package com.healthslate.patientapp.model.action.app.facilityadmin;

import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.FacilityAdminDTO;
import com.healthslate.patientapp.model.dto.FacilityDTO;
import com.healthslate.patientapp.model.entity.Facility;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.model.entity.UserRole;
import com.healthslate.patientapp.util.*;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="facilityadmin/facility.tiles"),
	@Result(name="ajax", type="json", params={"root", "jsonData"})
})

@SuppressWarnings("serial")
public class FacilitiesAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private long facilityId;
	private String jsonData;
	private String facilityData;
	private Facility facility;
	private String facilitiesListString;
	private String facilityAdminListString;

	private BaseDAO baseDAO = new BaseDAO();
	private FacilityDAO facilityDAO = new FacilityDAO();

	private Map<String, Object> session;
	private HttpServletRequest request;

	public String description;
	private String facilityAdminEmail;
	private String facilityAdminPhone;
	private String facilityAdminFirstName;
	private String facilityAdminLastName;

    private String coachesListString;

	@Action(value="facility")
	public String showFacilityPage() {

        Facility facilityFromSession = HSSessionUtils.getFacilitySession(session);
        Facility facilityFromDB = new FacilityDAO().getFacilityById(facilityFromSession.getFacilityId());
		facilitiesListString = StringEscapeUtils.escapeJava(JsonUtil.toJsonExcludedNull(facilityFromDB));

        List<FacilityAdminDTO> facilityAdminDTOs = facilityDAO.listFacilityAdmins(facilityFromDB.getFacilityId());
		facilityAdminListString = StringEscapeUtils.escapeJava(JsonUtil.toJsonExcludedNull(facilityAdminDTOs));

        coachesListString = StringEscapeUtils.escapeJava(JsonUtil.toJsonExcludedNull(new ProviderDAO().getCoachesByFacility(request, facilityFromSession.getFacilityId())));

		description = "Displayed facility: " + facilityFromDB.getName() + " data";
		return ActionSupport.SUCCESS;
	}

	@Action(value="saveFacility")
	public String saveFacility() {
		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

		//newly created
		try {
			facility = (Facility) JsonUtil.fromJson(facilityData, Facility.class);

            String logo = "logowhite.png";
            String slogan = "Diabetes education and support services are provided by HealthSlate LLC";
            String emailTextName = "HealthSlate LLC";
            String consentFileName = "default_consent.html";
            long leadCoach = 0;
            int groupId = 0;

            //setting default groupId to facility when newly created, __oz
            if(facility.getFacilityId() == null || facility.getFacilityId() == 0){
                String gId = (String) new PreferencesDAO().getPreference(AppConstants.PreferencesNames.SOCIAL_GROUP_ID.getValue());
                try {
                    groupId = Integer.parseInt(gId);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
                    returnMap.put(AppConstants.JsonConstants.REASON.name(), "Social group Id not set in preferences.");
                    jsonData = JsonUtil.toJson(returnMap);
                    return "ajax";
                }
                facility.setGroupId(groupId);
            }else{
                FacilityDTO facilityFromDB = facilityDAO.getFacilityDetailsById(facility.getFacilityId());
                logo = facilityFromDB.getLogo();
                slogan = facilityFromDB.getSlogan();
                emailTextName = facilityFromDB.getEmailTextName();
                groupId = facilityFromDB.getGroupId();
                consentFileName = ObjectUtils.isEmpty(facilityFromDB.getConsentFileName()) ? consentFileName : facilityFromDB.getConsentFileName();
            }

            facility.setFacilityLogo(logo);
            facility.setSlogan(slogan);
            facility.setEmailTextName(emailTextName);
            facility.setGroupId(groupId);
            facility.setConsentFormFile(consentFileName);
            facility.setTimezoneOffsetMillis(DateUtils.getTimezoneInMillisecondsByOffset(facility.getTimezoneOffset()));

			boolean shouldCallSave = saveAsFacilityAdmin(facility);
            if(shouldCallSave){
                baseDAO.save(facility);
                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
                session.put(AppConstants.SessionKeys.FACILITY.name(), facility);
                description = "Saved facility "+facility.getName();
            }else{
                returnMap.put(AppConstants.JsonConstants.REASON.name(), "This email address is already registered.");
            }
		} catch (Exception e) {
			e.printStackTrace();
			description = "Error in saving facility details";
		}

		jsonData = JsonUtil.toJson(returnMap);
		return "ajax";
	}

	private boolean saveAsFacilityAdmin(Facility facility) {
        boolean shouldSaved = false;
		if(!ObjectUtils.isEmpty(facilityAdminEmail) && !ObjectUtils.isEmpty(facilityAdminPhone)){
			try {
				User user = new UserDAO().getUserByEmail(facilityAdminEmail);
				if(user == null){
                    user = new User();
                    user.setEmail(facilityAdminEmail);
                    user.setPhone("+1 "+facilityAdminPhone);
					user.setFirstName(facilityAdminFirstName);
					user.setLastName(facilityAdminLastName);
					user.setIsEnabled(true);
					user.setUserType(AppConstants.Roles.ROLE_FACILITY_ADMIN.name());
					user.setRegistrationDate(new Date());

                    if(facility.getFacilityId() == 0){
                        facility.setFacilityId(null);
                    }

                    baseDAO.save(facility);
                    Provider provider = new Provider();
                    provider.setFacility(new Facility(facility.getFacilityId()));
                    provider.setType(AppConstants.PROVIDERTYPE.FACILITY_ADMIN.getValue());
					provider.setIsEmailEnabled(false);
					provider.setIsSMSEnabled(false);

                    user.setProvider(provider);
                    provider.setUser(user);

                    baseDAO.save(user);

                    UserRole userRole = new UserRoleDAO().getUserRoleById(user.getUserId());
                    if(userRole == null){
                        userRole = new UserRole();
                    }
                    userRole.setUser(user);
                    userRole.setAuthority(AppConstants.Roles.ROLE_FACILITY_ADMIN.name());
                    baseDAO.save(userRole);

                    EmailUtils.sendResetPasswordEmail(user, request, false);
                    shouldSaved = true;
                }else{
                    String type = user.getUserType();
                    if(type.equalsIgnoreCase(AppConstants.Roles.ROLE_FACILITY_ADMIN.name())){
                        user.setEmail(facilityAdminEmail);
                        user.setPhone("+1 "+facilityAdminPhone);
                        user.setFirstName(facilityAdminFirstName);
                        user.setLastName(facilityAdminLastName);
                        baseDAO.save(user);
                        shouldSaved = true;
                    }
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

        return shouldSaved;
	}

	@Action(value="getFacilityList")
	public String getFacilityList() {
		try {
			HashMap<String, String> returnMap = new LinkedHashMap<String, String>();
            Facility facilityFromSession = HSSessionUtils.getFacilitySession(session);
            Facility facilityFromDB = new FacilityDAO().getFacilityById(facilityFromSession.getFacilityId());
			returnMap.put("FACILITIES", JsonUtil.toJsonExcludedNull(facilityFromDB));
			returnMap.put("FACILITY_ADMINS", JsonUtil.toJson(facilityDAO.listFacilityAdmins(facilityFromSession.getFacilityId())));
			jsonData = JsonUtil.toJson(returnMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		description = "Displayed all facilities including atoz";
		return "ajax";
	}

    @Action(value="resendEmailPinCode")
    public String resendEmailPinCode() {
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        User user = null;
        try {
            user = new UserDAO().getUserByEmail(facilityAdminEmail);
            if(user != null){
                EmailUtils.sendResetPasswordEmail(user, request, false);
                description = "Resending Email/PinCode to facility admin, email: " + user.getEmail() + ", pinCode sent to: "+ user.getPhone();
            }else{
                returnMap.put(AppConstants.JsonConstants.REASON.name(), "The email is not registered.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFacilityData() {
		return facilityData;
	}

	public void setFacilityData(String facilityData) {
		this.facilityData = facilityData;
	}

	public Facility getFacility() {
		return facility;
	}

	public void setFacility(Facility facility) {
		this.facility = facility;
	}

	public long getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(long facilityId) {
		this.facilityId = facilityId;
	}

	public String getFacilitiesListString() {
		return facilitiesListString;
	}

	public void setFacilitiesListString(String facilitiesListString) {
		this.facilitiesListString = facilitiesListString;
	}

	public String getFacilityAdminEmail() {
		return facilityAdminEmail;
	}

	public void setFacilityAdminEmail(String facilityAdminEmail) {
		this.facilityAdminEmail = facilityAdminEmail;
	}

	public String getFacilityAdminPhone() {
		return facilityAdminPhone;
	}

	public void setFacilityAdminPhone(String facilityAdminPhone) {
		this.facilityAdminPhone = facilityAdminPhone;
	}

	public String getFacilityAdminFirstName() {
		return facilityAdminFirstName;
	}

	public void setFacilityAdminFirstName(String facilityAdminFirstName) {
		this.facilityAdminFirstName = facilityAdminFirstName;
	}

	public String getFacilityAdminLastName() {
		return facilityAdminLastName;
	}

	public void setFacilityAdminLastName(String facilityAdminLastName) {
		this.facilityAdminLastName = facilityAdminLastName;
	}

	public String getFacilityAdminListString() {
		return facilityAdminListString;
	}

	public void setFacilityAdminListString(String facilityAdminListString) {
		this.facilityAdminListString = facilityAdminListString;
	}

    public String getCoachesListString() {
        return coachesListString;
    }

    public void setCoachesListString(String coachesListString) {
        this.coachesListString = coachesListString;
    }
}
