package com.healthslate.patientapp.model.action.app.facilityadmin;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.CoachDTO;
import com.healthslate.patientapp.model.dto.PatientDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.Hibernate;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;

/**
 * Created by omursaleen on 4/3/2015.
 */

@Results({
    @Result(name=ActionSupport.SUCCESS, type="tiles", location="facilityadmin/inviteMembers.tiles"),
    @Result(name="prospectiveMembersR", type="tiles", location="facilityadmin/pendingInvites.tiles"),
    @Result(name="ajaxTable", type="stream", params={"contentType", "application/Json", "inputName", "is"}),
    @Result(name="ajax", type="json", params={"root", "jsonData"})
})
public class InvitationAction extends ActionSupport implements SessionAware, ServletRequestAware {
    private String jsonData;
    public String description;
    private String approvedDenied;

    private Map<String, Object> session;
    private HttpServletRequest request;
    private List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

    private String patientListString;
    private String providerListString;
    private String primaryCoachListString;
    private long patientId;
    private long providerId;

    private BaseDAO baseDAO = new BaseDAO();
    private PatientDAO patientDAO = new PatientDAO();
    private UserDAO userDAO = new UserDAO();

    private String uuid;
    private String mrn;

    private String facilityName;

    private String sEcho;
    private InputStream is;
    private Long iTotalRecords = null;
    private Integer iTotalDisplayRecords = null;
    private Map<Integer, String> fieldLabelMap;

    private String email;
    private String phone;
    private String firstName;
    private String lastName;
    private String dobString;
    private String superFacilityId;

    @Action(value="memberRecruitment")
    public String showMemberRecruitment() {
        description = "Displayed Member Recruitment page";
        Facility facility = (Facility)session.get(AppConstants.SessionKeys.FACILITY.name());
        Facility atoZFacility = new FacilityDAO().getAtoZFacility();
        superFacilityId = atoZFacility.getFacilityId().toString();
        List<Provider> primaryCoaches = new ArrayList<Provider>();
        List<Provider> supperFacilityProviders;
        Set<Provider> hsPrimaryCoaches = new HashSet<Provider>();
        supperFacilityProviders = new ProviderDAO().getProvidersByFacilityId(atoZFacility.getFacilityId());
        primaryCoaches.addAll(supperFacilityProviders);

        if(facility != null){
            facilityName = facility.getName();
        }

        Long patientFacilityId = facility.getFacilityId();
        if(!ObjectUtils.isEmpty(uuid)){
            Patient patient = patientDAO.getPatientByUUID(uuid);
            User user = patient.getUser();
            patientFacilityId = patient.getFacilities().get(0).getFacilityId();
            firstName = user.getFirstName();
            lastName = user.getLastName();
            mrn = ObjectUtils.nullSafe(patient.getMrn());
            if(patient.getDob() != null){
                dobString = DateUtils.getFormattedDate(patient.getDob().getTime(), DateUtils.DATE_FORMAT);
            }
            email = user.getEmail();
            phone = user.getPhone();
        }

        primaryCoaches.addAll(CommonUtils.getFoodCoachesList(new ProviderDAO().getProvidersByFacilityId(patientFacilityId)));
        // Removing duplicates
        hsPrimaryCoaches.addAll(primaryCoaches);
        primaryCoaches.clear();
        primaryCoaches.addAll(hsPrimaryCoaches);

        providerListString = StringEscapeUtils.escapeJava(JsonUtil.toJsonExcludedNull(new ProviderDAO().getAllCoaches(patientFacilityId)));
        primaryCoachListString = StringEscapeUtils.escapeJava(JsonUtil.toJsonExcludedNull(CommonUtils.getPrimaryCoachesDTOs(primaryCoaches)));

        return ActionSupport.SUCCESS;
    }

    @Action(value="prospectiveMembers")
    public String showProspectiveMembersPage() {
        description = "Displayed Prospective Members page";
        return "prospectiveMembersR";
    }

    @Action(value="fetchProspectiveMembersData")
    public String fetchProspectiveMembersData() {
        Facility facility = (Facility)HSSessionUtils.getFacilitySession(session);

        PatientDAO patientDAO = new PatientDAO();
        JQueryDataTableParamModel param = DataTablesParamUtility.getParam(request);
        sEcho = param.sEcho;
        Long iTotalRecordsInt; // total number of records (un-filtered)
        int iTotalDisplayRecordsInt;
        populateFieldLabelsMap();
        String sortIndex = fieldLabelMap.get(param.iSortColumnIndex);

        patientDTOs = patientDAO.getProspectiveMembersList(param.iDisplayStart, param.iDisplayLength, param.sSearch, sortIndex, param.sSortDirection, facility.getFacilityId());
        iTotalRecordsInt =  patientDAO.getProspectiveMembersListCount( param.sSearch, facility.getFacilityId());

        sEcho = param.sEcho;
        iTotalDisplayRecordsInt = patientDTOs.size();
        iTotalRecords = iTotalRecordsInt;
        iTotalDisplayRecords = iTotalDisplayRecordsInt;

        //iTotalRecordsInt =
        JsonObject jsonResponse = new JsonObject();
        jsonResponse.addProperty("sEcho", sEcho);
        jsonResponse.addProperty("iTotalRecords", iTotalRecords);
        jsonResponse.addProperty("iTotalDisplayRecords", iTotalRecords);

        Gson gson = new Gson();
        jsonResponse.add("aaData", gson.toJsonTree(patientDTOs));
        is = new ByteArrayInputStream(jsonResponse.toString().getBytes());

        description = "Fetched Prospective Members Data";
        return "ajaxTable";
    }

    private void populateFieldLabelsMap() {
        fieldLabelMap = new LinkedHashMap<Integer, String>();
        fieldLabelMap.put(0, "p.patient_id");
        fieldLabelMap.put(1, "p.mrn");
        fieldLabelMap.put(2, "u.first_name");
        fieldLabelMap.put(3, "u.email");
        fieldLabelMap.put(4, "u.phone");
        fieldLabelMap.put(5, "p.is_invited");
        fieldLabelMap.put(6, "p.invitation_sent_date, p.request_received_date");
    }

    @Action(value="approveInvite")
    public String approveInvite() {

        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        try {
            if(!ObjectUtils.isEmpty(uuid)){
                Patient patient = new PatientDAO().getPatientByUUID(uuid);
                Hibernate.initialize(patient.getFacilities());
                List<Facility> facilityAssigned = patient.getFacilities();
                if(patient != null){
                    patient.setIsApproved(approvedDenied);
                    Facility facility = facilityAssigned.get(0);
                    description = "Approving Prospective Member with uuid: " + uuid;
                    //if request has been denied, notify member for sorry email
                    if(approvedDenied.equalsIgnoreCase(AppConstants.REQ_DENIED)){
                        description = "Denying Prospective Member with uuid: " + uuid;
                        NotifyUtils.notifyMemberOfDeniedRequest(patient, request);
                    }

                    new BaseDAO().save(patient);
                    returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="saveMrn")
    public String saveMrn() {

        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        PatientDAO patientDAO = new PatientDAO();
        try {
            if(!ObjectUtils.isEmpty(uuid)){
                Patient patient = patientDAO.getPatientByUUID(uuid);
                if(patient != null){
                    if(!patientDAO.isMRNAlreadyAssigned(mrn, patient.getFacilities().get(0).getFacilityId(), patient.getPatientId())) {
                        patient.setMrn(mrn);
                        new BaseDAO().save(patient);
                        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
                    } else {
                        returnMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.MRN_ALREADY_ASSIGNED);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        description = "Saving Member MRN with uuid: " + uuid;
        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="removeMember")
    public String removeMember() {

        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        try {
            if(!ObjectUtils.isEmpty(uuid)){
                Patient patient = new PatientDAO().getPatientByUUID(uuid);
                if(patient != null){
                    User user = patient.getUser();
                    String emailNew = CommonUtils.flushEmail(user.getEmail());
                    SocialServicesUtils.removeUserFromElgg(user.getEmail());
                    patient.setIsDeleted(true);
                    patient.setDeletedOn(new Date());
                    user.setEmail(emailNew);
                    new BaseDAO().save(user);
                    returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        description = "Removing Member with uuid: " + uuid;
        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="saveLeadCoachRecruitment")
    public String saveLeadCoachRecruitment() {

        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        //newly created
        try {
            Patient patient = patientDAO.getPatientById(patientId);
            if(patient != null){
                patient.setLeadCoachId(providerId);
                patient.setIsApproved("3");
            }

            baseDAO.save(patient);

            //[2/23/2015]: send GCM to patients when assign lead coach __oz
            Provider provider = new ProviderDAO().getProviderById(providerId);
            NotifyUtils.notifyPatientsOfNewCoach(provider, request);

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
        } catch (Exception e) {
            e.printStackTrace();
        }
        description = "Saved lead coach for patient id "+patientId+" "+returnMap.get(AppConstants.JsonConstants.STATUS.name());
        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Action(value="savePrimaryFoodCoachRecruitment")
    public String savePrimaryFoodCoachRecruitment() {

        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        //newly created
        try {
            Patient patient = patientDAO.getPatientById(patientId);
            if(patient != null){
                patient.setPrimaryFoodCoachId(providerId);
                patient.setIsApproved(AppConstants.REQ_APPROVED);
            }

            baseDAO.save(patient);

            //[2/23/2015]: send GCM to patients when assign lead coach __oz
            Provider provider = new ProviderDAO().getProviderById(providerId);
            NotifyUtils.notifyPatientsOfNewCoach(provider, request);

            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
        } catch (Exception e) {
            e.printStackTrace();
        }
        description = "Saved Primary Food Coach for patient id "+patientId+" "+returnMap.get(AppConstants.JsonConstants.STATUS.name());
        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    public Map<String, Object> getSession(){
        return this.session;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public List<PatientDTO> getPatientDTOs() {
        return patientDTOs;
    }

    public void setPatientDTOs(List<PatientDTO> patientDTOs) {
        this.patientDTOs = patientDTOs;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getApprovedDenied() {
        return approvedDenied;
    }

    public void setApprovedDenied(String approvedDenied) {
        this.approvedDenied = approvedDenied;
    }

    public String getMrn() {
        return mrn;
    }

    public void setMrn(String mrn) {
        this.mrn = mrn;
    }

    public String getProviderListString() {
        return providerListString;
    }

    public void setProviderListString(String providerListString) {
        this.providerListString = providerListString;
    }

    public String getPatientListString() {
        return patientListString;
    }

    public void setPatientListString(String patientListString) {
        this.patientListString = patientListString;
    }


    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public long getProviderId() {
        return providerId;
    }

    public void setProviderId(long providerId) {
        this.providerId = providerId;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getsEcho() {
        return sEcho;
    }

    public void setsEcho(String sEcho) {
        this.sEcho = sEcho;
    }

    public InputStream getIs() {
        return is;
    }

    public void setIs(InputStream is) {
        this.is = is;
    }

    public Long getiTotalRecords() {
        return iTotalRecords;
    }

    public void setiTotalRecords(Long iTotalRecords) {
        this.iTotalRecords = iTotalRecords;
    }

    public Integer getiTotalDisplayRecords() {
        return iTotalDisplayRecords;
    }

    public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
        this.iTotalDisplayRecords = iTotalDisplayRecords;
    }

    public Map<Integer, String> getFieldLabelMap() {
        return fieldLabelMap;
    }

    public void setFieldLabelMap(Map<Integer, String> fieldLabelMap) {
        this.fieldLabelMap = fieldLabelMap;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDobString() {
        return dobString;
    }

    public void setDobString(String dobString) {
        this.dobString = dobString;
    }

    public String getPrimaryCoachListString() {
        return primaryCoachListString;
    }

    public void setPrimaryCoachListString(String primaryCoachListString) {
        this.primaryCoachListString = primaryCoachListString;
    }

    public String getSuperFacilityId() {
        return superFacilityId;
    }

    public void setSuperFacilityId(String superFacilityId) {
        this.superFacilityId = superFacilityId;
    }
}
