package com.healthslate.patientapp.model.action.app.facilityadmin;

import com.healthslate.patientapp.model.dao.BaseDAO;
import com.healthslate.patientapp.model.dao.MedicationMasterDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.dao.PatientDiabDietMedDAO;
import com.healthslate.patientapp.model.dto.PatientDetailDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/*
* ======= FILE CHANGE HISTORY =======
* [2/26/2015]: Created by __oz
* ===================================
 */

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="facilityadmin/patientAddEdit.tiles"),
	@Result(name="ajax", type="json", params={"root", "jsonData"})
})

@SuppressWarnings("serial")
public class PatientCreateAction extends ActionSupport implements SessionAware, ServletRequestAware{

	private String jsonData;
	private Long patientId;
    private Patient patient;
    private String patientDetailString;
    private String patientData;
    private String medicationsListing;

	private Map<String, Object> session;
	private HttpServletRequest request;
	
	private PatientDAO patientDAO = new PatientDAO();
    private BaseDAO baseDAO = new BaseDAO();
    private PatientDiabDietMedDAO patientDiabDietMedDAO = new PatientDiabDietMedDAO();

    private File uploadFile;
    private String fileName;

	public String description;

	@Action(value="addEditPatient")
	public String showAddEditPatientPage() {
        patient = patientDAO.getPatientById(patientId);
        patientDetailString = JsonUtil.toJson(CommonUtils.convertToPatientDTO(patient));
        medicationsListing = JsonUtil.toJson(new MedicationMasterDAO().list());

		description = "Displayed add/edit patient page of patient id" + patientId;
		return ActionSupport.SUCCESS;
	}

    @Action(value="saveUpdatePatientDetails")
    public String saveUpdatePatientDetails(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

        try {
            PatientDetailDTO patientDetailDTO  = (PatientDetailDTO) JsonUtil.fromJson(patientData, PatientDetailDTO.class);

            if(!patientDAO.isMRNAlreadyAssigned(patientDetailDTO.getMrn(), patientDetailDTO.getFacilityId(), patientDetailDTO.getPatientId())){
                CommonUtils.savePatientDetails(patientDetailDTO);
                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            } else {
                LOG.info("patientCreateAction: MRN_ALREADY_ASSIGNED");
                returnMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.MRN_ALREADY_ASSIGNED);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        description = "Add/Edit patient details, patient id " + patientId;
        jsonData = JsonUtil.toJson(returnMap);

        return "ajax";
    }

    @Action(value="uploadPatientImage")
    public String uploadPatientImage(){
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
        try{
            patient = new PatientDAO().getPatientById(patientId);
            if(uploadFile != null){
                String profileImagesFolder = CommonUtils.getProjectPath() + AppConstants.PROFILE_IMAGE_FOLDER;
                String fileName = "PA_"+patient.getPatientId()+".png";
                File destinationFile = new File(profileImagesFolder, fileName);
                FileUtils.copyFile(uploadFile, destinationFile);

                String profileImagePath = AppConstants.PROFILE_IMAGE_FOLDER + fileName;
                patient.setImagePath(profileImagePath);

                baseDAO.save(patient);

                returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
                returnMap.put(AppConstants.JsonConstants.DATA.name(), profileImagePath);
            }
        }catch(IOException e){
            e.printStackTrace();
        }

        jsonData = JsonUtil.toJson(returnMap);
        return "ajax";
    }

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Map<String, Object> getSession(){
		return this.session;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getPatientDetailString() {
        return patientDetailString;
    }

    public void setPatientDetailString(String patientDetailString) {
        this.patientDetailString = patientDetailString;
    }

    public String getPatientData() {
        return patientData;
    }

    public void setPatientData(String patientData) {
        this.patientData = patientData;
    }

    public File getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(File uploadFile) {
        this.uploadFile = uploadFile;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMedicationsListing() {
        return medicationsListing;
    }

    public void setMedicationsListing(String medicationsListing) {
        this.medicationsListing = medicationsListing;
    }
}
