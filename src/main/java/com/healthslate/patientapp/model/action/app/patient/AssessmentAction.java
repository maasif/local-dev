package com.healthslate.patientapp.model.action.app.patient;

import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.healthslate.patientapp.model.dao.FilledFormDAO;
import com.healthslate.patientapp.model.dao.FormDataDAO;
import com.healthslate.patientapp.model.dao.GoalCategoryDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.opensymphony.xwork2.ActionSupport;

@Results({
	@Result(name=ActionSupport.SUCCESS, type="tiles", location="educator/adaAssessment.tiles"),
	@Result(name="aade_assessment", type="tiles", location="educator/aadeAssessment.tiles"),
	@Result(name="ajax", type="json", params={"root", "jsonData"})
})

@SuppressWarnings("serial")
public class AssessmentAction extends ActionSupport{

	private String jsonString;
	private Long filledFormId;
	private String jsonData;
	private Long patientId;
	public String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private GoalCategoryDAO goalCategoryDAO = new GoalCategoryDAO();
	private FilledFormDAO filledFormDAO = new FilledFormDAO();
	private FormDataDAO formDataDAO = new FormDataDAO();
	private PatientDAO patientDAO = new PatientDAO();
	
	private Map<String, List<String>> dataMap;
	
	@Action(value="adaAssessment")
	public String showADAAssessmentForm() {				
		description = "Displayed ADA assessment form";
		return ActionSupport.SUCCESS;
	}
	
	@Action(value="aadeAssessment")
	public String showAADEAssessmentForm() {				
		description = "Displayed AADE assessment form";
		return "aade_assessment";
	}
	
	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public Map<String, List<String>> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, List<String>> dataMap) {
		this.dataMap = dataMap;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	public Long getFilledFormId() {
		return filledFormId;
	}

	public void setFilledFormId(Long filledFormId) {
		this.filledFormId = filledFormId;
	}
}
