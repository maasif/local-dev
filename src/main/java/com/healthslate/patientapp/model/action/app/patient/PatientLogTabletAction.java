package com.healthslate.patientapp.model.action.app.patient;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;

import com.healthslate.patientapp.model.dao.FoodLogDetailDAO;
import com.healthslate.patientapp.model.dao.FoodMasterDAO;
import com.healthslate.patientapp.model.dao.LogDAO;
import com.healthslate.patientapp.model.dao.NotesDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.dto.PatientDTO;
import com.healthslate.patientapp.model.entity.Log;
import com.healthslate.patientapp.util.AppConstants;
import com.opensymphony.xwork2.ActionSupport;

@Results({
	@Result(name=AppConstants.LOG, type="tiles", location="patient/patientLogs.tiles"),
	@Result(name=AppConstants.LOG_DETAIL, type="tiles", location="patient/patientLogDetail.tiles"),
	@Result(name=ActionSupport.NONE, location="/patient/blankPage.html"),
	@Result(name=ActionSupport.ERROR, type="redirectAction", params = {"actionName" , "logFailed"}),
	@Result(name=ActionSupport.SUCCESS, type="redirectAction", params = {"actionName" , "logSaved"}),
	@Result(name=AppConstants.REDIRECT, type="redirectAction", params = {"actionName" , "fetchLogDetail", 
																		  "logId", "%{logId}",
																		  "patientId","%{patientId}",
																		  "nutritionAction","%{nutritionAction}"}),
	@Result(name=AppConstants.AJAX, type="json", params={"root", "logsJson"})
})

public class PatientLogTabletAction extends ActionSupport implements SessionAware {

	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unused")
	private Map<String, Object> session;
	FoodLogDetailDAO foodLogDetailDAO = new FoodLogDetailDAO();
	FoodMasterDAO foodMasterDAO = new FoodMasterDAO();
	PatientDAO patientDAO = new PatientDAO();
	NotesDAO notesDAO = new NotesDAO();
	LogDAO logDAO = new LogDAO();
	
	private Map<String, String> servingUnitMap = new LinkedHashMap<String, String>();
	private Map<Integer, String> statusMap = new LinkedHashMap<Integer, String>();
	private Map<Long, String> mealMap = new LinkedHashMap<Long, String>();
	private List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();
	private List<Log> patientLogs;
	private Log log;
	private Date logTime;

	private Boolean logProcessStatus;
	private String mealOptions;
	private String patientName;
	
	private String selectedDate;
	private Date dateFrom = new Date();
	private Date dateTo = new Date();
	private Float totalCarbs;
	private Float totalCalories;
	private String foodName;
	private Integer foodCalories;
	private Integer foodCarbs;
	private String mealName;
	private Long mealId;
	private Long foodId;
	private int status;
	private String logsJson;
	
	private String deviceMacAddress;
	private long patientId;
	private String logId;
	private boolean shouldRenderForTablet = true;
	
	public String description;
	private String data;
	// TODO verify description after un-commenting any action
	/*@Action(value="viewPatientLogsTablet")
	public String viewPatientLogsTablet() {

		Patient patient = (Patient)session.get(AppConstants.SessionKeys.PATIENT.name());
		patientName = patient.getUser().getLastName() + ", " + patient.getUser().getFirstName();
		patientId = patient.getPatientId();
		
		patientLogs = logDAO.getLogsByPatientId(patient.getPatientId(), -1, dateFrom, dateTo);
		patientLogs = CommonUtils.getUniqueResults(patientLogs);
		
		populateMaps();
		description = "View Patient Logs for Patient Id: "+patient.getPatientId();
		return AppConstants.LOG;
	}
	
	@Action(value="viewPatientLogsByDateTablet")
	public String viewPatientLogsByDateTablet() {

		SimpleDateFormat formatter = new SimpleDateFormat("M/d/yyyy");

		if(selectedDate != null && !selectedDate.equalsIgnoreCase("")){
			
			try {
				dateFrom = formatter.parse(selectedDate);
				dateTo = formatter.parse(selectedDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return viewPatientLogsTablet();
	}
	
	public void populateMaps(){
		statusMap.put(-1, AppConstants.ALL);
		statusMap.put(0, AppConstants.NOT_PROCESSED);
		statusMap.put(1, AppConstants.PROCESSED);
	}
	
	@Action(value="viewLogDetailTablet")
	public String viewLogDetailTablet(){
		
		if(logId != null && logId != ""){
			log = logDAO.getLogById(logId);
			
			if(log != null){
				logProcessStatus = log.getIsProcessed();
				logTime = new Date(log.getLogTime());
			}
		}
		List<Log> logs = new ArrayList<>();
		logs.add(log);
		logs = CommonUtils.getUniqueResults(logs);
		log = logs.get(0);
		
		mealMap.put(null, AppConstants.SELECT);
		totalCalories = 0.0f;
		totalCarbs = 0.0f;
		
		if(log.getFoodLogSummary() != null){
			
			for(FoodLogDetail foodLogDetail: log.getFoodLogSummary().getFoodLogDetails()){
				
				if(foodLogDetail.getFoodMaster() != null){
					
					if(foodLogDetail.getFoodMaster().getCarbs() != 0){
						if(foodLogDetail.getNumberOfServings() != null && foodLogDetail.getNumberOfServings() != 0){
							totalCarbs = totalCarbs + (foodLogDetail.getNumberOfServings() * foodLogDetail.getFoodMaster().getCarbs());
						}
						else{
							totalCarbs = totalCarbs + foodLogDetail.getFoodMaster().getCarbs();
						}
					}
					
					if(foodLogDetail.getFoodMaster().getCalories() != null){
						if(foodLogDetail.getNumberOfServings() != null && foodLogDetail.getNumberOfServings() != 0){
							totalCalories = totalCalories + (foodLogDetail.getNumberOfServings() * foodLogDetail.getFoodMaster().getCalories());
						}
						else{
							totalCalories = totalCalories + foodLogDetail.getFoodMaster().getCalories();
						}
					}
				}
			}
		}
		
		patientId = log.getPatient().getPatientId();
		if(patientId != 0){
			Patient patient = patientDAO.getPatientById(patientId);
			patientName = patient.getUser().getLastName() + ", " + patient.getUser().getFirstName();
		}
		
		description = "View Log Detail for Patient Id: "+log.getPatient().getPatientId()+" And Log Id: "+log.getLogId();
		return AppConstants.LOG_DETAIL;
	}
	
	@Action(value="checkSession")
	public String checkSession() {
		return NONE; 
	}
	
	@Action(value="logFailed")
	public String logFailed() {
		return NONE; 
	}
	
	@Action(value="logSaved")
	public String logSaved() {
		return NONE; 
	}
	
	@Action(value="saveLog")
	public String saveLog() throws IOException {
		
		String returnValue = SUCCESS;
		
		try {
		    System.out.println(data);
			Log log = new Gson().fromJson(data, Log.class);
		
			Patient patient = (Patient)session.get(AppConstants.SessionKeys.PATIENT.name());
			log.setPatient(patient);
			
			if(log.getGlucoseLog() != null){
				log.getGlucoseLog().setLog(log);
			}
			if(log.getFoodLogSummary() != null){
				log.getFoodLogSummary().setLog(log);
			}
			if(log.getMedicationLog() != null){
				log.getMedicationLog().setLog(log);
			}
			if(log.getMiscLog() != null){
				log.getMiscLog().setLog(log);
			}
			
			if(log.getExerciseLogs() != null ){
				for(ExerciseLog exerciseLog: log.getExerciseLogs()){
					exerciseLog.setLog(log);
				}
			}
			
			log.setIsProcessed(false);
			log.setSyncStatus(AppConstants.LogSyncStatus.UNSYNCHRONIZED.name());
			log.setLastModified(System.currentTimeMillis());
			logDAO.save(log);
		} catch( Exception e) {
			e.printStackTrace();
			returnValue = ERROR;
		}
			
		return returnValue;
	}*/
	
	@Override
	public void setSession(Map<String, Object> arg0) {
		session = arg0;
	}
	
	public List<Log> getPatientLogs() {
		return patientLogs;
	}

	public void setPatientLogs(List<Log> patientLogs) {
		this.patientLogs = patientLogs;
	}

	public String getLogId() {
		return logId;
	}

	public void setLogId(String logId) {
		this.logId = logId;
	}

	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}

	public Boolean getLogProcessStatus() {
		return logProcessStatus;
	}

	public void setLogProcessStatus(Boolean logProcessStatus) {
		this.logProcessStatus = logProcessStatus;
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public List<PatientDTO> getPatientDTOs() {
		return patientDTOs;
	}

	public void setPatientDTOs(List<PatientDTO> patientDTOs) {
		this.patientDTOs = patientDTOs;
	}

	public String getFoodName() {
		return foodName;
	}

	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}

	public Float getTotalCarbs() {
		return totalCarbs;
	}

	public void setTotalCarbs(Float totalCarbs) {
		this.totalCarbs = totalCarbs;
	}

	public Float getTotalCalories() {
		return totalCalories;
	}

	public void setTotalCalories(Float totalCalories) {
		this.totalCalories = totalCalories;
	}

	public Long getFoodId() {
		return foodId;
	}

	public void setFoodId(Long foodId) {
		this.foodId = foodId;
	}

	public Long getMealId() {
		return mealId;
	}

	public void setMealId(Long mealId) {
		this.mealId = mealId;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public Integer getFoodCalories() {
		return foodCalories;
	}

	public void setFoodCalories(Integer foodCalories) {
		this.foodCalories = foodCalories;
	}

	public Integer getFoodCarbs() {
		return foodCarbs;
	}

	public void setFoodCarbs(Integer foodCarbs) {
		this.foodCarbs = foodCarbs;
	}

	public Map<Long, String> getMealMap() {
		return mealMap;
	}

	public void setMealMap(Map<Long, String> mealMap) {
		this.mealMap = mealMap;
	}

	public String getMealOptions() {
		return mealOptions;
	}

	public void setMealOptions(String mealOptions) {
		this.mealOptions = mealOptions;
	}

	public String getDeviceMacAddress() {
		return deviceMacAddress;
	}
	public void setDeviceMacAddress(String deviceMacAddress) {
		this.deviceMacAddress = deviceMacAddress;
	}

	public boolean getShouldRenderForTablet() {
		return shouldRenderForTablet;
	}

	public void setShouldRenderForTablet(boolean shouldRenderForTablet) {
		this.shouldRenderForTablet = shouldRenderForTablet;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public Map<Integer, String> getStatusMap() {
		return statusMap;
	}

	public void setStatusMap(Map<Integer, String> statusMap) {
		this.statusMap = statusMap;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Map<String, String> getServingUnitMap() {
		return servingUnitMap;
	}

	public void setServingUnitMap(Map<String, String> servingUnitMap) {
		this.servingUnitMap = servingUnitMap;
	}

	public String getLogsJson() {
		return logsJson;
	}

	public void setLogsJson(String logsJson) {
		this.logsJson = logsJson;
	}

	public Date getLogTime() {
		return logTime;
	}

	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}

	public String getSelectedDate() {
		return selectedDate;
	}

	public void setSelectedDate(String selectedDate) {
		this.selectedDate = selectedDate;
	}

	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
}
