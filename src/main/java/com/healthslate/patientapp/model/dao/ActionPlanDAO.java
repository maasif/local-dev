package com.healthslate.patientapp.model.dao;

import java.util.ArrayList;
import java.util.List;

import com.healthslate.patientapp.model.entity.ActionPlan;

public class ActionPlanDAO extends BaseDAO {

	@SuppressWarnings("unchecked")
	public List<ActionPlan> getAllActionPlans(){
		List<ActionPlan> actionPlans = new ArrayList<ActionPlan>();
		
		try {
			
			actionPlans  = (List<ActionPlan>)currentSession.createQuery(" FROM ActionPlan ").list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return actionPlans;
	}
	
	@SuppressWarnings("unchecked")
	public List<ActionPlan> getActionPlansByGoalId(long goalId){
		List<ActionPlan> actionPlans = new ArrayList<ActionPlan>();
		
		try {
			
			actionPlans  = (List<ActionPlan>)currentSession.createQuery("FROM ActionPlan ap WHERE ap.goal.goalId = ?").setParameter(0, goalId).list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return actionPlans;
	}
	
	public String getActionPlanNameById(long actionPlanId){
		String actionPlan = "";
		
		try {
			
			actionPlan  = (String)currentSession.createQuery("SELECT name FROM ActionPlan WHERE actionPlanId = ?").setParameter(0, actionPlanId).uniqueResult();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return actionPlan;
	}		
}
