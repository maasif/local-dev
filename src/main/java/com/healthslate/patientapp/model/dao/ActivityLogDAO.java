package com.healthslate.patientapp.model.dao;

import org.hibernate.Query;

/**
 * Created by omursaleen on 3/3/2015.
 */
public class ActivityLogDAO extends BaseDAO {

    public boolean removeActivityLog(String logId) {

        Query query = currentSession.createSQLQuery(" DELETE " +
                                                    " FROM activity_log " +
                                                    " WHERE log_id = '" + logId +"';");

        int result = query.executeUpdate();
        if ( result >= 0 ) {
            return true;
        }

        return false;
    }
}
