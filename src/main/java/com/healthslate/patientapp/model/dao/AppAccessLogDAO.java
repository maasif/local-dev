package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.OneToOneReminderSessionDTO;
import com.healthslate.patientapp.model.entity.AppAccessLog;
import com.healthslate.patientapp.model.entity.OneToOneSession;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.DateUtils;
import com.healthslate.patientapp.util.HibernateUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class AppAccessLogDAO extends BaseDAO{

    public AppAccessLog getAppAccessLogByUser(long userId){
        AppAccessLog appAccessLog = null;

        try {

            appAccessLog  = (AppAccessLog)currentSession.createQuery(" FROM AppAccessLog WHERE userId = ? ").setParameter(0, userId).uniqueResult();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return appAccessLog;
    }

    public AppAccessLog getAppAccessLogByUserAsync(long userId){
        AppAccessLog appAccessLog = null;

        Session session = HibernateUtil.getCurrentSession();
        Transaction transaction = null;

        if(session.getTransaction() != null && session.getTransaction().isActive()){
            transaction = session.getTransaction();
        }else{
            transaction = session.beginTransaction();
        }

        session.clear();

        try {

            appAccessLog  = (AppAccessLog)session.createQuery(" FROM AppAccessLog WHERE userId = ? ").setParameter(0, userId).uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }

        return appAccessLog;
    }
}
