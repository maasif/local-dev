package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.Message;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.healthslate.patientapp.util.HibernateUtil;
import org.hibernate.Transaction;

import java.util.List;

public class BaseDAO {
	
	Session currentSession;
	public BaseDAO(){
		currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
	}
	
	public boolean save(Object object){
		boolean isSaved = true;
        try{
        	currentSession.clear();
        	/*currentSession.evict(object);*/
        	currentSession.saveOrUpdate(object);
        	currentSession.flush();
        } catch (HibernateException e) {
            e.printStackTrace();
            isSaved = false;
        }
        return isSaved;
    }

    public boolean saveSchedular(Object object){
        boolean isSaved = true;
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getCurrentSession();
            if(session.getTransaction() != null && session.getTransaction().isActive()){
                transaction = session.getTransaction();
            }else{
                transaction = session.beginTransaction();
            }

            session.clear();
            session.update(object);
            session.flush();
            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            isSaved = false;
            transaction.rollback();
        }
        return isSaved;
    }

    public boolean saveAsync(Object object){
        boolean isSaved = true;
        Transaction transaction = null;
        try{
            Session session = HibernateUtil.getCurrentSession();
            if(session.getTransaction() != null && session.getTransaction().isActive()){
                transaction = session.getTransaction();
            }else{
                transaction = session.beginTransaction();
            }

            session.clear();
            session.saveOrUpdate(object);
            session.flush();
            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            isSaved = false;
            transaction.rollback();
        }
        return isSaved;
    }

    public boolean saveNew(Object object){
        boolean isSaved = true;
        try{
            currentSession.clear();
            currentSession.evict(object);
            currentSession.save(object);
            currentSession.flush();
        } catch (HibernateException e) {
            e.printStackTrace();
            isSaved = false;
        }
        return isSaved;
    }

	public void delete(Object object){
		
		try{
			currentSession.delete(object);
			currentSession.flush();
		}catch (HibernateException e) {
			e.printStackTrace();
		}
    }
}
