package com.healthslate.patientapp.model.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;

import com.healthslate.patientapp.model.entity.CoachGoalAssessment;

public class CoachGoalAssessmentDAO extends BaseDAO {
	
	public Long saveCoachAssessment(CoachGoalAssessment coachGoalAssessment){
		try{
			currentSession.saveOrUpdate(coachGoalAssessment);        	
        } catch (HibernateException e) {
            e.printStackTrace();            
        }
		
		return coachGoalAssessment.getCoachGoalAssessmentId();
	}
	
	@SuppressWarnings("unchecked")
	public List<CoachGoalAssessment> getCoachAssessmentsByFormId(long filledFormId){		
		List<CoachGoalAssessment> coachGoalAssessments = new ArrayList<CoachGoalAssessment>();
		
		try{
			coachGoalAssessments = (List<CoachGoalAssessment>) currentSession.createQuery("FROM CoachGoalAssessment cgs JOIN FETCH cgs.filledForm cgar WHERE cgar.filledFormId = ? ORDER BY cgs.assessedDate DESC").setParameter(0, filledFormId).list();
        	
        } catch (HibernateException e) {
            e.printStackTrace();            
        }
		
		return coachGoalAssessments;
	}	
	
	@SuppressWarnings("unchecked")
	public List<CoachGoalAssessment> getCoachAssessmentsByFormId(List<Long> filledFormIds){		
		List<CoachGoalAssessment> coachGoalAssessments = new ArrayList<CoachGoalAssessment>();
		
		try{
			coachGoalAssessments = (List<CoachGoalAssessment>) currentSession.createQuery("FROM CoachGoalAssessment cgs JOIN FETCH cgs.filledForm cgar WHERE cgar.filledFormId IN (:filledFormIds)").setParameterList("filledFormIds", filledFormIds).list();
        	
        } catch (HibernateException e) {
            e.printStackTrace();            
        }
		
		return coachGoalAssessments;
	}
}
