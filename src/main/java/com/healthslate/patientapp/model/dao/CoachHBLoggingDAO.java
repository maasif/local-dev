package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.CoachHBLogging;
import com.healthslate.patientapp.model.entity.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i.nasim on 30-Jun-15.
 */
public class CoachHBLoggingDAO extends BaseDAO {
    public List<CoachHBLogging> getAllHBLogsByPatient(long patientId) {
        List<CoachHBLogging> coachLogs = new ArrayList<CoachHBLogging>();

        try {

            coachLogs = (List<CoachHBLogging>) currentSession.createQuery(" FROM CoachHBLogging WHERE patient.patientId = ? ORDER BY loggingDate DESC ").setParameter(0, patientId).list();

            if (coachLogs != null && coachLogs.size() > 0) {
                for (CoachHBLogging log : coachLogs) {
                    User user = log.getProvider().getUser();
                    log.setCoachName(user.getLastName() + ", " + user.getFirstName());
                    log.setTimeToDateString(log.getLoggingDate()+"");
                    log.setHBLoggingDateToString(log.getHBloggingDate()+"");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return coachLogs;
    }
}
