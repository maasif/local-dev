package com.healthslate.patientapp.model.dao;


import com.healthslate.patientapp.model.entity.CoachNotes;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.DateUtils;

import java.util.ArrayList;
import java.util.List;

public class CoachNotesDAO extends BaseDAO{

    public List<CoachNotes> getAllNotesByPatient(long patientId){
        List<CoachNotes> coachNotes = new ArrayList<CoachNotes>();

        try {

            coachNotes  = (List<CoachNotes>)currentSession.createQuery(" FROM CoachNotes WHERE patient.patientId = ? ORDER BY notesDate DESC ").setParameter(0, patientId).list();

            if(coachNotes != null && coachNotes.size() > 0){
               for (CoachNotes notes : coachNotes){
                   User user = notes.getProvider().getUser();
                   notes.setCoachName(user.getLastName() + ", " + user.getFirstName());
                   notes.setTimeToDateString(notes.getNotesDate()+"");
               }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return coachNotes;
    }

    public List<Object> getProvidersLastNotesToPatients(Long providerId) {
        List<Object> result = null;
        String query = "SELECT cn.coach_notes_id, CONCAT_WS(' ', us.first_name, us.last_name) AS fullName,  " +
                " cn.provider_id, cn.notes, cn.patient_id, cn.notes_date FROM coach_notes cn " +
                "JOIN patient p ON p.patient_id = cn.patient_id " +
                "JOIN provider pro ON cn.provider_id = pro.provider_id " +
                "JOIN users us ON us.user_id = pro.user_id " +
                "JOIN provider_patient pp ON pp.patients_patient_id = p.patient_id "+
                "INNER JOIN ( " +
                " SELECT " +
                "  MAX(cn.notes_date) AS time " +
                " FROM " +
                " coach_notes cn " +
                " GROUP BY " +
                "  cn.provider_id, cn.patient_id " +
                " ORDER BY " +
                "  cn.notes_date " +
                ") AS innerQuery ON innerQuery.time = cn.notes_date " +
                " WHERE pp.providers_provider_id = "+ providerId +
                " ORDER BY cn.patient_id, cn.provider_id";
        try{
            result = currentSession.createSQLQuery(query).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  result;
    }
}
