package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.CoachSessionPreference;

public class CoachSessionPreferenceDAO extends BaseDAO {

    public CoachSessionPreference getSessionPreferenceByProviderId(long providerId) {
        CoachSessionPreference sessionPreference = null;
        StringBuilder query = new StringBuilder();

        try {
            query.append("SELECT ses ")
                    .append("FROM CoachSessionPreference ses ")
                    .append("WHERE ses.provider.providerId = :providerId");

            sessionPreference = (CoachSessionPreference) currentSession.createQuery(query.toString())
                    .setParameter("providerId", providerId).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sessionPreference;
    }
}
