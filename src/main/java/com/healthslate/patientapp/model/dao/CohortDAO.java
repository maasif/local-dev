package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.Cohort;

public class CohortDAO extends BaseDAO{

	public Cohort getCohortById(long id) {
		
		Cohort cohort = (Cohort) currentSession.createQuery("FROM Cohort WHERE cohortId = ?")
				.setParameter(0, id)
				.uniqueResult();
		return cohort;
	}
}
