package com.healthslate.patientapp.model.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.healthslate.patientapp.model.entity.Device;
import com.healthslate.patientapp.util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;


public class DeviceDAO extends BaseDAO{

	public Device getDeviceByMac(String mac) {
		
		Device device = null;
		
		try{
			List<Device> devices = (List<Device>) currentSession.createQuery(" FROM Device WHERE deviceMacAddress = ? ").setParameter(0, mac).list();
			if(devices != null && devices.size() > 0){
				device = devices.get(0);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return device;
	}

	public List<String> getDevicesByManufacturer(String manufacturer) {

		List<String> devices = new ArrayList<String>();

		try{
			devices = (List<String>) currentSession.createQuery("SELECT d.registrationId FROM Device d WHERE d.deviceType = ? ").setParameter(0, manufacturer).list();
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return devices;
	}

	public List<String> getDevicesByManufacturer(String manufacturer, String buildType, String appName) {

		List<String> devices = new ArrayList<String>();

		try{
			devices = (List<String>) currentSession.createQuery("SELECT d.registrationId FROM Device d WHERE d.deviceType = ? AND d.deviceBuildType = ? AND d.deviceAppName = ? ")
					.setParameter(0, manufacturer)
					.setParameter(1, buildType)
					.setParameter(2, appName)
					.list();
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return devices;
	}

	public Device getDeviceByMacScheduler(String mac) {
		
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = null;
		
		if(session.getTransaction() != null && session.getTransaction().isActive()){
			transaction = session.getTransaction();
		}else{
			transaction = session.beginTransaction();			
		}
		
		Device device = new Device();
		
		try{
			device = (Device) currentSession.createQuery(" FROM Device WHERE deviceMacAddress = ? ").setParameter(0, mac).uniqueResult();
			transaction.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			transaction.rollback();
		}
		return device;
	}

	public List<Device> getDevicesByPatientDeviceMacAddresses(List<String> macAddresses){
		List<Device> devices;
		StringBuilder query = new StringBuilder();

		query.append(" SELECT d ")
				.append(" FROM Device d ")
				.append(" WHERE d.deviceMacAddress IN(:macAddresses) ");

		devices = currentSession.createQuery(query.toString())
				.setParameterList("macAddresses", macAddresses)
				.list();

		return devices;
	}
}
