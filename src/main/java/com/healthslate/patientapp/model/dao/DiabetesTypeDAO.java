package com.healthslate.patientapp.model.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;

import com.healthslate.patientapp.model.entity.DiabetesType;

public class DiabetesTypeDAO extends BaseDAO{

	@SuppressWarnings("unchecked")
	public List<DiabetesType> getDiabetesTypes(){
		
		List<DiabetesType> diabetesTypes = new ArrayList<DiabetesType>();
		
        try{
        	diabetesTypes = currentSession.createQuery("FROM DiabetesType").list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        
        return diabetesTypes;
	}
}
