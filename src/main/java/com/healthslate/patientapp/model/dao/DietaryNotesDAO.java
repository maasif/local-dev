package com.healthslate.patientapp.model.dao;


import com.healthslate.patientapp.model.entity.DietaryNotes;
import com.healthslate.patientapp.model.entity.User;

import java.util.ArrayList;
import java.util.List;

public class DietaryNotesDAO extends BaseDAO{

    public List<DietaryNotes> getAllNotesByPatient(long patientId){
        List<DietaryNotes> dietaryNotes = new ArrayList<DietaryNotes>();

        try {

            dietaryNotes  = (List<DietaryNotes>)currentSession.createQuery(" FROM DietaryNotes WHERE patient.patientId = ? ORDER BY notesDate DESC ").setParameter(0, patientId).list();

            if(dietaryNotes != null && dietaryNotes.size() > 0){
               for (DietaryNotes notes : dietaryNotes){
                   User user = notes.getProvider().getUser();
                   notes.setCoachName(user.getLastName() + ", " + user.getFirstName());
//                   notes.setTimeToDateString(DateUtils.getFormattedDate(notes.getNotesDate(), DateUtils.DATE_FORMAT));
                   notes.setTimeToDateString(notes.getNotesDate()+"");
               }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dietaryNotes;
    }
}
