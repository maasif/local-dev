package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.DYKAdvice;

import java.util.ArrayList;
import java.util.List;

public class DykDAO extends BaseDAO {

	@SuppressWarnings("unchecked")
	public List<DYKAdvice> groupPosts() {
		
		List<DYKAdvice> DYKAdvices = new ArrayList<DYKAdvice>();
		try{
			DYKAdvices = currentSession.createQuery("from DYKAdvice").list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return DYKAdvices;
	}

	public DYKAdvice getAdviceById(Long adviceId) {
		
		DYKAdvice DYKAdvice = null;
		try{
			DYKAdvice = (DYKAdvice) currentSession.createQuery("FROM DYKAdvice WHERE adviceId = ?")
				.setParameter(0, adviceId)
				.uniqueResult();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return DYKAdvice;
	}
}
