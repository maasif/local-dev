package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.EmailNotificationMessage;

public class EmailNotificationMessageDAO extends BaseDAO {

	public EmailNotificationMessage saveEmailToken(EmailNotificationMessage emailToken) {
		try {
			currentSession.createSQLQuery("Update email_notification_message SET is_expired = 1 WHERE user_id = ? ").setParameter(0, emailToken.getUser().getUserId()).executeUpdate();
			currentSession.saveOrUpdate(emailToken);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return emailToken;
	}

	public EmailNotificationMessage getByToken(String token) {
		EmailNotificationMessage emailToken = null;
		try {
			emailToken = (EmailNotificationMessage)currentSession.createQuery(" FROM EmailNotificationMessage WHERE isExpired = 0 AND token = ?").setParameter(0, token).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return emailToken;
	}
}