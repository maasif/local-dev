package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.EmailToken;

public class EmailTokenDAO extends BaseDAO {

	public EmailToken saveEmailToken(EmailToken emailToken) {
		try {
			currentSession.createSQLQuery("Update email_token SET is_used = 1 WHERE username = ? ").setParameter(0, emailToken.getUsername()).executeUpdate();
			currentSession.saveOrUpdate(emailToken);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return emailToken;
	}

	public EmailToken getByToken(String token) {
		EmailToken emailToken = null;
		try {
			emailToken = (EmailToken)currentSession.createQuery(" FROM EmailToken WHERE isUsed = 0 AND token = ?").setParameter(0, token).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return emailToken;
	}
}