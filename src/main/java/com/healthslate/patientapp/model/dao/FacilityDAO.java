package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.FacilityAdminDTO;
import com.healthslate.patientapp.model.dto.FacilityDTO;
import com.healthslate.patientapp.model.entity.Facility;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.HibernateUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


public class FacilityDAO extends BaseDAO{
	
	public long saveFacility(Facility facility){
		
        try{
        	currentSession.clear();
        	currentSession.saveOrUpdate(facility);
        	currentSession.flush();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return facility.getFacilityId();
    }
	
	public void deleteFacility(Facility facility){
	    try { 
	    	currentSession.delete(facility);
	    	currentSession.flush();
	    } catch (HibernateException e) {
	        e.printStackTrace();
	    }
    }
	
	@SuppressWarnings("unchecked")
	public List<Facility> list(boolean isExcludedAtoz) {
		String facilityId = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.HEALTHSLATE_FACILITY_ID.getValue());
		String excludedAtoz = "";
		if(isExcludedAtoz){
			excludedAtoz = " WHERE facilityId != "+ facilityId;
		}

		List<Facility> facilityList = null;
		facilityList = (List<Facility>)currentSession.createQuery("from Facility " + excludedAtoz + " ORDER BY facilityId DESC").list();
		return facilityList;
	}

    public List<Facility> listRaw(boolean isExcludedAtoz) {
        String facilityIdPref = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.HEALTHSLATE_FACILITY_ID.getValue());
        String excludedAtoz = "";
        if(isExcludedAtoz){
            excludedAtoz = " WHERE f.facility_id <> "+ facilityIdPref;
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT f.facility_id, f.name, f.lead_coach FROM facility f "+excludedAtoz+ " ORDER BY f.facility_id DESC");
        List<Object> result = currentSession.createSQLQuery(stringBuilder.toString()).list();

        List<Facility> facilities = new ArrayList<Facility>();
        if(result != null && result.size() > 0){

            for(Object obj: result) {
                Object[] objArray = (Object[]) obj;

                Long facilityId = ObjectUtils.nullSafeLong(objArray[0]);
                String facilityName = ObjectUtils.nullSafe(objArray[1]);

                Facility facility = new Facility();
                facility.setFacilityId(facilityId);
                facility.setName(facilityName);
                facility.setLeadCoach(ObjectUtils.nullSafeLong(objArray[2]));
                facilities.add(facility);
            }
        }

        return facilities;
    }

    public Facility getFacilityByPatientId(long patientId){

        Facility facility = null;

        try{
            List<Object> objects = currentSession.createSQLQuery("SELECT facilities_facility_id FROM patient_facility WHERE patients_patient_id = " + patientId).list();

            if(objects != null && objects.size() > 0){
                BigInteger bigInteger = (BigInteger)objects.get(0);
                long facId = ObjectUtils.nullSafeLong(bigInteger.longValue());
                facility = getFacilityById(facId);
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }

        return facility;
    }

    public Facility getFacilityByPatientIdScheduler(long patientId){
        Session session = HibernateUtil.getCurrentSession();
        Transaction transaction = null;

        if(session.getTransaction() != null && session.getTransaction().isActive()){
            transaction = session.getTransaction();
        }else{
            transaction = session.beginTransaction();
        }

        session.clear();

        Facility facility = null;

        try{
            Object object = session.createSQLQuery("SELECT facilities_facility_id FROM patient_facility WHERE patients_patient_id = " + patientId).uniqueResult();
            transaction.rollback();

            if(object != null){
                long facId = ObjectUtils.nullSafeLong(object);
                facility = getFacilityByIdScheduler(facId);
            }
        } catch(Exception ex){
            ex.printStackTrace();
            transaction.rollback();
        }

        return facility;
    }

	public List<FacilityAdminDTO> listFacilityAdmins(long facilityId) {
		List<FacilityAdminDTO> facilityAdminDTOs = new ArrayList<FacilityAdminDTO>();
		StringBuilder queryString = new StringBuilder();

        String queryUpdate = "";
        if(facilityId > 0){
            queryUpdate = " AND p.facility_id =  " + facilityId;
        }
		try {
			queryString.append("SELECT us.user_id, us.first_name, us.last_name, us.email, us.phone, f.facility_id " +
					" FROM " +
					" provider p " +
					" LEFT JOIN users us ON us.user_id = p.user_id " +
                    " LEFT JOIN facility f ON f.facility_id = p.facility_id " +
					" WHERE us.user_type = '"+AppConstants.Roles.ROLE_FACILITY_ADMIN.name()+"' AND (p.is_deleted = 0 OR p.is_deleted IS NULL)"
                    + queryUpdate);

			List<Object> result = currentSession.createSQLQuery(queryString.toString()).list();

			if(result != null && result.size() > 0){
				for(Object obj: result) {
					Object[] objArray = (Object[]) obj;

					FacilityAdminDTO facilityAdminDTO = new FacilityAdminDTO();

					facilityAdminDTO.setUserId(ObjectUtils.nullSafeLong(objArray[0]));
					facilityAdminDTO.setFirstName(ObjectUtils.nullSafe(objArray[1]));
					facilityAdminDTO.setLastName(ObjectUtils.nullSafe(objArray[2]));
					facilityAdminDTO.setEmail(ObjectUtils.nullSafe(objArray[3]));
					facilityAdminDTO.setPhone(ObjectUtils.nullSafe(objArray[4]));
					facilityAdminDTO.setFacilityId(ObjectUtils.nullSafeLong(objArray[5]));

					facilityAdminDTOs.add(facilityAdminDTO);
				}
            }
		} catch (Exception e) {
			e.printStackTrace();
		}

		return facilityAdminDTOs;
	}

	public Facility getFacility()
	{
		Facility facility = null;
		facility = (Facility)currentSession.createQuery("from Facility where facilityId = (select max(facilityId) from Facility)").uniqueResult();
		return facility;
	}
	
	@SuppressWarnings("unchecked")
	public List<Facility> list(Long facilityId)
	{
		List<Facility> facilityList = null;
		facilityList = (List<Facility>)currentSession.createQuery("from Facility where facilityId = '" + facilityId +"' ").list();
		return facilityList;
	}
	
	public Facility getFacilityWithId(Long facilityId) {
		
		Facility facility = (Facility)currentSession.createQuery("from Facility where facilityId = ? ").setParameter(0, facilityId).uniqueResult();
		currentSession.evict(facility);
		return facility;
	}

    public Facility getFacilityWithIdAndPatients(Long facilityId) {

        Facility facility = (Facility)currentSession.createQuery("from Facility f JOIN fetch f.patients where f.facilityId = ? ").setParameter(0, facilityId).uniqueResult();
        if(facility != null){
            currentSession.evict(facility);
        }
        return facility;
    }

	public void updateFacility(Facility facility) {
		
        try{
        	currentSession.clear();
        	currentSession.update(facility);
        } catch (HibernateException e) {
            e.printStackTrace();
        }
	}

	public Facility getFacilityByName(String facilityName) {
		Facility facility = null;
		facility = (Facility)currentSession.createQuery("from Facility where name = ? ")
				.setParameter(0, facilityName).uniqueResult();
		return facility;
	}

    public Facility getFacilityById(long facilityId) {
        Facility facility = null;
        facility = (Facility)currentSession.createQuery("from Facility where facilityId = ? ").setParameter(0, facilityId).uniqueResult();
        return facility;
    }

    public Facility getFacilityByIdScheduler(long facilityId) {
        Session session = HibernateUtil.getCurrentSession();
        Transaction transaction = null;

        if(session.getTransaction() != null && session.getTransaction().isActive()){
            transaction = session.getTransaction();
        }else{
            transaction = session.beginTransaction();
        }

        session.clear();

        Facility facility = null;

        try{
            facility = (Facility)session.createQuery("from Facility where facilityId = ? ").setParameter(0, facilityId).uniqueResult();
            transaction.rollback();
        } catch(Exception ex){
            ex.printStackTrace();
            transaction.rollback();
        }

        return facility;
    }

	public Facility getAtoZFacility() {
		String facilityId = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.HEALTHSLATE_FACILITY_ID.getValue());
		Facility facility = null;
		facility = (Facility)currentSession.createQuery("from Facility where facilityId = ? ")
				.setParameter(0, Long.valueOf(facilityId)).uniqueResult();
		return facility;
	}

	@SuppressWarnings("unchecked")
	public List<Facility> searchFacility(String facilityName) {
		
		List<Facility> facilities = currentSession.createQuery("from Facility where facilityName like ? order by facilityName")
				.setParameter(0, "%"+ObjectUtils.nullSafe(facilityName)+"%")
				.list();
		return facilities;
	}

    public FacilityDTO getFacilityDetailsById(long facilityId){
        FacilityDTO facility = new FacilityDTO();
        try{
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("SELECT " +
                    " f.facility_id, " +
                    " f.`name`, " +
                    " f.contact_person_name, " +
                    " f.contact_info, " +
                    " f.is_notification_enabled, " +
                    " f.is_skip_consent, " +
                    " f.state, " +
                    " f.city, " +
                    " f.zip, " +
                    " f.address, " +
                    " innerQuery.pUserId, " +
                    " innerQuery.pFirstName, " +
                    " innerQuery.pLastName, " +
                    " innerQuery.pEmail, " +
                    " innerQuery.pPhone, " +
                    " f.facility_logo, f.slogan, f.email_text_name, f.group_id, " +
                    " f.timezone_name, f.timezone_offset, f.consent_form_file, f.lead_coach " +
                    " FROM " +
                    " facility f " +
                    " LEFT JOIN ( " +
                    " SELECT " +
                    "  us.user_id AS pUserId, " +
                    "  us.email AS pEmail, " +
                    "  us.first_name AS pFirstName, " +
                    "  us.last_name AS pLastName, " +
                    "  p.facility_id AS pFacilityId,   " +
                    "  us.phone AS pPhone " +
                    "  FROM " +
                    "  users us " +
                    "  LEFT JOIN provider p ON p.user_id = us.user_id " +
                    "  WHERE " +
                    "   us.user_type = 'ROLE_FACILITY_ADMIN' " +
                    "  AND ( " +
                    "   p.is_deleted = 0 " +
                    "   OR p.is_deleted IS NULL " +
                    " ) " +
                    " ) AS innerQuery ON f.facility_id = innerQuery.pFacilityId " +
                    " WHERE f.facility_id = " + facilityId +
                    " GROUP BY " +
                    " f.facility_id");

            List<Object> result = currentSession.createSQLQuery(stringBuilder.toString()).list();

            if(result != null && result.size() > 0){

                for(Object obj: result) {

                    Object[] objArray = (Object[]) obj;

                    long facId = ObjectUtils.nullSafeLong(objArray[0]);
                    String facName = ObjectUtils.nullSafe(objArray[1]);
                    String contactPersonName = ObjectUtils.nullSafe(objArray[2]);
                    String contactInfo = ObjectUtils.nullSafe(objArray[3]);
                    boolean isNotificationEnabled = ObjectUtils.nullSafeBoolean(objArray[4]);
                    boolean isSkipConsentForm = ObjectUtils.nullSafeBoolean(objArray[5]);

                    String state = ObjectUtils.nullSafe(objArray[6]);
                    String city = ObjectUtils.nullSafe(objArray[7]);
                    String zip = ObjectUtils.nullSafe(objArray[8]);
                    String address = ObjectUtils.nullSafe(objArray[9]);

                    long userId = ObjectUtils.nullSafeLong(objArray[10]);
                    String fAdminFirstName = ObjectUtils.nullSafe(objArray[11]);
                    String fAdminLastName = ObjectUtils.nullSafe(objArray[12]);
                    String fAdminEmail = ObjectUtils.nullSafe(objArray[13]);
                    String fAdminPhone = ObjectUtils.nullSafe(objArray[14]);

                    String logo = ObjectUtils.nullSafe(objArray[15]);
                    String slogan = ObjectUtils.nullSafe(objArray[16]);
                    String emailTextName = ObjectUtils.nullSafe(objArray[17]);
                    int groupId = ObjectUtils.nullSafeInteger(objArray[18]);

                    String timezoneName = ObjectUtils.nullSafe(objArray[19]);
                    String timezoneOffset = ObjectUtils.nullSafe(objArray[20]);
                    String consentFormFile = ObjectUtils.nullSafe(objArray[21]);
                    long leadCoach = ObjectUtils.nullSafeLong(objArray[22]);

                    facility = new FacilityDTO(facId, facName, contactPersonName, contactInfo, isNotificationEnabled, isSkipConsentForm, "", fAdminEmail);

                    facility.setState(state);
                    facility.setCity(city);
                    facility.setZip(zip);
                    facility.setAddress(address);

                    facility.setFacilityAdminFirstName(fAdminFirstName);
                    facility.setFacilityAdminLastName(fAdminLastName);
                    facility.setFacilityAdminEmail(fAdminEmail);
                    facility.setFacilityAdminPhone(fAdminPhone);

                    facility.setLogo(logo);
                    facility.setSlogan(slogan);
                    facility.setEmailTextName(emailTextName);
                    facility.setGroupId(groupId);
                    facility.setTimezoneName(timezoneName);
                    facility.setTimezoneOffset(timezoneOffset);
                    facility.setLeadCoach(leadCoach);
                    facility.setConsentFileName(consentFormFile);
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return facility;
    }

    public List<FacilityDTO> getFacilitiesList(int start, int end, String sSearch, String sortIndex, String sSortDirection){
        List<FacilityDTO> facilities = new ArrayList<FacilityDTO>();
        try{
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("SELECT " +
                    " f.facility_id, " +
                    " f.`name`, " +
                    " f.contact_person_name, " +
                    " f.contact_info, " +
                    " f.is_notification_enabled, " +
                    " f.is_skip_consent, " +
                    " innerQuery.fullName, " +
                    " innerQuery.pEmail " +
                    " FROM " +
                    " facility f " +
                    " LEFT JOIN ( " +
                    " SELECT " +
                    "  us.email AS pEmail, " +
                    "  CONCAT(us.first_name, ' ', us.last_name) AS fullName, " +
                    "  p.facility_id AS pFacilityId, " +
                    "  us.user_id AS pUserId " +
                    "  FROM " +
                    "  users us " +
                    "  LEFT JOIN provider p ON p.user_id = us.user_id " +
                    "  WHERE " +
                    "   us.user_type = 'ROLE_FACILITY_ADMIN' " +
                    "  AND ( " +
                    "   p.is_deleted = 0 " +
                    "   OR p.is_deleted IS NULL " +
                    " ) " +
                    " ) AS innerQuery ON f.facility_id = innerQuery.pFacilityId ");

            if(!sSearch.isEmpty()){
                stringBuilder.append(" WHERE (f.facility_id LIKE '%"+sSearch+"%'" +
                        " OR f.contact_person_name LIKE '%"+sSearch+"%'" +
                        " OR f.contact_info LIKE '%"+sSearch+"%'" +
                        " OR f.`name` LIKE '%"+sSearch+"%'" +
                        " OR innerQuery.fullName LIKE '%"+sSearch+"%'" +
                        " OR innerQuery.pEmail LIKE '%"+sSearch+"%' ) " );

            }

            stringBuilder.append(" GROUP BY f.facility_id ");

            if(!sortIndex.isEmpty()){
                stringBuilder.append(" ORDER BY " + sortIndex + " " + sSortDirection);
            } else {
                stringBuilder.append(" ORDER BY f.facility_id DESC ");
            }

            stringBuilder.append(" LIMIT " + start + "," + end + "");

            List<Object> result = currentSession.createSQLQuery(stringBuilder.toString()).list();

            if(result != null && result.size() > 0){

                for(Object obj: result) {

                    Object[] objArray = (Object[]) obj;

                    long facId = ObjectUtils.nullSafeLong(objArray[0]);
                    String facName = ObjectUtils.nullSafe(objArray[1]);
                    String contactPersonName = ObjectUtils.nullSafe(objArray[2]);
                    String contactInfo = ObjectUtils.nullSafe(objArray[3]);
                    boolean isNotificationEnabled = ObjectUtils.nullSafeBoolean(objArray[4]);
                    boolean isSkipConsentForm = ObjectUtils.nullSafeBoolean(objArray[5]);
                    String fAdminName = ObjectUtils.nullSafe(objArray[6]);
                    String fAdminEmail = ObjectUtils.nullSafe(objArray[7]);

                    facilities.add(new FacilityDTO(facId, facName, contactPersonName, contactInfo, isNotificationEnabled, isSkipConsentForm, fAdminName, fAdminEmail));
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return facilities;
    }

    public long getFacilitiesListCount(String sSearch) {
        long resultCount = 0l;
        try {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("SELECT " +
                    " f.facility_id, " +
                    " f.`name`, " +
                    " f.contact_person_name, " +
                    " f.contact_info, " +
                    " f.is_notification_enabled, " +
                    " f.is_skip_consent, " +
                    " innerQuery.fullName, " +
                    " innerQuery.pEmail " +
                    " FROM " +
                    " facility f " +
                    " LEFT JOIN ( " +
                    " SELECT " +
                    "  us.email AS pEmail, " +
                    "  CONCAT(us.first_name, ' ', us.last_name) AS fullName, " +
                    "  p.facility_id AS pFacilityId, " +
                    "  us.user_id AS pUserId " +
                    "  FROM " +
                    "  users us " +
                    "  LEFT JOIN provider p ON p.user_id = us.user_id " +
                    "  WHERE " +
                    "   us.user_type = 'ROLE_FACILITY_ADMIN' " +
                    "  AND ( " +
                    "   p.is_deleted = 0 " +
                    "   OR p.is_deleted IS NULL " +
                    " ) " +
                    " ) AS innerQuery ON f.facility_id = innerQuery.pFacilityId ");

            if (!sSearch.isEmpty()) {
                stringBuilder.append(" WHERE (f.facility_id LIKE '%" + sSearch + "%'" +
                        " OR f.contact_person_name LIKE '%" + sSearch + "%'" +
                        " OR f.contact_info LIKE '%" + sSearch + "%'" +
                        " OR f.`name` LIKE '%" + sSearch + "%'" +
                        " OR innerQuery.fullName LIKE '%" + sSearch + "%'" +
                        " OR innerQuery.pEmail LIKE '%" + sSearch + "%' ) ");

            }

            stringBuilder.append(" GROUP BY f.facility_id ORDER BY f.facility_id DESC ");

            List<Object> result = currentSession.createSQLQuery(stringBuilder.toString()).list();
            resultCount = result.size();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return resultCount;
    }

    public Object getAssessmentFormsCountByFacility(long facilityId){

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("select COUNT(f.form_id) from form f WHERE f.facility_id =  "+ facilityId);

        Object obj = null;
        try {
            obj = currentSession.createSQLQuery(stringBuilder.toString()).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj;
    }

    public Object getCompletedAssessmentFormsByPatientId(String uuid, long facilityId){

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("select COUNT(pf.form_id) from form f ");

        stringBuilder.append(" join patient_forms pf on pf.form_id = f.form_id ");
        stringBuilder.append(" where f.facility_id = " + facilityId);
        stringBuilder.append(" and pf.patient_id = '" + uuid + "'");
        stringBuilder.append(" and pf.form_id = f.form_id");
        stringBuilder.append(" and pf.is_completed=1 ");

        Object obj = null;
        try {
            obj = currentSession.createSQLQuery(stringBuilder.toString()).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj;
    }
}
