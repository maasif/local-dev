package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.FeedbackLogsDTO;
import com.healthslate.patientapp.model.entity.Feedback;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.util.DateUtils;
import com.healthslate.patientapp.util.ObjectUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class FeedbackDAO extends BaseDAO{

	@SuppressWarnings("unchecked")
	public List<Feedback> list() {
		List<Feedback> feedbackList = new ArrayList<Feedback>();
		try{
			feedbackList = currentSession.createQuery("from Feedback").list();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return feedbackList;
	}

	public Feedback getFeedbackByPatientId(Long id) {
		Feedback feedback = null;
		try {
			feedback = (Feedback) currentSession.createQuery("from Feedback f where f.patient.patientId = ? ")
					.setParameter(0, id).uniqueResult();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return feedback;
	}
	
	public int count() {
		Object countObj = null;
        int countInt = 0;
        try {
        	String query  = "SELECT COUNT(*) FROM feedback f " +
        			"JOIN patient p ON f.patient_id = p.patient_id " +
        			"JOIN users u ON u.user_id = p.user_id "+ 
        			" ORDER BY f.timestamp DESC ";
        	
            countObj = currentSession.createSQLQuery(query).uniqueResult();

            if(countObj != null){
                countInt = Integer.parseInt(countObj.toString());
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            countInt = 0;
        }

        return countInt;
	}
	
	 //[2/17/2015]: changed by __ad
    @SuppressWarnings("unchecked")
	public List<FeedbackLogsDTO> list(int start, int end, String sSearch, String sortIndex, String sSortDirection) {

        List<FeedbackLogsDTO> feedbackLogs = new ArrayList<FeedbackLogsDTO>();
        List<Object> serverObjs = new ArrayList<Object>();
        try {
        	String query  = "SELECT f.*, u.first_name FROM feedback f " +
        			"JOIN JOIN patient p ON f.patient_id = p.patient_id " +
        			"JOIN JOIN users u ON u.user_id = p.user_id ";

            if(!sSearch.isEmpty()){

                long timeStamp = DateUtils.getMillisecondsFromDateString(sSearch, DateUtils.DATE_FORMAT);
                long startOfDay = 0, endOfDay = 0;

                if(timeStamp > 0){
                    startOfDay = DateUtils.getStartOfDayTime(timeStamp);
                    endOfDay = DateUtils.getEndOfDayTime(timeStamp);
                    query += " WHERE (f.timestamp >= " + startOfDay + " AND f.timestamp <= " + endOfDay + ")";
                }else{
                    query += "WHERE (f.type LIKE '%"+sSearch+"%'" + ")" +
                            " OR u.first_name LIKE '%"+sSearch+"%'" +
                            " OR f.description LIKE '%"+sSearch+"%'" +
                            " OR f.patient_id LIKE '%"+sSearch+"%'";
                }
            }

            if(!sortIndex.isEmpty()){
                query += " ORDER BY " + sortIndex + " " + sSortDirection;
            } else {
                query += " ORDER BY f.timestamp DESC ";
            }

            query += " LIMIT " + start + "," + end + "";

        	serverObjs = (List<Object>) currentSession.createSQLQuery(query).list();

            if(serverObjs != null && serverObjs.size() > 0){

                for(Object obj: serverObjs) {
                    Object[] objArray = (Object[]) obj;

                    long feedbackLogId = Long.parseLong(objArray[0].toString());
                    Object descriptionFromDB = objArray[1];
                    String description = "";
                    if(descriptionFromDB != null){
                        description = (String)  descriptionFromDB;
                    }
                    Long patientId = ((BigInteger) objArray[2]).longValue();
                    Long timestamp = ((BigInteger) objArray[3]).longValue();;
                    String loggedDate = DateUtils.getFormattedDate(timestamp, DateUtils.DATE_FORMAT);

                    String type = ObjectUtils.nullSafe(objArray[4]);
                    String userName = ObjectUtils.nullSafe(objArray[5]);

                    feedbackLogs.add(new FeedbackLogsDTO(feedbackLogId, patientId, description, loggedDate, userName, type));
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return feedbackLogs;
    }

    //[2/17/2015]: changed by __ad
    @SuppressWarnings("unchecked")
    public List<FeedbackLogsDTO> listForPalite(int start, int end, String sSearch, String sortIndex, String sSortDirection) {

        List<FeedbackLogsDTO> feedbackLogs = new ArrayList<FeedbackLogsDTO>();
        List<Object> serverObjs = new ArrayList<Object>();
        try {
            String query  = "SELECT f.*, u.first_name, concat(u1.first_name, \" \", u1.last_name ) provider_name FROM feedback f " +
                    "LEFT JOIN patient p ON f.patient_id = p.patient_id " +
                    "LEFT JOIN users u ON u.user_id = p.user_id " +
                    "LEFT JOIN provider pr ON f.provider_id = pr.provider_id " +
                    "LEFT JOIN users u1 ON u1.user_id = pr.user_id ";

            if(!sSearch.isEmpty()){

                long timeStamp = DateUtils.getMillisecondsFromDateString(sSearch, DateUtils.DATE_FORMAT);
                long startOfDay = 0, endOfDay = 0;

                if(timeStamp > 0){
                    startOfDay = DateUtils.getStartOfDayTime(timeStamp);
                    endOfDay = DateUtils.getEndOfDayTime(timeStamp);
                    query += " WHERE (f.timestamp >= " + startOfDay + " AND f.timestamp <= " + endOfDay + ")";
                }else{
                    query += "WHERE (f.type LIKE '%"+sSearch+"%'" + ")" +
                            " OR u.first_name LIKE '%"+sSearch+"%'" +
                            " OR f.description LIKE '%"+sSearch+"%'" +
                            " OR f.patient_id LIKE '%"+sSearch+"%'";
                }
            }

            if(!sortIndex.isEmpty()){
                query += " ORDER BY " + sortIndex + " " + sSortDirection;
            } else {
                query += " ORDER BY f.timestamp DESC ";
            }

            query += " LIMIT " + start + "," + end + "";

            serverObjs = (List<Object>) currentSession.createSQLQuery(query).list();

            if(serverObjs != null && serverObjs.size() > 0){

                for(Object obj: serverObjs) {
                    Object[] objArray = (Object[]) obj;

                    long feedbackLogId = Long.parseLong(objArray[0].toString());
                    Object descriptionFromDB = objArray[1];
                    String description = "";
                    if(descriptionFromDB != null){
                        description = (String)  descriptionFromDB;
                    }
                    Long timestamp = ((BigInteger) objArray[3]).longValue();;
                    String loggedDate = DateUtils.getFormattedDate(timestamp, DateUtils.DATE_FORMAT);

                    String type = ObjectUtils.nullSafe(objArray[4]);
                    String userName = ObjectUtils.nullSafe(objArray[6]);
                    String providerName = ObjectUtils.nullSafe(objArray[7]);

                    feedbackLogs.add(new FeedbackLogsDTO(feedbackLogId, description, loggedDate, userName, type, providerName));
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return feedbackLogs;
    }

    public List<FeedbackLogsDTO> listByCoach(int start, int end, String sSearch, String sortIndex, String sSortDirection, Provider provider) {

        List<FeedbackLogsDTO> feedbackLogs = new ArrayList<FeedbackLogsDTO>();
        List<Object> serverObjs = new ArrayList<Object>();
        try {
            String query  = "SELECT f.*, u.first_name FROM feedback f " +
                    " JOIN patient p ON f.patient_id = p.patient_id " +
                    " JOIN provider_patient pp ON pp.patients_patient_id = p.patient_id " +
                    " JOIN users u ON u.user_id = p.user_id ";

            if(!sSearch.isEmpty()){

                long timeStamp = DateUtils.getMillisecondsFromDateString(sSearch, DateUtils.DATE_FORMAT);
                long startOfDay = 0, endOfDay = 0;

                if(timeStamp > 0){
                    startOfDay = DateUtils.getStartOfDayTime(timeStamp);
                    endOfDay = DateUtils.getEndOfDayTime(timeStamp);
                    query += " WHERE (f.timestamp >= " + startOfDay + " AND f.timestamp <= " + endOfDay + ") AND pp.providers_provider_id = " + provider.getProviderId();
                }else{
                    query += "WHERE (f.type LIKE '%"+sSearch+"%'" +
                            " OR u.first_name LIKE '%"+sSearch+"%'" +
                            " OR f.description LIKE '%"+sSearch+"%'" +
                            " OR f.patient_id LIKE '%"+sSearch+"%') AND f.type = 'Feedback' AND pp.providers_provider_id = " + provider.getProviderId();
                }
            }else{
                query += " WHERE f.type = 'Feedback' AND pp.providers_provider_id = " + provider.getProviderId();
            }

            if(!sortIndex.isEmpty()){
                query += " ORDER BY " + sortIndex + " " + sSortDirection;
            } else {
                query += " ORDER BY f.timestamp DESC ";
            }

            query += " LIMIT " + start + "," + end + "";

            serverObjs = (List<Object>) currentSession.createSQLQuery(query).list();

            if(serverObjs != null && serverObjs.size() > 0){

                for(Object obj: serverObjs) {
                    Object[] objArray = (Object[]) obj;

                    long feedbackLogId = Long.parseLong(objArray[0].toString());
                    Object descriptionFromDB = objArray[1];
                    String description = "";
                    if(descriptionFromDB != null){
                        description = (String)  descriptionFromDB;
                    }
                    Long patientId = ((BigInteger) objArray[2]).longValue();
                    Long timestamp = ((BigInteger) objArray[3]).longValue();;
                    String loggedDate = DateUtils.getFormattedDate(timestamp, DateUtils.DATE_FORMAT);

                    String type = ObjectUtils.nullSafe(objArray[4]);
                    String userName = ObjectUtils.nullSafe(objArray[6]);

                    feedbackLogs.add(new FeedbackLogsDTO(feedbackLogId, patientId, description, loggedDate, userName, type));
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return feedbackLogs;
    }

    public int countByCoach(Provider provider) {
        Object countObj = null;
        int countInt = 0;
        try {
            String query  = "SELECT COUNT(*) FROM feedback f " +
                    " JOIN patient p ON f.patient_id = p.patient_id " +
                    " JOIN provider_patient pp ON pp.patients_patient_id = p.patient_id " +
                    " JOIN users u ON u.user_id = p.user_id "+
                    " WHERE pp.providers_provider_id = " + provider.getProviderId() +
                    " ORDER BY f.timestamp DESC ";

            countObj = currentSession.createSQLQuery(query).uniqueResult();

            if(countObj != null){
                countInt = Integer.parseInt(countObj.toString());
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            countInt = 0;
        }

        return countInt;
    }

    public List<FeedbackLogsDTO> listByFacilityAdmin(int start, int end, String sSearch, String sortIndex, String sSortDirection, Provider provider) {

        List<FeedbackLogsDTO> feedbackLogs = new ArrayList<FeedbackLogsDTO>();
        List<Object> serverObjs = new ArrayList<Object>();
        try {
            String query  = "SELECT f.*, u.first_name FROM feedback f " +
                    " JOIN patient p ON f.patient_id = p.patient_id " +
                    " JOIN provider_patient pp ON pp.patients_patient_id = p.patient_id " +
                    " JOIN provider pro ON pro.provider_id = pp.providers_provider_id " +
                    " JOIN facility fac ON fac.facility_id = pro.facility_id " +
                    " JOIN users u ON u.user_id = p.user_id ";

            if(!sSearch.isEmpty()){

                long timeStamp = DateUtils.getMillisecondsFromDateString(sSearch, DateUtils.DATE_FORMAT);
                long startOfDay = 0, endOfDay = 0;

                if(timeStamp > 0){
                    startOfDay = DateUtils.getStartOfDayTime(timeStamp);
                    endOfDay = DateUtils.getEndOfDayTime(timeStamp);
                    query += " WHERE (f.timestamp >= " + startOfDay + " AND f.timestamp <= " + endOfDay + ") AND pp.providers_provider_id = " + provider.getProviderId();
                }else{
                    query += "WHERE (f.type LIKE '%"+sSearch+"%'" +
                            " OR u.first_name LIKE '%"+sSearch+"%'" +
                            " OR f.description LIKE '%"+sSearch+"%'" +
                            " OR f.patient_id LIKE '%"+sSearch+"%') AND f.type = 'Feedback' AND pp.providers_provider_id = " + provider.getProviderId() + " AND fac.facility_id = " + provider.getFacility().getFacilityId();
                }
            }else{
                query += " WHERE f.type = 'Feedback' AND pp.providers_provider_id = " + provider.getProviderId() + " AND fac.facility_id = " + provider.getFacility().getFacilityId();
            }

            if(!sortIndex.isEmpty()){
                query += " ORDER BY " + sortIndex + " " + sSortDirection;
            } else {
                query += " ORDER BY f.timestamp DESC ";
            }

            query += " LIMIT " + start + "," + end + "";

            serverObjs = (List<Object>) currentSession.createSQLQuery(query).list();

            if(serverObjs != null && serverObjs.size() > 0){

                for(Object obj: serverObjs) {
                    Object[] objArray = (Object[]) obj;

                    long feedbackLogId = Long.parseLong(objArray[0].toString());
                    Object descriptionFromDB = objArray[1];
                    String description = "";
                    if(descriptionFromDB != null){
                        description = (String)  descriptionFromDB;
                    }
                    Long patientId = ((BigInteger) objArray[2]).longValue();
                    Long timestamp = ((BigInteger) objArray[3]).longValue();;
                    String loggedDate = DateUtils.getFormattedDate(timestamp, DateUtils.DATE_FORMAT);

                    String type = ObjectUtils.nullSafe(objArray[4]);
                    String userName = ObjectUtils.nullSafe(objArray[5]);

                    feedbackLogs.add(new FeedbackLogsDTO(feedbackLogId, patientId, description, loggedDate, userName, type));
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return feedbackLogs;
    }

    public int countByFacilityAdmin(Provider provider) {
        Object countObj = null;
        int countInt = 0;
        try {
            String query  = "SELECT COUNT(*) FROM feedback f " +
                    " JOIN patient p ON f.patient_id = p.patient_id " +
                    " JOIN provider_patient pp ON pp.patients_patient_id = p.patient_id " +
                    " JOIN provider pro ON pro.provider_id = pp.providers_provider_id " +
                    " JOIN facility fac ON fac.facility_id = pro.facility_id " +
                    " JOIN users u ON u.user_id = p.user_id "+
                    " WHERE pp.providers_provider_id = " + provider.getProviderId() + " AND fac.facility_id = " + provider.getFacility().getFacilityId() +
                    " ORDER BY f.timestamp DESC ";

            countObj = currentSession.createSQLQuery(query).uniqueResult();

            if(countObj != null){
                countInt = Integer.parseInt(countObj.toString());
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            countInt = 0;
        }

        return countInt;
    }
}
