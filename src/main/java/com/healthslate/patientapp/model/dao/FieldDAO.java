package com.healthslate.patientapp.model.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.amazonaws.services.datapipeline.model.Field;
import com.healthslate.patientapp.util.HibernateUtil;

public class FieldDAO extends BaseDAO {

	@SuppressWarnings("unchecked")
	public List<Field> list() {		
		List<Field> fields = null;
		try {
			fields = currentSession.createQuery(" FROM Field ").list();			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return fields;
	}
}
