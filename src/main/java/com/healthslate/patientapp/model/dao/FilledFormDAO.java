package com.healthslate.patientapp.model.dao;

import java.math.BigInteger;
import java.util.*;

import com.amazonaws.services.opsworks.model.App;
import com.healthslate.patientapp.model.dto.*;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import org.hibernate.HibernateException;

import com.amazonaws.services.datapipeline.model.Query;
import com.google.gson.Gson;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class FilledFormDAO extends BaseDAO {

	public Long saveForm(FilledForm filledForm){
		try{
			currentSession.saveOrUpdate(filledForm);        	
        } catch (HibernateException e) {
            e.printStackTrace();
        }
		return filledForm.getFilledFormId();
	}

	public Long saveFormScheduler(FilledForm filledForm){
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = null;
		if(session.getTransaction() != null && session.getTransaction().isActive()){
			transaction = session.getTransaction();
		} else{
			transaction = session.beginTransaction();
		}
		session.clear();
		try{
			session.saveOrUpdate(filledForm);
			transaction.rollback();
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		}
		return filledForm.getFilledFormId();
	}

	public FilledForm getFormById(Long formId){
		FilledForm filledForm = null;
		try{
        	filledForm = (FilledForm) currentSession.createQuery("FROM FilledForm where filledFormId = ? ")
					.setParameter(0, formId).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();            
        }
		return filledForm;
	}

	public FilledForm getFormByIdScheduler(Long formId){
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = null;
		if(session.getTransaction() != null && session.getTransaction().isActive()){
			transaction = session.getTransaction();
		} else{
			transaction = session.beginTransaction();
		}
		session.clear();
		FilledForm filledForm = null;
		try{
			filledForm = (FilledForm) session.createQuery("FROM FilledForm where filledFormId = ? ")
					.setParameter(0, formId).uniqueResult();
			transaction.rollback();
		} catch (HibernateException e) {
			e.printStackTrace();
			transaction.rollback();
		}
		return filledForm;
	}

	@SuppressWarnings("unchecked")
	public List<GoalDetailsDTO> getGoalsByPatientId(Long patientId, int catId, String showAchieved) {		
		StringBuffer queryString = new StringBuffer();		
		List<GoalDetailsDTO> goalNames = new ArrayList<GoalDetailsDTO>();			
		
		String categoryName = "";
		String queryAll = "";
		String achievedSQL = "IS NOT NULL";
		
		//if achieved flag is empty then get those forms which dont yet achieved
		if(ObjectUtils.isEmpty(showAchieved)) {
			achievedSQL = "IS NULL";
		}
		
		//-1 is for All data to return, so check here for other categories and filter data according
		if(catId > -1) {
			categoryName = new GoalCategoryDAO().getGoalCategoryNameById(catId);
			queryAll = " WHERE gc.name = '"+categoryName+"' ";
		}		
		
		try {
			
			queryString.append("SELECT ff.created_date, fd.filled_form_id, fd.field_name, fd.field_value, gar.goal_assessment_rating, gar.rated_on, ff.date_achieved "
					+ " FROM filled_form ff "
					+ " JOIN form_data fd ON ff.filled_form_id = fd.filled_form_id "
					+ " LEFT JOIN goal_assessment_rating gar ON ff.filled_form_id = gar.filled_form_id "
					+ " WHERE ff.type = 'GOAL' "
					+ " AND (fd.field_name = '"+AppConstants.GOAL_CATEGORY+"' OR fd.field_name = '"+AppConstants.GOAL_OPTION+"')"
					+ " AND ff.patient_id = " + patientId +" "
					+ " AND ff.date_achieved " + achievedSQL
					+ " AND fd.field_value IN ( " 
					+ " SELECT g.name "
					+ " FROM goal g "
					+ " JOIN goal_category gc ON g.goal_category_id = gc.goal_category_id "
					+ queryAll
					+ " ) "					
					+ " ORDER BY ff.created_date DESC");
			
			List<Object> result = currentSession.createSQLQuery(queryString.toString()).list();
						
			for (Object obj: result) {							
				Object[] objArray = (Object[]) obj;
				Long formId = ((BigInteger) objArray[1]).longValue();
				
				GoalDetailsDTO goalDetailsDTO = new GoalDetailsDTO();
				
				goalDetailsDTO.setFormId(formId);
				goalDetailsDTO.setGoalName((String) objArray[3]);
				goalDetailsDTO.setCreatedOn(((Date) objArray[0]).getTime());				
				goalDetailsDTO.setRating(validateNull(objArray[4]));
				
				long ratedOn = -1;
				if(objArray[5] != null) {
					ratedOn = ((Date) objArray[5]).getTime();
				}
				
				String status = AppConstants.StatusConstants.NOTACHIEVED.getValue();
				if(objArray[6] != null) {
					status = AppConstants.StatusConstants.ACHIEVED.getValue();
				}
				
				goalDetailsDTO.setGoalStatus(status);
				goalDetailsDTO.setRatedOn(ratedOn);
				goalNames.add(goalDetailsDTO);
			}
			
			return goalNames;					
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		
		return null;
	}
	
	@SuppressWarnings("unused")
	private List<CoachGoalAssessment> getCoachListMapped(List<CoachGoalAssessment> coachGoalAssessments){
		
		List<CoachGoalAssessment> coachGoals = new ArrayList<CoachGoalAssessment>();
		
		if(coachGoalAssessments != null){
			for (CoachGoalAssessment coachGoalAssessment : coachGoalAssessments) {
				User user = coachGoalAssessment.getUser();
				if(user != null){												
					coachGoalAssessment.setAssessedByName(user.getLastName() + ", " + user.getFirstName());					
					String formattedDate = DateUtils.getFormattedDate(coachGoalAssessment.getAssessedDate(), DateUtils.DATE_FORMAT);						
					coachGoalAssessment.setAssessedDateString(formattedDate);
				}
				
				coachGoals.add(coachGoalAssessment);
			}
		}
		
		return coachGoals;
	}
	
	@SuppressWarnings("unchecked")
	public List<GoalDashboardDTO> getGoalsForDashboard(Long patientId, int start, int end, String sSearch, String sortIndex, String sSortDirection) {
		StringBuffer queryString = new StringBuffer();		
		List<GoalDashboardDTO> goalDashboardList = new ArrayList<GoalDashboardDTO>();			
		 
		try {

			queryString.append("SELECT ff.created_date, ff.filled_form_id, gar.goal_assessment_rating, gar.rated_on"
					+ " FROM filled_form ff "
					+ " LEFT JOIN goal_assessment_rating gar ON ff.filled_form_id = gar.filled_form_id "
					+ " WHERE ff.type = 'GOAL' "
					+ " AND ff.patient_id = " + patientId + " ");

            if(!ObjectUtils.isEmpty(sSearch)){
                /*queryString.append(" AND (p.patient_id LIKE '%"+sSearch+"%'" +
                        " OR u.first_name LIKE '%"+sSearch+"%'" +
                        " OR u.last_name LIKE '%"+sSearch+"%'" +
                        " OR u.display_name LIKE '%"+sSearch+"%'" +
                        " OR u.email LIKE '%"+sSearch+"%'" +
                        " OR u.phone LIKE '%"+sSearch+"%')");*/

            }
            queryString.append(" GROUP BY ff.filled_form_id ");
            if(!sortIndex.isEmpty()){
                queryString.append(" ORDER BY " + sortIndex + " " + sSortDirection);
            } else {
                queryString.append(" ORDER BY ff.created_date DESC ");
            }

            queryString.append(" LIMIT " + start + "," + end + "");

			List<Object> result = currentSession.createSQLQuery(queryString.toString()).list();
			
			//get all lists into id/name pair
			Map<Long, String> allGoalCategories = CommonUtils.listToMap(new GoalCategoryDAO().getAllCategories());
			Map<Long, String> allGoals = CommonUtils.listToMap(new GoalDAO().getAllGoals());
			Map<Long, String> allActionPlans = CommonUtils.listToMap(new ActionPlanDAO().getAllActionPlans());
				
			List<Long> filledFormIdList = new ArrayList<Long>();
			
			for (Object obj: result) {							
				Object[] objArray = (Object[]) obj;
				Long formId = ((BigInteger) objArray[1]).longValue();
				
				filledFormIdList.add(formId);
				
				GoalDashboardDTO goalDashboard = new GoalDashboardDTO();
				
				//gets formData list from DB
				List<FormData> dataList = new FormDataDAO().getFormWithFilledFormId(formId);
				goalDashboard.setFormDataList(dataList);
				
				//gets coachAssessment list from DB
				List<CoachGoalAssessment> coachGoalAssessments = getCoachListMapped(new CoachGoalAssessmentDAO().getCoachAssessmentsByFormId(formId));				
				goalDashboard.setCoachGoalAssessments(coachGoalAssessments);
				
				for (FormData formData : dataList) {
					
					String fieldName = formData.getFieldName();
					String fieldValue = formData.getFieldValue();
                    Map<String, String> details = CommonUtils.buildGoalDetailsMap(dataList);

					if(fieldName.equalsIgnoreCase(AppConstants.ACTION_PLAN)){

                        String actionPlan = allActionPlans.get(Long.parseLong(fieldValue));
                        String actionPlanString = CommonUtils.buildActionPlanString(details,
                                actionPlan,
                                details.get(AppConstants.PLAN_FREQUENCY),
                                details.get(AppConstants.CALENDAR_FREQUENCY),
                                details.get(AppConstants.GOAL_MEALS),
                                details.get(AppConstants.GOAL_OTHER));

                        goalDashboard.setActionPlan(actionPlanString);

					}else if(fieldName.equalsIgnoreCase(AppConstants.GOAL_OPTION)){
						goalDashboard.setGoalName(allGoals.get(Long.parseLong(fieldValue)));
                    }else if(fieldName.equalsIgnoreCase(AppConstants.GOAL_OTHERS)){
                        if(ObjectUtils.isEmpty(fieldValue)){
                            long goalOptionId = Long.parseLong(details.get(AppConstants.GOAL_OPTION));
                            goalDashboard.setGoalName(allGoals.get(goalOptionId));
                        }else{
                            goalDashboard.setGoalName(fieldValue);
                        }
					}else if(fieldName.equalsIgnoreCase(AppConstants.GOAL_CATEGORY)){
						goalDashboard.setGoalCategoryName(allGoalCategories.get(Long.parseLong(fieldValue)));						
					}
				}
								
				goalDashboard.setFilledFormId(formId);
								
				//goal created on date
				Object createdDateFromDB = (Object)objArray[0];
				String createdDate = "";
				if(createdDateFromDB != null){
					Date dt = (Date)createdDateFromDB;
					createdDate = DateUtils.formatDateOnly(dt);
				}
				goalDashboard.setGoalCreatedDate(createdDate);
				
				//goal rating
				Object goalRatingFromDB = (Object)objArray[2];
				int goalRating = -1;
				if(goalRatingFromDB != null){
					goalRating = (Integer)goalRatingFromDB;
				}
				goalDashboard.setGoalRating(goalRating);
				
				//goal rated on
				Object goalRatedOnFromDB = (Object)objArray[3];
				String goalRatedOn = "";
				if(goalRatedOnFromDB != null){
					Date dt = (Date)goalRatedOnFromDB;
					goalRatedOn = DateUtils.formatDateOnly(dt);
				}
				goalDashboard.setGoalRatedOn(goalRatedOn);
				
				goalDashboardList.add(goalDashboard);
			}		
			
			//get all CoachAssessment against all filledFormIds
			List<CoachGoalAssessment> coachGoalAssessments = new CoachGoalAssessmentDAO().getCoachAssessmentsByFormId(filledFormIdList);
			
			if(coachGoalAssessments != null && coachGoalAssessments.size() > 0){
				
				Map<Long, Object> latestCgsWithFilledForm = new HashMap<Long, Object>();
				
				//loop to get the list of recent created CoachAssessment
				for (CoachGoalAssessment coachGoalAssessment : coachGoalAssessments) {
					
					FilledForm filledForm = coachGoalAssessment.getFilledForm();
					
					if(filledForm != null){
						
						//if not in map, then add it
						if(!latestCgsWithFilledForm.containsKey(filledForm.getFilledFormId())){
						
							latestCgsWithFilledForm.put(filledForm.getFilledFormId(), coachGoalAssessment);	
							
						}else{
							
							CoachGoalAssessment coachGoalAssessmentFromMap = (CoachGoalAssessment) latestCgsWithFilledForm.get(filledForm.getFilledFormId());
							
							if(coachGoalAssessmentFromMap != null){
								
								Date firstDate = new Date(coachGoalAssessment.getAssessedDate());
								Date endDate = new Date(coachGoalAssessmentFromMap.getAssessedDate());
																
								//if date from loop is greater than date from map override it
								if(DateUtils.compareDates(firstDate, endDate)){									
									latestCgsWithFilledForm.put(filledForm.getFilledFormId(), coachGoalAssessment);										
								}
							}							
						}
					}					
				}
				
				//setting to main goals list (status, notes and date)				
				for (GoalDashboardDTO goalDashboardDTO : goalDashboardList) {
					
					CoachGoalAssessment coachGoalAssessmentFromMap = (CoachGoalAssessment) latestCgsWithFilledForm.get(goalDashboardDTO.getFilledFormId());
					
					if(coachGoalAssessmentFromMap != null){
						goalDashboardDTO.setCoachStatus(coachGoalAssessmentFromMap.getStatus());
						goalDashboardDTO.setNotes(coachGoalAssessmentFromMap.getNotes());						
						User user = coachGoalAssessmentFromMap.getUser();						
						if(user != null){														
							goalDashboardDTO.setAssessedBy(user.getLastName() + ", " + user.getFirstName());
						}
						String formattedDate = DateUtils.getFormattedDate(coachGoalAssessmentFromMap.getAssessedDate(), DateUtils.DATE_FORMAT);						
						goalDashboardDTO.setCoachAssessedDate(formattedDate);
					}
				}
			}
			
			return goalDashboardList;					
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		
		return goalDashboardList;
	}
	
	@SuppressWarnings("unchecked")
	public List<GoalSummaryDTO> getGoalsByPatientId(long patientId, long filledFormId) {		
		StringBuffer queryString = new StringBuffer();		
		List<GoalSummaryDTO> goalSummaryList = new ArrayList<GoalSummaryDTO>();			

		String filterByFilledFormId = "";
		if(filledFormId != -1){
			filterByFilledFormId = " AND ff.filled_form_id = " + filledFormId + " ";
		}
		
		try {
			
			queryString.append("SELECT fd.filled_form_id, fd.form_data_id, gar.goal_assessment_rating, fd.field_value, ff.should_remind, ff.created_date "
					+ " FROM filled_form ff "
					+ " JOIN form_data fd ON ff.filled_form_id = fd.filled_form_id "
					+ " LEFT JOIN goal_assessment_rating gar ON ff.filled_form_id = gar.filled_form_id "
					+ " WHERE ff.type = '"+ AppConstants.GOAL +"' "		
					+ " AND fd.field_name = '" + AppConstants.GOAL_CATEGORY +"' "			
					+ " AND ff.patient_id = " + patientId +" "
					+ filterByFilledFormId
					+ "GROUP BY ff.filled_form_id ORDER BY ff.created_date DESC");
			
			List<Object> result = currentSession.createSQLQuery(queryString.toString()).list();
						
			for (Object obj: result) {							
				Object[] objArray = (Object[]) obj;
				GoalSummaryDTO goalSummary = new GoalSummaryDTO();
				
				Long filledFormIdDB = ((BigInteger) objArray[0]).longValue();
				Long formDataId = ((BigInteger) objArray[1]).longValue();
				
				goalSummary.setFilledFormId(filledFormIdDB);
				goalSummary.setFormDataId(formDataId);
				goalSummary.setRating(validateNull(objArray[2]));
				goalSummary.setCategoryName(validateNullString(objArray[3]));

				Object shouldRemindFromDB = (Object)objArray[4];
				boolean shouldRemind = false;
				if(shouldRemindFromDB != null){
					shouldRemind = (Boolean)shouldRemindFromDB;
				}		
				goalSummary.setShouldRemind(shouldRemind);				
				
				Object createdDateFromDB = (Object)objArray[5];
				Date createdDate = new Date();
				if(createdDateFromDB != null){
					createdDate = (Date)createdDateFromDB;
				}
				
				goalSummary.setCreatedDate(DateUtils.formatDateOnly(createdDate));
				goalSummaryList.add(goalSummary);
			}
			
			return goalSummaryList;					
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		
		return null;
	}
	
	//get all habits by patient id, __oz
	public List<HabitDetailsDTO> getHabitsByPatientId(Long patientId){
		
		StringBuffer queryString = new StringBuffer();		
		List<HabitDetailsDTO> habits = new ArrayList<HabitDetailsDTO>();			
		
		try {
			
			//query to get habits by patient id
			queryString.append("SELECT fd.filled_form_id, fd.field_name, fd.field_value, ff.created_date, ff.should_remind "
							 + " FROM filled_form ff "
							 + " JOIN form_data fd ON ff.filled_form_id = fd.filled_form_id "					
							 + " WHERE ff.type = '"+AppConstants.HABIT+"' "					
							 + " AND ff.patient_id = "+patientId+" " 										
							 + " ORDER BY ff.filled_form_id DESC");
			
			@SuppressWarnings("unchecked")
			List<Object> result = currentSession.createSQLQuery(queryString.toString()).list();
			
			//created map for storing habit objects by id
			Map<Long, HabitDetailsDTO> habitsMap = new HashMap<Long, HabitDetailsDTO>();
			
			for (Object obj: result) {							
				Object[] objArray = (Object[]) obj;
				Long formId = ((BigInteger) objArray[0]).longValue();
				
				HabitDetailsDTO habitDTO = null;
				
				//if formid not available in map then create new object
				if(!habitsMap.containsKey(formId)) {
					
					habitDTO = new HabitDetailsDTO();
					habitDTO.setFormId(formId);					
					habitDTO = populateHabitsObject(habitDTO, objArray);
					
				}else {
					//else get already saved object and add field values to object
					habitDTO = habitsMap.get(formId);					
					habitDTO = populateHabitsObject(habitDTO, objArray);
				}
				
				habitsMap.put(formId, habitDTO);
			}
			
			//in the end get the list of habit objects from map and add to habit list
			habits = new ArrayList<HabitDetailsDTO>(habitsMap.values());

            Collections.sort(habits, new Comparator<HabitDetailsDTO>() {
                @Override
                public int compare(HabitDetailsDTO o1, HabitDetailsDTO o2) {
                    Date d1 = new Date(o1.getCreatedOn());
                    Date d2 = new Date(o2.getCreatedOn());

                    return d2.compareTo(d1);
                }
            });

			return habits;					
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		
		return null;
	}
	
	//populate habit object, __oz
	public HabitDetailsDTO populateHabitsObject(HabitDetailsDTO habitDTO, Object[] objArray) {
		try {
			String fieldName = objArray[1].toString();
			String fieldValue = validateNullString(objArray[2]);
			
			if(fieldName.equalsIgnoreCase("triggerActivity")) {
				habitDTO.setTriggeringActivity(fieldValue);
			}else if(fieldName.equalsIgnoreCase("healthierHabit")) {
				habitDTO.setHealthierHabit(fieldValue);
			}else if(fieldName.equalsIgnoreCase("celebrate")) {
				habitDTO.setCelebrate(fieldValue);
			}
							
			habitDTO.setCreatedOn(((Date) objArray[3]).getTime());
			
			Object shouldRemindFromDB = (Object)objArray[4];
			boolean shouldRemind = false;
			if(shouldRemindFromDB != null){
				shouldRemind = (Boolean)shouldRemindFromDB;
			}		
			habitDTO.setShouldRemind(shouldRemind);		
			
			return habitDTO;
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
		return null;
	}
	
	public boolean deleteFilledFormByType(long filledFormId, String formType){
		
		try {
			
			int result = currentSession.createSQLQuery(" DELETE FROM filled_form "
					+ " WHERE type = '"+ formType +"'"
					+ " AND filled_form_id = " + filledFormId + "").executeUpdate();
			
			return (result >= 0) ? true : false;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

    public GoalDTO getLatestActiveGoalByPatient(long patientId) {
        StringBuffer queryString = new StringBuffer();
        GoalDTO goal = null;

        //get all lists into id/name pair
        Map<Long, Object> allGoalCategories = CommonUtils.listToMapObjects(new GoalCategoryDAO().getAllCategories());
        Map<Long, String> allGoals = CommonUtils.listToMap(new GoalDAO().getAllGoals());
        Map<Long, String> allActionPlans = CommonUtils.listToMap(new ActionPlanDAO().getAllActionPlans());

        try {

            queryString.append("SELECT fd.filled_form_id, fd.form_data_id, gar.goal_assessment_rating, fd.field_value, ff.should_remind, ff.created_date "
                    + " FROM filled_form ff "
                    + " JOIN form_data fd ON ff.filled_form_id = fd.filled_form_id "
                    + " LEFT JOIN goal_assessment_rating gar ON ff.filled_form_id = gar.filled_form_id "
                    + " WHERE ff.type = '"+ AppConstants.GOAL +"' "
                    + " AND fd.field_name = '" + AppConstants.GOAL_CATEGORY +"' "
                    + " AND ff.patient_id = " + patientId +" "
                    + " ORDER BY ff.created_date DESC");

            Object result = currentSession.createSQLQuery(queryString.toString()).setMaxResults(1).uniqueResult();

            if(result != null){
                Object[] objArray = (Object[]) result;
                goal = new GoalDTO();

                Long filledFormIdDB = ((BigInteger) objArray[0]).longValue();
                Long formDataId = ((BigInteger) objArray[1]).longValue();

                //gets formData list from DB
                List<FormData> dataList = new FormDataDAO().getFormWithFilledFormId(filledFormIdDB);
                Map<String, String> details = CommonUtils.buildGoalDetailsMap(dataList);

                for (FormData formData : dataList) {

                    String fieldName = formData.getFieldName();
                    String fieldValue = formData.getFieldValue();

                    if(fieldName.equalsIgnoreCase(AppConstants.ACTION_PLAN)){

                        String actionPlan = allActionPlans.get(Long.parseLong(fieldValue));

                        String actionPlanString = CommonUtils.buildActionPlanString(details,
                                                    actionPlan,
                                                    details.get(AppConstants.PLAN_FREQUENCY),
                                                    details.get(AppConstants.CALENDAR_FREQUENCY),
                                                    details.get(AppConstants.GOAL_MEALS),
                                                    details.get(AppConstants.GOAL_OTHER));

                        goal.setActionPlan(actionPlanString);

                    }else if(fieldName.equalsIgnoreCase(AppConstants.GOAL_OPTION)){
                        goal.setGoalName(allGoals.get(Long.parseLong(fieldValue)));
                    }else if(fieldName.equalsIgnoreCase(AppConstants.GOAL_OTHERS)){
                        if(ObjectUtils.isEmpty(fieldValue)){
                            long goalOptionId = Long.parseLong(details.get(AppConstants.GOAL_OPTION));
                            goal.setGoalName(allGoals.get(goalOptionId));
                        }else{
                            goal.setGoalName(fieldValue);
                        }
                    }else if(fieldName.equalsIgnoreCase(AppConstants.GOAL_CATEGORY)){
                        GoalCategory goalCategory = (GoalCategory)allGoalCategories.get(Long.parseLong(fieldValue));
                        goal.setCategoryName(goalCategory.getName());
                        goal.setTextColor(goalCategory.getTextColor());
                        goal.setBgColor(goalCategory.getBgColor());
                    }else if(fieldName.equalsIgnoreCase(AppConstants.OVERCOMING_CHALLENGE)){
                        goal.setOvercomingChallenge(fieldValue);
                    }else if(fieldName.equalsIgnoreCase(AppConstants.MY_CHALLENGE)){
                        goal.setChallenge(fieldValue);
                    } else if(fieldName.equalsIgnoreCase(AppConstants.MY_REWARD)){
                        goal.setReward(fieldValue);
                    }
                }

                goal.setFilledFormId(filledFormIdDB);
                goal.setFormDataId(formDataId);
                goal.setRating(validateNull(objArray[2]));

                Object shouldRemindFromDB = (Object)objArray[4];
                boolean shouldRemind = false;
                if(shouldRemindFromDB != null){
                    shouldRemind = (Boolean)shouldRemindFromDB;
                }
                goal.setShouldRemind(shouldRemind);

                Object createdDateFromDB = (Object)objArray[5];
                Date createdDate = new Date();
                if(createdDateFromDB != null){
                    createdDate = (Date)createdDateFromDB;
                }

                goal.setCreatedDate(DateUtils.formatDateOnly(createdDate));
            }

            return goal;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

	public Map<String, Object> getGoalsAndHabitsByPatientId(long patientId, long filledFormId) {
		StringBuffer queryString = new StringBuffer();

		Map<String, Object> goalsHabitsCombined = new LinkedHashMap<String, Object>();

		List<GoalSummaryDTO> goals = new ArrayList<GoalSummaryDTO>();
		List<HabitDetailsDTO> habits = new ArrayList<HabitDetailsDTO>();

		String filterByFilledFormId = "";
		if(filledFormId != -1){
			filterByFilledFormId = " AND ff.filled_form_id = " + filledFormId + " ";
		}

		try {

			//get all lists into id/name pair
			Map<Long, String> allGoalCategories = CommonUtils.listToMap(new GoalCategoryDAO().getAllCategories());
			Map<Long, String> allGoals = CommonUtils.listToMap(new GoalDAO().getAllGoals());
			Map<Long, String> allActionPlans = CommonUtils.listToMap(new ActionPlanDAO().getAllActionPlans());

			queryString.append("SELECT fd.filled_form_id, gar.goal_assessment_rating, fd.field_name, fd.field_value, ff.should_remind, ff.type, ff.created_date "
					+ " FROM filled_form ff "
					+ " JOIN form_data fd ON ff.filled_form_id = fd.filled_form_id "
					+ " LEFT JOIN goal_assessment_rating gar ON ff.filled_form_id = gar.filled_form_id "
					+ " AND ff.patient_id = " + patientId +" "
					+ filterByFilledFormId
					+ " ORDER BY ff.created_date DESC");

			List<Object> result = currentSession.createSQLQuery(queryString.toString()).list();

			for (Object obj: result) {
				Object[] objArray = (Object[]) obj;

				String formType = ObjectUtils.nullSafe(objArray[5]);
				String fieldName = ObjectUtils.nullSafe(objArray[2]);
				String fieldValue = ObjectUtils.nullSafe(objArray[3]);

				if(formType.equalsIgnoreCase(AppConstants.GOAL)){
					GoalSummaryDTO goalSummary = new GoalSummaryDTO();
					boolean shouldAdd = false;
					if(fieldName.equalsIgnoreCase(AppConstants.ACTION_PLAN)){
						shouldAdd = true;
						goalSummary.setActionPlan(allActionPlans.get(Long.parseLong(fieldValue)));
					}else if(fieldName.equalsIgnoreCase(AppConstants.GOAL_OPTION)){
						shouldAdd = true;
						goalSummary.setGoalName(allGoals.get(Long.parseLong(fieldValue)));
					}else if(fieldName.equalsIgnoreCase(AppConstants.GOAL_CATEGORY)){
						shouldAdd = true;
						goalSummary.setCategoryName(allGoalCategories.get(Long.parseLong(fieldValue)));
					}

					if(shouldAdd){
						goals.add(goalSummary);
					}
				}else{
					HabitDetailsDTO habitDTO = new HabitDetailsDTO();
					boolean shouldAdd = false;
					if(fieldName.equalsIgnoreCase("triggerActivity")) {
						shouldAdd = true;
						habitDTO.setTriggeringActivity(fieldValue);
					}else if(fieldName.equalsIgnoreCase("healthierHabit")) {
						shouldAdd = true;
						habitDTO.setHealthierHabit(fieldValue);
					}else if(fieldName.equalsIgnoreCase("celebrate")) {
						shouldAdd = true;
						habitDTO.setCelebrate(fieldValue);
					}

					if(shouldAdd){
						habitDTO.setCreatedOn(((Date) objArray[6]).getTime());

						Object shouldRemindFromDB = (Object)objArray[4];
						boolean shouldRemind = false;
						if(shouldRemindFromDB != null){
							shouldRemind = (Boolean)shouldRemindFromDB;
						}
						habitDTO.setShouldRemind(shouldRemind);

						habits.add(habitDTO);
					}
				}
			}

			goalsHabitsCombined.put(AppConstants.GOAL, goals);
			goalsHabitsCombined.put(AppConstants.HABIT, habits);

			return goalsHabitsCombined;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<Object> getNonRatedGoals() {
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = null;
		if(session.getTransaction() != null && session.getTransaction().isActive()){
			transaction = session.getTransaction();
		}else{
			transaction = session.beginTransaction();
		}
		session.clear();
		List<Object> objects = null;
		try {
			String query = "SELECT " +
					" us.phone, " +
					" pat.patient_id, " +
					" innerQuery1.time AS time, " +
					" innerQuery1.id AS filledFormId, " +
					" innerQuery1.nCount, d.device_mac_address, d.device_type "+
					"FROM " +
					" users us " +
					"JOIN patient pat ON pat.user_id = us.user_id " +
					"JOIN filled_form ff ON pat.patient_id = ff.patient_id " +
					"LEFT JOIN goal_assessment_rating gar ON ff.filled_form_id = gar.filled_form_id " +
                    "LEFT JOIN device d ON d.device_mac_address = pat.device_mac_address " +
					"JOIN ( " +
					" SELECT " +
					"  ff.patient_id, " +
					"  ff.filled_form_id AS id, " +
					"  ff.notification_count AS nCount, "+
					"  innerQuery.time AS time " +
					" FROM " +
					"  filled_form ff " +
					" INNER JOIN ( " +
					"  SELECT " +
					"   MAX(ff.created_date) AS time " +
					"  FROM " +
					"   filled_form ff " +
					"  WHERE " +
					"   ff.type = 'GOAL' " +
					"  AND ( TO_DAYS(NOW()) - TO_DAYS(ff.created_date) ) > 6 " +
                    "  AND (ff.notification_count = 1 OR ff.notification_count IS NULL) " +
					"  GROUP BY " +
					"   ff.patient_id " +
					"  ORDER BY " +
					"   ff.created_date DESC " +
					" ) AS innerQuery ON innerQuery.time = ff.created_date  " +
					"  " +
					") AS innerQuery1 ON innerQuery1.patient_id = ff.patient_id " +
					"WHERE " +
					" (gar.goal_assessment_rating = 0 OR gar.goal_assessment_rating IS NULL) " +
					" AND ( pat.is_deleted = 0 OR pat.is_deleted IS NULL) " +
					"GROUP BY " +
					" pat.patient_id";
			objects = session.createSQLQuery(query).list();
            transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		}
		return objects;
	}
	public int validateNull(Object val) {
		return (val == null) ? -1 : (Integer)val;
	}	
	
	public String validateNullString(Object val) {
		return (val == null || val == "") ? "" : val.toString();
	}

    public List<GoalRatingDTO> getGoalRatingsByFormId(long filledFormId) {
        StringBuffer queryString = new StringBuffer();
        List<GoalRatingDTO> ratings = new ArrayList<GoalRatingDTO>();

        String filterByFilledFormId = "";
        if(filledFormId != -1){
            filterByFilledFormId = " AND ff.filled_form_id = " + filledFormId + " ";
        }

        try {

            queryString.append("SELECT ff.filled_form_id, gar.goal_assessment_rating, gar.rated_on, fd.field_value  "
                    + " FROM filled_form ff "
                    + " JOIN form_data fd ON ff.filled_form_id = fd.filled_form_id "
                    + " JOIN goal_assessment_rating gar ON ff.filled_form_id = gar.filled_form_id "
                    + " WHERE ff.type = '"+ AppConstants.GOAL +"' "
                    + " AND fd.field_name = '" + AppConstants.GOAL_CATEGORY +"' "
                    + " AND ff.filled_form_id = " + filledFormId + " ORDER BY gar.rated_on DESC");

            List<Object> result = currentSession.createSQLQuery(queryString.toString()).list();

            for (Object obj: result) {
                Object[] objArray = (Object[]) obj;

                long filledFormIdDB = ObjectUtils.nullSafeLong(objArray[0]);
                int rating  = ObjectUtils.nullSafeInteger(objArray[1]);
                Date dateRated = (Date)objArray[2];
                String formattedDate = "N/A";
                if(dateRated != null){
                    formattedDate = DateUtils.getFormattedDate(dateRated.getTime(), DateUtils.DATE_FORMAT);
                }
                GoalRatingDTO ratingDTO = new GoalRatingDTO(filledFormIdDB, rating, formattedDate);
                ratings.add(ratingDTO);
            }

            return ratings;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<GoalRatingDTO> getGoalRatingsByPatient(long patientId) {
        StringBuffer queryString = new StringBuffer();
        List<GoalRatingDTO> ratings = new ArrayList<GoalRatingDTO>();

        try {

            queryString.append("SELECT ff.filled_form_id, gar.goal_assessment_rating, gar.rated_on, fd.field_value  "
                    + " FROM filled_form ff "
                    + " JOIN form_data fd ON ff.filled_form_id = fd.filled_form_id "
                    + " JOIN goal_assessment_rating gar ON ff.filled_form_id = gar.filled_form_id "
                    + " WHERE ff.type = '"+ AppConstants.GOAL +"' "
                    + " AND fd.field_name = '" + AppConstants.GOAL_CATEGORY +"' "
                    + " AND ff.patient_id = " + patientId + " ORDER BY gar.rated_on DESC");

            List<Object> result = currentSession.createSQLQuery(queryString.toString()).list();

            for (Object obj: result) {
                Object[] objArray = (Object[]) obj;

                long filledFormIdDB = ObjectUtils.nullSafeLong(objArray[0]);
                int rating  = ObjectUtils.nullSafeInteger(objArray[1]);
                Date dateRated = (Date)objArray[2];
                String formattedDate = "N/A";
                if(dateRated != null){
                    formattedDate = DateUtils.getFormattedDate(dateRated.getTime(), DateUtils.DATE_FORMAT);
                }
                GoalRatingDTO ratingDTO = new GoalRatingDTO(filledFormIdDB, rating, formattedDate);
                ratings.add(ratingDTO);
            }

            return ratings;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
