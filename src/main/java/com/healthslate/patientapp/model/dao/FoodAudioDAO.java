package com.healthslate.patientapp.model.dao;

import java.util.ArrayList;
import java.util.List;

import com.healthslate.patientapp.model.entity.FoodAudio;

public class FoodAudioDAO extends BaseDAO {

	@SuppressWarnings("unchecked")
	public FoodAudio getFoodAudio(String audioName){
		
		List<FoodAudio> foodAudios = new ArrayList<FoodAudio>();
		FoodAudio foodAudio = new FoodAudio();
		try{
			foodAudios = (List<FoodAudio>) currentSession.createQuery(" FROM FoodAudio WHERE audioName ='" + audioName+"'").list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		if(foodAudios.size() > 0){
			foodAudio = foodAudios.get(0);
		}
		return foodAudio;
	}
	
	public void deleteBySummaryId(long foodLogSummaryId) {
		try {
			currentSession.createSQLQuery(" DELETE FROM food_audio WHERE food_log_summary_id = "+ foodLogSummaryId + "").executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
