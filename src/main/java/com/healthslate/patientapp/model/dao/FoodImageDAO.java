package com.healthslate.patientapp.model.dao;

import java.util.ArrayList;
import java.util.List;

import com.healthslate.patientapp.model.entity.FoodImage;

public class FoodImageDAO extends BaseDAO{

	@SuppressWarnings("unchecked")
	public FoodImage getFoodImage(String imageName){
		
		List<FoodImage> foodImages = new ArrayList<FoodImage>();
		FoodImage foodImage = new FoodImage();
		
		try{
			foodImages = (List<FoodImage>) currentSession.createQuery(" FROM FoodImage WHERE imageName ='" + imageName+"'").list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		if(foodImages.size() > 0){
			foodImage = foodImages.get(0);
		}
		return foodImage;
	}
}
