package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.FoodLogDetail;

public class FoodLogDetailDAO extends BaseDAO{

public FoodLogDetail getDetailById(long id){
		
	FoodLogDetail detail = null;
		try {
			
			detail = (FoodLogDetail) currentSession.createQuery(""
					+ " SELECT ld"
					+ "	FROM FoodLogDetail AS ld "
					+ " JOIN FETCH ld.foodMaster AS fm "
					+ " WHERE ld.foodLogDetailId = "+id).uniqueResult();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return detail;
	}

	public Float getDetailByIdAndFoodMasterId(String logId, long foodMasterId){

		Float noOfServings = null;
		try {

			noOfServings = (Float) currentSession.createSQLQuery(
					  " SELECT fld.no_of_servings "
					+ "	FROM food_log_detail fld "
					+ " JOIN food_log_summary fls ON fls.log_id = '" + logId + "' "
					+ " WHERE fld.food_log_summary_id = fls.food_log_summary_id "
					+ " AND fld.food_master_id = " + foodMasterId).uniqueResult();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return noOfServings;
	}
}
