package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.FoodLogSummary;

public class FoodLogSummaryDAO extends BaseDAO{

	public FoodLogSummary getSummaryById(long id){
		
		FoodLogSummary summary = null;
		try {
			
			summary = (FoodLogSummary) currentSession.createQuery(""
					+ " SELECT s"
					+ "	FROM FoodLogSummary s "
					+ " JOIN s.foodLogDetails d "
					+ " JOIN d.foodMaster m "
					+ " WHERE s.foodLogSummaryId = "+id).uniqueResult();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return summary;
	}
	
	public Long getFoodLogSummaryIdByLogId(String logId) {
		Long foodLogSummaryId = null;
		try {
			FoodLogSummary foodLogSummary = (FoodLogSummary) currentSession.createQuery(" FROM FoodLogSummary where log_id = '"+ logId +"'").uniqueResult();
			if(foodLogSummary != null) {
				foodLogSummaryId = foodLogSummary.getFoodLogSummaryId();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return foodLogSummaryId;
	}
}
