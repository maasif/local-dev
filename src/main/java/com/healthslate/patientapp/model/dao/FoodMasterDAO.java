package com.healthslate.patientapp.model.dao;

import java.util.ArrayList;
import java.util.List;

import com.healthslate.patientapp.model.entity.FoodMaster;

public class FoodMasterDAO extends BaseDAO{

	public FoodMaster getFoodMasterById(long foodMasterId) {
		
		FoodMaster foodMaster = (FoodMaster) currentSession.createQuery("FROM FoodMaster WHERE foodMasterId = ? ")
				.setParameter(0, foodMasterId)
				.uniqueResult();
		return foodMaster;
	}

	public FoodMaster getFoodMasterByName(String foodName) {

		FoodMaster foodMaster = (FoodMaster) currentSession.createQuery("FROM FoodMaster WHERE foodName = ? ")
				.setParameter(0, foodName)
				.uniqueResult();
		return foodMaster;
	}

	@SuppressWarnings("unchecked")
	public List<FoodMaster> getFoodMasters() {
		
		List<FoodMaster> foodMasters = new ArrayList<FoodMaster>();
		
		try{
			foodMasters = (List<FoodMaster>) currentSession.createQuery("FROM FoodMaster WHERE isCustom IS NULL OR isCustom = false GROUP BY foodName").list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return foodMasters;
	}
	
	public Long getLastInsertedRecordId(){
		Long foodMasterId = null ;
		
		try{
			foodMasterId = (Long) currentSession.createQuery("Select foodMasterId FROM FoodMaster order by foodMasterId DESC").setMaxResults(1).uniqueResult();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		return foodMasterId ;
	}
}
