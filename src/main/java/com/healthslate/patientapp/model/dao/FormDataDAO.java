package com.healthslate.patientapp.model.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;

import com.healthslate.patientapp.model.entity.FormData;

public class FormDataDAO extends BaseDAO {

	public void save (FormData formData){
		try{
			currentSession.saveOrUpdate(formData);        	
        } catch (HibernateException e) {
            e.printStackTrace();            
        }
	}
	
	public void saveformDataList(List<FormData> formDataList) {		
		try {
			for(int counter=0; counter<formDataList.size(); counter++){
				currentSession.save(formDataList.get(counter));
				if (counter % 20 == 0){
					currentSession.flush();
					currentSession.clear();
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();            
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<FormData> getFormWithFilledFormId(Long formId){
				
		StringBuilder queryString = new StringBuilder(); 		
		List<FormData> formDataList = new ArrayList<FormData>();
		
		try{
			
			queryString.append("SELECT fd.filled_form_id, fd.field_name, fd.field_value, fd.form_data_id "
					+ "FROM form_data fd "
					+ "WHERE fd.filled_form_id = "+formId);
			
			List<Object> result = currentSession.createSQLQuery(queryString.toString()).list();
						
			for(Object obj: result) {
				Object[] objArray = (Object[]) obj;
				
				FormData formData = new FormData();
				formData.setFieldName((String)objArray[1]);
				formData.setFieldValue((String)objArray[2]);
				formData.setFormDataId(((BigInteger) objArray[3]).longValue());
				
				formDataList.add(formData);
			}
		}
		catch(Exception ex){
			ex.printStackTrace();			
		}
		
		return formDataList;
	}
	
	public boolean deleteFormDataByFilledFormId(long filledFormId){		
		try {
			
			int result = currentSession.createSQLQuery(" DELETE FROM form_data "					
					+ " WHERE filled_form_id = " + filledFormId + "").executeUpdate();
			
			return (result >= 0) ? true : false;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public void deleteFormData(List<FormData> formDataList){		
		try{
			
			for(int counter=0; counter<formDataList.size(); counter++){
				currentSession.delete(formDataList.get(counter));
				if (counter % 20 == 0){
					currentSession.flush();
					currentSession.clear();
				}
			}
		}
		catch(Exception ex){
			ex.printStackTrace();			
		}
	}
}
