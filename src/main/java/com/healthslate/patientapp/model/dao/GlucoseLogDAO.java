

package com.healthslate.patientapp.model.dao;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GlucoseLogDAO extends BaseDAO{

    static final Logger LOG = LoggerFactory.getLogger(GlucoseLogDAO.class);

	public boolean removeglucoseLog(String logId) {
		Query query = currentSession.createSQLQuery(" DELETE FROM glucose_log WHERE log_id = '" + logId +"';");
		int result = query.executeUpdate();
		System.out.println("Rows affected: " + result);
		if ( result >= 0 ) {
			return true;
		} else {
			return false;
		}
	}

    public boolean removeGlucoseLogByPatientAndMeterName(long timeStamp, String meterSerialNumber, long patientId) {

        try {

            String queryString = " DELETE " +
                    " FROM " +
                    " glucose_log " +
                    " WHERE " +
                    " log_id IN ( " +
                    "  SELECT " +
                    "  log_id " +
                    "  FROM " +
                    "   log " +
                    "  WHERE " +
                    "   patient_id = " + patientId +
                    "  AND log_type = 'Glucose' " +
                    " ) " +
                    " AND meter_serial_number = '" + meterSerialNumber + "'" +
                    " AND timestamp = " + timeStamp;

            LOG.info("removeGlucoseLogByPatientAndMeterName: removing old logs, query: " + queryString);
            Query query = currentSession.createSQLQuery(queryString);
            int result = query.executeUpdate();
            LOG.info("removeGlucoseLogByPatientAndMeterName: removing old logs, result: " + result);
            if ( result >= 0 ) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
