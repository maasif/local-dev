package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.GlucoseLogType;

public class GlucoseLogTypeDAO extends BaseDAO{

	public GlucoseLogType getGlucoseLogTypeByDescription(String description) {
		
		GlucoseLogType glucoseLogType = (GlucoseLogType) currentSession.createQuery("FROM GlucoseLogType WHERE description = ? ")
				.setParameter(0, description)
				.uniqueResult();
		return glucoseLogType;
	}
}
