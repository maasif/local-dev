package com.healthslate.patientapp.model.dao;

import org.hibernate.HibernateException;

import com.healthslate.patientapp.model.entity.GoalAssessmentRating;

public class GoalAssessmentRatingDAO extends BaseDAO {

	public Long saveRating(GoalAssessmentRating goalAssessmentRating){
		try{
			currentSession.saveOrUpdate(goalAssessmentRating);        	
        } catch (HibernateException e) {
            e.printStackTrace();            
        }
		
		return goalAssessmentRating.getGoalAssessmentId();
	}
	
	public GoalAssessmentRating getRatingByFormById(long filledFormId){		
		GoalAssessmentRating goalAssessmentRating = null;
		
		try{
			goalAssessmentRating = (GoalAssessmentRating) currentSession.createQuery("FROM GoalAssessmentRating gar JOIN FETCH gar.filledForm fgar WHERE fgar.filledFormId = ? ")
        								.setParameter(0, filledFormId).uniqueResult();
        	
        } catch (HibernateException e) {
            e.printStackTrace();            
        }
		
		return goalAssessmentRating;
	}	
	
	public boolean deleteRatingByFilledFormId(long filledFormId){
		
		try {
			
			int result = currentSession.createSQLQuery(" DELETE FROM goal_assessment_rating "					
					+ " WHERE filled_form_id = " + filledFormId + "").executeUpdate();
			
			return (result >= 0) ? true : false;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
}
