package com.healthslate.patientapp.model.dao;

import java.util.List;

import com.healthslate.patientapp.model.entity.GoalCategory;

public class GoalCategoryDAO extends BaseDAO {

	@SuppressWarnings("unchecked")
	public List<GoalCategory> getAllCategories() {		
		List<GoalCategory> goalCategories = null;
		try {
			goalCategories = currentSession.createQuery(" FROM GoalCategory ").list();			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return goalCategories;
	}
	
	public String getGoalCategoryNameById(long id) {				
		try {			
			return (String) currentSession.createQuery("SELECT name FROM GoalCategory WHERE goalCategoryId = ? ")
										  .setParameter(0, id).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return null;
	}
}
