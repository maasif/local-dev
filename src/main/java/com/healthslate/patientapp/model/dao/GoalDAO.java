package com.healthslate.patientapp.model.dao;

import java.util.ArrayList;
import java.util.List;

import com.healthslate.patientapp.model.entity.Goal;

public class GoalDAO extends BaseDAO {

	@SuppressWarnings("unchecked")
	public List<Goal> getAllGoals(){
		List<Goal> goals = new ArrayList<Goal>();
		
		try {
			
			goals  = (List<Goal>)currentSession.createQuery(" FROM Goal ").list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return goals;
	}
	
	public String getGoalNameNyId(long goalId){
		String goalName = "";
		
		try {
			
			goalName  = (String)currentSession.createQuery("SELECT name FROM Goal WHERE goalId = ?").setParameter(0, goalId).uniqueResult();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return goalName;
	}
	
	public Goal getGoalByName(String name){
		
		Goal goal = null;
		
		try {
			
			goal = (Goal)currentSession.createQuery("FROM Goal WHERE name = ?").setParameter(0, name).uniqueResult();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return goal;
	}
}
