package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.ShareableMealDTO;
import com.healthslate.patientapp.model.entity.GroupPosts;
import com.healthslate.patientapp.model.entity.ShareMealPermissions;
import com.healthslate.patientapp.util.ObjectUtils;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import java.util.ArrayList;
import java.util.List;

public class GroupPostsDAO extends BaseDAO {

    public GroupPosts getGroupPostByLinkId(Long linkId) {

        GroupPosts gObject = null;
        try {
            gObject = (GroupPosts) currentSession.createQuery("FROM GroupPosts WHERE linkId = ? ")
                    .setParameter(0, linkId)
                    .uniqueResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return gObject;
    }

    public GroupPosts getGroupPostById(Long id) {

        GroupPosts gObject = null;
        try {
            gObject = (GroupPosts) currentSession.createQuery("FROM GroupPosts WHERE groupPostId = ? ")
                    .setParameter(0, id)
                    .uniqueResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return gObject;
    }

    public List<GroupPosts> getGroupPostsList(long providerId,String sSearch,int startLimit,int endLimit,String sortIndex,String sSortDirection) {

        List<GroupPosts> groupPostsDTO =new ArrayList<GroupPosts>();
        StringBuilder queryString = new StringBuilder();

        try{
            queryString.append(" SELECT "+
                    " g.group_post_id as groupPostId, "+
                    " g.description AS description, "+
                    " g.post_type AS postType, "+
                    " g.post_date AS postDate, "+
                    " g.video_link AS videoLink, "+
                    " g.video_thumbnail AS videoThumbnail, "+
                    " g.video_embed_url AS videoEmbedUrl, "+
                    " CONCAT_WS(' ', u.first_name, u.last_name) AS coachName, g.link_id AS linkId "+
                    " FROM " +
                    " group_posts g "+
                    " LEFT JOIN provider p ON p.provider_id = g.provider_id "+
                    " LEFT JOIN users u ON u.user_id = p.user_id ");

            if(!ObjectUtils.isEmpty(sSearch)){
                queryString.append(" WHERE (g.description LIKE '%"+sSearch+"%'" +
                        " OR g.post_type LIKE '%"+sSearch+"%'" +
                        " OR g.video_link LIKE '%"+sSearch+"%'" +
                        " OR CONCAT_WS(' ', u.first_name, u.last_name) LIKE '%"+sSearch+"%' )");

            }

            if(!ObjectUtils.isEmpty(sortIndex)){
                queryString.append(" ORDER BY " + sortIndex + " " + sSortDirection);
            } else {
                queryString.append(" ORDER BY g.post_date DESC");
            }

            queryString.append(" LIMIT " + startLimit + "," + endLimit + "");

            List result = currentSession.createSQLQuery(queryString.toString())
                    .addScalar("groupPostId", LongType.INSTANCE)
                    .addScalar("description", StringType.INSTANCE)
                    .addScalar("postType", StringType.INSTANCE)
                    .addScalar("postDate", LongType.INSTANCE)
                    .addScalar("videoLink", StringType.INSTANCE)
                    .addScalar("videoThumbnail", StringType.INSTANCE)
                    .addScalar("videoEmbedUrl", StringType.INSTANCE)
                    .addScalar("coachName", StringType.INSTANCE)
                    .addScalar("linkId", LongType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(GroupPosts.class))
                    .list();

            groupPostsDTO = (List<GroupPosts>) result;

        }catch(Exception ex){
            ex.printStackTrace();
        }

        return groupPostsDTO;
    }

    public long getGroupPostsListCount(long providerId,String sSearch) {
        long resultCount = 0l;

        StringBuilder queryString = new StringBuilder();
        List<Object> result = null;
        try{
            queryString.append(" SELECT "+
                    " g.group_post_id as groupPostId, "+
                    " g.description AS description, "+
                    " g.post_type AS postType, "+
                    " g.post_date AS postDate, "+
                    " g.video_link AS videoLink, "+
                    " g.video_thumbnail AS videoThumbnail, "+
                    " g.video_embed_url AS videoEmbedUrl, "+
                    " CONCAT_WS(' ', u.first_name, u.last_name) as coachName, g.link_id AS linkId "+
                    " FROM " +
                    " group_posts g "+
                    " LEFT JOIN provider p ON p.provider_id = g.provider_id "+
                    " LEFT JOIN users u ON u.user_id = p.user_id ");

            if(!ObjectUtils.isEmpty(sSearch)){
                queryString.append(" WHERE (g.description LIKE '%"+sSearch+"%'" +
                        " OR g.post_type LIKE '%"+sSearch+"%'" +
                        " OR g.video_link LIKE '%"+sSearch+"%'" +
                        " OR CONCAT_WS(' ', u.first_name, u.last_name) LIKE '%"+sSearch+"%' )");

            }

            queryString.append(" ORDER BY g.post_date DESC ");

            result = currentSession.createSQLQuery(queryString.toString()).list();
            resultCount = result.size();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return resultCount;
    }
}
