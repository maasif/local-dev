package com.healthslate.patientapp.model.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;

import com.healthslate.patientapp.model.entity.InsulinType;

public class InsulinTypeDAO extends BaseDAO{

	@SuppressWarnings("unchecked")
	public List<InsulinType> getInsulinTypes(){
		
		List<InsulinType> insulinTypes = new ArrayList<InsulinType>();
		
        try{
        	insulinTypes = currentSession.createQuery("FROM InsulinType").list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        
        return insulinTypes;
	}
}
