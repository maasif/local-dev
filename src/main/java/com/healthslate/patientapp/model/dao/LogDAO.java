package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.LogDTO;
import com.healthslate.patientapp.model.dto.RecentMealDTO;
import com.healthslate.patientapp.model.dto.TookMedsDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.sync.SyncLog;
import com.healthslate.patientapp.util.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


public class LogDAO extends BaseDAO{
	
	@SuppressWarnings("unchecked")
	public List<Log> getLogsByPatientMac(String macAddress) {
		
		List<Log> logs = new ArrayList<Log>();
		try{
			logs = currentSession.createQuery(""
							+ " SELECT DISTINCT l "
							+ " FROM Log l "
							+ " JOIN l.patient p "
							+ " WHERE p.deviceMacAddress = ? "
							+ " AND l.isRemoved = 0 ")
				.setParameter(0, macAddress)
				.list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return logs;
	}
	
	@SuppressWarnings("unchecked")
	public List<Log> getLogs() {
		Session session = HibernateUtil.getCurrentSession();
		Transaction tr = session.beginTransaction();
		List<Log> logs = new ArrayList<Log>();
		try{
			logs = session.createQuery(""
							+ " SELECT DISTINCT l "
							+ " FROM Log l "
							+ " WHERE l.isRemoved = 0 ")
				.list();
		tr.commit();
		}
		catch(Exception ex){
			tr.rollback();
			ex.printStackTrace();
		}
		return logs;
	}
	
	@SuppressWarnings("unchecked")
	public List<Log> getLogsByStatusAndDate(long patientId, Integer status, Date dateFrom, Date dateTo) {
		
		List<Log> logs = new ArrayList<Log>();
		StringBuilder queryString = new StringBuilder();
		
		try{
			
			queryString.append("SELECT DISTINCT l "
					+ " FROM Log l "
					+ " JOIN l.patient p"
					+ " WHERE l.isRemoved = 0");
			
			if(patientId != 0){
				queryString.append(" AND p.patientId ="+patientId);
			}
			
			if(status != 0){
				status = status - 1;
				queryString.append(" AND l.isProcessed ="+status);
			}
			
			
			
			if(dateFrom != null && dateTo != null){
				
				Long startTime = DateUtils.getStartOfDayTime(dateFrom).getTime();
				Long endTime = DateUtils.getEndOfDayTime(dateTo).getTime();
				
				queryString.append(" AND l.logTime >= "+startTime+" "
						+ " AND l.logTime <= "+endTime+" ");
			}
			queryString.append(" ORDER By l.logTime DESC");
			
			logs = currentSession.createQuery(queryString.toString()).list();
			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		logs = CommonUtils.getUniqueResults(logs);
		return logs;
	}
	
	@SuppressWarnings("unchecked")
	public List<LogDTO> getUnProcessedMealsList(long providerId,String sSearch,int startLimit,int endLimit,String sortIndex,String sSortDirection) {
	
		List<LogDTO> logdto =new ArrayList<LogDTO>();
		StringBuilder queryString = new StringBuilder();
        List<Object> result = null;
		try{
			queryString.append("SELECT " +
                    " p.patient_id, " +
                    " p.is_signed, " +
                    " l.log_id, " +
                    " l.log_type, " +
                    " fls.type, " +
                    " fls.carbs, " +
                    " u.display_name,  " +
                    " l.created_on, " +
                    " l.upload_time, " +
                    " l.log_time, " +
                    " l.log_time_offset, " +
                    " l.offset_key,  " +
                    " l.is_processed, " +
                    " fld.food_log_detail_id " +
                    "FROM " +
                    " log l " +
                    "LEFT JOIN patient p ON p.patient_id = l.patient_id " +
                    "LEFT JOIN food_log_summary fls ON fls.log_id = l.log_id " +
                    "LEFT JOIN users u ON u.user_id = p.user_id " +
                    "LEFT JOIN food_log_detail fld ON fld.food_log_summary_id = fls.food_log_summary_id " +
                    "WHERE " +
                    " l.is_removed = 0 " +
                    "AND l.log_type = 'Meal' " +
                    "AND l.is_processed = 0 " +
                    "AND l.is_suggested = 0 " +
                    "AND ( " +
                    " l.is_archive = 0 " +
                    " OR l.is_archive IS NULL " +
                    ") " +
                    "AND p.patient_id IN ( " +
                    " SELECT " +
                    "  pp.patients_patient_id " +
                    " FROM " +
                    "  provider_patient pp " +
                    " WHERE " +
                    "  pp.providers_provider_id = " + providerId +
                    ") ");

            if(!ObjectUtils.isEmpty(sSearch)){
                queryString.append(" AND (p.patient_id LIKE '%"+sSearch+"%'" +
                        " OR l.log_id LIKE '%"+sSearch+"%'" +
                        " OR fls.type LIKE '%"+sSearch+"%'" +
                        " OR u.display_name LIKE '%"+sSearch+"%' )");

            }

            queryString.append(" GROUP BY l.log_id ");

			if(!ObjectUtils.isEmpty(sortIndex)){
            	queryString.append(" ORDER BY " + sortIndex + " " + sSortDirection);
            } else {
            	queryString.append(" ORDER By l.upload_Time DESC");
            }
            queryString.append(" LIMIT " + startLimit + "," + endLimit + "");

            result = currentSession.createSQLQuery(queryString.toString()).list();

            if(result != null && result.size() > 0) {

                for (Object obj : result) {
                    Object[] objArray = (Object[]) obj;

                    long patientId = ObjectUtils.nullSafeLong(objArray[0]);
                    boolean isSigned = ObjectUtils.nullSafeBoolean(objArray[1]);
                    String logId = ObjectUtils.nullSafe(objArray[2]);
                    String logType = ObjectUtils.nullSafe(objArray[3]);
                    String mealType = ObjectUtils.nullSafe(objArray[4]);
                    float carbs = ObjectUtils.nullSafeFloat(objArray[5]);
                    String displayName = ObjectUtils.nullSafe(objArray[6]);
                    long createdOn = ObjectUtils.nullSafeLong(objArray[7]);
                    long uploadTime = ObjectUtils.nullSafeLong(objArray[8]);
                    long logTime = ObjectUtils.nullSafeLong(objArray[9]);
                    long logTimeOffset = ObjectUtils.nullSafeLong(objArray[10]);
                    String offsetKey = ObjectUtils.nullSafe(objArray[11]);
                    boolean isProcessed = ObjectUtils.nullSafeBoolean(objArray[12]);
                    long foodLogDetailId = ObjectUtils.nullSafeLong(objArray[13]);

                    LogDTO dto = new LogDTO();
                    dto.setLogId(logId);
                    dto.setLogTime(logTime);
                    dto.setIsProcessed(isProcessed);
                    dto.setFirstName(displayName);
                    dto.setLastName("");
                    dto.setPatientName(displayName);
                    dto.setPatientId(patientId);

                    dto.setLogTimeString(CommonUtils.timeInUserTimezone(logTime, logTimeOffset, offsetKey));

                    if(carbs <= 0 || !isProcessed) {
                        dto.setCarbs(AppConstants.NOT_APPLICABLE);
                    } else{
                        dto.setCarbs(CommonUtils.getStringFromFloat(carbs));
                    }

                    if(!ObjectUtils.isEmpty(logType) && logType.equalsIgnoreCase("Meal")){
                        dto.setMealType(mealType);
                    }else{
                        dto.setMealType(AppConstants.NOT_APPLICABLE);
                    }

                    if (foodLogDetailId > 0) {
                        dto.setStatus(AppConstants.StatusConstants.INPROCESS.getValue());
                    } else {
                        dto.setStatus(AppConstants.StatusConstants.NOTSTARTED.getValue());
                    }

                    dto.setLogType(logType);

                    Long time = uploadTime;
                    if(time == null){
                        time = logTime;
                    }

                    dto.setIsSigned(isSigned);
                    dto.setMinutesHours(DateUtils.getElapsedTime(time, System.currentTimeMillis()));
                    logdto.add(dto);
                }
            }

		}catch(Exception ex){
			ex.printStackTrace();
		}

		return logdto;
	}

	public long getUnProcessedMealsListCount(long providerId,String sSearch) {
		long resultCount = 0l;

		StringBuilder queryString = new StringBuilder();
		List<Object> result = null;
		try{
			queryString.append("SELECT " +
					" p.patient_id, " +
					" p.is_signed, " +
					" l.log_id, " +
					" l.log_type, " +
					" fls.type, " +
					" fls.carbs, " +
					" u.display_name,  " +
					" l.created_on, " +
					" l.upload_time, " +
					" l.log_time, " +
					" l.log_time_offset, " +
					" l.offset_key,  " +
					" l.is_processed, " +
					" fld.food_log_detail_id " +
					"FROM " +
					" log l " +
					"LEFT JOIN patient p ON p.patient_id = l.patient_id " +
					"LEFT JOIN food_log_summary fls ON fls.log_id = l.log_id " +
					"LEFT JOIN users u ON u.user_id = p.user_id " +
					"LEFT JOIN food_log_detail fld ON fld.food_log_summary_id = fls.food_log_summary_id " +
					"WHERE " +
					" l.is_removed = 0 " +
					"AND l.log_type = 'Meal' " +
					"AND l.is_processed = 0 " +
					"AND l.is_suggested = 0 " +
					"AND ( " +
					" l.is_archive = 0 " +
					" OR l.is_archive IS NULL " +
					") " +
					"AND p.patient_id IN ( " +
					" SELECT " +
					"  pp.patients_patient_id " +
					" FROM " +
					"  provider_patient pp " +
					" WHERE " +
					"  pp.providers_provider_id = " + providerId +
					") ");

			if(!ObjectUtils.isEmpty(sSearch)){
				queryString.append(" AND (p.patient_id LIKE '%"+sSearch+"%'" +
						" OR l.log_id LIKE '%"+sSearch+"%'" +
						" OR fls.type LIKE '%"+sSearch+"%'" +
						" OR u.display_name LIKE '%"+sSearch+"%' )");

			}

			queryString.append(" GROUP BY l.log_id ");
			queryString.append(" ORDER By l.upload_Time DESC");

			result = currentSession.createSQLQuery(queryString.toString()).list();
			resultCount = result.size();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return resultCount;
	}

	public List<Log> getAllFavoriteLogsByPatient(long patientId){
		List<Log> favoriteLogs = new ArrayList<Log>();
		try{
			favoriteLogs = (List<Log>) currentSession.createQuery(" FROM Log l WHERE l.patient.patientId = ? " +
					" AND l.isSuggested = false AND l.isRemoved = false AND l.isFavorite = true ORDER BY l.lastModified DESC ").setParameter(0,patientId).list();
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return favoriteLogs;
	}

	@SuppressWarnings({"unchecked"})
	public List<RecentMealDTO> getAllFavoriteLogsDTOsByPatient(long patientId, String type, String appPath){
		List<RecentMealDTO> favoriteMealDTOs = null;
		List<Object> objects = null;
		StringBuilder queryString = new StringBuilder();
		String filtered = " AND fls.type = '" + type + "'";

		if(type.equalsIgnoreCase(AppConstants.LOG_TYPE_MAP.get(0))){
			filtered = "";
		}

		try {
			queryString.append("SELECT l.log_id, fi.image_name, l.is_processed, l.log_time, l.log_time_offset, l.offset_key, fls.type, fls.meal_name, fls.carbs, fls.has_missing_food, fld.no_of_servings, fm.serving_size_unit, u.first_name, u.last_name "
					+ " FROM log l "
					+ " LEFT JOIN food_log_summary fls ON fls.log_id = l.log_id "
					+ " LEFT JOIN food_log_detail fld ON fld.food_log_summary_id = fls.food_log_summary_id "
					+ " LEFT JOIN food_master fm ON fm.food_master_id = fld.food_master_id "
					+ " LEFT JOIN food_image fi ON fi.food_log_summary_id = fls.food_log_summary_id "
					+ " LEFT JOIN users u ON u.user_id = l.user_id "
					+ " WHERE l.patient_id = " + patientId + ""
					+ " AND l.is_removed = 0 "
					+ " AND l.is_favorite = 1 "
					+ " AND (l.is_archive = 0 OR l.is_archive IS NULL) "
					+ " AND l.log_type = '"+AppConstants.LOG_TYPE_MEAL+"'"
					+ filtered
					+ " GROUP BY l.log_id "
					+ " ORDER BY l.last_modified DESC");

			objects = (List<Object>)currentSession.createSQLQuery(queryString.toString()).list();

			if(objects != null && objects.size() > 0){
				favoriteMealDTOs = new ArrayList<RecentMealDTO>();
				for (Object object : objects) {
					Object[] objArray = (Object[]) object;

					if(objArray != null){
						String logId = (String)objArray[0];
						String imageName = ObjectUtils.nullSafe(objArray[1]);

						Object isProcessedFromDB = objArray[2];
						boolean isDBProcessed = false;
						if(isProcessedFromDB != null){
							isDBProcessed = (Boolean)isProcessedFromDB;
						}

						Object logTimeFromDB = objArray[3];
						long logTime = 0;
						if(logTimeFromDB != null){
							logTime = ((BigInteger)logTimeFromDB).longValue();
						}

						Object logTimeOffsetFromDB = objArray[4];
						long logTimeOffset = 0;
						if(logTimeOffsetFromDB != null){
							logTimeOffset = ((BigInteger)logTimeOffsetFromDB).longValue();
						}

						String offsetKey = ObjectUtils.nullSafe(objArray[5]);
						String typeFromDB = ObjectUtils.nullSafe(objArray[6]);
						String mealNameFromDB = ObjectUtils.nullSafe(objArray[7]);

						Object carbsFromDB = objArray[8];
						float carbsFrom = 0;
						if(carbsFromDB != null){
							carbsFrom = (Float)carbsFromDB;
						}

						Object hasMissingFoodFromDB = objArray[9];
						boolean hasMissingFood = false;
						if(hasMissingFoodFromDB != null){
							hasMissingFood = (Boolean)hasMissingFoodFromDB;
						}

						String logTimeCalculated = CommonUtils.timeInUserTimezone(logTime, logTimeOffset, offsetKey);
						String imageSrc = appPath + imageName;

						String noOfServings = ObjectUtils.nullSafe(objArray[10]);
						String unit = ObjectUtils.nullSafe(objArray[11]);
						String processedBy = ObjectUtils.nullSafe(objArray[12]) + " " + ObjectUtils.nullSafe(objArray[13]);

						RecentMealDTO favoriteDTO = new RecentMealDTO(logId, imageSrc, isDBProcessed, logTimeCalculated, typeFromDB, mealNameFromDB, carbsFrom, hasMissingFood, processedBy);
						favoriteDTO.setUnit(unit);
						favoriteDTO.setNoOfServings(noOfServings);
						favoriteMealDTOs.add(favoriteDTO);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return favoriteMealDTOs;
	}

	@SuppressWarnings("unchecked")
	public List<SyncLog> getLogsByDateFilters(long patientId, Long dateFrom, Long dateTo) {
		List<SyncLog> logs = new ArrayList<SyncLog>();
		StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("SELECT DISTINCT l FROM SyncLog l WHERE (l.isRemoved = 0 OR l.isRemoved IS NULL) AND (l.isSuggested = 0 OR l.isSuggested IS NULL) AND (l.isFavorite = 0 OR l.isFavorite IS NULL) AND (l.isArchive = 0 OR l.isArchive IS NULL) ");
			if(patientId != 0){
				queryString.append(" AND l.patient.patientId = " + patientId);
			}					
							
			if(dateFrom != null && dateTo != null){
				Long startTime = DateUtils.getStartOfDayTime(dateFrom);
				Long endTime = DateUtils.getEndOfDayTime(dateTo);
				queryString.append(" AND l.createdOn >= "+startTime+" " + " AND l.createdOn <= "+endTime+" ");
			}			
			logs = currentSession.createQuery(queryString.toString()).list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return logs;
	}
		
	@SuppressWarnings("unchecked")
	public List<LogDTO> getLogsByFilters(long patientId,Boolean isAdvance,int AdvanceStatus ,int BasicStatus,int LogStatus,String sSearch,int startLimit,int endLimit,String sortIndex,String sSortDirection, Date dateFrom, Date dateTo) {
	
		List<Log> logs = new ArrayList<Log>();
		List<LogDTO> logdto =new ArrayList<LogDTO>();
		StringBuilder queryString = new StringBuilder();
		
		try{
			queryString.append("SELECT DISTINCT l "
					+ " FROM Log l"
					+ " JOIN l.patient p"
					+ " LEFT JOIN l.miscLog m "
					+ " LEFT JOIN l.foodLogSummary fs "
					+ " LEFT JOIN l.exerciseLogs el "	
					+ " LEFT JOIN l.glucoseLog gl "
					+ " LEFT JOIN l.activityLog al "
					+ " WHERE l.isRemoved = 0");
			
			if(patientId != 0){
				queryString.append(" AND l.patient.patientId ="+patientId);
			}
			
			if(BasicStatus == 1){
				queryString.append(" AND l.isProcessed = 0");
				
			}
			else if(BasicStatus == 2){
				queryString.append(" AND l.isProcessed = 1");
				
			}
			else if(BasicStatus == 3){
				queryString.append(" AND l.isArchive = 1");
			}

            queryString.append(" AND l.isSuggested = 0");

			if(dateFrom != null && dateTo != null){
				
				Long startTime = DateUtils.getStartOfDayTime(dateFrom).getTime();
				Long endTime = DateUtils.getEndOfDayTime(dateTo).getTime();
				
				queryString.append(" AND l.logTime >= "+startTime+" "
						+ " AND l.logTime <= "+endTime+" ");
			}

			if(LogStatus == 1){
				queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_MEAL+"' "); 
			}
			else if(LogStatus == 2){
				queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_ACTIVITY+"' ");
			}
			else if(LogStatus == 3){
				queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_GLUCOSE+"' ");
			}
			else if(LogStatus == 4){
				queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_MEDICATION+"' ");
			}
			else if(LogStatus == 5){
				queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_WEIGHT+"' ");
			}
			else if(LogStatus == 6){
				queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_MISC+"' ");
			} else if(LogStatus == 7){
				queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_ACTIVITY_MINUTE+"' ");
			}
			
			if(BasicStatus == 0 || BasicStatus != 3){
				queryString.append(" AND (l.isArchive IS NULL OR l.isArchive = 0)");
			}
			if(!sSearch.isEmpty()){
				int isProcesed =-1 ;
				
				if(sSearch.equals(AppConstants.YES)){
					isProcesed=1;
        		}
        		else if(sSearch.equals(AppConstants.NO)){
        			isProcesed=0;
        		}
				queryString.append("AND  (l.logId like '%"+sSearch+"%' "+
										"OR m.bloodPressureSystolic like '%"+sSearch+"%'  "+
										"OR m.bloodPressureDiastolic like '%"+sSearch+"%'  "+
										"OR fs.carbs like '%"+sSearch+"%'  "+
										"OR fs.type like '%"+sSearch+"%'  "+
										"OR el.type like '%"+sSearch+"%'  "+
										"OR m.weight like '%"+sSearch+"%'  "+
										"OR l.isProcessed = "+isProcesed+"  )");
			}
			
			if(!sortIndex.isEmpty()){
            	queryString.append("  ORDER BY " + sortIndex+ " "+sSortDirection);
            } else {
            	queryString.append(" ORDER By l.logTime DESC");
            }
			
			logs = currentSession.createQuery(queryString.toString())
					.setFirstResult(startLimit).setMaxResults(endLimit)
					.list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		logs = CommonUtils.getUniqueResults(logs);
		int unreadMessageCount = 0;
		
		
		
		
		for(Log log : logs){
			
			LogDTO dto = new LogDTO();
			dto.setLogId(log.getLogId());
			dto.setLogTime(log.getLogTime() ); //changed here
			dto.setIsProcessed(log.getIsProcessed());
			
			dto.setLogTimeString(CommonUtils.timeInUserTimezone(log.getLogTime(), log.getLogTimeOffset(), log.getOffsetKey()));
			
			if(log.getFoodLogSummary() != null){
				if(log.getFoodLogSummary().getCarbs()<=0 || !log.getIsProcessed())
				{
					
					dto.setCarbs(AppConstants.NOT_APPLICABLE);
				}
				else{
					//dto.setCarbs(Integer.toString(log.getFoodLogSummary().getCarbs().intValue()));
					dto.setCarbs(CommonUtils.getStringFromFloat(log.getFoodLogSummary().getCarbs()));
				}
			}
			if(log.getMiscLog()!=null){
				dto.setBloodPressureSystolic(log.getMiscLog().getBloodPressureSystolic());
				dto.setBloodPressureDiastolic(log.getMiscLog().getBloodPressureDiastolic());
				if(log.getMiscLog().getStepsPerDay() != null) {
					dto.setStepsPerDay(log.getMiscLog().getStepsPerDay().toString());
				}
				if(log.getMiscLog().getTookMeds() != null) {
					boolean isTookMeds = log.getMiscLog().getTookMeds();
					if(isTookMeds) {
						dto.setTookMeds("Yes");	
					} else {
						dto.setTookMeds("No");
					}	
				}
				if(log.getMiscLog().getWeight()<=0)
				{
					dto.setWeight(AppConstants.NOT_APPLICABLE);
				}
				else
				{
					dto.setWeight(CommonUtils.getStringFromFloat(log.getMiscLog().getWeight()));
				}
				if(dto.getBloodPressureDiastolic()<=0 ||dto.getBloodPressureSystolic()<=0){
					dto.setBp("N/A");
				}
				else{
				dto.setBp(dto.getBloodPressureSystolic()+"/"+dto.getBloodPressureDiastolic());
				}
			
				
			}
			
			if(log.getExerciseLogs().size() > 0)
			{							
				for(ExerciseLog exlog :log.getExerciseLogs())
				{
					if(exlog.getType().equals(AppConstants.NEWSFEED.CARDIO.getValue())){
						
						dto.setCardio(CommonUtils.getStringFromFloat(exlog.getMinutesPerformed()));
					
					}
					else if (exlog.getType().equals(AppConstants.NEWSFEED.STRENGTH.getValue())){
						dto.setStrength(CommonUtils.getStringFromFloat(exlog.getMinutesPerformed()));
					}
				}
			}
			
			if(log.getFoodLogSummary()!=null)
			{
				if(log.getFoodLogSummary().getType() != null){
					dto.setMealType(log.getFoodLogSummary().getType());
				}
				else{
					dto.setMealType(AppConstants.NOT_APPLICABLE);
				}
			}
			if(log.getFoodLogSummary() != null && log.getFoodLogSummary().getMessages() != null){
				
				for(Message m: log.getFoodLogSummary().getMessages()){
					
					if(m.getReadStatus() == null || m.getReadStatus() == false){
						unreadMessageCount += 1;
					}
				}
			}
			/*Temporary Stub*/
			
			dto.setUnreadMessageCount(unreadMessageCount);
			
			//setting processed time
			if(log.getProcessedTime() != null){		
				
				Long time = log.getUploadTime();
				if(time == null){
					time = log.getLogTime();
				}				
				dto.setMinutesHours(DateUtils.getElapsedTime(time, log.getProcessedTime()));
			}else{
				dto.setMinutesHours(AppConstants.NOT_APPLICABLE);
			}
			
			//setting coach name
			User user = new UserDAO().getUserById(log.getUserId());
			if(user != null){
				dto.setCoachName(user.getLastName()+ ", " + user.getFirstName());				
			}else{
				dto.setCoachName(AppConstants.NOT_APPLICABLE);
			}
			
			if(log.getGlucoseLog() != null) {
				dto.setGlucoseLevel(log.getGlucoseLog().getGlucoseLevel());
			}
			dto.setLogType(log.getLogType());

			Patient patient = log.getPatient();
			if(patient != null){
				if(patient.getIsSigned() != null && patient.getIsSigned()){
					dto.setIsSigned(true);
				}
			}

            ActivityLog activityLog = log.getActivityLog();
            if(activityLog != null){
                dto.setAlIntensity(activityLog.getIntensity());
                dto.setAlType(activityLog.getType());
                dto.setAlMinutesPerformed(activityLog.getMinutesPerformed());
                dto.setAlNotes(activityLog.getNotes());
            }

			logdto.add(dto);
		}

		return logdto;
	}
	
	public Long getLogsCountByStatusAndDate(long patientId,Boolean isAdvance,int AdvanceStatus ,int BasicStatus,int LogStatus,String sSearch, Date dateFrom, Date dateTo) {
		
		long count = 0l;
		StringBuilder queryString = new StringBuilder();
		try{
			
			queryString.append("SELECT COUNT(DISTINCT l) "
					+ " FROM Log l"
					+ " JOIN l.patient p"
					+ " LEFT JOIN l.miscLog m "
					+ " LEFT JOIN l.foodLogSummary fs "
					+ " LEFT JOIN l.exerciseLogs el "
					+ " LEFT JOIN l.activityLog al "
					+ " WHERE l.isRemoved = 0");
			
			if(patientId != 0){
				queryString.append(" AND l.patient.patientId ="+patientId);
			}
			
			if(BasicStatus == 1){
				queryString.append(" AND l.isProcessed = 0");
			}
			else if(BasicStatus == 2){
				queryString.append(" AND l.isProcessed = 1");
			}
			else if(BasicStatus == 3){
				queryString.append(" AND l.isArchive = 1");
			}

            queryString.append(" AND l.isSuggested = 0");

			if(dateFrom != null && dateTo != null){
				
				Long startTime = DateUtils.getStartOfDayTime(dateFrom).getTime();
				Long endTime = DateUtils.getEndOfDayTime(dateTo).getTime();
				
				queryString.append(" AND l.logTime >= "+startTime+" "
						+ " AND l.logTime <= "+endTime+" ");
			}
			
			if(LogStatus == 1){
				queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_MEAL+"' "); 
			}
			else if(LogStatus == 2){
				queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_ACTIVITY+"' ");
			}
			else if(LogStatus == 3){
				queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_GLUCOSE+"' ");
			}
			else if(LogStatus == 4){
				queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_MEDICATION+"' ");
			}
			else if(LogStatus == 5){
				queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_WEIGHT+"' ");
			}
			else if(LogStatus == 6){
				queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_MISC+"' ");
			} else if(LogStatus == 7){
				queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_ACTIVITY_MINUTE+"' ");
			}
			
			if(BasicStatus == 0 || BasicStatus != 3){
				queryString.append(" AND (l.isArchive IS NULL OR l.isArchive = 0)");
			}
			
			if(!sSearch.isEmpty()){
				int isProcesed =-1 ;
				
				if(sSearch.equals(AppConstants.YES)){
					isProcesed=1;
        		}
        		else if(sSearch.equals(AppConstants.NO)){
        			isProcesed=0;
        		}
				queryString.append("AND  (l.logId like '%"+sSearch+"%' "+
										"OR m.bloodPressureSystolic like '%"+sSearch+"%'  "+
										"OR m.bloodPressureDiastolic like '%"+sSearch+"%'  "+
										"OR fs.carbs like '%"+sSearch+"%'  "+
										"OR fs.type like '%"+sSearch+"%'  "+
										"OR el.type like '%"+sSearch+"%'  "+
										"OR m.weight like '%"+sSearch+"%'  "+
										"OR l.isProcessed = "+isProcesed+"  )");
			}
			
			count = (Long)currentSession.createQuery(queryString.toString()).uniqueResult();
			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		return count;
	}
	
	public Log getLogById(String logId) {
		
		
		Log log = null;
		try{
			log = (Log) currentSession.createQuery("Select DISTINCT l FROM Log l WHERE l.logId = ? ")
				.setParameter(0, logId)
				.uniqueResult();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return log;
	}
	
	@SuppressWarnings("unchecked")
	public List<Log> getLogByPatientId(Long id) {
		
		List<Log> logs = new ArrayList<Log>();
		try{
			logs = (List<Log>) currentSession.createQuery("Select DISTINCT l FROM Log l WHERE l.patient.patientId = ? AND l.isRemoved = 0 ")
				.setParameter(0, id)
				.list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return logs;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getUnprocessedLogsByPatientIds(List<Long> ids) {

		List<Object> countObjs = new ArrayList<Object>();
		
		try{
			countObjs = currentSession.createQuery(" SELECT l.patient.patientId, COUNT(l.logId) "
					+ " FROM Log l "
					+ " WHERE l.isRemoved = 0 "
					+ " AND l.isProcessed = 0 "
					+ " AND l.logType = '"+ AppConstants.LOG_TYPE_MEAL+"'"
					+ " AND l.patient.patientId IN(:ids) "
					+ " GROUP BY l.patient.patientId").setParameterList("ids", ids).list();
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return countObjs;
	}
	@SuppressWarnings("unchecked")
	public List<Object> getMealNameOfProcessedLog(List<Long> ids) {

		List<Object> countObjs = new ArrayList<Object>();
		
		try{
			countObjs = currentSession.createQuery(" SELECT l.patient.patientId , l.logId, lm.log.logId , lm.foodLogSummaryId , lm.mealName, fld.numberOfServings"
					+ " FROM Log l "
					+ " JOIN l.foodLogSummary lm"
					+ " JOIN lm.foodLogDetails fld"
				/*	+ " ON l.Log.logId=lm.Log.LogId"*/

					+ " WHERE l.isProcessed = 1 "
					+ " AND l.patient.patientId IN(:ids) GROUP BY lm.mealName").setParameterList("ids", ids).list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return countObjs;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getMealNameOfProcessedMealLogs(long providerId) {

		List<Object> countObjs = new ArrayList<Object>();

		try{
			countObjs = currentSession.createSQLQuery("SELECT " +
					" l.patient_id, " +
					" l.log_id as pLogId, " +
					" fls.log_id as fLogId, " +
					" fls.food_log_summary_id, " +
					" fls.meal_name, " +
					" fld.no_of_servings " +
					" FROM " +
					" log l " +
					" LEFT JOIN food_log_summary fls ON fls.log_id = l.log_id " +
					" LEFT JOIN food_log_detail fld ON fld.food_log_summary_id = fls.food_log_summary_id " +
					" WHERE" +
					" l.is_processed = 1" +
					" AND l.patient_id IN " +
					" (" +
					" SELECT pat.patient_id " +
					"  FROM patient pat " +
					" LEFT JOIN provider_patient pp ON pp.patients_patient_id = pat.patient_id\n" +
					"  WHERE pp.providers_provider_id = " + providerId+ ""+
					" ) " +
					" GROUP BY " +
					" fls.meal_name").list();

		} catch(Exception ex){
			ex.printStackTrace();
		}
		return countObjs;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getUnreadMessageCountByPatientIds(long providerId, List<Long> ids) {

		List<Object> countObjs = new ArrayList<Object>();
		
		try{
			countObjs = currentSession.createQuery("SELECT m.patient.patientId, COUNT(m.messageId) "
					+ " FROM Message m "
					+ " WHERE m.readStatus = false "
					+ " AND m.patient.patientId IN(:ids) "
                    + " AND m.user.userId = " + providerId
                    + " AND m.owner = '" + AppConstants.PATIENT + "'"
					+ " GROUP BY m.patient.patientId").setParameterList("ids", ids).list();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return countObjs;
	}

	public List<Object> getProvidersNameForUnreadMessages(List<Long> ids) {
		String idsString = "118, ";
		for(long id: ids) {
			idsString += id + ", ";
		}
		idsString = idsString.substring(0, idsString.length()-2);
		List<Object> objects = new ArrayList<Object>();
		try{
			String query = "SELECT us.first_name AS providerName, " +
					" us.user_id, " +
					" m.patient_id, " +
					" COUNT(m.message_id) As count " +
					" FROM " +
					" message m " +
					" JOIN patient p ON p.patient_id = m.patient_id " +
					" LEFT JOIN users us ON us.user_id = m.user_id " +
					" WHERE m.owner = 'PATIENT' " +
					" AND m.patient_id IN ("+idsString+") " +
					" AND m.read_status = 0 " +
					" GROUP BY m.patient_id, m.user_id ";
			objects = currentSession.createSQLQuery(query).list();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return objects;
	}

    @SuppressWarnings("unchecked")
    public List<Object> getUnreadMessageCountByPatientIds(List<Long> ids) {

        List<Object> countObjs = new ArrayList<Object>();

        try{
            countObjs = currentSession.createQuery("SELECT m.patient.patientId, COUNT(m.messageId) "
                    + " FROM Message m "
                    + " WHERE m.readStatus = false "
                    + " AND m.patient.patientId IN(:ids) "
                    + " GROUP BY m.patient.patientId").setParameterList("ids", ids).list();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return countObjs;
    }

    public long getUnreadMessageOfCoach(long patientId, long userId) {
        Object msgCount = null;
        long intCount = 0;

        try{
			String query = "SELECT COUNT(m.message_id) AS unreadCount FROM message m " +
					" WHERE m.`owner` = 'PATIENT' " +
					" AND m.patient_id = " + patientId +
                    " AND m.user_id = " + userId +
					" AND m.read_status = 0" +
					" GROUP BY m.patient_id";
            msgCount = currentSession.createSQLQuery(query).uniqueResult();

            if(!ObjectUtils.isEmpty(ObjectUtils.nullSafe(msgCount))){
                intCount = Long.parseLong(msgCount+"");
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return intCount;
    }

	public int getLogsCount(long patientId) {
		
		StringBuilder queryString = new StringBuilder();
		int count = 0;
		
		try{
			queryString.append("SELECT COUNT(DISTINCT l.logId) "
					+ " FROM Log l "
					+ " WHERE l.isRemoved = 0 ");
			
			if(patientId != 0){
				queryString.append(" AND l.patient.patientId ="+patientId);
			}
			
			count = ((Long) currentSession.createQuery(queryString.toString()).uniqueResult()).intValue();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return count;
	}
	
	@SuppressWarnings("unchecked")
	public int getLogsCountofLastFourHours(Long providerId) {
		
		List<Integer> count = new LinkedList<Integer>();
		
		try{
			
			Query query = currentSession.createSQLQuery(" SELECT COUNT(*) "
														+ " FROM log l "
														+ " WHERE l.patient_id IN ( SELECT patients_patient_id from provider_patient where providers_provider_id = "+ providerId + ")"
														+ " AND l.log_type = 'Meal' "
														+ " AND l.is_processed = 0 "
                                                        + " AND l.is_suggested = 0 "
														+ " AND l.is_removed = 0 "
                                                        + " AND (l.is_archive = 0 OR l.is_archive IS NULL) ");
			
			count = query.list();		
		}
		catch(Exception ex){
			ex.printStackTrace();
		}		
		return  Integer.parseInt(count.get(0)+"");	
	}
	
	/*public Object getLastProcessedLogDateByPatientId(long patientId) {
		Session session = HibernateUtil.getCurrentSession();
		Transaction tr = session.beginTransaction();
		Object obj = null;
		try{
			obj = session.createQuery(""
							+ " SELECT MAX(l.processDate) "
							+ " FROM Log l "
							+ " JOIN l.patient p "
							+ " WHERE p.patientId = ? "
							+ " AND l.isProcessed = 1 ")
				.setParameter(0, patientId)
				.uniqueResult();
		tr.commit();
		}
		catch(Exception ex){
			tr.rollback();
			ex.printStackTrace();
		}
		return obj;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<Object> getNewMessages(String logId) {
		
		List<Object> messages = new ArrayList<Object>();
				
		try{
			
			//first update all messages readstatus to '1'
			currentSession.createSQLQuery("UPDATE message m "
					+ " JOIN food_log_summary fls on m.food_log_summary_id = fls.food_log_summary_id"
					+ " JOIN log l ON fls.log_id = l.log_id"
					+ " SET m.read_status = TRUE"
					+ " WHERE l.log_id = '"+logId+"' ");

			messages = (List<Object>)currentSession.createQuery(""
							+ " SELECT m.owner, m.message, m.timestamp, m.messageId "
							+ " FROM Log l "
							+ " JOIN l.foodLogSummary fls "
							+ " JOIN fls.messages m "
							+ " WHERE l.logId = '"+logId+"' "																				
							+ " ORDER BY m.timestamp ").list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return messages;
	}

    public Object getGlucoseSummaryBeforeMealByPatientId(long patientId, Long startDate, Long endDate) {
        Object result = null;
        Target target = new TargetDAO().getTargetByPatientId(patientId);
        float beforeGlucoseMin = 0;
        float beforeGlucoseMax = 0;
        if(target != null) {
            beforeGlucoseMin = target.getGlucoseMin2();  // Before Meal
            beforeGlucoseMax = target.getGlucoseMax2(); // Before Meal
        }
        try{

            String query = " SELECT " +
                    " (innr2.totalGLs) / innr2.total AS avg," +
                    " (" +
                    " innr2.under * 100 / innr2.total" +
                    " ) AS undr," +
                    " (" +
                    " innr2.intarget * 100 / innr2.total" +
                    " ) AS inTar," +
                    " (innr2.over * 100 / innr2.total) AS over," +
                    " (innr2.total) AS total" +
                    " FROM" +
                    " log l" +
                    " JOIN (" +
                    " SELECT" +
                    " lo.log_id," +
                    " SUM(innr.glevel) AS totalGLs," +
                    " COUNT(lo.log_id) AS total," +
                    " SUM(innr.underTarget) AS under," +
                    " SUM(innr.InTarget) AS intarget," +
                    " SUM(innr.overTarget) over" +
                    " FROM" +
                    " log lo" +
                    " JOIN (" +
                    " SELECT" +
                    " l.log_id," +
                    " gl.glucose_level AS glevel," +
                    "" +
                    " IF (" +
                    " gl.glucose_time = '"+ AppConstants.GLUCOSE_BEFORE_MEAL + "' AND gl.glucose_level < " + beforeGlucoseMin + "," +
                    " 1," +
                    " 0" +
                    ") AS underTarget," +
                    "" +
                    " IF (" +
                    " gl.glucose_time = '" + AppConstants.GLUCOSE_BEFORE_MEAL + "'" +
                    " AND gl.glucose_level >= " + beforeGlucoseMin +
                    " AND gl.glucose_level <= " + beforeGlucoseMax + "," +
                    " 1," +
                    " 0" +
                    ") AS InTarget," +
                    "" +
                    " IF (" +
                    " gl.glucose_time = '" + AppConstants.GLUCOSE_BEFORE_MEAL + "'" +
                    " AND gl.glucose_level > "+beforeGlucoseMax+"," +
                    "1," +
                    "0" +
                    ") AS overTarget" +
                    " FROM" +
                    " log l" +
                    " JOIN glucose_log gl ON l.log_id = gl.log_id" +
                    " WHERE" +
                    " l.log_time >= "+ startDate +
                    " AND l.log_time <= "+ endDate +
                    " AND l.log_type = '" + AppConstants.LOG_TYPE_GLUCOSE + "'" +
                    " AND gl.glucose_time = '" + AppConstants.GLUCOSE_BEFORE_MEAL + "'" +
                    " AND l.patient_id = " + patientId +
                    " AND l.is_removed = 0" +
                    ") AS innr ON lo.log_id = innr.log_id" +
                    ") AS innr2 ON l.log_id = innr2.log_id";

            result = (Object)currentSession.createSQLQuery(query).uniqueResult();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

    public Object getGlucoseSummaryAfterMealByPatientId(long patientId, Long startDate, Long endDate) {
        Object result = null;
        Target target = new TargetDAO().getTargetByPatientId(patientId);
        float afterGlucoseMin = 0;
        float afterGlucoseMax = 0;
        if(target != null) {
            afterGlucoseMin = target.getGlucoseMin();  // After Meal
            afterGlucoseMax = target.getGlucoseMax(); // After Meal
        }
        try{

            String query = " SELECT " +
                    " (innr2.totalGLs) / innr2.total AS avg," +
                    " (" +
                    " innr2.under * 100 / innr2.total" +
                    " ) AS undr," +
                    " (" +
                    " innr2.intarget * 100 / innr2.total" +
                    " ) AS inTar," +
                    " (innr2.over * 100 / innr2.total) AS over," +
                    " (innr2.total) AS total" +
                    " FROM" +
                    " log l" +
                    " JOIN (" +
                    " SELECT" +
                    " lo.log_id," +
                    " SUM(innr.glevel) AS totalGLs," +
                    " COUNT(lo.log_id) AS total," +
                    " SUM(innr.underTarget) AS under," +
                    " SUM(innr.InTarget) AS intarget," +
                    " SUM(innr.overTarget) over" +
                    " FROM" +
                    " log lo" +
                    " JOIN (" +
                    " SELECT" +
                    " l.log_id," +
                    " gl.glucose_level AS glevel," +
                    " IF (" +
                    " gl.glucose_time = '" + AppConstants.GLUCOSE_AFTER_MEAL + "' AND gl.glucose_level < " + afterGlucoseMin + "," +
                    " 1," +
                    " 0" +
                    " ) AS underTarget," +
                    "" +
                    " IF (" +
                    " gl.glucose_time = '" + AppConstants.GLUCOSE_AFTER_MEAL + "'" +
                    " AND gl.glucose_level >= " + afterGlucoseMin +
                    " AND gl.glucose_level <= " + afterGlucoseMax + "," +
                    " 1," +
                    " 0" +
                    " ) AS InTarget," +
                    "" +
                    " IF (" +
                    " gl.glucose_time = '" + AppConstants.GLUCOSE_AFTER_MEAL + "'" +
                    " AND gl.glucose_level > "+afterGlucoseMax+"," +
                    " 1," +
                    " 0" +
                    " ) AS overTarget" +
                    " FROM" +
                    " log l" +
                    " JOIN glucose_log gl ON l.log_id = gl.log_id" +
                    " WHERE" +
                    " l.log_time >= "+ startDate +
                    " AND l.log_time <= "+ endDate +
                    " AND l.log_type = '" + AppConstants.LOG_TYPE_GLUCOSE + "'" +
                    " AND gl.glucose_time = '" + AppConstants.GLUCOSE_AFTER_MEAL + "'" +
                    " AND l.patient_id = " + patientId +
                    " AND l.is_removed = 0" +
                    " ) AS innr ON lo.log_id = innr.log_id" +
                    " ) AS innr2 ON l.log_id = innr2.log_id";

            result = (Object)currentSession.createSQLQuery(query).uniqueResult();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

	public Object getGlucoseSummaryByPatientId(long patientId, Long startDate, Long endDate) {
		Object result = null;
		Target target = new TargetDAO().getTargetByPatientId(patientId);
		float beforeGlucoseMin = 0;
		float beforeGlucoseMax = 0;
		float afterGlucoseMin = 0;
		float afterGlucoseMax = 0;
		if(target != null) {
			beforeGlucoseMin = target.getGlucoseMin2();  // Before Meal
			beforeGlucoseMax = target.getGlucoseMax2(); // Before Meal
			afterGlucoseMin = target.getGlucoseMin();  // After Meal
			afterGlucoseMax = target.getGlucoseMax(); // After Meal
		}
		try{

			String query = "SELECT " +
					" (innr2.totalGLs) / innr2.total AS avg, " +
					" (innr2.under * 100 / innr2.total) AS undr, " +
					" (innr2.intarget * 100 / innr2.total) AS inTar, " +
					" (innr2.over * 100 / innr2.total) AS over, " +
					" (innr2.total) As total " +
					" FROM log l" +
					" JOIN (" +
					" SELECT lo.log_id, SUM(innr.glevel) AS totalGLs, COUNT(lo.log_id) AS total, " +
					" SUM(innr.underTarget) AS under, " +
					" SUM(innr.InTarget) AS intarget, " +
					" SUM(innr.overTarget) over " +
					" FROM log lo JOIN ( " +
					" SELECT l.log_id, gl.glucose_level AS glevel, " +
					" IF (gl.glucose_time = '"+ AppConstants.GLUCOSE_BEFORE_MEAL + "' AND gl.glucose_level < "+ beforeGlucoseMin +" OR gl.glucose_time = '"+ AppConstants.GLUCOSE_AFTER_MEAL +"' AND gl.glucose_level < "+ afterGlucoseMin +", 1, 0) AS underTarget , " +
					" IF (gl.glucose_time = '"+ AppConstants.GLUCOSE_BEFORE_MEAL + "' AND gl.glucose_level >= "+ beforeGlucoseMin +" AND gl.glucose_level <= "+ beforeGlucoseMax +" OR gl.glucose_time = '"+ AppConstants.GLUCOSE_AFTER_MEAL +"' AND gl.glucose_level >=" + afterGlucoseMin +" AND gl.glucose_level <= "+ afterGlucoseMax +", 1, 0 ) AS InTarget , "+
					" IF (gl.glucose_time = '"+ AppConstants.GLUCOSE_BEFORE_MEAL + "' AND gl.glucose_level > "+ beforeGlucoseMax +" OR gl.glucose_time = '"+ AppConstants.GLUCOSE_AFTER_MEAL +"' AND gl.glucose_level > "+ afterGlucoseMax +", 1, 0) AS overTarget " +
					" FROM log l " +
					" JOIN glucose_log gl ON l.log_id = gl.log_id" +
					" WHERE l.log_time >= " + startDate + " AND l.log_time <= "+endDate+"" +
					" AND l.log_type = '"+AppConstants.LOG_TYPE_GLUCOSE+"'" +
					" AND l.patient_id = "+patientId+"" +
					" AND l.is_removed = 0" +
					" ) AS innr ON lo.log_id = innr.log_id )" +
					"AS innr2 ON l.log_id = innr2.log_id";

			result = (Object)currentSession.createSQLQuery(query).uniqueResult();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}
	
	public Object getPercentageOfMealsByPatientId(long patientId, Long startDate, Long endDate) {
		Object result = null;
		Target target = new TargetDAO().getTargetByPatientId(patientId);
		float [] targets = CommonUtils.getMealTargets(target);
		try{
			result = (Object)currentSession.createSQLQuery(""
					+ "SELECT "
					+ " ( SUM(innr.tsnacks) + SUM(innr.tbreakfast) + SUM(innr.tlunch) + SUM(innr.tdinner)) AS total, "
					+ " ( SUM(innr.snacks) + SUM(innr.breakfast) + SUM(innr.lunch) + SUM(innr.dinner)) AS sum, "
					+ " ( SUM(innr.snacks) + SUM(innr.breakfast) + SUM(innr.lunch) + SUM(innr.dinner)) * 100 /"
					+ " (SUM(innr.tsnacks) + SUM(innr.tbreakfast) + SUM(innr.tlunch) + SUM(innr.tdinner)) AS percentage "

					+ " FROM log lo JOIN ( "
					+ " 	SELECT l.log_id, "
					+ " 	IF ( fls.type = '"+AppConstants.TYPEKEYS.BREAKFAST.getValue()+"' AND "+ targets[0] +"> 0 AND fls.carbs > 0 AND fls.carbs < "+ targets[0]+", 1, 0 ) AS breakfast, "
					+ " 	IF ( fls.type = '"+AppConstants.TYPEKEYS.LUNCH.getValue()+"' AND "+ targets[1] +"> 0 AND fls.carbs > 0 AND fls.carbs < "+ targets[1]+", 1, 0 ) AS lunch, "
					+ "		IF ( fls.type = '"+AppConstants.TYPEKEYS.SNACK.getValue()+"' AND "+ targets[2] +"> 0 AND fls.carbs > 0 AND fls.carbs < "+ targets[2] +", 1, 0 ) AS snacks, "
					+ " 	IF ( fls.type = '"+AppConstants.TYPEKEYS.DINNER.getValue()+"' AND "+ targets[3] +"> 0 AND fls.carbs > 0 AND fls.carbs < "+ targets[3]+", 1, 0 ) AS dinner, "
					
					+ " 	IF ( fls.type = '"+AppConstants.TYPEKEYS.BREAKFAST.getValue()+"' AND "+ targets[0] +"> 0 , 1, 0 ) AS tbreakfast, "
					+ " 	IF ( fls.type = '"+AppConstants.TYPEKEYS.LUNCH.getValue()+"' AND "+ targets[1] +"> 0 , 1, 0 ) AS tlunch, "
					+ "		IF ( fls.type = '"+AppConstants.TYPEKEYS.SNACK.getValue()+"' AND "+ targets[2] +"> 0 , 1, 0 ) AS tsnacks, "
					+ " 	IF ( fls.type = '"+AppConstants.TYPEKEYS.DINNER.getValue()+"' AND "+ targets[3] +"> 0 , 1, 0 ) AS tdinner "
					
					+ " 	FROM log l "
					+ " 	JOIN food_log_summary fls ON l.log_id = fls.log_id "
					+ " 	WHERE l.log_time >= "+startDate+" "
					+ " 	AND l.log_time <= "+endDate+" "
					+ " 	AND l.log_type = '"+AppConstants.LOG_TYPE_MEAL+"' "
					+ "		AND l.patient_id = "+patientId+" "
					+ "		AND l.is_removed = 0 "
					+ "		AND l.is_processed = 1 "
                    + "		AND l.is_suggested = 0 "
					+ " ) AS innr ON lo.log_id = innr.log_id")
					.uniqueResult();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	public Object getAverageOfMealsByPatientId(long patientId, Long startDate, Long endDate) {
		Object result = null;
		try{
			String query = ""
					+ "SELECT COUNT(lo.log_id) AS total, "
					+ " ( SUM(innr.snacks) + SUM(innr.breakfast) + SUM(innr.lunch) + SUM(innr.dinner))/" +
					" (SUM(innr.tsnacks) + SUM(innr.tbreakfast) + SUM(innr.tlunch) + SUM(innr.tdinner)) AS average "

					+ "FROM log lo JOIN ( "
					+ "SELECT l.log_id, "
					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.SNACK.getValue()+"', fls.carbs, 0 ) AS snacks, "
					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.BREAKFAST.getValue()+"', fls.carbs, 0 ) AS breakfast, "
					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.LUNCH.getValue()+"', fls.carbs, 0 )  AS lunch, "
					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.DINNER.getValue()+"' , fls.carbs, 0 ) AS dinner, "

					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.SNACK.getValue()+"', 1, 0 ) AS tsnacks, "
					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.BREAKFAST.getValue()+"', 1, 0 ) AS tbreakfast, "
					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.LUNCH.getValue()+"', 1, 0 )  AS tlunch, "
					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.DINNER.getValue()+"' , 1, 0 ) AS tdinner "

					+ "FROM log l "
					+ "JOIN food_log_summary fls ON l.log_id = fls.log_id "
					+ "WHERE l.log_time >= "+startDate+ " "
					+ "AND l.log_time <= "+endDate+ " "
					+ "AND l.log_type = '"+AppConstants.LOG_TYPE_MEAL+"' "
					+ "AND l.patient_id ="+patientId + " "
					+ "AND l.is_removed = 0 "
					+ "AND l.is_processed = 1 "
					+ "AND l.is_suggested = 0 "
					+ ") AS innr ON lo.log_id = innr.log_id ";
			result = (Object)currentSession.createSQLQuery(query)
					.uniqueResult();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	public Object getAverageOfEachMealSummaryChart(long patientId, Long startDate, Long endDate) {
		Object result = null;
		try{
			String query = ""
					+ "SELECT COUNT(lo.log_id) AS total, "
					+ "SUM(innr.snacks)/SUM(innr.tsnacks) As snackAverage, "
					+ "SUM(innr.breakfast)/SUM(innr.tbreakfast) As breakfastAverage, "
					+ "SUM(innr.lunch)/SUM(innr.tlunch) As lunchAverage, "
					+ "SUM(innr.dinner)/SUM(innr.tdinner) As dinnerAverage "

					+ "FROM log lo JOIN ( "
					+ "SELECT l.log_id, "
					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.SNACK.getValue()+"', fls.carbs, 0 ) AS snacks, "
					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.BREAKFAST.getValue()+"', fls.carbs, 0 ) AS breakfast, "
					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.LUNCH.getValue()+"', fls.carbs, 0 )  AS lunch, "
					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.DINNER.getValue()+"' , fls.carbs, 0 ) AS dinner, "

					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.SNACK.getValue()+"', 1, 0 ) AS tsnacks, "
					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.BREAKFAST.getValue()+"', 1, 0 ) AS tbreakfast, "
					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.LUNCH.getValue()+"', 1, 0 )  AS tlunch, "
					+ "IF ( fls.type = '"+AppConstants.TYPEKEYS.DINNER.getValue()+"' , 1, 0 ) AS tdinner "

					+ "FROM log l "
					+ "JOIN food_log_summary fls ON l.log_id = fls.log_id "
					+ "WHERE l.log_time >= "+startDate+ " "
					+ "AND l.log_time <= "+endDate+ " "
					+ "AND l.log_type = '"+AppConstants.LOG_TYPE_MEAL+"' "
					+ "AND l.patient_id ="+patientId + " "
					+ "AND l.is_removed = 0 "
					+ "AND l.is_processed = 1 "
					+ "AND l.is_suggested = 0 "
					+ ") AS innr ON lo.log_id = innr.log_id ";
			result = (Object)currentSession.createSQLQuery(query)
					.uniqueResult();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}
	
	public Object getTrendsByPatientId(long patientId, Long startDate, Long endDate) {
		Object result = null;
		Target target = new TargetDAO().getTargetByPatientId(patientId);
		float mealsTarget[] = CommonUtils.getMealTargets(target);
		try{
			String query = ""
					+ " SELECT COUNT(lo.log_id),"					  // Index 0

					+ " SUM(innr.tBreakfast) AS totalBreakfast, "    // Total Breakfast  // Index 1
					+ " SUM(innr.tLunch) AS totalLunch, "			// Index 2
					+ " SUM(innr.tSnaks) AS totalSnaks, "			// Index 3
					+ " SUM(innr.tDinner) AS totalDinner, "			// Index 4

					+ " SUM(innr.breakfast) AS breakfast, "          // Under ceiling Breakfast // Index 5
					+ " SUM(innr.lunch) AS lunch, "					// Index 6
					+ " SUM(innr.snaks) AS snaks, "					// Index 7
					+ " SUM(innr.dinner) AS dinner, "				// Index 8

					+ " SUM(innr.breakfast) * 100 / SUM(innr.tBreakfast) AS breakfastPer, "     // percentage	// Index 9
					+ " SUM(innr.lunch) * 100 / SUM(innr.tLunch) AS lunchPer, "					// Index 10
					+ " SUM(innr.snaks) * 100 / SUM(innr.tSnaks) AS snakPer, "					// Index 11
					+ " SUM(innr.dinner) * 100 / SUM(innr.tDinner) AS dinnerPer "				// Index 12

					+ " FROM log lo JOIN ( "
					+ "		SELECT l.log_id, "

					+ "		IF (fls.type = '"+AppConstants.TYPEKEYS.BREAKFAST.getValue()+"', 1, 0) AS tBreakfast, "
					+ "		IF (fls.type = '"+AppConstants.TYPEKEYS.LUNCH.getValue()+"', 1, 0) AS tLunch, "
					+ "		IF (fls.type = '"+AppConstants.TYPEKEYS.SNACK.getValue()+"', 1, 0) AS tSnaks, "
					+ "		IF (fls.type = '"+AppConstants.TYPEKEYS.DINNER.getValue()+"', 1, 0) AS tDinner, "
					+ "		IF ( fls.type = '"+AppConstants.TYPEKEYS.BREAKFAST.getValue()+"' AND fls.carbs > 0 AND fls.carbs < "+mealsTarget[0]+", 1, 0 ) AS breakfast, "
					+ "		IF ( fls.type = '"+AppConstants.TYPEKEYS.LUNCH.getValue()+"' AND fls.carbs > 0 AND fls.carbs < "+mealsTarget[1]+", 1, 0 ) AS lunch, "
					+ "		IF ( fls.type = '"+AppConstants.TYPEKEYS.SNACK.getValue()+"' AND fls.carbs > 0 AND fls.carbs < "+mealsTarget[2]+", 1, 0 ) AS snaks, "
					+ "		IF ( fls.type = '"+AppConstants.TYPEKEYS.DINNER.getValue()+"' AND fls.carbs > 0 AND fls.carbs < "+mealsTarget[3]+", 1, 0 ) AS dinner "
					+ "		FROM log l "
					+ "		JOIN food_log_summary fls ON l.log_id = fls.log_id "
					+ " 	WHERE l.log_time >= "+startDate+" "
					+ " 	AND l.log_time <= "+endDate+" "
					+ "		AND l.log_type = '"+AppConstants.LOG_TYPE_MEAL+"' "
					+ "		AND l.patient_id = "+patientId+" "
					+ "		AND l.is_removed = 0 "
					+ "     AND l.is_processed = 1 "
                    + "     AND l.is_suggested = 0 "
					+ "	) AS innr ON lo.log_id = innr.log_id";
			result = (Object)currentSession.createSQLQuery(query)
					.uniqueResult();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
    public List<Object> getMealLogsDataByPatientId(long patientId, String startDate, String endDate) {
        float snackTarget = AppConstants.LOGS.SNAKS_TARGET.getIntValue();
        float breakfastTarget = AppConstants.LOGS.BREAKFAST_TARGET.getIntValue();
        float lunchTarget = AppConstants.LOGS.LUNCH_TARGET.getIntValue();
        float dinnerTarget = AppConstants.LOGS.DINNER_TARGET.getIntValue();
        Target target = new TargetDAO().getTargetByPatientId(patientId);
        if(target != null) {
            for (TargetMeal targetMeal : target.getMealTargets()) {
                if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.BREAKFAST.getValue())) {
                    breakfastTarget = targetMeal.getCarbTarget();
                } else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.LUNCH.getValue())) {
                    lunchTarget = targetMeal.getCarbTarget();
                } else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.DINNER.getValue())) {
                    dinnerTarget = targetMeal.getCarbTarget();
                } else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.SNACK.getValue())) {
                    snackTarget = targetMeal.getCarbTarget();
                }
            }
        }
        List<Object> results = new ArrayList<Object>();

        try{
            results = (List<Object>)currentSession.createSQLQuery(""
                    + " SELECT "
                    + " DATE_FORMAT( FROM_UNIXTIME(l.log_time / 1000), '%m/%d/%Y' ) AS date, "
                    + " DATE_FORMAT( FROM_UNIXTIME(l.log_time / 1000), '%h:%i %p' ) AS time, "
                    + " l.log_type AS type, "
                    + " gl.glucose_level AS glevel, "
                    + " fls.carbs AS carbs, "
                    + " fls.type AS mealType, "
                    + " IF ( l.log_type = '"+AppConstants.LOG_TYPE_GLUCOSE+"' AND gl.glucose_level < 80, 1, 0 ) AS underTarget, "
                    + " IF ( l.log_type = '"+AppConstants.LOG_TYPE_GLUCOSE+"' AND gl.glucose_level >= 80 AND gl.glucose_level <= 130, 1, 0 ) AS InTarget, "
                    + " IF ( l.log_type = '"+AppConstants.LOG_TYPE_GLUCOSE+"' AND gl.glucose_level > 130, 1, 0 ) AS overTarget, "
                    + " IF ( l.log_type = '"+AppConstants.LOG_TYPE_MEAL+"' AND fls.type = '"+AppConstants.TYPEKEYS.BREAKFAST.getValue()+"' AND fls.carbs > 0 AND fls.carbs < " + breakfastTarget +" , 1, 0 ) AS breakfast, "
                    + " IF ( l.log_type = '"+AppConstants.LOG_TYPE_MEAL+"' AND fls.type = '"+AppConstants.TYPEKEYS.LUNCH.getValue()+"' AND fls.carbs > 0 AND fls.carbs < "+ lunchTarget +", 1, 0 ) AS lunch, "
                    + " IF ( l.log_type = '"+AppConstants.LOG_TYPE_MEAL+"' AND fls.type = '"+AppConstants.TYPEKEYS.SNACK.getValue()+"' AND fls.carbs > 0 AND fls.carbs < "+ snackTarget +", 1, 0 ) AS snaks, "
                    + " IF ( l.log_type = '"+AppConstants.LOG_TYPE_MEAL+"' AND fls.type = '"+AppConstants.TYPEKEYS.DINNER.getValue()+"' AND fls.carbs > 0 AND fls.carbs < "+ dinnerTarget +", 1, 0 ) AS dinner,"
                    + " l.log_id "
                    + " FROM log l "
                    + " JOIN glucose_log gl ON l.log_id = gl.log_id "
                    + " LEFT JOIN food_log_summary fls ON l.log_id = fls.log_id "
                    + " WHERE DATE( FROM_UNIXTIME(l.log_time / 1000)) >= '"+startDate+"' "
                    + " AND DATE( FROM_UNIXTIME(l.log_time / 1000)) <= '"+endDate+"' "
                    + " AND ( l.log_type = '"+AppConstants.LOG_TYPE_GLUCOSE+"' OR l.log_type = '"+AppConstants.LOG_TYPE_MEAL+"' ) "
                    + "	AND l.patient_id = "+patientId+" "
                    + " AND l.is_removed = 0 "
                    + " AND l.is_processed = 1 "
                    + " AND l.is_suggested = 0 "
                    + " ORDER BY l.log_time DESC")
                    .list();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return results;
    }

    @SuppressWarnings({ "deprecation", "unchecked" })
	public List<LogDTO> getLogbookDataByPatientId(long patientId, String dateFrom, String dateTo, String logType){

        List<Log> logs = new ArrayList<Log>();
        List<LogDTO> logdto =new ArrayList<LogDTO>();
        StringBuilder queryString = new StringBuilder();

		String logTypeQuery = "";
		if(!ObjectUtils.isEmpty(logType)){
			logTypeQuery = " AND l.logType = '" + logType + "'";

			//for breakfast, lunch etc, have to set logType = Meal and set foodLogSummary type to Breakfast and lunch etc
			if(AppConstants.MEAL_TYPES.containsValue(logType)){
				logTypeQuery = " AND l.logType = 'Meal'  AND fs.type = '" + logType + "'";
			}
		}

        try{
			String offsetKey = CommonUtils.getPatientOffsetKey(patientId);
            Long startTime = 0l;
            Long endTime = 0l;

            if(ObjectUtils.isEmpty(offsetKey)){
                int offsetMillis = CommonUtils.getPatientFacilityOffset(patientId);
                startTime = DateUtils.getOffsetTime(dateFrom, offsetMillis);
                endTime = DateUtils.getEndDate(startTime, 7);
            }else{
                startTime = DateUtils.getOffsetTime(dateFrom, offsetKey);
                endTime = DateUtils.getEndDate(startTime, 7);
            }

			queryString.append("SELECT DISTINCT l "

                    + " FROM Log l"
                    + " JOIN l.patient p"
                    + " LEFT JOIN l.miscLog m "
                    + " LEFT JOIN l.foodLogSummary fs "
                    + " LEFT JOIN l.exerciseLogs el "
                    + " LEFT JOIN l.glucoseLog gl "
                    + " LEFT JOIN l.activityLog al "
                    + " WHERE l.isRemoved = 0"
                    + " AND l.patient.patientId ="+patientId
                    + " AND l.createdOn >= "+startTime+" "
                    + " AND l.createdOn <= "+endTime+" "
                    + " AND l.isSuggested = 0 "
					+ logTypeQuery
                    + " ORDER By l.createdOn DESC");

            logs = currentSession.createQuery(queryString.toString()).list();
        } catch(Exception ex){
            ex.printStackTrace();
        }
        //logs = CommonUtils.getUniqueResults(logs);
        int unreadMessageCount = 0;

        for(Log log : logs){

            LogDTO dto = new LogDTO();
            dto.setLogId(log.getLogId());
            dto.setLogTime(log.getLogTime() ); //changed here

			Long createdTime = log.getCreatedOn();
			//createdTime = createdTime - log.getLogTimeOffset();

			dto.setCreatedOn((createdTime == null) ? log.getLogTime() : createdTime);

            dto.setIsProcessed(ObjectUtils.nullSafe(log.getIsProcessed()));

            dto.setLogTimeString(CommonUtils.timeInUserTimezone(createdTime, log.getLogTimeOffset(), log.getOffsetKey()));

            if(log.getFoodLogSummary() != null){
                if(log.getFoodLogSummary().getCarbs()<=0 || !log.getIsProcessed())
                {

                    dto.setCarbs(AppConstants.NOT_APPLICABLE);
                }
                else{
                    //dto.setCarbs(Integer.toString(log.getFoodLogSummary().getCarbs().intValue()));
                    dto.setCarbs(CommonUtils.getStringFromFloat(log.getFoodLogSummary().getCarbs()));
                }
            }
            if(log.getMiscLog()!=null){
                dto.setBloodPressureSystolic(log.getMiscLog().getBloodPressureSystolic());
                dto.setBloodPressureDiastolic(log.getMiscLog().getBloodPressureDiastolic());
                if(log.getMiscLog().getStepsPerDay() != null) {
                    dto.setStepsPerDay(log.getMiscLog().getStepsPerDay().toString());
                }
                if(log.getMiscLog().getTookMeds() != null) {
                    boolean isTookMeds = log.getMiscLog().getTookMeds();
                    if(isTookMeds) {
                        dto.setTookMeds("Yes");
                    } else {
                        dto.setTookMeds("No");
                    }
                }
                if(log.getMiscLog().getWeight()<=0)
                {
                    dto.setWeight(AppConstants.NOT_APPLICABLE);
                }
                else
                {
                    dto.setWeight(CommonUtils.getStringFromFloat(log.getMiscLog().getWeight()));
                }
                if(dto.getBloodPressureDiastolic()<=0 ||dto.getBloodPressureSystolic()<=0){
                    dto.setBp("N/A");
                }
                else{
                    dto.setBp(dto.getBloodPressureSystolic()+"/"+dto.getBloodPressureDiastolic());
                }
            }

            if(log.getExerciseLogs().size() > 0)
            {
                for(ExerciseLog exlog :log.getExerciseLogs())
                {
                    if(exlog.getType().equals(AppConstants.NEWSFEED.CARDIO.getValue())){

                        dto.setCardio(CommonUtils.getStringFromFloat(exlog.getMinutesPerformed()));

                    }
                    else if (exlog.getType().equals(AppConstants.NEWSFEED.STRENGTH.getValue())){
                        dto.setStrength(CommonUtils.getStringFromFloat(exlog.getMinutesPerformed()));
                    }
                }
            }

            if(log.getFoodLogSummary()!=null)
            {
                if(log.getFoodLogSummary().getType() != null){
                    dto.setMealType(log.getFoodLogSummary().getType());
                }
                else{
                    dto.setMealType(AppConstants.NOT_APPLICABLE);
                }
            }
            if(log.getFoodLogSummary() != null && log.getFoodLogSummary().getMessages() != null){

                for(Message m: log.getFoodLogSummary().getMessages()){

                    if(m.getReadStatus() == null || m.getReadStatus() == false){
                        unreadMessageCount += 1;
                    }
                }
            }
			/*Temporary Stub*/

            dto.setUnreadMessageCount(unreadMessageCount);

            //setting processed time
            if(log.getProcessedTime() != null){

                Long time = log.getUploadTime();
                if(time == null){
                    time = log.getLogTime();
                }
                dto.setMinutesHours(DateUtils.getElapsedTime(time, log.getProcessedTime()));
            }else{
                dto.setMinutesHours(AppConstants.NOT_APPLICABLE);
            }

            //setting coach name
            User user = new UserDAO().getUserById(log.getUserId());
            if(user != null){
                dto.setCoachName(user.getLastName()+ ", " + user.getFirstName());
            }else{
                dto.setCoachName(AppConstants.NOT_APPLICABLE);
            }

			GlucoseLog glucoseLog = log.getGlucoseLog();
            if(glucoseLog != null) {
                dto.setGlucoseLevel(glucoseLog.getGlucoseLevel());
				dto.setgType(glucoseLog.getGlucoseTime());
            }
            dto.setLogType(log.getLogType());

            Patient patient = log.getPatient();
            if(patient != null){
                if(patient.getIsSigned() != null && patient.getIsSigned()){
                    dto.setIsSigned(true);
                }
            }

            ActivityLog activityLog = log.getActivityLog();
            if(activityLog != null){
                 dto.setAlIntensity(activityLog.getIntensity());
                 dto.setAlType(activityLog.getType());
                 dto.setAlMinutesPerformed(activityLog.getMinutesPerformed());
                 dto.setAlNotes(activityLog.getNotes());
            }

			dto.setOnlyDate(DateUtils.getFormattedDate(createdTime + log.getLogTimeOffset(), DateUtils.DATE_FORMAT));
			dto.setOnlyTime(CommonUtils.getOnlyTimeInUserTimeZone(createdTime, log.getLogTimeOffset(), log.getOffsetKey()));

            logdto.add(dto);
        }

        return logdto;
    }

	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<LogDTO> getGlucoseLogbookDataByPatientId(long patientId, String dateFrom, String dateTo){

		List<Log> logs = new ArrayList<Log>();
		List<LogDTO> logdto =new ArrayList<LogDTO>();
		StringBuilder queryString = new StringBuilder();
		try{
			String offsetKey = CommonUtils.getPatientOffsetKey(patientId);
			Long startTime = 0l;
			Long endTime = 0l;

			if(ObjectUtils.isEmpty(offsetKey)){
				int offsetMillis = CommonUtils.getPatientFacilityOffset(patientId);
				startTime = DateUtils.getOffsetTime(dateFrom, offsetMillis);
				endTime = DateUtils.getEndDate(startTime, 7);
			}else{
				startTime = DateUtils.getOffsetTime(dateFrom, offsetKey);
				endTime = DateUtils.getEndDate(startTime, 7);
			}

			queryString.append("SELECT DISTINCT l "

					+ " FROM Log l"
					+ " JOIN l.patient p"
					+ " LEFT JOIN l.glucoseLog gl "
					+ " WHERE l.isRemoved = 0"
					+ " AND l.patient.patientId ="+patientId
					+ " AND l.logTime >= "+startTime+" "
					+ " AND l.logTime <= "+endTime+" "
					+ " AND l.isSuggested = 0 "
					+ " AND l.logType =  " + "'Glucose'"
					+ " ORDER By l.createdOn DESC");

			logs = currentSession.createQuery(queryString.toString()).list();
		} catch(Exception ex){
			ex.printStackTrace();
		}
		//logs = CommonUtils.getUniqueResults(logs);
		int unreadMessageCount = 0;

		for(Log log : logs){

			LogDTO dto = new LogDTO();
			dto.setLogId(log.getLogId());
			dto.setLogTime(log.getLogTime() ); //changed here
			dto.setIsProcessed(log.getIsProcessed());

			dto.setLogTimeString(CommonUtils.timeInUserTimezone(log.getLogTime(), log.getLogTimeOffset(), log.getOffsetKey()));
			dto.setUnreadMessageCount(unreadMessageCount);

			//setting processed time
			if(log.getProcessedTime() != null){

				Long time = log.getUploadTime();
				if(time == null){
					time = log.getLogTime();
				}
				dto.setMinutesHours(DateUtils.getElapsedTime(time, log.getProcessedTime()));
			}else{
				dto.setMinutesHours(AppConstants.NOT_APPLICABLE);
			}

			//setting coach name
			User user = new UserDAO().getUserById(log.getUserId());
			if(user != null){
				dto.setCoachName(user.getLastName()+ ", " + user.getFirstName());
			}else{
				dto.setCoachName(AppConstants.NOT_APPLICABLE);
			}

			GlucoseLog glucoseLog = log.getGlucoseLog();
			if(glucoseLog != null) {
				dto.setGlucoseLevel(glucoseLog.getGlucoseLevel());
				dto.setgType(glucoseLog.getGlucoseTime());
			}
			dto.setLogType(log.getLogType());

			Patient patient = log.getPatient();
			if(patient != null){
				if(patient.getIsSigned() != null && patient.getIsSigned()){
					dto.setIsSigned(true);
				}
			}

			dto.setOnlyDate(DateUtils.getFormattedDate(log.getLogTime() + log.getLogTimeOffset(), DateUtils.DATE_FORMAT));
			dto.setOnlyTime(CommonUtils.getOnlyTimeInUserTimeZone(log.getLogTime(), log.getLogTimeOffset(), log.getOffsetKey()));
			logdto.add(dto);
		}

		return logdto;
	}

	public int getLogbookCount(long patientId, String dateFrom, String dateTo){

		Object avgSteps = null;
		StringBuilder queryString = new StringBuilder();
		int logBookCount = 0;
		try{

			Long startTime = DateUtils.getStartOfDayTime(new Date(dateFrom)).getTime();
			Long endTime = DateUtils.getEndOfDayTime(new Date(dateTo)).getTime();

			queryString.append("SELECT DISTINCT COUNT(l) "

					+ " FROM Log l"
					+ " JOIN l.patient p"
					+ " LEFT JOIN l.miscLog m "
					+ " LEFT JOIN l.foodLogSummary fs "
					+ " LEFT JOIN l.exerciseLogs el "
					+ " LEFT JOIN l.glucoseLog gl "
					+ " LEFT JOIN l.activityLog al "
					+ " WHERE l.isRemoved = 0"
					+ " AND l.patient.patientId ="+patientId
					+ " AND l.createdOn >= "+startTime+" "
					+ " AND l.createdOn <= "+endTime+" "
					+ " AND l.isSuggested = 0 "
					+ " ORDER By l.createdOn DESC");

			avgSteps = currentSession.createQuery(queryString.toString()).uniqueResult();
			if(avgSteps != null){
				logBookCount = Integer.parseInt(avgSteps + "");
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}

		return logBookCount;
	}

    public long getAverageActivityStepsByDate(long patientId, String dateFrom, String dateTo){

        StringBuilder queryString = new StringBuilder();
        Object avgCount = null;
        long longCount = 0;
        try{

            Long startTime = DateUtils.getStartOfDayTime(new Date(dateFrom)).getTime();
            Long endTime = DateUtils.getEndOfDayTime(new Date(dateTo)).getTime();

            queryString.append("SELECT DISTINCT SUM(l.steps)/7 "

                    + " FROM MisfitActivityLog l"
                    + " JOIN l.patient p"
                    + " WHERE l.patient.patientId ="+patientId
                    + " AND l.timestamp >= "+startTime+" "
                    + " AND l.timestamp <= "+endTime+" "
                    + " ORDER By l.timestamp DESC");

            avgCount = currentSession.createQuery(queryString.toString()).uniqueResult();

            if(!ObjectUtils.isEmpty(ObjectUtils.nullSafe(avgCount))){
                longCount = (Long)avgCount;
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }

        return longCount;
    }

	@SuppressWarnings("unchecked")
	 public List<Object> getGlucoseValuesPerDayByPatientId(long patientId, Long startDate, Long endDate) {

		List<Object> glucoseValueList = new ArrayList<Object>();
		StringBuilder queryString = new StringBuilder();

		try {
			queryString.append("SELECT DISTINCT l ,g.glucoseLevel ,g.timestamp, l.logId, g.glucoseTime "

					+ " FROM Log l"
					+ " JOIN l.patient p"
					+ " LEFT JOIN l.glucoseLog g "
					+ " WHERE l.isRemoved = 0 "
					+ " AND l.logType = '" + AppConstants.LOG_TYPE_GLUCOSE + "'");

			if (patientId != 0) {
				queryString.append(" AND l.patient.patientId =" + patientId);
			}

			if (startDate != null && endDate != null) {
				queryString.append(" AND g.timestamp >= " + startDate + " " + " AND g.timestamp <= " + endDate + " ");
			}

			glucoseValueList = currentSession.createQuery(queryString.toString()).list();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return glucoseValueList;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getGlucoseAfterMealValuesPerDayByPatientId(long patientId, Long startDate, Long endDate) {

		List<Object> glucoseValueList = new ArrayList<Object>();
		StringBuilder queryString = new StringBuilder();

		try {
			queryString.append("SELECT DISTINCT l ,g.glucoseLevel ,g.timestamp, l.logId, g.glucoseTime "

					+ " FROM Log l"
					+ " JOIN l.patient p"
					+ " LEFT JOIN l.glucoseLog g "
					+ " WHERE l.isRemoved = 0 "
					+ " AND g.glucoseTime = '" + AppConstants.GLUCOSE_AFTER_MEAL + "'"
					+ " AND l.logType = '" + AppConstants.LOG_TYPE_GLUCOSE + "'");

			if (patientId != 0) {
				queryString.append(" AND l.patient.patientId =" + patientId);
			}

			if (startDate != null && endDate != null) {
				queryString.append(" AND g.timestamp >= " + startDate + " " + " AND g.timestamp <= " + endDate + " ");
			}

			glucoseValueList = currentSession.createQuery(queryString.toString()).list();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return glucoseValueList;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getGlucoseBeforeMealValuesPerDayByPatientId(long patientId, Long startDate, Long endDate) {

		List<Object> glucoseValueList = new ArrayList<Object>();
		StringBuilder queryString = new StringBuilder();

		try {
			queryString.append("SELECT DISTINCT l ,g.glucoseLevel ,g.timestamp, l.logId, g.glucoseTime "

					+ " FROM Log l"
					+ " JOIN l.patient p"
					+ " LEFT JOIN l.glucoseLog g "
					+ " WHERE l.isRemoved = 0 "
					+ " AND g.glucoseTime = '" + AppConstants.GLUCOSE_BEFORE_MEAL + "'"
					+ " AND l.logType = '" + AppConstants.LOG_TYPE_GLUCOSE + "'");

			if (patientId != 0) {
				queryString.append(" AND l.patient.patientId =" + patientId);
			}

			if (startDate != null && endDate != null) {
				queryString.append(" AND g.timestamp >= " + startDate + " " + " AND g.timestamp <= " + endDate + " ");
			}

			glucoseValueList = currentSession.createQuery(queryString.toString()).list();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return glucoseValueList;
	}

	/**
	 * @Author Irfan Nasim
	 * @param patientId, startDate, endDate
	 * @Description to get Before Meal STD
	 *
	 * */
	@SuppressWarnings("unchecked")
	public List<Object> getGlucoseBeforeMealSTDPerDayByPatientId(long patientId, Long startDate, Long endDate) {

		List<Object> std = new ArrayList<Object>();
		StringBuilder queryString = new StringBuilder();

		try {
			queryString.append("SELECT FORMAT(STD(g.glucose_level), 2) std "
					+ " FROM log l"
					+ " JOIN patient p ON l.patient_id = p.patient_id "
					+ " LEFT JOIN glucose_log g ON g.log_id = l.log_id "
					+ " WHERE l.is_removed = 0 "
					+ " AND g.glucose_time = '" + AppConstants.GLUCOSE_BEFORE_MEAL + "'"
					+ " AND l.log_type = '" + AppConstants.LOG_TYPE_GLUCOSE + "'");

			if (patientId != 0) {
				queryString.append(" AND l.patient_id =" + patientId);
			}

			if (startDate != null && endDate != null) {
				queryString.append(" AND g.timestamp >= " + startDate + " " + " AND g.timestamp <= " + endDate + " ");
			}

			std = currentSession.createSQLQuery(queryString.toString()).list();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return std;
	}

	/**
	 * @Author Irfan Nasim
	 * @param patientId, startDate, endDate
	 * @Description to get Before Meal STD
	 *
	 * */
	@SuppressWarnings("unchecked")
	public List<Object> getGlucoseAfterMealSTDPerDayByPatientId(long patientId, Long startDate, Long endDate) {

		List<Object> std = new ArrayList<Object>();
		StringBuilder queryString = new StringBuilder();

		try {
			queryString.append("SELECT FORMAT(STD(g.glucose_level), 2) std "
					+ " FROM log l"
					+ " JOIN patient p ON l.patient_id = p.patient_id "
					+ " LEFT JOIN glucose_log g ON g.log_id = l.log_id "
					+ " WHERE l.is_removed = 0 "
					+ " AND g.glucose_Time = '" + AppConstants.GLUCOSE_AFTER_MEAL + "'"
					+ " AND l.log_Type = '" + AppConstants.LOG_TYPE_GLUCOSE + "'");

			if (patientId != 0) {
				queryString.append(" AND l.patient_id =" + patientId);
			}

			if (startDate != null && endDate != null) {
				queryString.append(" AND g.timestamp >= " + startDate + " " + " AND g.timestamp <= " + endDate + " ");
			}

			std = currentSession.createSQLQuery(queryString.toString()).list();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return std;
	}

	//for 3, week, 14, 30 days generic
	@SuppressWarnings("unchecked")
	public List<Object> getAvgGlucoseValuesPerDayByPatientId(long patientId, Long startDate, Long endDate) {
		List<Object> glucoseValueList = new ArrayList<Object>();
		StringBuilder queryString = new StringBuilder();

		try {
			queryString.append("SELECT DISTINCT g.timestamp,"
					+ " g.glucose_level as avg_glucose, g.glucose_time, l.created_on, l.log_time, l.log_time_offset, l.log_id, g.timestamp glTimestamp"
					+ " FROM log l"
					+ " JOIN patient p on p.patient_id = l.patient_id"
					+ " LEFT JOIN glucose_log g on g.log_id = l.log_id"
					+ " WHERE l.is_removed = 0"
					+ " AND l.log_type = '" + AppConstants.LOG_TYPE_GLUCOSE + "'"
					+ " AND l.patient_id = " + patientId
					+ " AND g.timestamp >= " + startDate + "" + " AND g.timestamp <= " + endDate + "");

			glucoseValueList = currentSession.createSQLQuery(queryString.toString()).list();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return glucoseValueList;
	}

	//for 3, week, 14, 30 days generic
	@SuppressWarnings("unchecked")
	public List<Object> getAvgGlucoseBeforeMealValuesPerDayByPatientId(long patientId, Long startDate, Long endDate) {
		List<Object> glucoseValueList = new ArrayList<Object>();
		StringBuilder queryString = new StringBuilder();

		try {
			queryString.append("SELECT DISTINCT g.timestamp,"
					+ " g.glucose_level as avg_glucose, g.glucose_time, l.created_on, l.log_time, l.log_time_offset, l.log_id, g.timestamp glTimestamp"
					+ " FROM log l"
					+ " JOIN patient p on p.patient_id = l.patient_id"
					+ " LEFT JOIN glucose_log g on g.log_id = l.log_id"
					+ " WHERE l.is_removed = 0"
					+ " AND l.log_type = '" + AppConstants.LOG_TYPE_GLUCOSE + "'"
					+ " AND g.glucose_time = '" + AppConstants.GLUCOSE_BEFORE_MEAL + "'"
					+ " AND l.patient_id = " + patientId
					+ " AND g.timestamp >= " + startDate + "" + " AND g.timestamp <= " + endDate + "");

			glucoseValueList = currentSession.createSQLQuery(queryString.toString()).list();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return glucoseValueList;
	}

	//for 3, week, 14, 30 days generic
	@SuppressWarnings("unchecked")
	public List<Object> getAvgGlucoseAfterMealValuesPerDayByPatientId(long patientId, Long startDate, Long endDate) {
		List<Object> glucoseValueList = new ArrayList<Object>();
		StringBuilder queryString = new StringBuilder();

		try {
			queryString.append("SELECT DISTINCT g.timestamp,"
					+ " g.glucose_level as avg_glucose, g.glucose_time, l.created_on, l.log_time, l.log_time_offset, l.log_id, g.timestamp glTimestamp"
					+ " FROM log l"
					+ " JOIN patient p on p.patient_id = l.patient_id"
					+ " LEFT JOIN glucose_log g on g.log_id = l.log_id"
					+ " WHERE l.is_removed = 0"
					+ " AND l.log_type = '" + AppConstants.LOG_TYPE_GLUCOSE + "'"
					+ " AND g.glucose_time = '" + AppConstants.GLUCOSE_AFTER_MEAL + "'"
					+ " AND l.patient_id = " + patientId
					+ " AND g.timestamp >= " + startDate + "" + " AND g.timestamp <= " + endDate + "");

			glucoseValueList = currentSession.createSQLQuery(queryString.toString()).list();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return glucoseValueList;
	}

	//for 3, week, 14, 30 days generic
	@SuppressWarnings("unchecked")
	public List<Object> getAvgGlucoseSTDBeforerMealValuesPerDayByPatientId(long patientId, Long startDate, Long endDate) {
		List<Object> glucoseValueList = new ArrayList<Object>();
		StringBuilder queryString = new StringBuilder();

		try {
			queryString.append("SELECT FORMAT(STD(g.glucose_level),2) "
					+ " FROM log l"
					+ " JOIN patient p on p.patient_id = l.patient_id"
					+ " LEFT JOIN glucose_log g on g.log_id = l.log_id"
					+ " WHERE l.is_removed = 0"
					+ " AND l.log_type = '" + AppConstants.LOG_TYPE_GLUCOSE + "'"
					+ " AND g.glucose_time = '" + AppConstants.GLUCOSE_BEFORE_MEAL + "'"
					+ " AND l.patient_id = " + patientId
					+ " AND g.timestamp >= " + startDate + "" + " AND g.timestamp <= " + endDate + "");

			glucoseValueList = currentSession.createSQLQuery(queryString.toString()).list();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return glucoseValueList;
	}

	//for 3, week, 14, 30 days generic
	@SuppressWarnings("unchecked")
	public List<Object> getAvgGlucoseSTDAfterMealValuesPerDayByPatientId(long patientId, Long startDate, Long endDate) {
		List<Object> glucoseValueList = new ArrayList<Object>();
		StringBuilder queryString = new StringBuilder();

		try {
			queryString.append("SELECT FORMAT(STD(g.glucose_level),2) "
					+ " FROM log l"
					+ " JOIN patient p on p.patient_id = l.patient_id"
					+ " LEFT JOIN glucose_log g on g.log_id = l.log_id"
					+ " WHERE l.is_removed = 0"
					+ " AND l.log_type = '" + AppConstants.LOG_TYPE_GLUCOSE + "'"
					+ " AND g.glucose_time = '" + AppConstants.GLUCOSE_AFTER_MEAL + "'"
					+ " AND l.patient_id = " + patientId
					+ " AND g.timestamp >= " + startDate + "" + " AND g.timestamp <= " + endDate + "");

			glucoseValueList = currentSession.createSQLQuery(queryString.toString()).list();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return glucoseValueList;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getWeightValuesPerDayByPatientId(long patientId, Long startDate, Long endDate) {

		List<Object> weightValueList = new ArrayList<Object>();
		StringBuilder queryString = new StringBuilder();
		
		try{
			queryString.append("SELECT DISTINCT l ,ml.weight ,l.logTime, l.logId"
					
					+ " FROM Log l"
					+ " JOIN l.patient p"
					+ " LEFT JOIN l.miscLog ml "	
					+ " WHERE l.isRemoved = 0 " 
					+ " AND l.logType = '"+ AppConstants.LOG_TYPE_WEIGHT +"'");
			
			if(patientId != 0){
				queryString.append(" AND l.patient.patientId ="+patientId);
			}
			
			if(startDate != null && endDate != null) {
				queryString.append(" AND l.logTime>= "+startDate+" " + " AND l.logTime <= "+endDate+" ");
			}
			
			weightValueList = currentSession.createQuery(queryString.toString()).list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		return weightValueList;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getCarbsValuesPerDayByPatientId(long patientId, Long startTime, Long endTime) {

		List<Object> carbsValueList = new ArrayList<Object>();
		StringBuilder queryString = new StringBuilder();
		
		try{
			queryString.append("SELECT DISTINCT l, fs.carbs, fs.type, fs.timestamp, fs.hasMissingFood, l.logId"
					
					+ " FROM Log l"
					+ " JOIN l.patient p"
					+ " LEFT JOIN l.foodLogSummary fs "	
					+ " WHERE l.isRemoved = 0 "
					+ " AND l.isProcessed = 1"
                    + " AND l.isSuggested = 0"); //_oz added
			
			if(patientId != 0){
				queryString.append(" AND l.patient.patientId ="+patientId);
			}
			
			queryString.append(" AND l.logType = '"+AppConstants.LOG_TYPE_MEAL+"' ");
			
			if(startTime != null && endTime != null){
				queryString.append(" AND fs.timestamp >= "+startTime+" " + " AND fs.timestamp <= "+endTime+" ");
			}
			
			carbsValueList = currentSession.createQuery(queryString.toString()).list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		return carbsValueList;
	}

	public List<Object> getMedsTimeByPatientId(long patientId, long startDate, long endDate) {
		List<Object> medsTimeList = new ArrayList<Object>();
		String query =  "SELECT ml.timestamp from misc_log ml JOIN  log l ON l.log_id "
				+" WHERE ml.timestamp >= "+startDate
				+" AND ml.timestamp <= " +endDate
				+" AND ml.took_meds = 1 "
				+" AND l.is_removed = 0 "
				+" AND l.patient_id = "+patientId;
		try {
			medsTimeList = currentSession.createSQLQuery(query).list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return medsTimeList;
	}

	public TookMedsDTO getTookMedsByPatientId(long patientId, long startDate, long endDate) {
		TookMedsDTO tookMedsDTO = new TookMedsDTO();
		StringBuilder queryString = new StringBuilder();
		
		try{
			queryString.append("SELECT SUM(sr.is_took_med) / COUNT(*) * 100 AS 'reply(%)', Count(*) As total, SUM(sr.is_took_med) As medsTaken "
					+ "	FROM sms_reply sr "					
					+ " WHERE sr.sent_time >= "+ startDate + ""
					+ " AND sr.sent_time <= " + endDate + ""
					+ " AND sr.patient_id = " + patientId + "");
						
			Object object = currentSession.createSQLQuery(queryString.toString()).uniqueResult();
			if(object != null) {
				Object[] objectArray = (Object[]) object;
				int medsPercentage = (int) (objectArray[0] == null ? 0: Math.ceil(((BigDecimal)objectArray[0]).doubleValue()));
				int totalCount = objectArray[1] == null ? 0: ((BigInteger)objectArray[1]).intValue();
				int medsTaken = objectArray[2] == null ? 0: ((BigDecimal)objectArray[2]).intValue();
				tookMedsDTO.setMedsPercentage(medsPercentage);
				tookMedsDTO.setTotalCount(totalCount);
				tookMedsDTO.setMedsTakenCount(medsTaken);
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		
		return tookMedsDTO;
	}

    public List<Object> getTookMedsByPatientId_LineChart(Long patientId, Long startDate, Long endDate) {
        List<Object> medsDetailData = new ArrayList<Object>();
        StringBuilder queryString = new StringBuilder();
        try{
            queryString.append("SELECT SUM(sr.is_took_med) / COUNT(*) * 100 AS 'reply(%)', "
                    + " Count(*) As total, "
                    + " SUM(sr.is_took_med) As medsTaken, "
                    + " sr.sent_time As day, "
                    + " DATE_FORMAT( FROM_UNIXTIME(sr.sent_time /1000), '%Y-%d-%m' ) as date"
                    + "	FROM sms_reply sr "
                    + " WHERE sr.sent_time >= "+ startDate + ""
                    + " AND sr.sent_time <= " + endDate + ""
                    + " AND sr.patient_id = " + patientId + ""
                    + " GROUP BY DATE_FORMAT( FROM_UNIXTIME(sr.sent_time/1000), '%Y-%d-%m')");

            medsDetailData = currentSession.createSQLQuery(queryString.toString()).list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return medsDetailData;
    }

	@SuppressWarnings("unchecked")
	public List<Object> getActivityDetailData(Long patientId, Long startDate, Long endTime) {
		List<Object> activityDetialList = new ArrayList<Object>();
		try{
			activityDetialList = currentSession.createSQLQuery("SELECT SUM(ml.steps_per_day) as stepsPerDay,"
					+ " l.created_on as day,"
					+ " DATE_FORMAT( FROM_UNIXTIME(l.created_on /1000), '%Y-%d-%m' ) as date"
					+ " FROM misc_log ml, log l" 
					+ " WHERE ml.log_id = l.log_id" 
					+ " AND l.patient_id ="+ patientId
					+ " AND l.created_on >="+ startDate 
					+ " AND l.created_on <="+ endTime
					+ " AND l.log_type = 'Activity'"
					+ " AND l.is_removed = 0"
					+ " group by DATE_FORMAT( FROM_UNIXTIME(l.created_on/1000), '%Y-%d-%m')").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return activityDetialList;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getActivityDetailDataObjects(Long patientId, Long startDate, Long endTime) {
		List<Object> activityDetialList = new ArrayList<Object>();
		try{
			activityDetialList = currentSession.createSQLQuery("SELECT Sum(al.minutes_performed) as minutsPerformed,"
					+ " al.timestamp as day,"
					+ " DATE_FORMAT( FROM_UNIXTIME(al.timestamp /1000), '%Y-%d-%m' ) as date, "
					+ " al.type as activity_log_type, "
					+ " GROUP_CONCAT(DISTINCT al.type SEPARATOR ', ') actType, "
					+ " l.created_on, l.log_time, l.log_time_offset "
					+ " FROM activity_log al JOIN log l ON al.log_id = l.log_id "
					+ " WHERE al.log_id = l.log_id"
					+ " AND l.patient_id = "+ patientId
					+ " AND l.created_on >= "+ startDate
					+ " AND l.created_on <= "+ endTime
					+ " AND l.log_type = '" + AppConstants.LOG_TYPE_ACTIVITY_MINUTE + "'"
					+ " AND l.is_removed = false "
					+ " group by DATE_FORMAT( FROM_UNIXTIME(al.timestamp/1000), '%Y-%d-%m')").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return activityDetialList;
	}

    @SuppressWarnings("unchecked")
    public List<Object> getAvgActivityDetailDataObjects(Long patientId, Long startDate, Long endTime, int noOfDays) {
        List<Object> activityDetialList = new ArrayList<Object>();
        try{
            activityDetialList = currentSession.createSQLQuery("SELECT  al.timestamp as day, "
                    + " ROUND((SUM(al.minutes_performed)/" + noOfDays + ") ,1) as minutsPerformed, "
                    + " GROUP_CONCAT(DISTINCT al.type SEPARATOR ', ') actType "
                    + " FROM activity_log al JOIN log l ON al.log_id = l.log_id "
                    + " WHERE al.log_id = l.log_id"
                    + " AND l.patient_id = "+ patientId
                    + " AND l.created_on >= "+ startDate
                    + " AND l.created_on <= "+ endTime
                    + " AND l.log_type = '" + AppConstants.LOG_TYPE_ACTIVITY_MINUTE + "'"
                    + " AND l.is_removed = false "
                    + " GROUP BY DATE_FORMAT( FROM_UNIXTIME(al.timestamp/1000), '%H')").list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return activityDetialList;
    }

	@SuppressWarnings("unchecked")
	public List<ActivityLog> getMinutesActivityDetailData(Long patientId, Long startDate, Long endDate) {
		List<ActivityLog> activityDetialList = new ArrayList<ActivityLog>();
		try{
			//commented by irfan nasim
	        //adding member offset when querying
        /*    long offset = CommonUtils.getPatientTimeZoneOffset(patientId);
            startDate = startDate - offset;
            endDate = endDate - offset; */

			activityDetialList = (List<ActivityLog>) currentSession.createQuery("SELECT al" +
					" FROM ActivityLog al " +
					" JOIN al.log l" +
					" WHERE l.patient.patientId = " + patientId +
					" AND l.createdOn >= " + startDate +
					" AND l.createdOn <= " + endDate +
					" AND l.logType = '" + AppConstants.LOG_TYPE_ACTIVITY_MINUTE + "'" +
					" AND l.isRemoved = false ").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return activityDetialList;
	}

	public List<Object> getMisfitActivityHourlyLogsByPatientId(Long patientId, Long startDate, Long endDate) {
		List<Object> misfitActivityHourlyLogs = new ArrayList<Object>();
		try{
			misfitActivityHourlyLogs =  currentSession.createSQLQuery("SELECT SUM(mfa.steps) as steps, "
					+ " mfa.`timestamp` as `time`, DATE_FORMAT( FROM_UNIXTIME(mfa.`timestamp`/1000), '%Y-%d-%m'), mfa.log_Time_Offset "
					+ " FROM misfit_activity_log mfa "
					+ " WHERE mfa.patient_id = "+patientId
					+ " AND mfa.timestamp >= "+ startDate
					+ " AND mfa.timestamp <= "+ endDate
					+ " group by DATE_FORMAT( FROM_UNIXTIME(mfa.`timestamp`/1000), '%Y-%d-%m %h')"
					+ " ORDER BY mfa.timestamp ASC ").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return misfitActivityHourlyLogs;
	}

    public List<Object> getAvgMisfitActivityHourlyLogsByPatientId(Long patientId, Long startDate, Long endDate, int noOfDays) {
        List<Object> misfitActivityHourlyLogs = new ArrayList<Object>();
        try{
            misfitActivityHourlyLogs =  currentSession.createSQLQuery("SELECT mfa.`timestamp` as `time`,"
                    + " ROUND(((SUM(mfa.steps))/" + noOfDays + "),1) as steps,"
					+ " DATE_FORMAT( FROM_UNIXTIME(mfa.`timestamp`/1000), '%Y-%d-%m') "
					+ " FROM misfit_activity_log mfa "
                    + " WHERE mfa.patient_id = "+patientId
                    + " AND mfa.timestamp >= "+ startDate
                    + " AND mfa.timestamp <= "+ endDate
                    + " group by DATE_FORMAT( FROM_UNIXTIME(mfa.`timestamp`/1000), '%H')").list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return misfitActivityHourlyLogs;
    }

    public List<Object> getMisfitActivityHourlyLogsSumByPatientId(Long patientId, Long startDate, Long endDate) {
        List<Object> misfitActivityHourlyLogs = new ArrayList<Object>();
        try{
            misfitActivityHourlyLogs =  currentSession.createSQLQuery("SELECT SUM(mfa.steps) as steps, "
                    + " mfa.`timestamp` as `time` "
                    + " FROM misfit_activity_log mfa "
                    + " WHERE mfa.patient_id = "+patientId
                    + " AND mfa.timestamp >= "+ startDate
                    + " AND mfa.timestamp <= "+ endDate
                    + " group by DATE_FORMAT( FROM_UNIXTIME(mfa.`timestamp`/1000), '%Y-%d-%m')"
                    + " ORDER BY mfa.timestamp ASC ").list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return misfitActivityHourlyLogs;
    }

	@SuppressWarnings("unchecked")
	public List<Object> getTookMedsDataByPatientId(Long patientId, Long startDate, Long endDate) {
		List<Object> tookMedsDataList = new ArrayList<Object>();
		try{
			tookMedsDataList = currentSession.createSQLQuery("SELECT COUNT(*) as totalMedsPerDay, "
					+ "  l.created_on as day "
					+ "  FROM misc_log ml, log l "
					+ "  WHERE ml.log_id = l.log_id "
					+ "  AND l.patient_id = "+ patientId
					+ "  AND l.created_on >= "+ startDate
					+ "  AND l.created_on <=  "+ endDate
					+ "  AND l.log_type = '"+ AppConstants.LOG_TYPE_MEDICATION+"'"
					+ "  AND l.is_removed = 0 "
					+ "  AND ml.took_meds = 1 "
					+ "  group by DATE_FORMAT( FROM_UNIXTIME(l.created_on/1000), '%Y-%d-%m') "
					+ "  ORDER BY l.created_on ASC ").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tookMedsDataList;
	}

	@SuppressWarnings("unchecked")
	public List<MiscLog> getWeightDataForTimeInterval(Long patientId, Long startDate, Long endTime) {
		List<MiscLog> miscLogList = new ArrayList<MiscLog>();
		try{
			miscLogList = (List<MiscLog>) currentSession.createQuery(" SELECT ml FROM MiscLog ml, Log l where ml.log.logId = l.logId " +
					"AND l.logType = '"+AppConstants.LOG_TYPE_WEIGHT +"' AND l.isRemoved = 0 AND l.patient.patientId = ? AND l.logTime >= ? AND l.logTime <= ? ORDER BY l.logTime DESC")
					.setParameter(0, patientId)
					.setParameter(1, startDate)
					.setParameter(2, endTime)
					.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return miscLogList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getWeightDataForChart(Long patientId, Long startDate, Long endDate) {
		List<Object> miscLogList = new ArrayList<Object>();
		try{
			String query = "SELECT 	AVG(ml.weight) As avg, 	" +
					"l.created_on as day, " +
					"DATE_FORMAT( FROM_UNIXTIME(l.created_on /1000), '%Y-%d-%m' ) as date " +
					"FROM 	misc_log ml, log l " +
					"WHERE	ml.log_id = l.log_id " +
					"AND l.log_type = '"+ AppConstants.LOG_TYPE_WEIGHT +"' " +
					"AND l.is_removed = 0 " +
					"AND l.patient_id = "+ patientId +" " +
					"AND l.log_time >= " + startDate +" " +
					"AND l.log_time <= "+ endDate +" " +
					"GROUP BY DATE_FORMAT( FROM_UNIXTIME(l.created_on/1000), '%Y-%d-%m') " +
					"ORDER BY l.created_on ASC "; 
					miscLogList = (List<Object>) currentSession.createSQLQuery(query).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return miscLogList;
	}
	
	@SuppressWarnings({"unchecked"})
	public List<RecentMealDTO> getAllRecentMeals(long patientId, String type, String appPath, String date, boolean isProcessed){
		
		List<RecentMealDTO> recentMealDTOs = null;
		List<Object> objects = null;
		StringBuilder queryString = new StringBuilder();
		
		String filtered = " AND fls.type = '" + type + "'";
		
		if(type.equalsIgnoreCase(AppConstants.LOG_TYPE_MAP.get(0))){
			filtered = "";
		}

		String filterByDate =  "";
		if(!ObjectUtils.isEmpty(date)) {
			String[] dateParts = date.split("-");
			Long dateFrom = DateUtils.getStartOfDayTime(CommonUtils.getFormatedDate(dateParts[0]));
			Long dateTo = DateUtils.getEndOfDayTime(CommonUtils.getFormatedDate(dateParts[1]));
			filterByDate = " AND l.created_on >= "+dateFrom
						 + " AND l.created_on <= "+dateTo+" ";
		}

		String filterIsProcessed = "";
		if(isProcessed) {
			filterIsProcessed = " AND l.is_processed = 1 ";
		}

		try {
						
			queryString.append("SELECT l.log_id, fi.image_name, l.is_processed, l.log_time, l.log_time_offset, l.offset_key, fls.type, fls.meal_name, fls.carbs, fls.has_missing_food, fld.no_of_servings, fm.serving_size_unit, u.first_name, u.last_name "
					+ " FROM log l "
					+ " LEFT JOIN food_log_summary fls ON fls.log_id = l.log_id "
                    + " LEFT JOIN food_log_detail fld ON fld.food_log_summary_id = fls.food_log_summary_id "
                    + " LEFT JOIN food_master fm ON fm.food_master_id = fld.food_master_id "
					+ " LEFT JOIN food_image fi ON fi.food_log_summary_id = fls.food_log_summary_id "
					+ " LEFT JOIN users u ON u.user_id = l.user_id "
					+ " WHERE l.patient_id = " + patientId + ""
					+ " AND l.is_removed = 0"
                    + " AND l.is_suggested = 0 AND (l.is_archive = 0 OR l.is_archive IS NULL) "
                    + " AND l.log_type = '"+AppConstants.LOG_TYPE_MEAL+"'"
					+ filtered
					+ filterByDate
					+ filterIsProcessed
					+ " GROUP BY l.log_id ORDER BY l.created_on DESC");
						
			objects = (List<Object>)currentSession.createSQLQuery(queryString.toString()).list();
			
			if(objects != null && objects.size() > 0){
				
				recentMealDTOs = new ArrayList<RecentMealDTO>();
				
				for (Object object : objects) {
					Object[] objArray = (Object[]) object;
					
					if(objArray != null){
						
						String logId = (String)objArray[0];
						
						String imageName = ObjectUtils.nullSafe(objArray[1]);
												
						Object isProcessedFromDB = objArray[2];
						boolean isDBProcessed = false;
						if(isProcessedFromDB != null){
							isDBProcessed = (Boolean)isProcessedFromDB;
						}
						
						Object logTimeFromDB = objArray[3];
						long logTime = 0;
						if(logTimeFromDB != null){
							logTime = ((BigInteger)logTimeFromDB).longValue();
						}
						
						Object logTimeOffsetFromDB = objArray[4];
						long logTimeOffset = 0;
						if(logTimeOffsetFromDB != null){
							logTimeOffset = ((BigInteger)logTimeOffsetFromDB).longValue();
						}
						
						String offsetKey = ObjectUtils.nullSafe(objArray[5]);
						String typeFromDB = ObjectUtils.nullSafe(objArray[6]);
						String mealNameFromDB = ObjectUtils.nullSafe(objArray[7]);
						
						Object carbsFromDB = objArray[8];
						float carbsFrom = 0; 
						if(carbsFromDB != null){
							carbsFrom = (Float)carbsFromDB;
						}
						
						Object hasMissingFoodFromDB = objArray[9];
						boolean hasMissingFood = false; 
						if(hasMissingFoodFromDB != null){
							hasMissingFood = (Boolean)hasMissingFoodFromDB;
						}
						
						String logTimeCalculated = CommonUtils.timeInUserTimezone(logTime, logTimeOffset, offsetKey);
						String imageSrc = appPath + imageName;

                        String noOfServings = ObjectUtils.nullSafe(objArray[10]);
                        String unit = ObjectUtils.nullSafe(objArray[11]);
						String processedBy = ObjectUtils.nullSafe(objArray[12]) + " " + ObjectUtils.nullSafe(objArray[13]);

                        RecentMealDTO recentMealDTO = new RecentMealDTO(logId, imageSrc, isDBProcessed, logTimeCalculated, typeFromDB, mealNameFromDB, carbsFrom, hasMissingFood, processedBy);
                        recentMealDTO.setUnit(unit);
                        recentMealDTO.setNoOfServings(noOfServings);

						recentMealDTOs.add(recentMealDTO);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return recentMealDTOs;
	}
	
	public Object getActivityDataByPatientId(Long patientId, Long startDate, Long endDate) {
		Object result = null;
		Target target = new TargetDAO().getTargetByPatientId(patientId);
		int activityGoal = 0;
		if(target != null) {
			activityGoal = target.getActivitySteps();
		}
		
		result = (Object)currentSession.createSQLQuery("" +
				"Select SUM(ml.steps_per_day)* 100 /"+ activityGoal +" As percentage, SUM(ml.steps_per_day) As sum"
				+ " from log l" 
				+ " JOIN misc_log ml"  
				+ " ON l.log_id = ml.log_id"
				+ " WHERE l.log_type = '"+ AppConstants.LOG_TYPE_ACTIVITY + "'" 
				+ " AND l.patient_id = "+ patientId
				+ " AND l.log_time >="+ startDate
				+ " AND l.is_removed = 0"
				+ " AND l.log_time <="+ endDate ).uniqueResult();
		return result;
	}

	/*public Object getSumOfActivityStepOfCurrentDay(Long patientId, Long todayStartDate, Long todayEndDate) {
		Object result = null;
		try {
			result = (Object)currentSession.createSQLQuery(
					"Select SUM(ml.steps_per_day)"
					+ " from log l" 
					+ " JOIN misc_log ml"  
					+ " ON l.log_id = ml.log_id"
					+ " WHERE l.log_type = '"+ AppConstants.LOG_TYPE_ACTIVITY + "'" 
					+ " AND l.patient_id = "+ patientId
					+ " AND l.log_time >="+ todayStartDate
					+ " AND l.is_removed = 0"
					+ " AND l.log_time <="+ todayEndDate ).uniqueResult();	
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return result;
	}*/
	
	@SuppressWarnings("unchecked")
	public String getOffsetKeyFromLog(Long patientId) {

		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = null;
		
		if(session.getTransaction() != null && session.getTransaction().isActive()){
			transaction = session.getTransaction();
		}else{
			transaction = session.beginTransaction();			
		}
		
		session.clear();
		
		List<Log> logs = new ArrayList<Log>();
		StringBuilder queryString = new StringBuilder();
		String offsetKey = "";
		try{
			
			queryString.append("SELECT DISTINCT l "
					+ " FROM Log l "
					+ " JOIN l.patient p"
					+ " WHERE l.isRemoved = 0"
					+ " AND p.patientId =" + patientId + " ORDER BY l.logTime DESC");
			
			
			logs = session.createQuery(queryString.toString()).setMaxResults(1).list();
			
			if(logs != null && logs.size() > 0){
				offsetKey = logs.get(0).getOffsetKey();
			}	
			
			transaction.commit();
			
		}catch(Exception ex){
			ex.printStackTrace();
			transaction.rollback();
		}
		
		return offsetKey;
	}

    public float getCurrentWeightOfPatient(long patientId){
        float weight = 0.0f;
        try {
            List<Object> obj = currentSession.createSQLQuery("SELECT ml.weight " +
                    "FROM " +
                    "log l " +
                    "JOIN misc_log ml ON ml.log_id = l.log_id " +
                    "WHERE l.patient_id = " + patientId + " AND ml.weight <> 0 " +
                    "ORDER BY l.log_time DESC LIMIT 1").list();

            if(obj != null && obj.size() > 0){
                weight = (Float)obj.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return weight;
    }

	@SuppressWarnings("unchecked")
	public List<SyncLog> getProcessedMealLogs(Long patientId, Long dateFrom, Long dateTo) {
		List<SyncLog> processedMealLogs = new ArrayList<SyncLog>();
		String query = "FROM SyncLog l " +
				" WHERE l.isProcessed = true " +
				" AND l.logType ='"+ AppConstants.LOG_TYPE_MEAL+"'" +
				" AND l.patient.patientId = "+ patientId +
				" AND l.logProcessedTime >= "+ dateFrom +
				" AND l.logProcessedTime <= "+ dateTo +
				" AND l.isRemoved = false "+
				" ORDER BY l.logProcessedTime ASC";
		processedMealLogs = currentSession.createQuery(query).list();
		return processedMealLogs;
	}

	public Integer getMisfitBatteryStatusByPatient(long patientId){

		StringBuilder queryString = new StringBuilder();
		Integer batteryStatus = -1;
		try{

			queryString.append(" SELECT mal.battery_status, mal.`timestamp` ");
			queryString.append(" FROM ( SELECT MAX(m.`timestamp`) AS t FROM misfit_activity_log m WHERE m.patient_id = ?) inn " );
			queryString.append(" JOIN misfit_activity_log mal ON mal.`timestamp` = inn.t ");
			queryString.append(" WHERE mal.patient_id = ? LIMIT 1 ");

			Object result = currentSession.createSQLQuery(queryString.toString()).setParameter(0, patientId).setParameter(1, patientId).uniqueResult();

			if(!ObjectUtils.isEmpty(ObjectUtils.nullSafe(result))){
				Object[] objArray = (Object[])result;
				batteryStatus = (Integer)objArray[0];
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}

		return batteryStatus;
	}
}
