package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.MedicationMaster;

import java.util.ArrayList;
import java.util.List;

public class MedicationMasterDAO extends BaseDAO {

	public List<MedicationMaster> list(){
		List<MedicationMaster> medicationMasters = new ArrayList<MedicationMaster>();
		
		try {

            medicationMasters  = (List<MedicationMaster>)currentSession.createQuery(" FROM MedicationMaster ORDER BY name").list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return medicationMasters;
	}
}
