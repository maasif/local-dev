package com.healthslate.patientapp.model.dao;


import com.healthslate.patientapp.model.entity.MemberKeyFacts;
import com.healthslate.patientapp.model.entity.User;

import java.util.ArrayList;
import java.util.List;

public class MemberKeyFactsDAO extends BaseDAO{

    public List<MemberKeyFacts> getAllMemberKeyFactsByPatient(long patientId){
        List<MemberKeyFacts> keyFacts = new ArrayList<MemberKeyFacts>();

        try {

            keyFacts  = (List<MemberKeyFacts>)currentSession.createQuery(" FROM MemberKeyFacts WHERE patient.patientId = ? ORDER BY timestamp DESC ").setParameter(0, patientId).list();

            if(keyFacts != null && keyFacts.size() > 0){
               for (MemberKeyFacts keyFact : keyFacts){
                   User user = keyFact.getProvider().getUser();
                   keyFact.setCoachName(user.getLastName() + ", " + user.getFirstName());
//                   keyFact.setTimeToDateString(DateUtils.getFormattedDate(keyFact.getTimestamp(), DateUtils.DATE_FORMAT));
                   keyFact.setTimeToDateString(keyFact.getTimestamp()+"");
               }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return keyFacts;
    }
}
