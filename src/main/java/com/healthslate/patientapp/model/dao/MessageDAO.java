package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.MessageGcmDTO;
import com.healthslate.patientapp.model.entity.Message;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.DateUtils;
import com.healthslate.patientapp.util.ObjectUtils;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MessageDAO extends BaseDAO{

	static final Logger LOG = LoggerFactory.getLogger(MessageDAO.class);

	@SuppressWarnings("unchecked")
	public List<String> getFavoriteQuestions(Long providerId) {

		List<String> favoriteQuestions = new ArrayList<String>();

		try{
			Query query = currentSession.createSQLQuery(  " SELECT m.message "
					+ " FROM message m "
					+ " WHERE m.user_id = ( SELECT p.user_id FROM provider p where p.provider_id = " + providerId + " ) "
					+ " GROUP BY m.message "
					+ " ORDER BY COUNT(*) DESC "
					+ " LIMIT 10;");

			favoriteQuestions = query.list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return  favoriteQuestions;
	}

	@SuppressWarnings("unchecked")
	public List<Message> getAllMessages(Provider provider, long patientId, boolean setRead) {
		List<Message> messages = null;

		try{
			messages = (List<Message>)currentSession.createQuery("SELECT msg " +
					"FROM Message msg " +
					"WHERE msg.user.userId " +
					"IN (SELECT user.userId FROM Provider WHERE providerId = ?)" +
					"AND msg.patient.patientId " +
					"IN (SELECT patientId FROM Patient WHERE patientId = ?) ORDER BY msg.timestamp ASC")
					.setParameter(0, provider.getProviderId())
					.setParameter(1, patientId).list();

			if(setRead){
				String query = "UPDATE message m SET m.read_status = 1 WHERE m.patient_id = "+patientId+" AND m.user_id = " + provider.getUser().getUserId();
				currentSession.createSQLQuery(query).executeUpdate();
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return  messages;
	}

	@SuppressWarnings("unchecked")
	public List<Message> getAllMessagesOfAllCoachesByDateRange(long patientId, Long startDate, Long endDate) {
		List<Message> messages = null;

		try{
			messages = (List<Message>)currentSession.createQuery("SELECT msg " +
					" FROM Message msg " +
					" WHERE msg.patient.patientId " +
					" IN (SELECT patientId FROM Patient WHERE patientId = ?) " +
					" AND msg.timestamp >= " + startDate +
					" AND msg.timestamp < " + endDate +
					" AND msg.user.userId IS NOT NULL "+
					" ORDER BY msg.timestamp DESC ")
					.setParameter(0, patientId).list();

			for ( Message message: messages ) {

				User messageUser = message.getUser();
				Patient messagePatient = message.getPatient();
				User messagePatientUser = messagePatient.getUser();

				//setting coach name
				if ( messageUser.getFirstName() != null && messageUser.getLastName() != null ) {
					message.setProviderName(messageUser.getFirstName() + " " + messageUser.getLastName());
				}

				//setting patient/member name
				if ( messagePatientUser.getFirstName() != null && messagePatientUser.getLastName() != null ) {
					message.setPatientName(messagePatientUser.getFirstName() + " " + messagePatientUser.getLastName());
				}

				//setting isMealMessage true/false if image found or any log have summary added
				if ( (message.getImagePath() != null && message.getImagePath().length() > 0) || message.getFoodLogSummary() != null ) {
					message.setIsMealMessage(true);
				} else {
					message.setIsMealMessage(false);
				}

				//setting patient/member profile image path
				if ( messagePatient.getImagePath() != null && messagePatient.getImagePath().length() > 0 ) {
					message.setPatientProfileImage(messagePatient.getImagePath());
				}
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}

		return messages;
	}

	public List<Message> getAllMessagesByDate(List<Long> providerIds, long patientId) {
		List<Message> messages = null;
		try{
			messages = (List<Message>)currentSession.createQuery("SELECT msg " +
					"FROM Message msg " +
					"WHERE msg.user.userId " +
					"IN (SELECT user.userId FROM Provider WHERE providerId IN (:providerIds))" +
					"AND msg.patient.patientId " +
					"IN (SELECT patientId FROM Patient WHERE patientId = ?) ORDER BY msg.timestamp ASC")
					.setParameter("providerIds", providerIds)
					.setParameter(1, patientId).list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return  messages;
	}

	@SuppressWarnings("unchecked")
	public List<MessageGcmDTO> getAllMessagesByDate(long patientId, Long dateFrom, Long dateTo) {
		List<MessageGcmDTO> messages = new ArrayList<MessageGcmDTO>();
		List<Object> objectList = new ArrayList<Object>();

		try{
			String queryString = "SELECT msg.*, pp.provider_id, f.log_id " +
					"FROM message msg " +
					"LEFT JOIN users u ON u.user_id = msg.user_id " +
					"LEFT JOIN provider pp ON u.user_id = pp.user_id " +
					"LEFT JOIN food_log_summary f ON f.food_log_summary_id = msg.food_log_summary_id " +
					"WHERE msg.timestamp >= "+  dateFrom + " AND msg.timestamp <= "+ dateTo +" "+
					"AND msg.patient_id = " + patientId +" " +
					"AND pp.type <> '" + AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue() +"' " +
//					"AND (msg.is_error = 0 OR msg.is_error is NULL)" +
					"GROUP BY msg.message_id " +
					"ORDER BY msg.timestamp ASC";
			objectList = (List<Object>) currentSession.createSQLQuery(queryString).list();

			if(objectList != null) {
				for(Object obj: objectList){
					MessageGcmDTO dto = new MessageGcmDTO();
					Object[] objArray = (Object[]) obj;
					// dto.setMessageId(((BigInteger)objArray[0]).longValue());
					String messageContent = ObjectUtils.nullSafe((String)objArray[1]);
					boolean isError = ObjectUtils.nullSafeBoolean(objArray[10]);
					if(isError){
						messageContent = replaceMsgString(messageContent);
					}
					dto.setMessageText(messageContent);
					dto.setOwner(ObjectUtils.nullSafe((String)objArray[2]));
					dto.setIsRead((Boolean)objArray[3]);
					dto.setTimeStamp(((BigInteger) objArray[4]).longValue());
					dto.setPatientId(((BigInteger)objArray[6]).longValue());
					dto.setImagePath(ObjectUtils.nullSafe((String)objArray[8]));
					dto.setProviderId(((BigInteger)objArray[11]).longValue());
					dto.setLogId(ObjectUtils.nullSafe((String)objArray[12]));
					messages.add(dto);
				}
			}

		} catch(Exception ex){
			ex.printStackTrace();
			LOG.info("getAllMessagesByDate method: exception: " + ex.getMessage());
		}
		return  messages;
	}

	@SuppressWarnings("unchecked")
	public List<MessageGcmDTO> getAllPatientProviderMessages(long patientId) {
		List<MessageGcmDTO> messages = new ArrayList<MessageGcmDTO>();
		List<Object> objectList = new ArrayList<Object>();

		try{
			String queryString = "SELECT msg.*, pp.provider_id, f.log_id" +
					" FROM message msg   " +
					" LEFT JOIN users u ON u.user_id = msg.user_id " +
					" LEFT JOIN provider pp ON u.user_id = pp.user_id  " +
					"LEFT JOIN food_log_summary f ON f.food_log_summary_id = msg.food_log_summary_id " +
					" WHERE msg.patient_id =  "+  patientId +
					" AND pp.type <>  '"+ AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue() + "'" +
					" GROUP BY msg.message_id " +
					" ORDER BY msg.timestamp ASC";
			objectList = (List<Object>) currentSession.createSQLQuery(queryString).list();

			if(objectList != null) {
				for(Object obj: objectList){
					MessageGcmDTO dto = new MessageGcmDTO();
					Object[] objArray = (Object[]) obj;
					// dto.setMessageId(((BigInteger)objArray[0]).longValue());
					String messageContent = ObjectUtils.nullSafe((String)objArray[1]);
					boolean isError = ObjectUtils.nullSafeBoolean(objArray[10]);
					if(isError){
						messageContent = replaceMsgString(messageContent);
					}
					dto.setMessageText(messageContent);
					dto.setOwner(ObjectUtils.nullSafe((String)objArray[2]));
					dto.setIsRead((Boolean)objArray[3]);
					dto.setTimeStamp(((BigInteger) objArray[4]).longValue());
					dto.setPatientId(((BigInteger)objArray[6]).longValue());
					dto.setImagePath(ObjectUtils.nullSafe((String)objArray[8]));
					dto.setProviderId(((BigInteger)objArray[11]).longValue());
					dto.setLogId(ObjectUtils.nullSafe((String)objArray[12]));
					messages.add(dto);
				}
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return  messages;
	}

	public List<User> getAllProvidersByPatientId(long patientId) {
		List<User> providers = null;

		try {
			providers = (List<User>) currentSession.createQuery("SELECT msg.user " +
					"FROM Message msg " +
					"WHERE msg.patient.patientId = ? GROUP BY msg.user.userId")
					.setParameter(0, patientId).list();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return providers;
	}

	private String replaceMsgString(String messageContent){
		String newMsgContent = messageContent;
		int endIndex = messageContent.lastIndexOf("Click <a");
		if(endIndex > -1){
			newMsgContent = messageContent.substring(0, endIndex);
		}
		return  newMsgContent;
	}
}