

package com.healthslate.patientapp.model.dao;

import org.hibernate.Query;

public class MiscLogDAO extends BaseDAO{

	public boolean removeMiscLog(String logId) {
		
		Query query = currentSession.createSQLQuery(" DELETE " +
													" FROM misc_log " + 
													" WHERE log_id = '" + logId +"';");
						
		int result = query.executeUpdate();		
		return (result >= 0 )? true: false;
	}	

}
