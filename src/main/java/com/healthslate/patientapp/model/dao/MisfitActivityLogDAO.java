package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.MisfitActivityLog;

import java.util.List;

/**
 * Created by omursaleen on 8/5/2015.
 */
public class MisfitActivityLogDAO extends BaseDAO {

    public List<MisfitActivityLog> getMisfitLogsByPatientId(long patientId){
        List<MisfitActivityLog> misfitActivityLogs = null;

        try {
            misfitActivityLogs = (List<MisfitActivityLog>)currentSession.createQuery("FROM MisfitActivityLog WHERE patient.patientId = ?").setParameter(0, patientId).list();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return misfitActivityLogs;
    }
}
