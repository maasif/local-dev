package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.OneToOneReminderSessionDTO;
import com.healthslate.patientapp.model.entity.OneToOneSession;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.DateUtils;
import com.healthslate.patientapp.util.HibernateUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class OneToOneSessionsDAO extends BaseDAO{

    public OneToOneSession getOneToOneSessionsById(long sessionId){
        OneToOneSession oneToOneSession = null;

        try {

            oneToOneSession  = (OneToOneSession)currentSession.createQuery(" FROM OneToOneSession WHERE sessionId = ? ").setParameter(0, sessionId).uniqueResult();
            if(oneToOneSession != null){
                User userPatient = oneToOneSession.getPatient().getUser();
                oneToOneSession.setTimeToDateString(DateUtils.getFormattedDateInUserTimeZone(oneToOneSession.getSessionTime(), userPatient.getLogTimeOffset(), userPatient.getOffsetKey(), DateUtils.DATE_FORMAT));
                oneToOneSession.setOnlyTimeString(DateUtils.getFormattedDateInUserTimeZone(oneToOneSession.getSessionTime(), userPatient.getLogTimeOffset(), userPatient.getOffsetKey(), DateUtils.TIME_FORMAT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return oneToOneSession;
    }

    public OneToOneSession getLatestOneToOneSessionByPatient(long patientId){
        OneToOneSession session = new OneToOneSession();

        try {

            long currentTime = System.currentTimeMillis();

            List<OneToOneSession> sessions  = (List<OneToOneSession>)currentSession.createQuery(" FROM OneToOneSession WHERE patient.patientId = ? AND sessionTime >= ? ORDER BY sessionTime ASC ").setParameter(1, currentTime).setParameter(0, patientId).list();

            if(sessions != null && sessions.size() > 0){

                session = sessions.get(0);

                User user = session.getProvider().getUser();
                User userPatient = session.getPatient().getUser();

                session.setCoachName(user.getLastName() + ", " + user.getFirstName());
                session.setTimeToDateString(session.getSessionTime()+"");
                session.setCoachId(user.getProvider().getProviderId());
                session.setCoachType(user.getProvider().getType());
                long todaysStartTime = System.currentTimeMillis();
                if(session.getSessionTime() > 0 && session.getSessionTime() <= todaysStartTime){
                    session.setDisableEditing(true);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return session;
    }

    public List<OneToOneSession> getAllOneToOneSessionsByPatient(long patientId){
        List<OneToOneSession> oneToOneSessions = new ArrayList<OneToOneSession>();

        try {

            oneToOneSessions  = (List<OneToOneSession>)currentSession.createQuery(" FROM OneToOneSession WHERE patient.patientId = ? ORDER BY sessionTime DESC ").setParameter(0, patientId).list();

            if(oneToOneSessions != null && oneToOneSessions.size() > 0){
                for (OneToOneSession session : oneToOneSessions){
                    User user = session.getProvider().getUser();
                    User userPatient = session.getPatient().getUser();

                    session.setCoachName(user.getLastName() + ", " + user.getFirstName());
                    session.setTimeToDateString(session.getSessionTime()+"");
                    //session.setTimeToDateString(DateUtils.getFormattedDateInUserTimeZone(session.getSessionTime(), userPatient.getLogTimeOffset(), userPatient.getOffsetKey(), DateUtils.DATE_FORMAT));
                    //session.setOnlyTimeString(DateUtils.getFormattedDateInUserTimeZone(session.getSessionTime(), userPatient.getLogTimeOffset(), userPatient.getOffsetKey(), DateUtils.TIME_FORMAT));
                    session.setCoachId(user.getProvider().getProviderId());
                    session.setCoachType(user.getProvider().getType());

                    long todaysStartTime = System.currentTimeMillis();

                    if(session.getSessionTime() > 0 && session.getSessionTime() <= todaysStartTime){
                        session.setDisableEditing(true);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return oneToOneSessions;
    }

    public boolean updateOneToOneSession(long sessionTime, long patientId, String oldFirstSecond, String firstSecond){
        Session session = HibernateUtil.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.clear();

        String combined = firstSecond;
        try {

            if(!ObjectUtils.isEmpty(oldFirstSecond)){
                combined = oldFirstSecond + ", " + firstSecond;

                if(oldFirstSecond.contains(AppConstants.PreferencesNames.FIRST_ONE_TO_ONE_SESSION.getValue())
                        && oldFirstSecond.contains(AppConstants.PreferencesNames.SECOND_ONE_TO_ONE_SESSION.getValue())){

                    combined = AppConstants.PreferencesNames.FIRST_ONE_TO_ONE_SESSION.getValue() + ", " + AppConstants.PreferencesNames.SECOND_ONE_TO_ONE_SESSION.getValue();
                }
            }

            Query query = session.createSQLQuery(" UPDATE " +
                                                 " one_to_one_sessions " +
                                                 " SET reminders_sent = '"+combined+"' WHERE patient_id = " + patientId +" AND session_time = "+sessionTime+";");

            int result = query.executeUpdate();

            transaction.commit();

            if ( result >= 0 ) {
                return true;
            }

        }catch(Exception e){
            e.printStackTrace();
            transaction.rollback();
        }
        return false;
    }

    public List<OneToOneReminderSessionDTO> getOneToOneSessionRemindersList(String todayOrTomorrowReminder){
        long now = System.currentTimeMillis();
        long startDate = now;

        List<OneToOneReminderSessionDTO> oneToOneSessions = new ArrayList<OneToOneReminderSessionDTO>();

        Session session = HibernateUtil.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.clear();
        List<Object> results = null;

        String queryUpdate = "";

        if(todayOrTomorrowReminder.equalsIgnoreCase(AppConstants.PreferencesNames.SECOND_ONE_TO_ONE_SESSION.getValue())){//if tomorrow reminder
//            startDate = DateUtils.addDayInTime(now, 1); //starting from tomorrow
            queryUpdate = " AND (o2o.reminders_sent IS NULL ) "; //reminder not already sent to its null
        }else{
            queryUpdate = " AND ( o2o.reminders_sent IS NULL  OR o2o.reminders_sent NOT LIKE '%" + todayOrTomorrowReminder + "%') ";
        }

        try {

            String query = "SELECT " +
                    " p.patient_id, " +
                    " u.first_name, " +
                    " u.email, " +
                    " u.phone, " +
                    " innerQuery.sessionTime, innerQuery.fullName, innerQuery.providerEmail, innerQuery.reminderSent, u.log_time_offset, u.offset_key, innerQuery.sessionID, " +
                    " f.timezone_offset_millis " +
                    "FROM " +
                    " patient p " +
                    "LEFT JOIN users u ON u.user_id = p.user_id " +
                    "LEFT JOIN patient_facility pf ON pf.patients_patient_id = p.patient_id " +
                    "LEFT JOIN facility f ON f.facility_id = pf.facilities_facility_id " +
                    "JOIN ( " +
                    " SELECT " +
                    "  o2o.patient_id AS pID, o2o.session_id AS sessionID, " +
                    "  MIN(o2o.session_time) AS sessionTime, u2.email as providerEmail, CONCAT(u2.first_name, ' ', u2.last_name) as fullName, o2o.reminders_sent AS reminderSent " +
                    " FROM " +
                    "  one_to_one_sessions o2o " +
                    " LEFT JOIN provider pro ON pro.provider_id = o2o.provider_id " +
                    " LEFT JOIN users u2 ON u2.user_id = pro.user_id " +
                    " WHERE " +
                    "  DATE_FORMAT(FROM_UNIXTIME(o2o.session_time / 1000), '%m/%d/%Y') >= DATE_FORMAT(NOW(), '%m/%d/%Y')  " +
                    " AND ( o2o.session_time >= " + startDate + " ) AND ( o2o.session_time <= " + (todayOrTomorrowReminder.equalsIgnoreCase(AppConstants.PreferencesNames.SECOND_ONE_TO_ONE_SESSION.getValue()) ? DateUtils.addMinutesInTime(DateUtils.addDayInTime(now, 1), 5) : DateUtils.addMinutesInTime(now, 5) ) + " )" +
                    queryUpdate +
                    " GROUP BY " +
                    "  o2o.patient_id " +
                    ") innerQuery ON p.patient_id = innerQuery.pID " +
                    "WHERE " +
                    " ( " +
                    "  p.is_deleted = 0 " +
                    "  OR p.is_deleted IS NULL " +
                    " ) " +
                    "GROUP BY " +
                    " p.patient_id";

            results = (List<Object>)session.createSQLQuery(query).list();

            transaction.commit();

            if(results != null && results.size() > 0){
                for (Object object : results) {

                    if(object != null){
                        Object[] objArray = (Object[]) object;

                        long patientId = ObjectUtils.nullSafeLong(objArray[0]);
                        String firstName = ObjectUtils.nullSafe(objArray[1]);
                        String email = ObjectUtils.nullSafe(objArray[2]);
                        String phone = ObjectUtils.nullSafe(objArray[3]);
                        long sessionTime = ObjectUtils.nullSafeLong(objArray[4]);
                        String coachName = ObjectUtils.nullSafe(objArray[5]);
                        String coachEmail = ObjectUtils.nullSafe(objArray[6]);
                        String reminderSent = ObjectUtils.nullSafe(objArray[7]);
                        long offsetTime = ObjectUtils.nullSafeLong(objArray[8]);
                        String offsetKey = ObjectUtils.nullSafe(objArray[9]);
                        long sessionId = ObjectUtils.nullSafeLong(objArray[10]);
                        long facilityOffsetTime;
                        if ( objArray[11] == null ) {
                            facilityOffsetTime = offsetTime;
                        }
                        else {
                            facilityOffsetTime = ObjectUtils.nullSafeLong(objArray[11]);
                        }

                        oneToOneSessions.add(new OneToOneReminderSessionDTO(patientId, firstName, email, phone, sessionTime, coachName, coachEmail, reminderSent, offsetTime, offsetKey, sessionId, facilityOffsetTime));
                    }
                }
            }

        }catch(Exception e){
            e.printStackTrace();
            transaction.rollback();
        }

        return oneToOneSessions;
    }
}
