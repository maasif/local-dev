package com.healthslate.patientapp.model.dao;

import org.hibernate.Query;

public class PatientCurriculumDAO extends BaseDAO {

    public boolean removeCurriculumByPatientId(long patientId) {

        Query query = currentSession.createSQLQuery(" DELETE " +
                                                    " FROM patient_curriculum " +
                                                    " WHERE patient_id = " + patientId +";");

        int result = query.executeUpdate();
        if ( result >= 0 ) {
            return true;
        }

        return false;
    }
}
