package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.CoachDTO;
import com.healthslate.patientapp.model.dto.MembersDataDTO;
import com.healthslate.patientapp.model.dto.PatientDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.DateUtils;
import com.healthslate.patientapp.util.HibernateUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.*;

import javax.persistence.Entity;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/*
* ======= FILE CHANGE HISTORY =======
* [1/29/2015]: Added getPatientByIdScheduler for getting patient by passing id, __oz
* [2/11/2015]: 1. changed .list() method to get patient by registrationDate DESC,
 *             2. added doesPatientAssignToProvider to check patient is assigned to provider, __oz
* ===================================
 */

public class PatientDAO extends BaseDAO{

	Session currentSession;

	public PatientDAO(){
		currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	public List<Patient> PatientList() {

		List<Patient> patients = new ArrayList<Patient>();
		try{
			patients = currentSession.createQuery("from Patient").list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return patients;
	}

	@SuppressWarnings("unchecked")
	public List<Patient> list() {
		List<Patient> patients = new ArrayList<Patient>();
		try{
            //[2/11/2015]: changed by __oz
			patients = currentSession.createQuery("SELECT p FROM Patient p JOIN p.user us WHERE (p.isDeleted = false OR p.isDeleted IS NULL) ORDER BY p.user.registrationDate DESC").list();
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return patients;
	}

	public List<MembersDataDTO> getMembersDataList(int start, int end, String sSearch, String sortIndex, String sSortDirection, long filterByFacility) {
		List<MembersDataDTO> membersDataDTOs = new ArrayList<MembersDataDTO>();

        String queryFacility = "";
        if(filterByFacility > 0){
            queryFacility = " AND f.facility_id = " + filterByFacility;
        }

		try{
			String query =  "SELECT p.patient_id, p.device_mac_address, us.first_name, us.last_name, us.registration_date,  "
					+ " COUNT(l.log_id) AS total_logs,  "
					+ " innerQuery.lastLogTime,  "
					+ " innerQuery.lastLogType,  "
					+ " us.email, d.device_type, p.uuid, f.`name`, us.address, us.city, us.state, us.phone, us.zip_code, d.device_manufacturer_os "
					+ " FROM  patient p "
                    + " LEFT JOIN users us ON us.user_id = p.user_id "
                    + " LEFT JOIN log l ON l.patient_id = p.patient_id  "
                    + " LEFT JOIN device d ON d.device_mac_address = p.device_mac_address "
                    + " LEFT JOIN patient_facility pf ON pf.patients_patient_id = p.patient_id "
                    + " LEFT JOIN facility f ON f.facility_id = pf.facilities_facility_id "
					+ "	LEFT JOIN (SELECT l.upload_time AS lastLogTime, l.patient_id AS patientID, l.log_type AS lastLogType FROM log l "
					+ " INNER JOIN (SELECT MAX(l.upload_time) AS uploadTime FROM log l WHERE l.is_removed = 0 " +
					"  GROUP BY l.patient_id " +
					" ) innerQuery1 ON innerQuery1.uploadTime = l.upload_time " +
					" GROUP BY l.patient_id  " +
					" ORDER BY l.patient_id DESC) AS innerQuery "
					+ "	ON p.patient_id = innerQuery.patientID  ";
			if(!sSearch.isEmpty()) {
				long timeStamp = DateUtils.getMillisecondsFromDateString(sSearch, DateUtils.DATE_FORMAT);
				long startOfDay = 0, endOfDay = 0;

				if (timeStamp > 0) {
					startOfDay = DateUtils.getStartOfDayTime(timeStamp);
					endOfDay = DateUtils.getEndOfDayTime(timeStamp);
					query += " WHERE (us.registration_date >= " + startOfDay + " AND us.registration_date <= " + endOfDay +
							" OR innerQuery.lastLogTime >= " + startOfDay + " AND innerQuery.lastLogTime <= "+endOfDay +")";
				} else {
					query += "WHERE (p.patient_id LIKE '%" + sSearch + "%'"  +
							" OR p.device_mac_address LIKE '%" + sSearch + "%'" +
							" OR us.first_name LIKE '%" + sSearch + "%'" +
							" OR us.last_name LIKE '%" + sSearch + "%'" +
							" OR innerQuery.lastLogType LIKE '%" + sSearch + "%'" +
                            " OR f.`name` LIKE '%" + sSearch + "%'" +
                            " OR d.device_type LIKE '%" + sSearch + "%'" +
                            " OR d.device_manufacturer_os LIKE '%" + sSearch + "%'" +
							" OR us.email LIKE '%" + sSearch + "%')";
				}
				query += " AND (l.is_removed = 0 OR l.is_removed IS NULL) AND (l.is_suggested = 0 OR l.is_suggested IS NULL) AND (p.is_deleted = 0 OR p.is_deleted IS NULL) ";
			} else {
				query += " WHERE (l.is_removed = 0 OR l.is_removed IS NULL) AND (l.is_suggested = 0 OR l.is_suggested IS NULL) AND (p.is_deleted = 0 OR p.is_deleted IS NULL) ";
			}

			query += queryFacility + " GROUP BY  p.patient_id ";
			if(!sortIndex.isEmpty()){
				query += " ORDER BY " + sortIndex + " " + sSortDirection;
			} else {
				query += "ORDER BY us.registration_date DESC ";
			}

			query += " LIMIT " + start + "," + end + "";

			List<Object> objects = currentSession.createSQLQuery(query).list();
			if(objects != null && objects.size() > 0) {
				for (Object object: objects) {
					Object[] objArray = (Object[]) object;
					MembersDataDTO membersDataDTO = new MembersDataDTO();
					membersDataDTO.setPatientId(ObjectUtils.nullSafeLong(objArray[0]));
					membersDataDTO.setMacAddress(ObjectUtils.nullSafe(objArray[1]));
					membersDataDTO.setFirstName(ObjectUtils.nullSafe(objArray[2]));
					membersDataDTO.setLastName(ObjectUtils.nullSafe(objArray[3]));
					String registrationDate = (objArray[4] == null) ? "" : DateUtils.getFormattedDate(((Date)objArray[4]).getTime(), DateUtils.DATE_FORMAT);
					membersDataDTO.setRegistrationDate(registrationDate);
					membersDataDTO.setTotalLogs(ObjectUtils.nullSafeInteger(objArray[5]));
					String lastLogTime = (objArray[6] == null) ? "" : DateUtils.getFormattedDate((((BigInteger)objArray[6]).longValue()), DateUtils.DATE_FORMAT);
                    String lastLogTimeMillis = (objArray[6] == null) ? "" : objArray[6]+"";
					membersDataDTO.setLastLogTime(lastLogTime);
                    membersDataDTO.setLastLogTimeMillis(lastLogTimeMillis);
					membersDataDTO.setLastLogType(ObjectUtils.nullSafe(objArray[7]));
					membersDataDTO.setEmail(ObjectUtils.nullSafe(objArray[8]));

                    String dType = ObjectUtils.nullSafe(objArray[9]);
                    String manufacturerOs = ObjectUtils.nullSafe(objArray[17]);

                    if(!ObjectUtils.isEmpty(manufacturerOs)){
                        dType = manufacturerOs;
                    }

                    membersDataDTO.setDeviceType(dType);

                    membersDataDTO.setUuid(ObjectUtils.nullSafe(objArray[10]));
                    membersDataDTO.setFacilityName(ObjectUtils.nullSafe(objArray[11]));

                    membersDataDTO.setAddress(ObjectUtils.nullSafe(objArray[12]));
                    membersDataDTO.setCity(ObjectUtils.nullSafe(objArray[13]));
                    membersDataDTO.setState(ObjectUtils.nullSafe(objArray[14]));
                    membersDataDTO.setPhone(ObjectUtils.nullSafe(objArray[15]));
                    membersDataDTO.setZip(ObjectUtils.nullSafe(objArray[16]));


                    membersDataDTOs.add(membersDataDTO);
				}
			}
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return membersDataDTOs;
	}

    public long getMembersDataListCount(String sSearch, long filterByFacility) {
        long resultCount = 0l;

        String queryFacility = "";
        if(filterByFacility > 0){
            queryFacility = " AND f.facility_id = " + filterByFacility;
        }

        try{
            String query =  "SELECT p.patient_id, p.device_mac_address, us.first_name, us.last_name, us.registration_date,  "
                    + " COUNT(l.log_id) AS total_logs,  "
                    + " innerQuery.lastLogTime,  "
                    + " innerQuery.lastLogType,  "
                    + " us.email, d.device_type, p.uuid, f.`name`, us.address, us.city, us.state, us.phone, us.zip_code "
                    + " FROM  patient p "
                    + " LEFT JOIN users us ON us.user_id = p.user_id "
                    + " LEFT JOIN log l ON l.patient_id = p.patient_id  "
                    + " LEFT JOIN device d ON d.device_mac_address = p.device_mac_address "
                    + " LEFT JOIN patient_facility pf ON pf.patients_patient_id = p.patient_id "
                    + " LEFT JOIN facility f ON f.facility_id = pf.facilities_facility_id "
                    + "	LEFT JOIN (SELECT l.upload_time AS lastLogTime, l.patient_id AS patientID, l.log_type AS lastLogType FROM log l "
                    + " INNER JOIN (SELECT MAX(l.upload_time) AS uploadTime FROM log l WHERE l.is_removed = 0 " +
                    "  GROUP BY l.patient_id " +
                    " ) innerQuery1 ON innerQuery1.uploadTime = l.upload_time " +
                    " GROUP BY l.patient_id  " +
                    " ORDER BY l.patient_id DESC) AS innerQuery "
                    + "	ON p.patient_id = innerQuery.patientID  ";
            if(!sSearch.isEmpty()) {
                long timeStamp = DateUtils.getMillisecondsFromDateString(sSearch, DateUtils.DATE_FORMAT);
                long startOfDay = 0, endOfDay = 0;

                if (timeStamp > 0) {
                    startOfDay = DateUtils.getStartOfDayTime(timeStamp);
                    endOfDay = DateUtils.getEndOfDayTime(timeStamp);
                    query += " WHERE (us.registration_date >= " + startOfDay + " AND us.registration_date <= " + endOfDay +
                            " OR innerQuery.lastLogTime >= " + startOfDay + " AND innerQuery.lastLogTime <= "+endOfDay +")";
                } else {
                    query += "WHERE (p.patient_id LIKE '%" + sSearch + "%'"  +
                            " OR p.device_mac_address LIKE '%" + sSearch + "%'" +
                            " OR us.first_name LIKE '%" + sSearch + "%'" +
                            " OR us.last_name LIKE '%" + sSearch + "%'" +
                            " OR innerQuery.lastLogType LIKE '%" + sSearch + "%'" +
                            " OR f.`name` LIKE '%" + sSearch + "%'" +
                            " OR us.email LIKE '%" + sSearch + "%')";
                }
                query += " AND (l.is_removed = 0 OR l.is_removed IS NULL) AND (l.is_suggested = 0 OR l.is_suggested IS NULL) AND (p.is_deleted = 0 OR p.is_deleted IS NULL) ";
            } else {
                query += " WHERE (l.is_removed = 0 OR l.is_removed IS NULL) AND (l.is_suggested = 0 OR l.is_suggested IS NULL) AND (p.is_deleted = 0 OR p.is_deleted IS NULL) ";
            }

            query += queryFacility + " GROUP BY  p.patient_id ";

            query += "ORDER BY us.registration_date DESC ";

            List<Object> objects = currentSession.createSQLQuery(query).list();
            resultCount = objects.size();
        } catch(Exception ex){
            ex.printStackTrace();
        }
        return resultCount;
    }

    public int getMembersDataListCount(long filterByFacility) {
        int count = 0;
        String queryFacility = "";
        if(filterByFacility > 0){
            queryFacility = " AND f.facility_id = " + filterByFacility;
        }

        try{
            String query =  "SELECT p.patient_id, p.device_mac_address, us.first_name, us.last_name, us.registration_date,  "
                    + " COUNT(l.log_id) AS total_logs,  "
                    + " innerQuery.lastLogTime,  "
                    + " innerQuery.lastLogType,  "
                    + " us.email, d.device_type, p.uuid, f.`name` "
                    + " FROM  patient p "
                    + " LEFT JOIN users us ON us.user_id = p.user_id "
                    + " LEFT JOIN log l ON l.patient_id = p.patient_id  "
                    + " LEFT JOIN device d ON d.device_mac_address = p.device_mac_address "
                    + " LEFT JOIN patient_facility pf ON pf.patients_patient_id = p.patient_id "
                    + " LEFT JOIN facility f ON f.facility_id = pf.facilities_facility_id "
                    + "	LEFT JOIN (SELECT l.upload_time AS lastLogTime, l.patient_id AS patientID, l.log_type AS lastLogType FROM log l "
                    + " INNER JOIN (SELECT MAX(l.upload_time) AS uploadTime FROM log l WHERE l.is_removed = 0 " +
                    "  GROUP BY l.patient_id " +
                    " ) innerQuery1 ON innerQuery1.uploadTime = l.upload_time " +
                    " GROUP BY l.patient_id  " +
                    " ORDER BY l.patient_id DESC) AS innerQuery "
                    + "	ON p.patient_id = innerQuery.patientID  ";

                query += " WHERE (l.is_removed = 0 OR l.is_removed IS NULL) AND (l.is_suggested = 0 OR l.is_suggested IS NULL) AND (p.is_deleted = 0 OR p.is_deleted IS NULL) ";
                query += queryFacility + " GROUP BY p.patient_id ";
                query += "ORDER BY us.registration_date DESC ";

            List<Object> objects = currentSession.createSQLQuery(query).list();
            if(objects != null && objects.size() > 0){
                count = objects.size();
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }

        return count;
    }

	public int count() {
		Object countObj = null;
		int countInt = 0;
		try {
			String query  = "SELECT COUNT(*) FROM patient p ";
			countObj = currentSession.createSQLQuery(query).uniqueResult();
			if(countObj != null){
				countInt = Integer.parseInt(countObj.toString());
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			countInt = 0;
		}
		return countInt;
	}

    public List<Patient> listPendingInvites() {
        List<Patient> patients = new ArrayList<Patient>();
        try {
            patients = currentSession.createQuery("SELECT p FROM Patient p JOIN p.user WHERE p.user.isRegistrationCompleted = 0 AND (p.isArchived Is NULL OR p.isArchived = 0 OR p.isArchived = false) AND (p.isDeleted Is NULL OR p.isDeleted = 0 OR p.isDeleted = false)  ORDER BY p.invitationSentDate DESC, p.requestReceivedDate DESC").list();
        } catch(Exception ex){
            ex.printStackTrace();
        }
        return patients;
    }

    public List<PatientDTO> getProspectiveMembersList(int start, int end, String sSearch, String sortIndex, String sSortDirection, long facilityId){
        List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

        try{
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("SELECT " +
                    " p.patient_id as patientId, " +
                    " p.uuid as uuid, " +
                    " u.email as email, " +
                    " u.phone as phone, " +
                    " u.first_name as firstName, " +
                    " u.last_name as lastName, " +
                    " p.invitation_sent_date as invitedDate, " +
                    " p.request_received_date as requestAnInvitedDate, " +
                    " p.use_insulin as useInsulin, " +
                    " p.is_consent_accepted as isConsentAccepted, " +
                    " p.is_approved as isApproved, " +
                    " p.mrn as mrn, " +
                    " f.`name` as facilityName, f.lead_coach as facilityLeadCoach, f.facility_id as facilityId" +
                    " FROM " +
                    " patient p " +
                    "LEFT JOIN users u ON u.user_id = p.user_id " +
                    "LEFT JOIN patient_facility pf ON pf.patients_patient_id = p.patient_id " +
                    "LEFT JOIN facility f ON f.facility_id = pf.facilities_facility_id " +
                    "WHERE " +
                    " u.is_registration_completed = 0 " +
                    "AND ( " +
                    " p.is_archived = 0 " +
                    " OR p.is_archived IS NULL " +
                    ") " +
                    "AND ( " +
                    " p.is_deleted = 0 " +
                    " OR p.is_deleted IS NULL " +
                    ") " );

            if(!sSearch.isEmpty()){
                stringBuilder.append(" AND (p.patient_id LIKE '%"+sSearch+"%'" +
                        " OR p.uuid LIKE '%"+sSearch+"%'" +
                        " OR p.mrn LIKE '%"+sSearch+"%'" +
                        " OR f.`name` LIKE '%"+sSearch+"%'" +
                        " OR u.first_name LIKE '%"+sSearch+"%'" +
                        " OR u.last_name LIKE '%"+sSearch+"%'" +
                        " OR u.email LIKE '%"+sSearch+"%'" +
                        " OR u.phone LIKE '%"+sSearch+"%')");
            }

            if(facilityId > 0){
                stringBuilder.append(" AND f.facility_id =" + facilityId + " ");
            }

            stringBuilder.append(" GROUP BY p.patient_id");
            if(!sortIndex.isEmpty()){
                stringBuilder.append(" ORDER BY " + sortIndex + " " + sSortDirection);
            } else {
                stringBuilder.append(" ORDER BY p.invitation_sent_date DESC, p.request_received_date ");
            }
            stringBuilder.append(" LIMIT " + start + "," + end + "");

            List result = currentSession.createSQLQuery(stringBuilder.toString())
                    .addScalar("patientId", LongType.INSTANCE)
                    .addScalar("uuid", StringType.INSTANCE)
                    .addScalar("email", StringType.INSTANCE)
                    .addScalar("phone", StringType.INSTANCE)
                    .addScalar("firstName", StringType.INSTANCE)
                    .addScalar("lastName", StringType.INSTANCE)
                    .addScalar("invitedDate", StringType.INSTANCE)
                    .addScalar("requestAnInvitedDate", StringType.INSTANCE)
                    .addScalar("useInsulin", BooleanType.INSTANCE)
                    .addScalar("isConsentAccepted", BooleanType.INSTANCE)
                    .addScalar("isApproved", StringType.INSTANCE)
                    .addScalar("mrn", StringType.INSTANCE)
                    .addScalar("facilityName", StringType.INSTANCE)
                    .addScalar("facilityLeadCoach", LongType.INSTANCE)
                    .addScalar("facilityId", LongType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(PatientDTO.class))
                    .list();

            patientDTOs = (List<PatientDTO>) result;

        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return patientDTOs;
    }

    public long getProspectiveMembersListCount(String sSearch, long facilityId){
        long resultCount = 0l;

        try{
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("SELECT " +
                    " p.patient_id, " +
                    " p.uuid, " +
                    " u.email, " +
                    " u.phone, " +
                    " u.first_name, " +
                    " u.last_name, " +
                    " p.invitation_sent_date, " +
                    " p.request_received_date, " +
                    " p.use_insulin, " +
                    " p.is_consent_accepted, " +
                    " p.is_approved, " +
                    " p.mrn, " +
                    " f.`name` " +
                    "FROM " +
                    " patient p " +
                    "LEFT JOIN users u ON u.user_id = p.user_id " +
                    "LEFT JOIN patient_facility pf ON pf.patients_patient_id = p.patient_id " +
                    "LEFT JOIN facility f ON f.facility_id = pf.facilities_facility_id " +
                    "WHERE " +
                    " u.is_registration_completed = 0 " +
                    "AND ( " +
                    " p.is_archived = 0 " +
                    " OR p.is_archived IS NULL " +
                    ") " +
                    "AND ( " +
                    " p.is_deleted = 0 " +
                    " OR p.is_deleted IS NULL " +
                    ") " );

            if(!sSearch.isEmpty()){
                stringBuilder.append(" AND (p.patient_id LIKE '%"+sSearch+"%'" +
                        " OR p.uuid LIKE '%"+sSearch+"%'" +
                        " OR p.mrn LIKE '%"+sSearch+"%'" +
                        " OR f.`name` LIKE '%"+sSearch+"%'" +
                        " OR u.first_name LIKE '%"+sSearch+"%'" +
                        " OR u.last_name LIKE '%"+sSearch+"%'" +
                        " OR u.email LIKE '%"+sSearch+"%'" +
                        " OR u.phone LIKE '%"+sSearch+"%')");

            }

            if(facilityId > 0){
                stringBuilder.append(" AND f.facility_id =" + facilityId + " ");
            }

            stringBuilder.append(" GROUP BY p.patient_id");
            stringBuilder.append(" ORDER BY p.invitation_sent_date DESC, p.request_received_date ");

            List<Object> result = currentSession.createSQLQuery(stringBuilder.toString()).list();
            resultCount = result.size();
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return resultCount;
    }

    @SuppressWarnings("unchecked")
	public List<Object> getPatients() {

		List<Object> patients = new ArrayList<Object>();
		StringBuilder queryString = new StringBuilder();

		try{
			queryString.append("SELECT p.patient_id, u.email, u.phone "
					+ " FROM patient p "
					+ " JOIN users u ON p.user_id = u.user_id "
					+ " JOIN provider_patient pp ON p.patient_id = pp.patients_patient_id "
					+ " JOIN provider pro ON pp.providers_provider_id = pro.provider_id ");

			patients = currentSession.createSQLQuery(queryString.toString()).list();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return patients;
	}

	public Patient getPatientByUserId(long userId) {

		Patient patient = null;
		try{
			patient = (Patient) currentSession.createQuery("Select p FROM Patient p JOIN p.user u WHERE u.userId = ?")
				.setParameter(0, userId)
				.uniqueResult();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return patient;
	}

	public Patient getPatientById(long patientId) {

		Patient patient = null;
		try{
			patient = (Patient) currentSession.createQuery("FROM Patient WHERE patientId = ?")
				.setParameter(0, patientId)
				.uniqueResult();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return patient;
	}

    public Patient getPatientByUUID(String uuid) {

        Patient patient = null;
        try{
            patient = (Patient) currentSession.createQuery("FROM Patient WHERE uuid = ?")
                    .setParameter(0, uuid)
                    .uniqueResult();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return patient;
    }

	public Patient getPatientByMacAddress(String macAddress) {

		Patient patient = null;
		try{

			patient = (Patient) currentSession.createQuery("FROM Patient where deviceMacAddress = ?")
				.setParameter(0, macAddress)
				.uniqueResult();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return patient;
	}

	public String getMacAddressByPatientId(long patientId) {

		String macAddress = null;
		try{

			macAddress = (String) currentSession.createQuery("SELECT deviceMacAddress FROM Patient p where p.patientId = ?")
				.setParameter(0, patientId)
				.uniqueResult();

			return macAddress;
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return "";
	}

	public String getMacAddressByPatientIdScheduler(long patientId) {

		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = null;

		if(session.getTransaction() != null && session.getTransaction().isActive()){
			transaction = session.getTransaction();
		}else{
			transaction = session.beginTransaction();
		}

		session.clear();

		String macAddress = null;
		try{

			macAddress = (String) session.createQuery("SELECT deviceMacAddress FROM Patient p where p.patientId = ?")
				.setParameter(0, patientId)
				.uniqueResult();

			transaction.rollback();
			return macAddress;

		}catch(Exception ex){
			ex.printStackTrace();
			transaction.rollback();
		}

		return "";
	}

	//[1/29/2015]: added by __oz
	public Patient getPatientByIdScheduler(long patientId) {

		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = null;

		if(session.getTransaction() != null && session.getTransaction().isActive()){
			transaction = session.getTransaction();
		}else{
			transaction = session.beginTransaction();
		}

		session.clear();

		Patient patient = null;
		try{
			patient = (Patient) session.createQuery("FROM Patient WHERE patientId = ?")
					.setParameter(0, patientId)
					.uniqueResult();

			transaction.rollback();
		}
		catch(Exception ex){
			ex.printStackTrace();
			transaction.rollback();
		}
		return patient;
	}

	public Patient getPatientByUserIdScheduler(long userId) {

		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = null;

		if(session.getTransaction() != null && session.getTransaction().isActive()){
			transaction = session.getTransaction();
		}else{
			transaction = session.beginTransaction();
		}
		session.clear();
		Patient patient = null;
		try{
			patient = (Patient) currentSession.createQuery("Select p FROM Patient p JOIN p.user u WHERE u.userId = ?")
					.setParameter(0, userId)
					.uniqueResult();
			transaction.commit();
		}
		catch(Exception ex){
			ex.printStackTrace();
			transaction.rollback();
		}
		return patient;
	}

    //[2/11/2015]: added by __oz
    public boolean doesPatientAssignToProvider(long providerId, long patientId, String logId){

        Log log = new LogDAO().getLogById(logId);
        List<Long> providerIds = new ArrayList<Long>();

        if(log != null){
            Patient patient = log.getPatient();
            if(patient != null){

                if(patientId > 0){
                    if(patientId != patient.getPatientId()){
                        return false;
                    }
                }

                List<Provider> providers = patient.getProviders();
                if(providers != null && providers.size() > 0){
                    for (Provider provider : providers){
                        if(provider.getProviderId() == providerId){
                            providerIds.add(provider.getProviderId());
                        }
                    }
                }
            }
            return (providerIds.size() > 0) ? true : false;
        }

       return true;
    }

    //[2/11/2015]: added by __oz
    public boolean doesPatientAssignToProvider(long providerId, long patientId){

        Patient patient = getPatientById(patientId);
        List<Long> providerIds = new ArrayList<Long>();
        if(patient != null){
            List<Provider> providers = patient.getProviders();
            if(providers != null && providers.size() > 0){
                for (Provider provider : providers){
                    if(provider.getProviderId() == providerId){
                        providerIds.add(provider.getProviderId());
                    }
                }
            }
        }

        return (providerIds.size() > 0) ? true: false;
    }

    public List<PatientDTO> getPatientListByCoach(int start, int end, String sSearch, String sortIndex, String sSortDirection, Provider provider, String filterByFoodCoach) {
        List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();
        Provider defaultTS = new ProviderDAO().getDefaultTechSupport();
        String queryMyMembers = "";
        if (!ObjectUtils.isEmpty(filterByFoodCoach) && filterByFoodCoach.equalsIgnoreCase("filterByPrimaryFoodCoach")) {
            queryMyMembers = " AND p.primary_food_coach_id = " + provider.getProviderId();
        } else if (!ObjectUtils.isEmpty(filterByFoodCoach) && filterByFoodCoach.equalsIgnoreCase("filterByLeadCoach")) {
            queryMyMembers = " AND p.lead_coach_id = " + provider.getProviderId();
        }
        User user = provider.getUser();
        String lastContactSentQuery = " LEFT JOIN ( SELECT p.patient_id AS pId, MAX(m.`timestamp`) AS lastContactDate, COUNT(*) AS unreadCount FROM patient p LEFT JOIN message m ON m.patient_id = p.patient_id WHERE m.user_id = "+user.getUserId()+" AND m.`owner` = 'PROVIDER' GROUP BY p.patient_id ORDER BY MAX(m.`timestamp`) ASC ) innerQueryLastContact ON p.patient_id = innerQueryLastContact.pId ";
        String lastContactReceivedQuery = " LEFT JOIN ( SELECT p.patient_id AS pId, MAX(m.`timestamp`) AS lastContactReceivedDate, COUNT(*) AS unreadCount FROM patient p LEFT JOIN message m ON m.patient_id = p.patient_id WHERE m.user_id = "+user.getUserId()+" AND m.`owner` = 'PATIENT' GROUP BY p.patient_id ORDER BY MAX(m.`timestamp`) ASC ) innerQueryLastContactReceived ON p.patient_id = innerQueryLastContactReceived.pId ";
        String primaryFoodCoachQuery = " LEFT JOIN ( SELECT p.primary_food_coach_id AS primaryFoodCoachId, userP.first_name AS primFirstName, userP.last_name AS primLastName FROM patient p LEFT JOIN provider prov ON prov.provider_id = p.primary_food_coach_id LEFT JOIN users userP ON prov.user_id = userP.user_id ORDER BY p.primary_food_coach_id) innerQueryPrimary ON p.primary_food_coach_id = innerQueryPrimary.primaryFoodCoachId ";
        String leadCoachQuery = " LEFT JOIN (SELECT p.lead_coach_id AS leadCoachId, userL.first_name AS leadFirstName, userL.last_name AS leadLastName FROM patient p LEFT JOIN provider prov ON prov.provider_id = p.lead_coach_id LEFT JOIN users userL ON prov.user_id = userL.user_id ORDER BY p.lead_coach_id) innerQueryLead ON p.lead_coach_id = innerQueryLead.leadCoachId ";
        
        try {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("SELECT " +
                    " p.patient_id, " +
                    " u.first_name, " +
                    " u.last_name, " +
                    " u.display_name, " +
                    " u.email, " +
                    " u.phone, " +
                    " innerQueryLastContact.unreadCount, " +
                    " p.image_path, " +
                    " CONCAT(innerQueryPrimary.primFirstName, ' ' , innerQueryPrimary.primLastName) AS primaryFoodCoachName, " +
                    " innerQuery2.sessionTime, " +
                    " innerQuery4.lastLogTime, " +
                    " innerQuery4.lastLogType, innerQuery5.time, u.address, u.city, u.state, u.zip_code, u.log_time_offset, u.offset_key, innerQuery2.coachName, innerQuery5.notesCoachName, " +
                    " ROUND((DATEDIFF(NOW(),u.registration_date))/7) AS registeredWeeks, " +
                    " CONCAT(innerQueryLead.leadFirstName, ' ' , innerQueryLead.leadLastName) AS leadCoachName, " +
                    " innerQueryLastContact.lastContactDate, innerQueryLastContactReceived.lastContactReceivedDate " +
                    " FROM patient p " +
                    " LEFT JOIN provider_patient pp ON p.patient_id = pp.patients_patient_id " +
                    " LEFT JOIN users u ON u.user_id = p.user_id ");

            if (provider.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())) {
                lastContactSentQuery = " LEFT JOIN ( SELECT p.patient_id AS pId, MAX(m.`timestamp`) AS lastContactDate, COUNT(*) AS unreadCount FROM patient p LEFT JOIN tech_support_message m ON m.patient_id = p.patient_id WHERE (m.user_id = "+defaultTS.getUser().getUserId()+" OR m.replied_by = "+user.getUserId()+") AND m.`owner` = 'Tech Support' GROUP BY p.patient_id ORDER BY MAX(m.`timestamp`) ASC ) innerQueryLastContact ON p.patient_id = innerQueryLastContact.pId ";
                lastContactReceivedQuery = " LEFT JOIN ( SELECT p.patient_id AS pId, MAX(m.`timestamp`) AS lastContactReceivedDate, COUNT(*) AS unreadCount FROM patient p LEFT JOIN tech_support_message m ON m.patient_id = p.patient_id WHERE (m.user_id = " +defaultTS.getUser().getUserId()+" OR m.notified_techsupport LIKE '%"+provider.getProviderId()+"' ) AND m.`owner` = 'PATIENT' GROUP BY p.patient_id ORDER BY MAX(m.`timestamp`) ASC ) innerQueryLastContactReceived ON p.patient_id = innerQueryLastContactReceived.pId ";
            }

            stringBuilder.append(lastContactSentQuery);
            stringBuilder.append(lastContactReceivedQuery);
            stringBuilder.append(primaryFoodCoachQuery);
            stringBuilder.append(
                    " LEFT JOIN ( " +
                    " SELECT o2o.patient_id as pID, o2o.session_time AS sessionTime, CONCAT(iUser.first_name,' ',iUser.last_name) AS coachName from one_to_one_sessions o2o " +
                    " LEFT JOIN provider iPro ON iPro.provider_id = o2o.provider_id " +
                    " LEFT JOIN users iUser ON iUser.user_id = iPro.user_id " +
                    " WHERE o2o.session_time IN ( SELECT MIN(o22o.session_time) AS sessionTime FROM one_to_one_sessions o22o WHERE DATE_FORMAT(FROM_UNIXTIME(o22o.session_time/1000), '%m/%d/%Y %H') >= DATE_FORMAT(NOW(), '%m/%d/%Y %H') GROUP BY o22o.patient_id ) " +
                    " GROUP BY o2o.patient_id " +
                    " ) innerQuery2 ON p.patient_id = innerQuery2.pID " +
                    " LEFT JOIN ( SELECT l.upload_time AS lastLogTime, l.patient_id AS patientID, l.log_type AS lastLogType FROM " +
                    " log l INNER JOIN ( SELECT MAX(l.upload_time) AS uploadTime FROM log l WHERE l.is_removed = 0 GROUP BY " +
                    " l.patient_id) AS innerQuery3 ON innerQuery3.uploadTime = l.upload_time" +
                    " GROUP BY l.patient_id ORDER BY l.patient_id DESC " +
                    " ) AS innerQuery4 ON p.patient_id = innerQuery4.patientID " +
                    "LEFT JOIN ( " +
                    " SELECT " +
                    " cn.notes_date AS time, " +
                    " cn.patient_id AS patientID, " +
                    " CONCAT(iUser.first_name, ' ' , iUser.last_name) AS notesCoachName " +
                    " FROM " +
                    " coach_notes cn " +
                    " LEFT JOIN provider iPro ON iPro.provider_id = cn.provider_id " +
                    " LEFT JOIN users iUser ON iUser.user_id = iPro.user_id " +
                    "WHERE cn.notes_date IN (" +
                    " SELECT MAX(cn2.notes_date) FROM coach_notes cn2 GROUP BY cn2.patient_id" +
                    ")" +
                    ") AS innerQuery5 ON p.patient_id = innerQuery5.patientID");

            stringBuilder.append(leadCoachQuery);
            stringBuilder.append(" WHERE ( p.is_deleted = 0 OR p.is_deleted IS NULL ) AND pp.providers_provider_id = " + provider.getProviderId() + queryMyMembers);

            if (!sSearch.isEmpty()) {

                if (sSearch.contains(" ")) {
                    sSearch = sSearch.replaceAll("\\s", "%");
                }
                stringBuilder.append(" AND (p.patient_id LIKE '%" + sSearch + "%'" +
                        " OR u.first_name LIKE '%" + sSearch + "%'" +
                        " OR u.last_name LIKE '%" + sSearch + "%'" +
                        " OR u.display_name LIKE '%" + sSearch + "%'" +
                        " OR u.email LIKE '%" + sSearch + "%'" +
                        " OR CONCAT_WS(' ', u.first_name, u.last_name) LIKE '%" + sSearch + "%'" +
                        " OR u.phone LIKE '%" + sSearch + "%')");

            }

            stringBuilder.append(" GROUP BY p.patient_id");
            if (!sortIndex.isEmpty()) {
                stringBuilder.append(" ORDER BY " + sortIndex + " " + sSortDirection);
            } else {
                stringBuilder.append(" ORDER BY p.patient_id DESC ");
            }
            stringBuilder.append(" LIMIT " + start + "," + end + "");

            List<Object> result = currentSession.createSQLQuery(stringBuilder.toString()).list();
            List<Long> ids = new ArrayList<Long>();
            if (result != null && result.size() > 0) {

                for (Object obj : result) {
                    Object[] objArray = (Object[]) obj;

                    PatientDTO patientDTO = new PatientDTO();

                    Long patientId = ObjectUtils.nullSafeLong(objArray[0]);
                    String firstName = ObjectUtils.nullSafe(objArray[1]);
                    String lastName = ObjectUtils.nullSafe(objArray[2]);
                    String displayName = ObjectUtils.nullSafe(objArray[3]);
                    String email = ObjectUtils.nullSafe(objArray[4]);
                    String phone = ObjectUtils.nullSafe(objArray[5]);
                    int unreadCount = ObjectUtils.nullSafeInteger(objArray[6]);
                    String imagePath = ObjectUtils.nullSafe(objArray[7]);

                    String fullPrimaryCoachName = ObjectUtils.nullSafe(objArray[8]);

                    patientDTO.setPatientId(patientId);

                    patientDTO.setEmail(email);
                    patientDTO.setPhone(phone);
                    patientDTO.setPatientName(firstName + " " + lastName);
                    patientDTO.setUnProcessedLogs(0);
                    patientDTO.setMessageCount(unreadCount);
                    patientDTO.setImagePath(imagePath);
                    patientDTO.setPrimaryCoachName(fullPrimaryCoachName);
                    patientDTO.setLeadCoachName(ObjectUtils.nullSafe(objArray[22]));

                    long nextSessionTime = ObjectUtils.nullSafeLong(objArray[9]);
                    long todaysStartTime = DateUtils.getStartOfDayTime(System.currentTimeMillis());
                    long userOffsetTime = ObjectUtils.nullSafeLong(objArray[17]);
                    String userOffsetKey = ObjectUtils.nullSafe(objArray[18]);

                    String sessionScheduledCoachName = ObjectUtils.nullSafe(objArray[19]);
                    String notesCoachName = ObjectUtils.nullSafe(objArray[20]);

                    patientDTO.setSessionCoachName(sessionScheduledCoachName);
                    patientDTO.setNotesCoachName(notesCoachName);

                    if (nextSessionTime > 0 && nextSessionTime >= todaysStartTime) {
                        /*String formattedInUserTimeZone = DateUtils.getFormattedDateInUserTimeZone(nextSessionTime, userOffsetTime, userOffsetKey, DateUtils.DATE_FORMAT_WITH_TIME);
                        patientDTO.setNextSession(formattedInUserTimeZone);*/
                        patientDTO.setNextSession(nextSessionTime + "");
                    }

                    String lastLogTime = (objArray[10] == null) ? "" : DateUtils.getFormattedDate((((BigInteger) objArray[10]).longValue()), DateUtils.DATE_FORMAT);
                    String lastLogTimeMillis = (objArray[10] == null) ? "" : objArray[10] + "";
                    patientDTO.setLastLogTime(lastLogTime);
                    patientDTO.setLastLogTimeMillis(lastLogTimeMillis);
                    patientDTO.setLastLogType(ObjectUtils.nullSafe(objArray[11]));

                    long notesDates = ObjectUtils.nullSafeLong(objArray[12]);
                    if (notesDates > 0) {
                        patientDTO.setNotesDate(DateUtils.getFormattedDate(notesDates, DateUtils.DATE_FORMAT));
                        patientDTO.setNotesDateLong(notesDates);
                    }

                    patientDTO.setAddress(ObjectUtils.nullSafe(objArray[13]));
                    patientDTO.setCity(ObjectUtils.nullSafe(objArray[14]));
                    patientDTO.setState(ObjectUtils.nullSafe(objArray[15]));
                    patientDTO.setPhone(phone);
                    patientDTO.setZip(ObjectUtils.nullSafe(objArray[16]));
                    patientDTO.setRegisteredWeeks(objArray[21].toString());

                    patientDTO.setLastContactSentDate(ObjectUtils.nullSafeLong(objArray[23]));
                    patientDTO.setLastContactReceivedDate(ObjectUtils.nullSafeLong(objArray[24]));

                    patientDTOs.add(patientDTO);
                    ids.add(patientId);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return patientDTOs;
    }

    public long getPatientListCountByCoach(String sSearch, Provider provider, String filterByFoodCoach) {
        long resultCount = 0l;

        String queryMyMembers = "";
        if (!ObjectUtils.isEmpty(filterByFoodCoach) && filterByFoodCoach.equalsIgnoreCase("filterByPrimaryFoodCoach")) {
            queryMyMembers = " AND p.primary_food_coach_id = " + provider.getProviderId();
        } else if (!ObjectUtils.isEmpty(filterByFoodCoach) && filterByFoodCoach.equalsIgnoreCase("filterByLeadCoach")) {
            queryMyMembers = " AND p.lead_coach_id = " + provider.getProviderId();
        }

        try {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("SELECT " +
                    "p.patient_id, " +
                    "u.first_name, " +
                    "u.last_name, " +
                    "u.display_name, " +
                    "u.email, " +
                    "u.phone, " +
                    "innerQuery.unreadCount, " +
                    "p.image_path, " +
                    "innerQuery1.primFirstName, " +
                    "innerQuery1.primLastName, " +
                    "innerQuery2.sessionTime, " +
                    "innerQuery4.lastLogTime, " +
                    "innerQuery4.lastLogType, innerQuery5.time, u.address, u.city, u.state, u.zip_code, u.log_time_offset, u.offset_key, innerQuery2.coachName, innerQuery5.notesCoachName " +
                    "FROM patient p " +
                    "LEFT JOIN provider_patient pp ON p.patient_id = pp.patients_patient_id " +
                    "LEFT JOIN users u ON u.user_id = p.user_id ");

            if (provider.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())) {
                stringBuilder.append("LEFT JOIN ( SELECT p.patient_id AS pId, " +
                        "COUNT(tsm.message_id) AS unreadCount " +
                        "FROM patient p " +
                        "LEFT JOIN tech_support_message tsm ON p.patient_id = tsm.patient_id " +
                        "WHERE tsm.`owner` = 'PATIENT' " +
                        "AND tsm.read_status = 0 " +
                        "AND tsm.notified_techsupport LIKE '%" + provider.getProviderId() + "%' " +
                        "GROUP BY p.patient_id " +
                        "ORDER BY p.patient_id) " +
                        "innerQuery ON p.patient_id = innerQuery.pId ");
            } else {
                stringBuilder.append("LEFT JOIN ( SELECT p.patient_id AS pId, " +
                        "COUNT(m.message_id) AS unreadCount " +
                        "FROM patient p " +
                        "LEFT JOIN message m ON p.patient_id = m.patient_id " +
                        "WHERE m.`owner` = 'patient' " +
                        "AND m.read_status = 0 " +
                        "AND m.user_id = " + provider.getUser().getUserId() + " " +
                        "GROUP BY p.patient_id " +
                        "ORDER BY p.patient_id) " +
                        "innerQuery ON p.patient_id = innerQuery.pId ");
            }

            stringBuilder.append(" LEFT JOIN ( " +
                    " SELECT " +
                    " p.primary_food_coach_id AS primaryFoodCoachId, " +
                    " userP.first_name as primFirstName, " +
                    " userP.last_name as primLastName " +
                    " FROM " +
                    " patient p " +
                    " LEFT JOIN provider prov ON prov.provider_id = p.primary_food_coach_id " +
                    " LEFT JOIN users userP ON prov.user_id = userP.user_id " +
                    " ORDER BY " +
                    " p.primary_food_coach_id " +
                    ") innerQuery1 ON p.primary_food_coach_id = innerQuery1.primaryFoodCoachId " +
                    " LEFT JOIN ( " +
                    " SELECT o2o.patient_id as pID, MIN(o2o.session_time) AS sessionTime, CONCAT(iUser.first_name,' ',iUser.last_name) AS coachName from one_to_one_sessions o2o " +
                    " LEFT JOIN provider iPro ON iPro.provider_id = o2o.provider_id " +
                    " LEFT JOIN users iUser ON iUser.user_id = iPro.user_id " +
                    " WHERE DATE_FORMAT(FROM_UNIXTIME(o2o.session_time/1000), '%m/%d/%Y') >= DATE_FORMAT(NOW(), '%m/%d/%Y') " +
                    " GROUP BY o2o.patient_id " +
                    " ) innerQuery2 ON p.patient_id = innerQuery2.pID " +
                    " LEFT JOIN ( SELECT l.upload_time AS lastLogTime, l.patient_id AS patientID, l.log_type AS lastLogType FROM " +
                    " log l INNER JOIN ( SELECT MAX(l.upload_time) AS uploadTime FROM log l WHERE l.is_removed = 0 GROUP BY " +
                    " l.patient_id) AS innerQuery3 ON innerQuery3.uploadTime = l.upload_time" +
                    " GROUP BY l.patient_id ORDER BY l.patient_id DESC " +
                    " ) AS innerQuery4 ON p.patient_id = innerQuery4.patientID " +
                    "LEFT JOIN ( " +
                    " SELECT " +
                    " cn.notes_date AS time, " +
                    " cn.patient_id AS patientID, " +
                    " CONCAT(iUser.first_name, ' ' , iUser.last_name) AS notesCoachName " +
                    " FROM " +
                    " coach_notes cn " +
                    " LEFT JOIN provider iPro ON iPro.provider_id = cn.provider_id " +
                    " LEFT JOIN users iUser ON iUser.user_id = iPro.user_id " +
                    "WHERE cn.notes_date IN (" +
                    " SELECT MAX(cn2.notes_date) FROM coach_notes cn2 GROUP BY cn2.patient_id " +
                    ")" +
                    ") AS innerQuery5 ON p.patient_id = innerQuery5.patientID" +
                    " WHERE ( p.is_deleted = 0 OR p.is_deleted IS NULL ) AND pp.providers_provider_id = " + provider.getProviderId() + queryMyMembers);
            if (!sSearch.isEmpty()) {
                stringBuilder.append(" AND (p.patient_id LIKE '%" + sSearch + "%'" +
                        " OR u.first_name LIKE '%" + sSearch + "%'" +
                        " OR u.last_name LIKE '%" + sSearch + "%'" +
                        " OR u.display_name LIKE '%" + sSearch + "%'" +
                        " OR u.email LIKE '%" + sSearch + "%'" +
                        " OR u.phone LIKE '%" + sSearch + "%')");

            }

            stringBuilder.append(" GROUP BY p.patient_id");
            stringBuilder.append(" ORDER BY p.patient_id DESC ");

            List<Object> result = currentSession.createSQLQuery(stringBuilder.toString()).list();
            resultCount = result.size();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return resultCount;
    }

    public boolean removeCoachAssignmentFromMember(long providerId){
        Query query = currentSession.createSQLQuery(" UPDATE patient " +
                                                    " SET primary_food_coach_id = NULL " +
                                                    " WHERE primary_food_coach_id = " + providerId + ";");

        int result = query.executeUpdate();
        if ( result >= 0 ) {
            return true;
        }

        return false;
    }

    public boolean removeMacAddressAssignmentFromMember(String macAddress){
        Query query = currentSession.createSQLQuery(" UPDATE patient " +
                " SET device_mac_address = NULL " +
                " WHERE device_mac_address = '" + macAddress + "';");

        int result = query.executeUpdate();
        if ( result >= 0 ) {
            return true;
        }

        return false;
    }

    public List<PatientDTO> getMyMembers(long coachId, String primaryOrLeadFilter){
        List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

        String primaryOrLeadFilterQuery = " WHERE p.lead_coach_id = ? ";
        if(primaryOrLeadFilter.equalsIgnoreCase(AppConstants.PRIMARY_FOOD_COACH)){
            primaryOrLeadFilterQuery = " WHERE p.primary_food_coach_id = ? ";
        }
        try{
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(" SELECT p.patient_id as patientId, p.uuid AS uuid, us.email as email, us.first_name as firstName,  us.last_name as lastName, us.display_name as displayName, us.registration_date as regDate ");
            stringBuilder.append(" FROM  patient p ");
            stringBuilder.append(" JOIN users us ON p.user_id = us.user_id ");
            stringBuilder.append(primaryOrLeadFilterQuery + " AND ( p.is_deleted = 0 OR p.is_deleted IS NULL) AND us.is_registration_completed = 1 ");
            stringBuilder.append(" ORDER BY us.registration_date DESC ");

            List result = currentSession.createSQLQuery(stringBuilder.toString())
                    .addScalar("patientId", LongType.INSTANCE)
                    .addScalar("uuid", StringType.INSTANCE)
                    .addScalar("email", StringType.INSTANCE)
                    .addScalar("firstName", StringType.INSTANCE)
                    .addScalar("lastName", StringType.INSTANCE)
                    .addScalar("displayName", StringType.INSTANCE)
                    .addScalar("regDate", DateType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(PatientDTO.class))
                    .setParameter(0, coachId)
                    .list();

            patientDTOs = (List<PatientDTO>) result;

        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return patientDTOs;
    }

    public List<PatientDTO> getInactiveMembers(long coachId, int daysThreshold){
        List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

        try{
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append(" SELECT p.patient_id as patientId, p.uuid AS uuid, us.email as email, us.first_name as firstName,  us.last_name as lastName, us.display_name as displayName, innerQuery.lastLogTime as invitedDate ");
            stringBuilder.append(" FROM patient p ");
            stringBuilder.append(" LEFT JOIN provider_patient pp ON p.patient_id = pp.patients_patient_id ");
            stringBuilder.append(" LEFT JOIN users us ON us.user_id = p.user_id ");
            stringBuilder.append(" LEFT JOIN ( SELECT MAX(l.upload_time) AS lastLogTime, l.patient_id AS patientID ");
            stringBuilder.append(" FROM log l WHERE l.is_removed = 0 GROUP BY l.patient_id ");
            stringBuilder.append(" ) innerQuery ON p.patient_id = innerQuery.patientID ");
            stringBuilder.append(" WHERE pp.providers_provider_id = ? ");
            stringBuilder.append(" AND ( p.is_deleted = 0 OR p.is_deleted IS NULL ) ");
            stringBuilder.append(" AND us.is_registration_completed = 1 ");
            stringBuilder.append(" AND TO_DAYS(NOW()) - TO_DAYS(DATE_FORMAT(FROM_UNIXTIME(innerQuery.lastLogTime / 1000),'%Y-%m-%d')) > ? ");
            stringBuilder.append(" GROUP BY p.patient_id ORDER BY innerQuery.lastLogTime DESC ");

            List result = currentSession.createSQLQuery(stringBuilder.toString())
                    .addScalar("patientId", LongType.INSTANCE)
                    .addScalar("uuid", StringType.INSTANCE)
                    .addScalar("email", StringType.INSTANCE)
                    .addScalar("firstName", StringType.INSTANCE)
                    .addScalar("lastName", StringType.INSTANCE)
                    .addScalar("displayName", StringType.INSTANCE)
                    .addScalar("invitedDate", StringType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(PatientDTO.class))
                    .setParameter(0, coachId)
                    .setParameter(1, daysThreshold)
                    .list();

            patientDTOs = (List<PatientDTO>) result;

        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return patientDTOs;
    }

    public List<PatientDTO> getUpcomingOneToOneSessions(long coachId){
        List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

        try{
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append(" SELECT p.patient_id as patientId, p.uuid AS uuid, us.email as email, us.first_name as firstName,  us.last_name as lastName, us.display_name as displayName, innerQuery.sessionTime as invitedDate ");
            stringBuilder.append(" FROM patient p ");
            stringBuilder.append(" LEFT JOIN provider_patient pp ON p.patient_id = pp.patients_patient_id ");
            stringBuilder.append(" LEFT JOIN users us ON us.user_id = p.user_id ");
            stringBuilder.append(" JOIN ( SELECT o2o.patient_id AS pID, MIN(o2o.session_time) AS sessionTime ");
            stringBuilder.append(" FROM one_to_one_sessions o2o ");
            stringBuilder.append(" LEFT JOIN provider iPro ON iPro.provider_id = o2o.provider_id ");
            stringBuilder.append(" LEFT JOIN users iUser ON iUser.user_id = iPro.user_id ");
            stringBuilder.append(" WHERE DATE_FORMAT(FROM_UNIXTIME(o2o.session_time / 1000),'%m/%d/%Y %H') >= DATE_FORMAT(NOW(), '%m/%d/%Y %H') ");
            stringBuilder.append(" AND o2o.provider_id = ? GROUP BY o2o.patient_id ) ");
            stringBuilder.append(" innerQuery ON p.patient_id = innerQuery.pID ");

            stringBuilder.append(" WHERE pp.providers_provider_id = ? ");
            stringBuilder.append(" AND ( p.is_deleted = 0 OR p.is_deleted IS NULL) ");

            List result = currentSession.createSQLQuery(stringBuilder.toString())
                    .addScalar("patientId", LongType.INSTANCE)
                    .addScalar("uuid", StringType.INSTANCE)
                    .addScalar("email", StringType.INSTANCE)
                    .addScalar("firstName", StringType.INSTANCE)
                    .addScalar("lastName", StringType.INSTANCE)
                    .addScalar("displayName", StringType.INSTANCE)
                    .addScalar("invitedDate", StringType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(PatientDTO.class))
                    .setParameter(0, coachId)
                    .setParameter(1, coachId)
                    .list();

            patientDTOs = (List<PatientDTO>) result;

        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return patientDTOs;
    }

    public List<PatientDTO> getCoachMessages(long coachId){
        List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

        try{
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append(" SELECT p.patient_id as patientId, p.uuid AS uuid, us.email as email, us.first_name as firstName,  us.last_name as lastName, us.display_name as displayName, innerQuery.time as invitedDate ");
            stringBuilder.append(" FROM patient p ");
            stringBuilder.append(" LEFT JOIN provider_patient pp ON p.patient_id = pp.patients_patient_id ");
            stringBuilder.append(" LEFT JOIN users us ON us.user_id = p.user_id ");
            stringBuilder.append(" JOIN ( SELECT MAX(cn.notes_date) AS time, cn.patient_id AS patientID ");
            stringBuilder.append(" FROM coach_notes cn ");
            stringBuilder.append(" LEFT JOIN provider iPro ON iPro.provider_id = cn.provider_id ");
            stringBuilder.append(" LEFT JOIN users iUser ON iUser.user_id = iPro.user_id ");
            stringBuilder.append(" WHERE cn.provider_id = ? OR cn.notified_coaches LIKE '%"+coachId+"%' GROUP BY cn.patient_id ) ");
            stringBuilder.append(" innerQuery ON p.patient_id = innerQuery.patientID ");

            stringBuilder.append(" WHERE pp.providers_provider_id = ? ");
            stringBuilder.append(" AND ( p.is_deleted = 0 OR p.is_deleted IS NULL) ORDER BY innerQuery.time DESC ");

            List result = currentSession.createSQLQuery(stringBuilder.toString())
                    .addScalar("patientId", LongType.INSTANCE)
                    .addScalar("uuid", StringType.INSTANCE)
                    .addScalar("email", StringType.INSTANCE)
                    .addScalar("firstName", StringType.INSTANCE)
                    .addScalar("lastName", StringType.INSTANCE)
                    .addScalar("displayName", StringType.INSTANCE)
                    .addScalar("invitedDate", StringType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(PatientDTO.class))
                    .setParameter(0, coachId)
                    .setParameter(1, coachId)
                    .list();

            patientDTOs = (List<PatientDTO>) result;

        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return patientDTOs;
    }

    public List<PatientDTO> getMemberMessages(Provider coach){
        List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

        try{
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append(" SELECT p.patient_id as patientId, p.uuid AS uuid, us.email as email, us.first_name as firstName, us.last_name as lastName, us.display_name as displayName, innerQuery.msgTime as invitedDate, innerQuery.unreadCount AS messageCount ");
            stringBuilder.append(" FROM patient p ");
            stringBuilder.append(" LEFT JOIN provider_patient pp ON p.patient_id = pp.patients_patient_id ");
            stringBuilder.append(" LEFT JOIN users us ON us.user_id = p.user_id ");
            stringBuilder.append(" JOIN ( SELECT pat.patient_id AS pId, COUNT(m.message_id) AS unreadCount, MAX(m.`timestamp`) as msgTime ");
            stringBuilder.append(" FROM patient pat ");
            stringBuilder.append(" LEFT JOIN message m ON pat.patient_id = m.patient_id ");
            stringBuilder.append(" WHERE m.`owner` = 'patient' AND m.read_status = 0 ");
            stringBuilder.append(" AND m.user_id = ? GROUP BY pat.patient_id ORDER BY pat.patient_id ) ");
            stringBuilder.append(" innerQuery ON p.patient_id = innerQuery.pId ");

            stringBuilder.append(" WHERE pp.providers_provider_id = ? ");
            stringBuilder.append(" AND ( p.is_deleted = 0 OR p.is_deleted IS NULL) ORDER BY innerQuery.msgTime DESC ");

            List result = currentSession.createSQLQuery(stringBuilder.toString())
                    .addScalar("patientId", LongType.INSTANCE)
                    .addScalar("uuid", StringType.INSTANCE)
                    .addScalar("email", StringType.INSTANCE)
                    .addScalar("firstName", StringType.INSTANCE)
                    .addScalar("lastName", StringType.INSTANCE)
                    .addScalar("displayName", StringType.INSTANCE)
                    .addScalar("invitedDate", StringType.INSTANCE)
                    .addScalar("messageCount", IntegerType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(PatientDTO.class))
                    .setParameter(0, coach.getUser().getUserId())
                    .setParameter(1, coach.getProviderId())
                    .list();

            patientDTOs = (List<PatientDTO>) result;

        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return patientDTOs;
    }

    public void deleteAssignedProvidersFromPatient(List<Long> ids, Patient patient){

        try{
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append("Delete FROM provider_patient WHERE providers_provider_id IN(:ids) AND patients_patient_id = :patientId" );

            int i = currentSession.createSQLQuery(stringBuilder.toString())
                    .setParameterList("ids",ids)
                    .setParameter("patientId", patient.getPatientId()).executeUpdate();
            currentSession.refresh(patient);

        } catch(Exception ex) {
            ex.printStackTrace();
        }

    }

    public void updateAssignedProvidersFromPatient(List<Long> ids, Patient patient){
        try{
            for(long id : ids){
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.append("INSERT INTO provider_patient VALUES (?, ?)" );

                currentSession.createSQLQuery(stringBuilder.toString())
                        .setParameter(0, id)
                        .setParameter(1, patient.getPatientId()).executeUpdate();
            }
            currentSession.refresh(patient);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<PatientDTO> getAllPatientByPrimaryFoodCoachId(Long primaryFoodCoachId) {
        List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

        try {
            StringBuilder query = new StringBuilder();

            query.append(" SELECT p.patient_id as patientId, p.uuid as uuid ,u.email as email, u.first_name as firstName, u.last_name as lastName, u.phone as phone, {pd.*}")
                    .append(" FROM patient p join users u on p.user_id = u.user_id ")
                    .append(" LEFT JOIN device pd on p.device_mac_address = pd.device_mac_address ")
                    .append(" WHERE (p.is_deleted = 0 OR p.is_deleted IS NULL)")
                    .append(" AND p.primary_food_coach_id = " + primaryFoodCoachId + " ");

            List result = currentSession.createSQLQuery(query.toString())
                    .addScalar("patientId", LongType.INSTANCE)
                    .addScalar("uuid", StringType.INSTANCE)
                    .addScalar("email", StringType.INSTANCE)
                    .addScalar("firstName", StringType.INSTANCE)
                    .addScalar("lastName", StringType.INSTANCE)
                    .addScalar("phone", StringType.INSTANCE)
                    .addEntity("pd", Device.class)
                    .setResultTransformer(Transformers.aliasToBean(PatientDTO.class))
                    .list();

            patientDTOs = (List<PatientDTO>) result;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return patientDTOs;
    }

    public List<PatientDTO> getAllPatientByLeadCoachId(Long leadCoachId) {
        List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

        try {
            StringBuilder query = new StringBuilder();

            query.append(" SELECT p.patient_id as patientId, p.uuid as uuid ,u.email as email, u.first_name as firstName, u.last_name as lastName, u.phone as phone, {pd.*}")
                    .append(" FROM patient p join users u on p.user_id = u.user_id ")
                    .append(" LEFT JOIN device pd on p.device_mac_address = pd.device_mac_address ")
                    .append(" WHERE (p.is_deleted = 0 OR p.is_deleted IS NULL)")
                    .append(" AND p.lead_coach_id = " + leadCoachId + " ");

            List result = currentSession.createSQLQuery(query.toString())
                    .addScalar("patientId", LongType.INSTANCE)
                    .addScalar("uuid", StringType.INSTANCE)
                    .addScalar("email", StringType.INSTANCE)
                    .addScalar("firstName", StringType.INSTANCE)
                    .addScalar("lastName", StringType.INSTANCE)
                    .addScalar("phone", StringType.INSTANCE)
                    .addEntity("pd", Device.class)
                    .setResultTransformer(Transformers.aliasToBean(PatientDTO.class))
                    .list();

            patientDTOs = (List<PatientDTO>) result;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return patientDTOs;
    }

    public List<PatientDTO> getAllPatientsByLeadCoachIdAndPrimaryFoodCoachId(Long coachId){
        List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

        try {
            StringBuilder query = new StringBuilder();

            query.append(" SELECT p.patient_id as patientId, p.uuid as uuid ,u.email as email, u.first_name as firstName, u.last_name as lastName, u.phone as phone, {pd.*}")
                    .append(" FROM patient p join users u on p.user_id = u.user_id ")
                    .append(" LEFT JOIN device pd on p.device_mac_address = pd.device_mac_address ")
                    .append(" WHERE (p.is_deleted = 0 OR p.is_deleted IS NULL)")
                    .append(" AND (p.lead_coach_id = " + coachId + " OR p.primary_food_coach_id = " + coachId + ") ");

            List result = currentSession.createSQLQuery(query.toString())
                    .addScalar("patientId", LongType.INSTANCE)
                    .addScalar("uuid", StringType.INSTANCE)
                    .addScalar("email", StringType.INSTANCE)
                    .addScalar("firstName", StringType.INSTANCE)
                    .addScalar("lastName", StringType.INSTANCE)
                    .addScalar("phone", StringType.INSTANCE)
                    .addEntity("pd", Device.class)
                    .setResultTransformer(Transformers.aliasToBean(PatientDTO.class))
                    .list();

            patientDTOs = (List<PatientDTO>) result;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return patientDTOs;
    }

    public List<Patient> getPatientsByLeadCoachIdAndPrimaryFoodCoachId(Long coachId, Long patientId) {
        List<Patient> patients = new ArrayList<Patient>();

        try {
            patients = currentSession.createQuery("SELECT p FROM Patient p JOIN p.user userP " +
                    " WHERE (p.isDeleted = false OR p.isDeleted IS NULL) " +
                    " AND (p.leadCoachId = :leadCoachId OR p.primaryFoodCoachId = :primaryFoodCoachId) " +
                    " AND p.patientId <> :patientId AND userP.isRegistrationCompleted = true " +
                    " GROUP BY p.patientId")
                    .setParameter("leadCoachId", coachId)
                    .setParameter("primaryFoodCoachId", coachId)
                    .setParameter("patientId", patientId)
                    .list();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return patients;
    }

    public List<PatientDTO> getPatientsLastContactOn(Provider coach){
        List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

        try{
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append(" SELECT p.patient_id as patientId, p.uuid AS uuid, us.email as email, us.first_name as firstName, us.last_name as lastName, us.display_name as displayName, MAX(m.`timestamp`) AS invitedDate ");
            stringBuilder.append(" FROM patient p ");
            stringBuilder.append(" LEFT JOIN provider_patient pp ON p.patient_id = pp.patients_patient_id ");
            stringBuilder.append(" LEFT JOIN users us ON us.user_id = p.user_id ");
            stringBuilder.append(" LEFT JOIN message m ON m.patient_id = p.patient_id ");
            stringBuilder.append(" WHERE pp.providers_provider_id = ? AND m.user_id = ? ");
            stringBuilder.append(" AND m.`owner` = 'PROVIDER' AND ( p.is_deleted = 0 OR p.is_deleted IS NULL) GROUP BY p.patient_id ORDER BY MAX(m.`timestamp`) ASC ");

            List result = currentSession.createSQLQuery(stringBuilder.toString())
                    .addScalar("patientId", LongType.INSTANCE)
                    .addScalar("uuid", StringType.INSTANCE)
                    .addScalar("email", StringType.INSTANCE)
                    .addScalar("firstName", StringType.INSTANCE)
                    .addScalar("lastName", StringType.INSTANCE)
                    .addScalar("displayName", StringType.INSTANCE)
                    .addScalar("invitedDate", StringType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(PatientDTO.class))
                    .setParameter(0, coach.getProviderId())
                    .setParameter(1, coach.getUser().getUserId())
                    .list();

            patientDTOs = (List<PatientDTO>) result;

        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return patientDTOs;
    }

    public Boolean isMRNAlreadyAssigned(String MRN, Long facilityId) {
        boolean isMRNAssigned = false;
        List<Object> patients = new ArrayList<Object>();
        StringBuilder query = new StringBuilder();

        if (MRN != null && !(MRN.isEmpty())) {

            query.append("SELECT p.*")
            .append("FROM patient p JOIN patient_facility pf ON p.patient_id = pf.patients_patient_id ")
            .append("JOIN facility f ON f.facility_id = pf.facilities_facility_id ")
            .append("WHERE p.mrn = " + "'" + MRN + "' ")
            .append("AND pf.facilities_facility_id = :facilityId");

            patients = currentSession.createSQLQuery(query.toString()).setParameter("facilityId", facilityId).list();

            if (patients.size() > 0) {
                isMRNAssigned = true;
            }
        }
        return isMRNAssigned;
    }

    public Boolean isMRNAlreadyAssigned(String mrn, Long facilityId, long patientId) {
        boolean isMRNAssigned = false;
        List<Object> patients = new ArrayList<Object>();
        StringBuilder query = new StringBuilder();

        if (!ObjectUtils.isEmpty(mrn)) {

            query.append("SELECT p.*")
                    .append("FROM patient p JOIN patient_facility pf ON p.patient_id = pf.patients_patient_id ")
                    .append("JOIN facility f ON f.facility_id = pf.facilities_facility_id ")
                    .append("WHERE p.mrn = " + "'" + mrn + "' ")
                    .append("AND pf.facilities_facility_id = :facilityId AND p.patient_id <> :patientId");

            patients = currentSession.createSQLQuery(query.toString()).setParameter("facilityId", facilityId).setParameter("patientId", patientId).list();

            if (patients.size() > 0) {
                isMRNAssigned = true;
            }
        }
        return isMRNAssigned;
    }
}