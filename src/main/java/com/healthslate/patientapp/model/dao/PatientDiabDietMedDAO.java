package com.healthslate.patientapp.model.dao;

import org.hibernate.Query;

/**
 * Created by omursaleen on 3/3/2015.
 */
public class PatientDiabDietMedDAO extends BaseDAO {

    public boolean removeDiabetesInfoByPatientId(long patientId) {

        Query query = currentSession.createSQLQuery(" DELETE " +
                                                    " FROM patient_diabetes_info " +
                                                    " WHERE patient_id = " + patientId +";");

        int result = query.executeUpdate();
        if ( result >= 0 ) {
            return true;
        }

        return false;
    }

    public boolean removeDietaryInfoByPatientId(long patientId) {

        Query query = currentSession.createSQLQuery(" DELETE " +
                                                    " FROM patient_dietary_info " +
                                                    " WHERE patient_id = " + patientId +";");

        int result = query.executeUpdate();
        if ( result >= 0 ) {
            return true;
        }

        return false;
    }

    public boolean removeMedicationByPatientId(long patientId) {

        Query query = currentSession.createSQLQuery(" DELETE " +
                                                    " FROM patient_medication " +
                                                    " WHERE patient_id = " + patientId +";");

        int result = query.executeUpdate();
        if ( result >= 0 ) {
            return true;
        }

        return false;
    }

    public boolean removeMedicationById(long id) {

        Query query = currentSession.createSQLQuery(" DELETE " +
                " FROM patient_medication " +
                " WHERE patient_medication_id = " + id +";");

        int result = query.executeUpdate();
        if ( result >= 0 ) {
            return true;
        }

        return false;
    }
}
