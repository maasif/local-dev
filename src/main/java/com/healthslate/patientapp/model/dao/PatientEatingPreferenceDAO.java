package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.PatientEatingPreference;

public class PatientEatingPreferenceDAO extends BaseDAO {

	public PatientEatingPreference getEatingPreferenceByPatientId(long patientId){
        PatientEatingPreference sessionPreference = null;
		
		try {

            sessionPreference  = (PatientEatingPreference)currentSession.createQuery("SELECT ses FROM PatientEatingPreference ses WHERE ses.patient.patientId = ?").setParameter(0, patientId).uniqueResult();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return sessionPreference;
	}
}
