package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.PatientMedication;
import com.healthslate.patientapp.model.entity.PatientSessionPreference;

public class PatientMedicationDAO extends BaseDAO {

	public PatientMedication getPatientMedicationById(long medId){
        PatientMedication patientMedication = null;
		
		try {

            patientMedication  = (PatientMedication)currentSession.createQuery("FROM PatientMedication WHERE patientMedicationId = ?").setParameter(0, medId).uniqueResult();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return patientMedication;
	}
}
