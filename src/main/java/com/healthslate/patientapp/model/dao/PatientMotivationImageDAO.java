package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.FilledForm;
import com.healthslate.patientapp.model.entity.PatientMotivationImage;
import org.hibernate.HibernateException;
import org.hibernate.Query;

/**
 * Created by omursaleen on 3/3/2015.
 */
public class PatientMotivationImageDAO extends BaseDAO {

    public boolean removePatientMotivationImageByPatientId(long patientId) {

        Query query = currentSession.createSQLQuery(" DELETE " +
                                                    " FROM patient_motivation_image " +
                                                    " WHERE patient_id = " + patientId +";");

        int result = query.executeUpdate();
        if ( result >= 0 ) {
            return true;
        }

        return false;
    }

    public PatientMotivationImage getPatientMotivationImageByPatientId(long patientId){
        PatientMotivationImage patientMotivationImage = null;

        try{
            patientMotivationImage = (PatientMotivationImage) currentSession.createQuery("FROM PatientMotivationImage p where p.patient.patientId = ? ")
                    .setParameter(0, patientId).uniqueResult();

        } catch (HibernateException e) {
            e.printStackTrace();
        }

        return patientMotivationImage;
    }
}
