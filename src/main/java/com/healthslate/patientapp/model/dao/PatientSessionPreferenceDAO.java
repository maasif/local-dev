package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.Goal;
import com.healthslate.patientapp.model.entity.PatientSessionPreference;

import java.util.ArrayList;
import java.util.List;

public class PatientSessionPreferenceDAO extends BaseDAO {

	public PatientSessionPreference getSessionPreferenceByPatientId(long patientId){
        PatientSessionPreference sessionPreference = null;
		
		try {

            sessionPreference  = (PatientSessionPreference)currentSession.createQuery("SELECT ses FROM PatientSessionPreference ses WHERE ses.patient.patientId = ?").setParameter(0, patientId).uniqueResult();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return sessionPreference;
	}
}
