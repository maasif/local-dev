package com.healthslate.patientapp.model.dao;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.Preferences;
import com.healthslate.patientapp.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class PreferencesDAO extends BaseDAO {

	@SuppressWarnings("unchecked")
	public List<Preferences> list()
	{
		List<Preferences> preferenceList = null;
		try {
			preferenceList = (List<Preferences>)currentSession.createQuery("from Preferences").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return preferenceList;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, String> map() {
		Map<String, String> prefMap = new LinkedHashMap<String, String>();
		List<Preferences> preferenceList = null;
		try {
			preferenceList = (List<Preferences>)currentSession.createQuery("from Preferences").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (Preferences pref: preferenceList){
			prefMap.put(pref.getPreferenceName() , pref.getPreferenceValue());
		}
		return prefMap;
	}

    public Map<String, String> mapSchedular() {
        Session session = HibernateUtil.getCurrentSession();
        Transaction transaction = null;

        if(session.getTransaction() != null && session.getTransaction().isActive()){
            transaction = session.getTransaction();
        }else{
            transaction = session.beginTransaction();
        }

        session.clear();

        Map<String, String> prefMap = new LinkedHashMap<String, String>();
        List<Preferences> preferenceList = null;
        try {
            preferenceList = (List<Preferences>)currentSession.createQuery("from Preferences").list();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }
        for (Preferences pref: preferenceList){
            prefMap.put(pref.getPreferenceName() , pref.getPreferenceValue());
        }
        return prefMap;
    }

    public String getPreference(String prefKey) {

        if(currentSession == null){
            currentSession = HibernateUtil.getCurrentSession();
        }

        String prefValue = "";
        try {

            Query query = currentSession.createQuery("FROM Preferences WHERE preferenceName = ?");
            System.out.println("Query: "+ query);
            query.setParameter(0, prefKey);

            Preferences preferences = (Preferences)query.uniqueResult();
            prefValue = preferences.getPreferenceValue();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return prefValue;
    }

    public String getPreferenceSchedule(String prefKey) {

        Session session = HibernateUtil.getCurrentSession();
        Transaction transaction = null;

        if(session.getTransaction() != null && session.getTransaction().isActive()){
            transaction = session.getTransaction();
        }else{
            transaction = session.beginTransaction();
        }

        session.clear();

        String prefValue = "";
        try{
            Preferences preferences = (Preferences)session.createQuery("FROM Preferences WHERE preferenceName = ?").setParameter(0, prefKey).uniqueResult();
            prefValue = preferences.getPreferenceValue();

            transaction.commit();
        } catch(Exception ex){
            ex.printStackTrace();
            transaction.rollback();
        }
        return prefValue;
    }
}