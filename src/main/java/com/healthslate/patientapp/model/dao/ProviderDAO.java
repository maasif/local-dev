package com.healthslate.patientapp.model.dao;

import com.amazonaws.services.opsworks.model.App;
import com.healthslate.patientapp.model.dto.CoachDTO;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.HibernateUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class ProviderDAO extends BaseDAO{

	@SuppressWarnings("unchecked")
	public List<Provider> list() {
		
		List<Provider> providers = currentSession.createQuery("from Provider").list();
        if(providers != null && providers.size() > 0){
            for(Provider provider : providers ){
                User userProvider = provider.getUser();
                if(userProvider != null){
                    provider.setFirstName(userProvider.getFirstName());
                    provider.setLastName(userProvider.getLastName());
                    provider.setEmail(userProvider.getEmail());
                    provider.setPhone(userProvider.getPhone());
                }
            }
        }
		return providers;
	}

	public Provider getProviderByUserId(long userId) {
		Session session = HibernateUtil.getCurrentSession();
		Transaction tr = session.beginTransaction();
		Provider provider = (Provider) session.createQuery("Select p FROM Provider p JOIN p.user u WHERE u.userId = ?")
				.setParameter(0, userId)
				.uniqueResult();
		tr.commit();
		return provider;
	}
	
	public Provider getProviderById(long id) {
		Provider provider = (Provider) currentSession.createQuery("FROM Provider WHERE providerId = ?")
				.setParameter(0, id)
				.uniqueResult();
		return provider;
	}

	public Provider getProviderByIdScheduler(long id) {
		Provider provider = null;
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = null;

		if(session.getTransaction() != null && session.getTransaction().isActive()){
			transaction = session.getTransaction();
		}else{
			transaction = session.beginTransaction();
		}
		session.clear();
		try{
			provider = (Provider) currentSession.createQuery("FROM Provider WHERE providerId = ? AND (isDeleted = false OR isDeleted IS NULL)")
					.setParameter(0, id)
					.uniqueResult();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		}
		return provider;
	}

	@SuppressWarnings("unchecked")
	public List<Provider> getProvidersByFacilityId(Long facilityId) {
		List<Provider> providerList = (ArrayList<Provider>) currentSession.createQuery("SELECT p FROM Provider p JOIN p.facility f WHERE f.facilityId = ? AND (p.isDeleted = false OR p.isDeleted IS NULL) AND p.type <> ? AND p.type <> ?")
				.setParameter(0, facilityId)
                .setParameter(1, AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())
                .setParameter(2, AppConstants.PROVIDERTYPE.COACH.getValue())
				.list();
		return providerList;
	}

    @SuppressWarnings("unchecked")
    public List<Provider> getRecruitmentProvidersByFacilityId(Long facilityId) {
        List<Provider> providerList = (ArrayList<Provider>) currentSession.createQuery("SELECT p FROM Provider p JOIN p.facility f WHERE f.facilityId = ? AND (p.isDeleted = false OR p.isDeleted IS NULL) ")
                .setParameter(0, facilityId).list();
        return providerList;
    }

    public List<Provider> getProvidersByIds(List<Long> providersIds) {
        List<Provider> providerList = (ArrayList<Provider>) currentSession.createQuery("SELECT DISTINCT p FROM Provider p JOIN FETCH p.patients WHERE p.providerId IN (:providersIds)")
                .setParameterList("providersIds", providersIds)
                .list();

        return providerList;
    }

	public Provider getProviderByUser(long userId) {

		Provider provider = (Provider) currentSession.createQuery("Select p FROM Provider p JOIN p.user u WHERE u.userId = ?")
				.setParameter(0, userId)
				.uniqueResult();
		return provider;
	}

    public Provider getProviderByUserScheduler(long userId) {

        Session session = HibernateUtil.getCurrentSession();
        Transaction transaction = null;

        if(session.getTransaction() != null && session.getTransaction().isActive()){
            transaction = session.getTransaction();
        }else{
            transaction = session.beginTransaction();
        }

        session.clear();

        try{

            Provider provider = (Provider) session.createQuery("Select p FROM Provider p JOIN p.user u WHERE u.userId = ?")
                    .setParameter(0, userId)
                    .uniqueResult();

            transaction.rollback();

            return provider;

        } catch(Exception ex){
            ex.printStackTrace();
            transaction.rollback();
        }

        return null;
    }

	public Provider getJoinedProviderWithProviderId(long providerId) {

		Provider provider = (Provider) currentSession.createQuery("Select p FROM Provider p JOIN p.user u WHERE p.providerId = ?")
				.setParameter(0, providerId)
				.uniqueResult();
		return provider;
	}

	public List<User> getFoodCoachesByPatientIdScheduler(long patientId) {

		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = null;

		if(session.getTransaction() != null && session.getTransaction().isActive()){
			transaction = session.getTransaction();
		}else{
			transaction = session.beginTransaction();
		}

		session.clear();
		List<Object> objects = null;
		List<User> coaches = new ArrayList<User>();
		try{
			objects = session.createSQLQuery("SELECT u.user_id, u.email, u.first_name from provider_patient pp " +
					" JOIN provider pro " +
					" ON pro.provider_id = pp.providers_provider_id " +
					" JOIN users u ON u.user_id = pro.user_id " +
					" where patients_patient_id = "+patientId + " " +
					" AND pro.type = '"+ AppConstants.CoachTypes.FOOD_COACH.getValue()+"' AND (pro.is_deleted = 0 OR pro.is_deleted IS NULL)").list();
			transaction.commit();

			if(objects != null) {
				for (Object object: objects) {
					Object[] objArray = (Object[]) object;
                    long userId = ObjectUtils.nullSafeLong(objArray[0]);
					String emailAddress = ObjectUtils.nullSafe(objArray[1]);
					String firstName = ObjectUtils.nullSafe(objArray[2]);
					User user = new User();
                    user.setUserId(userId);
					user.setEmail(emailAddress);
					user.setFirstName(firstName);
					coaches.add(user);
				}
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
			transaction.rollback();
		}
		return coaches;
	}

    public CoachDTO getCoachDetailsById(long coachId){
        String query =  "SELECT us.user_id, p.provider_id, us.first_name, us.last_name, us.user_type, us.phone,  "
                + " us.email, f.facility_id, f.name, p.image_path, p.designation, p.is_sms_enabled, p.is_email_enabled, p.is_access_all  "
                + " FROM provider p  "
                + " LEFT JOIN users us ON p.user_id = us.user_id "
                + " LEFT JOIN facility f ON f.facility_id = p.facility_id  "
                + " WHERE us.user_type <> '"+AppConstants.Roles.ROLE_FACILITY_ADMIN.name()+"' "
                + " AND (p.is_deleted = 0 OR p.is_deleted IS NULL) AND p.provider_id = " + coachId;

        Object result = currentSession.createSQLQuery(query).uniqueResult();

        if(result != null){
            Object[] objArray = (Object[]) result;

            long userId = ((BigInteger)objArray[0]).longValue();

            Object providerIdFromDB = objArray[1];
            long providerId = 0;
            if(providerIdFromDB != null){
                providerId = ((BigInteger)providerIdFromDB).longValue();
            }

            String firstName = ObjectUtils.nullSafe(objArray[2]);
            String lastName = ObjectUtils.nullSafe(objArray[3]);
            String userType = ObjectUtils.nullSafe(objArray[4]);
            String phone = ObjectUtils.nullSafe(objArray[5]);
            String email = ObjectUtils.nullSafe(objArray[6]);

            Object facilityIdFromDB = objArray[7];
            long facilityId = 0;
            if(facilityIdFromDB != null){
                facilityId = ((BigInteger)facilityIdFromDB).longValue();
            }

            String facilityName = ObjectUtils.nullSafe(objArray[8]);
            String designation = ObjectUtils.nullSafe(objArray[10]);
            boolean isSMSEnabled = ObjectUtils.nullSafeBoolean(objArray[11]);
            boolean isEmailEnabled = ObjectUtils.nullSafeBoolean(objArray[12]);
            boolean isAccessAll = ObjectUtils.nullSafeBoolean(objArray[13]);

            CoachDTO coachDTO = new CoachDTO(userId, providerId, firstName, lastName, userType, phone, email, facilityId, facilityName, 0, "", designation, isSMSEnabled, isEmailEnabled, 0);
            coachDTO.setIsAccessAll(isAccessAll);

            return coachDTO ;
        }

        return null;
    }

    public List<CoachDTO> getCoachesDataList(int start, int end, String sSearch, String sortIndex, String sSortDirection, long filterByFacility, HttpServletRequest request) {
        List<CoachDTO> coachDTOs = new ArrayList<CoachDTO>();

        String queryFacility = "";
        if(filterByFacility > 0){
            queryFacility = " AND f.facility_id = " + filterByFacility;
        }

        try{
            String query =  "SELECT us.user_id, p.provider_id, us.first_name, us.last_name, us.user_type, us.phone,  "
                    + " us.email, f.facility_id, f.name, p.image_path, p.designation, p.is_sms_enabled, p.is_email_enabled  "
                    + " FROM provider p  "
                    + " LEFT JOIN users us ON p.user_id = us.user_id "
                    + " LEFT JOIN facility f ON f.facility_id = p.facility_id  "
                    + " WHERE us.user_type <> '"+AppConstants.Roles.ROLE_FACILITY_ADMIN.name()+"' "
                    + " AND (p.is_deleted = 0 OR p.is_deleted IS NULL) ";

            if(!ObjectUtils.isEmpty(sSearch)) {
                    query += "AND (p.provider_id LIKE '%" + sSearch + "%'"  +
                            " OR us.first_name LIKE '%" + sSearch + "%'" +
                            " OR us.last_name LIKE '%" + sSearch + "%'" +
                            " OR us.user_type LIKE '%" + sSearch + "%'" +
                            " OR us.phone LIKE '%" + sSearch + "%'" +
                            " OR f.`name` LIKE '%" + sSearch + "%'" +
                            " OR us.email LIKE '%" + sSearch + "%')";

            }

            query += queryFacility;

            if(!sortIndex.isEmpty()){
                query += " ORDER BY " + sortIndex + " " + sSortDirection;
            } else {
                query += " ORDER BY us.registration_date DESC ";
            }

            query += " LIMIT " + start + "," + end + "";

            List<Object> results = currentSession.createSQLQuery(query).list();
            if(results != null && results.size() > 0) {
                for(Object obj : results){
                    Object[] objArray = (Object[]) obj;

                    long userId = ((BigInteger)objArray[0]).longValue();

                    Object providerIdFromDB = objArray[1];
                    long providerId = 0;
                    if(providerIdFromDB != null){
                        providerId = ((BigInteger)providerIdFromDB).longValue();
                    }

                    String firstName = ObjectUtils.nullSafe(objArray[2]);
                    String lastName = ObjectUtils.nullSafe(objArray[3]);
                    String userType = ObjectUtils.nullSafe(objArray[4]);
                    String phone = ObjectUtils.nullSafe(objArray[5]);
                    String email = ObjectUtils.nullSafe(objArray[6]);

                    Object facilityIdFromDB = objArray[7];
                    long facilityId = 0;
                    if(facilityIdFromDB != null){
                        facilityId = ((BigInteger)facilityIdFromDB).longValue();
                    }

                    String facilityName = ObjectUtils.nullSafe(objArray[8]);

                    String imagePath = "";
                    Object imagePathFromDB = (Object)objArray[9];
                    if(imagePathFromDB != null){
                        imagePath = request.getContextPath() + AppConstants.PROVIDER_IMAGE_FOLDER + (String)imagePathFromDB;
                    }

                    String designation = ObjectUtils.nullSafe(objArray[10]);
                    boolean isSMSEnabled = (objArray[11] == null) ? false: Boolean.parseBoolean(objArray[11]+"");
                    boolean isEmailEnabled = (objArray[12] == null) ? false: Boolean.parseBoolean(objArray[12]+"");

                    coachDTOs.add(new CoachDTO(userId, providerId, firstName, lastName, userType, phone, email, facilityId, facilityName, 0, imagePath, designation, isSMSEnabled, isEmailEnabled, 0));
                }
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }
        return coachDTOs;
    }

    public long  getCoachesDataListCount(String sSearch, long filterByFacility) {
        long resultCount = 0l;

        String queryFacility = "";
        if(filterByFacility > 0){
            queryFacility = " AND f.facility_id = " + filterByFacility;
        }

        try{
            String query =  "SELECT us.user_id, p.provider_id, us.first_name, us.last_name, us.user_type, us.phone,  "
                    + " us.email, f.facility_id, f.name, p.image_path, p.designation, p.is_sms_enabled, p.is_email_enabled  "
                    + " FROM provider p  "
                    + " LEFT JOIN users us ON p.user_id = us.user_id "
                    + " LEFT JOIN facility f ON f.facility_id = p.facility_id  "
                    + " WHERE us.user_type <> '"+AppConstants.Roles.ROLE_FACILITY_ADMIN.name()+"' "
                    + " AND (p.is_deleted = 0 OR p.is_deleted IS NULL) ";

            if(!ObjectUtils.isEmpty(sSearch)) {
                query += "AND (p.provider_id LIKE '%" + sSearch + "%'"  +
                        " OR us.first_name LIKE '%" + sSearch + "%'" +
                        " OR us.last_name LIKE '%" + sSearch + "%'" +
                        " OR us.user_type LIKE '%" + sSearch + "%'" +
                        " OR us.phone LIKE '%" + sSearch + "%'" +
                        " OR f.`name` LIKE '%" + sSearch + "%'" +
                        " OR us.email LIKE '%" + sSearch + "%')";

            }

            query += queryFacility;

            query += " ORDER BY us.registration_date DESC ";

            List<Object> results = currentSession.createSQLQuery(query).list();
            resultCount = results.size();
        } catch(Exception ex){
            ex.printStackTrace();
        }
        return resultCount;
    }

    public List<CoachDTO> getCoachesByFacility(HttpServletRequest request, long facilityIdParam){
        List<CoachDTO> coachDTOs = new ArrayList<CoachDTO>();

        try{
            String query =  "SELECT us.user_id, p.provider_id, us.first_name, us.last_name, us.user_type, us.phone,  "
                    + " us.email, f.facility_id, f.name, p.image_path, p.designation, p.is_sms_enabled, p.is_email_enabled  "
                    + " FROM provider p  "
                    + " LEFT JOIN users us ON p.user_id = us.user_id "
                    + " LEFT JOIN facility f ON f.facility_id = p.facility_id  "
                    + " WHERE us.user_type <> '"+AppConstants.Roles.ROLE_FACILITY_ADMIN.name()+"' "
                    + " AND (p.is_deleted = 0 OR p.is_deleted IS NULL) "
                    + " AND f.facility_id = " + facilityIdParam;

            List<Object> results = currentSession.createSQLQuery(query).list();

            if(results != null && results.size() > 0) {
                for(Object obj : results){
                    Object[] objArray = (Object[]) obj;

                    long userId = ((BigInteger)objArray[0]).longValue();

                    Object providerIdFromDB = objArray[1];
                    long providerId = 0;
                    if(providerIdFromDB != null){
                        providerId = ((BigInteger)providerIdFromDB).longValue();
                    }

                    String firstName = ObjectUtils.nullSafe(objArray[2]);
                    String lastName = ObjectUtils.nullSafe(objArray[3]);
                    String userType = ObjectUtils.nullSafe(objArray[4]);
                    String phone = ObjectUtils.nullSafe(objArray[5]);
                    String email = ObjectUtils.nullSafe(objArray[6]);

                    Object facilityIdFromDB = objArray[7];
                    long facilityId = 0;
                    if(facilityIdFromDB != null){
                        facilityId = ((BigInteger)facilityIdFromDB).longValue();
                    }

                    String facilityName = ObjectUtils.nullSafe(objArray[8]);

                    String imagePath = "";
                    Object imagePathFromDB = (Object)objArray[9];
                    if(imagePathFromDB != null){
                        imagePath = request.getContextPath() + AppConstants.PROVIDER_IMAGE_FOLDER + (String)imagePathFromDB;
                    }

                    String designation = ObjectUtils.nullSafe(objArray[10]);
                    boolean isSMSEnabled = (objArray[11] == null) ? false: Boolean.parseBoolean(objArray[11]+"");
                    boolean isEmailEnabled = (objArray[12] == null) ? false: Boolean.parseBoolean(objArray[12]+"");

                    coachDTOs.add(new CoachDTO(userId, providerId, firstName, lastName, userType, phone, email, facilityId, facilityName, 0, imagePath, designation, isSMSEnabled, isEmailEnabled, 0));
                }
            }

        } catch(Exception ex){
            ex.printStackTrace();
        }

        return coachDTOs;
    }

    public List<CoachDTO> getAllCoaches(long facilityIdParam){
        List<CoachDTO> coachDTOs = new ArrayList<CoachDTO>();

        String queryFilter = "";
        if(facilityIdParam > 0){
            queryFilter = " AND f.facility_id = " + facilityIdParam;
        }

        try{
            String query =  "SELECT us.user_id, p.provider_id, us.first_name, us.last_name, us.user_type, us.phone,  "
                    + " us.email, f.facility_id, f.name, p.image_path, p.designation, p.is_sms_enabled, p.is_email_enabled  "
                    + " FROM provider p  "
                    + " LEFT JOIN users us ON p.user_id = us.user_id "
                    + " LEFT JOIN facility f ON f.facility_id = p.facility_id  "
                    + " WHERE us.user_type <> '"+AppConstants.Roles.ROLE_FACILITY_ADMIN.name()+"' "
                    + " AND p.type != '" + AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue() + "'"
                    + " AND (p.is_deleted = 0 OR p.is_deleted IS NULL) "
                    + queryFilter ;

            List<Object> results = currentSession.createSQLQuery(query).list();

            if(results != null && results.size() > 0) {
                for(Object obj : results){
                    Object[] objArray = (Object[]) obj;

                    long userId = ((BigInteger)objArray[0]).longValue();

                    Object providerIdFromDB = objArray[1];
                    long providerId = 0;
                    if(providerIdFromDB != null){
                        providerId = ((BigInteger)providerIdFromDB).longValue();
                    }

                    String firstName = ObjectUtils.nullSafe(objArray[2]);
                    String lastName = ObjectUtils.nullSafe(objArray[3]);
                    String userType = ObjectUtils.nullSafe(objArray[4]);
                    String phone = ObjectUtils.nullSafe(objArray[5]);
                    String email = ObjectUtils.nullSafe(objArray[6]);

                    Object facilityIdFromDB = objArray[7];
                    long facilityId = 0;
                    if(facilityIdFromDB != null){
                        facilityId = ((BigInteger)facilityIdFromDB).longValue();
                    }

                    String facilityName = ObjectUtils.nullSafe(objArray[8]);
                    String designation = ObjectUtils.nullSafe(objArray[10]);
                    boolean isSMSEnabled = (objArray[11] == null) ? false: Boolean.parseBoolean(objArray[11]+"");
                    boolean isEmailEnabled = (objArray[12] == null) ? false: Boolean.parseBoolean(objArray[12]+"");

                    coachDTOs.add(new CoachDTO(userId, providerId, firstName, lastName, userType, phone, email, facilityId, facilityName, 0, "", designation, isSMSEnabled, isEmailEnabled, 0));
                }
            }

        } catch(Exception ex){
            ex.printStackTrace();
        }

        return coachDTOs;
    }

    public List<Provider> getAllTechSupport() {
        List<Provider> allTechSupport = new ArrayList<Provider>();
        StringBuilder query = new StringBuilder();
        try {
            query.append(" SELECT p")
                    .append(" FROM Provider p")
                    .append(" WHERE p.type = '" + AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue() + "'");

            allTechSupport = currentSession.createQuery(query.toString()).list();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return allTechSupport;
    }

    public Provider getDefaultTechSupport() {
        Provider techSupport = (Provider) currentSession.createQuery("FROM Provider ts WHERE ts.isDefaultTS = true AND ts.type = ?")
                .setParameter(0, AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())
                .uniqueResult();

        return techSupport;
    }
}
