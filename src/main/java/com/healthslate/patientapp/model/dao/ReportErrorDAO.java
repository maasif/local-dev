package com.healthslate.patientapp.model.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

public class ReportErrorDAO extends BaseDAO {
	
	@SuppressWarnings("unchecked")
	public List<Object> getErrors(long userId){

		List<Object> objs = new ArrayList<Object>();

        String allErrors = "";
        if(userId > 0){
            allErrors = "AND r.log.userId = " + userId;
        }
		try{

			Query query = currentSession.createQuery("SELECT r.reportId, l.patient.patientId, r.log.logId, r.description, r.timestamp, l.patient.user.lastName, l.patient.user.firstName"
					+ " FROM "
					+ " ReportError r, Log l "
					+ " WHERE "
					+ " r.timestamp IN ( "
					+ " SELECT"
					+ " MAX(re.timestamp) "
					+ " FROM"
					+ " ReportError re"
					+ " GROUP BY re.log.logId ) AND r.log.logId = l.logId " + allErrors);

			objs = query.list();
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return objs;
	}

    public List<Object> getErrorsByCoachId(long coachId){
        List<Object> objs = new ArrayList<Object>();

        String allErrors = "";
        if(coachId > 0){
            allErrors = "WHERE pro.provider_id = "+coachId;
        }

        try {
            objs = currentSession.createSQLQuery("SELECT re.report_error_id, p.patient_id, l.log_id, re.description, re.`timestamp`, us.first_name, us.last_name " +
                    "FROM report_error re " +
                    "JOIN log l ON l.log_id = re.log_id " +
                    "JOIN patient p ON l.patient_id = p.patient_id " +
                    "JOIN provider_patient pp ON pp.patients_patient_id = p.patient_id " +
                    "JOIN provider pro ON pro.provider_id = pp.providers_provider_id " +
                    "JOIN users us ON us.user_id = p.user_id " +
                    allErrors + " ORDER BY re.`timestamp` DESC ").list();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return objs;
    }

	@SuppressWarnings("unchecked")
	public List<Object> getErrorsByLogId(String logId) {

		List<Object> errorObjs = new ArrayList<Object>();
		
		try{
			
			errorObjs = currentSession.createQuery("SELECT re.description, re.timestamp "
						+ " FROM ReportError re "
						+ " WHERE re.log.logId = '"+logId+"' "
						+" ORDER BY re.timestamp DESC").list();
				
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return errorObjs;
	}
}
