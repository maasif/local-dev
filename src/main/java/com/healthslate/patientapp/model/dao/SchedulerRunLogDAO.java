package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.OneToOneReminderSessionDTO;
import com.healthslate.patientapp.model.entity.OneToOneSession;
import com.healthslate.patientapp.model.entity.SchedulerRunLog;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.DateUtils;
import com.healthslate.patientapp.util.HibernateUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class SchedulerRunLogDAO extends BaseDAO{

    public SchedulerRunLog saveSchedulerLog(String name, String delay){
        Session session = HibernateUtil.getCurrentSession();
        Transaction transaction = null;

        if(session.getTransaction() != null && session.getTransaction().isActive()){
            transaction = session.getTransaction();
        }else{
            transaction = session.beginTransaction();
        }

        session.clear();

        SchedulerRunLog schedulerRunLog = null;

        try {

            schedulerRunLog = new SchedulerRunLog(name, delay);
            session.saveOrUpdate(schedulerRunLog);
            session.flush();
            session.clear();
            transaction.commit();

        }catch(Exception e){
            e.printStackTrace();
            transaction.rollback();
        }

        return schedulerRunLog;
    }
}
