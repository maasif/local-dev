package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.ServerInfo;
import com.healthslate.patientapp.model.entity.UserRole;

import java.util.List;

/**
 * Created by omursaleen on 7/27/2015.
 */
public class ServerInfoDAO extends BaseDAO{

    public List<ServerInfo> listAll() {
        List<ServerInfo> serverInfoList = null;

        try {
            serverInfoList = currentSession.createQuery(" FROM ServerInfo WHERE isActive = true ORDER BY precedence").list();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return serverInfoList;
    }

    public List<ServerInfo> listAll(ServerInfo serverInfo) {
        List<ServerInfo> serverInfoList = null;

        try {
            serverInfoList = currentSession.createQuery(" FROM ServerInfo WHERE serverName <> ? AND isActive = true ORDER BY precedence").setParameter(0, serverInfo.getServerName()).list();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return serverInfoList;
    }

    public ServerInfo getServerInfoByName(String serverName){
        ServerInfo serverInfo = null;

        try {
            serverInfo = (ServerInfo)currentSession.createQuery("FROM ServerInfo WHERE serverName = ?").setParameter(0, serverName).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return serverInfo;
    }
}
