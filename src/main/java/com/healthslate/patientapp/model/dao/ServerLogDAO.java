package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.ServerLogsDTO;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.DateUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.healthslate.patientapp.model.entity.ServerLog;
import com.healthslate.patientapp.util.HibernateUtil;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
* ======= FILE CHANGE HISTORY =======
* [2/12/2015]: Added .list(), .getCount() method for getting all server logs, __oz
* ===================================
 */

public class ServerLogDAO {
	
	Session currentSession;
	public ServerLogDAO(){
		currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
	}

    //[2/12/2015]: changed by __oz
    public int getCount() {

        Object countObj = null;
        int countInt = 0;
        try {
            countObj = currentSession.createSQLQuery("SELECT COUNT(*) FROM server_log sl " +
                    "JOIN users us ON us.user_id = sl.logged_in_user_id " +
                    "ORDER BY sl.log_time DESC").uniqueResult();

            if(countObj != null){
                countInt = Integer.parseInt(countObj.toString());
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            countInt = 0;
        }

        return countInt;
    }

    //[2/12/2015]: changed by __oz
    @SuppressWarnings("unchecked")
	public List<ServerLogsDTO> list(int start, int end) {

        List<ServerLogsDTO> serverLogs = new ArrayList<ServerLogsDTO>();
        List<Object> serverObjs = new ArrayList<Object>();
        try {
            serverObjs = (List<Object>) currentSession.createSQLQuery("SELECT sl.*, us.first_name, us.last_name " +
                    "FROM server_log sl " +
                    "JOIN users us ON us.user_id = sl.logged_in_user_id " +
                    "ORDER BY sl.log_time DESC LIMIT " + start + ","+end+"").list();

            if(serverObjs != null && serverObjs.size() > 0){

                for(Object obj: serverObjs) {
                    Object[] objArray = (Object[]) obj;

                    long serverLogId = Long.parseLong(objArray[0].toString());
                    String actionName = objArray[1].toString();

                    Object descriptionFromDB = objArray[2];
                    String description = "";
                    if(descriptionFromDB != null){
                        description = (String)  descriptionFromDB;
                    }

                    Date dt = (Date)objArray[3];

                    String loggedDate = DateUtils.getFormattedDate(dt.getTime(), DateUtils.DATE_FORMAT);
                    String userRole = objArray[5].toString();
                    String loggedUserName = objArray[7] + ", " + objArray[6];

                    if(userRole.equalsIgnoreCase("["+AppConstants.Roles.ROLE_ADMIN.name()+"]")){
                        userRole = "Admin";
                    }else if(userRole.equalsIgnoreCase("["+AppConstants.Roles.ROLE_PROVIDER.name()+"]")){
                        userRole = "Coach";
                    }else{
                        userRole = "Patient";
                    }

                    serverLogs.add(new ServerLogsDTO(serverLogId, actionName, description, loggedDate, loggedUserName, userRole));
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return serverLogs;
    }

	public void saveServerLog(ServerLog serverLog){

		try{
			currentSession.saveOrUpdate(serverLog);
			currentSession.flush();
			currentSession.clear();
			
		}catch (HibernateException e) {
			e.printStackTrace();
		}
	}
}
