package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.ServerLogsDTO;
import com.healthslate.patientapp.model.entity.ServerLog;
import com.healthslate.patientapp.model.entity.ServiceFilterLog;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.DateUtils;
import com.healthslate.patientapp.util.HibernateUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ServiceFilterLogDAO {

	Session currentSession;
	public ServiceFilterLogDAO(){
		currentSession = HibernateUtil.getSessionFactory().getCurrentSession();
	}
    //[2/12/2015]: changed by __ad
    public String getLatestVersionNo(Long patientId) {
        String versionNo = "N/A";
        try {
            ServiceFilterLog serviceFilterLog = (ServiceFilterLog) currentSession.createQuery("FROM ServiceFilterLog sfl WHERE sfl.patientId = ? ORDER BY sfl.timestamp DESC").setParameter(0, patientId).setMaxResults(1).uniqueResult();
            if(serviceFilterLog != null) {
                versionNo = serviceFilterLog.getVersionNo();
                if (ObjectUtils.isEmpty(versionNo)) {
                    versionNo = "N/A";
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return versionNo;
    }
}
