package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.LogDTO;
import com.healthslate.patientapp.model.dto.PatientDTO;
import com.healthslate.patientapp.model.dto.ShareableMealDTO;
import com.healthslate.patientapp.model.entity.ShareMealPermissions;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.CommonUtils;
import com.healthslate.patientapp.util.DateUtils;
import com.healthslate.patientapp.util.ObjectUtils;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import java.util.ArrayList;
import java.util.List;

public class ShareMealPermissionsDAO extends BaseDAO {

    public ShareMealPermissions getShareMealPermissionsByLogId(Long smpId) {

        ShareMealPermissions smpObj = null;
        try {
            smpObj = (ShareMealPermissions) currentSession.createQuery("Select smp FROM ShareMealPermissions smp WHERE smp.shareMealPermissionsId = ? ")
                    .setParameter(0, smpId)
                    .uniqueResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return smpObj;
    }

    public ShareMealPermissions getShareMealPermissionsByLogId(String logId) {

        ShareMealPermissions smpObj = null;
        try {
            smpObj = (ShareMealPermissions) currentSession.createQuery("Select smp FROM ShareMealPermissions smp WHERE smp.logId = ? ")
                    .setParameter(0, logId)
                    .uniqueResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return smpObj;
    }

    public List<ShareableMealDTO> getShareableMealsList(long providerId,String sSearch,int startLimit,int endLimit,String sortIndex,String sSortDirection) {

        List<ShareableMealDTO> mealsDTO =new ArrayList<ShareableMealDTO>();
        StringBuilder queryString = new StringBuilder();

        try{
            queryString.append("SELECT " +
                    " p.patient_id as patientId, " +
                    " l.log_id as logId, " +
                    " fls.type as logType, " +
                    " fls.carbs as carbs, " +
                    " u.display_name as memberName,  " +
                    " smp.permissions as permission, " +
                    " smp.permission_asked_date as requestSentDate, " +
                    " smp.permission_granted_date as permissionGrantedDate, " +
                    " fls.meal_name as mealName, " +
                    " smp.meal_shared_date AS lastMealSharedDate, " +
                    " smp.meal_shared_count AS mealSharedCount, " +
                    " smp.permission_request_count AS requestSentCount, smp.notes AS notes " +
                    " FROM " +
                    " share_meal_permissions smp " +
                    " LEFT JOIN log l ON l.log_id = smp.log_id " +
                    " LEFT JOIN patient p ON p.patient_id = l.patient_id " +
                    " LEFT JOIN food_log_summary fls ON fls.log_id = l.log_id " +
                    " LEFT JOIN users u ON u.user_id = p.user_id " +
                    " LEFT JOIN food_log_detail fld ON fld.food_log_summary_id = fls.food_log_summary_id " +
                    " WHERE " +
                    " l.is_removed = 0 " +
                    " AND l.log_type = 'Meal' " +
                    " AND l.is_processed = 1 " +
                    " AND l.is_suggested = 0 " +
                    " AND ( " +
                    " l.is_archive = 0 " +
                    " OR l.is_archive IS NULL " +
                    " ) " +
                    " AND " +
                    " smp.provider_id = ?");

            if(!ObjectUtils.isEmpty(sSearch)){
                queryString.append(" AND (p.patient_id LIKE '%"+sSearch+"%'" +
                        " OR l.log_id LIKE '%"+sSearch+"%'" +
                        " OR fls.type LIKE '%"+sSearch+"%'" +
                        " OR fls.meal_name LIKE '%"+sSearch+"%'" +
                        " OR smp.permissions LIKE '%"+sSearch+"%'" +
                        " OR u.display_name LIKE '%"+sSearch+"%' )");

            }

            queryString.append(" GROUP BY l.log_id ");

            if(!ObjectUtils.isEmpty(sortIndex)){
                queryString.append(" ORDER BY " + sortIndex + " " + sSortDirection);
            } else {
                queryString.append(" ORDER By smp.permission_asked_date DESC");
            }

            queryString.append(" LIMIT " + startLimit + "," + endLimit + "");

            List result = currentSession.createSQLQuery(queryString.toString())
                    .addScalar("patientId", LongType.INSTANCE)
                    .addScalar("logId", StringType.INSTANCE)
                    .addScalar("logType", StringType.INSTANCE)
                    .addScalar("carbs", StringType.INSTANCE)
                    .addScalar("memberName", StringType.INSTANCE)
                    .addScalar("permission", StringType.INSTANCE)
                    .addScalar("requestSentDate", LongType.INSTANCE)
                    .addScalar("permissionGrantedDate", LongType.INSTANCE)
                    .addScalar("mealName", StringType.INSTANCE)
                    .addScalar("lastMealSharedDate", LongType.INSTANCE)
                    .addScalar("mealSharedCount", IntegerType.INSTANCE)
                    .addScalar("requestSentCount", IntegerType.INSTANCE)
                    .addScalar("notes", StringType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(ShareableMealDTO.class))
                    .setParameter(0, providerId)
                    .list();

            mealsDTO = (List<ShareableMealDTO>) result;

        }catch(Exception ex){
            ex.printStackTrace();
        }

        return mealsDTO;
    }

    public long getShareableMealsListCount(long providerId,String sSearch) {
        long resultCount = 0l;

        StringBuilder queryString = new StringBuilder();
        List<Object> result = null;
        try{
            queryString.append("SELECT " +
                    " p.patient_id as patientId, " +
                    " l.log_id as logId, " +
                    " fls.type as logType, " +
                    " fls.carbs as carbs, " +
                    " u.display_name as patientName,  " +
                    " l.created_on as createdOn, " +
                    " l.upload_time as onlyTime, " +
                    " l.log_time as logTime" +
                    " FROM " +
                    " share_meal_permissions smp " +
                    " LEFT JOIN log l ON l.log_id = smp.log_id " +
                    " LEFT JOIN patient p ON p.patient_id = l.patient_id " +
                    " LEFT JOIN food_log_summary fls ON fls.log_id = l.log_id " +
                    " LEFT JOIN users u ON u.user_id = p.user_id " +
                    " LEFT JOIN food_log_detail fld ON fld.food_log_summary_id = fls.food_log_summary_id " +
                    " WHERE " +
                    " l.is_removed = 0 " +
                    " AND l.log_type = 'Meal' " +
                    " AND l.is_processed = 1 " +
                    " AND l.is_suggested = 0 " +
                    " AND ( " +
                    " l.is_archive = 0 " +
                    " OR l.is_archive IS NULL " +
                    " ) " +
                    " AND " +
                    " smp.provider_id = " + providerId);

            if(!ObjectUtils.isEmpty(sSearch)){
                queryString.append(" AND (p.patient_id LIKE '%"+sSearch+"%'" +
                        " OR l.log_id LIKE '%"+sSearch+"%'" +
                        " OR fls.type LIKE '%"+sSearch+"%'" +
                        " OR fls.meal_name LIKE '%"+sSearch+"%'" +
                        " OR smp.permissions LIKE '%"+sSearch+"%'" +
                        " OR u.display_name LIKE '%"+sSearch+"%' )");

            }

            queryString.append(" GROUP BY l.log_id ");
            queryString.append(" ORDER By smp.permission_asked_date DESC");

            result = currentSession.createSQLQuery(queryString.toString()).list();
            resultCount = result.size();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return resultCount;
    }
}
