package com.healthslate.patientapp.model.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.healthslate.patientapp.model.entity.SmsReply;
import com.healthslate.patientapp.util.HibernateUtil;

public class SmsReplyDAO extends BaseDAO {

	public void saveOrUpdateSmsReply(SmsReply smsReply){
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = null;
		
		if(session.getTransaction() != null && session.getTransaction().isActive()){
			transaction = session.getTransaction();
		}else{
			transaction = session.beginTransaction();			
		}
		
		session.clear();
		
		try {
			session.saveOrUpdate(smsReply);			
			session.flush();
			session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		}
	}
	
	@SuppressWarnings("unchecked")
	public SmsReply getLatestMessageByPhoneNumber(String phoneNumber){		
		
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = null;
		
		if(session.getTransaction() != null && session.getTransaction().isActive()){
			transaction = session.getTransaction();
		}else{
			transaction = session.beginTransaction();			
		}
		
		session.clear();
		
		List<SmsReply> smsReplies = null;
		SmsReply smsReply = null;
		
		try{
			smsReplies = (List<SmsReply>) session.createQuery("FROM SmsReply sr WHERE sr.fromPhoneNumber = ? "
					+ " ORDER BY sr.sentTime DESC").setParameter(0, phoneNumber).list();
			
			if(smsReplies != null && smsReplies.size() > 0){
				smsReply = smsReplies.get(0);
			}
			
			transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();  
            transaction.rollback();
        } 
		
		return smsReply;
	}
}
