package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.entity.DiabetesType;
import com.healthslate.patientapp.model.entity.Preferences;
import com.healthslate.patientapp.model.entity.SmsTemplate;
import com.healthslate.patientapp.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

/*
* ======= FILE CHANGE HISTORY =======
* [2/5/2015]: Created by __oz
* ===================================
 */

public class SmsTemplateDAO extends BaseDAO{

	@SuppressWarnings("unchecked")
	public List<SmsTemplate> getAllSmsTemplates(){
		
		List<SmsTemplate> smsTemplates = new ArrayList<SmsTemplate>();
		
        try{
            smsTemplates = currentSession.createQuery("SELECT s FROM SmsTemplate s WHERE (s.flag IS NULL OR s.flag = 'TIME') ").list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        
        return smsTemplates;
	}

    public String getSmsTemplateByNameScheduler(String template) {

        Session session = HibernateUtil.getCurrentSession();
        Transaction transaction = null;

        if(session.getTransaction() != null && session.getTransaction().isActive()){
            transaction = session.getTransaction();
        }else{
            transaction = session.beginTransaction();
        }

        session.clear();

        String prefValue = "";
        try{
            SmsTemplate smsTemplate = (SmsTemplate)session.createQuery("FROM SmsTemplate WHERE template = ?").setParameter(0, template).uniqueResult();
            prefValue = smsTemplate.getTemplateHyperlink();

            transaction.commit();
        } catch(Exception ex){
            ex.printStackTrace();
            transaction.rollback();
        }
        return prefValue;
    }

    public String getSmsTemplateByName(String template) {

        String prefValue = "";
        try{
            SmsTemplate smsTemplate = (SmsTemplate)currentSession.createQuery("FROM SmsTemplate WHERE template = ?").setParameter(0, template).uniqueResult();
            prefValue = smsTemplate.getTemplateHyperlink();
        } catch(Exception ex){
            ex.printStackTrace();
        }
        return prefValue;
    }
}
