package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.RecentMealDTO;
import com.healthslate.patientapp.model.dto.SuggestedMealDTO;
import com.healthslate.patientapp.model.entity.Log;
import com.healthslate.patientapp.model.entity.SuggestedLog;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.CommonUtils;
import com.healthslate.patientapp.util.ObjectUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by omursaleen on 3/26/2015.
 */
public class SuggestedLogDAO extends BaseDAO {

    public SuggestedLog getSuggestedLogById(long suggestedLogId){
        SuggestedLog suggestedLog = null;
        try{
            suggestedLog = (SuggestedLog) currentSession.createQuery("Select DISTINCT sl FROM SuggestedLog sl WHERE sl.suggestedLogId = ? ")
                    .setParameter(0, suggestedLogId)
                    .uniqueResult();
        } catch(Exception ex){
            ex.printStackTrace();
        }
        return suggestedLog;
    }

    public SuggestedLog getSuggestedLogByLogId(String logId){
        SuggestedLog suggestedLog = null;
        try{
            suggestedLog = (SuggestedLog) currentSession.createQuery("Select DISTINCT sl FROM SuggestedLog sl WHERE sl.log.logId = ? ")
                    .setParameter(0, logId)
                    .uniqueResult();
        } catch(Exception ex){
            ex.printStackTrace();
        }
        return suggestedLog;
    }

    public List<Log> getAllSuggestedLogsByPatient(long patientId){
        List<Log> suggestedLogs = new ArrayList<Log>();
        try{
            suggestedLogs = (List<Log>) currentSession.createQuery("Select sl.log FROM SuggestedLog sl JOIN sl.log WHERE sl.log.patient.patientId = ? AND sl.log.isSuggested = true AND sl.log.isRemoved = false AND sl.isDrafted = true ORDER BY sl.lastModified DESC")
                    .setParameter(0, patientId).list();
        } catch(Exception ex){
            ex.printStackTrace();
        }

        return suggestedLogs;
    }

    public List<SuggestedMealDTO> getAllSuggestedMeals(long patientId, String type, String appPath){

        List<SuggestedMealDTO> suggestedMealDTOs = null;
        List<Object> objects = null;

        String filtered = " AND fls.type = '" + type + "'";

        if(type.equalsIgnoreCase(AppConstants.LOG_TYPE_MAP.get(0))){
            filtered = "";
        }

        try {

            String query = "SELECT l.suggested_log_id, fi.image_name, l.created_on, l.last_modified, up.log_time_offset, up.offset_key, fls.type, fls.meal_name, fls.carbs, fls.has_missing_food, u.first_name, u.last_name, l.log_id, fls.minor_name, l.is_drafted, fld.no_of_servings, fm.serving_size_unit"
                    + " FROM suggested_log l "
                    + " LEFT JOIN log lg ON lg.log_id = l.log_id"
                    + " LEFT JOIN food_log_summary fls ON fls.log_id = lg.log_id "
                    + " LEFT JOIN food_image fi ON fi.food_log_summary_id = fls.food_log_summary_id "
                    + " LEFT JOIN food_log_detail fld ON fld.food_log_summary_id = fls.food_log_summary_id "
                    + " LEFT JOIN food_master fm ON fm.food_master_id = fld.food_master_id "
                    + " LEFT JOIN provider p ON l.provider_id = p.provider_id "
                    + " LEFT JOIN users u ON p.user_id = u.user_id "
                    + " LEFT JOIN patient pat ON pat.patient_id = " + patientId + ""
                    + " LEFT JOIN users up ON pat.user_id = up.user_id"
                    + " WHERE lg.patient_id = " + patientId + ""
                    + " AND lg.is_removed = 0 "
                    + filtered
                    + " GROUP BY lg.log_id ORDER BY l.last_modified DESC";

            objects = (List<Object>)currentSession.createSQLQuery(query).list();

            if(objects != null && objects.size() > 0){

                suggestedMealDTOs = new ArrayList<SuggestedMealDTO>();

                for (Object object : objects) {
                    Object[] objArray = (Object[]) object;

                    if(objArray != null){

                        long logId = ((BigInteger)objArray[0]).longValue();

                        String imageName = ObjectUtils.nullSafe(objArray[1]);

                        Object logTimeFromDB = objArray[2];
                        long logTime = 0;
                        if(logTimeFromDB != null){
                            logTime = ((BigInteger)logTimeFromDB).longValue();
                        }

                        Object logTimeOffsetFromDB = objArray[4];
                        long logTimeOffset = 0;
                        if(logTimeOffsetFromDB != null){
                            logTimeOffset = ((BigInteger)logTimeOffsetFromDB).longValue();
                        }

                        String offsetKey = ObjectUtils.nullSafe(objArray[5]);
                        String typeFromDB = ObjectUtils.nullSafe(objArray[6]);
                        String mealNameFromDB = ObjectUtils.nullSafe(objArray[7]);


                        Object carbsFromDB = objArray[8];
                        float carbsFrom = 0;
                        if(carbsFromDB != null){
                            carbsFrom = (Float)carbsFromDB;
                        }

                        Object hasMissingFoodFromDB = objArray[9];
                        boolean hasMissingFood = false;
                        if(hasMissingFoodFromDB != null){
                            hasMissingFood = (Boolean)hasMissingFoodFromDB;
                        }

                        String createdBy = ObjectUtils.nullSafe(objArray[10]) + " " + ObjectUtils.nullSafe(objArray[11]);
                        String logIdString = ObjectUtils.nullSafe(objArray[12]);
                        String minorFoodName = ObjectUtils.nullSafe(objArray[13]);

                        String logTimeCalculated = CommonUtils.timeInUserTimezone(logTime, logTimeOffset, offsetKey);
                        String imageSrc = appPath + imageName;

                        Object isDraftedFromDB = objArray[14];
                        boolean isDrafted = false;
                        if(isDraftedFromDB != null){
                            isDrafted = (Boolean)isDraftedFromDB;
                        }

                        String noOfServings = ObjectUtils.nullSafe(objArray[15]);
                        String unit = ObjectUtils.nullSafe(objArray[16]);

                        SuggestedMealDTO suggestedMealDTO = new SuggestedMealDTO(logId, imageSrc, logTimeCalculated, typeFromDB, mealNameFromDB, carbsFrom, hasMissingFood, createdBy, logIdString, minorFoodName, isDrafted);
                        suggestedMealDTO.setNoOfServings(noOfServings);
                        suggestedMealDTO.setUnit(unit);

                        suggestedMealDTOs.add(suggestedMealDTO);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return suggestedMealDTOs;
    }
}
