package com.healthslate.patientapp.model.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.healthslate.patientapp.util.ObjectUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.healthslate.patientapp.model.dto.ReminderSmsDTO;
import com.healthslate.patientapp.model.entity.Target;
import com.healthslate.patientapp.util.HibernateUtil;

public class TargetDAO extends BaseDAO{

	public Target getTargetByPatientId(long patientId) {
		try {
			Target target = (Target)currentSession.createQuery("FROM Target t WHERE t.patient.patientId = ?").setParameter(0, patientId).uniqueResult();
			return target;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}	

	public boolean deleteOldRemindersByTargetId(long targetId) {
		Query query = currentSession.createSQLQuery(" DELETE " +
						" FROM reminder " + 
						" WHERE target_id = '" + targetId +"';");
		
		int result = query.executeUpdate();		
		return (result >= 0 )? true: false;
	}
	
	public boolean deleteOldMealTargetsByTargetId(long targetId) {
		Query query = currentSession.createSQLQuery(" DELETE " +
						" FROM target_meal " + 
						" WHERE target_id = '" + targetId +"';");
		
		int result = query.executeUpdate();		
		return (result >= 0 )? true: false;
	}
	
	public boolean deleteOldMedicationTargetsByTargetId(long targetId) {
		Query query = currentSession.createSQLQuery(" DELETE " +
						" FROM target_medication " + 
						" WHERE target_id = '" + targetId +"';");
		
		int result = query.executeUpdate();		
		return (result >= 0 )? true: false;
	}
	
	public boolean deleteOldGlucoseTargetsByTargetId(long targetId) {
		Query query = currentSession.createSQLQuery(" DELETE " +
						" FROM target_glucose " + 
						" WHERE target_id = '" + targetId +"';");
		
		int result = query.executeUpdate();		
		return (result >= 0 )? true: false;
	}
	
	@SuppressWarnings({ "unchecked" })
	public List<ReminderSmsDTO> getReminderTimes(){
		
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.clear();
		
		List<ReminderSmsDTO> remindersList = new ArrayList<ReminderSmsDTO>();
		List<Object> results = null;
		try {
			
			//targets = (List<Target>)session.createQuery("FROM Target t JOIN fetch t.medTargets tm WHERE tm.shouldReminded = 1").list();
						
			results = (List<Object>)session.createSQLQuery("SELECT t.patient_id, u.phone, tm.time, tm.day, f.timezone_offset_millis "
					+ " FROM targets t "
					+ " JOIN patient p ON t.patient_id = p.patient_id"
					+ " JOIN users u ON p.user_id = u.user_id"
                    + " LEFT JOIN patient_facility pf ON pf.patients_patient_id = p.patient_id "
                    + " LEFT JOIN facility f ON f.facility_id = pf.facilities_facility_id "
					+ " JOIN target_medication tm ON t.target_id = tm.target_id"
					+ "	WHERE tm.should_reminded = 1 AND (p.is_deleted = 0 OR p.is_deleted IS NULL)").list();
			transaction.commit();
			
			for (Object object : results) {
				
				if(object != null){
					Object[] objArray = (Object[]) object;
					if(objArray != null && objArray.length > 0){
						Long patientId = ((BigInteger) objArray[0]).longValue();
						String phoneNumber = (String) objArray[1];
						String time = (String) objArray[2];			
						String day = (String) objArray[3];
                        Long timeMillis = (objArray[4] == null) ? null : ObjectUtils.nullSafeLong(objArray[4]);
						remindersList.add(new ReminderSmsDTO(patientId, phoneNumber, time, day, timeMillis));
					}
				}				
			}
						
			return remindersList;
		}catch(Exception e){
			e.printStackTrace();
			transaction.rollback();
		}
		
		return null;
	}
}
