package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.MessageGcmDTO;
import com.healthslate.patientapp.model.dto.PatientDTO;
import com.healthslate.patientapp.model.dto.TechSupportMessageDTO;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.model.entity.TechSupportMessage;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.ObjectUtils;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class TechSupportMessageDAO extends BaseDAO {

    @SuppressWarnings("unchecked")
    public List<PatientDTO> getAllMessages() {
        List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

        try {
            StringBuilder query = new StringBuilder("SELECT p.patient_id as patientId, p.uuid as uuid, u.email as email, MAX(tsm.timestamp) as invitedDate, ")
                    .append("u.first_name as firstName, u.last_name as lastName, u.display_name as displayName, COUNT(tsm.message_id) as messageCount ")
                    .append("FROM tech_support_message tsm JOIN patient p on tsm.patient_id = p.patient_id ")
                    .append("JOIN users u on p.user_id = u.user_id ")
                    .append("WHERE tsm.read_status = 0 ")
                    .append("GROUP BY p.patient_id ORDER BY MAX(tsm.timestamp) DESC");

            List result = currentSession.createSQLQuery(query.toString())
                    .addScalar("patientId", LongType.INSTANCE)
                    .addScalar("uuid", StringType.INSTANCE)
                    .addScalar("email", StringType.INSTANCE)
                    .addScalar("firstName", StringType.INSTANCE)
                    .addScalar("lastName", StringType.INSTANCE)
                    .addScalar("displayName", StringType.INSTANCE)
                    .addScalar("invitedDate", StringType.INSTANCE)
                    .addScalar("messageCount", IntegerType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(PatientDTO.class))
                    .list();

            patientDTOs = (List<PatientDTO>) result;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return patientDTOs;
    }

    @SuppressWarnings("unchecked")
    public List<TechSupportMessage> getAllMessages(long patientId, boolean isRead) {
        List<TechSupportMessage> tsm = null;

        try {
            tsm = (List<TechSupportMessage>) currentSession.createQuery("SELECT tsm " +
                    "FROM TechSupportMessage tsm " +
                    "WHERE tsm.patient.patientId = ?").setParameter(0, patientId)
                    .list();

            if (isRead) {
                String query = "UPDATE TechSupportMessage tsm SET tsm.readStatus = 1 WHERE tsm.patient.patientId = " + patientId;
                currentSession.createQuery(query).executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return tsm;
    }

    @SuppressWarnings("unchecked")
    public List<TechSupportMessageDTO> getAllMessagesDTO(long patientId, boolean isRead) {
        List<TechSupportMessageDTO> tsm = new ArrayList<TechSupportMessageDTO>();
        StringBuilder selectQuery = new StringBuilder();
        List<Object> objectList;
        try {
            selectQuery.append("SELECT tsm.message_id, tsm.message, tsm.`owner`, tsm.`timestamp`, ")
            .append("tsm.food_log_summary_id, tsm.patient_id, tsm.user_id, u.first_name, u.last_name, ")
            .append("tsm.image_path, tsm.video_link, tsm.notified_techsupport, tsm.read_status, tsm.replied_by ")
            .append("FROM tech_support_message tsm LEFT JOIN users u ON tsm.replied_by = u.user_id ")
            .append("WHERE tsm.patient_id = " + patientId );

            objectList = (List<Object>) currentSession.createSQLQuery(selectQuery.toString()).list();

            if(objectList != null) {
                for(Object obj: objectList){
                    TechSupportMessageDTO dto = new TechSupportMessageDTO();
                    Object[] objArray = (Object[]) obj;
                    dto.setMsgId(((BigInteger) objArray[0]).longValue());
                    dto.setMessage(ObjectUtils.nullSafe((String) objArray[1]));
                    dto.setOwner(ObjectUtils.nullSafe((String) objArray[2]));
                    dto.setTimestamp(((BigInteger) objArray[3]).longValue());
                    dto.setFoodSummaryId(objArray[4] != null ? ((BigInteger) objArray[4]).longValue() : null);
                    dto.setPatientId(((BigInteger) objArray[5]).longValue());
                    dto.setUserId(((BigInteger) objArray[6]).longValue());
//                    dto.setUserFirstName(ObjectUtils.nullSafe((String) objArray[7]));
//                    dto.setUserLastName(ObjectUtils.nullSafe((String) objArray[8]));
                    dto.setImagePath(objArray[9] != null ? ((String) objArray[9]) : "");
                    dto.setVideoLink(objArray[10] != null ? ((String) objArray[10]) : "");
                    dto.setNotifiedTechSupport(objArray[11] != null ? ((String) objArray[11]) : "");
                    dto.setReadStatus((Boolean) objArray[12]);
//                    dto.setRepliedBy(((BigInteger) objArray[13]).longValue());
                    dto.setRepliedFirstUserName(ObjectUtils.nullSafe((String) objArray[7]));
                    dto.setRepliedLastUserName(ObjectUtils.nullSafe((String) objArray[8]));

                    tsm.add(dto);
                }
            }

            if (isRead) {
                String updateQuery = "UPDATE TechSupportMessage tsm SET tsm.readStatus = 1 WHERE tsm.patient.patientId = " + patientId;
                currentSession.createQuery(updateQuery).executeUpdate();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return tsm;
    }

    @SuppressWarnings("unchecked")
    public List<TechSupportMessage> getAllMessagesByTechSupport(Provider techSupport, long patientId, boolean isRead) {
        List<TechSupportMessage> tsm = null;

        try {
            tsm = (List<TechSupportMessage>) currentSession.createQuery("SELECT tsm " +
                    "FROM TechSupportMessage tsm ").list();

            if (isRead) {
                String query = "UPDATE TechSupportMessage tsm SET tsm.read_status = 1 ";
                currentSession.createSQLQuery(query).executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return tsm;
    }

    @SuppressWarnings("unchecked")
    public List<MessageGcmDTO> getAllPatientTechSupportMessages(long patientId) {
        List<MessageGcmDTO> messages = new ArrayList<MessageGcmDTO>();
        List<Object> objectList = new ArrayList<Object>();

        try{
            String queryString = "SELECT tsm.*, pp.provider_id, f.log_id" +
                    " FROM tech_support_message tsm " +
                    " LEFT JOIN users u ON u.user_id = tsm.user_id " +
                    " LEFT JOIN provider pp ON u.user_id = pp.user_id  " +
                    " LEFT JOIN food_log_summary f ON f.food_log_summary_id = tsm.food_log_summary_id " +
                    " WHERE tsm.patient_id =  "+  patientId +
                    " GROUP BY tsm.message_id " +
                    " ORDER BY tsm.timestamp ASC";
            objectList = (List<Object>) currentSession.createSQLQuery(queryString).list();

            if(objectList != null) {
                for(Object obj: objectList){
                    MessageGcmDTO dto = new MessageGcmDTO();
                    Object[] objArray = (Object[]) obj;
                    //dto.setMessageId(((BigInteger)objArray[0]).longValue());
                    dto.setMessageText(ObjectUtils.nullSafe((String)objArray[1]));

                    String owner = ObjectUtils.nullSafe(objArray[2]);
                    if(owner.equalsIgnoreCase(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())){
                        owner = AppConstants.PROVIDER;
                    }
                    dto.setOwner(owner);
                    dto.setIsRead((Boolean)objArray[10]);
                    dto.setTimeStamp(ObjectUtils.nullSafeLong(objArray[3]));
                    dto.setPatientId(ObjectUtils.nullSafeLong(objArray[5]));
                    dto.setImagePath(ObjectUtils.nullSafe((String)objArray[7]));
                    dto.setProviderId(ObjectUtils.nullSafeLong(objArray[12]));
                    dto.setLogId(ObjectUtils.nullSafe((String)objArray[13]));
                    messages.add(dto);
                }
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }
        return  messages;
    }

    @SuppressWarnings("unchecked")
    public List<MessageGcmDTO> getAllMessagesByDate(long patientId, Long dateFrom, Long dateTo) {
        List<MessageGcmDTO> messages = new ArrayList<MessageGcmDTO>();
        List<Object> objectList = new ArrayList<Object>();

        try{
            String queryString = "SELECT tsm.message, tsm.`owner`, tsm.read_status, tsm.`timestamp`, tsm.patient_id, tsm.image_path, pp.provider_id, f.log_id " +
                    "FROM tech_support_message tsm " +
                    "LEFT JOIN users u ON u.user_id = tsm.user_id " +
                    "LEFT JOIN provider pp ON u.user_id = pp.user_id " +
                    "LEFT JOIN food_log_summary f ON f.food_log_summary_id = tsm.food_log_summary_id " +
                    "WHERE tsm.timestamp >= "+  dateFrom + " AND tsm.timestamp <= "+ dateTo +" "+
                    "AND tsm.patient_id = " + patientId +" " +
                    "GROUP BY tsm.message_id " +
                    "ORDER BY tsm.timestamp ASC";
            objectList = (List<Object>) currentSession.createSQLQuery(queryString).list();

            if(objectList != null) {
                for(Object obj: objectList){
                    MessageGcmDTO dto = new MessageGcmDTO();
                    Object[] objArray = (Object[]) obj;
                    //dto.setMessageId(((BigInteger)objArray[0]).longValue());
                    dto.setMessageText(ObjectUtils.nullSafe((String)objArray[0]));
                    String owner = ObjectUtils.nullSafe(objArray[1]);
                    if(owner.equalsIgnoreCase(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())){
                        owner = AppConstants.PROVIDER;
                    }

                    dto.setOwner(owner);
                    dto.setIsRead((Boolean) objArray[2]);
                    dto.setTimeStamp(((BigInteger)objArray[3]).longValue());
                    dto.setPatientId(((BigInteger)objArray[4]).longValue());
                    dto.setImagePath(ObjectUtils.nullSafe((String)objArray[5]));
                    dto.setProviderId(((BigInteger)objArray[6]).longValue());
                    dto.setLogId(ObjectUtils.nullSafe((String) objArray[7]));
                    messages.add(dto);
                }
            }

        } catch(Exception ex){
            ex.printStackTrace();
        }
        return  messages;
    }

    public long getUnreadMessageOfTechSupport(long patientId, long providerId) {
        Object msgCount = null;
        long intCount = 0;
        try{
            String query = "SELECT COUNT(tsm.message_id) AS unreadCount FROM tech_support_message tsm " +
                    " WHERE tsm.`owner` = 'PATIENT'" +
                    " AND tsm.patient_id = " + patientId +
                    " AND tsm.notified_techsupport LIKE '%" + providerId + "%'"+
                    " AND tsm.read_status = 0 GROUP BY tsm.patient_id ";
            msgCount = currentSession.createSQLQuery(query).uniqueResult();

            if(!ObjectUtils.isEmpty(ObjectUtils.nullSafe(msgCount))){
                intCount = Long.parseLong(msgCount+"");
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return intCount;
    }

    public List<PatientDTO> getPatientsLastContactOn(Provider coach){
        List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

        try{
            StringBuilder stringBuilder = new StringBuilder();
            User user = coach.getUser();

            Provider defaultTS = new ProviderDAO().getDefaultTechSupport();

            stringBuilder.append(" SELECT p.patient_id as patientId, p.uuid AS uuid, us.email as email, us.first_name as firstName, us.last_name as lastName, us.display_name as displayName, MAX(m.`timestamp`) AS invitedDate ");
            stringBuilder.append(" FROM patient p ");
            stringBuilder.append(" LEFT JOIN provider_patient pp ON p.patient_id = pp.patients_patient_id ");
            stringBuilder.append(" LEFT JOIN users us ON us.user_id = p.user_id ");
            stringBuilder.append(" LEFT JOIN tech_support_message m ON m.patient_id = p.patient_id ");
            stringBuilder.append(" WHERE pp.providers_provider_id = ? AND (m.user_id = ? OR m.replied_by = ?)");
            stringBuilder.append(" AND m.`owner` = '"+AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue()+"' AND ( p.is_deleted = 0 OR p.is_deleted IS NULL) GROUP BY p.patient_id ORDER BY MAX(m.`timestamp`) ASC ");

            List result = currentSession.createSQLQuery(stringBuilder.toString())
                    .addScalar("patientId", LongType.INSTANCE)
                    .addScalar("uuid", StringType.INSTANCE)
                    .addScalar("email", StringType.INSTANCE)
                    .addScalar("firstName", StringType.INSTANCE)
                    .addScalar("lastName", StringType.INSTANCE)
                    .addScalar("displayName", StringType.INSTANCE)
                    .addScalar("invitedDate", StringType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(PatientDTO.class))
                    .setParameter(0, coach.getProviderId())
                    .setParameter(1, defaultTS.getUser().getUserId())
                    .setParameter(2, user.getUserId())
                    .list();

            patientDTOs = (List<PatientDTO>) result;

        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return patientDTOs;
    }
}