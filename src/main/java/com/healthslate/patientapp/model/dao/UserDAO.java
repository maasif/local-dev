package com.healthslate.patientapp.model.dao;

import com.healthslate.patientapp.model.dto.CoachDTO;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.DateUtils;
import com.healthslate.patientapp.util.HibernateUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.*;

public class UserDAO extends BaseDAO{

	@SuppressWarnings("unchecked")
	public List<User> list() {
		
		List<User> users = currentSession.createQuery("from User").list();
		return users;
	}

	@SuppressWarnings("unchecked")
	public List<CoachDTO> onlyProviders(HttpServletRequest request) {
		List<CoachDTO> ngDTOList = new ArrayList<CoachDTO>();
		List<Object> results = null;

		try {
			results = currentSession.createSQLQuery("SELECT us.user_id, p.provider_id, us.first_name, us.last_name, us.user_type, us.phone, " +
					"us.email, f.facility_id, f.name, p.image_path, p.designation, p.is_sms_enabled, p.is_email_enabled " +
					"FROM provider p " +
                    "LEFT JOIN users us ON p.user_id = us.user_id " +
					"LEFT JOIN facility f ON f.facility_id = p.facility_id " +
                    "WHERE us.user_type <> '"+AppConstants.Roles.ROLE_FACILITY_ADMIN.name()+"' "+
					" AND (p.is_deleted = 0 OR p.is_deleted IS NULL) ORDER BY us.registration_date DESC").list();

			if(results != null && results.size() > 0){
				for(Object obj : results){
					Object[] objArray = (Object[]) obj;

					long userId = ((BigInteger)objArray[0]).longValue();

					Object providerIdFromDB = objArray[1];
					long providerId = 0;
					if(providerIdFromDB != null){
						providerId = ((BigInteger)providerIdFromDB).longValue();
					}

                    String firstName = ObjectUtils.nullSafe(objArray[2]);
                    String lastName = ObjectUtils.nullSafe(objArray[3]);
                    String userType = ObjectUtils.nullSafe(objArray[4]);
                    String phone = ObjectUtils.nullSafe(objArray[5]);
                    String email = ObjectUtils.nullSafe(objArray[6]);

					Object facilityIdFromDB = objArray[7];
					long facilityId = 0;
					if(facilityIdFromDB != null){
						facilityId = ((BigInteger)facilityIdFromDB).longValue();
					}

                    String facilityName = ObjectUtils.nullSafe(objArray[8]);

					String imagePath = "";
					Object imagePathFromDB = (Object)objArray[9];
					if(imagePathFromDB != null){
						imagePath = request.getContextPath() + AppConstants.PROVIDER_IMAGE_FOLDER + (String)imagePathFromDB;
					}

					String designation = ObjectUtils.nullSafe(objArray[10]);
					boolean isSMSEnabled = (objArray[11] == null) ? false: Boolean.parseBoolean(objArray[11]+"");;
					boolean isEmailEnabled = (objArray[12] == null) ? false: Boolean.parseBoolean(objArray[12]+"");;
					
					ngDTOList.add(new CoachDTO(userId, providerId, firstName, lastName, userType, phone, email, facilityId, facilityName, 0, imagePath, designation, isSMSEnabled, isEmailEnabled, 0));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ngDTOList;
	}

	public List<CoachDTO> onlyFacilityProviders(HttpServletRequest request, long facilityIdParam) {
		List<CoachDTO> ngDTOList = new ArrayList<CoachDTO>();
		List<Object> results = null;

		try {
			String query = "SELECT us.user_id, p.provider_id, us.first_name, us.last_name, us.user_type, us.phone, " +
					" us.email, f.facility_id, f.name, p.image_path, p.designation, p.is_sms_enabled, p.is_email_enabled " +
					" FROM provider p " +
					" LEFT JOIN users us ON p.user_id = us.user_id " +
					" LEFT JOIN facility f ON f.facility_id = p.facility_id " +
					" WHERE f.facility_id = " + facilityIdParam + "" +
					" AND us.user_type <> '"+AppConstants.Roles.ROLE_FACILITY_ADMIN.name()+"' "+
					" AND (p.is_deleted = 0 OR p.is_deleted IS NULL) ORDER BY us.registration_date DESC";

			results = currentSession.createSQLQuery(query).list();

			if(results != null && results.size() > 0){
				for(Object obj : results){
					Object[] objArray = (Object[]) obj;

					long userId = ((BigInteger)objArray[0]).longValue();

					Object providerIdFromDB = objArray[1];
					long providerId = 0;
					if(providerIdFromDB != null){
						providerId = ((BigInteger)providerIdFromDB).longValue();
					}

					String firstName = ObjectUtils.nullSafe(objArray[2]);
					String lastName = ObjectUtils.nullSafe(objArray[3]);
					String userType = ObjectUtils.nullSafe(objArray[4]);
					String phone = ObjectUtils.nullSafe(objArray[5]);
					String email = ObjectUtils.nullSafe(objArray[6]);

					Object facilityIdFromDB = objArray[7];
					long facilityId = 0;
					if(facilityIdFromDB != null){
						facilityId = ((BigInteger)facilityIdFromDB).longValue();
					}

					String facilityName = ObjectUtils.nullSafe(objArray[8]);

					String imagePath = "";
					Object imagePathFromDB = (Object)objArray[9];
					if(imagePathFromDB != null){
						imagePath = request.getContextPath() + AppConstants.PROVIDER_IMAGE_FOLDER + (String)imagePathFromDB;
					}

					String designation = ObjectUtils.nullSafe(objArray[10]);
					boolean isSMSEnabled = (objArray[11] == null) ? false: Boolean.parseBoolean(objArray[11]+"");;
					boolean isEmailEnabled = (objArray[12] == null) ? false: Boolean.parseBoolean(objArray[12]+"");;

					ngDTOList.add(new CoachDTO(userId, providerId, firstName, lastName, userType, phone, email, facilityId, facilityName, 0, imagePath, designation, isSMSEnabled, isEmailEnabled, 0));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ngDTOList;
	}

	@SuppressWarnings("unchecked")
	public List<CoachDTO> getCoachesByPatientId(long patientId, boolean showAll) {
		List<CoachDTO> ngDTOList = new ArrayList<CoachDTO>();
		List<Object> results = null;
        String includeAtozCoaches = "";

        if(showAll){
        	String facilityId = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.HEALTHSLATE_FACILITY_ID.getValue());
            includeAtozCoaches = " AND f.facility_id <> " + facilityId + " ";
        }

		try {
			results = currentSession.createSQLQuery("SELECT us.user_id, pro.provider_id, us.first_name, us.last_name, us.user_type, us.phone, " +
					"us.email, f.facility_id, f.name, p.lead_coach_id, pro.image_path, pro.designation, pro.sort_type, pro.is_sms_enabled, pro.is_email_enabled, p.primary_food_coach_id " +
					" FROM provider_patient pp " +
					" JOIN patient p ON pp.patients_patient_id = p.patient_id " +
					" JOIN provider pro ON pp.providers_provider_id = pro.provider_id " +
					" JOIN users us ON us.user_id = pro.user_id " +
					" JOIN facility f ON f.facility_id = pro.facility_id " +
					" WHERE pp.patients_patient_id = "+patientId+" "+
					" AND us.user_type <> '"+AppConstants.Roles.ROLE_FACILITY_ADMIN.name()+"' "+
					 includeAtozCoaches +
					" AND (pro.is_deleted = 0 OR pro.is_deleted IS NULL) AND (pro.is_access_all = 0 OR pro.is_access_all IS NULL) ORDER BY us.registration_date DESC").list();

			if(results != null && results.size() > 0){
				for(Object obj : results){
					Object[] objArray = (Object[]) obj;

					long userId = ((BigInteger)objArray[0]).longValue();
					long providerId = ((BigInteger)objArray[1]).longValue();

					String firstName = (String)objArray[2];
					String lastName = (String)objArray[3];

					String userType = (String)objArray[4];
					String phone = (String)objArray[5];
					String email = (String)objArray[6];

					long facilityId = ((BigInteger)objArray[7]).longValue();
					String facilityName = (String)objArray[8];

					long leadCoachId = 0;
					Object isLeadCoachFromDB = (Object)objArray[9];
					if(isLeadCoachFromDB != null){
						leadCoachId = ((BigInteger)isLeadCoachFromDB).longValue();
					}
					
					String imagePath = "";
					Object imagePathFromDB = (Object)objArray[10];
					if(imagePathFromDB != null){
						imagePath = (String)imagePathFromDB;
					}
					
					String designation = (String)objArray[11];

                    Object sorTypeFromDB = objArray[12];
                    int sortType = -1;
                    if(sorTypeFromDB != null){
                        sortType = (Integer)sorTypeFromDB;
                    }

                    boolean isSMSEnabled = (objArray[13] == null) ? false: Boolean.parseBoolean(objArray[13]+"");
					boolean isEmailEnabled = (objArray[14] == null) ? false: Boolean.parseBoolean(objArray[14]+ "");

					long primaryFoodCoachId = 0;
					Object isPrimaryFoodCoachFromDB = (Object)objArray[15];
					if(isPrimaryFoodCoachFromDB != null){
						primaryFoodCoachId = ((BigInteger)isPrimaryFoodCoachFromDB).longValue();
					}
                    CoachDTO coachDTO = new CoachDTO(userId, providerId, firstName, lastName, userType, phone, email, facilityId, facilityName, leadCoachId, imagePath, designation, isSMSEnabled, isEmailEnabled, primaryFoodCoachId);
                    coachDTO.setSortType(sortType);

					ngDTOList.add(coachDTO);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ngDTOList;
	}

	@SuppressWarnings("unchecked")
	public List<CoachDTO> getCoachesExcludeDefaultTechSupportByPatientId(long patientId, boolean showAll) {
		List<CoachDTO> ngDTOList = new ArrayList<CoachDTO>();
		List<Object> results = null;
		Provider defaultTechSuppport = null;
		ProviderDAO providerDAO = new ProviderDAO();
		String includeAtozCoaches = "";

		if(showAll){
			String facilityId = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.HEALTHSLATE_FACILITY_ID.getValue());
			includeAtozCoaches = " AND f.facility_id <> " + facilityId + " ";
		}

		try {
			results = currentSession.createSQLQuery("SELECT us.user_id, pro.provider_id, us.first_name, us.last_name, us.user_type, us.phone, " +
					"us.email, f.facility_id, f.name, p.lead_coach_id, pro.image_path, pro.designation, pro.sort_type, pro.is_sms_enabled, pro.is_email_enabled, p.primary_food_coach_id " +
					" FROM provider_patient pp " +
					" JOIN patient p ON pp.patients_patient_id = p.patient_id " +
					" JOIN provider pro ON pp.providers_provider_id = pro.provider_id " +
					" JOIN users us ON us.user_id = pro.user_id " +
					" JOIN facility f ON f.facility_id = pro.facility_id " +
					" WHERE pp.patients_patient_id = "+patientId+" "+
					" AND us.user_type <> '"+AppConstants.Roles.ROLE_FACILITY_ADMIN.name()+"' "+
					" AND us.user_type <> '"+AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue()+"' "+
					includeAtozCoaches +
					" AND (pro.is_deleted = 0 OR pro.is_deleted IS NULL) AND (pro.is_access_all = 0 OR pro.is_access_all IS NULL) ORDER BY us.registration_date DESC").list();

			defaultTechSuppport = providerDAO.getDefaultTechSupport();
			CoachDTO defaultTechSupportDTO = getTechSupportCoachDTO(defaultTechSuppport);

			if(results != null && results.size() > 0){
				for(Object obj : results){
					Object[] objArray = (Object[]) obj;

					long userId = ((BigInteger)objArray[0]).longValue();
					long providerId = ((BigInteger)objArray[1]).longValue();

					String firstName = (String)objArray[2];
					String lastName = (String)objArray[3];

					String userType = (String)objArray[4];
					String phone = (String)objArray[5];
					String email = (String)objArray[6];

					long facilityId = ((BigInteger)objArray[7]).longValue();
					String facilityName = (String)objArray[8];

					long leadCoachId = 0;
					Object isLeadCoachFromDB = (Object)objArray[9];
					if(isLeadCoachFromDB != null){
						leadCoachId = ((BigInteger)isLeadCoachFromDB).longValue();
					}

					String imagePath = "";
					Object imagePathFromDB = (Object)objArray[10];
					if(imagePathFromDB != null){
						imagePath = (String)imagePathFromDB;
					}

					String designation = (String)objArray[11];

					Object sorTypeFromDB = objArray[12];
					int sortType = -1;
					if(sorTypeFromDB != null){
						sortType = (Integer)sorTypeFromDB;
					}

					boolean isSMSEnabled = (objArray[13] == null) ? false: Boolean.parseBoolean(objArray[13]+"");
					boolean isEmailEnabled = (objArray[14] == null) ? false: Boolean.parseBoolean(objArray[14]+ "");

					long primaryFoodCoachId = 0;
					Object isPrimaryFoodCoachFromDB = (Object)objArray[15];
					if(isPrimaryFoodCoachFromDB != null){
						primaryFoodCoachId = ((BigInteger)isPrimaryFoodCoachFromDB).longValue();
					}
					CoachDTO coachDTO = new CoachDTO(userId, providerId, firstName, lastName, userType, phone, email, facilityId, facilityName, leadCoachId, imagePath, designation, isSMSEnabled, isEmailEnabled, primaryFoodCoachId);
					coachDTO.setSortType(sortType);

					ngDTOList.add(coachDTO);
				}
			}
			ngDTOList.add(defaultTechSupportDTO);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ngDTOList;
	}

	public User getUserByUsername(String username) {
		
		Session session = HibernateUtil.getCurrentSession();
		Transaction tr = session.beginTransaction();
		User user = (User) session.createQuery("from User where email = ?")
				.setParameter(0, username)
				.uniqueResult();
		
		tr.commit();
		return user;
	}

	public User getUserPatientByUsername(String username) {
		
		User user = (User) currentSession.createQuery("from User where email = ? AND userType = '" + AppConstants.PATIENT+"'")
				.setParameter(0, username)
				.uniqueResult();
		return user;
	}

	public User getUserProviderByUsername(String username) {

		User user = (User) currentSession.createQuery("from User where email = ? AND userType != '" + AppConstants.PATIENT+"'")
				.setParameter(0, username)
				.uniqueResult();
		return user;
	}

	public User getUserByMac(String deviceMacAddress) {
		User user = (User) currentSession.createQuery("from User where deviceMacAddress = ?")
				.setParameter(0, deviceMacAddress)
				.uniqueResult();
		return user;
	} 
	
	public User getUserById(Long userId) {
		
		User user = (User) currentSession.createQuery("from User where userId = ?")
				.setParameter(0, userId)
				.uniqueResult();
		return user;
	}

	public User getUserByPatientId(Long patientId) {

		User user = (User) currentSession.createQuery("from User where User.patient.patientId = ? ")
				.setParameter(0, patientId)
				.uniqueResult();
		return user;
	}

	public User getUserByEmail(String email) {
		
		User user = (User) currentSession.createQuery("from User where email = ?")
				.setParameter(0, email)
				.uniqueResult();
		return user;
	} 
	
	public boolean isUserExist(String email) {
		boolean isUserExist = false;
		User user = (User) currentSession.createQuery("from User where email = ?")
				.setParameter(0, email)
				.uniqueResult();
		if(user != null) {
			isUserExist = true;
		}
		return isUserExist;
	}

	public List<User> getUsersWithNewlyRegistration() {
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = null;
		if(session.getTransaction() != null && session.getTransaction().isActive()){
			transaction = session.getTransaction();
		}else{
			transaction = session.beginTransaction();
		}

		session.clear();

		String date = DateUtils.getThreeDaysOldDate(new Date(), DateUtils.DATE_FORMAT_REGISTRAION);
		List<User> users = null;
		try {
			users = session.createQuery("Select u FROM User u JOIN u.patient p "
					+ " WHERE u.isRegistrationCompleted = true "
					+ " AND p.isThreeDayEmailSent = false AND (p.isDeleted = false OR p.isDeleted IS NULL) ").list();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		}
		return users;
	}

	public Map<String, String> getPatientByMeterSerialNumber(String meterSerialNumber) {
		Object result = null;
		Map<String, String> map = null;
		try {
			result = currentSession.createSQLQuery("SELECT u.first_name, u.last_name, p.patient_id " +
					" FROM users u " +
					" JOIN patient p ON u.user_id = p.user_id " +
					" WHERE p.meter_serial_number = '" + meterSerialNumber + "' AND (p.is_deleted IS NULL OR p.is_deleted = 0)").uniqueResult();

			if(result != null ) {
				Object[] objArray = (Object[]) result;
				if ( objArray != null ) {
					map = new HashMap<String, String>();
					map.put("patientName", objArray[0].toString() + " " + objArray[1].toString());
					map.put("patientId", objArray[2].toString());
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	public CoachDTO getTechSupportCoachDTO(Provider techSupport){

		long userId = techSupport.getUser().getUserId();
		long providerId = techSupport.getProviderId();

		String firstName = techSupport.getUser().getFirstName();
		String lastName = techSupport.getUser().getLastName();

		String userType = techSupport.getType();
		String phone = techSupport.getPhone();
		String email = techSupport.getEmail();

		long facilityId = techSupport.getFacility().getFacilityId();
		String facilityName = techSupport.getFacility().getName();

		long leadCoachId = 0;
		Object isLeadCoachFromDB = techSupport.getFacility().getLeadCoach();
		if(isLeadCoachFromDB != null){
			leadCoachId = ((BigInteger)isLeadCoachFromDB).longValue();
		}

		String imagePath = "";
		Object imagePathFromDB = techSupport.getImagePath();
		if(imagePathFromDB != null){
			imagePath = (String)imagePathFromDB;
		}

		String designation = techSupport.getDesignation();

		boolean isSMSEnabled = techSupport.getIsSMSEnabled();
		boolean isEmailEnabled = techSupport.getIsEmailEnabled();

		long primaryFoodCoachId = 0;
		int sortType = 3; // for Tech Support
		int sorTypeFromDB = techSupport.getSortType();
		if(sorTypeFromDB != 0){
			sortType = sorTypeFromDB;
		}

		CoachDTO defaultTechSupport = new CoachDTO(userId, providerId, firstName, lastName, userType, phone, email, facilityId, facilityName, leadCoachId,
				imagePath, designation, isSMSEnabled, isEmailEnabled, primaryFoodCoachId);
		defaultTechSupport.setSortType(sortType);

		return defaultTechSupport;
	}

	public List<CoachDTO> getCoachesExcludeDefaultTechSupportByUUID(String uuid, boolean showAll) {
		List<CoachDTO> ngDTOList = new ArrayList<CoachDTO>();
		List<Object> results = null;
		Provider defaultTechSuppport = null;
		ProviderDAO providerDAO = new ProviderDAO();
		String includeAtozCoaches = "";

		if(showAll){
			String facilityId = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.HEALTHSLATE_FACILITY_ID.getValue());
			includeAtozCoaches = " AND f.facility_id <> " + facilityId + " ";
		}

		try {
			results = currentSession.createSQLQuery("SELECT us.user_id, pro.provider_id, us.first_name, us.last_name, us.user_type, us.phone, " +
					"us.email, f.facility_id, f.name, p.lead_coach_id, pro.image_path, pro.designation, pro.sort_type, pro.is_sms_enabled, pro.is_email_enabled, p.primary_food_coach_id " +
					" FROM provider_patient pp " +
					" JOIN patient p ON pp.patients_patient_id = p.patient_id " +
					" JOIN provider pro ON pp.providers_provider_id = pro.provider_id " +
					" JOIN users us ON us.user_id = pro.user_id " +
					" JOIN facility f ON f.facility_id = pro.facility_id " +
					" WHERE p.uuid = '"+uuid+"' "+
					" AND us.user_type <> '"+AppConstants.Roles.ROLE_FACILITY_ADMIN.name()+"' "+
					" AND us.user_type <> '"+AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue()+"' "+
					includeAtozCoaches +
					" AND (pro.is_deleted = 0 OR pro.is_deleted IS NULL) AND (pro.is_access_all = 0 OR pro.is_access_all IS NULL) ORDER BY us.registration_date DESC").list();

			defaultTechSuppport = providerDAO.getDefaultTechSupport();
			CoachDTO defaultTechSupportDTO = getTechSupportCoachDTO(defaultTechSuppport);

			if(results != null && results.size() > 0){
				for(Object obj : results){
					Object[] objArray = (Object[]) obj;

					long userId = ((BigInteger)objArray[0]).longValue();
					long providerId = ((BigInteger)objArray[1]).longValue();

					String firstName = (String)objArray[2];
					String lastName = (String)objArray[3];

					String userType = (String)objArray[4];
					String phone = (String)objArray[5];
					String email = (String)objArray[6];

					long facilityId = ((BigInteger)objArray[7]).longValue();
					String facilityName = (String)objArray[8];

					long leadCoachId = 0;
					Object isLeadCoachFromDB = (Object)objArray[9];
					if(isLeadCoachFromDB != null){
						leadCoachId = ((BigInteger)isLeadCoachFromDB).longValue();
					}

					String imagePath = "";
					Object imagePathFromDB = (Object)objArray[10];
					if(imagePathFromDB != null){
						imagePath = (String)imagePathFromDB;
					}

					String designation = (String)objArray[11];

					Object sorTypeFromDB = objArray[12];
					int sortType = -1;
					if(sorTypeFromDB != null){
						sortType = (Integer)sorTypeFromDB;
					}

					boolean isSMSEnabled = (objArray[13] == null) ? false: Boolean.parseBoolean(objArray[13]+"");
					boolean isEmailEnabled = (objArray[14] == null) ? false: Boolean.parseBoolean(objArray[14]+ "");

					long primaryFoodCoachId = 0;
					Object isPrimaryFoodCoachFromDB = (Object)objArray[15];
					if(isPrimaryFoodCoachFromDB != null){
						primaryFoodCoachId = ((BigInteger)isPrimaryFoodCoachFromDB).longValue();
					}
					CoachDTO coachDTO = new CoachDTO(userId, providerId, firstName, lastName, userType, phone, email, facilityId, facilityName, leadCoachId, imagePath, designation, isSMSEnabled, isEmailEnabled, primaryFoodCoachId);
					coachDTO.setSortType(sortType);

					ngDTOList.add(coachDTO);
				}
			}
			ngDTOList.add(defaultTechSupportDTO);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ngDTOList;
	}
}
