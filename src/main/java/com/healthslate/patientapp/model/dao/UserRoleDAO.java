package com.healthslate.patientapp.model.dao;


import com.healthslate.patientapp.model.entity.UserRole;

public class UserRoleDAO extends BaseDAO{

    public UserRole getUserRoleById(long userId){
        UserRole userRole = null;

        try {
            userRole = (UserRole)currentSession.createQuery("SELECT ur FROM UserRole ur JOIN ur.user u ON u.userId = ?").setParameter(0, userId).uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return userRole;
    }
}
