package com.healthslate.patientapp.model.dto;

import com.google.gson.annotations.Expose;

/*
* ======= FILE CHANGE HISTORY =======
* [2/2/2015]: Created by __oz
* ===================================
 */

public class CoachDTO {

    @Expose
    private long userId;

    @Expose
    private long providerId;

    @Expose
    private String firstName;

    @Expose
    private String lastName;

    @Expose
    private String type;

    @Expose
    private String phone;

    @Expose
    private String email;

    @Expose
    private long facilityId;

    @Expose
    private String facilityName;

    @Expose
    private long leadCoachId;

    @Expose
    private long primaryFoodCoachId;

    @Expose
    private String imagePath;
    
    @Expose
    private String designation;

    @Expose
    private int sortType;
    
    @Expose
    private boolean isSMSEnabled;
    
    @Expose
    private boolean isEmailEnabled;

    @Expose
    private boolean isAccessAll;

    public CoachDTO(){

    }

    public CoachDTO(long userId, long providerId, String firstName, String lastName, String userType, String phone, String email, 
    		long facilityId, String facilityName, long leadCoachId, String imagePath, String designation, boolean isSMSEnabled, boolean isEmailEnabled, long primaryFoodCoachId) {
        this.userId = userId;
        this.providerId = providerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.type = userType;
        this.phone = phone;
        this.email = email;
        this.facilityId = facilityId;
        this.facilityName = facilityName;
        this.leadCoachId = leadCoachId;
        this.imagePath = imagePath;
        this.designation = designation;
        this.isSMSEnabled = isSMSEnabled;
        this.isEmailEnabled = isEmailEnabled;
        this.primaryFoodCoachId = primaryFoodCoachId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getProviderId() {
        return providerId;
    }

    public void setProviderId(long providerId) {
        this.providerId = providerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(long facilityId) {
        this.facilityId = facilityId;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public long getLeadCoachId() {
        return leadCoachId;
    }

    public void setLeadCoachId(long isLeadCoach) {
        this.leadCoachId = isLeadCoach;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getSortType() {
        return sortType;
    }

    public void setSortType(int sortType) {
        this.sortType = sortType;
    }

    public long getPrimaryFoodCoachId() {
        return primaryFoodCoachId;
    }

    public void setPrimaryFoodCoachId(long primaryFoodCoachId) {
        this.primaryFoodCoachId = primaryFoodCoachId;
    }

    public boolean getIsAccessAll() {
        return isAccessAll;
    }

    public void setIsAccessAll(boolean isAccessAll) {
        this.isAccessAll = isAccessAll;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public boolean isSMSEnabled() {
        return isSMSEnabled;
    }

    public void setSMSEnabled(boolean isSMSEnabled) {
        this.isSMSEnabled = isSMSEnabled;
    }

    public boolean isEmailEnabled() {
        return isEmailEnabled;
    }

    public void setEmailEnabled(boolean isEmailEnabled) {
        this.isEmailEnabled = isEmailEnabled;
    }

    public boolean isAccessAll() {
        return isAccessAll;
    }

    public void setAccessAll(boolean isAccessAll) {
        this.isAccessAll = isAccessAll;
    }
}
