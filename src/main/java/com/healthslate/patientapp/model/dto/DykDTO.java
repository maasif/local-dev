package com.healthslate.patientapp.model.dto;

public class DykDTO {

	
	
	Long adviceId ;
	String advice ;
	public Long getAdviceId() {
		return adviceId;
	}
	public void setAdviceId(Long adviceId) {
		this.adviceId = adviceId;
	}
	public String getAdvice() {
		return advice;
	}
	public void setAdvice(String advice) {
		this.advice = advice;
	}
	
	
}
