package com.healthslate.patientapp.model.dto;

import com.google.gson.annotations.Expose;

/**
 * Created by i.nasim on 23-Jul-15.
 */
public class ExistingPatientSignUpDTO {

    @Expose
    String serialNo;

    @Expose
    private String email;

    public ExistingPatientSignUpDTO(String serialNo, String email) {
        this.serialNo = serialNo;
        this.email = email;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
