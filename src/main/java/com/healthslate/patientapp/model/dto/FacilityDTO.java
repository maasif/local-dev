package com.healthslate.patientapp.model.dto;

import com.google.gson.annotations.Expose;

/**
 * Created by omursaleen on 6/16/2015.
 */
public class FacilityDTO {

    @Expose
    private long facilityId;

    @Expose
    private String state;

    @Expose
    private String city;

    @Expose
    private String zip;

    @Expose
    private String address;

    @Expose
    private String facilityName;

    @Expose
    private String contactPersonName;

    @Expose
    private String contactInfo;

    @Expose
    private boolean isNotificationEnabled;

    @Expose
    private boolean isSkipConsent;

    @Expose
    private String facilityAdminName;

    @Expose
    private String facilityAdminEmail;

    @Expose
    private String facilityAdminFirstName;

    @Expose
    private String facilityAdminLastName;

    @Expose
    private String facilityAdminPhone;

    @Expose
    private long userId;

    @Expose
    private String logo;

    @Expose
    private String slogan;

    @Expose
    private String emailTextName;

    @Expose
    private int groupId;

    @Expose
    private String timezoneName;

    @Expose
    private String timezoneOffset;

    @Expose
    private String consentFileName;

    @Expose
    private long leadCoach;

    public FacilityDTO(){}

    public FacilityDTO(long facilityId, String facilityName, String contactPersonName, String contactInfo, boolean isNotificationEnabled, boolean isSkipConsent, String facilityAdminName, String facilityAdminEmail) {
        this.facilityId = facilityId;
        this.facilityName = facilityName;
        this.contactPersonName = contactPersonName;
        this.contactInfo = contactInfo;
        this.isNotificationEnabled = isNotificationEnabled;
        this.isSkipConsent = isSkipConsent;
        this.facilityAdminName = facilityAdminName;
        this.facilityAdminEmail = facilityAdminEmail;
    }

    public long getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(long facilityId) {
        this.facilityId = facilityId;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public boolean isNotificationEnabled() {
        return isNotificationEnabled;
    }

    public void setNotificationEnabled(boolean isNotificationEnabled) {
        this.isNotificationEnabled = isNotificationEnabled;
    }

    public boolean isSkipConsent() {
        return isSkipConsent;
    }

    public void setSkipConsentForm(boolean isSkipConsent) {
        this.isSkipConsent = isSkipConsent;
    }

    public String getFacilityAdminName() {
        return facilityAdminName;
    }

    public void setFacilityAdminName(String facilityAdminName) {
        this.facilityAdminName = facilityAdminName;
    }

    public String getFacilityAdminEmail() {
        return facilityAdminEmail;
    }

    public void setFacilityAdminEmail(String facilityAdminEmail) {
        this.facilityAdminEmail = facilityAdminEmail;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFacilityAdminFirstName() {
        return facilityAdminFirstName;
    }

    public void setFacilityAdminFirstName(String facilityAdminFirstName) {
        this.facilityAdminFirstName = facilityAdminFirstName;
    }

    public String getFacilityAdminLastName() {
        return facilityAdminLastName;
    }

    public void setFacilityAdminLastName(String facilityAdminLastName) {
        this.facilityAdminLastName = facilityAdminLastName;
    }

    public String getFacilityAdminPhone() {
        return facilityAdminPhone;
    }

    public void setFacilityAdminPhone(String facilityAdminPhone) {
        this.facilityAdminPhone = facilityAdminPhone;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setSkipConsent(boolean isSkipConsent) {
        this.isSkipConsent = isSkipConsent;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getEmailTextName() {
        return emailTextName;
    }

    public void setEmailTextName(String emailTextName) {
        this.emailTextName = emailTextName;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getTimezoneName() {
        return timezoneName;
    }

    public void setTimezoneName(String timezoneName) {
        this.timezoneName = timezoneName;
    }

    public String getTimezoneOffset() {
        return timezoneOffset;
    }

    public void setTimezoneOffset(String timezoneOffset) {
        this.timezoneOffset = timezoneOffset;
    }

    @Override
    public String toString() {
        return "FacilityDTO{" +
                "facilityId=" + facilityId +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", zip='" + zip + '\'' +
                ", address='" + address + '\'' +
                ", facilityName='" + facilityName + '\'' +
                ", contactPersonName='" + contactPersonName + '\'' +
                ", contactInfo='" + contactInfo + '\'' +
                ", isNotificationEnabled=" + isNotificationEnabled +
                ", isSkipConsent=" + isSkipConsent +
                ", facilityAdminName='" + facilityAdminName + '\'' +
                ", facilityAdminEmail='" + facilityAdminEmail + '\'' +
                ", facilityAdminFirstName='" + facilityAdminFirstName + '\'' +
                ", facilityAdminLastName='" + facilityAdminLastName + '\'' +
                ", facilityAdminPhone='" + facilityAdminPhone + '\'' +
                ", userId=" + userId +
                ", logo='" + logo + '\'' +
                ", slogan='" + slogan + '\'' +
                ", emailTextName='" + emailTextName + '\'' +
                ", groupId=" + groupId +
                '}';
    }

    public String getConsentFileName() {
        return consentFileName;
    }

    public void setConsentFileName(String consentFileName) {
        this.consentFileName = consentFileName;
    }

    public long getLeadCoach() {
        return leadCoach;
    }

    public void setLeadCoach(long leadCoach) {
        this.leadCoach = leadCoach;
    }
}
