package com.healthslate.patientapp.model.dto;

public class FeedbackLogsDTO {

    private long feedbackLogId;
    private long patientId;
    private String description;
    private String loggedDate;
    private String firstName; 
    private String type;
    private String providerName;

    public FeedbackLogsDTO(){

    }

    public FeedbackLogsDTO(long feedbackLogId, long patientId, String description, String loggedDate, String firstName, String type) {
        this.feedbackLogId = feedbackLogId;
        this.patientId = patientId;
        this.description = description;
        this.loggedDate = loggedDate;
        this.firstName = firstName;
        this.type = type;
    }

    public FeedbackLogsDTO(long feedbackLogId, String description, String loggedDate, String firstName, String type, String providerName) {
        this.feedbackLogId = feedbackLogId;
        this.description = description;
        this.loggedDate = loggedDate;
        this.firstName = firstName;
        this.type = type;
        this.providerName = providerName;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLoggedDate() {
        return loggedDate;
    }

    public void setLoggedDate(String loggedDate) {
        this.loggedDate = loggedDate;
    }

	public long getFeedbackLogId() {
		return feedbackLogId;
	}

	public void setFeedbackLogId(long feedbackLogId) {
		this.feedbackLogId = feedbackLogId;
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
}
