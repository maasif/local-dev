package com.healthslate.patientapp.model.dto;

public class FoodDetailDTO {

	
	private Long foodMasterId;
	private String foodName;
	private Float numberOfServings;
	private float fats;
	private float carbs;
	private float protein;
	private String servingSizeUnit;
	public Long getFoodMasterId() {
		return foodMasterId;
	}
	public void setFoodMasterId(Long foodMasterId) {
		this.foodMasterId = foodMasterId;
	}
	public String getFoodName() {
		return foodName;
	}
	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}
	public Float getNumberOfServings() {
		return numberOfServings;
	}
	public void setNumberOfServings(Float numberOfServings) {
		this.numberOfServings = numberOfServings;
	}
	public float getFats() {
		return fats;
	}
	public void setFats(float fats) {
		this.fats = fats;
	}
	public float getCarbs() {
		return carbs;
	}
	public void setCarbs(float carbs) {
		this.carbs = carbs;
	}
	public float getProtein() {
		return protein;
	}
	public void setProtein(float protein) {
		this.protein = protein;
	}
	public String getServingSizeUnit() {
		return servingSizeUnit;
	}
	public void setServingSizeUnit(String servingSizeUnit) {
		this.servingSizeUnit = servingSizeUnit;
	}
	
	
	
}
