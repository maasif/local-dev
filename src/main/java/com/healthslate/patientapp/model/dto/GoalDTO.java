package com.healthslate.patientapp.model.dto;

import com.google.gson.annotations.Expose;
import com.healthslate.patientapp.model.entity.FormData;

import java.util.List;

public class GoalDTO {

    @Expose
	private long filledFormId;

    @Expose
	private long formDataId;

    @Expose
	private int rating;

    @Expose
	private boolean shouldRemind;

    @Expose
	private String categoryName;

    @Expose
	private String goalName;

    @Expose
	private String actionPlan;

    @Expose
	private String createdDate;

    @Expose
	private String challenge;

    @Expose
    private String overcomingChallenge;

    @Expose
    private String reward;

    @Expose
    private String textColor;

    @Expose
    private String bgColor;

    public long getFilledFormId() {
        return filledFormId;
    }

    public void setFilledFormId(long filledFormId) {
        this.filledFormId = filledFormId;
    }

    public long getFormDataId() {
        return formDataId;
    }

    public void setFormDataId(long formDataId) {
        this.formDataId = formDataId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public boolean isShouldRemind() {
        return shouldRemind;
    }

    public void setShouldRemind(boolean shouldRemind) {
        this.shouldRemind = shouldRemind;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getGoalName() {
        return goalName;
    }

    public void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    public String getActionPlan() {
        return actionPlan;
    }

    public void setActionPlan(String actionPlan) {
        this.actionPlan = actionPlan;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    public String getOvercomingChallenge() {
        return overcomingChallenge;
    }

    public void setOvercomingChallenge(String overcomingChallenge) {
        this.overcomingChallenge = overcomingChallenge;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }
}
