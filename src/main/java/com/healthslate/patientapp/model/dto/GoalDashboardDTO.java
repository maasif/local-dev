package com.healthslate.patientapp.model.dto;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.healthslate.patientapp.model.entity.CoachGoalAssessment;
import com.healthslate.patientapp.model.entity.FormData;

public class GoalDashboardDTO {

	@Expose
	private long filledFormId;
	
	@Expose
	private String goalCategoryName;
	
	@Expose
	private String goalName;
	
	@Expose
	private String actionPlan;
	
	@Expose
	private String goalCreatedDate;
	
	@Expose
	private int goalRating;
	
	@Expose
	private String goalRatedOn;
	
	@Expose
	private String coachStatus;
	
	@Expose
	private String coachAssessedDate;
	
	@Expose
	private String notes;
	
	@Expose
	private List<FormData> formDataList;
	
	@Expose
	private List<CoachGoalAssessment> coachGoalAssessments;
	
	@Expose
	private String assessedBy;
	
	public long getFilledFormId() {
		return filledFormId;
	}
	public void setFilledFormId(long filledFormId) {
		this.filledFormId = filledFormId;
	}
	public String getGoalCategoryName() {
		return goalCategoryName;
	}
	public void setGoalCategoryName(String goalCategoryName) {
		this.goalCategoryName = goalCategoryName;
	}
	public String getGoalName() {
		return goalName;
	}
	public void setGoalName(String goalName) {
		this.goalName = goalName;
	}
	public String getActionPlan() {
		return actionPlan;
	}
	public void setActionPlan(String actionPlan) {
		this.actionPlan = actionPlan;
	}
	public String getGoalCreatedDate() {
		return goalCreatedDate;
	}
	public void setGoalCreatedDate(String goalCreatedDate) {
		this.goalCreatedDate = goalCreatedDate;
	}
	public int getGoalRating() {
		return goalRating;
	}
	public void setGoalRating(int goalRating) {
		this.goalRating = goalRating;
	}
	public String getGoalRatedOn() {
		return goalRatedOn;
	}
	public void setGoalRatedOn(String goalRatedOn) {
		this.goalRatedOn = goalRatedOn;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public List<FormData> getFormDataList() {
		return formDataList;
	}
	public void setFormDataList(List<FormData> formDataList) {
		this.formDataList = formDataList;
	}
	public String getCoachAssessedDate() {
		return coachAssessedDate;
	}
	public void setCoachAssessedDate(String coachAssessedDate) {
		this.coachAssessedDate = coachAssessedDate;
	}
	public String getCoachStatus() {
		return coachStatus;
	}
	public void setCoachStatus(String coachStatus) {
		this.coachStatus = coachStatus;
	}
	public List<CoachGoalAssessment> getCoachGoalAssessments() {
		return coachGoalAssessments;
	}
	public void setCoachGoalAssessments(List<CoachGoalAssessment> coachGoalAssessments) {
		this.coachGoalAssessments = coachGoalAssessments;
	}
	public String getAssessedBy() {
		return assessedBy;
	}
	public void setAssessedBy(String assessedBy) {
		this.assessedBy = assessedBy;
	}
}
