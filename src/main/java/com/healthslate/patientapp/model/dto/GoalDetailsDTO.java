package com.healthslate.patientapp.model.dto;

public class GoalDetailsDTO {

	
	private long formId;
	private String goalName;
	private long createdOn;
	private long ratedOn;
	private int rating;
	private String goalStatus;
	
	public long getFormId() {
		return formId;
	}
	public String getGoalName() {
		return goalName;
	}
	public long getCreatedOn() {
		return createdOn;
	}
	public int getRating() {
		return rating;
	}
	public void setFormId(long formId) {
		this.formId = formId;
	}
	public void setGoalName(String goalName) {
		this.goalName = goalName;
	}
	public void setCreatedOn(long createdOn) {
		this.createdOn = createdOn;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public long getRatedOn() {
		return ratedOn;
	}
	public void setRatedOn(long ratedOn) {
		this.ratedOn = ratedOn;
	}
	/**
	 * @return the goalStatus
	 */
	public String getGoalStatus() {
		return goalStatus;
	}
	/**
	 * @param goalStatus the goalStatus to set
	 */
	public void setGoalStatus(String goalStatus) {
		this.goalStatus = goalStatus;
	}
}
