package com.healthslate.patientapp.model.dto;

/**
 * Created by omursaleen on 6/30/2015.
 */
public class GoalRatingDTO {

    private long formId;
    private String goalName;
    private int rating;
    private String ratedOn;

    public GoalRatingDTO(){}
    
    public GoalRatingDTO(long formId, int rating, String ratedOn) {
        this.formId = formId;
        this.rating = rating;
        this.ratedOn = ratedOn;
    }

    public long getFormId() {
        return formId;
    }

    public void setFormId(long formId) {
        this.formId = formId;
    }

    public String getGoalName() {
        return goalName;
    }

    public void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getRatedOn() {
        return ratedOn;
    }

    public void setRatedOn(String ratedOn) {
        this.ratedOn = ratedOn;
    }
}
