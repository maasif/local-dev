package com.healthslate.patientapp.model.dto;

import java.util.Date;
import java.util.List;

import com.healthslate.patientapp.model.entity.FormData;

public class GoalSummaryDTO {

	private long filledFormId;
	private long formDataId;
	private int rating;
	private boolean shouldRemind;
	private String categoryName;
	private String goalName;
	private String actionPlan;	
	private List<FormData> formDataList;
	private String createdDate;
	
	public long getFilledFormId() {
		return filledFormId;
	}
	public void setFilledFormId(long filledFormId) {
		this.filledFormId = filledFormId;
	}
	public long getFormDataId() {
		return formDataId;
	}
	public void setFormDataId(long formDataId) {
		this.formDataId = formDataId;
	}	
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public boolean isShouldRemind() {
		return shouldRemind;
	}
	public void setShouldRemind(boolean shouldRemind) {
		this.shouldRemind = shouldRemind;
	}
	public List<FormData> getFormDataList() {
		return formDataList;
	}
	public void setFormDataList(List<FormData> formDataList) {
		this.formDataList = formDataList;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getGoalName() {
		return goalName;
	}
	public void setGoalName(String goalName) {
		this.goalName = goalName;
	}
	public String getActionPlan() {
		return actionPlan;
	}
	public void setActionPlan(String actionPlan) {
		this.actionPlan = actionPlan;
	}	
}
