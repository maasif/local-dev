package com.healthslate.patientapp.model.dto;

public class GroupsDTO {

	private int groupId;
	private String groupName;
	private int groupMembers;
	private int likes;
	private String description;
	private String avatarPath;
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public int getGroupId() {
		return groupId;
	}
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	public int getGroupMembers() {
		return groupMembers;
	}
	public void setGroupMembers(int groupMembers) {
		this.groupMembers = groupMembers;
	}
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAvatarPath() {
		return avatarPath;
	}
	public void setAvatarPath(String avatarPath) {
		this.avatarPath = avatarPath;
	}
}
