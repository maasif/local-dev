package com.healthslate.patientapp.model.dto;

public class HabitDetailsDTO {

	private long formId;
	private String triggeringActivity;
	private String healthierHabit;
	private String celebrate;
	private long createdOn;
	private boolean shouldRemind;
	
	public long getFormId() {
		return formId;
	}
	public String getTriggeringActivity() {
		return triggeringActivity;
	}
	public String getHealthierHabit() {
		return healthierHabit;
	}
	public String getCelebrate() {
		return celebrate;
	}
	public long getCreatedOn() {
		return createdOn;
	}
	public void setFormId(long formId) {
		this.formId = formId;
	}
	public void setTriggeringActivity(String triggeringActivity) {
		this.triggeringActivity = triggeringActivity;
	}
	public void setHealthierHabit(String healthierHabit) {
		this.healthierHabit = healthierHabit;
	}
	public void setCelebrate(String celebrate) {
		this.celebrate = celebrate;
	}
	public void setCreatedOn(long createdOn) {
		this.createdOn = createdOn;
	}
	public boolean isShouldRemind() {
		return shouldRemind;
	}
	public void setShouldRemind(boolean shouldRemind) {
		this.shouldRemind = shouldRemind;
	}
}
