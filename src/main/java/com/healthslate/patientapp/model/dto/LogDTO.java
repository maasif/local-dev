package com.healthslate.patientapp.model.dto;

public class LogDTO {
	
	private boolean isOtherLog ;
	private boolean isMealLog ;
	private String logType ;
	private String status;
	private String message;
	
	// from patient	
	private String firstName;	
	private String lastName;
	

	private String patientName; 
	private Long patientId;
		
	/*from log*/
	private String logId;
	private Long logTime;
	private Long createdOn;


	private String logTimeString ;
	private Boolean isProcessed;
	private String isProccessedString ;
		
	/*Misc Log*/
	private int bloodPressureSystolic;
	private int bloodPressureDiastolic;
	private String Bp ;
	private String weight;
	private String stepsPerDay;
	private String tookMeds;
		
	/* Glucose */
	private String glucoseLevel;
	
	/*From FoodLogSummery*/
	private String carbs;
	private String MealType;
	
	private String minutesHours;
	
	private String coachName;

	private boolean isSigned;


    private String onlyDate;
    private String onlyTime;

    //activity log properties, 'al' stands for Activity Log
    private int alIntensity;
    private String alType;
    private int alMinutesPerformed;
    private String alNotes;

	//glucose Type, After/Before Meal
	private String gType;
	public String getMealType() {
		return MealType;
	}

	public void setMealType(String mealType) {
		MealType = mealType;
	}

	/*from  exercise table*/
	private String Cardio;
	private String Strength;
	
	/*Calculate after list get populated*/
	private int unreadMessageCount ;

	public String getLogId() {
		return logId;
	}

	public void setLogId(String logId) {
		this.logId = logId;
	}

	public Long getLogTime() {
		return logTime;
	}

	public void setLogTime(Long logTime) {
		this.logTime = logTime;		
	}

	public Boolean getIsProcessed() {
		return isProcessed;
	}

	public void setIsProcessed(Boolean isProcessed) {
		this.isProcessed = isProcessed;
		if(this.isProcessed != null && this.isProcessed){
			setIsProccessedString("Yes");
		}else{
			setIsProccessedString("No");
		}
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public int getBloodPressureSystolic() {
		return bloodPressureSystolic;
	}

	public void setBloodPressureSystolic(int bloodPressureSystolic) {
		this.bloodPressureSystolic = bloodPressureSystolic;
	}

	public int getBloodPressureDiastolic() {
		return bloodPressureDiastolic;
	}

	public void setBloodPressureDiastolic(int bloodPressureDiastolic) {
		this.bloodPressureDiastolic = bloodPressureDiastolic;
	}

	public String getCarbs() {
		return carbs;
	}

	public void setCarbs(String carbs) {
		this.carbs = carbs;
	}

	public String getCardio() {
		return Cardio;
	}

	public void setCardio(String cardio) {
		
		
		if(cardio.isEmpty())
		{
			Cardio = "N/A";
		}
		else{
			Cardio = cardio;
		}
		Cardio = cardio;
	}

	public String getStrength() {
		return Strength;
	}

	public void setStrength(String strength) {
		if(strength.isEmpty())
		{
			Strength = "N/A";
		}
		else{
		Strength = strength;
		}
	}

	public int getUnreadMessageCount() {
		return unreadMessageCount;
	}

	public void setUnreadMessageCount(int unreadMessageCount) {
		this.unreadMessageCount = unreadMessageCount;
	}
	
	public String getLogTimeString() {
		return logTimeString;
	}

	public void setLogTimeString(String logTimeString) {
		this.logTimeString = logTimeString;
	}
	
	
	public String getBp() {
		return Bp;
	}

	public void setBp(String bp) {
		Bp = bp;
	}

	public LogDTO()
	{
		setCardio("N/A");
		setStrength("N/A");
		setBp("N/A");
		setCarbs("N/A");
		setMealType("N/A");
		setWeight("N/A");
		setLogType("N/A");
		
	}

	public String getIsProccessedString() {
		return isProccessedString;
	}

	public void setIsProccessedString(String isProccessedString) {
		this.isProccessedString = isProccessedString;
	}





	public boolean isOtherLog() {
		return isOtherLog;
	}

	public void setOtherLog(boolean isOtherLog) {
		this.isOtherLog = isOtherLog;
	}

	public boolean isMealLog() {
		return isMealLog;
	}

	public void setMealLog(boolean isMealLog) {
		this.isMealLog = isMealLog;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		if(logType==null)
		{
			logType="N/A";
		}
		else{
		this.logType = logType;
		}
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the minutesHours
	 */
	public String getMinutesHours() {
		return minutesHours;
	}

	/**
	 * @param minutesHours the minutesHours to set
	 */
	public void setMinutesHours(String minutesHours) {
		this.minutesHours = minutesHours;
	}

	public String getCoachName() {
		return coachName;
	}

	public void setCoachName(String coachName) {
		this.coachName = coachName;
	}

	public String getStepsPerDay() {
		return stepsPerDay;
	}

	public void setStepsPerDay(String stepsPerDay) {
		this.stepsPerDay = stepsPerDay;
	}

	public String getTookMeds() {
		return tookMeds;
	}

	public void setTookMeds(String tookMeds) {
		this.tookMeds = tookMeds;
	}

	public String getGlucoseLevel() {
		return glucoseLevel;
	}

	public void setGlucoseLevel(String glucoseLevel) {
		this.glucoseLevel = glucoseLevel;
	}

	public boolean getIsSigned() {
		return isSigned;
	}

	public void setIsSigned(boolean isSigned) {
		this.isSigned = isSigned;
	}


    public String getOnlyDate() {
        return onlyDate;
    }

    public void setOnlyDate(String onlyDate) {
        this.onlyDate = onlyDate;
    }

    public String getOnlyTime() {
        return onlyTime;
    }

    public void setOnlyTime(String onlyTime) {
        this.onlyTime = onlyTime;
    }

    public int getAlIntensity() {
        return alIntensity;
    }

    public void setAlIntensity(int alIntensity) {
        this.alIntensity = alIntensity;
    }

    public String getAlType() {
        return alType;
    }

    public void setAlType(String alType) {
        this.alType = alType;
    }

    public int getAlMinutesPerformed() {
        return alMinutesPerformed;
    }

    public void setAlMinutesPerformed(int alMinutesPerformed) {
        this.alMinutesPerformed = alMinutesPerformed;
    }

    public String getAlNotes() {
        return alNotes;
    }

    public void setAlNotes(String alNotes) {
        this.alNotes = alNotes;
    }

	public String getgType() {
		return gType;
	}

	public void setgType(String gType) {
		this.gType = gType;
	}

	public Long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Long createdOn) {
		this.createdOn = createdOn;
	}
}
