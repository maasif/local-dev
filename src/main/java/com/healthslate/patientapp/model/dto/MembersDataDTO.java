package com.healthslate.patientapp.model.dto;

import com.google.gson.annotations.Expose;
import com.healthslate.patientapp.model.entity.UserActivity;

import java.util.Date;

/*
* ======= FILE CHANGE HISTORY =======
* [5/5/2015]: Created by __ad
* ===================================
 */

public class MembersDataDTO {

    @Expose
    private long userId;
    @Expose
    private long patientId;
    @Expose
    private String macAddress;
    @Expose
    private String firstName;
    @Expose
    private String email;
    @Expose
    private String lastName;
    @Expose
    private String registrationDate;
    @Expose
    private int totalLogs;
    @Expose
    private String lastLogTime;
    @Expose
    private String lastLogType;
    @Expose
    private String versionNo;
    @Expose
    private UserActivity userActivity;
    @Expose
    private String deviceType;

    @Expose
    private String uuid;

    @Expose
    private String facilityName;

    @Expose
    private String address;

    @Expose
    private String city;

    @Expose
    private String state;

    @Expose
    private String phone;

    @Expose
    private String zip;

    @Expose
    private String lastLogTimeMillis;

    public MembersDataDTO(){
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public int getTotalLogs() {
        return totalLogs;
    }

    public void setTotalLogs(int totalLogs) {
        this.totalLogs = totalLogs;
    }

    public String getLastLogTime() {
        return lastLogTime;
    }

    public void setLastLogTime(String lastLogTime) {
        this.lastLogTime = lastLogTime;
    }

    public String getLastLogType() {
        return lastLogType;
    }

    public void setLastLogType(String lastLogType) {
        this.lastLogType = lastLogType;
    }

    public String getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserActivity getUserActivity() {
        return userActivity;
    }

    public void setUserActivity(UserActivity userActivity) {
        this.userActivity = userActivity;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getLastLogTimeMillis() {
        return lastLogTimeMillis;
    }

    public void setLastLogTimeMillis(String lastLogTimeMillis) {
        this.lastLogTimeMillis = lastLogTimeMillis;
    }
}
