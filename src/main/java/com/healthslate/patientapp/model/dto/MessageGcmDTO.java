package com.healthslate.patientapp.model.dto;

import com.google.gson.annotations.Expose;

/*
* ======= FILE CHANGE HISTORY =======
* [2/4/2015]: Created by __oz
* ===================================
 */

public class MessageGcmDTO {

    int messageId;

    @Expose(serialize = true, deserialize = true)
    long patientId;

    @Expose(serialize = true, deserialize = true)
    long providerId;

    @Expose(serialize = true, deserialize = true)
    String logId;

    @Expose(serialize = true, deserialize = true)
    long timeStamp;

    @Expose(serialize = true, deserialize = true)
    String messageText;

    @Expose(serialize = true, deserialize = true)
    String owner;

    @Expose(serialize = true, deserialize = true)
    String imagePath;
    
    @Expose(serialize = true, deserialize = true)
    String imageContent;

    @Expose(serialize = true, deserialize = true)
    boolean isRead;

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public long getProviderId() {
        return providerId;
    }

    public void setProviderId(long providerId) {
        this.providerId = providerId;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

	public String getImageContent() {
		return imageContent;
	}

	public void setImageContent(String imageContent) {
		this.imageContent = imageContent;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

    @Override
    public String toString() {
        return "MessageGcmDTO{" +
                "messageId=" + messageId +
                ", patientId=" + patientId +
                ", providerId=" + providerId +
                ", logId='" + logId + '\'' +
                ", timeStamp=" + timeStamp +
                ", messageText='" + messageText + '\'' +
                ", owner='" + owner + '\'' +
                ", imagePath='" + imagePath + '\'' +
                ", imageContent='" + imageContent + '\'' +
                ", isRead=" + isRead +
                '}';
    }
}
