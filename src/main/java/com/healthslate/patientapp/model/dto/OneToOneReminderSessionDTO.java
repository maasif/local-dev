package com.healthslate.patientapp.model.dto;

/**
 * Created by omursaleen on 6/8/2015.
 */
public class OneToOneReminderSessionDTO {

    private long patientId;
    private String firstName;
    private String email;
    private String phone;
    private String coachName;
    private String coachEmail;
    private long offsetTime;
    private String offsetKey;
    private long reminderSession;
    private long sessionId;
    private String reminderSent;
    private long facilityOffsetTime;

    public OneToOneReminderSessionDTO(){}

    public OneToOneReminderSessionDTO(long patientId, String firstName, String email, String phone, long reminderSession, String coachName, String coachEmail, String reminderSent, long offsetTime, String offsetKey, long sessionId, long facilityOffsetTime) {
        this.patientId = patientId;
        this.firstName = firstName;
        this.email = email;
        this.phone = phone;
        this.reminderSession = reminderSession;
        this.coachName = coachName;
        this.coachEmail = coachEmail;
        this.reminderSent = reminderSent;
        this.offsetTime = offsetTime;
        this.offsetKey = offsetKey;
        this.sessionId = sessionId;
        this.facilityOffsetTime = facilityOffsetTime;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getReminderSession() {
        return reminderSession;
    }

    public void setReminderSession(long reminderSession) {
        this.reminderSession = reminderSession;
    }

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    public String getCoachEmail() {
        return coachEmail;
    }

    public void setCoachEmail(String coachEmail) {
        this.coachEmail = coachEmail;
    }

    public String getReminderSent() {
        return reminderSent;
    }

    public void setReminderSent(String reminderSent) {
        this.reminderSent = reminderSent;
    }

    public long getOffsetTime() {
        return offsetTime;
    }

    public void setOffsetTime(long offsetTime) {
        this.offsetTime = offsetTime;
    }

    public String getOffsetKey() {
        return offsetKey;
    }

    public void setOffsetKey(String offsetKey) {
        this.offsetKey = offsetKey;
    }

    public long getSessionId() {
        return sessionId;
    }

    public void setSessionId(long sessionId) {
        this.sessionId = sessionId;
    }

    public long getFacilityOffsetTime() {
        return facilityOffsetTime;
    }

    public void setFacilityOffsetTime(long facilityOffsetTime) {
        this.facilityOffsetTime = facilityOffsetTime;
    }
}
