package com.healthslate.patientapp.model.dto;

import com.google.gson.annotations.Expose;

/**
 * Created by arslan.khan on 07/09/2015.
 */
public class PALitePatientSignupDTO {

    @Expose
    private String firstName;

    @Expose
    private String lastName;

    @Expose
    private String email;

    @Expose
    private String emailCoach;

    @Expose
    private String meterSerialNumber;

    @Expose
    private String deviceMacAddress;

    public PALitePatientSignupDTO(){

    }

    public PALitePatientSignupDTO(String firstName, String lastName, String emailCoach, String email, String meterSerialNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailCoach = emailCoach;
        this.email = email;
        this.meterSerialNumber = meterSerialNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailCoach() {
        return emailCoach;
    }

    public void setEmailCoach(String emailCoach) {
        this.emailCoach = emailCoach;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMeterSerialNumber() {
        return meterSerialNumber;
    }

    public void setMeterSerialNumber(String meterSerialNumber) {
        this.meterSerialNumber = meterSerialNumber;
    }

    public String getDeviceMacAddress() {
        return deviceMacAddress;
    }

    public void setDeviceMacAddress(String deviceMacAddress) {
        this.deviceMacAddress = deviceMacAddress;
    }
}
