package com.healthslate.patientapp.model.dto;

import com.google.gson.annotations.Expose;
import com.healthslate.patientapp.model.entity.Device;
import com.healthslate.patientapp.model.entity.UserActivity;

import java.util.Date;

public class PatientDTO {

    @Expose
	private Long patientId;
	private String patientName;

    @Expose
    private String firstName;

    @Expose
    private String lastName;

    @Expose
	private String email;
	private String phone;
	private String macAddress;
	private int unProcessedLogs;

    @Expose
	private int messageCount;
	private Date logLastReviewedDate;
	private String imagePath;

    @Expose
    private String uuid;

    private String dateString;
    private String notesDate;
    private String providersName;
    private Boolean isConsentAccepted;
    private String isApproved;
    private Boolean useInsulin;

    @Expose
    private String invitedDate;

    @Expose
    private String requestAnInvitedDate;

    private String mrn;
    private long notesDateLong;
    private String facilityName;

    private String primFirstName;
    private String primLastName;
    private String primaryCoachName;
    private String leadCoachName;
    private String lastLogType;
    private String lastLogTime;
    private String lastLogTimeMillis;
    private String nextSession;

    private String sessionCoachName;
    private String notesCoachName;

    @Expose
    private String address;

    @Expose
    private String city;

    @Expose
    private String state;

    @Expose
    private String zip;

    @Expose
    private Long facilityLeadCoach;

    @Expose
    private Long facilityId;

    @Expose
    private Date regDate;

    UserActivity userActivity;

    @Expose
    private String displayName;

    @Expose
    private String registeredWeeks;

    private Device pd;

    private long lastContactSentDate;
    private long lastContactReceivedDate;

    public PatientDTO() {
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public int getUnProcessedLogs() {
        return unProcessedLogs;
    }

    public void setUnProcessedLogs(int unProcessedLogs) {
        this.unProcessedLogs = unProcessedLogs;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public Date getLogLastReviewedDate() {
        return logLastReviewedDate;
    }

    public void setLogLastReviewedDate(Date logLastReviewedDate) {
        this.logLastReviewedDate = logLastReviewedDate;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public String getNotesDate() {
        return notesDate;
    }

    public void setNotesDate(String notesDate) {
        this.notesDate = notesDate;
    }

    public String getProvidersName() {
        return providersName;
    }

    public void setProvidersName(String providersName) {
        this.providersName = providersName;
    }

    public Boolean getIsConsentAccepted() {
        return isConsentAccepted;
    }

    public void setIsConsentAccepted(Boolean isConsentAccepted) {
        this.isConsentAccepted = isConsentAccepted;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public Boolean getUseInsulin() {
        return useInsulin;
    }

    public void setUseInsulin(Boolean useInsulin) {
        this.useInsulin = useInsulin;
    }

    public String getInvitedDate() {
        return invitedDate;
    }

    public void setInvitedDate(String invitedDate) {
        this.invitedDate = invitedDate;
    }

    public String getRequestAnInvitedDate() {
        return requestAnInvitedDate;
    }

    public void setRequestAnInvitedDate(String requestAnInvitedDate) {
        this.requestAnInvitedDate = requestAnInvitedDate;
    }

    public String getMrn() {
        return mrn;
    }

    public void setMrn(String mrn) {
        this.mrn = mrn;
    }

    public long getNotesDateLong() {
        return notesDateLong;
    }

    public void setNotesDateLong(long notesDateLong) {
        this.notesDateLong = notesDateLong;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getPrimFirstName() {
        return primFirstName;
    }

    public void setPrimFirstName(String primFirstName) {
        this.primFirstName = primFirstName;
    }

    public String getPrimLastName() {
        return primLastName;
    }

    public void setPrimLastName(String primLastName) {
        this.primLastName = primLastName;
    }

    public String getPrimaryCoachName() {
        return primaryCoachName;
    }

    public void setPrimaryCoachName(String primaryCoachName) {
        this.primaryCoachName = primaryCoachName;
    }

    public String getLastLogType() {
        return lastLogType;
    }

    public void setLastLogType(String lastLogType) {
        this.lastLogType = lastLogType;
    }

    public String getLastLogTime() {
        return lastLogTime;
    }

    public void setLastLogTime(String lastLogTime) {
        this.lastLogTime = lastLogTime;
    }

    public String getLastLogTimeMillis() {
        return lastLogTimeMillis;
    }

    public void setLastLogTimeMillis(String lastLogTimeMillis) {
        this.lastLogTimeMillis = lastLogTimeMillis;
    }

    public String getNextSession() {
        return nextSession;
    }

    public void setNextSession(String nextSession) {
        this.nextSession = nextSession;
    }

    public String getSessionCoachName() {
        return sessionCoachName;
    }

    public void setSessionCoachName(String sessionCoachName) {
        this.sessionCoachName = sessionCoachName;
    }

    public String getNotesCoachName() {
        return notesCoachName;
    }

    public void setNotesCoachName(String notesCoachName) {
        this.notesCoachName = notesCoachName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public Long getFacilityLeadCoach() {
        return facilityLeadCoach;
    }

    public void setFacilityLeadCoach(Long facilityLeadCoach) {
        this.facilityLeadCoach = facilityLeadCoach;
    }

    public Long getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(Long facilityId) {
        this.facilityId = facilityId;
    }

    public UserActivity getUserActivity() {
        return userActivity;
    }

    public void setUserActivity(UserActivity userActivity) {
        this.userActivity = userActivity;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public String getRegisteredWeeks() {
        return registeredWeeks;
    }

    public void setRegisteredWeeks(String registeredWeeks) {
        this.registeredWeeks = registeredWeeks;
    }

    public Device getPd() {
        return pd;
    }

    public void setPd(Device pd) {
        this.pd = pd;
    }

    public String getLeadCoachName() {
        return leadCoachName;
    }

    public void setLeadCoachName(String leadCoachName) {
        this.leadCoachName = leadCoachName;
    }

    public long getLastContactSentDate() {
        return lastContactSentDate;
    }

    public void setLastContactSentDate(long lastContactSentDate) {
        this.lastContactSentDate = lastContactSentDate;
    }

    public long getLastContactReceivedDate() {
        return lastContactReceivedDate;
    }

    public void setLastContactReceivedDate(long lastContactReceivedDate) {
        this.lastContactReceivedDate = lastContactReceivedDate;
    }
}
