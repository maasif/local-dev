package com.healthslate.patientapp.model.dto;

/**
 * Created by omursaleen on 3/3/2015.
 */
public class PatientDetailDTO {

    private long patientId;
    private String firstName;
    private String lastName;
    private String displayName;
    private String email;
    private String phone;
    private String dob;
    private float weight;
    private String gender;
    private String nickname;
    private String race;
    private String state;
    private String mrn;
    private String education;
    private String diagnosis;
    private String diagnosisDate;
    private String diabetesSupport;
    private String timeSinceLastDiabetesEdu;
    private String medications;
    private String dietaryRestrictions;
    private String dietaryRestrictionsOther;
    private String mealPreparer;
    private String imagePath;
    private String language;
    private String treatedDepression;
    private long facilityId;

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMrn() {
        return mrn;
    }

    public void setMrn(String mrn) {
        this.mrn = mrn;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getDiagnosisDate() {
        return diagnosisDate;
    }

    public void setDiagnosisDate(String diagnosisDate) {
        this.diagnosisDate = diagnosisDate;
    }

    public String getDiabetesSupport() {
        return diabetesSupport;
    }

    public void setDiabetesSupport(String diabetesSupport) {
        this.diabetesSupport = diabetesSupport;
    }

    public String getMedications() {
        return medications;
    }

    public void setMedications(String medications) {
        this.medications = medications;
    }

    public String getDietaryRestrictions() {
        return dietaryRestrictions;
    }

    public void setDietaryRestrictions(String dietaryRestrictions) {
        this.dietaryRestrictions = dietaryRestrictions;
    }

    public String getMealPreparer() {
        return mealPreparer;
    }

    public void setMealPreparer(String mealPreparer) {
        this.mealPreparer = mealPreparer;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTreatedDepression() {
        return treatedDepression;
    }

    public void setTreatedDepression(String treatedDepression) {
        this.treatedDepression = treatedDepression;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getTimeSinceLastDiabetesEdu() {
        return timeSinceLastDiabetesEdu;
    }

    public void setTimeSinceLastDiabetesEdu(String timeSinceLastDiabetesEdu) {
        this.timeSinceLastDiabetesEdu = timeSinceLastDiabetesEdu;
    }

    public String getDietaryRestrictionsOther() {
        return dietaryRestrictionsOther;
    }

    public void setDietaryRestrictionsOther(String dietaryRestrictionsOther) {
        this.dietaryRestrictionsOther = dietaryRestrictionsOther;
    }

    public long getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(long facilityId) {
        this.facilityId = facilityId;
    }
}
