package com.healthslate.patientapp.model.dto;

import java.util.List;

/**
 * Created by omursaleen on 5/15/2015.
 */
public class PatientSessionPreferenceDTO {

    private int patientId;

    private List<String> daysList;

    private List<String> timesList;

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public List<String> getDaysList() {
        return daysList;
    }

    public void setDaysList(List<String> daysList) {
        this.daysList = daysList;
    }

    public List<String> getTimesList() {
        return timesList;
    }

    public void setTimesList(List<String> timesList) {
        this.timesList = timesList;
    }

    @Override
    public String toString() {
        return "PatientSessionPreferenceDTO{" +
                "patientId=" + patientId +
                ", daysList=" + daysList +
                ", timesList=" + timesList +
                '}';
    }
}
