package com.healthslate.patientapp.model.dto;

public class RecentMealDTO {

	private String logId;
	private String imageSrc;
	private boolean isProcessed;
	private String logTime;
	private String type;
	private String mealName;
	private float carbs;
	private boolean hasMissingFood;
	private String unit;
    private String noOfServings;
	private String processedBy;

	public String getImageSrc() {
		return imageSrc;
	}
	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}	
	public boolean isProcessed() {
		return isProcessed;
	}
	public void setProcessed(boolean isProcessed) {
		this.isProcessed = isProcessed;
	}
	public String getLogTime() {
		return logTime;
	}
	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getLogId() {
		return logId;
	}
	public void setLogId(String logId) {
		this.logId = logId;
	}
	public String getMealName() {
		return mealName;
	}
	public void setMealName(String mealName) {
		this.mealName = mealName;
	}	
	
	public RecentMealDTO(){
		
	}
	
	public RecentMealDTO(String logId, String imageSrc, boolean isProcessed, String logTime, String type, String mealName, float carbs, boolean hasMissingFood, String processedBy) {
		
		this.logId = logId;
		this.imageSrc = imageSrc;
		this.isProcessed = isProcessed;
		this.logTime = logTime;		
		this.type = type;
		this.mealName = mealName;
		this.carbs = carbs;
		this.hasMissingFood = hasMissingFood;
		this.processedBy = processedBy;
	}
	
	public float getCarbs() {
		return carbs;
	}
	public void setCarbs(float carbs) {
		this.carbs = carbs;
	}
	public boolean isHasMissingFood() {
		return hasMissingFood;
	}
	public void setHasMissingFood(boolean hasMissingFood) {
		this.hasMissingFood = hasMissingFood;
	}

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getNoOfServings() {
        return noOfServings;
    }

    public void setNoOfServings(String noOfServings) {
        this.noOfServings = noOfServings;
    }

	public String getProcessedBy() {
		return processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}
}
