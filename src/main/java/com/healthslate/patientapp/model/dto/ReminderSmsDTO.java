package com.healthslate.patientapp.model.dto;

public class ReminderSmsDTO {

	private Long patientId;
	private String phoneNumber;
	private String time;
	private String day;
	private Long facilityTimeMillis;

	public Long getPatientId() {
		return patientId;
	}
	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public ReminderSmsDTO(){
		
	}
	
	public ReminderSmsDTO(Long patientId, String phoneNumber, String time, String day, Long facilityTimeMillis) {
		this.patientId = patientId;
		this.phoneNumber = phoneNumber;
		this.time = time;
		this.day = day;
        this.facilityTimeMillis = facilityTimeMillis;
	}

    public Long getFacilityTimeMillis() {
        return facilityTimeMillis;
    }

    public void setFacilityTimeMillis(Long facilityTimeMillis) {
        this.facilityTimeMillis = facilityTimeMillis;
    }
}
