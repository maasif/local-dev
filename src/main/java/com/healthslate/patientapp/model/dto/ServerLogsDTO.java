package com.healthslate.patientapp.model.dto;

public class ServerLogsDTO {

    private long serverLogId;
    private String actionName;
    private String description;
    private String loggedDate;
    private String loggedUserName;
    private String userRole;

    public ServerLogsDTO(){

    }

    public ServerLogsDTO(long serverLogId, String actionName, String description, String loggedDate, String loggedUserName, String userRole) {
        this.serverLogId = serverLogId;
        this.actionName = actionName;
        this.description = description;
        this.loggedDate = loggedDate;
        this.loggedUserName = loggedUserName;
        this.userRole = userRole;
    }

    public long getServerLogId() {
        return serverLogId;
    }

    public void setServerLogId(long serverLogId) {
        this.serverLogId = serverLogId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLoggedDate() {
        return loggedDate;
    }

    public void setLoggedDate(String loggedDate) {
        this.loggedDate = loggedDate;
    }

    public String getLoggedUserName() {
        return loggedUserName;
    }

    public void setLoggedUserName(String loggedUserName) {
        this.loggedUserName = loggedUserName;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
}
