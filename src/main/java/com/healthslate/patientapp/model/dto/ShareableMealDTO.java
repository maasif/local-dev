package com.healthslate.patientapp.model.dto;

import com.google.gson.annotations.Expose;
import org.hibernate.type.IntegerType;

/*
* ======= FILE CHANGE HISTORY =======
* [9/18/2015]: Created by __oz
* ===================================
 */

public class ShareableMealDTO {

    @Expose
    private Long patientId;

    @Expose
    private String logId;

    @Expose
    private String permission;

    @Expose
    private String logType;

    @Expose
    private String mealName;

    @Expose
    private String carbs;

    @Expose
    private String memberName;

    @Expose
    private Long requestSentDate;

    @Expose
    private Long permissionGrantedDate;

    @Expose
    private Long lastMealSharedDate;

    @Expose
    private String mealSharedBy;

    @Expose
    private Integer mealSharedCount;

    @Expose
    private Integer requestSentCount;

    @Expose
    private String notes;

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public String getCarbs() {
        return carbs;
    }

    public void setCarbs(String carbs) {
        this.carbs = carbs;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public Long getRequestSentDate() {
        return requestSentDate;
    }

    public void setRequestSentDate(Long requestSentDate) {
        this.requestSentDate = requestSentDate;
    }

    public Long getPermissionGrantedDate() {
        return permissionGrantedDate;
    }

    public void setPermissionGrantedDate(Long permissionGrantedDate) {
        this.permissionGrantedDate = permissionGrantedDate;
    }

    public Long getLastMealSharedDate() {
        return lastMealSharedDate;
    }

    public void setLastMealSharedDate(Long lastMealSharedDate) {
        this.lastMealSharedDate = lastMealSharedDate;
    }

    public String getMealSharedBy() {
        return mealSharedBy;
    }

    public void setMealSharedBy(String mealSharedBy) {
        this.mealSharedBy = mealSharedBy;
    }

    public int getMealSharedCount() {
        return mealSharedCount;
    }

    public void setMealSharedCount(Integer mealSharedCount) {
        this.mealSharedCount = mealSharedCount;
    }

    public Integer getRequestSentCount() {
        return requestSentCount;
    }

    public void setRequestSentCount(Integer requestSentCount) {
        this.requestSentCount = requestSentCount;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
