package com.healthslate.patientapp.model.dto;

import com.google.gson.annotations.Expose;

public class SignUpDTO {

    @Expose
    String firstName;

    @Expose
    String lastName;

    @Expose
    String password;

    @Expose
    String facility;

    @Expose
    String email;

    @Expose
    String gender;

    @Expose
    String phoneNumber;

    @Expose
    Long dob;

    @Expose
    String dobString;

    @Expose
    String image;

    @Expose
    String zipCode;

    @Expose
    boolean useInsulin;

    @Expose
    boolean isInvitation;

    @Expose
    String callFrom;

    @Expose
    private String address;

    @Expose
    private String city;

    @Expose
    private String state;

    @Expose
    private long leadCoach;

    @Expose
    private String mrn;

    @Expose
    private String selectedProviders;

    @Expose
    private long primaryCoach;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getDob() {
        return dob;
    }

    public void setDob(Long dob) {
        this.dob = dob;
    }

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public boolean isUseInsulin() {
        return useInsulin;
    }

    public void setUseInsulin(boolean useInsulin) {
        this.useInsulin = useInsulin;
    }

    public boolean isInvitation() {
        return isInvitation;
    }

    public void setInvitation(boolean isInvitation) {
        this.isInvitation = isInvitation;
    }

    public String getCallFrom() {
        return callFrom;
    }

    public void setCallFrom(String callFrom) {
        this.callFrom = callFrom;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "SignUpDTO{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", facility='" + facility + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", dob=" + dob +
                ", image='" + image + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", useInsulin=" + useInsulin +
                ", isInvitation=" + isInvitation +
                ", callFrom='" + callFrom + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                '}';
    }

    public long getLeadCoach() {
        return leadCoach;
    }

    public void setLeadCoach(long leadCoach) {
        this.leadCoach = leadCoach;
    }

    public String getDobString() {
        return dobString;
    }

    public void setDobString(String dobString) {
        this.dobString = dobString;
    }

    public String getMrn() {
        return mrn;
    }

    public void setMrn(String mrn) {
        this.mrn = mrn;
    }

    public String getSelectedProviders() {
        return selectedProviders;
    }

    public void setSelectedProviders(String selectedProviders) {
        this.selectedProviders = selectedProviders;
    }

    public long getPrimaryCoach() {
        return primaryCoach;
    }

    public void setPrimaryCoach(long primaryCoach) {
        this.primaryCoach = primaryCoach;
    }
}
