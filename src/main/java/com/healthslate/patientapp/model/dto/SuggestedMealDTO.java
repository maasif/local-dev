package com.healthslate.patientapp.model.dto;

public class SuggestedMealDTO {

	private long suggestedLogId;
	private String imageSrc;
	private String logTime;
	private String type;
	private String mealName;
	private float carbs;
	private boolean hasMissingFood;
	private String createdBy;
    private String logId;
    private String minorFoodName;
	private boolean isDrafted;
    private String unit;
    private String noOfServings;
	public String getImageSrc() {
		return imageSrc;
	}
	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}
	public String getLogTime() {
		return logTime;
	}
	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getMealName() {
		return mealName;
	}
	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public SuggestedMealDTO(){

	}

	public SuggestedMealDTO(long suggestedLogId, String imageSrc, String logTime, String type, String mealName, float carbs, boolean hasMissingFood, String createdBy, String logId, String minorFoodName, boolean isDrafted) {
		
		this.suggestedLogId = suggestedLogId;
		this.imageSrc = imageSrc;
		this.createdBy = createdBy;
		this.logTime = logTime;		
		this.type = type;
		this.mealName = mealName;
		this.carbs = carbs;
		this.hasMissingFood = hasMissingFood;
        this.logId = logId;
        this.minorFoodName = minorFoodName;
		this.isDrafted = isDrafted;
	}
	
	public float getCarbs() {
		return carbs;
	}
	public void setCarbs(float carbs) {
		this.carbs = carbs;
	}
	public boolean isHasMissingFood() {
		return hasMissingFood;
	}
	public void setHasMissingFood(boolean hasMissingFood) {
		this.hasMissingFood = hasMissingFood;
	}

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public long getSuggestedLogId() {
        return suggestedLogId;
    }

    public void setSuggestedLogId(long suggestedLogId) {
        this.suggestedLogId = suggestedLogId;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getMinorFoodName() {
        return minorFoodName;
    }

    public void setMinorFoodName(String minorFoodName) {
        this.minorFoodName = minorFoodName;
    }

	public boolean isDrafted() {
		return isDrafted;
	}

	public void setDrafted(boolean isDrafted) {
		this.isDrafted = isDrafted;
	}

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getNoOfServings() {
        return noOfServings;
    }

    public void setNoOfServings(String noOfServings) {
        this.noOfServings = noOfServings;
    }
}
