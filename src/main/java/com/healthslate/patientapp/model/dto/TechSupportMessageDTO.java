package com.healthslate.patientapp.model.dto;

import com.google.gson.annotations.Expose;

public class TechSupportMessageDTO {

    @Expose
    private Long msgId;
    @Expose
    private String message;
    @Expose
    private String owner;
    @Expose
    private Long timestamp;
    @Expose
    private Long foodSummaryId;
    @Expose
    private Long patientId;
    @Expose
    private Long userId;
    @Expose
    private Long repliedBy;
    @Expose
    private String userFirstName;
    @Expose
    private String userLastName;
    @Expose
    private String imagePath;
    @Expose
    private String videoLink;
    @Expose
    private String notifiedTechSupport;
    @Expose
    private Boolean readStatus;
    @Expose
    private String repliedFirstUserName;
    @Expose
    private String repliedLastUserName;

    public Long getMsgId() {
        return msgId;
    }

    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getFoodSummaryId() {
        return foodSummaryId;
    }

    public void setFoodSummaryId(Long foodSummaryId) {
        this.foodSummaryId = foodSummaryId;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getNotifiedTechSupport() {
        return notifiedTechSupport;
    }

    public void setNotifiedTechSupport(String notifiedTechSupport) {
        this.notifiedTechSupport = notifiedTechSupport;
    }

    public Boolean getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(Boolean readStatus) {
        this.readStatus = readStatus;
    }

    public Long getRepliedBy() {
        return repliedBy;
    }

    public void setRepliedBy(Long repliedBy) {
        this.repliedBy = repliedBy;
    }

    public String getRepliedFirstUserName() {
        return repliedFirstUserName;
    }

    public void setRepliedFirstUserName(String repliedFirstUserName) {
        this.repliedFirstUserName = repliedFirstUserName;
    }

    public String getRepliedLastUserName() {
        return repliedLastUserName;
    }

    public void setRepliedLastUserName(String repliedLastUserName) {
        this.repliedLastUserName = repliedLastUserName;
    }
}
