package com.healthslate.patientapp.model.dto;

import com.google.gson.annotations.Expose;


/*
* ======= FILE CHANGE HISTORY =======
* [2/24/2015]: Created by __ad
* ===================================
 */

public class TookMedsDTO {

	@Expose(serialize = true, deserialize = false)
	int medsPercentage;
	@Expose(serialize = true, deserialize = false)
	int totalCount;
	@Expose(serialize = true, deserialize = false)
	int medsTakenCount;

    public TookMedsDTO(){

    }

    public TookMedsDTO(int medsPercentage,int totalCount, int medsTakenCount) {
       this.medsPercentage = medsPercentage;
       this.totalCount = totalCount;
       this.medsTakenCount = medsTakenCount;
    }

	public int getMedsPercentage() {
		return medsPercentage;
	}

	public void setMedsPercentage(int medsPercentage) {
		this.medsPercentage = medsPercentage;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getMedsTakenCount() {
		return medsTakenCount;
	}

	public void setMedsTakenCount(int medsTakenCount) {
		this.medsTakenCount = medsTakenCount;
	}
}
