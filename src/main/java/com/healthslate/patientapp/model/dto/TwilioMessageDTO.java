package com.healthslate.patientapp.model.dto;

import java.util.Date;

public class TwilioMessageDTO implements Comparable<TwilioMessageDTO>{

	private String direction;
	private long dateSent;
	private String from;
	private String to;
	private String status;
	private String body;
	private String ssid;
	
	public TwilioMessageDTO() {
		
	}
	
	public TwilioMessageDTO(String direction, long dateSent, String from,
			String to, String status, String body, String ssid) {
		super();
		this.direction = direction;
		this.dateSent = dateSent;
		this.from = from;
		this.to = to;
		this.status = status;
		this.body = body;
		this.ssid = ssid;
	}

	public String getDirection() {
		return direction;
	}
	public long getDateSent() {
		return dateSent;
	}
	public String getFrom() {
		return from;
	}
	public String getTo() {
		return to;
	}
	public String getStatus() {
		return status;
	}
	public String getBody() {
		return body;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public void setDateSent(long dateSent) {
		this.dateSent = dateSent;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setBody(String body) {
		this.body = body;
	}

	@Override
	public int compareTo(TwilioMessageDTO twilio) {
		return new Date(getDateSent()).compareTo(new Date(twilio.getDateSent()));
	}

	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
}
