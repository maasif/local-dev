package com.healthslate.patientapp.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="action_plan")
public class ActionPlan {

	@Id
	@GeneratedValue
	@Column(name="action_plan_id")
	@Expose
	private Long actionPlanId;
	
	@Column(name="name")
	@Expose
	private String name;
	
	@Column(name="show_frequency_times")
	@Expose
	private Boolean showFrequencyTimes;
	
	@Column(name="show_full_frequency_calendar")
	@Expose
	private Integer showFullFrequencyCalendar;
	
	@Column(name="show_meals")
	@Expose
	private Boolean showMeals;
	
	@Column(name="show_record_label")
	@Expose
	private Boolean showRecordLabel;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="goal_id")
	private Goal goal;

	@Column(name="order_by")
	private Integer orderBy;
	
	public Long getActionPlanId() {
		return actionPlanId;
	}

	public void setActionPlanId(Long actionPlanId) {
		this.actionPlanId = actionPlanId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Goal getGoal() {
		return goal;
	}

	public void setGoal(Goal goal) {
		this.goal = goal;
	}

	public Boolean getIsShowFrequencyTimes() {
		return showFrequencyTimes;
	}

	public Integer getShowFullFrequencyCalendar() {
		return showFullFrequencyCalendar;
	}

	public boolean getIsShowMeals() {
		return showMeals;
	}

	public void setShowFrequencyTimes(Boolean showFrequencyTimes) {
		this.showFrequencyTimes = showFrequencyTimes;
	}

	public void setShowMeals(boolean showMeals) {
		this.showMeals = showMeals;
	}

	public void setShowFullFrequencyCalendar(Integer showFullFrequencyCalendar) {
		this.showFullFrequencyCalendar = showFullFrequencyCalendar;
	}

	public boolean getIsShowRecordLabel() {
		return showRecordLabel;
	}

	public void setShowRecordLabel(boolean showRecordLabel) {
		this.showRecordLabel = showRecordLabel;
	}

	/**
	 * @return the orderBy
	 */
	public Integer getOrderBy() {
		return orderBy;
	}

	/**
	 * @param orderBy the orderBy to set
	 */
	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}
}
