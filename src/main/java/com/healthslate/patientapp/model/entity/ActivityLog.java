package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name="activity_log")
public class ActivityLog {

    public static final String ACTIVITY_LOG_ID = "activity_log_id";
	public static final String TYPE = "type";
	public static final String MINUTS_PERFORMED = "minutes_performed";
	public static final String TIMESTAMP = "timestamp";
    public static final String INTENSITY = "intensity";
    public static final String NOTES = "notes";

    @Id
    @GeneratedValue
	@Expose (serialize = false, deserialize = true)
    @Column(name= ACTIVITY_LOG_ID)
	private Long activityLogId;

    @Expose(serialize = true, deserialize = true)
    @Column(name=INTENSITY)
    private Integer intensity;

    @Expose(serialize = true, deserialize = true)
    @Column(name=TYPE)
	private String type;

    @Expose(serialize = true, deserialize = true)
    @Column(name=MINUTS_PERFORMED)
	private Integer minutesPerformed;

    @Expose(serialize = true, deserialize = true)
    @Column(name=TIMESTAMP)
	private Long timestamp;

    @Expose(serialize = true, deserialize = true)
    @Column(name=NOTES, columnDefinition = "text")
    private String notes;

    @Expose(serialize = true, deserialize = false)
    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name="log_id")
    private Log log;

    public Long getActivityLogId() {
        return activityLogId;
    }

    public void setActivityLogId(Long activityLogId) {
        this.activityLogId = activityLogId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMinutesPerformed() {
        return minutesPerformed;
    }

    public void setMinutesPerformed(int minutesPerformed) {
        this.minutesPerformed = minutesPerformed;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getIntensity() {
        return intensity;
    }

    public void setIntensity(Integer intensity) {
        this.intensity = intensity;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Log getLog() {
        return log;
    }

    public void setLog(Log log) {
        this.log = log;
    }

    @Override
    public String toString() {
        return "ActivityLog{" +
                "activityLogId=" + activityLogId +
                ", intensity='" + intensity + '\'' +
                ", type='" + type + '\'' +
                ", minutesPerformed=" + minutesPerformed +
                ", timestamp=" + timestamp +
                ", notes='" + notes + '\'' +
                '}';
    }
}