package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="app_access_log")
public class AppAccessLog {

	@Id
	@GeneratedValue
	@Column(name="access_log_id")
	@Expose
	private Long accessLogId;

    @Column(name="user_id")
    @Expose
    private Long userId;

    @Column(name="user_role")
    @Expose
    private String userRole;

    @Column(name="browser_version")
    @Expose
    private String browserVersion;

    @Column(name="operating_system")
    @Expose
    private String operatingSystem;

    @Column(name="ip_address")
    @Expose
    private String ipAddress;

    @Column(name="access_date")
    @Expose
    private Date accessDate;

    public AppAccessLog(){}

    public AppAccessLog(long userId, String userRole, String browserVersion, String operatingSystem, String ipAddress) {
        this.userId = userId;
        this.userRole = userRole;
        this.browserVersion = browserVersion;
        this.operatingSystem = operatingSystem;
        this.ipAddress = ipAddress;
        this.accessDate = new Date();
    }

    public Long getAccessLogId() {
        return accessLogId;
    }

    public void setAccessLogId(Long accessLogId) {
        this.accessLogId = accessLogId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Date getAccessDate() {
        return accessDate;
    }

    public void setAccessDate(Date accessDate) {
        this.accessDate = accessDate;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }
}
