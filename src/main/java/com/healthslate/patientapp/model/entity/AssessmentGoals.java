package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="assessment_goals")
public class AssessmentGoals {

	@Id
	@GeneratedValue
	@Column(name="assessment_goals_id")
	@Expose
	private Long assessmentGoalsId;
	
	@Column(name="assessment_form_id")
	@Expose
	private Long assessmentFormId;
	
	@Column(name="goal_id")
	@Expose
	private Long goalId;

	public Long getAssessmentGoalsId() {
		return assessmentGoalsId;
	}

	public void setAssessmentGoalsId(Long assessmentGoalsId) {
		this.assessmentGoalsId = assessmentGoalsId;
	}

	public Long getAssessmentFormId() {
		return assessmentFormId;
	}

	public void setAssessmentFormId(Long assessmentFormId) {
		this.assessmentFormId = assessmentFormId;
	}

	public Long getGoalId() {
		return goalId;
	}

	public void setGoalId(Long goalId) {
		this.goalId = goalId;
	}
}
