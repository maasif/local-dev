package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="coach_goal_assessment")
public class CoachGoalAssessment {

	@Id
	@GeneratedValue
	@Column(name="coach_goal_assessment_id")
	@Expose
	private Long coachGoalAssessmentId;
	
	@Column(name="status")
	@Expose
	private String status;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "filled_form_id")
	private FilledForm filledForm;

	@Column(name="assessed_date")
	@Expose
	private Long assessedDate;
	
	@Column(name="notes", columnDefinition = "text")
	@Expose
	private String notes;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private User user;
	
	@Expose
	private String assessedDateString;
	
	@Expose
	private String assessedByName;
	
	public CoachGoalAssessment(){
		
	}
	
	public CoachGoalAssessment(String status, FilledForm filledForm, long assessedDate, String notes, User user) {
		this.status = status;
		this.filledForm = filledForm;
		this.assessedDate = assessedDate;
		this.notes = notes;
		this.user = user;
	}

	public Long getCoachGoalAssessmentId() {
		return coachGoalAssessmentId;
	}

	public void setCoachGoalAssessmentId(long coachGoalAssessmentId) {
		this.coachGoalAssessmentId = coachGoalAssessmentId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
		
	public FilledForm getFilledForm() {
		return filledForm;
	}

	public void setFilledForm(FilledForm filledForm) {
		this.filledForm = filledForm;
	}

	public long getAssessedDate() {
		return assessedDate;
	}

	public void setAssessedDate(long assessedDate) {
		this.assessedDate = assessedDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getAssessedDateString() {
		return assessedDateString;
	}

	public void setAssessedDateString(String assessedDateString) {
		this.assessedDateString = assessedDateString;
	}

	public String getAssessedByName() {
		return assessedByName;
	}

	public void setAssessedByName(String assessedByName) {
		this.assessedByName = assessedByName;
	}
}
