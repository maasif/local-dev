package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

/**
 * Created by i.nasim on 29-Jun-15.
 */
@Entity
@Table(name = "coach_hb_logging")
public class CoachHBLogging {
    @Id
    @GeneratedValue
    @Column(name = "coach_logging_id")
    @Expose
    private Long coachLoggingId;

    @Column(name = "logging_date")
    @Expose
    private Long loggingDate;

    @Column(name = "hb_logging_date")
    @Expose
    private Long HBloggingDate;

    @Column(name = "log", columnDefinition = "text")
    @Expose
    private String log;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "provider_id")
    private Provider provider;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Transient
    @Expose
    private String coachName;

    @Transient
    @Expose
    private String timeToDateString;

    @Transient
    @Expose
    private String HBLoggingDateToString;

    public CoachHBLogging() {

    }

    public CoachHBLogging(String log, Long loggingDate, Provider provider, Patient patient, Long HBloggingDate) {
        this.log = log;
        this.loggingDate = loggingDate;
        this.provider = provider;
        this.patient = patient;
        this.HBloggingDate = HBloggingDate;
    }

    public Long getCoachLoggingId() {
        return coachLoggingId;
    }

    public void setCoachLoggingId(Long coachLoggingId) {
        this.coachLoggingId = coachLoggingId;
    }

    public Long getLoggingDate() {
        return loggingDate;
    }

    public void setLoggingDate(Long loggingDate) {
        this.loggingDate = loggingDate;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    public String getTimeToDateString() {
        return timeToDateString;
    }

    public void setTimeToDateString(String timeToDateString) {
        this.timeToDateString = timeToDateString;
    }

    public Long getHBloggingDate() {
        return HBloggingDate;
    }

    public void setHBloggingDate(Long HBloggingDate) {
        this.HBloggingDate = HBloggingDate;
    }

    public String getHBLoggingDateToString() {
        return HBLoggingDateToString;
    }

    public void setHBLoggingDateToString(String HBLoggingDateToString) {
        this.HBLoggingDateToString = HBLoggingDateToString;
    }
}
