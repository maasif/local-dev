package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name="coach_notes")
public class CoachNotes {

	@Id
	@GeneratedValue
	@Column(name="coach_notes_id")
	@Expose
	private Long coachNotesId;

	@Column(name="notes_date")
	@Expose
	private Long notesDate;

	@Column(name="notes", columnDefinition = "text")
	@Expose
	private String notes;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "provider_id")
    private Provider provider;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Transient
    @Expose
    private String coachName;

    @Transient
    @Expose
    private String timeToDateString;

    @Column(name = "notified_coaches")
    @Expose
    private String notifiedCoaches;

    public CoachNotes() {

    }

    public CoachNotes(String notes, Provider provider, Patient patient, long notesDate) {
        this.notes = notes;
        this.provider = provider;
        this.patient = patient;
        this.notesDate = notesDate;
    }

    public CoachNotes(String notes, Provider provider, Patient patient) {
        this.notes = notes;
        this.provider = provider;
        this.patient = patient;
        this.notesDate = System.currentTimeMillis();
    }

    public Long getCoachNotesId() {
        return coachNotesId;
    }

    public void setCoachNotesId(Long coachNotesId) {
        this.coachNotesId = coachNotesId;
    }

    public Long getNotesDate() {
        return notesDate;
    }

    public void setNotesDate(Long notesDate) {
        this.notesDate = notesDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    public String getTimeToDateString() {
        return timeToDateString;
    }

    public void setTimeToDateString(String timeToDateString) {
        this.timeToDateString = timeToDateString;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getNotifiedCoaches() {
        return notifiedCoaches;
    }

    public void setNotifiedCoaches(String notifiedCoaches) {
        this.notifiedCoaches = notifiedCoaches;
    }
}
