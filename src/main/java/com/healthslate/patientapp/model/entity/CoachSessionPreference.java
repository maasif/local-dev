package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name="coach_session_preference")
public class CoachSessionPreference {

	@Id
	@GeneratedValue
	@Column(name="session_id")
	@Expose
	private Long sessionId;

	@Column(name="days")
	@Expose
	private String days;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "provider_id")
    private Provider provider;

    @Column(name="times")
    @Expose
    private String times;

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    @Override
    public String toString() {
        return "CoachSessionPreference{" +
                "sessionId=" + sessionId +
                ", days='" + days + '\'' +
                ", times='" + times + '\'' +
                '}';
    }
}
