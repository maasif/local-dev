package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cohort")
public class Cohort {

	@Id
	@GeneratedValue
	@Column(name="cohort_id")
	private Long cohortId;
	
	@Column(name="name")
	private String name;
	
	@Column(name="note")
	private String note;
	
	@Column(name="description")
	private String description;
	
	@Column(name="glucose_target_low")
	private int glucoseTargetLow;
	
	@Column(name="glucose_target_high")
	private int glucoseTargetHigh;
	
	@Column(name="glucose_testing_times")
	private String glucoseTestngTimes;
	
	@Column(name="food_target_low")
	private int foodTargetLow;
	
	@Column(name="food_target_high")
	private int foodTargetHigh;
	
	@Column(name="activity_target_low")
	private int activityTargetLow;
	
	@Column(name="activity_target_high")
	private int activityTargetHigh;
	
	public Long getCohortId() {
		return cohortId;
	}

	public void setCohortId(Long cohortId) {
		this.cohortId = cohortId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getGlucoseTargetLow() {
		return glucoseTargetLow;
	}

	public void setGlucoseTargetLow(int glucoseTargetLow) {
		this.glucoseTargetLow = glucoseTargetLow;
	}

	public int getGlucoseTargetHigh() {
		return glucoseTargetHigh;
	}

	public void setGlucoseTargetHigh(int glucoseTargetHigh) {
		this.glucoseTargetHigh = glucoseTargetHigh;
	}

	public String getGlucoseTestngTimes() {
		return glucoseTestngTimes;
	}

	public void setGlucoseTestngTimes(String glucoseTestngTimes) {
		this.glucoseTestngTimes = glucoseTestngTimes;
	}

	public int getFoodTargetLow() {
		return foodTargetLow;
	}

	public void setFoodTargetLow(int foodTargetLow) {
		this.foodTargetLow = foodTargetLow;
	}

	public int getFoodTargetHigh() {
		return foodTargetHigh;
	}

	public void setFoodTargetHigh(int foodTargetHigh) {
		this.foodTargetHigh = foodTargetHigh;
	}

	public int getActivityTargetLow() {
		return activityTargetLow;
	}

	public void setActivityTargetLow(int activityTargetLow) {
		this.activityTargetLow = activityTargetLow;
	}

	public int getActivityTargetHigh() {
		return activityTargetHigh;
	}

	public void setActivityTargetHigh(int activityTargetHigh) {
		this.activityTargetHigh = activityTargetHigh;
	}
	
}
