package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;


@Entity
@Table(name="dyk_advice")
public class DYKAdvice {
	
	@Id
	@Expose (serialize = true, deserialize = true)
	@Column(name="advice_id")
	Long adviceId ;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="advice_content")
	String adviceContent ;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="advice_refrence")
	String adviceReferce ;

	public Long getAdviceId() {
		return adviceId;
	}

	public void setAdviceId(Long adviceId) {
		this.adviceId = adviceId;
	}

	public String getAdviceContent() {
		return adviceContent;
	}

	public void setAdviceContent(String adviceContent) {
		this.adviceContent = adviceContent;
	}

	public String getAdviceReferce() {
		return adviceReferce;
	}

	public void setAdviceReferce(String adviceReferce) {
		this.adviceReferce = adviceReferce;
	}
}
