package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="device")
public class Device {
	@Id
	@GeneratedValue
	@Column(name="device_id")
	private Long deviceId;
	
	@Column(name="device_mac_address")
	private String deviceMacAddress;
	
	@Column(name="registration_id")
	private String registrationId;

	@Column(name="device_type")
	private String deviceType;

    @Column(name="device_build_type")
    private String deviceBuildType;

	@Column(name="device_app_name")
	private String deviceAppName;

	@Column(name="device_manufacturer_os")
	private String deviceManufacturerAndOS;

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceMacAddress() {
		return deviceMacAddress;
	}

	public void setDeviceMacAddress(String deviceMacAddress) {
		this.deviceMacAddress = deviceMacAddress;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

    public String getDeviceBuildType() {
        return deviceBuildType;
    }

    public void setDeviceBuildType(String deviceBuildType) {
        this.deviceBuildType = deviceBuildType;
    }

	public String getDeviceAppName() {
		return deviceAppName;
	}

	public void setDeviceAppName(String deviceAppName) {
		this.deviceAppName = deviceAppName;
	}

	public String getDeviceManufacturerAndOS() {
		return deviceManufacturerAndOS;
	}

	public void setDeviceManufacturerAndOS(String userAgentString) {
		this.deviceManufacturerAndOS = userAgentString;
	}

	@Override
	public String toString() {
		return "Device{" +
				"deviceId=" + deviceId +
				", deviceMacAddress='" + deviceMacAddress + '\'' +
				", registrationId='" + registrationId + '\'' +
				", deviceType='" + deviceType + '\'' +
				", deviceBuildType='" + deviceBuildType + '\'' +
				", deviceAppName='" + deviceAppName + '\'' +
				", deviceManufacturerAndOS='" + deviceManufacturerAndOS + '\'' +
				'}';
	}
}
