package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="diabetes_type")
public class DiabetesType {

	@Id
	@GeneratedValue
	@Column(name="diabetes_type_id")
	private Long diabetesTypeId;
	
	@Column(name="name")
	private String name;

	public Long getDiabetesTypeId() {
		return diabetesTypeId;
	}

	public void setDiabetesTypeId(Long diabetesTypeId) {
		this.diabetesTypeId = diabetesTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
