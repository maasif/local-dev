package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name="dietary_notes")
public class DietaryNotes {

	@Id
	@GeneratedValue
	@Column(name="dietary_notes_id")
	@Expose
	private Long dietaryNotesId;

	@Column(name="notes_date")
	@Expose
	private Long notesDate;

	@Column(name="notes", columnDefinition = "text")
	@Expose
	private String notes;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "provider_id")
    private Provider provider;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Transient
    @Expose
    private String coachName;

    @Transient
    @Expose
    private String timeToDateString;

    public DietaryNotes() {

    }

    public DietaryNotes(String notes, Provider provider, Patient patient) {
        this.notesDate = System.currentTimeMillis();
        this.notes = notes;
        this.provider = provider;
        this.patient = patient;
    }

    public DietaryNotes(String notes, Provider provider, Patient patient, long noteDate) {
        this.notesDate = noteDate;
        this.notes = notes;
        this.provider = provider;
        this.patient = patient;
    }

    public Long getDietaryNotesId() {
        return dietaryNotesId;
    }

    public void setDietaryNotesId(Long dietaryNotesId) {
        this.dietaryNotesId = dietaryNotesId;
    }

    public Long getNotesDate() {
        return notesDate;
    }

    public void setNotesDate(Long notesDate) {
        this.notesDate = notesDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    public String getTimeToDateString() {
        return timeToDateString;
    }

    public void setTimeToDateString(String timeToDateString) {
        this.timeToDateString = timeToDateString;
    }
}
