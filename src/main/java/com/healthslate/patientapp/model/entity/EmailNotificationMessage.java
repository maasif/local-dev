package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name="email_notification_message")
public class EmailNotificationMessage {
	public static final String EMAIL_NOTIFICATION_ID = "email_notification_id";
	public static final String EMAIL_BODY = "email_body";
	public static final String SMS_BODY = "sms_body";
	public static final String TOKEN = "token";
	public static final String TOKEN_EXPIRY = "token_expiry";
	public static final String PIN_CODE = "pin_code";
	public static final String SENT_TIME = "sent_time";
	public static final String USER_ID = "user_id";
	public static final String IS_EXPIRED = "is_expired";
	
	@Id
	@GeneratedValue
	@Column(name = EMAIL_NOTIFICATION_ID)
	private Long emailNotificationId;
	
	@Expose
	@Column(name = EMAIL_BODY)
	private String emailBody;

	@Expose
	@Column(name = SMS_BODY)
	private String smsBody;

	@Expose
	@Column(name = TOKEN)
	private String token;

	@Expose
	@Column(name = TOKEN_EXPIRY)
	private Long tokenExpiry;

	@Expose
	@Column(name = PIN_CODE)
	private String pinCode;

	@Expose
	@Column(name = SENT_TIME)
	private Long sentTime;

	@Expose
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = USER_ID)
	private User user;

	@Expose
	@Column(name = IS_EXPIRED)
	private Boolean isExpired;

	public EmailNotificationMessage(){}

	public EmailNotificationMessage(String emailBody, String smsBody, String token, Long tokenExpiry, String pinCode, Long sentTime, User user) {
		this.emailBody = emailBody;
		this.smsBody = smsBody;
		this.token = token;
		this.tokenExpiry = tokenExpiry;
		this.pinCode = pinCode;
		this.sentTime = sentTime;
		this.user = user;
	}

	public Long getEmailNotificationId() {
		return emailNotificationId;
	}

	public void setEmailNotificationId(Long emailNotificationId) {
		this.emailNotificationId = emailNotificationId;
	}

	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public String getSmsBody() {
		return smsBody;
	}

	public void setSmsBody(String smsBody) {
		this.smsBody = smsBody;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getTokenExpiry() {
		return tokenExpiry;
	}

	public void setTokenExpiry(Long tokenExpiry) {
		this.tokenExpiry = tokenExpiry;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public Long getSentTime() {
		return sentTime;
	}

	public void setSentTime(Long sentTime) {
		this.sentTime = sentTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "EmailNotificationMessage{" +
				"emailNotificationId=" + emailNotificationId +
				", emailBody='" + emailBody + '\'' +
				", smsBody='" + smsBody + '\'' +
				", token='" + token + '\'' +
				", tokenExpiry=" + tokenExpiry +
				", pinCode='" + pinCode + '\'' +
				", sentTime=" + sentTime +
				'}';
	}

	public Boolean getIsExpired() {
		return isExpired;
	}

	public void setIsExpired(Boolean isExpired) {
		this.isExpired = isExpired;
	}
}
