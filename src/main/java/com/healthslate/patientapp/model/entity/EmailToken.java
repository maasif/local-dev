package com.healthslate.patientapp.model.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="email_token")
public class EmailToken {

	@Id
	@GeneratedValue
	@Column(name="email_token_id")
	private long emailTokenId;
	
	@Column(name="username")
	private String username;
	
	@Column(name="token")
	private String token;
	
	@Column(name="is_used")
	private boolean isUsed;
	
	@Column(name="date_created")
	private Date dateCreated;
	
	@Column(name="last_modified")
	private Timestamp lastModified;
	
	
	public long getEmailTokenId() {
		return emailTokenId;
	}
	public void setEmailTokenId(long emailTokenId) {
		this.emailTokenId = emailTokenId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public boolean isUsed() {
		return isUsed;
	}
	public void setUsed(boolean isUsed) {
		this.isUsed = isUsed;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Timestamp getLastModified() {
		return lastModified;
	}
	public void setLastModified(Timestamp lastModified) {
		this.lastModified = lastModified;
	}

	@Override
	public String toString() {
		return "EmailToken{" +
				"emailTokenId=" + emailTokenId +
				", username='" + username + '\'' +
				", token='" + token + '\'' +
				", isUsed=" + isUsed +
				", dateCreated=" + dateCreated +
				", lastModified=" + lastModified +
				'}';
	}
}
