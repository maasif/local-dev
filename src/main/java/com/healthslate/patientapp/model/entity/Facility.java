package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="facility")
public class Facility {

	@Expose
	@Id
	@GeneratedValue
	@Column(name="facility_id")
	private Long facilityId;
	
	@Expose
	@Column(name="name")
	private String name;

	@Expose
	@Column(name="address")
	private String address;

	@Expose
	@Column(name="city")
	private String city;

	@Expose
	@Column(name="state")
	private String state;

	@Expose
	@Column(name="zip")
	private String zip;

	@Expose
	@Column(name="contact_person_name")
	private String contactPersonName;

	@Expose
	@Column(name="facility_logo")
	private String facilityLogo;

	@Expose
	@Column(name="slogan")
	private String slogan;

	@Expose
	@Column(name="email_text_name")
	private String emailTextName;

	@Expose
	@Column(name="contact_info")
	private String contactInfo;

    @Expose
    @Column(name="group_id")
    private Integer groupId;

    @ManyToMany(mappedBy = "facilities", cascade=CascadeType.ALL, fetch= FetchType.LAZY)
    @LazyCollection(LazyCollectionOption.TRUE)
    private List<Patient> patients;

    @Expose
    @Column(name="is_notification_enabled")
    private Boolean isNotificationEnabled;

    @Expose
    @Column(name="is_skip_consent")
    private Boolean isSkipConsent;

	@Expose
	@Column(name="timezone_name")
	private String timezoneName;

	@Expose
	@Column(name="timezone_offset")
	private String timezoneOffset;

    @Expose
    @Column(name="timezone_offset_millis")
    private Long timezoneOffsetMillis;

    @Expose
    @Column(name="consent_form_file")
    private String consentFormFile;

    @Expose
    @Column(name="lead_coach")
    private Long leadCoach;

	public Facility(){

	}

	public Facility(long facilityId){
		this.facilityId = facilityId;
	}

	public Facility(long facilityId, String facilityName){
		this.facilityId = facilityId;
		this.name = facilityName;
	}

	public Long getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(Long facilityId) {
		this.facilityId = facilityId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public String getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(String contactInfo) {
		this.contactInfo = contactInfo;
	}

	public List<Patient> getPatients() {
		return patients;
	}

	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Boolean getIsNotificationEnabled() {
        return isNotificationEnabled;
    }

    public void setIsNotification(Boolean isNotificationEnabled) {
        this.isNotificationEnabled = isNotificationEnabled;
    }


	public String getFacilityLogo() {
		return facilityLogo;
	}

	public void setFacilityLogo(String facilityLogo) {
		this.facilityLogo = facilityLogo;
	}

	public String getSlogan() {
		return slogan;
	}

	public void setSlogan(String slogan) {
		this.slogan = slogan;
	}

	public String getEmailTextName() {
		return emailTextName;
	}

	public void setEmailTextName(String emailTextName) {
		this.emailTextName = emailTextName;
	}

    public Boolean getIsSkipConsent() {
        return isSkipConsent;
    }

    public void setIsSkipConsent(Boolean isSkipConsent) {
        this.isSkipConsent = isSkipConsent;
    }

	public String getTimezoneName() {
		return timezoneName;
	}

	public void setTimezoneName(String timezoneName) {
		this.timezoneName = timezoneName;
	}

	public String getTimezoneOffset() {
		return timezoneOffset;
	}

	public void setTimezoneOffset(String timezoneOffset) {
		this.timezoneOffset = timezoneOffset;
	}

    public Long getTimezoneOffsetMillis() {
        return timezoneOffsetMillis;
    }

    public void setTimezoneOffsetMillis(Long timezoneOffsetMillis) {
        this.timezoneOffsetMillis = timezoneOffsetMillis;
    }

    public String getConsentFormFile() {
        return consentFormFile;
    }

    public void setConsentFormFile(String consentFormFile) {
        this.consentFormFile = consentFormFile;
    }

    public Long getLeadCoach() {
        return leadCoach;
    }

    public void setLeadCoach(Long leadCoach) {
        this.leadCoach = leadCoach;
    }
}
