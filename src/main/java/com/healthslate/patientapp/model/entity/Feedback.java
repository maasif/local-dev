package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="feedback")
public class Feedback {
	public static final String FEEDBACK_ID = "feedback_id";
	public static final String DESCRIPTION = "description";
	public static final String TIMESTAMP = "timestamp";
	public static final String TYPE = "type";
	
	@Id
	@GeneratedValue
	@Column(name = FEEDBACK_ID)
	private Long feedbackId;
	
	@Expose
	@Column(name = DESCRIPTION)
	private String description;
	
	@Expose
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="patient_id")
	private Patient patient;

	@Expose
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="provider_id")
	private Provider provider;

	@Column(name = TYPE)
	private String type;

	@Column(name = TIMESTAMP)
	private Long timestamp;

	public Long getFeedbackId() {
		return feedbackId;
	}

	public void setFeedbackId(Long feedbackId) {
		this.feedbackId = feedbackId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "Feedback{" +
				"feedbackId=" + feedbackId +
				", description='" + description + '\'' +
				", timestamp=" + timestamp +
				'}';
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}
}
