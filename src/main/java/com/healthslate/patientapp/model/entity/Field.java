package com.healthslate.patientapp.model.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="field")
public class Field {

	@Id
	@GeneratedValue
	@Column(name="field_id")
	@Expose
	private Long field_id;
	
	@Column(name="field_label")
	@Expose
	private String fieldLabel;
	
	@Column(name="field_name")
	@Expose
	private String fieldName;
	
	@Column(name="field_type")
	@Expose
	private String fieldType;
	
	@Column(name="field_tag_type")
	@Expose
	private String fieldTagType;
	
	@Column(name="field_validation")
	@Expose
	private String fieldValidation;
	
	@Column(name="field_class")
	@Expose
	private String fieldClass;
	
	@Column(name="field_linked")
	@Expose
	private String fieldLinked;
		
	@OneToMany(cascade=CascadeType.ALL, mappedBy="field", fetch = FetchType.EAGER)
	@Expose
	private List<FieldOption> fieldOptions;

	public Long getField_id() {
		return field_id;
	}

	public void setField_id(Long field_id) {
		this.field_id = field_id;
	}

	public String getFieldLabel() {
		return fieldLabel;
	}

	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getFieldTagType() {
		return fieldTagType;
	}

	public void setFieldTagType(String fieldTagType) {
		this.fieldTagType = fieldTagType;
	}

	public String getFieldValidation() {
		return fieldValidation;
	}

	public void setFieldValidation(String fieldValidation) {
		this.fieldValidation = fieldValidation;
	}

	public String getFieldClass() {
		return fieldClass;
	}

	public void setFieldClass(String fieldClass) {
		this.fieldClass = fieldClass;
	}

	public List<FieldOption> getFieldOptions() {
		return fieldOptions;
	}

	public void setFieldOptions(List<FieldOption> fieldOptions) {
		this.fieldOptions = fieldOptions;
	}

	public String getFieldLinked() {
		return fieldLinked;
	}

	public void setFieldLinked(String fieldLinked) {
		this.fieldLinked = fieldLinked;
	}
}
