package com.healthslate.patientapp.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="field_option")
public class FieldOption {

	@Id
	@GeneratedValue
	@Column(name="field_option_id")
	@Expose
	private Long fieldOptionId;
	
	@Column(name="option_label")
	@Expose
	private String optionLabel;
	
	@Column(name="option_value")
	@Expose
	private String optionValue;
		
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="field_id")
	private Field field;

	public Long getFieldOptionId() {
		return fieldOptionId;
	}

	public void setFieldOptionId(Long fieldOptionId) {
		this.fieldOptionId = fieldOptionId;
	}

	public String getOptionLabel() {
		return optionLabel;
	}

	public void setOptionLabel(String optionLabel) {
		this.optionLabel = optionLabel;
	}

	public String getOptionValue() {
		return optionValue;
	}

	public void setOptionValue(String optionValue) {
		this.optionValue = optionValue;
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}
}
