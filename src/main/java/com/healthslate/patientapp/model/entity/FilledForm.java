package com.healthslate.patientapp.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="filled_form")
public class FilledForm {

	@Id
	@GeneratedValue
	@Column(name="filled_form_id")
	@Expose
	private Long filledFormId;
	
	@OneToOne
	@JoinColumn(name="patient_id")
	private Patient patient;
	
	@Column(name="type")
	@Expose
	private String type;
	
	@Column(name="created_date")
	@Expose
	private Date createdDate;
	
	@Column(name="last_modified_date")
	@Expose
	private Date lastModifiedDate;
	
	@Column(name="date_achieved")
	@Expose
	private Date dateAchieved;

	@Column(name="notification_count")
	@Expose
	private Integer notificationCount;

	@OneToMany(cascade = CascadeType.ALL, mappedBy="filledForm", fetch = FetchType.LAZY)
	@Expose
	private List<FormData> formDataList;

	@Expose (serialize = false, deserialize = true)
	@OneToMany(cascade = CascadeType.ALL, mappedBy="filledForm", fetch = FetchType.LAZY)
	private List<GoalAssessmentRating> goalAssessmentRating;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="filledForm", fetch = FetchType.LAZY)
	@Expose
	private List<CoachGoalAssessment> coachAssessmentList;
	
	@Column(name="should_remind")
	@Expose
	private Boolean shouldRemind;
			
	public FilledForm() {
	
	}
	
	public FilledForm(Long fillFormId) {
		this.filledFormId = fillFormId;
	}
	
	public List<FormData> getFormDataList() {
		return formDataList;
	}

	public void setFormDataList(List<FormData> formDataList) {
		this.formDataList = formDataList;
	}

	public Long getFilledFormId() {
		return filledFormId;
	}

	public void setFilledFormId(Long filledFormId) {
		this.filledFormId = filledFormId;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public List<GoalAssessmentRating> getGoalAssessmentRating() {
		return goalAssessmentRating;
	}

	public void setGoalAssessmentRating(List<GoalAssessmentRating> goalAssessmentRating) {
		this.goalAssessmentRating = goalAssessmentRating;
	}

	public Date getDateAchieved() {
		return dateAchieved;
	}
	
	public void setDateAchieved(Date dateAchieved) {
		this.dateAchieved = dateAchieved;
	}

	public Boolean getShouldRemind() {
		return shouldRemind;
	}

	public void setShouldRemind(Boolean shouldRemind) {
		this.shouldRemind = shouldRemind;
	}

	public List<CoachGoalAssessment> getCoachAssessmentList() {
		return coachAssessmentList;
	}

	public void setCoachAssessmentList(List<CoachGoalAssessment> coachAssessmentList) {
		this.coachAssessmentList = coachAssessmentList;
	}


	public Integer getNotificationCount() {
		return notificationCount;
	}

	public void setNotificationCount(Integer notificationCount) {
		this.notificationCount = notificationCount;
	}
}
