package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="food_audio")
public class FoodAudio {

	@Id
	@GeneratedValue
	@Column(name="food_audio_id")
	private Long foodAudioId;
	
	@Expose
	@Column(name="server_audio_path")
	private String serverAudioPath;
	
	@Expose
	@Column(name="client_audio_path")
	private String clientAudioPath;
	
	@Expose
	@Column(name="audio_name")
	private String audioName;
	
	@Expose
	@Column(name="timestamp")
	private Long timestamp;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="food_log_summary_id")
	private FoodLogSummary foodLogSummary;

	public Long getFoodAudioId() {
		return foodAudioId;
	}

	public void setFoodAudioId(Long foodAudioId) {
		this.foodAudioId = foodAudioId;
	}

	public String getServerAudioPath() {
		return serverAudioPath;
	}

	public void setServerAudioPath(String serverAudioPath) {
		this.serverAudioPath = serverAudioPath;
	}

	public String getClientAudioPath() {
		return clientAudioPath;
	}

	public void setClientAudioPath(String clientAudioPath) {
		this.clientAudioPath = clientAudioPath;
	}

	public String getAudioName() {
		return audioName;
	}

	public void setAudioName(String audioName) {
		this.audioName = audioName;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public FoodLogSummary getFoodLogSummary() {
		return foodLogSummary;
	}

	public void setFoodLogSummary(FoodLogSummary foodLogSummary) {
		this.foodLogSummary = foodLogSummary;
	}
}
