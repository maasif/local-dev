package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="food_image")
public class FoodImage {

	@Id
	@GeneratedValue
	@Column(name="food_image_id")
	private Long foodImageId;
	
	@Expose
	@Column(name="server_image_path")
	private String serverImagePath;
	
	@Expose
	@Column(name="client_image_path")
	private String clientImagePath;
	
	@Expose
	@Column(name="image_name")
	private String imageName;
	
	@Expose
	@Column(name="timestamp")
	private Long timestamp;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="food_log_summary_id")
	private FoodLogSummary foodLogSummary;

	public Long getFoodImageId() {
		return foodImageId;
	}

	public void setFoodImageId(Long foodImageId) {
		this.foodImageId = foodImageId;
	}

	public String getServerImagePath() {
		return serverImagePath;
	}

	public void setServerImagePath(String serverImagePath) {
		this.serverImagePath = serverImagePath;
	}

	public String getClientImagePath() {
		return clientImagePath;
	}

	public void setClientImagePath(String clientImagePath) {
		this.clientImagePath = clientImagePath;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public FoodLogSummary getFoodLogSummary() {
		return foodLogSummary;
	}

	public void setFoodLogSummary(FoodLogSummary foodLogSummary) {
		this.foodLogSummary = foodLogSummary;
	}
}
