package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="food_log_detail")
public class FoodLogDetail {

	@Id
	@GeneratedValue
	@Column(name="food_log_detail_id")
	private Long foodLogDetailId;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="food_log_summary_id")
	private FoodLogSummary foodLogSummary;
	
	@Expose
	@Column(name="no_of_servings")
	private Float numberOfServings;
	
	@Expose (serialize = true, deserialize = false)
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="food_master_id")
	private FoodMaster foodMaster;

	public FoodLogSummary getFoodLogSummary() {
		return foodLogSummary;
	}

	public void setFoodLogSummary(FoodLogSummary foodLogSummary) {
		this.foodLogSummary = foodLogSummary;
	}

	public Float getNumberOfServings() {
		return numberOfServings;
	}

	public void setNumberOfServings(Float numberOfServings) {
		this.numberOfServings = numberOfServings;
	}

	public FoodMaster getFoodMaster() {
		return foodMaster;
	}

	public void setFoodMaster(FoodMaster foodMaster) {
		this.foodMaster = foodMaster;
	}

	public Long getFoodLogDetailId() {
		return foodLogDetailId;
	}

	public void setFoodLogDetailId(Long foodLogDetailId) {
		this.foodLogDetailId = foodLogDetailId;
	}

	@Override
	public String toString() {
		return "FoodLogDetail{" +
				"foodLogDetailId=" + foodLogDetailId +
				", numberOfServings=" + numberOfServings +
				'}';
	}
}
