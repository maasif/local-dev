package com.healthslate.patientapp.model.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="food_log_summary")
public class FoodLogSummary {

	@Id
	@GeneratedValue
	@Column(name="food_log_summary_id")
	private Long foodLogSummaryId;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="carbs")
	private Float carbs;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="calories")
	private Float calories;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="fats")
	private Float fats;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="protein")
	private Float protein;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="type")
	private String type;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="timestamp")
	private Long timestamp;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="notes", columnDefinition = "text")
	private String notes;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="has_image")
	private Boolean hasImage;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="has_audio")
	private Boolean hasAudio;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="has_missing_food")
	private Boolean hasMissingFood;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="log_id")
	private Log log;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="meal_name")
	private String mealName;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="guess_carbs")
	private Float guessCarbs;
	
	@Expose (serialize = false, deserialize = true)
	private boolean isAteAgain;
	
	@Expose (serialize = true, deserialize = false)
	@OneToMany(cascade=CascadeType.ALL, mappedBy="foodLogSummary", fetch = FetchType.EAGER)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<FoodLogDetail> foodLogDetails;

	@Expose
	@OneToOne(optional=true, cascade=CascadeType.ALL, mappedBy="foodLogSummary", fetch = FetchType.EAGER)
	private FoodImage foodImage;
	
	@Expose (serialize = true, deserialize = true)
	@OneToMany(cascade=CascadeType.ALL, mappedBy="foodLogSummary", fetch = FetchType.LAZY)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<FoodAudio> foodAudios;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="foodLogSummary", fetch = FetchType.LAZY)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<Message> messages;

    @Column(name="minor_name")
    private String minorName;

	public Long getFoodLogSummaryId() {
		return foodLogSummaryId;
	}

	public void setFoodLogSummaryId(Long foodLogSummaryId) {
		this.foodLogSummaryId = foodLogSummaryId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}

	public List<FoodLogDetail> getFoodLogDetails() {
		return foodLogDetails;
	}

	public void setFoodLogDetails(List<FoodLogDetail> foodLogDetails) {
		this.foodLogDetails = foodLogDetails;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Float getCarbs() {
		return carbs;
	}

	public void setCarbs(Float carbs) {
		this.carbs = carbs;
	}

	public Float getCalories() {
		return calories;
	}

	public void setCalories(Float calories) {
		this.calories = calories;
	}

	public Float getFats() {
		return fats;
	}

	public void setFats(Float fats) {
		this.fats = fats;
	}

	public Float getProtein() {
		return protein;
	}

	public void setProtein(Float protein) {
		this.protein = protein;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public FoodImage getFoodImage() {
		return foodImage;
	}

	public void setFoodImage(FoodImage foodImage) {
		this.foodImage = foodImage;
	}

	public Boolean getHasImage() {
		return hasImage;
	}

	public void setHasImage(Boolean hasImage) {
		this.hasImage = hasImage;
	}

	public List<FoodAudio> getFoodAudios() {
		return foodAudios;
	}

	public void setFoodAudios(List<FoodAudio> foodAudios) {
		this.foodAudios = foodAudios;
	}

	public Boolean getHasAudio() {
		return hasAudio;
	}

	public void setHasAudio(Boolean hasAudio) {
		this.hasAudio = hasAudio;
	}

	public Boolean getHasMissingFood() {
		return hasMissingFood;
	}

	public void setHasMissingFood(Boolean hasMissingFood) {
		this.hasMissingFood = hasMissingFood;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public float getGuessCarbs() {
		return guessCarbs;
	}

	public void setGuessCarbs(float guessCarbs) {
		this.guessCarbs = guessCarbs;
	}

	public boolean isAteAgain() {
		return isAteAgain;
	}

	public void setAteAgain(boolean isAteAgain) {
		this.isAteAgain = isAteAgain;
	}

	@Override
	public String toString() {
		return "FoodLogSummary{" +
				"foodLogSummaryId=" + foodLogSummaryId +
				", carbs=" + carbs +
				", calories=" + calories +
				", fats=" + fats +
				", protein=" + protein +
				", type='" + type + '\'' +
				", timestamp=" + timestamp +
				", notes='" + notes + '\'' +
				", hasImage=" + hasImage +
				", hasAudio=" + hasAudio +
				", hasMissingFood=" + hasMissingFood +
				", mealName='" + mealName + '\'' +
				", guessCarbs=" + guessCarbs +
				", isAteAgain=" + isAteAgain +
				'}';
	}

    public String getMinorName() {
        return minorName;
    }

    public void setMinorName(String minorName) {
        this.minorName = minorName;
    }
}
