package com.healthslate.patientapp.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="food_master")
public class FoodMaster {

	@Id
	@GeneratedValue
	@Expose
	@Column(name="food_master_id")
	private Long foodMasterId;
	
	@Expose
	@Column(name="foodName")
	private String foodName;
	
	@Column(name="brand_name")
	private String brandName;
	
	@Column(name="brand_id")
	private String brandId;
	
	@Column(name="item_id")
	private String itemId;
	
	private String description;
	
	@Column(name="serving_size_qty")
	private String servingSizeQuantity;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="serving_size_unit")
	private String servingSizeUnit;
	
	@Column(name="serving_weight_grams")
	private String servingWeightGrams;
	
	@Column(name="serving_per_container")
	private String servingPerContainer;
	
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="last_modified")
	private Date lastModified;
	
	@Column(name="is_custom")
	private Boolean isCustom;
	
	private Integer calories;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="fats")
	private float fats;
	
	private Float saturated;
	
	@Column(name="poly_unsaturated")
	private Float polyUnsaturated;
	
	@Column(name="mono_unsaturated")
	private Float monoUnsaturated;
	
	private Float trans;
	
	private Float cholestrol;
	
	private Integer sodium;
	
	private Float potassium;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="carbs")
	private float carbs;
	
	private Float fiber;
	
	private Float sugars;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="protein")
	private float protein;

	@Column(name="vitamin_a")
	private Float vitaminA;
	
	@Column(name="vitamin_c")
	private Float vitaminC;
	
	private Float calcium;
	
	private Float iron;
	
	public Long getFoodMasterId() {
		return foodMasterId;
	}

	public void setFoodMasterId(Long foodMasterId) {
		this.foodMasterId = foodMasterId;
	}

	public String getFoodName() {
		return foodName;
	}

	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public Boolean isCustom() {
		return isCustom;
	}

	public void setCustom(Boolean isCustom) {
		this.isCustom = isCustom;
	}

	public Integer getCalories() {
		return calories;
	}

	public void setCalories(Integer calories) {
		this.calories = calories;
	}

	public Float getSaturated() {
		return saturated;
	}

	public void setSaturated(Float saturated) {
		this.saturated = saturated;
	}

	public Float getPolyUnsaturated() {
		return polyUnsaturated;
	}

	public void setPolyUnsaturated(Float polyUnsaturated) {
		this.polyUnsaturated = polyUnsaturated;
	}

	public Float getMonoUnsaturated() {
		return monoUnsaturated;
	}

	public void setMonoUnsaturated(Float monoUnsaturated) {
		this.monoUnsaturated = monoUnsaturated;
	}

	public Float getTrans() {
		return trans;
	}

	public void setTrans(Float trans) {
		this.trans = trans;
	}

	public Float getCholestrol() {
		return cholestrol;
	}

	public void setCholestrol(Float cholestrol) {
		this.cholestrol = cholestrol;
	}

	public Integer getSodium() {
		return sodium;
	}

	public void setSodium(Integer sodium) {
		this.sodium = sodium;
	}

	public Float getPotassium() {
		return potassium;
	}

	public void setPotassium(Float potassium) {
		this.potassium = potassium;
	}

	public Float getFiber() {
		return fiber;
	}

	public void setFiber(Float fiber) {
		this.fiber = fiber;
	}

	public Float getSugars() {
		return sugars;
	}

	public void setSugars(Float sugars) {
		this.sugars = sugars;
	}

	public Float getVitaminA() {
		return vitaminA;
	}

	public void setVitaminA(Float vitaminA) {
		this.vitaminA = vitaminA;
	}

	public Float getVitaminC() {
		return vitaminC;
	}

	public void setVitaminC(Float vitaminC) {
		this.vitaminC = vitaminC;
	}

	public Float getCalcium() {
		return calcium;
	}

	public void setCalcium(Float calcium) {
		this.calcium = calcium;
	}

	public Float getIron() {
		return iron;
	}

	public void setIron(Float iron) {
		this.iron = iron;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServingSizeQuantity() {
		return servingSizeQuantity;
	}

	public void setServingSizeQuantity(String servingSizeQuantity) {
		this.servingSizeQuantity = servingSizeQuantity;
	}

	public String getServingSizeUnit() {
		return servingSizeUnit;
	}

	public void setServingSizeUnit(String servingSizeUnit) {
		this.servingSizeUnit = servingSizeUnit;
	}

	public String getServingWeightGrams() {
		return servingWeightGrams;
	}

	public void setServingWeightGrams(String servingWeightGrams) {
		this.servingWeightGrams = servingWeightGrams;
	}

	public String getServingPerContainer() {
		return servingPerContainer;
	}

	public void setServingPerContainer(String servingPerContainer) {
		this.servingPerContainer = servingPerContainer;
	}

	public Boolean getIsCustom() {
		return isCustom;
	}

	public void setIsCustom(Boolean isCustom) {
		this.isCustom = isCustom;
	}

	public float getFats() {
		return fats;
	}

	public void setFats(float fats) {
		this.fats = fats;
	}

	public float getCarbs() {
		return carbs;
	}

	public void setCarbs(float carbs) {
		this.carbs = carbs;
	}

	public float getProtein() {
		return protein;
	}

	public void setProtein(float protein) {
		this.protein = protein;
	}
	
	
}
