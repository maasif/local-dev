package com.healthslate.patientapp.model.entity;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="form_data")
public class FormData {

	@Id
	@GeneratedValue
	@Column(name="form_data_id")
	@Expose
	private Long formDataId;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="filled_form_id")
	private FilledForm filledForm;
	
	@Column(name="field_name")
	@Expose
	private String fieldName;
	
	@Column(name="field_value", columnDefinition="text")
	@Expose
	private String fieldValue;

	public Long getFormDataId() {
		return formDataId;
	}

	public void setFormDataId(Long formDataId) {
		this.formDataId = formDataId;
	}

	public FilledForm getFilledForm() {
		return filledForm;
	}

	public void setFilledForm(FilledForm filledForm) {
		this.filledForm = filledForm;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
}
