package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="glucose_log_type")
public class GlucoseLogType {

	@Id
	@Expose (serialize = true, deserialize = true)
	@Column(name="glucose_log_type_id")
	private String glucoseLogTypeId;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="description")
	private String description;

	public String getGlucoseLogTypeId() {
		return glucoseLogTypeId;
	}

	public void setGlucoseLogTypeId(String glucoseLogTypeId) {
		this.glucoseLogTypeId = glucoseLogTypeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
