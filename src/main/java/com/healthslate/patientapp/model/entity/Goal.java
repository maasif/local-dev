package com.healthslate.patientapp.model.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="goal")
public class Goal {

	@Id
	@GeneratedValue
	@Column(name="goal_id")
	@Expose
	private Long goalId;
	
	@Column(name="name")
	@Expose
	private String name;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="goal", fetch = FetchType.EAGER)
	@Expose
	@OrderBy(value = "orderBy ASC")
	private List<ActionPlan> actionPlans;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="goal_category_id")
	private GoalCategory goalCategory;

	public Long getGoalId() {
		return goalId;
	}

	public void setGoalId(Long goalId) {
		this.goalId = goalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GoalCategory getGoalCategory() {
		return goalCategory;
	}

	public void setGoalCategory(GoalCategory goalCategory) {
		this.goalCategory = goalCategory;
	}

	public List<ActionPlan> getActionPlans() {
		return actionPlans;
	}

	public void setActionPlans(List<ActionPlan> actionPlans) {
		this.actionPlans = actionPlans;
	}
}
