package com.healthslate.patientapp.model.entity;

import java.util.Date;

import javax.persistence.*;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="goal_assessment_rating")
public class GoalAssessmentRating {

	@Id
	@GeneratedValue
	@Column(name = "goal_assessment_id")
	@Expose
	private Long goalAssessmentId;
		
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="filled_form_id")
	private FilledForm filledForm;
	
	@Column(name = "goal_assessment_rating")
	@Expose
	private Integer rating;

	@Column(name = "rated_on")
	private Date ratedOn;
	
	public Long getGoalAssessmentId() {
		return goalAssessmentId;
	}

	public FilledForm getFilledForm() {
		return filledForm;
	}

	public Integer getRating() {
		return rating;
	}

	public void setGoalAssessmentId(Long goalAssessmentId) {
		this.goalAssessmentId = goalAssessmentId;
	}

	public void setFilledForm(FilledForm filledForm) {
		this.filledForm = filledForm;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Date getRatedOn() {
		return ratedOn;
	}

	public void setRatedOn(Date ratedOn) {
		this.ratedOn = ratedOn;
	}
}
