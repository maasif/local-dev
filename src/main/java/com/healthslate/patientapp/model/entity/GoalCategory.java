package com.healthslate.patientapp.model.entity;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="goal_category")
public class GoalCategory {

	@Id
	@GeneratedValue
	@Column(name="goal_category_id")
	@Expose
	private Long goalCategoryId;
	
	@Column(name="name")
	@Expose
	private String name;

    @Column(name="text_color")
    @Expose
    private String textColor;

    @Column(name="bg_color")
    @Expose
    private String bgColor;

	@OneToMany(cascade = CascadeType.ALL, mappedBy="goalCategory", fetch = FetchType.EAGER)
	@Expose
	private List<Goal> goals;

	public Long getGoalCategoryId() {
		return goalCategoryId;
	}

	public void setGoalCategoryId(Long goalCategoryId) {
		this.goalCategoryId = goalCategoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Goal> getGoals() {
		return goals;
	}

	public void setGoals(List<Goal> goals) {
		this.goals = goals;
	}

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }
}
