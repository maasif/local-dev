package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name="group_posts")
public class GroupPosts {

	@Id
	@GeneratedValue
	@Column(name="group_post_id")
	@Expose
	private Long groupPostId;

	@Column(name="video_link")
	@Expose
    private String videoLink;

	@Column(name="description", columnDefinition = "text")
	@Expose
	private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "provider_id")
    private Provider provider;

    @Column(name="post_date")
    @Expose
    private Long postDate;

    @Column(name="post_type")
    @Expose
    private String postType;

    @Column(name="video_thumbnail")
    @Expose
    private String videoThumbnail;

    @Column(name="video_embed_url")
    @Expose
    private String videoEmbedUrl;

    @Column(name="link_id")
    @Expose
    private Long linkId;

    @Transient
    @Expose
    private String coachName;

    public GroupPosts(){

    }

    public GroupPosts(String description, Provider provider, Long postDate, String postType, String videoLink) {
        this.description = description;
        this.provider = provider;
        this.postDate = postDate;
        this.postType = postType;
        this.videoLink = videoLink;
    }

    public Long getGroupPostId() {
        return groupPostId;
    }

    public void setGroupPostId(Long groupPostId) {
        this.groupPostId = groupPostId;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Long getPostDate() {
        return postDate;
    }

    public void setPostDate(Long postDate) {
        this.postDate = postDate;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getVideoThumbnail() {
        return videoThumbnail;
    }

    public void setVideoThumbnail(String videoThumbnail) {
        this.videoThumbnail = videoThumbnail;
    }

    public String getVideoEmbedUrl() {
        return videoEmbedUrl;
    }

    public void setVideoEmbedUrl(String videoEmbedUrl) {
        this.videoEmbedUrl = videoEmbedUrl;
    }

    @Override
    public String toString() {
        return "GroupPosts{" +
                "groupPostId=" + groupPostId +
                ", videoLink='" + videoLink + '\'' +
                ", description='" + description + '\'' +
                ", postDate=" + postDate +
                ", postType='" + postType + '\'' +
                ", videoThumbnail='" + videoThumbnail + '\'' +
                ", videoEmbedUrl='" + videoEmbedUrl + '\'' +
                '}';
    }

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }
}
