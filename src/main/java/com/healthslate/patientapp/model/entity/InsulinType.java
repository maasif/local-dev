package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="insulin_type")
public class InsulinType {

	@Id
	@GeneratedValue
	@Column(name="insulin_type_id")
	private Long insulinTypeId;
	
	@Column(name="name")
	private String name;

	public Long getInsulinTypeId() {
		return insulinTypeId;
	}

	public void setInsulinTypeId(Long insulinTypeId) {
		this.insulinTypeId = insulinTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
