package com.healthslate.patientapp.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.amazonaws.services.autoscaling.model.Activity;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="log")
public class Log {

	@Id
	@Expose (serialize = true, deserialize = true)
	@Column(name="log_id")
	private String logId;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name="patient_id")
	private Patient patient;
	
	@Column(name="log_source")
	private String logSource;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="device_type")
	private String deviceType;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="device_mac_address")
	private String deviceMacAddress;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="created_on")
	private Long createdOn;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="log_time")
	private Long logTime;

	@Expose (serialize = true, deserialize = true)
	@Column(name="last_modified")
	private Long lastModified;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="sync_time")
	private Long syncTime;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="sync_status")
	private String syncStatus;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="is_processed")
	private Boolean isProcessed;
	
	@Column(name="is_archive")
	private Boolean isArchive;
	
	@Column(name="is_removed")
	private Boolean isRemoved;

    @Expose (serialize = true, deserialize = true)
    @Column(name="is_suggested")
    private Boolean isSuggested;

	@Expose (serialize = true, deserialize = true)
	@Column(name="is_favorite")
	private Boolean isFavorite;

	@Column(name="process_date")
	private Date processDate;
	
	@Column(name="remove_date")
	private Date removeDate;
	
	@Column(name="log_time_offset")
	private Long logTimeOffset;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="offset_key")
	private String offsetKey;
	
	@Column(name="processed_time")
	private Long processedTime;
	
	@Column(name = "log_processed_time")
	private Long logProcessedTime;

	@Expose(serialize = true, deserialize = true)
	@Column(name="user_id")
	private Long userId;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="log_type")
	private String logType;
	
	@Expose (serialize = false, deserialize = true)
	@Column(name="is_share_with_group")
	private Boolean isShareWithGroup;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="post_id")
	private Integer postId;
	
	@Expose (serialize = true, deserialize = true)
	@OneToMany(cascade=CascadeType.ALL, mappedBy="log",  fetch=FetchType.EAGER)
	private List<LogMood> logMoodList;
	
	@Expose (serialize = false, deserialize = true)
	@OneToOne(optional=true, cascade=CascadeType.ALL, mappedBy="log", fetch = FetchType.LAZY)
	private GlucoseLog glucoseLog;

	@Expose (serialize = false, deserialize = true)
	@OneToOne(optional=true, cascade=CascadeType.ALL, mappedBy="log", fetch = FetchType.EAGER)
	private ActivityLog activityLog;
	
	@Expose (serialize = false, deserialize = true)
	@OneToMany(cascade=CascadeType.ALL, mappedBy="log", fetch=FetchType.LAZY)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<ExerciseLog> exerciseLogs;
	
	@Expose (serialize = false, deserialize = true)
	@OneToMany(cascade=CascadeType.ALL, mappedBy="log", fetch=FetchType.LAZY)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<ReportError> reportErrors;
	
	@Expose (serialize = false, deserialize = true)
	@OneToOne(optional=true, cascade=CascadeType.ALL, mappedBy="log", fetch = FetchType.LAZY)
	private MedicationLog medicationLog;
	
	@Expose (serialize = true, deserialize = true)
	@OneToOne(optional=true, cascade=CascadeType.ALL, mappedBy="log", fetch = FetchType.LAZY)
	private FoodLogSummary foodLogSummary;
	
	@Expose (serialize = false, deserialize = true)
	@OneToOne(optional=true, cascade=CascadeType.ALL, mappedBy="log", fetch = FetchType.LAZY)
	private MiscLog miscLog;
	
	@Expose (serialize = false, deserialize = true)
	@OneToMany(cascade=CascadeType.ALL, mappedBy="log", fetch=FetchType.LAZY)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<Notes> notes;
	
	//To set is processed status for webservice
	@Expose (serialize = false, deserialize = true)
	@Transient
	private String status;
	
	//to save when log is uploaded
	@Expose (serialize = false, deserialize = true)
	@Column(name="upload_time")
	private Long uploadTime;

    @Expose (serialize = false, deserialize = true)
    @Transient
    private boolean isEdited;
    
	public String getLogId() {
		return logId;
	}

	public void setLogId(String logId) {
		this.logId = logId;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String getLogSource() {
		return logSource;
	}

	public void setLogSource(String logSource) {
		this.logSource = logSource;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public Long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Long createdOn) {
		this.createdOn = createdOn;
	}

	public Long getLogTime() {
		return logTime;
	}

	public void setLogTime(Long logTime) {
		this.logTime = logTime;
	}

	public Long getLastModified() {
		return lastModified;
	}

	public void setLastModified(Long lastModified) {
		this.lastModified = lastModified;
	}

	public Long getSyncTime() {
		return syncTime;
	}

	public void setSyncTime(Long syncTime) {
		this.syncTime = syncTime;
	}

	public String getSyncStatus() {
		return syncStatus;
	}

	public void setSyncStatus(String syncStatus) {
		this.syncStatus = syncStatus;
	}

	public List<LogMood> getLogMoods() {
		return logMoodList;
	}
	public void setLogMoods(List<LogMood> logMood) {
		this.logMoodList = logMood;
	}

	public GlucoseLog getGlucoseLog() {
		return glucoseLog;
	}

	public void setGlucoseLog(GlucoseLog glucoseLog) {
		this.glucoseLog = glucoseLog;
	}
	
	public FoodLogSummary getFoodLogSummary() {
		return foodLogSummary;
	}

	public void setFoodLogSummary(FoodLogSummary foodLogSummary) {
		this.foodLogSummary = foodLogSummary;
	}

	public MedicationLog getMedicationLog() {
		return medicationLog;
	}

	public void setMedicationLog(MedicationLog medicationLog) {
		this.medicationLog = medicationLog;
	}

	public MiscLog getMiscLog() {
		return miscLog;
	}

	public void setMiscLog(MiscLog miscLog) {
		this.miscLog = miscLog;
	}

	public Boolean getIsProcessed() {
		return isProcessed;
	}

	public void setIsProcessed(Boolean isProcessed) {
		this.isProcessed = isProcessed;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public List<Notes> getNotes() {
		return notes;
	}

	public void setNotes(List<Notes> notes) {
		this.notes = notes;
	}

	public List<ExerciseLog> getExerciseLogs() {
		return exerciseLogs;
	}

	public void setExerciseLogs(List<ExerciseLog> exerciseLogs) {
		this.exerciseLogs = exerciseLogs;
	}

	public Long getLogTimeOffset() {
		return logTimeOffset;
	}

	public void setLogTimeOffset(Long logTimeOffset) {
		this.logTimeOffset = logTimeOffset;
	}

	public String getDeviceMacAddress() {
		return deviceMacAddress;
	}
	public void setDeviceMacAddress(String deviceMacAddress) {
		this.deviceMacAddress = deviceMacAddress;
	}

	public Boolean getIsRemoved() {
		return isRemoved;
	}

	public void setIsRemoved(Boolean isRemoved) {
		this.isRemoved = isRemoved;
	}

	public Date getRemoveDate() {
		return removeDate;
	}

	public void setRemoveDate(Date removeDate) {
		this.removeDate = removeDate;
	}

	public Boolean getIsShareWithGroup() {
		return isShareWithGroup;
	}

	public void setIsShareWithGroup(Boolean isShareWithGroup) {
		this.isShareWithGroup = isShareWithGroup;
	}

	public List<ReportError> getReportErrors() {
		return reportErrors;
	}

	public void setReportErrors(List<ReportError> reportErrors) {
		this.reportErrors = reportErrors;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}

	public Boolean getIsArchive() {
		return isArchive;
	}

	public void setIsArchive(Boolean isArchive) {
		this.isArchive = isArchive;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOffsetKey() {
		return offsetKey;
	}

	public void setOffsetKey(String offsetKey) {
		this.offsetKey = offsetKey;
	}

	public Long getProcessedTime() {
		return processedTime;
	}

	public void setProcessedTime(Long processedTime) {
		this.processedTime = processedTime;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public Integer getPostId() {
		return postId;
	}

	public void setPostId(Integer postId) {
		this.postId = postId;
	}

	public Long getUploadTime() {
		return uploadTime;
	}

	public void setUploadTime(Long uploadTime) {
		this.uploadTime = uploadTime;
	}

    public boolean getIsEdited() {
        return isEdited;
    }

    public void setIsEdited(boolean isEdited) {
        this.isEdited = isEdited;
    }

	public Long getLogProcessedTime() {
		return logProcessedTime;
	}

	public void setLogProcessedTime(Long logProcessedTime) {
		this.logProcessedTime = logProcessedTime;
	}

	public List<LogMood> getLogMoodList() {
		return logMoodList;
	}

	public void setLogMoodList(List<LogMood> logMoodList) {
		this.logMoodList = logMoodList;
	}

	public void setEdited(boolean isEdited) {
		this.isEdited = isEdited;
	}

	public ActivityLog getActivityLog() {
		return activityLog;
	}

	public void setActivityLog(ActivityLog activityLog) {
		this.activityLog = activityLog;
	}

	@Override
	public String toString() {
		return "Log{" +
				"logId='" + logId + '\'' +
				", logSource='" + logSource + '\'' +
				", deviceType='" + deviceType + '\'' +
				", deviceMacAddress='" + deviceMacAddress + '\'' +
				", createdOn=" + createdOn +
				", logTime=" + logTime +
				", lastModified=" + lastModified +
				", syncTime=" + syncTime +
				", syncStatus='" + syncStatus + '\'' +
				", isProcessed=" + isProcessed +
				", isArchive=" + isArchive +
				", isRemoved=" + isRemoved +
				", processDate=" + processDate +
				", removeDate=" + removeDate +
				", logTimeOffset=" + logTimeOffset +
				", offsetKey='" + offsetKey + '\'' +
				", processedTime=" + processedTime +
				", logProcessedTime=" + logProcessedTime +
				", userId=" + userId +
				", logType='" + logType + '\'' +
				", isShareWithGroup=" + isShareWithGroup +
				", postId=" + postId +
				", notes=" + notes +
				", status='" + status + '\'' +
				", uploadTime=" + uploadTime +
				", isEdited=" + isEdited +
				", isFavorite=" + isFavorite+
				'}';
	}

    public Boolean getIsSuggested() {
        return isSuggested;
    }

    public void setIsSuggested(Boolean isSuggested) {
        this.isSuggested = isSuggested;
    }

	public boolean isEdited() {
		return isEdited;
	}

	public Boolean getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(Boolean isFavorite) {
		this.isFavorite = isFavorite;
	}
}
