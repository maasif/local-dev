package com.healthslate.patientapp.model.entity;


import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@SuppressWarnings("serial")
@Entity
@Table(name="log_mood")
public class LogMood implements Serializable {

	@Id 
	@GeneratedValue
	@Expose (serialize = true, deserialize = true)
	@Column(name="log_mood_id")
	private Long logMoodId;
	
	@Expose (serialize = true, deserialize = true)
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="mood_id")
	private Mood mood;

	@Expose (serialize = false, deserialize = true)
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="log_id")
    private Log log;

	public Mood getMood() {
		return mood;
	}

	public void setMood(Mood mood) {
		this.mood = mood;
	}

	public Long getLogMoodId() {
		return logMoodId;
	}

	public void setLogMoodId(Long logMoodId) {
		this.logMoodId = logMoodId;
	}

    public boolean isExist(String moodDiscription) {
        return (mood.getDescription().equalsIgnoreCase(moodDiscription));
    }

    public Log getLog() {
        return log;
    }

    public void setLog(Log log) {
        this.log = log;
    }
}
