package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="medication_master")
public class MedicationMaster {

	@Id
	@GeneratedValue
	@Column(name="medication_master_id")
	private Long medicationMasterId;
	
	private String name;
	
	private String type;
	
	private String description;

	@Column(name="suggested_dosage")
	private String suggestedDosage;

	public Long getMedicationMasterId() {
		return medicationMasterId;
	}

	public void setMedicationMasterId(Long medicationMasterId) {
		this.medicationMasterId = medicationMasterId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSuggestedDosage() {
		return suggestedDosage;
	}

	public void setSuggestedDosage(String suggestedDosage) {
		this.suggestedDosage = suggestedDosage;
	}
	
	
}
