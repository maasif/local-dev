package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name="member_key_facts")
public class MemberKeyFacts {

	@Id
	@GeneratedValue
	@Column(name="member_key_facts_id")
	@Expose
	private Long memberKeyFactsId;

	@Column(name="timestamp")
	@Expose
	private Long timestamp;

	@Column(name="key_fact", columnDefinition = "text")
	@Expose
	private String keyFact;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "provider_id")
    private Provider provider;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Transient
    @Expose
    private String coachName;

    @Transient
    @Expose
    private String timeToDateString;

    public MemberKeyFacts() {

    }

    public MemberKeyFacts(String keyFact, Provider provider, Patient patient) {
        this.timestamp = System.currentTimeMillis();
        this.keyFact = keyFact;
        this.provider = provider;
        this.patient = patient;
    }

    public MemberKeyFacts(String keyFact, Provider provider, Patient patient, long keyFactsDate) {
        this.timestamp = keyFactsDate;
        this.keyFact = keyFact;
        this.provider = provider;
        this.patient = patient;
    }

    public Long getMemberKeyFactsId() {
        return memberKeyFactsId;
    }

    public void setMemberKeyFactsId(Long memberKeyFactsId) {
        this.memberKeyFactsId = memberKeyFactsId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getKeyFact() {
        return keyFact;
    }

    public void setKeyFact(String keyFact) {
        this.keyFact = keyFact;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    public String getTimeToDateString() {
        return timeToDateString;
    }

    public void setTimeToDateString(String timeToDateString) {
        this.timeToDateString = timeToDateString;
    }
}
