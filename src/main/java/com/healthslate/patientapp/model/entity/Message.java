package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

/*
* ======= FILE CHANGE HISTORY =======
* [2/4/2015]: Added imagePath property  __oz
* ===================================
 */

@Entity
@Table(name="message")
public class Message {

	@Id
	@GeneratedValue
	@Column(name="message_id")
	private Long messageId;

	@ManyToOne()
	@JoinColumn(name="user_id")
	private User user;

	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name="patient_id")
	private Patient patient;

	@Expose(serialize= false, deserialize = false)
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="food_log_summary_id")
	private FoodLogSummary foodLogSummary;

	@Expose
	@Column(name="owner")
	private String owner;

	@Expose
	@Column(name="message")
	private String message;

	@Expose
	@Column(name="timestamp")
	private Long timestamp;

	@Expose
	@Column(name="read_status")
	private Boolean readStatus;

	@Expose
	@Column(name="image_path")
	private String imagePath;

	@Expose
	@Column(name="video_link")
	private String videoLink;

	@Expose
	@Column(name = "is_error")
	private Boolean isError;

	@Transient
	@Expose
	private String providerName;

	@Transient
	@Expose
	private String patientName;

	@Transient
	@Expose
	private Boolean isMealMessage;

	@Transient
	@Expose
	private String patientProfileImage;

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public FoodLogSummary getFoodLogSummary() {
		return foodLogSummary;
	}

	public void setFoodLogSummary(FoodLogSummary foodLogSummary) {
		this.foodLogSummary = foodLogSummary;
	}

	public Boolean getReadStatus() {
		return readStatus;
	}

	public void setReadStatus(Boolean readStatus) {
		this.readStatus = readStatus;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public Boolean getIsMealMessage() {
		return isMealMessage;
	}

	public void setIsMealMessage(Boolean isMealMessage) {
		this.isMealMessage = isMealMessage;
	}

	public String getPatientProfileImage() {
		return patientProfileImage;
	}

	public void setPatientProfileImage(String patientProfileImage) {
		this.patientProfileImage = patientProfileImage;
	}

	public String getVideoLink() {
		return videoLink;
	}

	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}

	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	@Override
	public String toString() {
		return "Message{" +
				"messageId=" + messageId +
				", owner='" + owner + '\'' +
				", message='" + message + '\'' +
				", timestamp=" + timestamp +
				", readStatus=" + readStatus +
				", imagePath='" + imagePath + '\'' +
				", videoLink='" + videoLink + '\'' +
				", isError=" + isError +
				", providerName='" + providerName + '\'' +
				", patientName='" + patientName + '\'' +
				", isMealMessage=" + isMealMessage +
				", patientProfileImage='" + patientProfileImage + '\'' +
				'}';
	}
}
