package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name="misfit_activity_log")
public class MisfitActivityLog {

	public static final String ACTIVITY_LOG_ID = "activity_log_id";
	public static final String STEPS = "steps";
	public static final String TIMESTAMP = "timestamp";
    public static final String UPLOAD_TIME = "upload_Time";
    public static final String LOG_TIME_OFFSET = "log_Time_Offset";
    public static final String BATTERY_STATUS = "battery_status";

	@Expose (serialize = false, deserialize = true)
    @Id
    @GeneratedValue
    @Column(name = ACTIVITY_LOG_ID)
	private Long activityLogId;

    @Expose (serialize = false, deserialize = true)
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="patient_id")
    private Patient patient;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name = TIMESTAMP)
	private long timestamp;

    @Expose (serialize = true, deserialize = true)
    @Column(name = UPLOAD_TIME)
    private long uploadTime;

    @Expose (serialize = true, deserialize = true)
    @Column(name = STEPS)
    private int steps;

    @Expose (serialize = true, deserialize = true)
    @Column(name = LOG_TIME_OFFSET)
    private Long logTimeOffset;

    @Expose (serialize = true, deserialize = true)
    @Column(name = BATTERY_STATUS)
    private int batteryStatus;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public long getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(long uploadTime) {
        this.uploadTime = uploadTime;
    }

    public Long getActivityLogId() {
        return activityLogId;
    }

    public void setActivityLogId(Long activityLogId) {
        this.activityLogId = activityLogId;
    }

    public Long getLogTimeOffset() {
        return logTimeOffset;
    }

    public void setLogTimeOffset(Long logTimeOffset) {
        this.logTimeOffset = logTimeOffset;
    }

    public int getBatteryStatus() {
        return batteryStatus;
    }

    public void setBatteryStatus(int batteryStatus) {
        this.batteryStatus = batteryStatus;
    }
}
