package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="news_feed_new_post")
public class NewsfeedNewPost {

	@Id
	@GeneratedValue
	@Expose (serialize = true, deserialize = true)
	@Column(name="news_feed_new_post_id")
	private Long newsfeedNewPostId;
	
	@Column(name="title")
	@Expose (serialize = true, deserialize = true)
	private String title;
	
	@Column(name="description")
	@Expose (serialize = true, deserialize = true)
	private String description;
	
	@Column(name="tags")
	@Expose (serialize = true, deserialize = true)
	private String tags;
	
	@Column(name="user_name")
	@Expose (serialize = true, deserialize = true)
	private String userName;
	
	@Column(name="file_name")
	@Expose (serialize = true, deserialize = true)
	private String filename;
	
	@Column(name="encoded_content")
	@Expose (serialize = true, deserialize = true)
	private String encodedContent;
	
	@Column(name="file_type")
	@Expose (serialize = true, deserialize = true)
	private String fileType;

	public Long getNewsfeedNewPostId() {
		return newsfeedNewPostId;
	}

	public void setNewsfeedNewPostId(Long newsfeedNewPostId) {
		this.newsfeedNewPostId = newsfeedNewPostId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getEncodedContent() {
		return encodedContent;
	}

	public void setEncodedContent(String encodedContent) {
		this.encodedContent = encodedContent;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
}
