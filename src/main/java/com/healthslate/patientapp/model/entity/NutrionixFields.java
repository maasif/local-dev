package com.healthslate.patientapp.model.entity;

public class NutrionixFields {
	
    private String item_id;
    private String item_name;
    private String brand_id;
    private String brand_name;
    private String item_description;
    private Integer nf_calories;
    private Integer nf_total_fat;
    private Integer nf_sodium;
    private Integer nf_total_carbohydrate;
    private Integer nf_protein;
    private String nf_serving_size_unit;
    
	public String getItem_id() {
		return item_id;
	}
	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	public String getBrand_id() {
		return brand_id;
	}
	public void setBrand_id(String brand_id) {
		this.brand_id = brand_id;
	}
	public String getBrand_name() {
		return brand_name;
	}
	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name;
	}
	public String getItem_description() {
		return item_description;
	}
	public void setItem_description(String item_description) {
		this.item_description = item_description;
	}
	public Integer getNf_calories() {
		return nf_calories;
	}
	public void setNf_calories(Integer nf_calories) {
		this.nf_calories = nf_calories;
	}
	public Integer getNf_total_fat() {
		return nf_total_fat;
	}
	public void setNf_total_fat(Integer nf_total_fat) {
		this.nf_total_fat = nf_total_fat;
	}
	public Integer getNf_sodium() {
		return nf_sodium;
	}
	public void setNf_sodium(Integer nf_sodium) {
		this.nf_sodium = nf_sodium;
	}
	public Integer getNf_total_carbohydrate() {
		return nf_total_carbohydrate;
	}
	public void setNf_total_carbohydrate(Integer nf_total_carbohydrate) {
		this.nf_total_carbohydrate = nf_total_carbohydrate;
	}
	public Integer getNf_protein() {
		return nf_protein;
	}
	public void setNf_protein(Integer nf_protein) {
		this.nf_protein = nf_protein;
	}
	public String getNf_serving_size_unit() {
		return nf_serving_size_unit;
	}
	public void setNf_serving_size_unit(String nf_serving_size_unit) {
		this.nf_serving_size_unit = nf_serving_size_unit;
	}
}
