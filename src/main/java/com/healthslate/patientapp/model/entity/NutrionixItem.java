package com.healthslate.patientapp.model.entity;

public class NutrionixItem {
	
	    private String item_id;
	    private String item_name;
	    private String brand_id;
	    private String brand_name;
	    private String item_description;
	    private String updated_at;
	    private String nf_calories;
	    private String nf_total_fat;
	    private String nf_saturated_fat;
	    private String nf_trans_fatty_acid;
	    private String nf_polyunsaturated_fat;
	    private String nf_monounsaturated_fat;
	    private String nf_cholesterol;
	    private String nf_sodium;
	    private String nf_total_carbohydrate;
	    private String nf_dietary_fiber;
	    private String nf_sugars;
	    private String nf_protein;
	    private String nf_vitamin_a_dv;
	    private String nf_vitamin_c_dv;
	    private String nf_calcium_dv;
	    private String nf_iron_dv;
	    private String nf_refuse_pct;
	    private String nf_servings_per_container;
	    private String nf_serving_size_qty;
	    private String nf_serving_size_unit;
	    private String nf_serving_weight_grams;
		public String getItem_id() {
			return item_id;
		}
		public void setItem_id(String item_id) {
			this.item_id = item_id;
		}
		public String getItem_name() {
			return item_name;
		}
		public void setItem_name(String item_name) {
			this.item_name = item_name;
		}
		public String getBrand_id() {
			return brand_id;
		}
		public void setBrand_id(String brand_id) {
			this.brand_id = brand_id;
		}
		public String getBrand_name() {
			return brand_name;
		}
		public void setBrand_name(String brand_name) {
			this.brand_name = brand_name;
		}
		public String getItem_description() {
			return item_description;
		}
		public void setItem_description(String item_description) {
			this.item_description = item_description;
		}
		public String getUpdated_at() {
			return updated_at;
		}
		public void setUpdated_at(String updated_at) {
			this.updated_at = updated_at;
		}
		public String getNf_calories() {
			return nf_calories;
		}
		public void setNf_calories(String nf_calories) {
			this.nf_calories = nf_calories;
		}
		public String getNf_total_fat() {
			return nf_total_fat;
		}
		public void setNf_total_fat(String nf_total_fat) {
			this.nf_total_fat = nf_total_fat;
		}
		public String getNf_saturated_fat() {
			return nf_saturated_fat;
		}
		public void setNf_saturated_fat(String nf_saturated_fat) {
			this.nf_saturated_fat = nf_saturated_fat;
		}
		public String getNf_trans_fatty_acid() {
			return nf_trans_fatty_acid;
		}
		public void setNf_trans_fatty_acid(String nf_trans_fatty_acid) {
			this.nf_trans_fatty_acid = nf_trans_fatty_acid;
		}
		public String getNf_polyunsaturated_fat() {
			return nf_polyunsaturated_fat;
		}
		public void setNf_polyunsaturated_fat(String nf_polyunsaturated_fat) {
			this.nf_polyunsaturated_fat = nf_polyunsaturated_fat;
		}
		public String getNf_monounsaturated_fat() {
			return nf_monounsaturated_fat;
		}
		public void setNf_monounsaturated_fat(String nf_monounsaturated_fat) {
			this.nf_monounsaturated_fat = nf_monounsaturated_fat;
		}
		public String getNf_cholesterol() {
			return nf_cholesterol;
		}
		public void setNf_cholesterol(String nf_cholesterol) {
			this.nf_cholesterol = nf_cholesterol;
		}
		public String getNf_sodium() {
			return nf_sodium;
		}
		public void setNf_sodium(String nf_sodium) {
			this.nf_sodium = nf_sodium;
		}
		public String getNf_total_carbohydrate() {
			return nf_total_carbohydrate;
		}
		public void setNf_total_carbohydrate(String nf_total_carbohydrate) {
			this.nf_total_carbohydrate = nf_total_carbohydrate;
		}
		public String getNf_dietary_fiber() {
			return nf_dietary_fiber;
		}
		public void setNf_dietary_fiber(String nf_dietary_fiber) {
			this.nf_dietary_fiber = nf_dietary_fiber;
		}
		public String getNf_sugars() {
			return nf_sugars;
		}
		public void setNf_sugars(String nf_sugars) {
			this.nf_sugars = nf_sugars;
		}
		public String getNf_protein() {
			return nf_protein;
		}
		public void setNf_protein(String nf_protein) {
			this.nf_protein = nf_protein;
		}
		public String getNf_vitamin_a_dv() {
			return nf_vitamin_a_dv;
		}
		public void setNf_vitamin_a_dv(String nf_vitamin_a_dv) {
			this.nf_vitamin_a_dv = nf_vitamin_a_dv;
		}
		public String getNf_vitamin_c_dv() {
			return nf_vitamin_c_dv;
		}
		public void setNf_vitamin_c_dv(String nf_vitamin_c_dv) {
			this.nf_vitamin_c_dv = nf_vitamin_c_dv;
		}
		public String getNf_calcium_dv() {
			return nf_calcium_dv;
		}
		public void setNf_calcium_dv(String nf_calcium_dv) {
			this.nf_calcium_dv = nf_calcium_dv;
		}
		public String getNf_iron_dv() {
			return nf_iron_dv;
		}
		public void setNf_iron_dv(String nf_iron_dv) {
			this.nf_iron_dv = nf_iron_dv;
		}
		public String getNf_refuse_pct() {
			return nf_refuse_pct;
		}
		public void setNf_refuse_pct(String nf_refuse_pct) {
			this.nf_refuse_pct = nf_refuse_pct;
		}
		public String getNf_servings_per_container() {
			return nf_servings_per_container;
		}
		public void setNf_servings_per_container(String nf_servings_per_container) {
			this.nf_servings_per_container = nf_servings_per_container;
		}
		public String getNf_serving_size_qty() {
			return nf_serving_size_qty;
		}
		public void setNf_serving_size_qty(String nf_serving_size_qty) {
			this.nf_serving_size_qty = nf_serving_size_qty;
		}
		public String getNf_serving_size_unit() {
			return nf_serving_size_unit;
		}
		public void setNf_serving_size_unit(String nf_serving_size_unit) {
			this.nf_serving_size_unit = nf_serving_size_unit;
		}
		public String getNf_serving_weight_grams() {
			return nf_serving_weight_grams;
		}
		public void setNf_serving_weight_grams(String nf_serving_weight_grams) {
			this.nf_serving_weight_grams = nf_serving_weight_grams;
		}
	    
	    
}
