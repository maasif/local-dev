package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name="one_to_one_sessions")
public class OneToOneSession {

	@Id
	@GeneratedValue
	@Column(name="session_id")
	@Expose
	private Long sessionId;

	@Column(name="session_time")
	@Expose
	private Long sessionTime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "provider_id")
    private Provider provider;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Column(name="session_created_by")
    @Expose
    private Long sessionCreatedBy;

    @Transient
    @Expose
    private String coachName;

    @Transient
    @Expose
    private String timeToDateString;

    @Transient
    @Expose
    private String onlyTimeString;

    @Transient
    @Expose
    private String coachType;

    @Transient
    @Expose
    private long coachId;

    @Transient
    @Expose
    private boolean disableEditing;

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(Long sessionTime) {
        this.sessionTime = sessionTime;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    public String getTimeToDateString() {
        return timeToDateString;
    }

    public void setTimeToDateString(String timeToDateString) {
        this.timeToDateString = timeToDateString;
    }

    public String getCoachType() {
        return coachType;
    }

    public void setCoachType(String coachType) {
        this.coachType = coachType;
    }

    public Long getSessionCreatedBy() {
        return sessionCreatedBy;
    }

    public void setSessionCreatedBy(Long sessionCreatedBy) {
        this.sessionCreatedBy = sessionCreatedBy;
    }

    public String getOnlyTimeString() {
        return onlyTimeString;
    }

    public void setOnlyTimeString(String onlyTimeString) {
        this.onlyTimeString = onlyTimeString;
    }

    public long getCoachId() {
        return coachId;
    }

    public void setCoachId(long coachId) {
        this.coachId = coachId;
    }

    public boolean isDisableEditing() {
        return disableEditing;
    }

    public void setDisableEditing(boolean disableEditing) {
        this.disableEditing = disableEditing;
    }
}
