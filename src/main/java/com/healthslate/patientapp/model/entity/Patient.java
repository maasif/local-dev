package com.healthslate.patientapp.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.google.gson.annotations.Expose;
import com.healthslate.patientapp.model.dto.CoachDTO;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="patient")
public class Patient {

	@Id
	@Expose
	@GeneratedValue
	@Column(name="patient_id")
	private Long patientId;

	@Expose
	@OneToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User user;
	
	@Expose
	@Column(name="mrn")
	private String mrn;
	
	@Expose
	@Column(name="dob")
	private Date dob;
	
	@Expose
	@Column(name="gender")
	private String gender;
	
	@Expose
	@Column(name="state")
	private String state;
	
	@Expose
	@Column(name="race")
	private String race;

    @Expose
    @Column(name="nickname")
    private String nickname;

    @Expose
    @Column(name="education")
    private String education;

    @Expose
    @Column(name="language")
    private String language;

    @Expose
    @Column(name="image_path")
    private String imagePath;

	@Expose
	@Transient
	private String patientAppVersionNo;

	@Transient
	private String first_name;
	
	@Transient
	private String last_name;
	
	@Transient
	private String age;
	
	@Expose
	@Column(name="weight")
	private Float weight;
	
	@Expose
	@Column(name="height")
	private Float height;
	
	@Expose
	@Column(name="is_pregnant")
	private Boolean isPregnant;
	
	@Expose
	@Column(name="is_diabetic")
	private Boolean isDiabetic;
	
	@Expose
	@Column(name="carb_ratio")
	private Float carbRatio;
	
	@Expose
	@Column(name="take_oral_medic")
	private Boolean takeOralMedic;
	
	@Expose
	@Column(name="use_insulin")
	private Boolean useInsulin;
	
	@Expose
	@Column(name="date_first_insulin")
	private Date dateFirstInsulin;
	
	@Expose
	@Column(name="insulin_correction")
	private Float insulinCorrection;
	
	@Expose
	@Column(name="device_mac_address")
	private String deviceMacAddress;

	@Expose
	@Column(name="password_expiry_time")
	private Long passwordExpiryTime;

	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="cohort_id")
	private Cohort cohort;
	
    @ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Facility> facilities;

	@ManyToMany(cascade=CascadeType.ALL, mappedBy = "patients", fetch = FetchType.LAZY)
	private List<Provider> providers;

	@Expose (serialize = false, deserialize = true)
	@OneToMany(cascade=CascadeType.ALL, mappedBy="patient", fetch=FetchType.LAZY)
	@LazyCollection(LazyCollectionOption.TRUE)
	private List<MisfitActivityLog> misfitActivityLogs;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="diabetes_type_id")
	private DiabetesType diabetesType;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="insulin_type_id")
	private InsulinType insulinType;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="patient", fetch = FetchType.LAZY)
	private List<Message> messages;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="patient", fetch = FetchType.LAZY)
	@Expose
	private List<FilledForm> filledForms;
	
	@Expose (serialize = true, deserialize = true)
	@OneToOne(optional=true, cascade=CascadeType.ALL, mappedBy="patient", fetch = FetchType.LAZY)
    private Target target;

	@OneToMany(cascade=CascadeType.ALL, mappedBy="patient", fetch = FetchType.LAZY)
	private List<Feedback> feedback;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="is_signed")
	private Boolean isSigned;

    @Expose (serialize = true, deserialize = true)
    @Column(name="is_invited")
    private Boolean isInvited;

    @Expose (serialize = true, deserialize = true)
    @Column(name="is_consent_accepted")
    private Boolean isConsentAccepted;

    @Expose (serialize = true, deserialize = true)
    @Column(name="is_archived")
    private Boolean isArchived;

	@Expose (serialize = true, deserialize = true)
	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Transient
	@Expose (serialize = false, deserialize = true)
	private String image;

	@Column(name="lead_coach_id")
	@Expose
	private Long leadCoachId;

    @Column(name="invitation_sent_date")
    @Expose
    private Long invitationSentDate;

    @Column(name="request_received_date")
    @Expose
    private Long requestReceivedDate;

    @Expose (serialize = true, deserialize = true)
    @Column(name="is_approved")
    private String isApproved;

	@Expose (serialize = true, deserialize = true)
	@Transient
	private List<CoachDTO> coachList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patient", fetch = FetchType.LAZY)
    private List<OneToOneSession> oneToOneSessions;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patient", fetch = FetchType.LAZY)
    private List<CoachNotes> coachNotes;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patient", fetch = FetchType.LAZY)
    private List<DietaryNotes> dietaryNotes;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "patient", fetch = FetchType.LAZY)
    private PatientDiabetesInfo diabetesInfo;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "patient", fetch = FetchType.LAZY)
    private PatientDietaryInfo dietaryInfo;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patient", fetch = FetchType.LAZY)
    @OrderBy(value = "timestamp DESC")
    private List<PatientMedication> medication;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "patient", fetch = FetchType.LAZY)
    private PatientCurriculum curriculum;

    @Expose (serialize = true, deserialize = true)
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "patient", fetch = FetchType.LAZY)
    private PatientMotivationImage patientMotivationImage;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "patient", fetch = FetchType.LAZY)
    private PatientSessionPreference patientSessionPreference;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "patient", fetch = FetchType.LAZY)
    private PatientEatingPreference patientEatingPreference;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patient", fetch = FetchType.LAZY)
    private List<MemberKeyFacts> keyFacts;

    @Expose
    @Transient
    private Boolean isPatientSessionPreferenceSet;

    @Expose
    @Transient
    private Boolean isEatingPreferenceSet;

    @Column(name="uuid")
    @Expose
    private String uuid;

	@Column(name="primary_food_coach_id")
	@Expose
	private Long primaryFoodCoachId;

	@Expose
	@Column(name="is_three_day_email_sent")
	private Boolean isThreeDayEmailSent;

    @Expose
    @Column(name="deleted_on")
    private Date deletedOn;

    @Expose
    @Transient
    private String facilityName;

	@Expose
	@Transient
	private long facilityId;

	@Expose (serialize = true, deserialize = true)
	@Column(name="meter_serial_number")
	private String meterSerialNumber;

	public Patient() {
	}

	public List<FilledForm> getFilledForms() {
		return filledForms;
	}

	public void setFilledForms(List<FilledForm> filledForms) {
		this.filledForms = filledForms;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getMrn() {
		return mrn;
	}

	public void setMrn(String mrn) {
		this.mrn = mrn;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public Float getHeight() {
		return height;
	}

	public void setHeight(Float height) {
		this.height = height;
	}

	public Boolean getIsPregnant() {
		return isPregnant;
	}

	public void setIsPregnant(Boolean isPregnant) {
		this.isPregnant = isPregnant;
	}

	public Boolean getIsDiabetic() {
		return isDiabetic;
	}

	public void setIsDiabetic(Boolean isDiabetic) {
		this.isDiabetic = isDiabetic;
	}

	public Float getCarbRatio() {
		return carbRatio;
	}

	public void setCarbRatio(Float carbRatio) {
		this.carbRatio = carbRatio;
	}

	public Boolean getTakeOralMedic() {
		return takeOralMedic;
	}

	public void setTakeOralMedic(Boolean takeOralMedic) {
		this.takeOralMedic = takeOralMedic;
	}

	public Boolean getUseInsulin() {
		return useInsulin;
	}

	public void setUseInsulin(Boolean useInsulin) {
		this.useInsulin = useInsulin;
	}

	public Date getDateFirstInsulin() {
		return dateFirstInsulin;
	}

	public void setDateFirstInsulin(Date dateFirstInsulin) {
		this.dateFirstInsulin = dateFirstInsulin;
	}

	public Float getInsulinCorrection() {
		return insulinCorrection;
	}

	public void setInsulinCorrection(Float insulinCorrection) {
		this.insulinCorrection = insulinCorrection;
	}

	public String getDeviceMacAddress() {
		return deviceMacAddress;
	}

	public void setDeviceMacAddress(String deviceMacAddress) {
		this.deviceMacAddress = deviceMacAddress;
	}

	public Cohort getCohort() {
		return cohort;
	}

	public void setCohort(Cohort cohort) {
		this.cohort = cohort;
	}

	public List<Facility> getFacilities() {
		return facilities;
	}

	public void setFacilities(List<Facility> facilities) {
		this.facilities = facilities;
	}

	public List<Provider> getProviders() {
		return providers;
	}

	public void setProviders(List<Provider> providers) {
		this.providers = providers;
	}

	public DiabetesType getDiabetesType() {
		return diabetesType;
	}

	public void setDiabetesType(DiabetesType diabetesType) {
		this.diabetesType = diabetesType;
	}

	public InsulinType getInsulinType() {
		return insulinType;
	}

	public void setInsulinType(InsulinType insulinType) {
		this.insulinType = insulinType;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public Target getTarget() {
		return target;
	}

	public void setTarget(Target target) {
		this.target = target;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "Patient [patientId=" + patientId + ", mrn=" + mrn + ", dob="
				+ dob + ", gender=" + gender + ", state=" + state + ", race="
				+ race + ", first_name=" + first_name + ", last_name="
				+ last_name + ", age=" + age + ", weight=" + weight
				+ ", height=" + height + ", isPregnant=" + isPregnant
				+ ", isDiabetic=" + isDiabetic + ", carbRatio=" + carbRatio
				+ ", takeOralMedic=" + takeOralMedic + ", useInsulin="
				+ useInsulin + ", dateFirstInsulin=" + dateFirstInsulin
				+ ", insulinCorrection=" + insulinCorrection
				+ ", deviceMacAddress=" + deviceMacAddress + ", diabetesType="
				+ diabetesType + ", insulinType=" + insulinType+ ", patientAppVersionNo="
				+ patientAppVersionNo
				+ "]";
	}

	public Boolean getIsSigned() {
		return isSigned;
	}

	public void setIsSigned(Boolean isSigned) {
		this.isSigned = isSigned;
	}

	public Long getLeadCoachId() {
		return leadCoachId;
	}

	public void setLeadCoachId(Long leadCoachId) {
		this.leadCoachId = leadCoachId;
	}

	public List<CoachDTO> getCoachList() {
		return coachList;
	}

	public void setCoachList(List<CoachDTO> coachList) {
		this.coachList = coachList;
	}

	public List<Feedback> getFeedback() {
		return feedback;
	}

	public void setFeedback(List<Feedback> feedback) {
		this.feedback = feedback;
	}

    public List<OneToOneSession> getOneToOneSessions() {
        return oneToOneSessions;
    }

    public void setOneToOneSessions(List<OneToOneSession> oneToOneSessions) {
        this.oneToOneSessions = oneToOneSessions;
    }

    public List<CoachNotes> getCoachNotes() {
        return coachNotes;
    }

    public void setCoachNotes(List<CoachNotes> coachNotes) {
        this.coachNotes = coachNotes;
    }

    public PatientDiabetesInfo getDiabetesInfo() {
        return diabetesInfo;
    }

    public void setDiabetesInfo(PatientDiabetesInfo diabetesInfo) {
        this.diabetesInfo = diabetesInfo;
    }

    public PatientDietaryInfo getDietaryInfo() {
        return dietaryInfo;
    }

    public void setDietaryInfo(PatientDietaryInfo dietaryInfo) {
        this.dietaryInfo = dietaryInfo;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public PatientCurriculum getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(PatientCurriculum curriculum) {
        this.curriculum = curriculum;
    }

    public Long getPrimaryFoodCoachId() {
        return primaryFoodCoachId;
    }

    public void setPrimaryFoodCoachId(Long primaryFoodCoachId) {
        this.primaryFoodCoachId = primaryFoodCoachId;
    }

	public String getPatientAppVersionNo() {
		return patientAppVersionNo;
	}

	public void setPatientAppVersionNo(String patientAppVersionNo) {
		this.patientAppVersionNo = patientAppVersionNo;
	}

	public List<MisfitActivityLog> getMisfitActivityLogs() {
		return misfitActivityLogs;
	}

	public void setMisfitActivityLogs(List<MisfitActivityLog> misfitActivityLogs) {
		this.misfitActivityLogs = misfitActivityLogs;
	}

    public List<DietaryNotes> getDietaryNotes() {
        return dietaryNotes;
    }

    public void setDietaryNotes(List<DietaryNotes> dietaryNotes) {
        this.dietaryNotes = dietaryNotes;
    }

    public Boolean getIsInvited() {
        return isInvited;
    }

    public void setIsInvited(Boolean isInvited) {
        this.isInvited = isInvited;
    }

    public Boolean getIsConsentAccepted() {
        return isConsentAccepted;
    }

    public void setIsConsentAccepted(Boolean isConsentAccepted) {
        this.isConsentAccepted = isConsentAccepted;
    }

    public Long getInvitationSentDate() {
        return invitationSentDate;
    }

    public void setInvitationSentDate(Long invitationSentDate) {
        this.invitationSentDate = invitationSentDate;
    }

    public Long getRequestReceivedDate() {
        return requestReceivedDate;
    }

    public void setRequestReceivedDate(Long requestReceivedDate) {
        this.requestReceivedDate = requestReceivedDate;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public Boolean getIsArchived() {
        return isArchived;
    }

    public void setIsArchived(Boolean isArchived) {
        this.isArchived = isArchived;
    }

	public Boolean getIsThreeDayEmailSent() {
		return isThreeDayEmailSent;
	}

	public void setIsThreeDayEmailSent(Boolean isThreeDayEmailSent) {
		this.isThreeDayEmailSent = isThreeDayEmailSent;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Long getPasswordExpiryTime() {
		return passwordExpiryTime;
	}

	public void setPasswordExpiryTime(Long passwordExpiryTime) {
		this.passwordExpiryTime = passwordExpiryTime;
	}

    public PatientMotivationImage getPatientMotivationImage() {
        return patientMotivationImage;
    }

    public void setPatientMotivationImage(PatientMotivationImage patientMotivationImage) {
        this.patientMotivationImage = patientMotivationImage;
    }

    public List<PatientMedication> getMedication() {
        return medication;
    }

    public void setMedication(List<PatientMedication> medication) {
        this.medication = medication;
    }

    public PatientSessionPreference getPatientSessionPreference() {
        return patientSessionPreference;
    }

    public void setPatientSessionPreference(PatientSessionPreference patientSessionPreference) {
        this.patientSessionPreference = patientSessionPreference;
    }

    public Boolean getIsPatientSessionPreferenceSet() {
        return isPatientSessionPreferenceSet;
    }

    public void setIsPatientSessionPreferenceSet(Boolean isPatientSessionPreferenceSet) {
        this.isPatientSessionPreferenceSet = isPatientSessionPreferenceSet;
    }

    public Boolean getIsEatingPreferenceSet() {
        return isEatingPreferenceSet;
    }

    public void setIsEatingPreferenceSet(Boolean isEatingPreferenceSet) {
        this.isEatingPreferenceSet = isEatingPreferenceSet;
    }

    public PatientEatingPreference getPatientEatingPreference() {
        return patientEatingPreference;
    }

    public void setPatientEatingPreference(PatientEatingPreference patientEatingPreference) {
        this.patientEatingPreference = patientEatingPreference;
    }

    public List<MemberKeyFacts> getKeyFacts() {
        return keyFacts;
    }

    public void setKeyFacts(List<MemberKeyFacts> keyFacts) {
        this.keyFacts = keyFacts;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

	public long getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(long facilityId) {
		this.facilityId = facilityId;
	}

	public String getMeterSerialNumber() {
		return meterSerialNumber;
	}

	public void setMeterSerialNumber(String meterSerialNumber) {
		this.meterSerialNumber = meterSerialNumber;
	}
}
