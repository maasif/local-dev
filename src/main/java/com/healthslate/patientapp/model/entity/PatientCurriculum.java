package com.healthslate.patientapp.model.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="patient_curriculum")
public class PatientCurriculum {

	@Id
	@GeneratedValue
	@Column(name="patient_curriculum_id")
	private Long patientCurriculumId;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="patient_id")
	private Patient patient;

    @Column(name="week")
    private Long week;

    @Column(name="progress")
    private Long progress;

    @Column(name="start")
    private String start;

    @Column(name="last_activity")
    private String lastActivity;

    public Long getPatientCurriculumId() {
        return patientCurriculumId;
    }

    public void setPatientCurriculumId(Long patientCurriculumId) {
        this.patientCurriculumId = patientCurriculumId;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Long getWeek() {
        return week;
    }

    public void setWeek(Long week) {
        this.week = week;
    }

    public Long getProgress() {
        return progress;
    }

    public void setProgress(Long progress) {
        this.progress = progress;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(String lastActivity) {
        this.lastActivity = lastActivity;
    }
}
