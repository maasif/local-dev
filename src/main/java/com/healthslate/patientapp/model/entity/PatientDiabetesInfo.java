package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

/**
 * Created by omursaleen on 3/3/2015.
 */

@Entity
@Table(name="patient_diabetes_info")
public class PatientDiabetesInfo {

    @Id
    @GeneratedValue
    @Column(name="diabetes_info_id")
    @Expose
    private Long diabetesInfoId;

    @Column(name="diagnosis")
    @Expose
    private String diagnosis;

    @Column(name="diagnosis_date")
    @Expose
    private String diagnosisDate;

    @Column(name="diabetes_support")
    @Expose
    private String diabetesSupport;

    @Column(name="treated_depression")
    @Expose
    private String treatedDepression;

    @Column(name="time_since_last_diabetes_edu")
    @Expose
    private String timeSinceLastDiabetesEdu;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    public Long getDiabetesInfoId() {
        return diabetesInfoId;
    }

    public void setDiabetesInfoId(Long diabetesInfoId) {
        this.diabetesInfoId = diabetesInfoId;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getDiagnosisDate() {
        return diagnosisDate;
    }

    public void setDiagnosisDate(String diagnosisDate) {
        this.diagnosisDate = diagnosisDate;
    }

    public String getDiabetesSupport() {
        return diabetesSupport;
    }

    public void setDiabetesSupport(String diabetesSupport) {
        this.diabetesSupport = diabetesSupport;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getTreatedDepression() {
        return treatedDepression;
    }

    public void setTreatedDepression(String treatedDepression) {
        this.treatedDepression = treatedDepression;
    }

    public String getTimeSinceLastDiabetesEdu() {
        return timeSinceLastDiabetesEdu;
    }

    public void setTimeSinceLastDiabetesEdu(String timeSinceLastDiabetesEdu) {
        this.timeSinceLastDiabetesEdu = timeSinceLastDiabetesEdu;
    }
}
