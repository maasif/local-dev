package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

/**
 * Created by omursaleen on 3/3/2015.
 */

@Entity
@Table(name="patient_dietary_info")
public class PatientDietaryInfo {

    @Id
    @GeneratedValue
    @Column(name="dietary_info_id")
    @Expose
    private Long dietaryInfoId;

    @Column(name="dietary_restrictions")
    @Expose
    private String dietaryRestrictions;

    @Column(name="meal_preparer")
    @Expose
    private String mealPreparer;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Column(name="dietary_restrictions_other")
    @Expose
    private String dietaryRestrictionsOther;

    public Long getDietaryInfoId() {
        return dietaryInfoId;
    }

    public void setDietaryInfoId(Long dietaryInfoId) {
        this.dietaryInfoId = dietaryInfoId;
    }

    public String getDietaryRestrictions() {
        return dietaryRestrictions;
    }

    public void setDietaryRestrictions(String dietaryRestrictions) {
        this.dietaryRestrictions = dietaryRestrictions;
    }

    public String getMealPreparer() {
        return mealPreparer;
    }

    public void setMealPreparer(String mealPreparer) {
        this.mealPreparer = mealPreparer;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getDietaryRestrictionsOther() {
        return dietaryRestrictionsOther;
    }

    public void setDietaryRestrictionsOther(String dietaryRestrictionsOther) {
        this.dietaryRestrictionsOther = dietaryRestrictionsOther;
    }
}
