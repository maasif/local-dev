package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name="patient_eating_preference")
public class PatientEatingPreference {

	@Id
	@GeneratedValue
	@Column(name="session_id")
	@Expose
	private Long sessionId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Expose
    @Column(name="restriction")
    private String restriction;

    @Column(name="is_cold_beverage")
    @Expose
    private boolean isColdBeverage;

    @Column(name="is_hot_beverage")
    @Expose
    private boolean isHotBeverage;

    @Column(name="is_alcohol")
    @Expose
    private boolean isAlcohol;

    @Column(name="cold_beverage_type")
    @Expose
    private String coldBeverageType;

    @Column(name="hot_beverage_teaspoon_added")
    @Expose
    private String hotBeverageTeaspoonAdded;

    @Column(name="hot_beverage_size")
    @Expose
    private String hotBeverageSize;

    @Column(name="alcohol_type")
    @Expose
    private String alcoholType;

    public String getRestriction() {
        return restriction;
    }

    public void setRestriction(String restriction) {
        this.restriction = restriction;
    }

    public boolean getIsColdBeverage() {
        return isColdBeverage;
    }

    public void setIsColdBeverage(boolean isColdBeverage) {
        this.isColdBeverage = isColdBeverage;
    }

    public boolean getIsHotBeverage() {
        return isHotBeverage;
    }

    public void setIsHotBeverage(boolean isHotBeverage) {
        this.isHotBeverage = isHotBeverage;
    }

    public boolean getIsAlcohol() {
        return isAlcohol;
    }

    public void setIsAlcohol(boolean isAlcohol) {
        this.isAlcohol = isAlcohol;
    }

    public String getColdBeverageType() {
        return coldBeverageType;
    }

    public void setColdBeverageType(String coldBeverageType) {
        this.coldBeverageType = coldBeverageType;
    }

    public String getHotBeverageTeaspoonAdded() {
        return hotBeverageTeaspoonAdded;
    }

    public void setHotBeverageTeaspoonAdded(String hotBeverageTeaspoonAdded) {
        this.hotBeverageTeaspoonAdded = hotBeverageTeaspoonAdded;
    }

    public String getHotBeverageSize() {
        return hotBeverageSize;
    }

    public void setHotBeverageSize(String hotBeverageSize) {
        this.hotBeverageSize = hotBeverageSize;
    }

    public String getAlcoholType() {
        return alcoholType;
    }

    public void setAlcoholType(String alcoholType) {
        this.alcoholType = alcoholType;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String toString() {
        return "PatientEatingPreference{" +
                "sessionId=" + sessionId +
                ", restriction='" + restriction + '\'' +
                ", isColdBeverage=" + isColdBeverage +
                ", isHotBeverage=" + isHotBeverage +
                ", isAlcohol=" + isAlcohol +
                ", coldBeverageType='" + coldBeverageType + '\'' +
                ", hotBeverageTeaspoonAdded='" + hotBeverageTeaspoonAdded + '\'' +
                ", hotBeverageSize='" + hotBeverageSize + '\'' +
                ", alcoholType='" + alcoholType + '\'' +
                '}';
    }
}
