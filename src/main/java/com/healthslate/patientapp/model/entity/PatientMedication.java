package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="patient_medication")
public class PatientMedication {

	@Id
	@GeneratedValue
	@Column(name="patient_medication_id")
    @Expose
	private Long patientMedicationId;

    @ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="patient_id")
	private Patient patient;

    @Expose
    @Column(name="medication_names")
    private String medicationsName;

	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="medication_master_id")
	private List<MedicationMaster> medicationMaster;

    @Expose
    @Column(name="dosage")
	private String dosage;

    @Expose
    @Column(name="timestamp")
    private Long timestamp;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "provider_id")
    private Provider provider;

    @Expose
    @Column(name="is_confirmed")
    private Boolean isConfirmed;

	public Long getPatientMedicationId() {
		return patientMedicationId;
	}

	public void setPatientMedicationId(Long patientMedicationId) {
		this.patientMedicationId = patientMedicationId;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public List<MedicationMaster> getMedicationMaster() {
		return medicationMaster;
	}

	public void setMedicationMaster(List<MedicationMaster> medicationMaster) {
		this.medicationMaster = medicationMaster;
	}

    public String getMedicationsName() {
        return medicationsName;
    }

    public void setMedicationsName(String medicationsName) {
        this.medicationsName = medicationsName;
    }

	@Override
	public String toString() {
		return "PatientMedication{" +
				"patientMedicationId=" + patientMedicationId +
				", medicationsName='" + medicationsName + '\'' +
				", medicationMaster=" + medicationMaster +
				", dosage='" + dosage + '\'' +
				'}';
	}

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Boolean getIsConfirmed() {
        return isConfirmed;
    }

    public void setIsConfirmed(Boolean isConfirmed) {
        this.isConfirmed = isConfirmed;
    }
}
