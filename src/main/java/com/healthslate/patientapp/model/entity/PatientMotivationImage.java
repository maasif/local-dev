package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name="patient_motivation_image")
public class PatientMotivationImage {

	@Id
	@GeneratedValue
	@Column(name="patient_motivation_id")
	private Long patientMotivationId;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="patient_id")
	private Patient patient;

    @Expose
    @Column(name="image_name")
    private String imageName;

    @Expose
    @Column(name="description")
    private String description;

    @Expose
    @Column(name="is_public")
    private Boolean isPublic;

    @Column(name="image_content", columnDefinition = "text")
    private String imageContent;

    public Long getPatientMotivationId() {
        return patientMotivationId;
    }

    public void setPatientMotivationId(Long patientMotivationId) {
        this.patientMotivationId = patientMotivationId;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageContent() {
        return imageContent;
    }

    public void setImageContent(String imageContent) {
        this.imageContent = imageContent;
    }

    public Boolean getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Boolean isPublic) {
        this.isPublic = isPublic;
    }

    @Override
    public String toString() {
        return "PatientMotivationImage{" +
                "patientMotivationId=" + patientMotivationId +
                ", imageName='" + imageName + '\'' +
                ", description='" + description + '\'' +
                ", isPublic=" + isPublic +
                ", imageContent='" + imageContent + '\'' +
                '}';
    }
}
