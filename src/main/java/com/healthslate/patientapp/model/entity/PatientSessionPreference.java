package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

@Entity
@Table(name="patient_session_preference")
public class PatientSessionPreference {

	@Id
	@GeneratedValue
	@Column(name="session_id")
	@Expose
	private Long sessionId;

	@Column(name="days")
	@Expose
	private String days;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @Column(name="times")
    @Expose
    private String times;

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    @Override
    public String toString() {
        return "PatientSessionPreference{" +
                "sessionId=" + sessionId +
                ", days='" + days + '\'' +
                ", times='" + times + '\'' +
                '}';
    }
}
