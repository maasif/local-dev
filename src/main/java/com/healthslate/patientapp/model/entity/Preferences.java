package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="atoz_preferences")
public class Preferences {
	@Id
	@GeneratedValue
	@Column(name="preference_id") 
	private long preferenceId;
	
	@Column(name="preference_name")
	private String preferenceName;
	
	@Column(name="preference_value")
	private String preferenceValue;

	public long getPreferenceId() {
		return preferenceId;
	}
	public void setPreferenceId(long preferenceId) {
		this.preferenceId = preferenceId;
	}

	public String getPreferenceName() {
		return preferenceName;
	}
	public void setPreferenceName(String preferenceName) {
		this.preferenceName = preferenceName;
	}

	public String getPreferenceValue() {
		return preferenceValue;
	}
	public void setPreferenceValue(String preferenceValue) {
		this.preferenceValue = preferenceValue;
	}
}