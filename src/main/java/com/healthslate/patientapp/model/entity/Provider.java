package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="provider")
public class Provider {

	@Id
	@GeneratedValue
	@Column(name="provider_id")
    @Expose
	private Long providerId;

	@OneToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User user;

	@Expose
	@Column(name="type")
	private String type;

	@Expose
	@Column(name="designation")
	private String designation;

	@Expose
	@Column(name="is_sms_enabled")
	private Boolean isSMSEnabled;

	@Expose
	@Column(name="is_email_enabled")
	private Boolean isEmailEnabled;

    @Expose
    @Column(name="is_deleted")
    private Boolean isDeleted;

    @Expose
    @Column(name="deleted_on")
    private Date deletedOn;

	@Expose
	@Column(name="image_path")
	private String imagePath;

    @Expose
    @Column(name="is_access_all")
    private Boolean isAccessAll;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="facility_id")
	private Facility facility;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Patient> patients;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "provider", fetch = FetchType.LAZY)
    private List<CoachNotes> coachNotes;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "provider", fetch = FetchType.LAZY)
    private List<DietaryNotes> dietaryNotes;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "provider", fetch = FetchType.LAZY)
    private List<MemberKeyFacts> keyFacts;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "provider", fetch = FetchType.LAZY)
    private List<OneToOneSession> oneToOneSessions;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "provider", fetch = FetchType.LAZY)
	private List<GroupPosts> groupPosts;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "provider", fetch = FetchType.LAZY)
	private CoachSessionPreference coachSessionPreference;

    @Expose
    @Column(name="sort_type")
    private Integer sortType;

    @Transient
    @Expose
    private String firstName;

    @Transient
    @Expose
    private String lastName;

    @Transient
    @Expose
    private String email;

    @Transient
    @Expose
    private String phone;

	@Expose
	@Column(name="is_default_ts")
	private Boolean isDefaultTS;

	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Facility getFacility() {
		return facility;
	}

	public void setFacility(Facility facility) {
		this.facility = facility;
	}

	public List<Patient> getPatients() {
		return patients;
	}

	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

    public Integer getSortType() {
        return sortType;
    }

    public void setSortType(Integer sortType) {
        this.sortType = sortType;
    }

	public Boolean getIsSMSEnabled() {
		return isSMSEnabled;
	}

	public void setIsSMSEnabled(Boolean isSMSEnabled) {
		this.isSMSEnabled = isSMSEnabled;
	}

	public Boolean getIsEmailEnabled() {
		return isEmailEnabled;
	}

	public void setIsEmailEnabled(Boolean isEmailEnabled) {
		this.isEmailEnabled = isEmailEnabled;
	}


    public List<CoachNotes> getCoachNotes() {
        return coachNotes;
    }

    public void setCoachNotes(List<CoachNotes> coachNotes) {
        this.coachNotes = coachNotes;
    }

    public List<OneToOneSession> getOneToOneSessions() {
        return oneToOneSessions;
    }

    public void setOneToOneSessions(List<OneToOneSession> oneToOneSessions) {
        this.oneToOneSessions = oneToOneSessions;
    }

	@Override
	public String toString() {
		return "Provider{" +
				"providerId=" + providerId +
				", type='" + type + '\'' +
				", designation='" + designation + '\'' +
				", isSMSEnabled=" + isSMSEnabled +
				", isEmailEnabled=" + isEmailEnabled +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", imagePath='" + imagePath + '\'' +
				", sortType=" + sortType +
				'}';
	}

    public List<DietaryNotes> getDietaryNotes() {
        return dietaryNotes;
    }

    public void setDietaryNotes(List<DietaryNotes> dietaryNotes) {
        this.dietaryNotes = dietaryNotes;
    }

    public List<MemberKeyFacts> getKeyFacts() {
        return keyFacts;
    }

    public void setKeyFacts(List<MemberKeyFacts> keyFacts) {
        this.keyFacts = keyFacts;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Boolean getIsAccessAll() {
        return isAccessAll;
    }

    public void setIsAccessAll(Boolean isAccessAll) {
        this.isAccessAll = isAccessAll;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

	public List<GroupPosts> getGroupPosts() {
		return groupPosts;
	}

	public void setGroupPosts(List<GroupPosts> groupPosts) {
		this.groupPosts = groupPosts;
	}

    public Boolean getIsDefaultTS() {
        return isDefaultTS;
    }

    public void setIsDefaultTS(Boolean isDefaultTS) {
        this.isDefaultTS = isDefaultTS;
    }

	public CoachSessionPreference getCoachSessionPreference() {
		return coachSessionPreference;
	}

	public void setCoachSessionPreference(CoachSessionPreference coachSessionPreference) {
		this.coachSessionPreference = coachSessionPreference;
	}
}
