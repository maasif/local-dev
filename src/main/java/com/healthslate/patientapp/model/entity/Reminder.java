package com.healthslate.patientapp.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="reminder")
public class Reminder {

    public static final String REMINDER_ID = "reminder_id";
    private static final String IS_MEAL_TIME = "is_meal_time";
    private static final String IS_GLUCOSE_TESTING = "is_glucose_testing";
    private static final String IS_WEIGHING_MYSELF = "is_weighing_myself";
    private static final String IS_GOAL_ACTION_PLAN = "is_goal_action_plan";
    private static final String IS_NEW_TOPIC_AVAILABLE = "is_new_topic_available";
    
    @Id
	@GeneratedValue
	@Column(name = REMINDER_ID)
    @Expose
    private Long reminderId;
    
    @Expose
    @Column(name = IS_MEAL_TIME)
    private Boolean isMealTime;

    @Expose
    @Column(name = IS_GLUCOSE_TESTING)
    private Boolean isGlucoseTesting;

    @Expose
    @Column(name = IS_WEIGHING_MYSELF)
    private Boolean isWeighingMyself;

    @Expose
    @Column(name = IS_GOAL_ACTION_PLAN)    
    private Boolean isGoalActionPlan;

    @Expose
    @Column(name = IS_NEW_TOPIC_AVAILABLE)    
    private Boolean isNewTopicAvailable;
    
    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name = Target.TARGET_ID)
	private Target target;
    
	public Long getReminderId() {
		return reminderId;
	}

	public void setReminderId(Long reminderId) {
		this.reminderId = reminderId;
	}

	public Boolean getIsMealTime() {
		return isMealTime;
	}

	public void setIsMealTime(Boolean isMealTime) {
		this.isMealTime = isMealTime;
	}

	public Boolean getIsGlucoseTesting() {
		return isGlucoseTesting;
	}

	public void setIsGlucoseTesting(Boolean isGlucoseTesting) {
		this.isGlucoseTesting = isGlucoseTesting;
	}

	public Boolean getIsWeighingMyself() {
		return isWeighingMyself;
	}

	public void setIsWeighingMyself(Boolean isWeighingMyself) {
		this.isWeighingMyself = isWeighingMyself;
	}

	public Boolean getIsGoalActionPlan() {
		return isGoalActionPlan;
	}

	public void setIsGoalActionPlan(Boolean isGoalActionPlan) {
		this.isGoalActionPlan = isGoalActionPlan;
	}

	public Boolean getIsNewTopicAvailable() {
		return isNewTopicAvailable;
	}

	public void setIsNewTopicAvailable(Boolean isNewTopicAvailable) {
		this.isNewTopicAvailable = isNewTopicAvailable;
	}

	public Target getTarget() {
		return target;
	}

	public void setTarget(Target target) {
		this.target = target;
	}
}
