package com.healthslate.patientapp.model.entity;

/**
 * Created by omursaleen on 8/4/2015.
 */
public class RequestedEmailServer {

    private String email;
    private String server;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public RequestedEmailServer(){}

    public RequestedEmailServer(String email, String server) {
        this.email = email;
        this.server = server;
    }
}
