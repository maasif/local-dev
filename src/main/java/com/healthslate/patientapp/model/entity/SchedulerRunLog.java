package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="scheduler_run_log")
public class SchedulerRunLog {

	@Id
	@GeneratedValue
	@Column(name="scheduler_run_id")
	@Expose
	private Long schedulerRunId;

	@Column(name="run_date")
	@Expose
	private Date runDate;

	@Column(name="scheduler_name")
	@Expose
	private String schedulerName;

    public String getSchedulerOccurrence() {
        return schedulerOccurrence;
    }

    public void setSchedulerOccurrence(String schedulerOccurrence) {
        this.schedulerOccurrence = schedulerOccurrence;
    }

    @Column(name="scheduler_occurrence")
    @Expose
    private String schedulerOccurrence;

    public SchedulerRunLog(){

    }

    public SchedulerRunLog(String schedulerName, String schedulerOccurrence) {
        this.schedulerName = schedulerName;
        this.schedulerOccurrence = schedulerOccurrence;
        this.runDate = new Date();
    }

    public Long getSchedulerRunId() {
        return schedulerRunId;
    }

    public void setSchedulerRunId(Long schedulerRunId) {
        this.schedulerRunId = schedulerRunId;
    }

    public Date getRunDate() {
        return runDate;
    }

    public void setRunDate(Date runDate) {
        this.runDate = runDate;
    }

    public String getSchedulerName() {
        return schedulerName;
    }

    public void setSchedulerName(String schedulerName) {
        this.schedulerName = schedulerName;
    }
}
