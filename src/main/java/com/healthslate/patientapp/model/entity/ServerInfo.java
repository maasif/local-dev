package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="server_info")
public class ServerInfo {

	@Expose
	@Id
	@GeneratedValue
	@Column(name="server_info_id")
	private Long serverInfoId;

	@Expose
	@Column(name="server_name")
	private String serverName;

    @Expose
	@Column(name="server_url")
	private String serverURL;

    @Expose
    @Column(name="is_active")
    private Boolean isActive;

    @Expose
    @Column(name="precedence")
    private Integer precedence;

    public Long getServerInfoId() {
        return serverInfoId;
    }

    public void setServerInfoId(Long serverInfoId) {
        this.serverInfoId = serverInfoId;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerURL() {
        return serverURL;
    }

    public void setServerURL(String serverURL) {
        this.serverURL = serverURL;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getPrecedence() {
        return precedence;
    }

    public void setPrecedence(Integer precedence) {
        this.precedence = precedence;
    }

    @Override
    public String toString() {
        return "ServerInfo{" +
                "serverInfoId=" + serverInfoId +
                ", serverName='" + serverName + '\'' +
                ", serverURL='" + serverURL + '\'' +
                ", isActive=" + isActive +
                ", precedence=" + precedence +
                '}';
    }
}
