package com.healthslate.patientapp.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="server_log")
public class ServerLog {

	@Id
	@GeneratedValue
	@Column(name="server_log_id")
	long serverLogId;
	
	@Column(name="logged_in_user_id")
	long loggedInUserId;
	
	@Column(name="role")
	String role;
	
	@Column(name="action_name")
	String actionName;
	
	@Column(name="description", columnDefinition="text")
	String description;
	
	@Column(name="log_time")
	Date logTime;

	public long getServerLogId() {
		return serverLogId;
	}

	public void setServerLogId(long serverLogId) {
		this.serverLogId = serverLogId;
	}

	public long getLoggedInUserId() {
		return loggedInUserId;
	}

	public void setLoggedInUserId(long loggedInUserId) {
		this.loggedInUserId = loggedInUserId;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getLogTime() {
		return logTime;
	}
	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
