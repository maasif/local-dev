package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="service_filter_log")
public class ServiceFilterLog {

	public static final String LOGGING_ID = "logging_id";
	public static final String IP_ADDRESS = "ip_address";
	public static final String SERVICE 	  = "service";
	public static final String TIMESTAMP  = "timestamp";
	public static final String STATUS     = "status";
	public static final String PATIENT_ID = "patient_id";
	public static final String VERSION_NO = "version_no";
	public static final String MAC_ADDRESS = "mac_address";
	
	@Id
	@GeneratedValue
	@Column(name = LOGGING_ID)
	long loggingId;
	
	@Column(name = IP_ADDRESS)
	String ipAddress;
	
	@Column(name = SERVICE)
	String service;
	
	@Column(name=TIMESTAMP)
	Long timestamp;
	
	@Column(name=STATUS)
	String status;

	@Column(name = PATIENT_ID)
	Long patientId;
	
	@Column(name = VERSION_NO) 
	String versionNo;

	@Column(name = MAC_ADDRESS)
	String macAddress;
	
	public long getLoggingId() {
		return loggingId;
	}

	public void setLoggingId(long loggingId) {
		this.loggingId = loggingId;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
}