package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.math.BigInteger;


@Entity
@Table(name = "share_meal_permissions")
public class ShareMealPermissions {

    public static final String ID = "id";
    public static final String PROVIDER_ID = "provider_id";
    public static final String PERMISSION_ASKED_DATE = "permission_asked_date";
    public static final String PERMISSION_GRANTED_DATE = "permission_granted_date";
    public static final String PERMISSIONS = "permissions";
    public static final String LOG_ID = "log_id";
    public static final String MEAL_SHARED_COUNT = "meal_shared_count";
    public static final String MEAL_SHARED_DATE = "meal_shared_date";
    public static final String PERMISSION_REQUEST_COUNT = "permission_request_count";
    public static final String MEAL_SHARED_BY = "meal_shared_by";
    public static final String MEAL_NOTES = "notes";

    @Id
    @GeneratedValue
    @Column(name = ID)
    @Expose
    private Long shareMealPermissionsId;

    @Expose
    @Column(name = PROVIDER_ID)
    private Long providerId;

    @Expose
    @Column(name = PERMISSION_ASKED_DATE)
    private Long permissionsAskedDate;

    @Expose
    @Column(name = PERMISSION_GRANTED_DATE)
    private Long permissionsGrantedDate;

    @Expose
    @Column(name = PERMISSIONS)
    private String permissions;

    @Expose
    @Column(name = LOG_ID)
    private String logId;

    @Expose
    @Column(name = MEAL_SHARED_COUNT)
    private Integer mealSharedCount;

    @Expose
    @Column(name = MEAL_SHARED_DATE)
    private Long mealSharedDate;

    @Expose
    @Column(name = PERMISSION_REQUEST_COUNT)
    private Integer requestSentCount;

    @Expose
    @Column(name = MEAL_SHARED_BY)
    private Long mealSharedBy;

    @Expose
    @Column(name = MEAL_NOTES, columnDefinition = "text")
    private String notes;

    public ShareMealPermissions() {

    }

    public ShareMealPermissions(Long providerId, Long permissionsAskedDate, Long permissionsGrantedDate, String permissions, String logId) {
        this.providerId = providerId;
        this.permissionsAskedDate = permissionsAskedDate;
        this.permissionsGrantedDate = permissionsGrantedDate;
        this.permissions = permissions;
        this.logId = logId;
    }

    public Long getShareMealPermissionsId() {
        return shareMealPermissionsId;
    }

    public void setShareMealPermissionsId(Long shareMealPermissionsId) {
        this.shareMealPermissionsId = shareMealPermissionsId;
    }

    public Long getProviderId() {
        return providerId;
    }

    public void setProviderId(Long providerId) {
        this.providerId = providerId;
    }

    public Long getPermissionsAskedDate() {
        return permissionsAskedDate;
    }

    public void setPermissionsAskedDate(Long permissionsAskedDate) {
        this.permissionsAskedDate = permissionsAskedDate;
    }

    public Long getPermissionsGrantedDate() {
        return permissionsGrantedDate;
    }

    public void setPermissionsGrantedDate(Long permissionsGrantedDate) {
        this.permissionsGrantedDate = permissionsGrantedDate;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public Integer getMealSharedCount() {
        return mealSharedCount;
    }

    public void setMealSharedCount(Integer mealSharedCount) {
        this.mealSharedCount = mealSharedCount;
    }

    public Long getMealSharedDate() {
        return mealSharedDate;
    }

    public void setMealSharedDate(Long mealSharedDate) {
        this.mealSharedDate = mealSharedDate;
    }

    public Integer getRequestSentCount() {
        return requestSentCount;
    }

    public void setRequestSentCount(Integer requestSentCount) {
        this.requestSentCount = requestSentCount;
    }

    public Long getMealSharedBy() {
        return mealSharedBy;
    }

    public void setMealSharedBy(Long mealSharedBy) {
        this.mealSharedBy = mealSharedBy;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
