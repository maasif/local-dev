package com.healthslate.patientapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sms_reply")
public class SmsReply {

	@Id
	@GeneratedValue
	@Column(name="sms_reply_id")
	private Long smsReplyId;
	
	@Column(name="patient_id")
	private Long patientId;
	
	@Column(name="message_body")
	private String messageBody;
	
	@Column(name="replied_time")
	private Long repliedTime;
	
	@Column(name="sent_time")
	private Long sentTime;
	
	@Column(name="from_phone_number")
	private String fromPhoneNumber;

	@Column(name="is_took_med")
	private Boolean isTookMed;
	
	@Column(name="message_reply_body")
	private String messageReplyBody;
	
	public long getSmsReplyId() {
		return smsReplyId;
	}

	public void setSmsReplyId(long smsReplyId) {
		this.smsReplyId = smsReplyId;
	}

	public String getMessageBody() {
		return messageBody;
	}

	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	public Long getRepliedTime() {
		return repliedTime;
	}

	public void setRepliedTime(Long repliedTime) {
		this.repliedTime = repliedTime;
	}

	public String getFromPhoneNumber() {
		return fromPhoneNumber;
	}

	public void setFromPhoneNumber(String fromPhoneNumber) {
		this.fromPhoneNumber = fromPhoneNumber;
	}

	public Boolean getIsTookMed() {
		return isTookMed;
	}

	public void setIsTookMed(Boolean isTookMed) {
		this.isTookMed = isTookMed;
	}

	public String getMessageReplyBody() {
		return messageReplyBody;
	}

	public void setMessageReplyBody(String messageReplyBody) {
		this.messageReplyBody = messageReplyBody;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public Long getSentTime() {
		return sentTime;
	}

	public void setSentTime(Long sentTime) {
		this.sentTime = sentTime;
	}

	@Override
	public String toString() {
		return "SmsReply{" +
				"smsReplyId=" + smsReplyId +
				", patientId=" + patientId +
				", messageBody='" + messageBody + '\'' +
				", repliedTime=" + repliedTime +
				", sentTime=" + sentTime +
				", fromPhoneNumber='" + fromPhoneNumber + '\'' +
				", isTookMed=" + isTookMed +
				", messageReplyBody='" + messageReplyBody + '\'' +
				'}';
	}
}
