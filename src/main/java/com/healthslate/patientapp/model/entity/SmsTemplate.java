package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

import javax.persistence.*;

/*
* ======= FILE CHANGE HISTORY =======
* [2/5/2015]: Created by __oz
* ===================================
 */

@Entity
@Table(name="sms_template")
public class SmsTemplate {

	@Expose
	@Id
	@GeneratedValue
	@Column(name="sms_template_id")
	private Long smsTemplateId;

	@Expose
	@Column(name="template")
	private String template;

	@Expose
	@Column(name="template_hyperlink")
	private String templateHyperlink;

	@Expose
	@Column(name="flag")
	private String flag;

	public Long getSmsTemplateId() {
		return smsTemplateId;
	}

	public void setSmsTemplateId(Long smsTemplateId) {
		this.smsTemplateId = smsTemplateId;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getTemplateHyperlink() {
		return templateHyperlink;
	}

	public void setTemplateHyperlink(String templateHyperlink) {
		this.templateHyperlink = templateHyperlink;
	}
}
