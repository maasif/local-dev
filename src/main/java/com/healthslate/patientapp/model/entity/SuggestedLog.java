package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="suggested_log")
public class SuggestedLog {

	@Id
	@Expose (serialize = true, deserialize = true)
    @GeneratedValue
	@Column(name="suggested_log_id")
	private Long suggestedLogId;

    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @JoinColumn(name="provider_id")
    private Provider createdBy;

	@Expose (serialize = true, deserialize = true)
	@Column(name="created_on")
	private Long createdOn;

	@Column(name="last_modified")
	private Long lastModified;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="log_id")
    private Log log;

    @Expose (serialize = true, deserialize = true)
    @Column(name="is_drafted")
    private Boolean isDrafted;

    public long getSuggestedLogId() {
        return suggestedLogId;
    }

    public void setSuggestedLogId(long suggestedLogId) {
        this.suggestedLogId = suggestedLogId;
    }

    public Provider getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Provider createdBy) {
        this.createdBy = createdBy;
    }

    public Long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Long createdOn) {
        this.createdOn = createdOn;
    }

    public Long getLastModified() {
        return lastModified;
    }

    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    public Log getLog() {
        return log;
    }

    public void setLog(Log log) {
        this.log = log;
    }

    public Boolean getIsDrafted() {
        return isDrafted;
    }

    public void setIsDrafted(Boolean isDrafted) {
        this.isDrafted = isDrafted;
    }
}
