package com.healthslate.patientapp.model.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="targets")
public class Target {

    public static final String TARGET_ID = "target_id";
    public static final String WEIGHT_START = "starting_weight";
    public static final String WEIGHT_TARGET = "target_weight";
    public static final String WEIGHT_FREQUENCY = "weight_frequency";
    public static final String SOURCE = "target_source";
    public static final String GLUCOSE_MIN = "glucose_min";
    public static final String GLUCOSE_MAX = "glucose_max";
    public static final String GLUCOSE_MIN_2 = "glucose_min_2";
    public static final String GLUCOSE_MAX_2 = "glucose_max_2";
    public static final String ACTIVITY_STEPS = "activity_steps";

    @Id
	@GeneratedValue
	@Column(name = TARGET_ID)
    @Expose
    private Long targetId;
    
    @Expose
    @Column(name = WEIGHT_START)
    private Float startingWeight;

    @Expose
    @Column(name = WEIGHT_TARGET)
    private Float targetWeight;

    @Expose
    @Column(name = WEIGHT_FREQUENCY)
    private String weightFrequency;

    @Expose
    @Column(name = GLUCOSE_MIN)    
    private Integer glucoseMin;

    @Expose
    @Column(name = GLUCOSE_MAX)    
    private Integer glucoseMax;

    @Expose
    @Column(name = GLUCOSE_MIN_2)
    private Integer glucoseMin2;

    @Expose
    @Column(name = GLUCOSE_MAX_2)
    private Integer glucoseMax2;

    @Expose
    @Column(name = ACTIVITY_STEPS)
    private Integer activitySteps;
    
    @Expose
    @ManyToMany(cascade=CascadeType.ALL, mappedBy="target", fetch=FetchType.LAZY)
    private List<TargetMeal> mealTargets;

    @Expose
    @ManyToMany(cascade=CascadeType.ALL, mappedBy="target", fetch=FetchType.LAZY)
    private List<TargetGlucose> glucoseTargets;

    @Expose
    @ManyToMany(cascade=CascadeType.ALL, mappedBy="target", fetch=FetchType.LAZY)
    private List<TargetMedication> medTargets;
    
    @Expose
    @Column(name = SOURCE)  
    private String source;
    
    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name="patient_id")
	private Patient patient;

    @Expose
    @OneToOne(cascade=CascadeType.ALL, mappedBy="target", fetch=FetchType.LAZY)
	private Reminder reminder;
    
	public Long getTargetId() {
		return targetId;
	}

	public Float getStartingWeight() {
		return startingWeight;
	}

	public Float getTargetWeight() {
		return targetWeight;
	}

	public String getWeightFrequency() {
		return weightFrequency;
	}

	public Integer getGlucoseMin() {
		return glucoseMin;
	}

	public Integer getGlucoseMax() {
		return glucoseMax;
	}

	public Integer getGlucoseMin2() {
		return glucoseMin2;
	}

	public Integer getGlucoseMax2() {
		return glucoseMax2;
	}

	public Integer getActivitySteps() {
		return activitySteps;
	}

	public List<TargetMeal> getMealTargets() {
		return mealTargets;
	}

	public List<TargetGlucose> getGlucoseTargets() {
		return glucoseTargets;
	}

	public String getSource() {
		return source;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

	public void setStartingWeight(Float startingWeight) {
		this.startingWeight = startingWeight;
	}

	public void setTargetWeight(Float targetWeight) {
		this.targetWeight = targetWeight;
	}

	public void setWeightFrequency(String weightFrequency) {
		this.weightFrequency = weightFrequency;
	}

	public void setGlucoseMin(Integer glucoseMin) {
		this.glucoseMin = glucoseMin;
	}

	public void setGlucoseMax(Integer glucoseMax) {
		this.glucoseMax = glucoseMax;
	}

	public void setGlucoseMin2(Integer glucoseMin2) {
		this.glucoseMin2 = glucoseMin2;
	}

	public void setGlucoseMax2(Integer glucoseMax2) {
		this.glucoseMax2 = glucoseMax2;
	}

	public void setActivitySteps(Integer activitySteps) {
		this.activitySteps = activitySteps;
	}

	public void setMealTargets(List<TargetMeal> mealTargets) {
		this.mealTargets = mealTargets;
	}

	public void setGlucoseTargets(List<TargetGlucose> glucoseTargets) {
		this.glucoseTargets = glucoseTargets;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public List<TargetMedication> getMedTargets() {
		return medTargets;
	}

	public void setMedTargets(List<TargetMedication> medTargets) {
		this.medTargets = medTargets;
	}

	public Reminder getReminder() {
		return reminder;
	}

	public void setReminder(Reminder reminder) {
		this.reminder = reminder;
	}

	@Override
	public String toString() {
		return "Target [targetId=" + targetId + ", startingWeight="
				+ startingWeight + ", targetWeight=" + targetWeight
				+ ", weightFrequency=" + weightFrequency + ", glucoseMin="
				+ glucoseMin + ", glucoseMax=" + glucoseMax + ", glucoseMin2="
				+ glucoseMin2 + ", glucoseMax2=" + glucoseMax2
				+ ", activitySteps=" + activitySteps + ", source=" + source
				+ "]";
	}
}
