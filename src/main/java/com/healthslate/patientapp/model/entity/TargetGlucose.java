package com.healthslate.patientapp.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;


@Entity
@Table(name="target_glucose")
public class TargetGlucose {

    public static final String GLUCOSE_TARGET_ID = "glucose_target_id";
    public static final String TIME = "time";
    public static final String MINIMUM = "minimum";
    public static final String MAXIMUM = "maximum";
    public static final String DAYS = "days";
    public static final String FREQUENCY = "frequency";
    private static final String TARGET_ID = "target_id";
    
    @Id
	@GeneratedValue
	@Column(name = GLUCOSE_TARGET_ID)   
    @Expose
    private Long glucoseTargetId;

    @Expose
    @Column(name = TIME)
    private String time;

    @Expose
    @Column(name = MINIMUM)
    private Float minTarget;

    @Expose
    @Column(name = MAXIMUM)
    private Float maxTarget;
    
    @Expose
    @Column(name = FREQUENCY)
    private Integer frequency;

    @Expose
    @Column(name = DAYS)
    private String days;

    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name = TARGET_ID)
    private Target target;

    public long getGlucoseTargetId() {
        return glucoseTargetId;
    }

    public void setGlucoseTargetId(long glucoseTargetId) {
        this.glucoseTargetId = glucoseTargetId;
    }

    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public float getMinTarget() {
        return minTarget;
    }

    public void setMinTarget(float minTarget) {
        this.minTarget = minTarget;
    }

    public float getMaxTarget() {
        return maxTarget;
    }

    public void setMaxTarget(float maxTarget) {
        this.maxTarget = maxTarget;
    }

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }
}
