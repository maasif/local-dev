package com.healthslate.patientapp.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;


@Entity
@Table(name="target_meal")
public class TargetMeal {

    public static final String MEAL_TARGET_ID  = "meal_target_id";
    public static final String MEAL_TYPE  = "meal_type";
    public static final String MEAL_TIME = "meal_time";
    public static final String CARB_TARGET = "carb_target";
    private static final String TARGET_ID = "target_id";

    @Id
	@GeneratedValue
	@Column(name = MEAL_TARGET_ID)
    @Expose
    private Long mealTargetId;

    @Expose
    @Column(name = MEAL_TYPE)  
    private String mealType;

    @Expose
    @Column(name = MEAL_TIME)  
    private String mealTime;

    @Expose
    @Column(name = CARB_TARGET)  
    private Float carbTarget;
    
    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name = TARGET_ID)
    private Target target;
    
    public long getMealTargetId() {
        return mealTargetId;
    }

    public void setMealTargetId(long mealTargetId) {
        this.mealTargetId = mealTargetId;
    }

    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    public String getMealType() {
        return mealType;
    }

    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

    public String getMealTime() {
        return mealTime;
    }

    public void setMealTime(String mealTime) {
        this.mealTime = mealTime;
    }

    public float getCarbTarget() {
        return carbTarget;
    }

    public void setCarbTarget(float carbTarget) {
        this.carbTarget = carbTarget;
    }
}
