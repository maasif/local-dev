package com.healthslate.patientapp.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="target_medication")
public class TargetMedication {

    public static final String MEDS_TARGET_ID = "med_target_id";
    public static final String TIME = "time";
    public static final String FREQUENCY = "frequency";
    public static final String DAY = "day";
    public static final String SHOULD_REMINDED = "should_reminded";
    private static final String TARGET_ID = "target_id";
    
    @Id
   	@GeneratedValue
    @Column(name = MEDS_TARGET_ID)
    private Long medsTargetId;

    @Expose
    @Column(name = TIME)
    private String time;
    
    @Expose
    @Column(name = FREQUENCY)
    private String frequency;
    
    @Expose
    @Column(name = DAY)
    private String day;

    @Expose
    @Column(name = SHOULD_REMINDED)   
    private boolean shouldReminded;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = TARGET_ID)
    private Target target;

    public TargetMedication() {
    }

    public TargetMedication(String time, boolean shouldReminded) {
        this.time = time;
        this.shouldReminded = shouldReminded;
    }

    public long getMedsTargetId() {
        return medsTargetId;
    }

    public void setMedsTargetId(long medsTargetId) {
        this.medsTargetId = medsTargetId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isShouldReminded() {
        return shouldReminded;
    }

    public void setShouldReminded(boolean shouldReminded) {
        this.shouldReminded = shouldReminded;
    }

    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

	@Override
	public String toString() {
		return "TargetMedication [medsTargetId=" + medsTargetId + ", time="
				+ time + ", shouldReminded=" + shouldReminded + "]";
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}
}
