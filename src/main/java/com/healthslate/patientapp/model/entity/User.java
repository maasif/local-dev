package com.healthslate.patientapp.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.google.gson.annotations.Expose;

/*
* ======= FILE CHANGE HISTORY =======
* [1/29/2015]: Added logTimeOffset and offsetKey in User Table while login, __oz
* ===================================
 */

@Entity
@Table(name="users")
public class User {

	@Expose
	@Id
	@GeneratedValue
	@Column(name="user_id")
	private long userId;
	
	@Column(name="social_id")
	private int socialId;
	
	@Column(name="group_id")
	private int groupId;

	@Expose
	@Column(name="email")
	private String email;
	
	@Column(name="password")
	private String password;

	@Expose
	@Column(name="user_type")
	private String userType;
	
	@Expose
	@Column(name="gender")
	private String gender;
	
	@Expose
	@Column(name="registration_date")
	private Date registrationDate;
	
	@Column(name="is_enabled")
	private Boolean isEnabled;

	@Expose
	@Column(name="first_name")
	private String firstName;

	@Expose
	@Column(name="last_name")
	private String lastName;

    @Expose
    @Column(name="display_name")
    private String displayName;

	@Column(name="is_registration_completed")
	private Boolean isRegistrationCompleted;

	@Column(name="wrong_password_count")
	private Integer wrongPasswordCount;

	@Column(name="user_block_time")
	private Long userBlockTime;

	@Expose
	@Column(name="phone")
	private String phone;

	@Expose
	@OneToOne(optional = true, cascade=CascadeType.ALL, mappedBy="user")
	private Provider provider;
	
	@OneToOne(optional = true, cascade=CascadeType.ALL, mappedBy="user")
	private CareGiver careGiver;
	
	@OneToOne(optional = true, cascade=CascadeType.ALL, mappedBy="user")
	private Patient patient;
	
	@OneToMany(mappedBy="user")

	@LazyCollection(LazyCollectionOption.TRUE)
	private List<Message> messages;

	@Column(name="log_time_offset")
	private Long logTimeOffset;

	@Column(name="password_modified_time")
	private Long passwordModifiedTime;

	@Column(name="offset_key")
	private String offsetKey;

    @Column(name="zip_code")
    private String zipCode;

    @Column(name="address")
    private String address;

    @Column(name="city")
    private String city;

    @Column(name="state")
    private String state;

	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Boolean getIsEnabled() {
		return isEnabled;
	}
	public void setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String usertype) {
		this.userType = usertype;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Provider getProvider() {
		return provider;
	}
	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	public CareGiver getCareGiver() {
		return careGiver;
	}
	public void setCareGiver(CareGiver careGiver) {
		this.careGiver = careGiver;
	}
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	public List<Message> getMessages() {
		return messages;
	}
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	public int getSocialId() {
		return socialId;
	}
	public void setSocialId(int socialId) {
		this.socialId = socialId;
	}
	public int getGroupId() {
		return groupId;
	}
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	@Override
	public String toString() {
		return "User [userId=" + userId + ", socialId=" + socialId
				+ ", groupId=" + groupId + ", email=" + email + ", usertype="
				+ userType + ", registrationDate="
				+ registrationDate + ", isEnabled=" + isEnabled
				+ ", firstName=" + firstName + ", lastName=" + lastName
				+ ", phone=" + phone +"]";
	}

	public Long getLogTimeOffset() {
		return logTimeOffset;
	}

	public void setLogTimeOffset(Long logTimeOffset) {
		this.logTimeOffset = logTimeOffset;
	}

	public String getOffsetKey() {
		return offsetKey;
	}

	public void setOffsetKey(String offsetKey) {
		this.offsetKey = offsetKey;
	}

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

    public Boolean getIsRegistrationCompleted() {
		return isRegistrationCompleted;
	}

	public void setIsRegistrationCompleted(Boolean isRegistrationCompleted) {
		this.isRegistrationCompleted = isRegistrationCompleted;
	}

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

	public Long getPasswordModifiedTime() {
		return passwordModifiedTime;
	}

	public void setPasswordModifiedTime(Long passwordModifiedTime) {
		this.passwordModifiedTime = passwordModifiedTime;
	}

	public Integer getWrongPasswordCount() {
		return wrongPasswordCount;
	}

	public void setWrongPasswordCount(Integer wrongPasswordCount) {
		this.wrongPasswordCount = wrongPasswordCount;
	}

	public Long getUserBlockTime() {
		return userBlockTime;
	}

	public void setUserBlockTime(Long userBlockTime) {
		this.userBlockTime = userBlockTime;
	}

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
