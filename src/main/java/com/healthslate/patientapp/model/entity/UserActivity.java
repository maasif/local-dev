package com.healthslate.patientapp.model.entity;

import com.google.gson.annotations.Expose;

/**
 * Created by omursaleen on 2/25/2015.
 */
public class UserActivity {

    @Expose
    private long totalPosts = 0;
    @Expose
    private long comments = 0;
    @Expose
    private long sharedPost = 0;
    @Expose
    private long likes = 0;

    public long getTotalPosts() {
        return totalPosts;
    }

    public void setTotalPosts(long totalPosts) {
        this.totalPosts = totalPosts;
    }

    public long getComments() {
        return comments;
    }

    public void setComments(long comments) {
        this.comments = comments;
    }

    public long getSharedPost() {
        return sharedPost;
    }

    public void setSharedPost(long sharedPost) {
        this.sharedPost = sharedPost;
    }

    public long getLikes() {
        return likes;
    }

    public void setLikes(long likes) {
        this.likes = likes;
    }
}
