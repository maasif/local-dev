package com.healthslate.patientapp.model.entity;

/**
 * Created by aamjad on 3/17/2015.
 */
public class UserInfo {
    String macAddress;
    String versionNo;
    String ipAddress;
    String autherization;

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getAutherization() {
        return autherization;
    }

    public void setAutherization(String autherization) {
        this.autherization = autherization;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "macAddress='" + macAddress + '\'' +
                ", versionNo='" + versionNo + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", autherization='" + autherization + '\'' +
                '}';
    }
}