package com.healthslate.patientapp.schedulers;

import com.healthslate.patientapp.util.RemindersUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by omursaleen on 6/19/2015.
 */
public class GoalAssessmentReminderScheduleJob implements Job {

    public GoalAssessmentReminderScheduleJob(){}

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            RemindersUtil.sendMessagesForGoalAssessment();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
