package com.healthslate.patientapp.schedulers;

import com.healthslate.patientapp.util.RemindersUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by omursaleen on 6/19/2015.
 */
public class MedicationReminderScheduleJob implements Job {

    public MedicationReminderScheduleJob(){}

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            RemindersUtil.sendMedicationSmsReminders();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
