package com.healthslate.patientapp.schedulers;

import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.RemindersUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Created by omursaleen on 6/19/2015.
 */
public class OneToOneSessionTodayReminderScheduleJob implements Job {

    public OneToOneSessionTodayReminderScheduleJob(){}

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            RemindersUtil.sendOneToOneSessionReminders(AppConstants.PreferencesNames.FIRST_ONE_TO_ONE_SESSION.getValue(), "Every 10 Minutes");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
