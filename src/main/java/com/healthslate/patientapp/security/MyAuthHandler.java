package com.healthslate.patientapp.security;

import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.entity.AppAccessLog;
import com.healthslate.patientapp.model.entity.Facility;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.BrowserDetectUtil;
import com.healthslate.patientapp.util.DateUtils;
import com.healthslate.patientapp.util.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

public class MyAuthHandler implements AuthenticationSuccessHandler, AuthenticationFailureHandler {
    static final Logger LOG = LoggerFactory.getLogger(MyAuthHandler.class);
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    private BaseDAO baseDAO = new BaseDAO();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        handle(request, response, authentication);
        clearAuthenticationAttributes(request);
    }

    protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {

        SavedRequest savedRequest = new HttpSessionRequestCache().getRequest(request, response);
        String requestedURL = "";
        if(savedRequest != null){
            requestedURL = savedRequest.getRedirectUrl();
        }

        String targetUrl = determineTargetUrl(request, authentication, request.getSession(), requestedURL);

		// Save to cookie
		Cookie usernameCookie = new Cookie("username", authentication.getName());
		usernameCookie.setMaxAge(60*60*24*365); // Make the cookie last a year!
		response.addCookie(usernameCookie);

        if (response.isCommitted()) {
            return;
        }

        redirectStrategy.sendRedirect(request, response, targetUrl);
    }
 
    /** Builds the target URL according to the logic defined in the main class Javadoc. */
    protected String determineTargetUrl(HttpServletRequest request, Authentication authentication, HttpSession session, String requestedUrl) {
        
    	boolean isAdmin = false;
    	boolean isProvider = false;
        boolean isFacilityAdmin = false;
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

        for (GrantedAuthority grantedAuthority : authorities) {
        	if (grantedAuthority.getAuthority().equals(AppConstants.Roles.ROLE_ADMIN.name())) {
        		isAdmin = true;
    		} else if (grantedAuthority.getAuthority().equals(AppConstants.Roles.ROLE_PROVIDER.name())) {
    			isProvider = true;
    		} else if (grantedAuthority.getAuthority().equals(AppConstants.Roles.ROLE_FACILITY_ADMIN.name())) {
                isFacilityAdmin = true;
            }
         }
		User user = new UserDAO().getUserByUsername(authentication.getName());
        LOG.info("onAuthenticationSuccess: Logged in with user :"+user.getEmail());
        long passwordModifiedTime = ObjectUtils.nullSafeLong(user.getPasswordModifiedTime());
        if(DateUtils.getDifferenceInMonths(passwordModifiedTime, System.currentTimeMillis()) >= 2 && passwordModifiedTime > 0) {
            LOG.info("onAuthenticationSuccess: Redirecting to login for password change");
            return "/login.jsp?error=pwchange";
        }

        if(isBlocked(user)) {
            LOG.info("onAuthenticationSuccess: Redirecting to login, user is blocked");
            return "/login.jsp?error=blocked";
        }

        if( ( user.getWrongPasswordCount() != null && user.getWrongPasswordCount() > 0)
                || (user.getUserBlockTime() != null && user.getUserBlockTime() > 0) ){

            user.setWrongPasswordCount(0);
            user.setUserBlockTime(0l);
            baseDAO.saveSchedular(user);
        }

        AppAccessLog appAccessLog = new AppAccessLogDAO().getAppAccessLogByUserAsync(user.getUserId());
        if(appAccessLog == null){
            appAccessLog = new AppAccessLog();
        }

        appAccessLog.setUserId(user.getUserId());
        appAccessLog.setUserRole(user.getUserType());
        appAccessLog.setBrowserVersion(BrowserDetectUtil.detectBrowser(request));
        appAccessLog.setOperatingSystem(BrowserDetectUtil.detectOS(request));
        appAccessLog.setIpAddress(request.getRemoteAddr());
        appAccessLog.setAccessDate(new Date());
        baseDAO.saveAsync(appAccessLog);

		session.setAttribute(AppConstants.SessionKeys.USER.name(), user);
		if (isAdmin) {
            session.setAttribute(AppConstants.SessionKeys.ADMIN.name(), user);
            LOG.info("onAuthenticationSuccess: Redirecting admin");
            //redirect to dashboard
            if(requestedUrl.toLowerCase().contains("feedback.action")){
                LOG.info("onAuthenticationSuccess: Redirecting to feedback page");
                return requestedUrl;
            }
            return "/app/admin/viewDashBoard";
        } else if(isFacilityAdmin){
            session.setAttribute(AppConstants.SessionKeys.FACILITY_ADMIN.name(), user);
            Facility facility = user.getProvider().getFacility();
            if(facility != null){
                session.setAttribute(AppConstants.SessionKeys.FACILITY.name(), facility);
            }

            //redirect to dashboard
            if(requestedUrl.toLowerCase().contains("prospectivemembers.action")){
                LOG.info("onAuthenticationSuccess: Redirecting to prospective members");
                return requestedUrl;
            }

            LOG.info("onAuthenticationSuccess: Redirecting to facility admin dashboard");
            return "/app/facilityadmin/viewDashBoard";

        } else if (isProvider) {
			Provider provider = new ProviderDAO().getProviderByUserId(user.getUserId()); 
			session.setAttribute(AppConstants.SessionKeys.PROVIDER.name(), provider);

            //redirect to saved URL
            if(requestedUrl.toLowerCase().contains("dashboard.action?token")
                    || requestedUrl.toLowerCase().contains("fetchlogdetail.action?") ){
                return requestedUrl;
            }

            LOG.info("onAuthenticationSuccess: Redirecting to CoachDashboard");
			return "/app/educator/coachesDashboard";
        } else {
            throw new IllegalStateException();
        }
    }
 
    protected void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        try {
            String username = httpServletRequest.getParameter("j_username");
            User user = new UserDAO().getUserByUsername(username);
            if(user != null){
                int wrongPasswordCount = ObjectUtils.nullSafeInteger(user.getWrongPasswordCount());
                LOG.info("onAuthenticationFailure: Failed login attempts :"+user.getEmail()+", attempts count: "+wrongPasswordCount);
                if(wrongPasswordCount >= 5){
                    user.setWrongPasswordCount(0);
                    baseDAO.saveSchedular(user);
                    LOG.info("onAuthenticationFailure: Redirecting to login page");
                    redirectStrategy.sendRedirect(httpServletRequest, httpServletResponse, "/app/loginFailed.action?error=blocked");
                }else{
                    if(wrongPasswordCount >= 4){
                        user.setUserBlockTime(System.currentTimeMillis());
                    }
                    wrongPasswordCount++;
                    user.setWrongPasswordCount(wrongPasswordCount);
                    baseDAO.saveSchedular(user);
                    LOG.info("onAuthenticationFailure: Redirecting to login page");
                    redirectStrategy.sendRedirect(httpServletRequest, httpServletResponse, "/app/loginFailed.action?error=true");
                }
            }else{
                redirectStrategy.sendRedirect(httpServletRequest, httpServletResponse, "/app/loginFailed.action?error=true");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            redirectStrategy.sendRedirect(httpServletRequest, httpServletResponse, "/app/loginFailed.action?error=true");
        }
    }

    private boolean isBlocked(User user) {
        long[] timeArray = DateUtils.getElapsedTimes(ObjectUtils.nullSafeLong(user.getUserBlockTime()), System.currentTimeMillis());
        long days = Math.abs(timeArray[0]);
        long hours = Math.abs(timeArray[1]);
        long minutes = Math.abs(timeArray[2]);
        if(days <= 0 && hours <= 0 && minutes <= 10) {
            return true;
        }
        return false;
    }

    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }

    protected RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }
}