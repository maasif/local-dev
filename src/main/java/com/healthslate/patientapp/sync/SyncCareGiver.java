package com.healthslate.patientapp.sync;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="care_giver")
public class SyncCareGiver {

	@Id
	@GeneratedValue
	@Column(name="care_giver_id")
	private long careGiverId;
	
	@OneToOne
	@JoinColumn(name="user_id")
	private SyncUser user;
	
	@Column(name="relationship")
	private String relationship;
	
	@OneToOne
	@JoinColumn(name="patient_id")
	private SyncPatient patient;
	
	public long getCareGiverId() {
		return careGiverId;
	}

	public void setCareGiverId(long careGiverId) {
		this.careGiverId = careGiverId;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public SyncPatient getPatient() {
		return patient;
	}

	public void setPatient(SyncPatient patient) {
		this.patient = patient;
	}

	public SyncUser getUser() {
		return user;
	}

	public void setUser(SyncUser user) {
		this.user = user;
	}
}
