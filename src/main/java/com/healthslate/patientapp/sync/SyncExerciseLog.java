package com.healthslate.patientapp.sync;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="exercise_log")
public class SyncExerciseLog {

	@Id
	@GeneratedValue
	@Column(name="exercise_log_id")
	private Long exerciseLogId;
	
	private String name;
	
	@Expose (serialize = true, deserialize = true)
	private String type;
	
	@Expose (serialize = true, deserialize = true)
	private int sets;
	
	@Expose (serialize = true, deserialize = true)
	private float weight;
	
	@Expose (serialize = true, deserialize = true)
	private float minutesPerformed;
	
	private String description;

	@Expose (serialize = true, deserialize = true)
	private Long timestamp;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="log_id")
	private SyncLog log;

	public Long getExerciseLogId() {
		return exerciseLogId;
	}

	public void setExerciseLogId(Long exerciseLogId) {
		this.exerciseLogId = exerciseLogId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSets() {
		return sets;
	}

	public void setSets(int sets) {
		this.sets = sets;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public float getMinutesPerformed() {
		return minutesPerformed;
	}

	public void setMinutesPerformed(float minutesPerformed) {
		this.minutesPerformed = minutesPerformed;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public SyncLog getLog() {
		return log;
	}

	public void setLog(SyncLog log) {
		this.log = log;
	}

	
}
