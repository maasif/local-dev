package com.healthslate.patientapp.sync;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="food_log_detail")
public class SyncFoodLogDetail {

	@Id
	@GeneratedValue
	@Column(name="food_log_detail_id")
	private Long foodLogDetailId;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="food_log_summary_id")
	private SyncFoodLogSummary foodLogSummary;
	
	@Expose
	@Column(name="no_of_servings")
	private Float numberOfServings;
	
	@Expose (serialize = true, deserialize = false)
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="food_master_id")
	private SyncFoodMaster foodMaster;

	public SyncFoodLogSummary getFoodLogSummary() {
		return foodLogSummary;
	}

	public void setFoodLogSummary(SyncFoodLogSummary foodLogSummary) {
		this.foodLogSummary = foodLogSummary;
	}

	public Float getNumberOfServings() {
		return numberOfServings;
	}

	public void setNumberOfServings(Float numberOfServings) {
		this.numberOfServings = numberOfServings;
	}

	public SyncFoodMaster getFoodMaster() {
		return foodMaster;
	}

	public void setFoodMaster(SyncFoodMaster foodMaster) {
		this.foodMaster = foodMaster;
	}

	public Long getFoodLogDetailId() {
		return foodLogDetailId;
	}

	public void setFoodLogDetailId(Long foodLogDetailId) {
		this.foodLogDetailId = foodLogDetailId;
	} 
}
