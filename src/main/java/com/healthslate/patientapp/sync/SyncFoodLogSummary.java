package com.healthslate.patientapp.sync;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.OrderBy;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="food_log_summary")
public class SyncFoodLogSummary {

	@Id
	@GeneratedValue
	@Column(name="food_log_summary_id")
	private Long foodLogSummaryId;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="carbs")
	private Float carbs;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="calories")
	private Float calories;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="fats")
	private Float fats;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="protein")
	private Float protein;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="type")
	private String type;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="timestamp")
	private Long timestamp;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="notes")
	private String notes;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="has_image")
	private Boolean hasImage;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="has_audio")
	private Boolean hasAudio;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="has_missing_food")
	private Boolean hasMissingFood;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="post_id")
	private Integer postId;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="log_id")
	private SyncLog log;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="meal_name")
	private String mealName;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="guess_carbs")
	private String guessCarbs;
	
	@Expose (serialize = true, deserialize = false)
	@OneToMany(cascade=CascadeType.ALL, mappedBy="foodLogSummary", fetch = FetchType.EAGER)
	@LazyCollection(LazyCollectionOption.FALSE)
	private Set<SyncFoodLogDetail> foodLogDetails; 
	
	@Expose (serialize = true, deserialize = false)
	@OneToOne(optional=true, cascade=CascadeType.ALL, mappedBy="foodLogSummary", fetch = FetchType.EAGER)
	private SyncFoodImage foodImage;
	
	@Expose (serialize = true, deserialize = false)
	@OneToMany(cascade=CascadeType.ALL, mappedBy="foodLogSummary", fetch = FetchType.EAGER)
	@LazyCollection(LazyCollectionOption.FALSE)	
	private Set<SyncFoodAudio> foodAudios;

	public Long getFoodLogSummaryId() {
		return foodLogSummaryId;
	}

	public void setFoodLogSummaryId(Long foodLogSummaryId) {
		this.foodLogSummaryId = foodLogSummaryId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public SyncLog getLog() {
		return log;
	}

	public void setLog(SyncLog log) {
		this.log = log;
	}

	public Set<SyncFoodLogDetail> getFoodLogDetails() {
		return foodLogDetails;
	}

	public void setFoodLogDetails(Set<SyncFoodLogDetail> foodLogDetails) {
		this.foodLogDetails = foodLogDetails;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Float getCarbs() {
		return carbs;
	}

	public void setCarbs(Float carbs) {
		this.carbs = carbs;
	}

	public Float getCalories() {
		return calories;
	}

	public void setCalories(Float calories) {
		this.calories = calories;
	}

	public Float getFats() {
		return fats;
	}

	public void setFats(Float fats) {
		this.fats = fats;
	}

	public Float getProtein() {
		return protein;
	}

	public void setProtein(Float protein) {
		this.protein = protein;
	}

	public SyncFoodImage getFoodImage() {
		return foodImage;
	}

	public void setFoodImage(SyncFoodImage foodImage) {
		this.foodImage = foodImage;
	}

	public Boolean getHasImage() {
		return hasImage;
	}

	public void setHasImage(Boolean hasImage) {
		this.hasImage = hasImage;
	}

	public Set<SyncFoodAudio> getFoodAudios() {
		return foodAudios;
	}

	public void setFoodAudios(Set<SyncFoodAudio> foodAudios) {
		this.foodAudios = foodAudios;
	}

	public Boolean getHasAudio() {
		return hasAudio;
	}

	public void setHasAudio(Boolean hasAudio) {
		this.hasAudio = hasAudio;
	}

	public Boolean getHasMissingFood() {
		return hasMissingFood;
	}

	public void setHasMissingFood(Boolean hasMissingFood) {
		this.hasMissingFood = hasMissingFood;
	}

	public Integer getPostId() {
		return postId;
	}

	public void setPostId(Integer postId) {
		this.postId = postId;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public String getGuessCarbs() {
		return guessCarbs;
	}

	public void setGuessCarbs(String guessCarbs) {
		this.guessCarbs = guessCarbs;
	}
	
	
}
