package com.healthslate.patientapp.sync;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="glucose_log")
public class SyncGlucoseLog {

	@Id
	@GeneratedValue
	@Column(name="glucose_log_id")
	private Long glucoseLogId;
	
	@Expose (serialize = true, deserialize = true)
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="glucose_log_type_id")
	private SyncGlucoseLogType glucoseLogType;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name="log_id")
	private SyncLog log;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="glucose_level")
	private String glucoseLevel;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="timestamp")
	private Long timestamp;

	@Expose (serialize = true, deserialize = true)
	@Column(name="glucose_time")
	private String glucoseTime;

    @Expose (serialize = true, deserialize = true)
    @Column(name="meter_serial_number")
    private String meterSerialNumber;

	public Long getGlucoseLogId() {
		return glucoseLogId;
	}

	public void setGlucoseLogId(Long glucoseLogId) {
		this.glucoseLogId = glucoseLogId;
	}

	public SyncGlucoseLogType getGlucoseLogType() {
		return glucoseLogType;
	}

	public void setGlucoseLogType(SyncGlucoseLogType glucoseLogType) {
		this.glucoseLogType = glucoseLogType;
	}

	public SyncLog getLog() {
		return log;
	}

	public void setLog(SyncLog log) {
		this.log = log;
	}

	public String getGlucoseLevel() {
		return glucoseLevel;
	}

	public void setGlucoseLevel(String glucoseLevel) {
		this.glucoseLevel = glucoseLevel;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getGlucoseTime() {
		return glucoseTime;
	}

	public void setGlucoseTime(String glucoseTime) {
		this.glucoseTime = glucoseTime;
	}

    public String getMeterSerialNumber() {
        return meterSerialNumber;
    }

    public void setMeterSerialNumber(String meterSerialNumber) {
        this.meterSerialNumber = meterSerialNumber;
    }
}
