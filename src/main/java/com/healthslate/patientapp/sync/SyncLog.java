package com.healthslate.patientapp.sync;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.healthslate.patientapp.model.entity.ActivityLog;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="log")
public class SyncLog {

	@Id
	@Expose (serialize = true, deserialize = true)
	@Column(name="log_id")
	private String logId;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="patient_id")
	private SyncPatient patient;
	
	@Column(name="log_source")
	private String logSource;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="device_type")
	private String deviceType;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="device_mac_address")
	private String deviceMacAddress;

	@Expose (serialize = true, deserialize = true)
	@Column(name="is_suggested")
	private Boolean isSuggested;

	@Expose (serialize = true, deserialize = true)
	@Column(name="is_favorite")
	private Boolean isFavorite;

	@Expose (serialize = true, deserialize = true)
	@Column(name="created_on")
	private Long createdOn;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="log_time")
	private Long logTime;
	
	@Column(name="last_modified")
	private Long lastModified;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="sync_time")
	private Long syncTime;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="sync_status")
	private String syncStatus;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="is_processed")
	private Boolean isProcessed;
	
	@Column(name="is_archive")
	private Boolean isArchive;
	
	@Column(name="is_removed")
	private Boolean isRemoved;
	
	@Column(name="process_date")
	private Date processDate;
	
	@Column(name="remove_date")
	private Date removeDate;
	
	@Column(name="log_time_offset")
	private Long logTimeOffset;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="log_type")
	private String logType;
	
	@Expose (serialize = true, deserialize = false)
	@Column(name = "log_processed_time")
	private Long logProcessedTime;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="is_share_with_group")
	private Boolean isShareWithGroup;
	
	@Expose (serialize = true, deserialize = true)
	@ManyToMany(cascade=CascadeType.ALL, mappedBy="log", fetch=FetchType.EAGER)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<SyncLogMood> logMoodList;
	
	@Expose (serialize = true, deserialize = true)
	@OneToOne(optional=true, cascade=CascadeType.ALL, mappedBy="log", fetch = FetchType.EAGER)
	private SyncGlucoseLog glucoseLog;
	
	@Expose (serialize = true, deserialize = true)
	@OneToMany(cascade=CascadeType.ALL, mappedBy="log", fetch=FetchType.EAGER)
	@LazyCollection(LazyCollectionOption.FALSE)	
	private Set<SyncExerciseLog> exerciseLogs;

	@Expose (serialize = true, deserialize = true)
	@OneToOne(optional=true, cascade=CascadeType.ALL, mappedBy="log", fetch = FetchType.EAGER)
	private SyncActivityLog activityLog;
	
	@Expose (serialize = false, deserialize = true)
	@OneToMany(cascade=CascadeType.ALL, mappedBy="log", fetch=FetchType.EAGER)
	@LazyCollection(LazyCollectionOption.TRUE)
	private Set<SyncReportError> reportErrors;
	
	@Expose (serialize = true, deserialize = true)
	@OneToOne(optional=true, cascade=CascadeType.ALL, mappedBy="log", fetch = FetchType.EAGER)
	private SyncMedicationLog medicationLog;
	
	@Expose (serialize = true, deserialize = true)
	@OneToOne(optional=true, cascade=CascadeType.ALL, mappedBy="log", fetch = FetchType.EAGER)
	private SyncFoodLogSummary foodLogSummary;
	
	@Expose (serialize = true, deserialize = true)
	@OneToOne(optional=true, cascade=CascadeType.ALL, mappedBy="log", fetch = FetchType.EAGER)
	private SyncMiscLog miscLog;
	
	@Expose (serialize = false, deserialize = true)
	@OneToMany(cascade=CascadeType.ALL, mappedBy="log", fetch=FetchType.EAGER)
	@LazyCollection(LazyCollectionOption.TRUE)
	private Set<SyncNotes> notes;

	@Expose(serialize = true, deserialize = true)
	@Column(name="user_id")
	private Long userId;
	
	public String getLogId() {
		return logId;
	}

	public void setLogId(String logId) {
		this.logId = logId;
	}

	public SyncPatient getPatient() {
		return patient;
	}

	public void setPatient(SyncPatient patient) {
		this.patient = patient;
	}

	public String getLogSource() {
		return logSource;
	}

	public void setLogSource(String logSource) {
		this.logSource = logSource;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public Long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Long createdOn) {
		this.createdOn = createdOn;
	}

	public Long getLogTime() {
		return logTime;
	}

	public void setLogTime(Long logTime) {
		this.logTime = logTime;
	}

	public Long getLastModified() {
		return lastModified;
	}

	public void setLastModified(Long lastModified) {
		this.lastModified = lastModified;
	}

	public Long getSyncTime() {
		return syncTime;
	}

	public void setSyncTime(Long syncTime) {
		this.syncTime = syncTime;
	}

	public String getSyncStatus() {
		return syncStatus;
	}

	public void setSyncStatus(String syncStatus) {
		this.syncStatus = syncStatus;
	}

	@Fetch(value = FetchMode.SUBSELECT)
	public List<SyncLogMood> getLogMoodList() {
		return logMoodList;
	}
	public void setLogMoodList(List<SyncLogMood> syncLogMoods) {
		this.logMoodList = syncLogMoods;
	}

	public SyncGlucoseLog getGlucoseLog() {
		return glucoseLog;
	}

	public void setGlucoseLog(SyncGlucoseLog glucoseLog) {
		this.glucoseLog = glucoseLog;
	}
	
	public SyncFoodLogSummary getFoodLogSummary() {
		return foodLogSummary;
	}

	public void setFoodLogSummary(SyncFoodLogSummary foodLogSummary) {
		this.foodLogSummary = foodLogSummary;
	}

	public SyncMedicationLog getMedicationLog() {
		return medicationLog;
	}

	public void setMedicationLog(SyncMedicationLog medicationLog) {
		this.medicationLog = medicationLog;
	}

	public SyncMiscLog getMiscLog() {
		return miscLog;
	}

	public void setMiscLog(SyncMiscLog miscLog) {
		this.miscLog = miscLog;
	}

	public Boolean getIsProcessed() {
		return isProcessed;
	}

	public void setIsProcessed(Boolean isProcessed) {
		this.isProcessed = isProcessed;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public Set<SyncNotes> getNotes() {
		return notes;
	}

	public void setNotes(Set<SyncNotes> notes) {
		this.notes = notes;
	}

	public Set<SyncExerciseLog> getExerciseLogs() {
		return exerciseLogs;
	}

	 public void setExerciseLogs(Set<SyncExerciseLog> exerciseLogs) {
		this.exerciseLogs = exerciseLogs;
	}

	public Long getLogTimeOffset() {
		return logTimeOffset;
	}

	public void setLogTimeOffset(Long logTimeOffset) {
		this.logTimeOffset = logTimeOffset;
	}

	public String getDeviceMacAddress() {
		return deviceMacAddress;
	}
	public void setDeviceMacAddress(String deviceMacAddress) {
		this.deviceMacAddress = deviceMacAddress;
	}

	public Boolean getIsRemoved() {
		return isRemoved;
	}

	public void setIsRemoved(Boolean isRemoved) {
		this.isRemoved = isRemoved;
	}

	public Date getRemoveDate() {
		return removeDate;
	}

	public void setRemoveDate(Date removeDate) {
		this.removeDate = removeDate;
	}

	public Boolean getIsShareWithGroup() {
		return isShareWithGroup;
	}

	public void setIsShareWithGroup(Boolean isShareWithGroup) {
		this.isShareWithGroup = isShareWithGroup;
	}

	public Set<SyncReportError> getReportErrors() {
		return reportErrors;
	}

	public void setReportErrors(Set<SyncReportError> reportErrors) {
		this.reportErrors = reportErrors;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}

	public Boolean getIsArchive() {
		return isArchive;
	}

	public void setIsArchive(Boolean isArchive) {
		this.isArchive = isArchive;
	}

	public SyncActivityLog getActivityLog() {
		return activityLog;
	}

	public void setActivityLog(SyncActivityLog activityLog) {
		this.activityLog = activityLog;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Boolean getIsSuggested() {
		return isSuggested;
	}

	public void setIsSuggested(Boolean isSuggested) {
		this.isSuggested = isSuggested;
	}

	public Boolean getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(Boolean isFavorite) {
		this.isFavorite = isFavorite;
	}

	public Long getLogProcessedTime() {
		return logProcessedTime;
	}

	public void setLogProcessedTime(Long logProcessedTime) {
		this.logProcessedTime = logProcessedTime;
	}
}
