package com.healthslate.patientapp.sync;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="medication_log")
public class SyncMedicationLog {

	@Id
	@GeneratedValue
	@Column(name="medication_log_id")
	private Long medicationLogId;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name="log_id")
	private SyncLog log;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="took_meds")
	private Boolean tookMeds;
	
	private String quantityTaken;
	
	@Expose (serialize = true, deserialize = true)
	private Long timeStamp;

	public Long getMedicationLogId() {
		return medicationLogId;
	}

	public void setMedicationLogId(Long medicationLogId) {
		this.medicationLogId = medicationLogId;
	}

	public SyncLog getLog() {
		return log;
	}

	public void setLog(SyncLog log) {
		this.log = log;
	}

	public String getQuantityTaken() {
		return quantityTaken;
	}

	public void setQuantityTaken(String quantityTaken) {
		this.quantityTaken = quantityTaken;
	}

	public Long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Boolean getTookMeds() {
		return tookMeds;
	}

	public void setTookMeds(Boolean tookMeds) {
		this.tookMeds = tookMeds;
	}
}
