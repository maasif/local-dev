package com.healthslate.patientapp.sync;

import com.google.gson.annotations.Expose;
import com.healthslate.patientapp.model.entity.FoodLogSummary;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.User;

import javax.persistence.*;

/*
* ======= FILE CHANGE HISTORY =======
* [2/4/2015]: Added imagePath property  __oz
* ===================================
 */

@Entity
@Table(name="message")
public class SyncMessage {

	@Id
	@GeneratedValue
	@Column(name="message_id")
	private Long messageId;

	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private SyncUser user;

	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name="patient_id")
	private SyncPatient patient;

	@Expose
	@Column(name="owner")
	private String owner;

	@Expose
	@Column(name="message")
	private String message;

	@Expose
	@Column(name="timestamp")
	private Long timestamp;

	@Expose
	@Column(name="read_status")
	private Boolean readStatus;

	@Expose
	@Column(name="image_path")
	private String imagePath;

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public SyncUser getUser() {
		return user;
	}

	public void setUser(SyncUser user) {
		this.user = user;
	}

	public SyncPatient getPatient() {
		return patient;
	}

	public void setPatient(SyncPatient patient) {
		this.patient = patient;
	}

	public Boolean getReadStatus() {
		return readStatus;
	}

	public void setReadStatus(Boolean readStatus) {
		this.readStatus = readStatus;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@Override
	public String toString() {
		return "Message{" +
				"messageId=" + messageId +
				", user=" + user +
				", owner='" + owner + '\'' +
				", timestamp=" + timestamp +
				", readStatus=" + readStatus +
				", imagePath='" + imagePath + '\'' +
				'}';
	}
}
