package com.healthslate.patientapp.sync;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="misc_log")
public class SyncMiscLog {

	@Id
	@GeneratedValue
	@Column(name="misc_log_id")
	private long miscLogId;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name="log_id")
	private SyncLog log;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="sleep_hours")
	private float sleepHours;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="sleep_minuts")
	private float sleepMinuts;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="weight")
	private float weight;

	@Expose (serialize = true, deserialize = true)
	@Column(name="blood_pressure_systolic")
	private int bloodPressureSystolic;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="blood_pressure_diastolic")
	private int bloodPressureDiastolic;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="steps_per_day")
	private Integer stepsPerDay;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="intensity")
	private String intensity;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="took_meds")
	private Boolean tookMeds;
	
	@Column(name="timestamp")
	private Long timestamp;

	public long getMiscLogId() {
		return miscLogId;
	}

	public void setMiscLogId(long miscLogId) {
		this.miscLogId = miscLogId;
	}

	public SyncLog getLog() {
		return log;
	}

	public void setLog(SyncLog log) {
		this.log = log;
	}

	public float getSleepHours() {
		return sleepHours;
	}

	public void setSleepHours(float sleepHours) {
		this.sleepHours = sleepHours;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public int getBloodPressureSystolic() {
		return bloodPressureSystolic;
	}

	public void setBloodPressureSystolic(int bloodPressureSystolic) {
		this.bloodPressureSystolic = bloodPressureSystolic;
	}

	public int getBloodPressureDiastolic() {
		return bloodPressureDiastolic;
	}

	public void setBloodPressureDiastolic(int bloodPressureDiastolic) {
		this.bloodPressureDiastolic = bloodPressureDiastolic;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public float getSleepMinuts() {
		return sleepMinuts;
	}

	public void setSleepMinuts(float sleepMinuts) {
		this.sleepMinuts = sleepMinuts;
	}

	public Integer getStepsPerDay() {
		return stepsPerDay;
	}

	public void setStepsPerDay(Integer stepsPerDay) {
		this.stepsPerDay = stepsPerDay;
	}

	public String getIntensity() {
		return intensity;
	}

	public void setIntensity(String intensity) {
		this.intensity = intensity;
	}

	public Boolean getTookMeds() {
		return tookMeds;
	}

	public void setTookMeds(Boolean tookMeds) {
		this.tookMeds = tookMeds;
	}
}
