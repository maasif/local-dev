package com.healthslate.patientapp.sync;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="mood")
public class SyncMood {

	@Id
	@GeneratedValue
	@Expose (serialize = true, deserialize = true)
	@Column(name="mood_id")
	private Long moodId;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="description")
	private String description;
	
	@Expose (serialize = true, deserialize = true)
	@Column(name="file_name")
	private String fileName;
	
	
	@Expose (serialize = false, deserialize = true)
	@Column(name = "is_selected")
	private boolean isSelected;

	public Long getMoodId() {
		return moodId;
	}

	public void setMoodId(Long moodId) {
		this.moodId = moodId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
}
