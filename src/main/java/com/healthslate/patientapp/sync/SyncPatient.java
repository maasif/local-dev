package com.healthslate.patientapp.sync;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="patient")
public class SyncPatient {

	@Id
	@Expose
	@GeneratedValue
	@Column(name="patient_id")
	private Long patientId;
	
	@OneToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="user_id")
	private SyncUser user;
	
	@Expose
	@Column(name="mrn")
	private String mrn;
	
	@Expose
	@Column(name="dob")
	private Date dob;
	
	@Expose
	@Column(name="gender")
	private String gender;
	
	@Expose
	@Column(name="state")
	private String state;
	
	@Expose
	@Column(name="race")
	private String race;
	
	@Transient
	private String first_name;
	
	@Transient
	private String last_name;
	
	@Transient
	private String age;
	
	@Expose
	@Column(name="weight")
	private Float weight;
	
	@Expose
	@Column(name="height")
	private Float height;
	
	@Expose
	@Column(name="is_pregnant")
	private Boolean isPregnant;
	
	@Expose
	@Column(name="is_diabetic")
	private Boolean isDiabetic;
	
	@Expose
	@Column(name="carb_ratio")
	private Float carbRatio;
	
	@Expose
	@Column(name="take_oral_medic")
	private Boolean takeOralMedic;
	
	@Expose
	@Column(name="use_insulin")
	private Boolean useInsulin;
	
	@Expose
	@Column(name="date_first_insulin")
	private Date dateFirstInsulin;
	
	@Expose
	@Column(name="insulin_correction")
	private Float insulinCorrection;
	
	@Expose
	@Column(name="device_mac_address")
	private String deviceMacAddress;
	
//	@ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
//	@JoinColumn(name="cohort_id")
//	private SyncCohort cohort;
//	
//    @ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
////  @javax.persistence.OrderColumn(name="facility_id")
//    private List<SyncFacility> facilities;
//    
//	@ManyToMany(mappedBy = "patients", fetch = FetchType.LAZY)
////	@javax.persistence.OrderColumn(name="provider_id")
//	private List<SyncProvider> providers;
//	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="diabetes_type_id")
//	private SyncDiabetesType diabetesType;
//	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="insulin_type_id")
//	private SyncInsulinType insulinType;
//	
//	@OneToMany(cascade=CascadeType.ALL, mappedBy="patient", fetch = FetchType.LAZY)
//	private List<SyncMessage> messages;
	
//	@OneToMany(cascade = CascadeType.ALL, mappedBy="patient", fetch = FetchType.LAZY)
//	@Expose
//	private List<SyncFilledForm> filledForms;
//
//	public List<SyncFilledForm> getFilledForms() {
//		return filledForms;
//	}
//
//	public void setFilledForms(List<SyncFilledForm> filledForms) {
//		this.filledForms = filledForms;
//	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public SyncUser getUser() {
		return user;
	}

	public void setUser(SyncUser user) {
		this.user = user;
	}

	public String getMrn() {
		return mrn;
	}

	public void setMrn(String mrn) {
		this.mrn = mrn;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public Float getHeight() {
		return height;
	}

	public void setHeight(Float height) {
		this.height = height;
	}

	public Boolean getIsPregnant() {
		return isPregnant;
	}

	public void setIsPregnant(Boolean isPregnant) {
		this.isPregnant = isPregnant;
	}

	public Boolean getIsDiabetic() {
		return isDiabetic;
	}

	public void setIsDiabetic(Boolean isDiabetic) {
		this.isDiabetic = isDiabetic;
	}

	public Float getCarbRatio() {
		return carbRatio;
	}

	public void setCarbRatio(Float carbRatio) {
		this.carbRatio = carbRatio;
	}

	public Boolean getTakeOralMedic() {
		return takeOralMedic;
	}

	public void setTakeOralMedic(Boolean takeOralMedic) {
		this.takeOralMedic = takeOralMedic;
	}

	public Boolean getUseInsulin() {
		return useInsulin;
	}

	public void setUseInsulin(Boolean useInsulin) {
		this.useInsulin = useInsulin;
	}

	public Date getDateFirstInsulin() {
		return dateFirstInsulin;
	}

	public void setDateFirstInsulin(Date dateFirstInsulin) {
		this.dateFirstInsulin = dateFirstInsulin;
	}

	public Float getInsulinCorrection() {
		return insulinCorrection;
	}

	public void setInsulinCorrection(Float insulinCorrection) {
		this.insulinCorrection = insulinCorrection;
	}

	public String getDeviceMacAddress() {
		return deviceMacAddress;
	}

	public void setDeviceMacAddress(String deviceMacAddress) {
		this.deviceMacAddress = deviceMacAddress;
	}

//	public SyncCohort getCohort() {
//		return cohort;
//	}
//
//	public void setCohort(SyncCohort cohort) {
//		this.cohort = cohort;
//	}
//
//	public List<SyncFacility> getFacilities() {
//		return facilities;
//	}
//
//	public void setFacilities(List<SyncFacility> facilities) {
//		this.facilities = facilities;
//	}
//
//	public List<SyncProvider> getProviders() {
//		return providers;
//	}
//
//	public void setProviders(List<SyncProvider> providers) {
//		this.providers = providers;
//	}
//
//	public SyncDiabetesType getDiabetesType() {
//		return diabetesType;
//	}
//
//	public void setDiabetesType(SyncDiabetesType diabetesType) {
//		this.diabetesType = diabetesType;
//	}
//
//	public SyncInsulinType getInsulinType() {
//		return insulinType;
//	}
//
//	public void setInsulinType(SyncInsulinType insulinType) {
//		this.insulinType = insulinType;
//	}
//
//	public List<SyncMessage> getMessages() {
//		return messages;
//	}
//
//	public void setMessages(List<SyncMessage> messages) {
//		this.messages = messages;
//	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

/*	public List<ReportError> getReportErrors() {
		return reportErrors;
	}

	public void setReportErrors(List<ReportError> reportErrors) {
		this.reportErrors = reportErrors;
	}*/
	
}
