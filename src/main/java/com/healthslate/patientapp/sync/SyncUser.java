package com.healthslate.patientapp.sync;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="users")
public class SyncUser {

	@Id
	@GeneratedValue
	@Column(name="user_id")
	private long userId;
	
	@Column(name="social_id")
	private int socialId;
	
	@Column(name="group_id")
	private int groupId;
	
	@Column(name="email")
	private String email;
	
	@Column(name="password")
	private String password;
	
	@Column(name="user_type")
	private String usertype;
	
	@Column(name="registration_date")
	private Date registrationDate;
	
	@Column(name="is_enabled")
	private Boolean isEnabled;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="phone")
	private String phone;
	
//	@OneToOne(optional = true, cascade=CascadeType.ALL, mappedBy="user")
//	private SyncProvider provider;
	
	@OneToOne(optional = true, cascade=CascadeType.ALL, mappedBy="user")
	private SyncCareGiver careGiver;
	
	@OneToOne(optional = true, cascade=CascadeType.ALL, mappedBy="user")
	private SyncPatient patient;
	
//	@OneToMany(mappedBy="user")
//	@LazyCollection(LazyCollectionOption.FALSE)
//	private List<SyncMessage> messages;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Boolean getIsEnabled() {
		return isEnabled;
	}
	public void setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
//	public SyncProvider getProvider() {
//		return provider;
//	}
//	public void setProvider(SyncProvider provider) {
//		this.provider = provider;
//	}
	
	public SyncCareGiver getCareGiver() {
		return careGiver;
	}
	public void setCareGiver(SyncCareGiver careGiver) {
		this.careGiver = careGiver;
	}
	public SyncPatient getPatient() {
		return patient;
	}
	public void setPatient(SyncPatient patient) {
		this.patient = patient;
	}
	
//	public List<SyncMessage> getMessages() {
//		return messages;
//	}
//	public void setMessages(List<SyncMessage> messages) {
//		this.messages = messages;
//	}
	
	public int getSocialId() {
		return socialId;
	}
	public void setSocialId(int socialId) {
		this.socialId = socialId;
	}
	public int getGroupId() {
		return groupId;
	}
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
}
