package com.healthslate.patientapp.test;

import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.DateUtils;
import com.healthslate.patientapp.util.EmailUtils;
import com.healthslate.patientapp.util.HibernateUtil;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Account;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.*;

public class Main {
    public static long ONE_MINUTE_MILLIS = 1000 * 60 * 1;
	public static void main(String []args){
        try {
            sendEmail();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int testHNQueries() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.HOUR_OF_DAY , -4);
        List<Integer> count = new LinkedList<Integer>();

        try{

            Query query = getHBSession().createSQLQuery(  " SELECT COUNT(*) "
                            + " FROM log l "
                            + " WHERE l.patient_id IN ( SELECT patients_patient_id from provider_patient where providers_provider_id = 52)"
                            + " AND l.log_type = 'Meal' "
                            + " AND l.is_processed = 0 "
                            + " AND l.is_suggested = 0 "
                            + " AND l.is_removed = 0 "
														/*+ " AND l.log_time >= " + cal.getTimeInMillis()*/);

            count = query.list();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return  Integer.parseInt(count.get(0)+"");
    }

    public static Session getHBSession(){
        Transaction transaction = null;
        Session session = null;
        try{
            session = HibernateUtil.getCurrentSession();
            if(session.getTransaction() != null && session.getTransaction().isActive()){
                transaction = session.getTransaction();
            }else{
                transaction = session.beginTransaction();
            }

            session.clear();
            session.flush();
            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            transaction.rollback();
        }
        return session;
    }

    public static Map<String, String> sendSMS(String num, String body) {
        Map<String, String> returnMap = new LinkedHashMap<String, String>();
        returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

		/* Find your sid and token at twilio.com/user/account */
        boolean isSent = false;

        String accountSSID = "AC5ad136fca4caa9948decdd21430408b4";
        String authToken = "c41ed5cff0256c2443d95aa9ee4e9855";
        String twilioNumber = "+1 530-491-4746";

        TwilioRestClient client = new TwilioRestClient(accountSSID, authToken);
        Account account = client.getAccount();
        MessageFactory messageFactory = account.getMessageFactory();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("To", num)); // Replace with a valid phone number for your account.
        params.add(new BasicNameValuePair("From", twilioNumber));
        params.add(new BasicNameValuePair("Body", body));
        try {
            com.twilio.sdk.resource.instance.Message sms = messageFactory.create(params);
            returnMap.put(AppConstants.JsonConstants.DATA.name(), sms.getSid());
            returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
            isSent = true;
        } catch (TwilioRestException e) {
            e.printStackTrace();
            isSent = false;
        }
        System.out.println("SMS to: "+num+", SMS SENT? "+isSent);
        return returnMap;
    }

    public static void sendEmail() {

        String emailString = "osamammursleen@gmail.com";

        try {
            boolean isSent = EmailUtils.sendTextInEmail("This is test email",
                    emailString,
                    "healthslate@healthslate.com",
                    "AKIAIUZLHPKKMKLF2IVA",
                    "AoCf6NVfjwhvoIJJURgxKIqfXHI9iz8TmT+kQKPFrtVP",
                    "email-smtp.us-east-1.amazonaws.com",
                    "Test Email");

            System.out.println("Email sent?: " + isSent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
