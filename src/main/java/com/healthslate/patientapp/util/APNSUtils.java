package com.healthslate.patientapp.util;

import java.util.ArrayList;
import java.util.List;

import com.healthslate.patientapp.model.entity.Device;
import com.healthslate.patientapp.ws.LogServices;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

public class APNSUtils {

    static final Logger LOG = LoggerFactory.getLogger(APNSUtils.class);

	public static void sendMsg(List<Device> devices, String action, @Context HttpServletRequest httpRequest){
		
		List<String> devicesTokensStaging = new ArrayList<String>();
        List<String> devicesTokensProduction = new ArrayList<String>();

		try{

          for (Device device : devices){
                if(device.getDeviceType().equalsIgnoreCase(AppConstants.DeviceTypes.IOS.getValue())){
                    if(device.getDeviceBuildType().equalsIgnoreCase(AppConstants.DeviceBuildTypes.DEBUG.getValue())){
                        devicesTokensStaging.add(device.getRegistrationId());
                        LOG.info("DEBUG: Sending IOS Push notification, registrationId: " + device.getRegistrationId());
                    }else{
                        devicesTokensProduction.add(device.getRegistrationId());
                        LOG.info("RELEASE: Sending IOS Push notification, registrationId: " + device.getRegistrationId());
                    }
                }
            }

            //for debug devices sending push notification
			if(devicesTokensStaging.size() > 0) {
                String certPath = httpRequest.getServletContext().getRealPath("/WEB-INF/"+AppConstants.STAGING_DEBUG_CERTIFICATE);
                ApnsService service = APNS.newService()
                        .withCert(certPath, AppConstants.DEV_KEY_STORE_PASSWORD)
                        .withSandboxDestination()
                        .build();

                String payload = APNS.newPayload()
                        /*.sound(AppConstants.SOUND_NAME)*/
                        .alertBody(action)
                        /*.badge(1)*/
                        .build();

                LOG.info("DEBUG: Sending IOS Push notification, payload: " + payload);
                service.push(devicesTokensStaging, payload);
            }

            //for release devices sending push notification
            if(devicesTokensProduction.size() > 0) {
                String certPath = httpRequest.getServletContext().getRealPath("/WEB-INF/"+AppConstants.STAGING_RELEASE_CERTIFICATE);
                ApnsService service = APNS.newService()
                        .withCert(certPath, AppConstants.PROD_KEY_STORE_PASSWORD)
                        .withProductionDestination()
                        .build();

                String payload = APNS.newPayload()
                        .sound(AppConstants.SOUND_NAME)
                        .alertBody(action).build();

                LOG.info("RELEASE: Sending IOS Push notification, payload: " + payload);
                service.push(devicesTokensProduction, payload);
            }

		} catch(Exception ex){
			ex.printStackTrace();
		}
	}

    public static void AmazonPush(){

    }
}
