package com.healthslate.patientapp.util;

import java.io.*;
import java.util.Map;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.healthslate.patientapp.model.dao.PreferencesDAO;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.internal.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AmazonS3Util {

    static final Logger LOG = LoggerFactory.getLogger(AmazonS3Util.class);

    public static final String AMAZON_FOLDER_MOTIVATION = "motivationImages/";

	public static AmazonS3 getS3Client(Map<String, String> prefMap) {
		AWSCredentials credentials = new BasicAWSCredentials(prefMap.get(AppConstants.PreferencesNames.ADMIN_ACCESS_KEY.getValue()), prefMap.get(AppConstants.PreferencesNames.ADMIN_SECRET_KEY.getValue()));
		return new AmazonS3Client(credentials);
	}

    public static boolean uploadFileToS3(String fileName, String folderName, String imageContent){

        try {

            File tempFile = File.createTempFile(fileName, ".jpg");
            tempFile.deleteOnExit();
            byte[] btDataFile = new sun.misc.BASE64Decoder().decodeBuffer(imageContent);
            File file = new File(tempFile.getAbsolutePath());
            FileOutputStream osf = new FileOutputStream(file);
            osf.write(btDataFile);
            osf.flush();
            osf.close();
            file.deleteOnExit();

            PreferencesDAO preferencesDAO = new PreferencesDAO();
            Map<String, String> prefMap = preferencesDAO.map();

            AmazonS3 amazonS3 = getS3Client(prefMap);

            String bucketName = prefMap.get(AppConstants.PreferencesNames.CONTENT_BUCKET_NAME.getValue());
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, folderName + fileName, file);
            PutObjectResult result = amazonS3.putObject(putObjectRequest);

            CommonUtils.deleteFileFromDisc(tempFile.getAbsolutePath());
            CommonUtils.deleteFileFromDisc(file.getAbsolutePath());
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static InputStream getFileFromS3(String fileName, String folderName){

        try {

            PreferencesDAO preferencesDAO = new PreferencesDAO();
            Map<String, String> prefMap = preferencesDAO.map();

            AmazonS3 amazonS3 = getS3Client(prefMap);
            String bucketName = prefMap.get(AppConstants.PreferencesNames.CONTENT_BUCKET_NAME.getValue());
            GetObjectRequest request = new GetObjectRequest(bucketName, folderName + fileName);
            S3Object object = amazonS3.getObject(request);
            InputStream objectContent = object.getObjectContent();

            return objectContent;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getImageFileFromS3(String fileName, String folderName){

        try {

            InputStream stream = getFileFromS3(fileName, folderName);
            byte[] bytes = IOUtils.toByteArray(stream);
            String base64Encoded = new sun.misc.BASE64Encoder().encode(bytes);
            return base64Encoded;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static boolean deleteFileFromS3(String fileName, String folderName){

        try {

            PreferencesDAO preferencesDAO = new PreferencesDAO();
            Map<String, String> prefMap = preferencesDAO.map();
            AmazonS3 amazonS3 = getS3Client(prefMap);

            String bucketName = prefMap.get(AppConstants.PreferencesNames.CONTENT_BUCKET_NAME.getValue());
            amazonS3.deleteObject(bucketName, folderName + fileName);

            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
