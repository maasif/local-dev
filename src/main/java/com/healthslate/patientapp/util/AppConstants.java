package com.healthslate.patientapp.util;

import java.util.LinkedHashMap;
import java.util.Map;

public class AppConstants {
	
	public final String NUTRIONIX_APP_ID = "";
	public final String NUTRIONIX_APP_KEY= "";
	
	public static final String ELGG_WEB_SERVICE_PATH = "http://staging.healthslate.com:8080/elgg/services/api/rest/json/?method=";
	public static final String GET_SOCIAL_DATA = "getSocialData";
	public static final String POST_SOCIAL_DATA = "postSocialData";

	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String AUTHENTICATE_ACCOUNT = "authenticateAccount";
	public static final String ACCESS_DENIED = "Access Denied";
    public static final String ACCESS_NOT_FOUND = "Not Found";
	public static final String ACCESS_GRANTED = "Access Granted";
	public static final String AUTHENTICATION_NOT_REQ = "Authentication not required";
	public static final String GROUPS = "GROUPS";
	public static final String FACTS_KEY = "facts";
	public static final String TRUE ="true" ;
	public static final String FALSE ="false" ;
	public static final String LOG_DEBUG_FOLDER = "/debug/";
	public static final String LOG_IMAGE_FOLDER = "/logImages/";
	public static final String PROFILE_IMAGE_FOLDER = "/profileImages/";
	public static final String PROVIDER_IMAGE_FOLDER = "/providerImages/";
    public static final String MOTIVATION_IMAGE_FOLDER = "/motivationImages/";
	public static final String FILE_TOS = "/WEB-INF/tos.html";
	public static final String DEBUG_FILES = "/WEB-INF/debugFiles";
	public static final String IMAGE = "image";
	public static final String LOG_AUDIO_FOLDER = "/logAudios/";
    public static final String AUDIO = "audio";
	public static final String SUCCESS = "Success";
	public static final String ERROR = "Error";
	public static final int ADD_TO_FAVORITE_FLAG = 1;
	public static final Long MILLI_SECOND_IN_DAY = 24*60*60*1000L ;
	public static final String FAVORITE = "Favorite";
	public static final String SELECT = "--Select--";
	public static final String UNIT = "Unit";
	public static final String YES = "Yes";
	public static final String NO = "No";
	public static final String AJAX = "AJAX";
	public static final String AJAX_FOOD_MASTER = "ajaxfoodMaster";
	public static final String AJAX_USER_LIST = "userList";
	public static final String AJAX_QUESTION_LIST= "questionList" ;
	public static final String AJAX_MESSAGE = "ajaxError";
	public static final String AJAX_NEW_MESSAGES = "newMessages";
	public static final String AJAX_DYK_SEND = "dyksend";
	public static final String AJAX_SUCCESS = "AJAX_SUCCESS";
	public static final String AJAX_NUTRI_INFO = "nutriInfo";
	public static final String ALL = "All";
	public static final String NOT_PROCESSED = "Not Processed";
	public static final String PROCESSED = "Processed";
	public static final String ARCHIVED = "Archived";
	public static final String METHOD = "METHOD";
	public static final String VALUES = "VALUES";
	public static final String MESSAGES = "MESSAGES";
	public static final String FOOD_DETAILS = "FOOD_DETAILS";
	public static final String SUCCESS_DYK ="SUCCESS_DYK";
	public static final String LOG_TYPE_ACTIVITY = "Activity";
	public static final String LOG_TYPE_ACTIVITY_MINUTE = "ActivityMinute";
	public static final String LOG_TYPE_GLUCOSE = "Glucose";
	public static final String LOG_TYPE_OTHER = "Other";
	public static final String LOG_TYPE_MEAL = "Meal";
	public static final String GLUCOSE_BEFORE_MEAL = "Before Meal";
	public static final String GLUCOSE_AFTER_MEAL = "After Meal";
	//public static final String SERVER_ERROR = "Server error";
	public static final String UBABLE_TO_SIGNUP = "Unable to signup. Please contact customer service at (888) 291-7245.";
	public static final String UBABLE_TO_SAVE_LOG = "Unable to save log. Please contact customer service at (888) 291-7245.";
	public static final String UBABLE_TO_CHANGE_PASSWORD = "Unable to change password. Please contact customer service at (888) 291-7245.";
	public static final String UBABLE_TO_VERIFY_PASSWORD = "Unable to verify password. Please contact customer service at (888) 291-7245.";
	public static final String UBABLE_TO_SEND_TOS = "Unable to send Terms of Services. Please contact customer service at (888) 291-7245.";
	public static final String UBABLE_TO_SAVE_FEEDBACK = "Unable to save feedback. Please contact customer service at (888) 291-7245.";
	public static final String UBABLE_TO_SEND_EMAIL = "Unable to send email. Please contact customer service at (888) 291-7245.";
	public static final String UBABLE_TO_FIND_COACH = "Unable to find coach. Please contact customer service at (888) 291-7245.";
	
	public static final String ELGG_WELCOME_MSG = "I'm your Lead Coach. I look forward to meeting you soon. Please let me know some good times for our first video call.";
	public static final String ELGG_CURRICULAM_MSG = "You can start the Topics for Week 1 as soon as you are ready. Just touch the bulb button in the top right corner of the app!";

	public static final String ELGG_MSG_COACH = "Touch <ic>header_support_white<ic> in the top right corner to contact your Coaches or Tech Support.";
	public static final String ELGG_MSG_ADD_LOG = "Touch <ic>header_add_white<ic> in the top right corner to log Meals, Glucose, Activity, etc.";
	public static final String ELGG_MSG_TOPICS = "Touch <ic>header_topic_white<ic> in the top right for each week's educational videos.";
	public static final String ELGG_MSG_MENU = "Touch <ic>header_menu_white<ic> in the top left corner to access Menu, which you can use to do anything with this app.";

	public static final String NON_RATED_SELF_ASSESSMENT_MSG = "Your HealthSlate Coach would like to check in on how you are doing with the Goal you recently set. You can rate how you're doing, tap http://healthslate.com/goals and then choose the HealthSlate App.";

	//  ************ new code ******************
	public static final String LOG_TYPE_EXERCISE = "Exercise";
	public static final String LOG_TYPE_MEDICATION = "Medication";
	public static final String LOG_TYPE_WEIGHT = "Weight";
	public static final String LOG_TYPE_BP = "Blood Pressure";	
	public static final String LOG_TYPE_MISC = "Misc";
	// ********** end of new code *******************
	
	public static final String NOT_APPLICABLE = "N/A";
	public static final String NOT_AVAILABLE = "-";

	public static final String MSG = "MSG";
	public static final String WELCOME_MSG = "Welcome to HealthSlate";
	public static final String CURRICULUM = "CURRICULUM";
	
	public static final String SP_REGISTRATION_ID = "SP_REGISTRATION_ID";

	public static final String MALE = "Male";
	public static final String FEMALE = "Female";
	public static final String LOG = "LOG";
	public static final String PATIENTS = "PATIENTS";
	public static final String PATIENT = "PATIENT";
    public static final String PROVIDER = "PROVIDER";
	public static final String ADMIN = "ADMIN";
	public static final String NEW_LOG = "NEW_LOG";
	public static final String LOG_REPORT = "LOG_REPORT";
	public static final String LOG_BOOK = "LOG_BOOK";
	public static final String GLUCOSE_LOG_BOOK = "GLUCOSE_LOG_BOOK";
	public static final String PATIENT_POPULATION = "PATIENT_POPULATION";
	public static final String LOG_REPORT_DATA = "LOG_REPORT_DATA";
	public static final String GLUCOSE_LOG_REPORT_DATA = "GLUCOSE_LOG_REPORT_DATA";
	public static final String LOG_DETAIL = "LOG_DETAIL";
	public static final String REDIRECT = "REDIRECT";
    public static final String REDIRECT_SUGGESTED_MEAL = "REDIRECT_SUGGESTED_MEAL";
	public static final String REDIRECT_LOGS = "REDIRECT_LOGS";
	public static final String COMMENT_ADD = "COMMENT_ADD";
	public static final String MAX_TIME = "MAX_TIME";
	public static final String REPORT_ERROR = "REPORT_ERROR";
	public static final String MY_MEALS = "MY_MEALS";
	public static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static final String PATIENT_ID ="patientId" ;
	public static final String DESCRIPTION ="description";
	public static final String MONTH = "MONTH";
	public enum ServicesConstants {STATUS, REASON, SUCCESS, ERROR, DATA, NEW_LOG, NO_NEW_LOG, IMAGE, AUDIO, HEADER, REFRESH, COUNT, TARGETS, STARTING_WEIGHT, TARGET_WEIGHT, MINMAX, MAXYAXIS, TIME_STAMP, CHANGE_PASSWORD, PASSWORD_EXPIRY, TECH_SUPPORT_NUMBER, EATING_PREFERENCE, SESSION_PREFERENCE, SERVER_INFO}
	public enum SessionKeys {ADMIN, PROVIDER, USER, PATIENT, DATE_FROM, DATE_TO, ADV_SEARCH, EDUCATOR_MEMBER_ID, EDUCATOR_FACILITY_ID, EDUCATOR_AADE_ID, EDUCATOR_NAME, USER_ID, QUEUE_ID, REGISTRATION, FACILITY, ACTIVE_PATIENT, MODULE_TYPE, CONTACT_NUMBER, ENROLL_PROGRAM, FACILITY_ADMIN}
	public enum NutritionInfoKeys {CARBS, CALORIES, FATS, PROTEIN, FOOD_UNIT, SERVING_SIZE}
	public enum LogSyncStatus {SYNCHRONIZED, UNSYNCHRONIZED, SYNC_FAILED}
	public enum Roles {ROLE_ADMIN, ROLE_PROVIDER, ROLE_PATIENT, ROLE_FACILITY_ADMIN, ROLE_SUPER_ADMIN}
	public enum MultiPartKeys {LOGS, IMAGES, AUDIOS};	

	public static final String MEAL_TYPE_BREAKFAST = "Breakfast";
	public static final String MEAL_TYPE_LUNCH = "Lunch";
	public static final String MEAL_TYPE_SNACK = "Snack";
	public static final String MEAL_TYPE_DINNER = "Dinner";
	public static final String MEAL_TYPE_BEVERAGE = "Beverage";
	
	public static final String NEW_LINE = "\n";
	public static final String FILE_SAVE_LOG_REQUEST = "save_log_request.txt";

	public static final String ALL_MESSAGES = "ALL_MESSAGES";
	
	/*public enum Facility {
		ATOZ("atoz");
		String name;
		private Facility(String name) {
			this.name = name;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}
	*/
	
	public enum PROVIDERTYPE {
		COACH("Coach"),
		FOOD_COACH("Food Coach"),
		FACILITY_ADMIN("Facility Admin"),
		TECH_SUPPORT("Tech Support");
		String value;
		
		private PROVIDERTYPE(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
	
	public enum NEWSFEED {
		TITLE("title"),
		DESCRIPTION("description"),
		TAGS("tags"),
		POST_TYPE("postType"),
		POST_ACCESS("postAccess"),
		USERNAME("userName"),
		FILENAME("filename"),
		ENCODEDCONTENT("encodedContent"),
		DECODEDCONTENT("decodedContent"),
		FILETYPE("filetype"),
		TYPE("type"),
		GLUCOSE("GLUCOSE"),
		WEIGHT("WEIGHT"),
		FOODLOG("FOODLOG"),
		VIDEOURL("VIDEOURL"),
		UPCOMING_FOODLOG("UPCOMING_FOODLOG"),
		CARDIO("cardio"),
		STRENGTH("strength"),
		CAP_CARDIO("Cardio"),
		CAP_STRENGTH("Strength"),
		ACTIVITY_CARDIO("ACTIVITY_CARDIO"),
		ACTIVITY_STRENGTH("ACTIVITY_STRENGTH"),
		ACTIVITY("ACTIVITY"),
		MEDICATION("MEDICATION"),
		GROUP("group"),
		PRIVATE("private"),
		PUBLIC("public"),
		MEAL_NAME("mealName"),
		CARBS("carbs"),
		POST_ID("postId"),
		HAS_MISSING_FOOD("hasMissingFood"),
		HAS_SHARED_EVER("hasSharedEver"),
		CUSTOM_ID("customId"),
		GUESS_CARBS("guessCarbs"),
		MESSAGE("MESSAGE"),
		WELCOME("WELCOME"),
		TEXT("TEXT"),
		DYK("DYK"),
		POST_COMMENT("postComment"),
		OWNER_USERNAME("ownerUserName"),
		Q_CUSTOM_ID("qCustomId"),
		FILE_URL("fileUrl"),
		LOG_TIME("logTime"),
		COACH_MEAL("COACHMEAL"),
		OWNER_NAME("ownerName"),
		OWNER_AVATAR_URL("ownerAvatarUrl"),
		ACCESS_ID("accessId"),
		MIME_TYPE("mimeType"),
		NOTES("notes"),
		FILE_ICON("fileIcon"),
		_FILE_ICON("file_icon"),
		OTHER_DATA("otherData"),
		HYPERLINK("HYPERLINK"),
		LIST_OF_USERS("listOfUsers");

		String value;
		
		private NEWSFEED(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
		
	}



	public enum SOCIALKEYS {
		NAME("name"),
		USER_DISPLAY_NAME("userDisplayName"),
		TIME_STAMP("timestamp"),
		GROUP_ID("groupid"),
		GROUP_GUID("groupGuid"),
		EMAIL("email"),
		USERNAME("username"),
		USER("userName"),
		PASSWORD("password"),
		CREATE_USER("createUser"),
		UPLOAD_PROFILE_PIC("uploadProfilePic"),
		LIST_GROUPS("listGroups"),
		LIST_USER("groupMembersList"),
		GET_GROUPS_FULL_VIEW("getGroupsFullView"),
		GROUP_NAME("groupName"),
		MESSAGE_SENT("messagesSent"),
		MESSAGE_SEND("messageSend"),
		STATUS("status"),
		SUCCESS("success"),
		MESSAGE("message"),
		RESULT("result"),
		GUID("guid"),
		GROUP_JOIN("groupJoin"),
		GROUPS("GROUPS"),
		DESCRIPTION("description"),
		MEMBERS("members"),
		LIKES("likes"),
		AVATAR_URL("avatar_url"),
		OWNER_GUID("owner_guid"),
		CREATE_GROUP("createGroup"),
		UPLOADPOST("uploadPost"),
        CURRICULUM("curriculum"),
		INFO("INFO"),
		SEND_TO("sendTo"),
		SEND_BY("sendBy"),
		UPDATEPOST("updatePost"),
        USER_ACTIVITY_COUNTS("userAcitivityCounts"),
        USER_ACTIVITY_COUNTS_BY_DATE("userAcitivityCountsByDate"),
		DELETEPOST("deletePostWithCustomId"),
		POST_BY_CUSTOM_ID("getPostByCustomId"),
		UPLOAD_POST_FOR_USERS("uploadPostForUsers"),
		DELETE_USER("deleteUser"),
		GET_DATA_BY_POST_ID("getPost"),
		POST_GUID("postGuid");

		String value;
		
		private SOCIALKEYS(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
		
	}
	
	public enum GCMKEYS {
		BROWSER_KEY("AIzaSyCbbZFkq8LMhwEilD2pIAaiWOFkPZ0-Bco"),
		DATA("data"),
		PROVIDER_NAME("providerName"),
		MESSAGE("message"),
		PAYLOAD("payload"),
		ACTIONS("actions"),
		QUESTION("question"),
		NUTRITIONAL_INFO("nutritionalInfo"),
		LOG_INFO("logInfo"),
		LOG_ID("logId"),
		TARGET("target"),
        COACHES("coaches"),
        SUGGESTED_MEALS("suggestedMeals"),
        UPDATE_GOAL("updateGoal"),
		NOTIFY_COMMENT("notifyComment"),
		SHARE_MEAL_PERMISSION("shareMealPermission"),
		SHARED_VIDEO("sharedVideo"),
		MEDICATION_LOG_INFO("medicationLoglInfo"),
		CANNED_MESSAGE("cannedMessage");
		
		String value;
		
		private GCMKEYS(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
		
	}
	
	public enum LOGS {
		GLUCOSE("Glucose"),
		AFTER_MEAL_GLUCOSE("After_Meal_Glucose"),
		NOT_AFTER_MEAL_GLUCOSE("Not_After_Meal_Glucose"),
		MEAL_LOGS("Meal_Logs"),
		Glucose_LOGS("Glucose_Logs"),
		MEAL("Meal"),
		WEEKLY_MEAL("Weekly_Meal"),
		MONTHLY_MEAL("Monthly_Meal"),
		THREE_DAYS_MEAL("three_days_Meal"),
		FOURTEEN_DAYS_MEAL("fourteen_days_Meal"),
		MEAL_AVG("Meal_AVG"),
		MEAL_PERCENTAGE("Meal_Percentage"),
		BREAKFAST("Breakfast"),
		LUNCH("Lunch"),
		SNAKS("Snaks"),
		DINNER("Dinner"),
        TARGET("Target"),
		BREAKFAST_TARGET(0),
		LUNCH_TARGET(0),
		SNAKS_TARGET(0),
		DINNER_TARGET(0);
		
		String value;
		Integer intValue;
		
		private LOGS(String value) {
			this.value = value;
		}
		
		private LOGS(Integer intValue) {
			this.intValue = intValue;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public Integer getIntValue() {
			return intValue;
		}

		public void setIntValue(Integer intValue) {
			this.intValue = intValue;
		}
		
	}
	
	public enum TYPEKEYS {
		BREAKFAST("Breakfast"),
		LUNCH("Lunch"),
		SNACK("Snack"),
		DINNER("Dinner"),
		BEVERAGE("Beverage");
		
		String value;
		
		private TYPEKEYS(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
		
	}
	
	public enum NUTRIONIX {
		APP_ID("57659bc0"),
		APP_KEY("a142fbe339f6254cfdbf3f73acf41611"),
		SEARCH_URL("https://api.nutritionix.com/v1_1/search/"),
		ITEM_URL("https://api.nutritionix.com/v1_1/item"),
		APP_ID_URL_KEY("appId"),
		APP_KEY_URL_KEY("appKey"),
		RESULTS_URL_KEY("results"),
		FIELDS_URL_KEY("fields"),
		ID_URL_KEY("id");
		
		String value;
		
		private NUTRIONIX(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
	
	public enum LOGGING {
		PATIENT_NOT_FOUND_IN_SESSION("Patient not found in session");
		
		String value;
		
		LOGGING(String value) {
			this.value = value;
		}
		
		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
	
	public enum FOODNAMES {
		BOTTLE("Bottle"),
		BOX("Box"),
		CAN("Can"),
		CONTAINER("Container"),
		CUBE("Cube"),
		CUP("Cup"),
		EACH("Each"),
		FLUID_OUNCE("Fluid Ounce"),
		GALLON("Gallon"),
		GRAM("Gram"),
		JAR("Jar"),
		KILOGRAM("Kilogram"),
		LITER("Liter"),
		MICROGRAM("Microgram"),
		MILLIGRAM("Milligram"),
		MILLILITER("Milliliter"),
		OUNCE("Ounce"),
		PACKAGE("Package"),
		PIECE("Piece"),
		PINT("Pint"),
		POUCH("Pouch"),
		QUART("Quart"),
		SCOOP("Scoop"),
		SERVING("Serving"),
		SLICE("Slice"),
		STICK("Stick"),
		TABLESPOON("Tablespoon"),
		TABLET("Tablet"),
		TEASPOON("Teaspoon");
		
		String value;
		
		private FOODNAMES(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}		
	}
	
	public enum GLUCOSELEVELS{
		LOW(80),
		HIGH(180);
		
		int value;
		
		private GLUCOSELEVELS(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public void setValue(int value) {
			this.value = value;
		}
	}
	
	public enum TARGETSETTINGS {		
		PATIENT("Patient"),
		DIETITIAN("Dietitian");
		
		String value;
		
		private TARGETSETTINGS(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
	
	public enum Questions{
		
		Question1("Bagel", "Is the bagel you ate most similar in size to: <b>a)</b> the top lid of a can of beans (3&#34;), <b>b</b>) a standard size can of tuna (3.5&#34;), or <b>c)</b> a CD disc (4.5&#34;)? Type a, b, or c in reply"),
		Question3("Baked potato or yam", "Is each baked potato/yam that you ate: <b>a)</b> bigger, <b>b)</b> smaller or <b>c)</b> the same size as a standard computer mouse? Type a, b, or c in reply."),
		Question5("Banana", "Is the banana you ate: <b>a)</b> longer, <b>b)</b> shorter or <b>c)</b> the same size as a pair of scissors? Type a, b, or c in reply."),
		Question7("Beverages", "Is the size of the (drink) you drank: <b>a)</b> bigger, <b>b)</b> smaller or <b>c)</b> the same size as a standard can of soda pop? Type a, b, or c in reply."),
		Question9("Beverages", "Is the size of the (drink) you drank: <b>a)</b> bigger, <b>b)</b> smaller or <b>c)</b> the same size as a plastic bottle of soda pop? Type a, b, or c in reply."),
		Question11("Biscuit", "Is each biscuit you ate most similar in size to the top of: <b>a)</b> a can of soda (2&#34; across) or <b>b)</b> a hockey puck (3&#34; across)?Type a or b in reply."),
		Question13("Bread", "Is each bread slice you ate: <b>a)</b> bigger, <b>b)</b> smaller or <b>c)</b> the same size as a CD case? Type a, b, or c in reply."),
		Question15("Bread roll", "Is each bread roll you ate most similar in size to: <b>a)</b> the top of a can of soda (2&#34; across) or <b>b)</b> a hockey puck (3&#34; across)? Type a or b in reply."),
		Question17("Brownie", "Is the portion of brownie you ate: <b>a)</b> bigger, <b>b)</b> smaller or <b>c)</b> the same size as a dental floss package? Type a, b, or c in reply."),
		Question19("Cake", "Is the portion of cake you ate: <b>a)</b> bigger, <b>b)</b> smaller or <b>c)</b>  the same size as a standard cupcake? Type a, b, or c in reply."),				
		Question21("Chocolate piece", "Is the portion of chocolate you ate: <b>a)</b> bigger, <b>b)</b> smaller or <b>c)</b> the same size as a standard dental floss package? Type a, b, or c in reply."),
		Question23("Corn, peas, other starchy vegetables", "Is the portion of (corn/peas/other starchy vegetable) you ate most similar in size to: <b>a)<b> a tennis ball, <b>b)<b> a baseball, or <b>c)<b> a softball? Type a, b, or c in reply"),
		Question25("Cookie", "Is each cookie you ate: <b>a)</b> bigger, <b>b)</b> smaller or <b>c)</b> the same size as a Chips Ahoy&#33;&#174; cookie? Type a, b, or c in reply."),
		Question43("Cereal (cold or hot-cooked)", "Is the portion of cereal you ate most similar in size to: <b>a)</b> a tennis ball, <b>b)</b> a baseball, or <b>c)</b> a softball? Type a, b, or c in reply."),
		Question29("Fruit, piece", "Is the (food) you ate most similar in size to: <b>a)</b> a tennis ball, <b>b)</b> a baseball, or <b>c)</b> a softball? Type a, b, or c in reply"),
		Question31("Ice cream", "Is the portion of ice cream you ate most similar in size to: <b>a)</b> a tennis ball, <b>b)</b> a baseball, or <b>c)</b> softball? Type a, b, or c in reply."),
		Question44("Misc", "How many (tablespoons, cups, pieces) did you eat of (food)?"),
		Question45("Misc", "Did you eat the whole portion of (food)?"),
		Question46("Misc", "Did you drink the whole portion of (food)?"),		
		Question33("Muffin", "Is each muffin you ate most similar in size to: <b>a)</b> a standard muffin (like the top of a can of soda, 2&#34; across) or <b>b)</b> a Costco-sized muffin (like a hockey puck, 3&#34; across)? Type a or b in reply."),		
		Question35("Pancake", "Is each pancake you ate most similar in size to: <b>a)</b> a CD disc (4.5-5&#34;), <b>b)</b> a CD case (6&#34;) or <b>c)</b> a Frisbee (8-10&#34;)? Type a, b, or c in reply."),
		Question37("Pasta/noodles", "Is the portion of pasta/noodles you ate most similar in size to a: <b>a)</b> tennis ball, <b>b)</b> baseball, or <b>c)</b> softball? Type a, b, or c in reply."),		
		Question39("Rice", "Is each portion of rice you ate most similar in size to: <b>a)</b> a tennis ball, baseball, or <b>b)</b> a softball? Type a or b in reply."),
		Question41("Snack foods","Is the portion of (chips/crackers/pretzels/ nuts/other snack food) you ate most similar in size to: <b>a)</b> a tennis ball, <b>b)</b> a baseball, or <b>c)</b> a softball? Type a, b, or c in reply.");
		
		String category;
		String question;
		
		private Questions(String category ,String  question) {			
			this.category = category;
			this.question = question;
		}
		
		public String getQuestion() {
			return question;
		}
		
		public void setQuestion(String question) {
			this.question = question;
		}
		
		public String getCategory() {
			return category;
		}
		
		public void setCategory(String category) {
			this.category = category;
		}
	}
	
	public enum ExcludedServices {
		SIGN_UP("signUp/patientSignUp"),
        V2_SIGN_UP("signUp/v2/patientSignUp"),
		SIGN_UP_COMPLETE("signUp/patientSignUpComplete"),
        V2_SIGN_UP_COMPLETE("signUp/v2/patientSignUpComplete"),
		VERIFY_EMAIL("signUp/verifyEmail"),
        V2_VERIFY_EMAIL("signUp/v2/verifyEmail"),
		EMAIL_TOS("signUp/emailTOS"),
        V2_EMAIL_TOS("signUp/v2/emailTOS"),
		FORGOT_PASSWORD("user/forgotPassword"),
        V2_FORGOT_PASSWORD("user/v2/forgotPassword"),
        LOOK_FOR_USER("auth/lookForUser"),
        SAVE_FEEDBACK("user/saveFeedback"),
        V2_SAVE_FEEDBACK("user/v2/saveFeedback"),
        CHANGE_PASSWORD("user/changePassword"),
        V2_CHANGE_PASSWORD("user/v2/changePassword"),
        BROADCAST_SEND_EMAIL("broadcast/sendEmail"),
		SMS_REPLY("log/saveSmsReply"),
        LOGIN_FACILITY("user/getLoginFacilities"),
        V2_LOGIN_FACILITY("user/v2/getLoginFacilities"),
        GET_TOKEN("sightcall/getToken"),
		LOG_IN("palite/login"),
		PATIENT_NAME_WITH_METER_SERIAL_NUMBER("palite/getPatientByMeterSerialNumber"),
        SAVE_LOG("log/saveLog"),
        BROADCAST_REMOVE_DEVICE("broadcast/removeDevice"),
		ASSESSMENT_FORM_URL("assessment/getAssessmentFormUrl"),
        PALITE_SAVE_LOG("palite/saveLog"),
        GLUCOSE_CHART_DATA("palite/getGlucoseChartData"),
        GLUCOSE_CHART_DAILY_DATA("palite/getDailyMealGlucoseChartData"),
		GLUCOSE_CHART_DETAIL_DATA("palite/getGlucoseDetailChartData"),
		GLUCOSE_CHART_DATA_COMBINED("palite/getGlucoseChartDataCombined"),
		PALITE_PATIENT_SIGNUP("palite/patientSignUp"),
		PALITE_EXISTING_PATIENT_SIGNUP("palite/existingPatientSignUp"),
		PATIENT_SAVE_FEEDBACK("palite/saveFeedback"),
        PALITE_FORGOT_PASSWORD("palite/forgotPassword"),
		PALITE_SEND_DEBUG_EMAIL("palite/emailDebugFile");
		
		private ExcludedServices(String value) {
			this.value = value;
		}
		String value;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
		
		public static boolean isExcluded(String uri) {
			boolean isExcluded = false;
			for (ExcludedServices excludedServices : values()) {
				if(excludedServices.getValue().equalsIgnoreCase(uri.trim())) {
					isExcluded = true;
				}
			}
			return isExcluded;
		}
	}
	
	
	public static Map<String, String> SERVING_UNIT_MAP;
	static {
		SERVING_UNIT_MAP = new LinkedHashMap<String, String>();
		SERVING_UNIT_MAP.put("", "-- select unit --");
		/*SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.BOTTLE.getValue(),AppConstants.FOODNAMES.BOTTLE.getValue());*/
		/*SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.BOX.getValue(),AppConstants.FOODNAMES.BOX.getValue());*/
		/*SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.CAN.getValue(),AppConstants.FOODNAMES.CAN.getValue());*/
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.CONTAINER.getValue(),AppConstants.FOODNAMES.CONTAINER.getValue());
		/*SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.CUBE.getValue(),AppConstants.FOODNAMES.CUBE.getValue());*/
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.CUP.getValue(),AppConstants.FOODNAMES.CUP.getValue());
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.EACH.getValue(),AppConstants.FOODNAMES.EACH.getValue());
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.FLUID_OUNCE.getValue(),AppConstants.FOODNAMES.FLUID_OUNCE.getValue());
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.GALLON.getValue(),AppConstants.FOODNAMES.GALLON.getValue());
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.GRAM.getValue(),AppConstants.FOODNAMES.GRAM.getValue());
		/*SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.JAR.getValue(),AppConstants.FOODNAMES.JAR.getValue());*/
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.KILOGRAM.getValue(),AppConstants.FOODNAMES.KILOGRAM.getValue());
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.LITER.getValue(),AppConstants.FOODNAMES.LITER.getValue());
		/*SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.MICROGRAM.getValue(),AppConstants.FOODNAMES.MICROGRAM.getValue());
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.MILLIGRAM.getValue(),AppConstants.FOODNAMES.MILLIGRAM.getValue());*/
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.MILLILITER.getValue(),AppConstants.FOODNAMES.MILLILITER.getValue());
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.OUNCE.getValue(),AppConstants.FOODNAMES.OUNCE.getValue());
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.PACKAGE.getValue(),AppConstants.FOODNAMES.PACKAGE.getValue());
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.PIECE.getValue(),AppConstants.FOODNAMES.PIECE.getValue());
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.PINT.getValue(),AppConstants.FOODNAMES.PINT.getValue());
		/*SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.POUCH.getValue(),AppConstants.FOODNAMES.POUCH.getValue());*/
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.QUART.getValue(),AppConstants.FOODNAMES.QUART.getValue());
	/*	SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.SCOOP.getValue(),AppConstants.FOODNAMES.SCOOP.getValue());*/
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.SERVING.getValue(),AppConstants.FOODNAMES.SERVING.getValue());
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.SLICE.getValue(),AppConstants.FOODNAMES.SLICE.getValue());
		/*SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.STICK.getValue(),AppConstants.FOODNAMES.STICK.getValue());*/
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.TABLESPOON.getValue(),AppConstants.FOODNAMES.TABLESPOON.getValue());
		/*SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.TABLET.getValue(),AppConstants.FOODNAMES.TABLET.getValue());*/
		SERVING_UNIT_MAP.put(AppConstants.FOODNAMES.TEASPOON.getValue(),AppConstants.FOODNAMES.TEASPOON.getValue());
	}
	
	public static Map<Float, String> SERVING_MAP;
	static {
		SERVING_MAP = new LinkedHashMap<Float, String>();
		SERVING_MAP.put(0f, "0");
		SERVING_MAP.put(0.25f, "1/4");
		SERVING_MAP.put(0.3f, "1/3");
		SERVING_MAP.put(0.5f, "1/2");
		SERVING_MAP.put(0.6f, "2/3");
		SERVING_MAP.put(0.75f, "3/4");
		SERVING_MAP.put(1f, "1");
		SERVING_MAP.put(1.25f, "1.25");
		SERVING_MAP.put(1.50f, "1.50");
		SERVING_MAP.put(1.75f, "1.75");
		SERVING_MAP.put(2f, "2");
	 	SERVING_MAP.put(3f, "3");
		SERVING_MAP.put(4f, "4");
		SERVING_MAP.put(5f, "5");
		SERVING_MAP.put(6f, "6");
		SERVING_MAP.put(7f, "7");
		SERVING_MAP.put(8f, "8");
		SERVING_MAP.put(9f, "9");
		SERVING_MAP.put(10f, "10");
	}
	
	public static Map<Integer, String> LOG_STATUS_MAP;
	static {
		LOG_STATUS_MAP = new LinkedHashMap<Integer, String>();
		LOG_STATUS_MAP.put(0, AppConstants.ALL);
		LOG_STATUS_MAP.put(1, AppConstants.NOT_PROCESSED);
		LOG_STATUS_MAP.put(2, AppConstants.PROCESSED);
		LOG_STATUS_MAP.put(3, AppConstants.ARCHIVED);
	}
	
	public static Map<Integer, String> LOG_TYPE_MAP;
	static {
		LOG_TYPE_MAP = new LinkedHashMap<Integer, String>();
		LOG_TYPE_MAP.put(0, AppConstants.ALL);		
		LOG_TYPE_MAP.put(1, AppConstants.LOG_TYPE_MEAL);
		// *********** new code ***********
		/*LOG_TYPE_MAP.put(2, AppConstants.LOG_TYPE_EXERCISE);*/
		LOG_TYPE_MAP.put(2, AppConstants.LOG_TYPE_ACTIVITY);
		LOG_TYPE_MAP.put(3, AppConstants.LOG_TYPE_GLUCOSE);
		LOG_TYPE_MAP.put(4, AppConstants.LOG_TYPE_MEDICATION);
		LOG_TYPE_MAP.put(5, AppConstants.LOG_TYPE_WEIGHT);
		LOG_TYPE_MAP.put(6, AppConstants.LOG_TYPE_MISC);
		LOG_TYPE_MAP.put(7, AppConstants.LOG_TYPE_ACTIVITY_MINUTE);

		// *********** end of new code ***********
	}
	public static Map<String, String> TITLE_MAP;
	static {
		TITLE_MAP = new LinkedHashMap<String, String>();
		TITLE_MAP.put(AppConstants.TYPEKEYS.BREAKFAST.getValue(), AppConstants.TYPEKEYS.LUNCH.getValue());
		TITLE_MAP.put(AppConstants.TYPEKEYS.LUNCH.getValue(), AppConstants.TYPEKEYS.SNACK.getValue());
		TITLE_MAP.put(AppConstants.TYPEKEYS.SNACK.getValue(), AppConstants.TYPEKEYS.DINNER.getValue());
	}
		
	public static Map<Integer, String> MEAL_TYPES;
	static {
		MEAL_TYPES = new LinkedHashMap<Integer, String>();
		MEAL_TYPES.put(0, ALL);
		MEAL_TYPES.put(1, MEAL_TYPE_BREAKFAST);		
		MEAL_TYPES.put(2, MEAL_TYPE_LUNCH);		
		MEAL_TYPES.put(3, MEAL_TYPE_SNACK);
		MEAL_TYPES.put(4, MEAL_TYPE_DINNER);
		MEAL_TYPES.put(5, MEAL_TYPE_BEVERAGE);
	}
	
	public enum JsonConstants {STATUS, REASON, SUCCESS, ERROR, DATA, PRINT, GOALS, HABITS, SUGGESTED_MEALS, CUSTOM_DATA, FAVORITE_MEALS}
	
	public enum TwillioConstants {		
		ACCOUNT_SID("AC5ad136fca4caa9948decdd21430408b4"), // Replace with SID of your account.
		AUTH_TOKEN("c41ed5cff0256c2443d95aa9ee4e9855"), // Replace with new token from your account.
		TWILIO_NUM("+1 202-800-1189"); // Replace with a valid phone number for your account.
		
		String value;
		
		private TwillioConstants(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}		
	}
	
	public enum StatusConstants {		
		INPROCESS("In Process"),
		NOTSTARTED("Not Started"),
		ACHIEVED("Achieved"),
		DEVICETYPE("DEVICE_TYPE"),
        DEVICE_BUILD_TYPE("SP_DEVICE_BUILD_TYPE"),
		DEVICE_NAME("SP_DEVICE_APP_NAME"),
		USER_AGENT_STRING("USER_AGENT_STRING"),
		LOG_TIME_OFFSET("LOG_TIME_OFFSET"),
		OFFSET_KEY("OFFSET_KEY"),
		SMS_LIST("SMS_LIST"),
		SMS_TEMPLATES("SMS_TEMPLATES"),
        RESULT("result"),
		NOTACHIEVED("Not Achieved");
		
		String value;
		
		private StatusConstants(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}		
	}

    public enum ServiceResponseMessages {
        OLD_PASSWORD_INCORRECT("Old password is incorrect."),
        PATIENT_NOT_FOUND("Patient not found."),
        USER_NOT_FOUND("User not found."),
        PASSWORD_CHANGE_SUCCESS("Password changed successfully.");

        String value;

        private ServiceResponseMessages(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

	public static final String GOAL_CATEGORY = "goalCategory";
	public static final String GOAL_OPTION = "goalOption";
	public static final String GOAL_RATING = "goalRating";
	public static final String GOAL = "GOAL";
    public static final String GOAL_RATINGS = "GOAL_RATINGS";
	public static final String ACTION_PLAN = "actionPlan";
    public static final String OVERCOMING_CHALLENGE = "overcomeChallenge";
    public static final String MY_CHALLENGE = "myChallenge";
    public static final String PLAN_FREQUENCY = "planFrequency";
    public static final String CALENDAR_FREQUENCY = "calendarFrequency";
    public static final String GOAL_MEALS = "meals";
    public static final String GOAL_OTHER = "other";
    public static final String GOAL_OTHERS = "goalOthers";
    public static final String MY_REWARD = "myReward";

	public static final String HABIT = "HABIT";
	
	public static final String REMINDER = "REMINDER";
	public static final String NOREMINDER = "NO REMINDER";
	
	//public static final String SIGHT_CALL_APP_ID = "6u2uuamuqkxe";
	
	public enum ChartServiceConstants {
		AVG("avg"),
		AVERAGE_OF_EACH_MEALS("averageOfEachMeals"),
		ACTIVITY_LOGS("activityLogs"),
		/*BEVERAGE_AVERAGE("beverageAverage"),*/
		BREAKFAST_AVERAGE("breakfastAverage"),
		CARBS("carbs"),
		CARBS_LEVEL_DATA("carbsLevelData"),
        TOOK_MED_DATA("tookMedData"),
		CURRENT_DAY_SUM("currentDaySum"),
		CURRENT_WEIGHT("currentWeight"),
		DINNER_AVERAGE("dinnerAverage"),
		DAY("day"),
		DATE("date"),
		GLUCOSE_LEVEL_DATA("glucoseLevelData"),
		HAS_MISSING_FOOD("hasMissingFood"),
		IN_TARGET("inTarget"),
		ID("id"),
		LEVEL("level"),
		GLUCOSE_TIME("glucoseTime"),
		LUNCH_AVERAGE("lunchAverage"),
		MEDS_TAKEN("medsTaken"),
		MISFIT_ACTIVITY("misfitActivity"),
		OVER("over"),
		PERCENTAGE("percentage"),
		PERCENTAGE_UNDER_CEILING_MEALS("percentageUnderCeilingMeals"),
		SUM("sum"),
		SNACK_AVERAGE("snackAverage"),
		STEPS("steps"),
		STEPS_PER_DAY("stepsPerDay"),
		TOTAL("total"),
		TOOK_MEDS_PERCENTAGE("tookMedsPercentage"),
		TIMESTAMP("timestamp"),
		TYPE("type"),
		UNDER("under"),
        BEFORE_MEAL("beforeMeal"),
        AFTER_MEAL("afterMeal"),
		WEIGHT_LOST("weightLost"),
		ACTIVITY_MINUTES("activityMinutes"),
		ACTIVITY_MINUTES_TYPE("activityMinutesType"),
		ACTIVITY_MINUTES_TYPES("activityMinutesTypes"),
		ACTIVITY_MINUTES_PERFORMED("minutesPerformed");;
		
		String value;
		
		private ChartServiceConstants(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}		
	}

	public enum DeviceTypes {
		ANDROID("Android"),
		IOS("IOS");
		String value;

		private DeviceTypes(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}

    public enum DeviceBuildTypes {
        DEBUG("Debug"),
        RELEASE("Release");
        String value;

        private DeviceBuildTypes(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

	public enum EmailSubjects{
		PATIENT("Requested Healthslate Resource"),
		SPONSOR_RESPONSE("Sponsor's Response"),
		NEW_REQUEST("New Product Sample Request"),
		RESET_PASSWORD("Reset Password Request"),
        CREATE_PASSWORD("Create New Password"),
		TOS("Terms of Service"),
		MEMBER_FEEDBCK("Member Feedback"),
        UNPROCESSED_MEAL_LOG("Unprocessed Meal Logged"),
        IN_APP_MESSAGE("New In-app Message"),
		NEW_MEMBER_JOINED("New Member Joined"),
		SET_DINNER_CARB_CEILING("Set Dinner Carb Ceiling"),
        FIRST_WEEK_TASKS("1st Week Tasks For New Member"),
        NEW_COACH_NOTE("New Coach Notes"),
		NEW_COACH_ASSESSMENT("New Coach Assessment"),
		MEAL_ERROR("Meal Error Reported"),
        JOINED_HEALTHSLATE_PROGRAM("HealthSlate Program Invitation"),
        ENROLL_HEALTHSLATE_PROGRAM("Welcome to the $$fName$$ HealthSlate program"),
		DENIED_EMAIL_SUBJECT("Your request to join the $$fName$$ HealthSlate program"),
        HEALTHSLATE_MOBILE_APP_LINK("HealthSlate Mobile App Download Link"),
        TWILIO_FUNDS_ZERO("Twilio Zero Funds"),
        NEW_MEMBER_REQUEST("New Member Request"),
        MORE_SUGGESTED_MEALS("More Suggested Meals"),
		CUSTOMER_SUPPORT("Customer Support"),
		DEBUG_EMAIL("CDE BG Debug Email"),
		TECH_SUPPORT_MESSAGE("New tech-support Message");

		String value;

		EmailSubjects(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}

	public enum PreferencesNames{
		EMAIL_ID("emailId"),
		HOST("smtpHost"),
		USERNAME("username"),
		PASS("password"),
		ACCESS_KEY("accessKey"),
		SECRET_KEY("secretKey"),
		END_POINT("endPoint"),
		MAIN_BUCKET("mainBucketPath"),
		ADMIN_ACCESS_KEY("s3AdminAccessKey"),
		ADMIN_SECRET_KEY("s3AdminSecretKey"),
		CONTENT_BUCKET_NAME("contentBucketName"),
		MONITORED_EMAIL("monitoredEmail"),
		EMAIL_REPLY_ADDRESS("emailReplyAddress"),
        SOCIAL_GROUP_ID("socialGroupId"),
        COACH_MESSAGE_TEXT("coachMessageText"),
        UNPROCESSED_EMAIL_ADDRESS("unprocessedCoachEmailAddress"),
        UNPROCESSED_SMS_NUMBERS("unprocessedCoachSMS"),
        UNPROCESSED_COACH_SMS_TEMPLATE("unprocessedCoachSMSTemplate"),
        NEW_IN_APP_MESSAGE_SMS_TEMPLATE("newInAppMessageSMSTemplate"),
        SIGHT_CALL_APP_ID("sightCallAppId"),
		FEED_BACK_EMAIL_ID("feedbackEmailId"),
		HEALTHSLATE_FACILITY_ID("healthslateFacilityId"),
        SIGHTCALL_P12_PASS("sightCallP12Pass"),
        SIGHTCALL_CLIENT_ID("sightcallAuthAPIClientId"),
        SIGHTCALL_CLIENT_SECRET("sightcallAuthAPIClientSecret"),
        ELGG_WEB_SERVICE_PATH("elggWebservicePath"),
        TWILIO_ACCOUNT_SSID("twilioAccounSSid"),
        TWILIO_AUTH_TOKEN("twilioAuthToken"),
        TWILIO_NUMBER("twilioNumber"),
        CURRICULUM_API("curriculumAPI"),
        SUPPORT_EMAIL_ADDRESS("supportEmailAddress"),
		APPLICATION_PATH("applicationPath"),
        PLAYSTORE_APP_URL("playStoreAppUrl"),
        APPLESTORE_APP_URL("appleStoreAppUrl"),
        TWILIO_FUNDS_ZERO("twilioFundsZeroEmail"),
        FIRST_ONE_TO_ONE_SESSION("firstOneToOneSessionReminderDelay"),
        SECOND_ONE_TO_ONE_SESSION("secondOneToOneSessionReminderDelay"),
        TECH_SUPPORT_NUMBER("techSupportNumber"),
		FORMS_ASSESSMENT_URL("formsAssessmentUrl"),
		RECRUITMENT_FACILITY("recruitmentFacility"),
		WISTIA_API_TOKEN("wistiaApiToken");

		String value;

		PreferencesNames(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
	}

	//public static final String ATOZ_FACILITY = "atoz";

	public enum ResponseErrorMessages{
		ERROR_MESSAGE("Server error, unable to send message.");

		String value;

		ResponseErrorMessages(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
	}

    public enum JsonKeys{
        OLD_PASSWORD("oldPassword"),
        NEW_PASSWORD("newPassword"),
        PATIENT_ID("patientId"),
        EMAIL_ADDRESS("emailAddress"),
        LOG_ID("logId"),
		TITLE("title"),
		MESSAGE("message");

        String value;

        JsonKeys(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public enum CoachesArea{
        COACH_NOTES,
        DIETARY_NOTES,
        ONE_TO_ONE_SESSIONS,
        LATEST_ONE_TO_ONE_SESSION,
        PATIENT_COACHES,
        PATIENT_DETAILS,
        UUID,
        CURRICULUM_API_ADDRESS,
        CURRICULUM_WEEK,
        EXCLUDED_ME_COACH,
        LEAD_COACH_ID,
        PRIMARY_FOOD_COACH_ID,
        COACH_TYPE,
        COACHES_LISTING,
        PATIENT_MEDICATIONS,
        PATIENT_SESSION_PREFERENCE,
        PATIENT_EATING_PREFERENCE,
        MEMBER_KEY_FACTS,
		COACH_LOGS,
        MEDICATIONS_LISTING,
		COACH_SESSION_PREFERENCE,
    }

    public enum CoachTypes{
        FOOD_COACH("Food Coach", 2),
        COACH("Coach", 4);

        int sortType;
        String value;

        CoachTypes(String value, int sortType) {
            this.value = value;
            this.sortType = sortType;
        }

        public static CoachTypes getCoach(String coachType) {
            for (CoachTypes coach : values()){
                if(coach.value.equalsIgnoreCase(coachType)){
                    return coach;
                }
            }
            return null;
        }

        public void setCoach(String value) {
            this.value = value;
        }

        public int getSortType(){
            return this.sortType;
        }

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public void setSortType(int sortType) {
			this.sortType = sortType;
		}
    }

	public enum UserInfo {
		AUTHORIZATION("AUTHORIZATION"),
		MAC_ADDRESS("SP_MAC_ADDRESS"),
        SP_EMAIL("SP_EMAIL"),
        SP_SERVER_INFO("SP_SERVER_INFO"),
		VERSION("version");
		String value;

		UserInfo(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}

    public static final String DEV = "dev";
    public static final String PROD = "prod";
    public static final String DEV_KEY_STORE_PASSWORD = "123Pat!3Nt123";
    public static final String PROD_KEY_STORE_PASSWORD = "123Pat!3Nt123";

	public static final String STAGING_DEBUG_CERTIFICATE ="APNS_Certificates/staging/debug/PatientApp-Staging-DebugCert.p12";
    public static final String STAGING_RELEASE_CERTIFICATE ="APNS_Certificates/staging/release/PatientApp-Staging-ReleaseCert.p12";

	public static final String PROD_DEBUG_CERTIFICATE ="APNS_Certificates/production/debug/PatientApp-Prod-DebugCert.p12";
	public static final String PROD_RELEASE_CERTIFICATE ="APNS_Certificates/production/release/PatientApp-Prod-ReleaseCert.p12";

    public static final String SOUND_NAME = "default";

    public static final String CURRICULUM_CONTENT_API = "content/week/$$week$$/coaching";
    public static final String CURRICULUM_CONTENT_USERS = "users/$$uuid$$/summary/";

    public static final String IOS_SMS_KEY = "healthslate://";
    public static final String HTTP_KEY = "http://";

    public enum SmsTemplates {
        VIDEO_CALL_INSTANT_MSG("Video Call Instant Message"),
        VIDEO_CALL_CANCELLED_MESSAGE("Video Call Cancelled Message"),
        VIDEO_CALL_RESCHEDULED_MESSAGE("Video Call Rescheduled Message"),
        VIDEO_CALL_TODAY_MESSAGE("Video Call Today Message"),
        VIDEO_CALL_TOMORROW_MESSAGE("Video Call Tomorrow Message"),
        GOAL_REMINDER_MESSAGE("Goal Reminder Message"),
        DAILY_REMINDER("Daily Reminder"),
        WEEKLY_REMINDER("Weekly Reminder"),
		MEAL_ERROR_EMAIL("Meal Error Email"),
		MEAL_ERROR_SMS("Meal Error SMS"),
		POST_COMMENT("Post Comment"),
		SHARE_VIDEO("Share Video"),
		SHARE_MEAL_CONSENT("Share Meal Consent"),
		SUGGESTED_MEAL("Suggested Meal"),
		CANNED_EMAIL("Canned Email"),
		CANNED_EMAIL_TITLE("Canned Email Title"),
		WELCOME_MEMBER_MESSAGE("Welcome Member Message"),
		WELCOME_MEMBER_MESSAGE_WITHOUT_PREFS("Welcome Member Message Without Prefs");

        String value;

        SmsTemplates(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public enum StringReplaceChunks {
        DATE("$$date$$"),
        NEW_DATE("$$newDate$$"),
        TIME("$$time$$"),
        NEW_TIME("$$newTime$$"),
        DATE_TIME("$$datetime$$"),
        F_NAME("$$fName$$"),
		C_NAME("$$cName$$"),
        ID("$$id$$"),
		COACH_NAME("$$coachName$$"),
		MEAL_CATEGORY("$$mealCategory$$"),
		HASH("#"),
		PREFS("$$prefs$$");

        String value;

        StringReplaceChunks(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public enum ServerInfoConstants {
        HSTAGE("hstage"),
        HSTAGE_2("hstage2"),
        PROD("prod"),
        TPROD("tprod"),
        OZY("ozy"),
        SANDBOX("sandbox");

        String value;

        ServerInfoConstants(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public static final String NO_DATA_FOUND_IN_REQ = "No data found in request";
    public static final String PATIENT_ALREADY_EXISTS = "Member already exist";
    public static final String EMAIL_NOT_REGISTERED = "Email address is not registered in our system";
    public static final String DENIED_REQUEST_MESSAGE = AppConstants.UBABLE_TO_SIGNUP;
    public static final String NOT_APPROVED_MESSAGE = "Your request is in process. Please try again later or contact customer service at (888) 291-7245.";
    public static final String EMAIL_ALREADY_REGISTERED = "The email address is already registered";
    public static final String EMAIL_ALREADY_EXIST = "ALREADY_EXIST";
    public static final String INVALID_EMAIL = "Invalid email address";
    public static final String ENROLL_FORM = "ENROLL_FORM";
    public static final String ADD_REQUESTED_MEMBER = "ADD_REQUESTED_MEMBER";
    public static final String INVALID_EMAIL_PASSWORD = "Invalid username/password";
    public static final String REG_NOT_COMPLETED = "Your Sign Up process is not completed. Please contact customer service at (888) 291-7245.";
    public static final String SERVER_INFO_ERROR = "Unable to find server. Please check spellings.";
	public static final String MRN_ALREADY_ASSIGNED = "MRN already assigned";

    // [0, 1] means request is in process not approved yet
    // 2 means denied by admin
    // 3 means approved by admin
    public static final String REQ_IN_PROCESS_0 = "0";
    public static final String REQ_IN_PROCESS_1 = "1";
    public static final String REQ_DENIED = "2";
    public static final String REQ_APPROVED = "3";

	public static final String IOS_STAGING_APP_NAME = "HealthSlate-Staging";
	public static final String IOS_PROD_APP_NAME = "HealthSlate";

	public enum PushNotificationConstants {
		CONTENT_AVAILABLE("content-available"),
		EVENT("event"),
		DATA("data"),
		APS("aps"),
		ALERT("alert"),
		BADGE("badge"),
		SOUND("sound"),
		LOG_ID("logId"),
		POST_ID("postId"),
		LINK("link"),
		IMAGE("image"),
		DESCRIPTION("description"),
		CANNED_MESSAGE("cannedMessage");

		String value;

		PushNotificationConstants(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}

	public static final String PRIMARY_FOOD_COACH = "PRIMARY_FOOD_COACH";

	public enum ShareMealConstants {
		DONT_SHARE,
		SHARE,
		SHARE_ANONYMOUSLY;
	}

	public enum MiscConstants {
		LOG_ENTRIES_COUNT,
		AVG_STEPS,
		MISFIT_BATTERY_STATUS,
		CURRENT_WEIGHT,
		DATE_SPAN;
	}
}




