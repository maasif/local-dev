package com.healthslate.patientapp.util;

import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by omursaleen on 6/26/2015.
 */
public class BrowserDetectUtil {

    public static String detectBrowser(HttpServletRequest request){

        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        String uaString = userAgent.getBrowser() + " / " + userAgent.getBrowserVersion();

        return  uaString;
    }

    public static String detectOS(HttpServletRequest request){

        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        OperatingSystem operatingSystem = userAgent.getOperatingSystem();

        return operatingSystem.getName() + " / " + operatingSystem.getManufacturer();
    }
}
