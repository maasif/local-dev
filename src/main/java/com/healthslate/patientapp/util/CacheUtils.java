

package com.healthslate.patientapp.util;

import java.io.InputStream;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.store.DiskStoreBootstrapCacheLoader;

public class CacheUtils {
	
	public static CacheManager manager;
	public static Ehcache cache;		
	public static CacheUtils cacheUtils = null;
			
	private CacheUtils() {
		
		// load configuration file for ehcache
		InputStream inputStream = CacheUtils.class.getClassLoader().getResourceAsStream("ehcache.xml");
		manager = CacheManager.create(inputStream);	
				
		if (!manager.cacheExists("cache")) {
			manager.addCache("cache");			
			cache = manager.getCache("cache");		
		}
		else {
			cache = manager.getCache("cache");
			
			// loads  
//			DiskStoreBootstrapCacheLoader diskStoreBootstrapCacheLoader = new DiskStoreBootstrapCacheLoader(true);
//			diskStoreBootstrapCacheLoader.load(cache);					
		}
	}
	
	public static synchronized CacheUtils getInstance() {
		if ( cacheUtils == null ) {
			cacheUtils = new CacheUtils();		
		}
		return CacheUtils.cacheUtils;	
	}
	
	// inserts element in cache
	// if already exists updates it's value
	// returs true if inserted or updated. else returns false
	public static boolean insertElement(String key, String value) {
		
		cacheUtils = getInstance(); 
		Element element = new Element(key, value);
		cache.put(element);			
				
		if ( cache.get(key) != null ) {
			cache.flush();			
			return true;
		}			
		else
			return false;
	}	
	
	// pass "facts" as key
	// returns all facts in json form
	// returns an element from cache as ( KEY, Value) pair
	// to get value from returned element call .getObjectValue() method on that element	
	public static Element getElement(String key) {
		cacheUtils = getInstance();
		Element element = null;
		try {
			element = cache.get(key);			
		}
		catch (Exception e) {
		}	
		return element;
	}
	
	// removes an element whose key matches with recieved key
	// returns true when element is removed. else returns false
	public static boolean removeElement(String key) {
		cacheUtils = getInstance();
		if ( cache.remove(key) ) {			
			cache.flush();
			return true;
		}			
		else
			return false;
	}

	
}
