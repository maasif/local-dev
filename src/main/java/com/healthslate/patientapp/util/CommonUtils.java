package com.healthslate.patientapp.util;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.healthslate.patientapp.model.action.app.admin.AdminAction;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.CoachDTO;
import com.healthslate.patientapp.model.dto.PatientDTO;
import com.healthslate.patientapp.model.dto.PatientDetailDTO;
import com.healthslate.patientapp.model.entity.*;
import com.opensymphony.xwork2.ActionContext;
import jersey.repackaged.com.google.common.base.Joiner;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.codec.Base64;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

public class CommonUtils {
	static final Logger LOG = LoggerFactory.getLogger(CommonUtils.class);
	public static String getProjectPath(){

    	String WEBINF = "WEB-INF";
    	String filePath = "";
        URL url = AdminAction.class.getResource("AdminAction.class");
        String className = url.getFile();
        filePath = className.substring(0,className.indexOf(WEBINF) + WEBINF.length());
        filePath = filePath.replaceAll("\\\\", "/");
        filePath = filePath.replaceAll("%20"," ");
        filePath = filePath.replace("/WEB-INF", "");
        return filePath;
    }
	
	public static String getAppPath(){

    	String WEBINF = "WEB-INF";
    	String filePath = "";
        URL url = AdminAction.class.getResource("AdminAction.class");
        String className = url.getFile();
        filePath = className.substring(0,className.indexOf(WEBINF) + WEBINF.length());
        filePath = filePath.replaceAll("\\\\", "/");
        filePath = filePath.replaceAll("%20"," ");
        filePath = filePath.replace("/WEB-INF", "");
        filePath = filePath.substring(filePath.indexOf("/PatientApp"));
        return filePath;
    }
	
	public static String gmtToest(Long timestamp)
	{
		Date date = new Date();
		date.setTime(timestamp);
		DateFormat gmtFormat = new SimpleDateFormat();
	    /*TimeZone EstTime = TimeZone.getTimeZone("America/New_York");
	    gmtFormat.setTimeZone(EstTime);*/
		
		return gmtFormat.format(date);

	}
	
	public static String gmtToTimeZone(Long timestamp ,String TimeZoneId)
	{
		Date date = new Date();
		date.setTime(timestamp);
		DateFormat gmtFormat = new SimpleDateFormat();
	    TimeZone timzone = TimeZone.getTimeZone(TimeZoneId);
	    gmtFormat.setTimeZone(timzone);
		
		return gmtFormat.format(date);

	}
	
	public static Long gmtToEstInLong(Long timestamp)
	{
		Date date = new Date();
		date.setTime(timestamp);
		DateFormat gmtFormat = new SimpleDateFormat();
	    TimeZone EstTime = TimeZone.getTimeZone("America/New_York");
	    gmtFormat.setTimeZone(EstTime);
		
		return date.getTime();

	}
	public static Date getCurrentDateTimeEST(){
		DateFormat gmtFormat = new SimpleDateFormat();
		TimeZone estTime =TimeZone.getTimeZone("America/New_York");
		Calendar calendar = new GregorianCalendar();
		calendar.setTimeZone(estTime);
		calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY)-12);
		gmtFormat.setTimeZone(estTime);
		gmtFormat.format(calendar.getTime());
		return calendar.getTime();
	}
	

	public static Date getFormatedDateOnly(Date date) throws ParseException{
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		if(date == null){
			return null;
		}
		return dateFormat.parse(dateFormat.format(date));
	}
	
	public static long timeInMilisFromDate(Date date){
		
		return date.getTime();
	}
	
	public static String getIntegerFromFloatWIthUnit(Float number, String unit){
		
		if(number == null || number == 0.0){
			return "N/A";
		}
		else{
			return number.intValue() +" "+ unit;
		}
	}
	
	public static String getFormatedString(String dailyReportDateString){
		
		try {
			if(dailyReportDateString.length() == 8){
				dailyReportDateString = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("MM/dd/yy").parse(dailyReportDateString));
			}
			else if(dailyReportDateString.length() == 7){
				dailyReportDateString = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("MM/d/yy").parse(dailyReportDateString));
			}
			else{
				dailyReportDateString = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("MM/dd/yyyy").parse(dailyReportDateString));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return dailyReportDateString;
	}
	
	public static long getFormatedDate(String dateStr){
		
		try {
			if(dateStr.length() == 8){
				return new SimpleDateFormat("MM/dd/yy").parse(dateStr).getTime();
			}
			else if(dateStr.length() == 7){
				return new SimpleDateFormat("MM/dd/yyyy").parse(dateStr).getTime();
			}
			else{
				return new SimpleDateFormat("MM/dd/yyyy").parse(dateStr).getTime();
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	/*to get offset for local time zone */
	public static Long getOffset()
	{
		TimeZone timeZone = TimeZone.getDefault();
		
		return (long)timeZone.getRawOffset();
	}

    public static long getPatientTimeZoneOffset(long patientId){
        Patient patient = new PatientDAO().getPatientById(patientId);
        User user = patient.getUser();
        return user.getLogTimeOffset();
    }

	public static String getPatientOffsetKey(long patientId){
		Patient patient = new PatientDAO().getPatientById(patientId);
		User user = patient.getUser();
		return user.getOffsetKey();
	}

    public static int getPatientFacilityOffset(long patientId){
        try {
            Patient patient = new PatientDAO().getPatientById(patientId);
            Facility facility = patient.getFacilities().get(0);
            return facility.getTimezoneOffsetMillis().intValue();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return TimeZone.getDefault().getRawOffset();
    }

	public static String getCalculatedSize(String dataValue, String unit){
		
		if(dataValue != null && !dataValue.equalsIgnoreCase("") &&
				unit != null && !unit.equalsIgnoreCase("")){
			Float number = Float.parseFloat(dataValue) * Float.parseFloat(unit);
			return number.toString().replaceAll("\\.?0*$", "");
		}
		else if(dataValue != null && !dataValue.equalsIgnoreCase("")){
			return dataValue.toString().replaceAll("\\.?0*$", "");
		}
		return "0";
	}
	
	public static String getStringFromFloat(Float value){
		return value.toString().replaceAll("\\.?0*$", "");
	}
	
	public static TimeZone getTimezoneByOffset(String[] timeZones, String offsetKey) {
		TimeZone timeZoneName = TimeZone.getTimeZone("GMT");
		for (String timeZone : timeZones) {
		  TimeZone tz = TimeZone.getTimeZone(timeZone);
		  
		  if(timeZone.equalsIgnoreCase(offsetKey) ) {
			  timeZoneName = tz;
			  break;
		  }		  
		}
		
		return timeZoneName;
	}
	
	public static String timeInUserTimezone(long logTime, long offset, String offsetKey) {
		
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a z");
			
			TimeZone timeZone = getTimezoneByOffset(TimeZone.getAvailableIDs(), offsetKey);
			Calendar cal = new GregorianCalendar();
			cal.setTimeInMillis(logTime);		
			//cal.setTimeZone(timeZone);						
			
			sdf.setTimeZone(timeZone);
			return sdf.format(cal.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return "";
	}

    public static String getOnlyTimeInUserTimeZone(long logTime, long offset, String offsetKey) {

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");

            TimeZone timeZone = getTimezoneByOffset(TimeZone.getAvailableIDs(), offsetKey);
            Calendar cal = new GregorianCalendar();
            cal.setTimeInMillis(logTime);
            //cal.setTimeZone(timeZone);

            sdf.setTimeZone(timeZone);
            return sdf.format(cal.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

	public static List<Log> getUniqueResults(List<Log> logs){
		
		List<Log> resultLogs = new ArrayList<Log>();
		for(Log log: logs){
			
			/*Map<Long, LogMood> uniqueMoods = new LinkedHashMap<>();
			List<Mood> moods = new ArrayList<>();
			for(LogMood logMood: log.getLogMoods()){
				
				if(!uniqueMoods.containsKey(logMood.getMood().getMoodId())){
					uniqueMoods.put(logMood.getMood().getMoodId(), logMood);
					moods.add(logMood.getMood());
				}
			}
			
			log.setLogMoods(moods);*/
			
			Map<Long, ExerciseLog> uniqueExerciseLogs = new LinkedHashMap<Long, ExerciseLog>();
			List<ExerciseLog> exerciseLogs = new ArrayList<ExerciseLog>();
			for(ExerciseLog exerciseLog: log.getExerciseLogs()){
				
				if(!uniqueExerciseLogs.containsKey(exerciseLog.getExerciseLogId())){
					uniqueExerciseLogs.put(exerciseLog.getExerciseLogId(), exerciseLog);
					exerciseLogs.add(exerciseLog);
				}
			}
			
			log.setExerciseLogs(exerciseLogs);
			
			Map<Long, Notes> uniqueNotes = new LinkedHashMap<Long, Notes>();
			List<Notes> notes = new ArrayList<Notes>();
			for(Notes note: log.getNotes()){
				
				if(!uniqueNotes.containsKey(note.getNoteId())){
					uniqueNotes.put(note.getNoteId(), note);
					notes.add(note);
				}
			}
			
			log.setNotes(notes);
			
			if(log.getFoodLogSummary() != null){
				Map<Long, FoodLogDetail> uniqueFoodDetails = new LinkedHashMap<Long, FoodLogDetail>();
				List<FoodLogDetail> foodLogDetails = new ArrayList<FoodLogDetail>();
				for(FoodLogDetail detail: log.getFoodLogSummary().getFoodLogDetails()){
					
					if(!uniqueFoodDetails.containsKey(detail.getFoodLogDetailId())){
						uniqueFoodDetails.put(detail.getFoodLogDetailId(), detail);
						foodLogDetails.add(detail);
					}
				}
				
				log.getFoodLogSummary().setFoodLogDetails(foodLogDetails);
			}
			
			resultLogs.add(log);
		}
		
		return resultLogs;		
	}
		
	public static <T> Map<Long, String> listToMap(List<T> list){
		Map<Long, String> map = new HashMap<Long, String>();
		
		for (T t : list) {
			Object obj = (Object)t;
			
			if(obj instanceof Goal){
				Goal goal = (Goal)obj;
				map.put(goal.getGoalId(), goal.getName());
			}else if(obj instanceof ActionPlan){
				ActionPlan actionPlan = (ActionPlan)obj;
				map.put(actionPlan.getActionPlanId(), actionPlan.getName());
			}else if(obj instanceof GoalCategory){
				GoalCategory goalCategory = (GoalCategory)obj;
				map.put(goalCategory.getGoalCategoryId(), goalCategory.getName());
			}
		}
		
		return map;
	}

    public static <T> Map<Long, Object> listToMapObjects(List<T> list){
        Map<Long, Object> map = new HashMap<Long, Object>();

        for (T t : list) {
            Object obj = (Object)t;
            if (obj instanceof Provider){
                Provider provider = (Provider)obj;
                map.put(provider.getProviderId(), provider);
            }else if(obj instanceof GoalCategory){
                GoalCategory goalCategory = (GoalCategory)obj;
                map.put(goalCategory.getGoalCategoryId(), goalCategory);
            }
        }

        return map;
    }
	/* This method is to convert the response stream to String */
	public static String convertStreamToString(InputStream is) {
		@SuppressWarnings("resource")
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		String str = s.hasNext() ? s.next() : "";
		return str.trim();
	}
	
	// save uploaded file to new location
    public static void saveToFile(InputStream uploadedInputStream, String uploadedFileLocation) {
        try {
            OutputStream out = null;
            int read = 0;
            byte[] bytes = new byte[1024];
 
            out = new FileOutputStream(new File(uploadedFileLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
			LOG.info("Saved File at Location: "+uploadedFileLocation);
        } catch (IOException e) {
            e.printStackTrace();
			LOG.info("Saving File failed at Location: "+uploadedFileLocation+", with exception: "+e.getMessage());
        }
    }

    public static int getStatusFromElgg(String jsonResponse) {
    	JsonElement jelement = new JsonParser().parse(jsonResponse);
        JsonObject jobject = jelement.getAsJsonObject();
        int status = jobject.get(AppConstants.SOCIALKEYS.STATUS.getValue()).getAsInt();
        return status;
        
    }
    
    public static String readFile(String path, Charset encoding) throws IOException {
		  byte[] encoded = Files.readAllBytes(Paths.get(path));
		  return new String(encoded, encoding);
    }

    public static String removeBasicFromString(String credentials){
        String[] credArray = credentials.split(" ");
        return credArray[credArray.length -1];
    }

    public static String[] extractEmailFromHeader(String credentials){
        try {
            if(!ObjectUtils.isEmpty(credentials)) {
                credentials = removeBasicFromString(credentials);
                return decode(credentials);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    //authenticate user
  	public static Map<String, Object> authenticateUser(String credentials) {
  		long patientId = 0;
  		Map<String, Object> returnMap = new HashMap<String, Object>();
  		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.ACCESS_DENIED);
  		returnMap.put(AppConstants.JsonConstants.DATA.name(), patientId);
  		
  		UserDAO userDAO = new UserDAO();
  		try {
  			if(!ObjectUtils.isEmpty(credentials)){
                credentials = removeBasicFromString(credentials);
                String[] credsArray = decode(credentials);

				//ArrayIndexOutOfBoundsException: 1, fixation, [08/18/2015] __oz
				if(credsArray != null && credsArray.length > 0){
					String username = credsArray[0];
					String password = credsArray[1];

					User user = userDAO.getUserPatientByUsername(username);

					if(user == null){
						returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.ACCESS_NOT_FOUND);
					}else{
						if(BCrypt.checkpw(password, user.getPassword())){
							patientId = user.getPatient().getPatientId();
							returnMap.put(AppConstants.JsonConstants.DATA.name(), patientId);
							returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.ACCESS_GRANTED);
						}
					}
				}
  			}
  		} catch (Exception e) {
  			//e.printStackTrace();
  		}
  		return returnMap;
  	}

	public static Map<String, Object> authenticateCoach(String userName, String password) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.ACCESS_DENIED);

		UserDAO userDAO = new UserDAO();
		try {
			if(!ObjectUtils.isEmpty(userName) && !ObjectUtils.isEmpty(password)) {
				User user = userDAO.getUserProviderByUsername(userName);
				if (user != null) {
					if (BCrypt.checkpw(password, user.getPassword()) && !user.getUserType().equals(AppConstants.PATIENT)) {
						returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.ACCESS_GRANTED);
						returnMap.put(AppConstants.JsonConstants.DATA.name(), user);
					}
					else {
						returnMap.put(AppConstants.JsonConstants.REASON.name(), "Invalid username/password.");
					}
				}
				else {
					returnMap.put(AppConstants.JsonConstants.REASON.name(), "User not found.");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnMap;
	}
  	
    public static String[] decode(String auth) {

        auth = removeBasicFromString(auth);

		//Decode the Base64 into byte[]
		byte[] decodedBytes = Base64.decode(auth.getBytes());

		//If the decode fails in any case
		if (decodedBytes == null || decodedBytes.length == 0) {
			return null;
		}

		//Now we can convert the byte[] into a splitted array :
		//  - the first one is login,
		//  - the second one password
		return new String(decodedBytes).split(":");
	}

    public static String generateUUID(){
        return UUID.randomUUID().toString();
    }

    public static String formatPatientName(String firstName, String lastName, String displayName){

        String newName = "";

        if(!ObjectUtils.isEmpty(displayName)){
            newName = displayName;
            if(displayName.equalsIgnoreCase("null null")){
                newName = "";
            }
        }else{
            if(!ObjectUtils.isEmpty(firstName)){
                newName = firstName;
            }

            if(!ObjectUtils.isEmpty(lastName)){
                newName += " " + lastName.substring(0, 1).toUpperCase();
            }
        }

        return newName;
    }

	public static String formatPhoneNumber(String phone){

		String phoneNew = phone;
		if(!ObjectUtils.isEmpty(phone) && phone.indexOf("+1") < 0){
			phoneNew = "+1 " + phone;
		}

		return phoneNew;
	}

    public static boolean isValidPhoneNumber(String phone){

        if(!ObjectUtils.isEmpty(phone)){
            if(phone.contains("+92") && phone.startsWith("+92")){
                return true;
            }

            if(phone.contains("+1")){
                phone = phone.substring(phone.indexOf("+1")+2, phone.length()).trim();
            }

            String regex = "^\\(?([0-9]{3})\\)?[-.\\s]?([0-9]{3})[-.\\s]?([0-9]{4})$";
            Pattern pattern = Pattern.compile(regex);
            return pattern.matcher(phone).matches();
        }

        return false;
    }

    public static String getMobileAppVersionOfPatient(long patientId){
       return new ServiceFilterLogDAO().getLatestVersionNo(patientId);
    }

    public static String getDeviceTypeOfPatient(long patientId, String macAddress){
		Device device = new DeviceDAO().getDeviceByMac(macAddress);

        if(device != null){
			if(!ObjectUtils.isEmpty(device.getDeviceManufacturerAndOS())){
				return device.getDeviceManufacturerAndOS();
			}
            return ObjectUtils.nullSafe(device.getDeviceType());
        }

        return "N/A";
    }


    public static float[] getMealTargets(Target target) {
		float [] mealsTarget = {AppConstants.LOGS.BREAKFAST_TARGET.getIntValue(), AppConstants.LOGS.LUNCH_TARGET.getIntValue(),
				AppConstants.LOGS.SNAKS_TARGET.getIntValue(), AppConstants.LOGS.DINNER_TARGET.getIntValue()};
		if(target != null) {
			for (TargetMeal targetMeal : target.getMealTargets()) {
				if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.BREAKFAST.getValue())) {
					mealsTarget[0] = targetMeal.getCarbTarget();
				} else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.LUNCH.getValue())) {
					mealsTarget[1] = targetMeal.getCarbTarget();
				} else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.SNACK.getValue())) {
					mealsTarget[2] = targetMeal.getCarbTarget();
				} else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.DINNER.getValue())) {
					mealsTarget[3] = targetMeal.getCarbTarget();
				}
			}
		}
		return mealsTarget;
	}

    public static Map<String, Integer> getMealTargetsWithKeys(Target target) {
        Map<String, Integer> mealTargets = new LinkedHashMap<String, Integer>();

        mealTargets.put(AppConstants.TYPEKEYS.BREAKFAST.getValue(), AppConstants.LOGS.BREAKFAST_TARGET.getIntValue());
        mealTargets.put(AppConstants.TYPEKEYS.LUNCH.getValue(), AppConstants.LOGS.LUNCH_TARGET.getIntValue());
        mealTargets.put(AppConstants.TYPEKEYS.SNACK.getValue(), AppConstants.LOGS.SNAKS_TARGET.getIntValue());
        mealTargets.put(AppConstants.TYPEKEYS.DINNER.getValue(), AppConstants.LOGS.DINNER_TARGET.getIntValue());

        if(target != null) {
            for (TargetMeal targetMeal : target.getMealTargets()) {
				int carbTarget = removeTrailingZeros(targetMeal.getCarbTarget());
                if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.BREAKFAST.getValue())) {
                    mealTargets.put(AppConstants.TYPEKEYS.BREAKFAST.getValue(), carbTarget);
                } else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.LUNCH.getValue())) {
                    mealTargets.put(AppConstants.TYPEKEYS.LUNCH.getValue(), carbTarget);
                } else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.SNACK.getValue())) {
                    mealTargets.put(AppConstants.TYPEKEYS.SNACK.getValue(), carbTarget);
                } else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.DINNER.getValue())) {
                    mealTargets.put(AppConstants.TYPEKEYS.DINNER.getValue(), carbTarget);
                }
            }
        }
        return mealTargets;
    }

	public static Map<String, String> getMealTargetsWithKeysAndTime(Target target) {
		Map<String, String> mealTargets = new LinkedHashMap<String, String>();

		if(target != null) {
			for (TargetMeal targetMeal : target.getMealTargets()) {
				String mealTime = targetMeal.getMealTime();
				if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.BREAKFAST.getValue())) {
					mealTargets.put(AppConstants.TYPEKEYS.BREAKFAST.getValue(), mealTime);
				} else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.LUNCH.getValue())) {
					mealTargets.put(AppConstants.TYPEKEYS.LUNCH.getValue(), mealTime);
				} else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.SNACK.getValue())) {
					mealTargets.put(AppConstants.TYPEKEYS.SNACK.getValue(), mealTime);
				} else if(targetMeal.getMealType().equalsIgnoreCase(AppConstants.TYPEKEYS.DINNER.getValue())) {
					mealTargets.put(AppConstants.TYPEKEYS.DINNER.getValue(), mealTime);
				}
			}
		}
		return mealTargets;
	}

	public static int removeTrailingZeros(float myValue){
		BigDecimal myDecimal = new BigDecimal(myValue);
		myDecimal.stripTrailingZeros();
		Integer intV = myDecimal.intValue();
		return intV;
	}

    public static String buildActionPlanString(Map<String, String> details, String actionPlan, String planFrequency, String calendarFrequency, String meals, String other){

        String mealsString = "";

        if(!ObjectUtils.isEmpty(meals)){
            mealsString =  " when " + meals;
        }

        if(ObjectUtils.isEmpty(calendarFrequency)){
            calendarFrequency = "";
        }

        String actionPlanString = actionPlan;

        if(actionPlan.equalsIgnoreCase("Create my Own")){
            actionPlanString = details.get("actionPlanOthers");
        }

		String returnString = actionPlanString + " " + planFrequency;
		if(!calendarFrequency.equalsIgnoreCase("in the coming week")) {
			returnString += " per " + calendarFrequency + " " + mealsString;
		} else {
			returnString += " " + calendarFrequency + " " + mealsString;
		}

        return returnString;
    }

    public static Map<String, String> buildGoalDetailsMap(List<FormData> dataList){
        Map<String, String> goalDetails = new LinkedHashMap<String, String>();

        for (FormData formData : dataList) {
            goalDetails.put(formData.getFieldName(), formData.getFieldValue());
        }

        return goalDetails;
    }

	public static List<Provider> getFoodCoachesList(List<Provider> providers) {
		List<Provider> providersList =  new ArrayList<Provider>();
		if(providers != null && providers.size() > 0) {
			for (Provider provider: providers) {
				if(provider.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.FOOD_COACH.getValue())) {
					providersList.add(provider);
				}
			}
		}
		return providersList;
	}

	public static Provider getLeadCoach(Patient patient) {
		Provider provider = null;
		if(patient != null) {
			long leadCoachId = ObjectUtils.nullSafeLong(patient.getLeadCoachId());
			ProviderDAO providerDAO = new ProviderDAO();
			provider = providerDAO.getProviderById(leadCoachId);
		}
		return provider;
	}

	public static Provider getPrimaryFoodCoach(Patient patient) {
		Provider provider = null;
		if(patient != null) {
			long primaryFoodCoachId = ObjectUtils.nullSafeLong(patient.getPrimaryFoodCoachId());
			ProviderDAO providerDAO = new ProviderDAO();
			provider = providerDAO.getProviderById(primaryFoodCoachId);
		}
		return provider;
	}

	public static List<FoodLogDetail> removeDuplicateFoodDetails(List<FoodLogDetail> foodLogSummaryDetails){
		List<FoodLogDetail> newList = new ArrayList<FoodLogDetail>();
		HashMap<Long, FoodLogDetail> list = new LinkedHashMap<Long, FoodLogDetail>();

		if(foodLogSummaryDetails != null && foodLogSummaryDetails.size() > 0){
			for(FoodLogDetail foodLogDetail: foodLogSummaryDetails){
				FoodMaster foodMaster = foodLogDetail.getFoodMaster();
				if(!list.containsKey(foodMaster.getFoodMasterId())){
					list.put(foodMaster.getFoodMasterId(), foodLogDetail);
				}
			}
		}

		if(list != null && list.size() > 0){
			for(long key: list.keySet()){
				newList.add(list.get(key));
			}
		}

		return newList;
	}

	public static FoodMaster addNewFoodMaster(String foodName, float fCarbs, float fFats, float fProteins, String servingUnit){
		FoodMasterDAO foodMasterDAO = new FoodMasterDAO();
		FoodMaster foodMaster = foodMasterDAO.getFoodMasterByName(foodName);

		if(foodMaster == null){
			foodMaster = new FoodMaster();
		}
		foodMaster.setFoodName(foodName);
		foodMaster.setCarbs(fCarbs);
		foodMaster.setFats(fFats);
		foodMaster.setProtein(fProteins);
		foodMaster.setServingSizeUnit(servingUnit);
		foodMaster.setIsCustom(true);
		foodMasterDAO.save(foodMaster);

		return foodMaster;
	}

	public static Facility getRecruitmentFacility(){
		String facilityId = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.RECRUITMENT_FACILITY.getValue());
		try{
			long facId = Long.parseLong(facilityId);
			return new FacilityDAO().getFacilityWithId(facId);
		}catch (Exception e){
			e.printStackTrace();
		}

		return null;
	}

	public static String extractDataFromRequest(String data, HttpServletRequest httpRequest){
		if(!ObjectUtils.isEmpty(data)){
			return data;
		}

		return httpRequest.getParameter("data");
	}

    public static boolean saveImageToDisc(String folderName, String imageName, String imageContent){
        try {
            byte[] btDataFile = new sun.misc.BASE64Decoder().decodeBuffer(imageContent);

            File fileDir = new File(folderName);
            if (!fileDir.exists()){
                fileDir.mkdir();
            }
            String uploadedFileLocation = fileDir.getAbsolutePath()+ "\\" + imageName;
            File of = new File(uploadedFileLocation);
            FileOutputStream osf = new FileOutputStream(of);
            osf.write(btDataFile);
            osf.flush();

            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteFileFromDisc(String folderName, String imageName){
        try {
            File fileDir = new File(folderName);
            String uploadedFileLocation = fileDir.getAbsolutePath()+ "\\" + imageName;
            File of = new File(uploadedFileLocation);
            if(of.exists()){
                of.delete();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteFileFromDisc(String imageName){
        try {
            File of = new File(imageName);
            if(of.exists()){
                of.delete();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    //returns the list of coaches, if logged in coach is simple Coach Or Food Coach then the leadCoach/PrimaryFoodCoach if set
    //Or, if logged in coach is leadCoach then returns primary food coach or vice versa
    public static List<Provider> getCoachesToNotify(Provider loggedInProvider, Patient patient){
        List<Provider> coachesList = new ArrayList<Provider>();
        try {
            ProviderDAO providerDAO = new ProviderDAO();
            Long leadCoachId = ObjectUtils.nullSafeLong(patient.getLeadCoachId());
            Long primaryFoodCoachId = ObjectUtils.nullSafeLong(patient.getPrimaryFoodCoachId());
            Provider leadCoachProvider = providerDAO.getProviderById(leadCoachId);
            Provider foodCoachProvider = providerDAO.getProviderById(primaryFoodCoachId);

            if(loggedInProvider.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.COACH.getValue())
					|| loggedInProvider.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.FOOD_COACH.getValue()) ) {

                if(leadCoachProvider != null && loggedInProvider.getProviderId() != leadCoachProvider.getProviderId()) {
                    coachesList.add(leadCoachProvider);
                }

                if(foodCoachProvider != null && loggedInProvider.getProviderId() != foodCoachProvider.getProviderId()) {
                    coachesList.add(foodCoachProvider);
                }
            }else{
                //means, lead coach is logged in currently
                if(loggedInProvider.getProviderId() == leadCoachProvider.getProviderId()){
                    //then send email to primary food coach if set
                    if(foodCoachProvider != null){
                        coachesList.add(foodCoachProvider);
                    }
                    //means, primary food coach is logged in currently
                }else if(loggedInProvider.getProviderId() == foodCoachProvider.getProviderId()){
                    //then send email to lead coach if set
                    if(leadCoachProvider != null){
                        coachesList.add(leadCoachProvider);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return coachesList;
    }

    public static List<Provider> excludedLeadOrPrimaryFoodCoach(Provider loggedInProvider, Patient patient){
        List<Provider> coachesList = new ArrayList<Provider>();

        try {

            ProviderDAO providerDAO = new ProviderDAO();
            Long leadCoachId = ObjectUtils.nullSafeLong(patient.getLeadCoachId());
            Long primaryFoodCoachId = ObjectUtils.nullSafeLong(patient.getPrimaryFoodCoachId());
            Provider leadCoachProvider = providerDAO.getProviderById(leadCoachId);
            Provider foodCoachProvider = providerDAO.getProviderById(primaryFoodCoachId);

            List<Provider> patientProviders = patient.getProviders();

            patientProviders.remove(leadCoachProvider);
            patientProviders.remove(loggedInProvider);

            if(foodCoachProvider != null){
                patientProviders.remove(foodCoachProvider);
            }

            for (Provider provider: patientProviders){
                if(provider.getProviderId() != loggedInProvider.getProviderId() && !ObjectUtils.nullSafeBoolean(provider.getIsDeleted()) && !ObjectUtils.nullSafeBoolean(provider.getIsAccessAll())){
                    coachesList.add(provider);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return coachesList;
    }

    public static String listToCommaSeparatedString(List<String> strings, String delimeter){
        try {
            return Joiner.on(delimeter).join(strings);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static List<String> stringToList(String string){
        try {
            return Arrays.asList(string.split("\\s*,\\s*"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

	public static InternetAddress[] stringToInternetAddressArray(String string) {
		String[] emails = string.split(",");
		InternetAddress[] internetAddresses = new InternetAddress[emails.length];
		try {
			for (int i = 0; i < emails.length; i++) {
				internetAddresses[i] = new InternetAddress(emails[i]);
			}
		} catch (AddressException e) {
			e.printStackTrace();
		}
		return internetAddresses;
	}

    public static void addEmailTokenToDB(String userName, String token){
        EmailToken emailToken = new EmailToken();

        try {
            emailToken.setDateCreated(new Date());
            emailToken.setUsername(userName);
            emailToken.setToken(token);
            emailToken.setUsed(true);

            new BaseDAO().save(emailToken);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getEncodedUrlString(String stringToEncode){
        String returnedString = "";
        try {
            returnedString = URLEncoder.encode(new EncryptionUtils().encrypt(stringToEncode + ""), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            try {
                returnedString = new EncryptionUtils().encrypt(stringToEncode + "");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return returnedString;
    }

    public static String flushEmail(String email){
        Random r = new Random(System.currentTimeMillis());
        int random = (1 + r.nextInt(2)) * 10000 + r.nextInt(10000);

        email = "_"+random+"_"+email;

        return email;
    }

	public static List<ActivityLog> addFacilityTimeOffsetInActivityLog(List<ActivityLog> activityLogList, long patientId){
		Patient patient = new PatientDAO().getPatientById(patientId);
		long offsetTime = CommonUtils.getPatientFacilityTimezoneOffset(patient);
		for (int index = 0; index < activityLogList.size(); index++) {
			Log logActivity = activityLogList.get(index).getLog();
			long fromLogTime = logActivity.getCreatedOn();
			fromLogTime = fromLogTime - logActivity.getLogTimeOffset();
			activityLogList.get(index).setTimestamp(fromLogTime + offsetTime);
		}
		return activityLogList;
	}

	public static List<Object> addFacilityTimeOffset(List<Object> activityLogList, long patientId){
		List<Object> adjustedList = new ArrayList<Object>();
		Patient patient = new PatientDAO().getPatientById(patientId);
		long facilityOffsetTime = CommonUtils.getPatientFacilityTimezoneOffset(patient);
		for (Object obj : activityLogList) {
			Object[] objArray = (Object[]) obj;
			long objOffset = 0l;
			try {
				objOffset = ((BigInteger) objArray[3]).longValue();
			} catch (Exception e){

			}
			long objTimestamp = Long.parseLong(objArray[1].toString());
			//objArray[1] = objTimestamp + facilityOffsetTime; osama commented
			adjustedList.add(objArray);
		}
		return adjustedList;
	}

	public static List<Object> adjustMisfitActivityLabelsByDayTime(List<Object> activityLogList, long patientId){
		List<Object> adjustedList = new ArrayList<Object>();
		Patient patient = new PatientDAO().getPatientById(patientId);
		long facilityOffsetTime = CommonUtils.getPatientFacilityTimezoneOffset(patient);
		for (Object obj : activityLogList) {
			Object[] objArray = (Object[]) obj;
			long objOffset = 0l;
			try {
				objOffset = ((BigInteger) objArray[3]).longValue();
			} catch (Exception e){

			}
			long objTimestamp = Long.parseLong(objArray[1].toString());
			objTimestamp = objTimestamp - objOffset;
			objArray[1] = objTimestamp;
			adjustedList.add(objArray);
		}
		return adjustedList;
	}

    public static String getMemberDashboardLink(HttpServletRequest request, String uuid){
        return ObjectUtils.getApplicationUrl(request) + "/app/educator/dashboard.action?token=" + uuid;
    }

    public static void sendAppLinks(Patient patient){
        try {
            String appLinkAndroid = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.PLAYSTORE_APP_URL.getValue());
            String appLinkIOS = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.APPLESTORE_APP_URL.getValue());
            NotifyUtils.notifyMemberOfAppUrl(patient, appLinkAndroid, appLinkIOS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	public static long getPatientFacilityTimezoneOffset(Patient patient) {
		User patientUser = patient.getUser();
		long offsetTime;
		if ( patient.getFacilities() != null && patient.getFacilities().size() > 0 && patient.getFacilities().get(0).getTimezoneOffsetMillis() != null ) {
			offsetTime = patient.getFacilities().get(0).getTimezoneOffsetMillis();
		}
		else {
			offsetTime = patientUser.getLogTimeOffset();
		}
		return  offsetTime;
	}

	public static String getMealTypeByTime(Map<String, Long> mealTimeMap, long logTime) {
		String mealType = null;
		if (logTime >= DateUtils.getStartOfDayTime(mealTimeMap.get("BreakfastTime")) && logTime <= mealTimeMap.get("BreakfastTime")) {
			mealType = "Breakfast";
		} else if (logTime >= mealTimeMap.get("BreakfastTime") && logTime <= mealTimeMap.get("LunchTime")) {
			mealType = "Lunch";
		} else if (logTime >= mealTimeMap.get("LunchTime") && logTime <= mealTimeMap.get("SnackTime")) {
			mealType = "Snack";
		} else if (logTime >= mealTimeMap.get("SnackTime") && logTime <= mealTimeMap.get("DinnerTime")) {
			mealType = "Dinner";
		} else {
			mealType = "Beverage";
		}
		return mealType;
	}

    public static RequestedEmailServer extractEmailAndServerFromHashedString(String email){
        int hashIndex = email.indexOf("#");
        RequestedEmailServer requestedEmailServer = null;

        try {

            //extracting #serverName from email
            String serverName = email.substring(hashIndex + 1, email.length());

            //removing # from serverName
            serverName = serverName.replaceAll("#", "").trim();

            //removing # from email
            email = email.substring(0, hashIndex).trim();

            requestedEmailServer = new RequestedEmailServer(email, serverName);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return requestedEmailServer;
    }

	public static void savePatientDetails(PatientDetailDTO patientDetailDTO){

		PatientDAO patientDAO = new PatientDAO();
		BaseDAO baseDAO = new BaseDAO();
		PatientDiabDietMedDAO patientDiabDietMedDAO = new PatientDiabDietMedDAO();

		patientDiabDietMedDAO.removeDiabetesInfoByPatientId(patientDetailDTO.getPatientId());
		patientDiabDietMedDAO.removeDietaryInfoByPatientId(patientDetailDTO.getPatientId());
		patientDiabDietMedDAO.removeMedicationByPatientId(patientDetailDTO.getPatientId());

		Patient patient = patientDAO.getPatientById(patientDetailDTO.getPatientId());

		User user = patient.getUser();

		user.setFirstName(patientDetailDTO.getFirstName());
		user.setLastName(patientDetailDTO.getLastName());
		user.setEmail(patientDetailDTO.getEmail());
		user.setPhone(patientDetailDTO.getPhone());

		//Demographics
		patient.setDob(new Date(patientDetailDTO.getDob()));
		patient.setWeight(patientDetailDTO.getWeight());
		patient.setGender(patientDetailDTO.getGender());
		patient.setNickname(patientDetailDTO.getNickname());
		patient.setRace(patientDetailDTO.getRace());
		patient.setState(patientDetailDTO.getState());
		patient.setMrn(patientDetailDTO.getMrn());
		patient.setEducation(patientDetailDTO.getEducation());
		patient.setLanguage(patientDetailDTO.getLanguage());
		//patient.setImagePath(patientDetailDTO.getImagePath());

		//Diabetes info
		PatientDiabetesInfo diabetesInfo = new PatientDiabetesInfo();
		diabetesInfo.setPatient(patient);
		diabetesInfo.setDiagnosis(patientDetailDTO.getDiagnosis());
		diabetesInfo.setTreatedDepression(patientDetailDTO.getTreatedDepression());
		diabetesInfo.setDiagnosisDate(patientDetailDTO.getDiagnosisDate());

		diabetesInfo.setDiabetesSupport(patientDetailDTO.getDiabetesSupport());
		diabetesInfo.setTimeSinceLastDiabetesEdu(patientDetailDTO.getTimeSinceLastDiabetesEdu());

		//Medications
		PatientMedication medication = new PatientMedication();
		medication.setPatient(patient);
		if(!ObjectUtils.isEmpty(patientDetailDTO.getMedications())){
			medication.setMedicationsName(patientDetailDTO.getMedications());
			baseDAO.save(medication);
		}

		//Dietary info
		PatientDietaryInfo dietaryInfo = new PatientDietaryInfo();
		dietaryInfo.setPatient(patient);
		dietaryInfo.setDietaryRestrictions(patientDetailDTO.getDietaryRestrictions());
		dietaryInfo.setMealPreparer(patientDetailDTO.getMealPreparer());
		dietaryInfo.setDietaryRestrictionsOther(ObjectUtils.nullSafe(patientDetailDTO.getDietaryRestrictionsOther()));

		baseDAO.save(diabetesInfo);
		baseDAO.save(dietaryInfo);
		baseDAO.save(patient);
	}

	public static PatientDetailDTO convertToPatientDTO(Patient patient){

		PatientDetailDTO patientDetailDTO = new PatientDetailDTO();

		User user = patient.getUser();
		patientDetailDTO.setPatientId(patient.getPatientId());

		//Demographics
		patientDetailDTO.setFirstName(ObjectUtils.nullSafe(user.getFirstName()));
		patientDetailDTO.setLastName(ObjectUtils.nullSafe(user.getLastName()));
		patientDetailDTO.setDisplayName(ObjectUtils.nullSafe(user.getDisplayName()));
		patientDetailDTO.setEmail(ObjectUtils.nullSafe(user.getEmail()));
		patientDetailDTO.setPhone(ObjectUtils.nullSafe(user.getPhone()));

		if(patient.getDob() != null){
			patientDetailDTO.setDob(DateUtils.getFormattedDate(patient.getDob().getTime(), DateUtils.DATE_FORMAT));
		}

		if(patient.getWeight() != null){
			patientDetailDTO.setWeight(patient.getWeight());
		}

		String gender = ObjectUtils.nullSafe(patient.getGender());
		if(ObjectUtils.isEmpty(gender)){
			gender = user.getGender();
		}
		patientDetailDTO.setGender(gender);
		patientDetailDTO.setNickname(ObjectUtils.nullSafe(patient.getNickname()));
		patientDetailDTO.setRace(ObjectUtils.nullSafe(patient.getRace()));
		patientDetailDTO.setState((patient.getState() == null)?user.getState(): patient.getState() );
		patientDetailDTO.setMrn(ObjectUtils.nullSafe(patient.getMrn()));
		patientDetailDTO.setEducation(ObjectUtils.nullSafe(patient.getEducation()));
		patientDetailDTO.setImagePath(ObjectUtils.nullSafe(patient.getImagePath()));
		patientDetailDTO.setLanguage(ObjectUtils.nullSafe(patient.getLanguage()));

		//Diabetes info
		PatientDiabetesInfo diabetesInfo = patient.getDiabetesInfo();
		if(diabetesInfo != null){
			patientDetailDTO.setDiagnosis(diabetesInfo.getDiagnosis());
			patientDetailDTO.setDiagnosisDate(ObjectUtils.nullSafe(diabetesInfo.getDiagnosisDate()));
			patientDetailDTO.setDiabetesSupport(diabetesInfo.getDiabetesSupport());
			patientDetailDTO.setTreatedDepression(diabetesInfo.getTreatedDepression());
			patientDetailDTO.setTimeSinceLastDiabetesEdu(diabetesInfo.getTimeSinceLastDiabetesEdu());
		}

		//Medications
        /*PatientMedication medication = patient.getMedication();
        if(medication != null){
            patientDetailDTO.setMedications(medication.getMedicationsName());
        }*/

		//Dietary info
		PatientDietaryInfo dietaryInfo = patient.getDietaryInfo();
		if(diabetesInfo != null){
			patientDetailDTO.setDietaryRestrictions(dietaryInfo.getDietaryRestrictions());
			patientDetailDTO.setMealPreparer(dietaryInfo.getMealPreparer());
			patientDetailDTO.setDietaryRestrictionsOther(ObjectUtils.nullSafe(dietaryInfo.getDietaryRestrictionsOther()));
		}

		List<Facility> facilities = patient.getFacilities();
		if(facilities != null && facilities.size() > 0){
			patientDetailDTO.setFacilityId(facilities.get(0).getFacilityId());
		}

		return patientDetailDTO;
	}

	public static Target getDefaultTarget(Patient patient) {
		Target target = new Target();
		target.setActivitySteps(7000);
		target.setGlucoseMax(180);
		target.setGlucoseMax2(130);
		target.setGlucoseMin(80);
		target.setGlucoseMin2(80);

		//[2/23/2015]: set starting/target weight to 0F , __oz
		target.setStartingWeight(0F);
		target.setTargetWeight(0F);

		target.setWeightFrequency("Once/Week");
		target.setSource(AppConstants.PATIENT);

		target.setPatient(patient);

		TargetMeal breakfastTargetMeal = new TargetMeal();
		breakfastTargetMeal.setMealTime("07:00 AM");
		breakfastTargetMeal.setMealType(AppConstants.MEAL_TYPE_BREAKFAST);
		breakfastTargetMeal.setCarbTarget(0);
		breakfastTargetMeal.setTarget(target);

		TargetMeal lunchTargetMeal = new TargetMeal();
		lunchTargetMeal.setMealTime("11:30 AM");
		lunchTargetMeal.setMealType(AppConstants.MEAL_TYPE_LUNCH);
		lunchTargetMeal.setCarbTarget(0);
		lunchTargetMeal.setTarget(target);

		TargetMeal snackTargetMeal = new TargetMeal();
		snackTargetMeal.setMealTime("03:30 PM");
		snackTargetMeal.setMealType(AppConstants.MEAL_TYPE_SNACK);
		snackTargetMeal.setCarbTarget(0);
		snackTargetMeal.setTarget(target);

		TargetMeal dinnerTargetMeal = new TargetMeal();
		dinnerTargetMeal.setMealTime("05:30 PM");
		dinnerTargetMeal.setMealType(AppConstants.MEAL_TYPE_DINNER);
		dinnerTargetMeal.setCarbTarget(0);
		dinnerTargetMeal.setTarget(target);

		List<TargetMeal> mealsTarget = new ArrayList<TargetMeal>();
		mealsTarget.add(breakfastTargetMeal);
		mealsTarget.add(lunchTargetMeal);
		mealsTarget.add(snackTargetMeal);
		mealsTarget.add(dinnerTargetMeal);

		target.setMealTargets(mealsTarget);

		/*TargetMedication medicationTarget =  new TargetMedication();
        medicationTarget.setShouldReminded(false);
		medicationTarget.setTime("07:00 am");
		medicationTarget.setTarget(target);

		List<TargetMedication> medicationTargets = new ArrayList<>();
		medicationTargets.add(medicationTarget);
		target.setMedTargets(medicationTargets);*/

		Reminder reminder = new Reminder();
		reminder.setIsGlucoseTesting(false);
		reminder.setIsGoalActionPlan(false);
		reminder.setIsMealTime(false);
		reminder.setIsNewTopicAvailable(false);
		reminder.setIsWeighingMyself(false);
		reminder.setTarget(target);
		target.setReminder(reminder);
		return target;
	}

	public static void saveAccessDeniedLog(HttpServletRequest request, long patientId){
		try {
			ServiceFilterLog logging = new ServiceFilterLog();
			logging.setService(ActionContext.getContext().getName() + ": this member is not assigned to the coach.");
			logging.setStatus(AppConstants.ACCESS_DENIED);
			logging.setTimestamp(System.currentTimeMillis());
			logging.setIpAddress(request.getRemoteAddr());
			logging.setPatientId(patientId);
			new BaseDAO().save(logging);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<Long> getLongListIdsFromStringListIds(List<String> ids) {
		List<Long> longIds = new ArrayList<Long>();

		for (String id : ids) {
			longIds.add(Long.parseLong(id));
		}
		return longIds;
	}

	public static List<Long> getLongListIdsFromStringArrayIds(String[] ids) {
		List<Long> longIds = new ArrayList<Long>();

		for (String id : ids) {
			longIds.add(Long.parseLong(id));
		}
		return longIds;
	}

	public static List<Provider> getExcludedSuperFacilityProviders(List<Provider> providers, boolean isIncluded) {
		List<Provider> excludedProviders = new ArrayList<Provider>();
		long superFacilityId = new FacilityDAO().getAtoZFacility().getFacilityId();

		for (Provider p : providers) {

			if (isIncluded) {
				if(p.getFacility().getFacilityId() == superFacilityId && !ObjectUtils.nullSafeBoolean(p.getIsDeleted())){
					excludedProviders.add(p);
				}
			} else if (!(p.getFacility().getFacilityId() == superFacilityId) &&
					!(p.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.FACILITY_ADMIN.getValue()))
					&& !ObjectUtils.nullSafeBoolean(p.getIsDeleted())) {
				excludedProviders.add(p);
			}
		}
		return excludedProviders;
	}

	public static List<Provider> getProvidersFromString(String providersString) {
		List<Provider> providers;
		ProviderDAO providerDAO = new ProviderDAO();

		String stringProviderIds[] = providersString.split(", ");
		List<Long> providerIds = CommonUtils.getLongListIdsFromStringArrayIds(stringProviderIds);

		providers = providerDAO.getProvidersByIds(providerIds);

		return providers;
	}

	public static List<CoachDTO> getPrimaryCoachesDTOs(List<Provider> coachList){
		List<CoachDTO> primaryCoachList = new ArrayList<CoachDTO>();
		for(Provider p : coachList){
			User user = p.getUser();
			Facility facility = p.getFacility();

			if(user != null && p != null && facility != null){
				CoachDTO c = new CoachDTO(user.getUserId(), p.getProviderId(), user.getFirstName(), user.getLastName(), p.getType(), p.getPhone(),
						p.getEmail(), facility.getFacilityId(), facility.getName(), 0, "", p.getDesignation(), ObjectUtils.nullSafe(p.getIsSMSEnabled()), ObjectUtils.nullSafe(p.getIsEmailEnabled()),
						0);
				primaryCoachList.add(c);
			}
		}
		return primaryCoachList;
	}

	public static List<PatientDTO> buildPatientDTOsFromPatients(List<Patient> patients){
		List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

		for(Patient p : patients){
			PatientDTO patientDTO = new PatientDTO();
			User patientUser = p.getUser();
			patientDTO.setPatientId(p.getPatientId());
			patientDTO.setEmail(patientUser.getEmail());
			patientDTO.setFirstName(patientUser.getFirstName());
			patientDTO.setLastName(patientUser.getLastName());
			patientDTO.setUuid(p.getUuid());
			patientDTO.setFacilityId(p.getFacilities().get(0).getFacilityId());

			patientDTOs.add(patientDTO);
		}

		return patientDTOs;
	}

	public static List<String> getEmailListFromPatients(List<Patient> patients){
		List<String> emails = new ArrayList<String>();
		for(Patient p : patients){
			String email = p.getUser().getEmail();
			emails.add(email);
		}

		return emails;
	}

	public static void getAccessToken(String userName, String password,Map<String, Object> returnDataMap,@Context HttpServletRequest request) throws MalformedURLException {

		String token = null;
		String resquestedURL = request.getRequestURL().toString();
		URL url = new URL(resquestedURL);

		StringBuffer baseURL = new StringBuffer();

		if(url.getProtocol()!=null){
			baseURL=baseURL.append(url.getProtocol()).append("://");
			if (url.getHost()!=null) {
				baseURL=baseURL.append(url.getHost()).append(":");
				if (url.getPort()!=0){
					baseURL=baseURL.append(url.getPort());
				}
			}
		}
		String response = null;

		try {

			OAuthClient client = new OAuthClient(new URLConnectionClient());

			OAuthClientRequest oauthRequest = OAuthClientRequest.tokenLocation(baseURL + "/PatientApp/oauth/token")
					.setGrantType(GrantType.PASSWORD)
					.setClientId("my-trusted-client")
					.setUsername(userName)
					.setPassword(password)
					.buildQueryMessage();

			OAuthJSONAccessTokenResponse res = client.accessToken(oauthRequest, OAuthJSONAccessTokenResponse.class);
			token = client.accessToken(oauthRequest, OAuthJSONAccessTokenResponse.class).getAccessToken();
			String refreshToken = client.accessToken(oauthRequest, OAuthJSONAccessTokenResponse.class).getRefreshToken();
			LOG.info("OAuth Token : "+token);
			LOG.info("Refresh Token : "+refreshToken);
			returnDataMap.put("accessToken", token);
			returnDataMap.put("refreshToken", refreshToken);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("getAccessToken service: Exception: "+e.getMessage());
		}

		LOG.info("getAccessToken service: Returning response: "+response);
	}
}
