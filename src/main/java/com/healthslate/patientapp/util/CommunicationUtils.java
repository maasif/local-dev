package com.healthslate.patientapp.util;

import com.healthslate.patientapp.model.entity.Facility;
import org.springframework.security.crypto.codec.Base64;

public class CommunicationUtils {

	public static String getEmailTextForRegistration(String firstName, String id, String applicationUrl) {
		StringBuffer buff = new StringBuffer();
		buff.append("<html><body>")
			.append("<br/>")
			.append("Dear "+firstName + ",")
			.append("<br/>")
			.append("Thank you for creating your HealthSlate.com account by setting your password.")
			.append("<br/><br/>")
			.append("<a href=\""+applicationUrl+"/getMemberInfo.action?memberId="+new String(Base64.encode(id.getBytes()))+"\" >Please click this link to continue your registration.</a>")			
			.append("<br/><br/>")
			.append("If you need any help please call at 813-343-5805 (extension 1) or email <a href=\"support@healthslate.com\">support@healthslate.com</a>")
			.append("<br/><br/>")
			.append("Regards,")
			.append("<br/>")
			.append("The HealthSlate Team.")
			.append("<br/><br/>")
			.append("<span style=\"font-size: 12px\" >")
			.append("The HealthSlate tablet and website are for informational purposes only. ")
			.append("They are not a substitute for professional medical advice, diagnosis, or treatment. ")
			.append("Always seek the advice of a qualified health provider with any medical questions, and do not disregard professional medical advice. ")
			.append("A to Z Health and AADE do not recommend or endorse any specific products. ")
			.append("If you think you may have a medical emergency, call your doctor or 911 immediately.")
			.append("</span>")
			.append("</body></html>");
		
		return buff.toString();
	}
	
	public static String getSecondEmailTextForRegistration(String firstName) {
		StringBuffer buff = new StringBuffer();
		buff.append("<html><body>")
			.append("<br/>")
			.append("Dear "+firstName + ",")
			.append("<br/><br/>")
			.append("Thank you for creating your HealthSlate.com account & ordering your AADE HealthSlate.")
			.append("<br/><br/>")
			.append("Your credit card will be billed immediately and the charge will show up under ")
			.append("\"Informational Media Brands\", the parent company of HealthSlate.")
			.append("<br/><br/>")
			.append("We will do our best to start shipping the devices between August & September.")
			.append("If there any significant changes around this timing, we will notify you via email.")
			.append("<br/><br/>")
			.append("Since you have set up and paid for an account, you can start using a lot of the great content")
			.append(" we provide like videos in English and Spanish from our website  <a href=\"www.healthslate.com\">www.healthslate.com</a> ")
			.append("<br/><br/>")
			.append("Feel free to contact us directly at 813-343-5805 (extension 1) or <a href=\"info@healthslate.com\">info@healthslate.com</a> if you have any questions.")
			.append("<br/><br/>")
			.append("Regards,")
			.append("<br/>")
			.append("The HealthSlate Team.")
			.append("<br/><br/>")
			.append("<span style=\"font-size: 12px\" >")
			.append("The HealthSlate tablet and website are for informational purposes only. ")
			.append("They are not a substitute for professional medical advice, diagnosis, or treatment. ")
			.append("Always seek the advice of a qualified health provider with any medical questions, and do not disregard professional medical advice. ")
			.append("A to Z Health and AADE do not recommend or endorse any specific products. ")
			.append("If you think you may have a medical emergency, call your doctor or 911 immediately.")
			.append("</span>")
			.append("</body></html>");
		
		return buff.toString();
	}
	
	public static String getEmailTextForResetPassword(String username , String url) {
		
		StringBuffer buff = new StringBuffer();
		buff.append("<html><body>")
			.append("<br/>")
            .append("Hello,")
            .append("<br/><br/>")
            .append("You're receiving this e-mail because you forgot your HealthSlate Program account password. Please use the link below to reset your password: <br/>")
            .append("<a href=\"" + url + "\">" + url + "</a>")
            .append("<br/><br/>")
            .append("If you did not request a password reset, you may ignore this message.")
            .append("<br/><br/>")
			.append("Sincerely,")
			.append("<br/>")
			.append("The HealthSlate Team")
			.append("<br/><br/>")
			/*.append("<span style=\"font-size: 12px\" >")
			.append("The HealthSlate tablet and website are for informational purposes only. ")
			.append("They are not a substitute for professional medical advice, diagnosis, or treatment. ")
			.append("Always seek the advice of a qualified health provider with any medical questions, and do not disregard professional medical advice. ")
			.append("A to Z Health and AADE do not recommend or endorse any specific products. ")
			.append("If you think you may have a medical emergency, call your doctor or 911 immediately.")
			.append("</span>")*/
			.append("</body></html>");
		
		return buff.toString();
	}

    public static String getEmailTextForCreatePassword(String username , String url) {

        StringBuffer buff = new StringBuffer();
        buff.append("<html><body>")
                .append("<br/>")
                .append("Hello,")
                .append("<br/><br/>")
                .append("Please use the link below to create your password: <br/>")
                .append("<a href=\"" + url + "\">" + url + "</a>")
                .append("<br/><br/>")
                .append("Sincerely,")
                .append("<br/>")
                .append("The HealthSlate Team")
                .append("<br/><br/>")
                .append("</body></html>");

        return buff.toString();
    }

	public static String getEmailTextForFeedback(String url) {
		
		StringBuffer buff = new StringBuffer();
		buff.append("<html><body>")
			.append("<br/>")
            .append("Hello,")
            .append("<br/><br/>")
            .append("A HealthSlate user has sent feedback. Please use the link below to see the feedback: <br/>")
            .append("<a href=\"" + url + "\">" + url + "</a>")
            .append("<br/><br/>")
            .append("<br/><br/>")
			.append("Sincerely,")
			.append("<br/>")
			.append("The HealthSlate Team")
			.append("<br/><br/>")
			/*.append("<span style=\"font-size: 12px\" >")
			.append("The HealthSlate tablet and website are for informational purposes only. ")
			.append("They are not a substitute for professional medical advice, diagnosis, or treatment. ")
			.append("Always seek the advice of a qualified health provider with any medical questions, and do not disregard professional medical advice. ")
			.append("A to Z Health and AADE do not recommend or endorse any specific products. ")
			.append("If you think you may have a medical emergency, call your doctor or 911 immediately.")
			.append("</span>")*/
			.append("</body></html>");
		
		return buff.toString();
	}

    public static String getEmailTextForUnprocessedMeal(String username , String url) {

        StringBuffer buff = new StringBuffer();
        buff.append("<html><body>")
                .append("<br/>")
                .append("Hello " + username + ", ")
                .append("<br/><br/>")
                .append("You have received a new unprocessed meal log from member, Click below to view: <br/>")
                .append("<a href=\"" + url + "\">" + url + "</a>")
                .append("<br/><br/>")
                .append("Sincerely,")
                .append("<br/>")
                .append("The HealthSlate Team")
                .append("<br/><br/>")
			    .append("</body></html>");

        return buff.toString();
    }

    public static String getEmailTextForNewCoachNote(String username , String url) {

        StringBuffer buff = new StringBuffer();
        buff.append("<html><body>")
                .append("<br/>")
                .append("Hello " + username + ", ")
                .append("<br/><br/>")
                .append("New coach notes have been added for one of your members, click below to view: <br/>")
                .append("<a href=\"" + url + "\">" + url + "</a>")
                .append("<br/><br/>")
                .append("Sincerely,")
                .append("<br/>")
                .append("The HealthSlate Team")
                .append("<br/><br/>")
                .append("</body></html>");

        return buff.toString();
    }

	public static String getEmailTextForGoalAssement(String username , String url) {
		StringBuffer buff = new StringBuffer();
		buff.append("<html><body>")
				.append("<br/>")
				.append("Hello " + username + ", ")
				.append("<br/><br/>")
				.append("New goal assessment has been added for one of your members, click below to view: <br/>")
				.append("<a href=\"" + url + "\">" + url + "</a>")
				.append("<br/><br/>")
				.append("Sincerely,")
				.append("<br/>")
				.append("The HealthSlate Team")
				.append("<br/><br/>")
				.append("</body></html>");

		return buff.toString();
	}

	public static String getInvitationEmailText(String username, String facilityEmailText, String url) {

        StringBuffer buff = new StringBuffer();
        buff.append("<html><body>")
                .append("<br/>")
                .append("Hello " + username + ", ")
                .append("<br/><br/>")
                .append("Welcome to "+facilityEmailText+" pilot of the HealthSlate Program. Please review the consent form for the pilot at the link below. After you accept the consent form you will be sent an invite to download the HealthSlate App. <br/>")
                .append("<a href=\"" + url + "\">" + url + "</a>")
                .append("<br/><br/>")
                .append("Sincerely,")
                .append("<br/>")
                .append(facilityEmailText)
                .append("<br/><br/>")
                .append("</body></html>");

        return buff.toString();
    }

    public static String getEnrollEmailText(String appUrl , String url, Facility facility) {

        StringBuffer buff = new StringBuffer();
        String branding = facility.getName();
        String questionString = "Have questions about the program? Contact the diabetes program manager: <a href='mailto:info@healthslate.com' style='text-decoration:underline;'>info@healthslate.com</a>";

        //providence facility check
        if (facility != null
                && !ObjectUtils.isEmpty(facility.getFacilityLogo())
                && facility.getFacilityLogo().equalsIgnoreCase("providence_solutions_logo.png")) {

            questionString = "Have questions about the program? Contact the Providence diabetes program manager: <a href='mailto:kathy.schwab@providence.org' style='text-decoration:underline;'>kathy.schwab@providence.org</a>";
            branding = "Providence";
        }

        buff.append("<html><body>")
                .append("<table style='width:100%;margin-bottom:10px'><tbody><tr><td><div><img src='" + appUrl + "resources/css/images/recruitment_icons/" + facility.getFacilityLogo() + "' style='width:250px;height:54px;' width='250' height='54' /></div></td><td><div><img src='" + appUrl + "resources/css/images/recruitment_icons/logowhite.png' style='width:250px;height:45px;' width='250' height='45' /></div></td></tr></tbody></table>")
                .append("Congratulations! You are now enrolled in the mobile diabetes management pilot program sponsored by " + facility.getEmailTextName() + " and HealthSlate. This program gives you convenient access to diabetes information, tools, and diabetes experts - all from your smart phone.")
                .append("<br/><br/>")
                .append("Click <a href=\"" + url + "\" style='text-decoration:underline;'> here </a> to go to the " + branding + " enrollment page and get started.")
                .append("<br/><br/>")
                .append("Need technical help? Call HealthSlate tech support at <span style='text-decoration:underline;'>(888) 291-7245</span> between 6 am and 6 pm daily. <br/>")
                .append(questionString)
                .append("<br/><br/>")
                .append("Thank you for helping " + branding + " test this new smartphone app designed to allow you live the life you want, while managing your diabetes.")
                .append("<br/>")
                .append(facility.getEmailTextName() + " and HealthSlate.")
                .append("<br/><br/>")
                .append("</body></html>");

        return buff.toString();
    }

    public static String getPlayStoreAppLinkEmailText(String username , String urlAndroid, String urlIOS) {

        StringBuffer buff = new StringBuffer();
        buff.append("<html><body>")
                .append("<br/>")
                .append("Hello "+username+", ")
                .append("<br/><br/>")
                .append("Thank you for joining the HealthSlate Program. Please click below to download the Mobile App: <br/><br/><br/>")
                .append("For Android: <br/> <a href=\"" + urlAndroid + "\">" + urlAndroid + "</a><br/><br/><br/>")
                .append("For iOS: <br/> <a href=\"" + urlIOS + "\">" + urlIOS + "</a><br/><br/>")
                .append("<br/><br/>")
                .append("Sincerely,")
                .append("<br/>")
                .append("The HealthSlate Team")
                .append("<br/><br/>")
                .append("</body></html>");

        return buff.toString();
    }

    public static String getEmailTextForPatientMessage(String username , String url) {

        StringBuffer buff = new StringBuffer();
        buff.append("<html><body>")
                .append("<br/>")
                .append("Hello " + username + ", ")
                .append("<br/><br/>")
                .append("You have received new message from member, Click below to view: <br/>")
                .append("<a href=\"" + url + "\">" + url + "</a>")
                .append("<br/><br/>")
                .append("Sincerely,")
                .append("<br/>")
                .append("The HealthSlate Team")
                .append("<br/><br/>")
                .append("</body></html>");

        return buff.toString();
    }

	public static String getEmailTextForNewMember(String username , String url, String facilityName) {

		StringBuffer buff = new StringBuffer();
		buff.append("<html><body>")
				.append("<br/>")
				.append("Hello " + username + ", ")
				.append("<br/><br/>")
				.append("A new member has joined the HealthSlate Program at "+facilityName+". Within 24 hours please decide together with the other Food Coaches for that facility who will be the member's Primary Food Coach. That coach needs to<br/>a) assign her/himself the Primary Food Coach role in the portal, and<br/>b) introduce yourself to the member via in-app messaging 2 - 3 days from now.<br/><br/> Click below to view member details: <br/>")
				.append("<a href=\"" + url + "\">" + url + "</a>")
				.append("<br/><br/>")
				.append("Sincerely,")
				.append("<br/>")
				.append("The HealthSlate Team")
				.append("<br/><br/>")
				.append("</body></html>");

		return buff.toString();
	}

	public static String getEmailTextForLeadCoachOfNewMember(String username , String url, String facilityName) {

		StringBuffer buff = new StringBuffer();
		buff.append("<html><body>")
				.append("<br/>")
				.append("Hello " + username + ", ")
				.append("<br/><br/>")
				.append("You have a new HealthSlate member. Please contact the member within the next 24 hours to schedule their first 1:1, and attempt to conduct the 1:1 if possible within the coming week.<br/><br/> Click below to view member details: <br/>")
				.append("<a href=\"" + url + "\">" + url + "</a>")
				.append("<br/><br/>")
				.append("Sincerely,")
				.append("<br/>")
				.append("The HealthSlate Team")
				.append("<br/><br/>")
				.append("</body></html>");

		return buff.toString();
	}

    public static String getEmailTextForLeadCoachOfNewMember3Days(String username , String url, String facilityName) {

        StringBuffer buff = new StringBuffer();
        buff.append("<html><body>")
                .append("<br/>")
                .append("Hello " + username + ", ")
                .append("<br/><br/>")
                .append("Primary Food Coach must set the member's dinner carb ceiling within the next 24 hours.<br/><br/> Click below to view member details: <br/>")
                .append("<a href=\"" + url + "\">" + url + "</a>")
                .append("<br/><br/>")
                .append("Sincerely,")
                .append("<br/>")
                .append("The HealthSlate Team")
                .append("<br/><br/>")
                .append("</body></html>");

        return buff.toString();
    }

	public static String getReminderEmailTextForNewMember(String username , String url) {

		StringBuffer buff = new StringBuffer();
		buff.append("<html><body>")
				.append("<br/>")
				.append("Hello " + username + ", ")
				.append("<br/><br/>")
				.append("The new member you were assigned last week needs to have their first 1:1 with you and receive their first Suggested Meal this week.<br/><br/> Click below to view member details: <br/>")
				.append("<a href=\"" + url + "\">" + url + "</a>")
				.append("<br/><br/>")
				.append("Sincerely,")
				.append("<br/>")
				.append("The HealthSlate Team")
				.append("<br/><br/>")
				.append("</body></html>");

		return buff.toString();
	}

    public static String getEmailTextForQueue(String url) {
		
		StringBuffer buff = new StringBuffer();
		buff.append("<html><body>")
			.append("<br/>")
			.append("Click on the link below to view content from your appointment today: <br/>")
			.append(" <a href=\""+url+"\">"+url+"</a>")
			.append("<br/><br/>")
			.append("Regards,")
			.append("<br/>")
			.append("HealthSlate Team.")
			.append("<br/><br/>")
			.append("<span style=\"font-size: 12px\" >")
			.append("The HealthSlate tablet and website are for informational purposes only. ")
			.append("They are not a substitute for professional medical advice, diagnosis, or treatment. ")
			.append("Always seek the advice of a qualified health provider with any medical questions, and do not disregard professional medical advice. ")
			.append("A to Z Health and AADE do not recommend or endorse any specific products. ")
			.append("If you think you may have a medical emergency, call your doctor or 911 immediately.")
			.append("</span>")
			.append("</body></html>");
		
		return buff.toString();
	}

    public static String getFacilityAdminEmailTemplateForNewMember(String username, String url) {

        StringBuffer buff = new StringBuffer();
        buff.append("<html><body>")
                .append("<br/>")
                .append("Hello " + username + ", ")
                .append("<br/><br/>")
                .append("A new member request is received to join the HealthSlate Program.<br/><br/> Click below to view request: <br/>")
                .append("<a href=\"" + url + "\">" + url + "</a>")
                .append("<br/><br/>")
                .append("Sincerely,")
                .append("<br/>")
                .append("The HealthSlate Team")
                .append("<br/><br/>")
                .append("</body></html>");

        return buff.toString();
    }

    public static String getDeniedRequestEmailTemplate(String appUrl, String name, Facility facility){

        StringBuffer buff = new StringBuffer();

        String questionString = "If you have any questions, or believe our decision to be in error, please contact the diabetes program manager: info@healthslate.com";
        String branding = "";
        String regards = facility.getEmailTextName();

        //providence facility check
        if (facility != null
                && !ObjectUtils.isEmpty(facility.getFacilityLogo())
                && facility.getFacilityLogo().equalsIgnoreCase("providence_solutions_logo.png")) {

            questionString = "If you have any questions, or believe our decision to be in error, please contact the Providence diabetes program manager: kathy.schwab@providence.org";
            branding = "Providence";
            regards = branding;
        }

        buff.append("<html><body>")
                .append("<table style='width:100%;margin-bottom:10px'><tbody><tr><td><div><img src='" + appUrl + "resources/css/images/recruitment_icons/" + facility.getFacilityLogo() + "' style='width:250px;height:54px;' width='250' height='54' /></div></td><td><div><img src='" + appUrl + "resources/css/images/recruitment_icons/logowhite.png' style='width:250px;height:45px;' width='250' height='45' /></div></td></tr></tbody></table>")
                .append("Thank you for your interest in the mobile diabetes management pilot program sponsored by " + facility.getEmailTextName() + " and HealthSlate.")
                .append("<br/><br/>")
                .append("This pilot program is currently limited to people who have type 2 diabetes, a "+branding+" Medical Group doctor, insurance through "+branding+" Health Plan, and meet certain other criteria. Our records show that you do not meet one or more of these criteria. This makes you ineligible to participate in this pilot.")
                .append("<br/><br/>")
                .append(questionString)
                .append("<br/><br/>")
                .append("Thank you,")
                .append("<br/>")
                .append("The HealthSlate & " + regards + " Pilot Team")
                .append("<br/><br/>")
                .append("</body></html>");

        return buff.toString();
    }

    public static String getSendMeMoreSuggestedMealsEmailTemplate(String username, String url) {

        StringBuffer buff = new StringBuffer();
        buff.append("<html><body>")
                .append("<br/>")
                .append("Hello " + username + ", ")
                .append("<br/><br/>")
                .append("One of your member would like to have send him/her more suggested meals.<br/><br/> Click below to view request: <br/>")
                .append("<a href=\"" + url + "\">" + url + "</a>")
                .append("<br/><br/>")
                .append("Sincerely,")
                .append("<br/>")
                .append("The HealthSlate Team")
                .append("<br/><br/>")
                .append("</body></html>");

        return buff.toString();
    }

    public static String getMealErrorEmailTemplate(String username, String smsTemplate, String url) {

        StringBuffer buff = new StringBuffer();
        buff.append("<html><body>")
                .append("<br/>")
                .append("Hello " + username + ", ")
                .append("<br/><br/>")
                .append(smsTemplate.trim() + " Click <a href=\"" + url + "\"> here </a> to view the error.")
                .append("<br/><br/>")
                .append("Sincerely,")
                .append("<br/>")
                .append("The HealthSlate Team")
                .append("<br/><br/>")
                .append("</body></html>");

        return buff.toString();
    }

    public static String getEmailTextForDebugFile() {

        StringBuffer buff = new StringBuffer();
        buff.append("<html><body>")
                .append("<br/>")
                .append("Hello,")
                .append("<br/><br/>")
                .append("A HealthSlate user has sent feedback. Please use the link below to see the feedback: <br/>")
                .append("<br/><br/>")
                .append("<br/><br/>")
                .append("Sincerely,")
                .append("<br/>")
                .append("The HealthSlate Team")
                .append("<br/><br/>")
                .append("</body></html>");

        return buff.toString();
    }
}
