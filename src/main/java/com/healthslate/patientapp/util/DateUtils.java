package com.healthslate.patientapp.util;

import org.apache.commons.lang3.tuple.Pair;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/*
* ======= FILE CHANGE HISTORY =======
* [1/29/2015]: Removed unused methods, passed TIME_FORMAT constant while formatting date, __oz
* ===================================
 */

public class DateUtils {
	public static long ONE_DAY_MILLIS = 1000 * 60 * 60 * 24;

	public static String DATE_FORMAT_WITH_TIME = "MM/dd/yyyy hh:mm a";
	public static String DATE_FORMAT_DASHED = "yyyy-MM-dd";
	public static String DATE_FORMAT = "MM/dd/yyyy";
    public static String TIME_FORMAT = "hh:mm a";
    public static String DATE_FORMAT_IMAGE = "yyyyMMdd_HHmmss";
    public static String DATE_FORMAT_LOG_ID = "yyyyMMddHHmmss";
    public static String DATE_FORMAT_DASHBOARD = "MMM dd";
    public static String DATE_FORMAT_YEAR = "yyyy";
    public static String DAY_FORMAT = "EEEE";
    public static String DATE_FORMAT_CURRICULUM = "yyyy-MM-dd'T'HH:mm:ss";
	public static String DATE_FORMAT_REGISTRAION = "yyyy-MM-dd HH:mm:ss";
    public static String DATE_FORMAT_ONLY_MONTH = "MMMM dd";

    // return elapsed time between two dates
	public static String getElapsedTime(long startDate, long endDate) {
		// milliseconds
		long diff = endDate - startDate;

		@SuppressWarnings("unused")
		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);

		String timeString = String.format("%d min", diffMinutes);

		if (diffDays > 0) {
			timeString = String.format("%d day, %d hr, %d min", diffDays, diffHours, diffMinutes);
		} else if (diffHours > 0) {
			timeString = String.format("%d hr, %d min", diffHours, diffMinutes);
		}

		return timeString;
	}

    // return elapsed time between two dates
    public static long[] getElapsedTimes(long startDate, long endDate) {
        // milliseconds
        long diff = endDate - startDate;

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        return new long[]{diffDays, diffHours, diffMinutes, diffSeconds};
    }

	public static String getCurrentTimeByOffset(String offsetKey) {
		try {
			TimeZone timeZone = getTimezoneByOffset(TimeZone.getAvailableIDs(),	offsetKey);
			Calendar cal = new GregorianCalendar(timeZone);
			SimpleDateFormat format = new SimpleDateFormat(TIME_FORMAT);

			//when run local comment this line because we have latest jdk, on staging its not, __oz [1/29/2015]
			format.setTimeZone(timeZone);
			return format.format(cal.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

	public static TimeZone getTimezoneByOffset(String[] timeZones, String offsetKey) {
		TimeZone timeZoneName = TimeZone.getTimeZone("GMT");
		for (String timeZone : timeZones) {
			TimeZone tz = TimeZone.getTimeZone(timeZone);

			if (timeZone.equalsIgnoreCase(offsetKey)) {
				timeZoneName = tz;
				break;
			}
		}

		return timeZoneName;
	}

    public static String formatDateOnly(Date someDate) {
        try {
            DateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
            return sdf.format(someDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

	public static String getFormattedDateUTC(long dateInMillies, String format) {
		try {
			DateFormat sdf = new SimpleDateFormat(format);
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			return sdf.format(dateInMillies);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	public static String getFormattedDate(long dateInMillies, String format) {
		try {
			DateFormat sdf = new SimpleDateFormat(format);
			return sdf.format(dateInMillies);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

    public static Date parseDate(String dateString, String format) {
        try {
            Date date = new SimpleDateFormat(format).parse(dateString);
            return date;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Date();
    }

	public static long getStartDate(long currentMilliSecond, int day) {
		long startDate = 1000 + currentMilliSecond - (day * ONE_DAY_MILLIS);
		return startDate;
	}

	public static long getEndDate(long currentMilliSecond, int day) {
		long endD = currentMilliSecond + (day * ONE_DAY_MILLIS) - 1000;
		return endD;
	}

    public static long addHoursInTime(int days){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR_OF_DAY, days);

        return cal.getTime().getTime();
    }

    public static long addMinutesInTime(int minutes){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, minutes);

        return cal.getTime().getTime();
    }

    public static long addMinutesInTime(long date, int minutes){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date);
        cal.add(Calendar.MINUTE, minutes);

        return cal.getTime().getTime();
    }

	public static boolean compareDates(Date dateOne, Date secondDate){
		return dateOne.getTime() > secondDate.getTime();
	}

	public Pair<Date, Date> getDateRange(Date first, Date second) {
		Date beginning = null;
		Date end = null;

		Calendar calendarStart = getCalendarForNow(first);
		calendarStart.set(Calendar.DAY_OF_MONTH, calendarStart.getActualMinimum(Calendar.DAY_OF_MONTH));
		setTimeToBeginningOfDay(calendarStart);
		beginning = calendarStart.getTime();

		Calendar calendarEnd = getCalendarForNow(second);
		calendarEnd.set(Calendar.DAY_OF_MONTH, calendarEnd.getActualMaximum(Calendar.DAY_OF_MONTH));
		setTimeToEndOfDay(calendarEnd);
		end = calendarEnd.getTime();

		return Pair.of(beginning, end);
	}

	private static Calendar getCalendarForNow(Date date) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}

	private static void setTimeToBeginningOfDay(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	}

	private static void setTimeToEndOfDay(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
	}

	public static Long getMonthStartTime(Long timeInMillies) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTimeInMillis(timeInMillies);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTimeInMillis();
	}

	public static Long getMonthEndTime(Long timeInMillies) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTimeInMillis(timeInMillies);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);

		return calendar.getTimeInMillis();
	}

	public static int getMonthTotalDays(Long timeInMillies) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTimeInMillis(timeInMillies);
		int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		return days;
	}

	public static Date getStartOfDayTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}

	public static Date getEndOfDayTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		return cal.getTime();
	}

	public static Date getStartOfDayTime(Date date, String timeZone) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone(timeZone));
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}

	public static Date getEndOfDayTime(Date date, String timeZone) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone(timeZone));
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		return cal.getTime();
	}

	public static Long getStartOfDayTime(Long timeInMillies) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timeInMillies);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTimeInMillis();
	}

	public static Long getEndOfDayTime(Long timeInMillies) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timeInMillies);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTimeInMillis();
	}

	public static String getDateFromLong(Long timeInMilis) throws ParseException{

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
		if(timeInMilis == null || timeInMilis == 0){
			return null;
		}
		return dateFormat.format(new Date(timeInMilis));
	}

	public static Long getOffsetTime(String dateStr, String timeZone) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
			return sdf.parse(dateStr).getTime();
		}catch (Exception e){
			return  0l;
		}
	}

    public static String getOffsetTime(int offsetMillis) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);
            String[] ids = TimeZone.getAvailableIDs(offsetMillis);
            if(ids != null && ids.length > 0){
                String topInTheList = ids[0];

                TimeZone timeZone = getTimezoneByOffset(TimeZone.getAvailableIDs(),	topInTheList);
                Calendar cal = new GregorianCalendar(timeZone);
                SimpleDateFormat format = new SimpleDateFormat(TIME_FORMAT);

                //when run local comment this line because we have latest jdk, on staging its not, __oz [1/29/2015]
                format.setTimeZone(timeZone);
                return format.format(cal.getTime());
            }
        }catch (Exception e){
            return  "";
        }
        return  "";
    }

    public static Long getOffsetTime(String dateStr, int offsetMillis) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            String[] ids = TimeZone.getAvailableIDs(offsetMillis);
            if(ids != null && ids.length > 0){
                String topInTheList = ids[0];
                sdf.setTimeZone(TimeZone.getTimeZone(topInTheList));
                return sdf.parse(dateStr).getTime();
            }

        }catch (Exception e){
            return  0l;
        }

        return  0l;
    }

	public static Date getDateFromString(String date) throws ParseException{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		if(date != null && !date.equalsIgnoreCase("")){
			return dateFormat.parse(date);
		}
		return new Date();
	}

	public static long getMillisecondsFromDateString(String dateString, String format) throws ParseException{
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		long millis = 0;
		if(!ObjectUtils.isEmpty(dateString)){
			try {
				millis = dateFormat.parse(dateString).getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return millis;
	}

	public static boolean isExistInRDay(Long timeToCheck, Long specificDayTime) {
		boolean isExist = false;
		Long dayStartTime = getStartOfDayTime(specificDayTime);
		Long dayEndTime = getEndOfDayTime(specificDayTime);
		if(timeToCheck >= dayStartTime && timeToCheck <= dayEndTime) {
			isExist = true;
		}
		return isExist;
	}

	public static boolean isExistInSameDay(Long timeToCheck, Long specificDayTime) {
		String timeToCheckDate = getFormattedDateUTC(timeToCheck, DATE_FORMAT);
		String specificDate = getFormattedDateUTC(specificDayTime, DATE_FORMAT);

		if(timeToCheckDate.equalsIgnoreCase(specificDate)){
			return true;
		}
		return false;
	}

	public static int getMonth(Long timeInMillies) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTimeInMillis(timeInMillies);
		int month = calendar.get(Calendar.MONTH);
		return month;
	}

	public static int getDay(Long timeInMillies) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTimeInMillis(timeInMillies);
		int days = calendar.get(Calendar.DAY_OF_MONTH);
		return days;
	}

	public static String getPreviousMonthDate(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		return dateFormat.format(cal.getTimeInMillis());
	}

	public static String getNextMonthDate(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, 1);
		return dateFormat.format(cal.getTimeInMillis());
	}

	public static String getLastWeekDate(Date date){
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, - 6);
		return dateFormat.format(c.getTime());
	}

    public static long getLastWeekDateInMillis(Date date){
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, - 6);
        return c.getTime().getTime();
    }

	public static String getLastThreeDays(Date date){
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, - 2);
		return dateFormat.format(c.getTime());
	}

	public static String getThreeDaysOldDate(Date date, String format){
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, - 3);
		String dateString = dateFormat.format(c.getTime());
		return dateString;
	}

	public static String getLastFourteenDays(Date date){
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, - 13);
		return dateFormat.format(c.getTime());
	}

	public static String getNextThreeDays(Date date){
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, 3);
		return dateFormat.format(c.getTime());
	}

    public static Date getLastWeekDateFromCurrent(Date date){

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, - 6);
        return c.getTime();
    }

    public static int getDaysBetween(Long firstTime, Long secondTime){
    	Date d1 = new Date(firstTime);
    	Date d2 = new Date(secondTime);
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }

    public static Long getUTCTime(String timestamp) {
    	Calendar calender = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		calender.setTimeInMillis(Long.valueOf(timestamp));
		calender.set(Calendar.HOUR_OF_DAY, 0);
		calender.set(Calendar.MINUTE, 0);
		calender.set(Calendar.SECOND, 0);
		calender.set(Calendar.MILLISECOND, 0);
		return calender.getTimeInMillis();
    }

	public static Long getMidnight(){
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.DAY_OF_MONTH,1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return cal.getTimeInMillis();
	}


	public static Long addDayInTime(long timestamp, int days) {
		Calendar calender = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		calender.setTimeInMillis(timestamp);
		calender.add(Calendar.DAY_OF_MONTH, days);
		return calender.getTimeInMillis();
	}

	public static int getDifferenceInMonths(Long timeFrom, Long timeTo) {
		Calendar startCalendar = new GregorianCalendar();
		Calendar endCalendar = new GregorianCalendar();
		startCalendar.setTimeInMillis(timeFrom);
		endCalendar.setTimeInMillis(timeTo);
		int difInMonths = endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
		return Math.abs(difInMonths);
	}

    public static long getMillisecondsByUserOffset(long sessionTime, String offsetKey, String format) {
        try {

            SimpleDateFormat sdf = new SimpleDateFormat(format);
            TimeZone timeZone = getTimezoneByOffset(TimeZone.getAvailableIDs(), offsetKey);
            String strDate = getFormattedDate(sessionTime,format);
            sdf.setTimeZone(timeZone);
            return sdf.parse(strDate).getTime();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static String getFormattedDateInUserTimeZone(long time, long offset, String offsetKey, String format) {

        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);

            TimeZone timeZone = getTimezoneByOffset(TimeZone.getAvailableIDs(), offsetKey);
            Calendar cal = new GregorianCalendar();
            cal.setTimeInMillis(time);

            sdf.setTimeZone(timeZone);
            return sdf.format(cal.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static long getTimezoneInMillisecondsByOffset(String offsetKey){

        int intOffset = Integer.parseInt(offsetKey);
        int offsetMillis = intOffset * 60 * 60 * 1000;

        return offsetMillis;
    }

	public static String getTimezoneByMilliOffset(long offset){
		return null;
	}

    public static Map<String, Long> getCurrentDateInMilliMap(Map<String, String> mealTypeMap) {
        Map<String, Long> currDateMap = new LinkedHashMap<String, Long>();

        for (String key : mealTypeMap.keySet()) {
            if (key.equalsIgnoreCase("Breakfast")) {
                currDateMap.put("BreakfastTime", getDateInMilli(mealTypeMap.get("Breakfast")));
            } else if (key.equalsIgnoreCase("Lunch")) {
                currDateMap.put("LunchTime", getDateInMilli(mealTypeMap.get("Lunch")));
            } else if (key.equalsIgnoreCase("Snack")) {
                currDateMap.put("SnackTime", getDateInMilli(mealTypeMap.get("Snack")));
            } else if (key.equalsIgnoreCase("Dinner")) {
                currDateMap.put("DinnerTime", getDateInMilli(mealTypeMap.get("Dinner")));
            } else {
                currDateMap.put("BeverageTime", getDateInMilli(mealTypeMap.get("Beverage")));
            }
        }
        return currDateMap;
    }

    public static long getDateInMilli(String time) {
        Date milliSeconds = null;
        try {
            Date currDate = new Date();
            String tempDate = new SimpleDateFormat("dd/MM/yyyy").format(currDate);
            tempDate = tempDate + " " + time;
            // System.out.println(tempDate);
            milliSeconds = new SimpleDateFormat("dd/MM/yy hh:mm a").parse(tempDate);
            // System.out.println(datetest.getTime());

        } catch (ParseException ex) {
            System.out.println("Exception..." + ex);
        }
        return milliSeconds.getTime();
    }
}