package com.healthslate.patientapp.util;

import com.healthslate.patientapp.model.dao.BaseDAO;
import com.healthslate.patientapp.model.dao.EmailNotificationMessageDAO;
import com.healthslate.patientapp.model.dao.PreferencesDAO;
import com.healthslate.patientapp.model.entity.EmailNotificationMessage;
import com.healthslate.patientapp.model.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.codec.Base64;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;

public class EmailUtils {

    static final Logger LOG = LoggerFactory.getLogger(EmailUtils.class);

	public static boolean sendTextInEmail(final String emailText, final String recipent, final String emailFrom, final String userName, final String password, final String hostName, final String subjectText) throws Exception {

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					final String from = emailFrom;
					final String to = recipent;
					final String body = emailText;
					final String subject = subjectText;
					final String smtp_username = userName;
					final String smtp_password = password;
					String host = hostName;
					final int port = 25;

					// Get system properties
					Properties properties = System.getProperties();
					properties.put("mail.transport.protocol", "smtp");
					properties.put("mail.smtp.port", port);

					// Setup mail server
					properties.put("mail.smtp.auth", "true");
					properties.put("mail.smtp.starttls.enable", "true");
					properties.put("mail.smtp.starttls.required", "true");


					// Create a Session object to represent a mail session with the specified properties.
					Session session = Session.getDefaultInstance(properties);

					// Create a message with the specified information.
					MimeMessage msg = new MimeMessage(session);
					// TODO
					msg.setFrom(new InternetAddress(from, ""));
					msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
					msg.setSubject(subject);
					msg.setContent(body,"text/html");

					// Create a transport.
					Transport transport = session.getTransport();

					// Send the message.
					try{
						// Connect to Amazon SES using the SMTP username and password you specified above.
						transport.connect(host, smtp_username, smtp_password);

						// Send the email.
						transport.sendMessage(msg, msg.getAllRecipients());

						LOG.info("sendEmail async, sent: true");

					}catch (Exception ex) {
						ex.printStackTrace();
						LOG.info("sendEmail async, Exception: " + ex.getMessage());
					}finally{
						transport.close(); // Close and terminate the connection.
					}
				} catch (MessagingException me) {
					me.printStackTrace();
					LOG.info("sendEmail async, MessagingException: " + me.getMessage());
				} catch (UnsupportedEncodingException uee) {
					uee.printStackTrace();
					LOG.info("sendEmail async, UnsupportedEncodingException: " + uee.getMessage());
				}
			}
		}).start();

		return true;
	}

	public static boolean sendResetPasswordEmail(User user, HttpServletRequest request, boolean isResetOrNew){
		boolean isSent = false;
		Map<String, String> prefMap = new PreferencesDAO().map();

		try {
			EmailNotificationMessage emailNotificationMessage = new EmailNotificationMessage();
			String token = user.getEmail()+System.currentTimeMillis();
			token = new String (Base64.encode(token.getBytes()));

			String resetPasswordUrl = ObjectUtils.getApplicationUrl(request);
			resetPasswordUrl += "/app/ResetPassword.action?token="+token;
			String emailText = CommunicationUtils.getEmailTextForCreatePassword(user.getEmail(), resetPasswordUrl);
            String emailSubject = AppConstants.EmailSubjects.CREATE_PASSWORD.getValue();

            if(isResetOrNew){
                emailText = CommunicationUtils.getEmailTextForResetPassword(user.getEmail(), resetPasswordUrl);
                emailSubject = AppConstants.EmailSubjects.RESET_PASSWORD.getValue();
            }

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, 1);

			emailNotificationMessage.setToken(token);
			emailNotificationMessage.setSentTime(System.currentTimeMillis());
			emailNotificationMessage.setEmailBody(emailText);
			emailNotificationMessage.setTokenExpiry(cal.getTimeInMillis());
			emailNotificationMessage.setIsExpired(false);
			emailNotificationMessage.setUser(user);
			Random r = new Random(System.currentTimeMillis());
			int randomPinCode = (1 + r.nextInt(2)) * 10000 + r.nextInt(10000);

			emailNotificationMessage.setPinCode(randomPinCode + "");
			String smsBody = "Please use following pin while resetting your password: \n" + randomPinCode;
			emailNotificationMessage.setSmsBody(smsBody);

			//update all already sent email flags is_expired = true,
			//because we only need one email token active at the moment
			new EmailNotificationMessageDAO().saveEmailToken(emailNotificationMessage);

			new BaseDAO().save(emailNotificationMessage);

			TwilioMessageUtils.sendSMS(user.getPhone(), smsBody);

			isSent = EmailUtils.sendTextInEmail(emailText,
                    user.getEmail(),
                    prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                    emailSubject);

            LOG.info("sendResetPasswordEmail: Sending Email to: "+user.getEmail()+", isSent: " +isSent);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return isSent;
	}

	public static boolean sendOneToOneSessionEmail(String patientEmailAddress, String emailSubject, String emailBody, boolean isFromScheduler){
		boolean isSent = false;

		Map<String, String> prefMap = null;

		if(isFromScheduler){
			prefMap = new PreferencesDAO().mapSchedular();
		}else{
			prefMap = new PreferencesDAO().map();
		}

		try {

			isSent = EmailUtils.sendTextInEmail(emailBody,
					patientEmailAddress,
					prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
					prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
					prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
					prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
					emailSubject);

			LOG.info("sendOneToOneSessionEmail: Sending Email to: "+patientEmailAddress+", " +
					"emailSubject: "+emailSubject+ " , " +
					"emailBody: " + emailBody + " , isSent: " +isSent);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return isSent;
	}

	public static boolean sendEmailWithAttachment(final String emailText, final String recipient, final String emailFrom, final String userName, final String password, final String hostName, final String subjectText, final String attachmentName, final InternetAddress[] ccEmails, final List<MimeBodyPart> bodyParts) throws Exception {

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					final String from = emailFrom;
					final String to = recipient;
					final String body = emailText;
					final String subject = subjectText;
					final String smtp_username = userName;
					final String smtp_password = password;
					String host = hostName;
					final int port = 25;

					// Get system properties
					Properties properties = System.getProperties();
					properties.put("mail.transport.protocol", "smtp");
					properties.put("mail.smtp.port", port);

					// Setup mail server
					properties.put("mail.smtp.auth", "true");
					properties.put("mail.smtp.starttls.enable", "true");
					properties.put("mail.smtp.starttls.required", "true");

					// Create a Session object to represent a mail session with the specified properties.
					Session session = Session.getDefaultInstance(properties);

					// Create a message with the specified information.
					MimeMessage msg = new MimeMessage(session);
					msg.setFrom(new InternetAddress(from, ""));
					msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
					msg.setRecipients(Message.RecipientType.CC, ccEmails);
					msg.setSubject(subject);
					// msg.setContent(body,"text/html");

					// Creating Multipart object from BodyParts
					Multipart multipart = new MimeMultipart();

					for (MimeBodyPart bodyPart : bodyParts) {
						multipart.addBodyPart(bodyPart);
					}

					msg.setContent(multipart);
					// System.out.println("Sending");

					// Create a transport.
					Transport transport = session.getTransport();

					// Send the message.
					try {
						// Connect to Amazon SES using the SMTP username and password you specified above.
						transport.connect(host, smtp_username, smtp_password);

						// Send the email.
						transport.sendMessage(msg, msg.getAllRecipients());

						LOG.info("sendEmail async, sent: true");

					} catch (Exception ex) {
						ex.printStackTrace();
						LOG.info("sendEmail async, Exception: " + ex.getMessage());
					} finally {
						transport.close(); // Close and terminate the connection.
					}
				} catch (MessagingException me) {
					me.printStackTrace();
					LOG.info("sendEmail async, MessagingException: " + me.getMessage());
				} catch (UnsupportedEncodingException uee) {
					uee.printStackTrace();
					LOG.info("sendEmail async, UnsupportedEncodingException: " + uee.getMessage());
				}
			}
		}).start();

		return true;
	}
}
