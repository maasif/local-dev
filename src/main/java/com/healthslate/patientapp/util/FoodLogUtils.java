package com.healthslate.patientapp.util;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.healthslate.patientapp.model.entity.FoodLogDetail;
import com.healthslate.patientapp.util.AppConstants.NutritionInfoKeys;

public class FoodLogUtils {

	
	public static Map<String, Float> calculateNutritionSum(List<FoodLogDetail> foodLogDetails) {
		
		Map<String, Float> calculatedValues = new LinkedHashMap<String, Float>();
		Float totalCarbs = 0f;
		Float totalFats = 0f;
		Float totalProtein = 0.0f;
		
		for(FoodLogDetail foodLogDetail: foodLogDetails){
			
			if(foodLogDetail.getFoodMaster() != null){
				
				if(foodLogDetail.getFoodMaster().getCarbs() != 0){
					if(foodLogDetail.getNumberOfServings() != null && foodLogDetail.getNumberOfServings() != 0){
						totalCarbs = totalCarbs + (foodLogDetail.getNumberOfServings() * foodLogDetail.getFoodMaster().getCarbs());
					}
					else{
						totalCarbs = totalCarbs + foodLogDetail.getFoodMaster().getCarbs();
					}
				}
				
				if(foodLogDetail.getFoodMaster().getFats() != 0){
					if(foodLogDetail.getNumberOfServings() != null && foodLogDetail.getNumberOfServings() != 0){
						totalFats = totalFats + (foodLogDetail.getNumberOfServings() * foodLogDetail.getFoodMaster().getFats());
					}
					else{
						totalFats = totalFats + foodLogDetail.getFoodMaster().getFats();
					}
				}
				
				if(foodLogDetail.getFoodMaster().getProtein() != 0){
					if(foodLogDetail.getNumberOfServings() != null && foodLogDetail.getNumberOfServings() != 0){
						totalProtein = totalProtein + (foodLogDetail.getNumberOfServings() * foodLogDetail.getFoodMaster().getProtein());
					}
					else{
						totalProtein = totalProtein + foodLogDetail.getFoodMaster().getProtein();
					}
				}
			}
		}
		
		calculatedValues.put(NutritionInfoKeys.CARBS.name(), totalCarbs);
		calculatedValues.put(NutritionInfoKeys.FATS.name(), totalFats);
		calculatedValues.put(NutritionInfoKeys.PROTEIN.name(), totalProtein);
		
		return calculatedValues;
	}
}
