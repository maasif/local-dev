package com.healthslate.patientapp.util;

import java.util.ArrayList;
import java.util.List;

import com.healthslate.patientapp.model.entity.FoodMaster;
import com.healthslate.patientapp.model.entity.NutrionixHit;
import com.healthslate.patientapp.model.entity.NutrionixItem;
import com.healthslate.patientapp.util.ObjectUtils;

public class FoodUtils {

	public static List<FoodMaster> getFoodMasterListFromNutrionixHitList(
			List<NutrionixHit> nutrionixHitsList) {
		List<FoodMaster> foodMasters = new ArrayList<FoodMaster>();
		for (NutrionixHit nHit: nutrionixHitsList) {
			foodMasters.add(initFoodMasterFromNutrionxHit(nHit));
		}
		return foodMasters;
	}

	private static FoodMaster initFoodMasterFromNutrionxHit(NutrionixHit nHit) {
		FoodMaster foodMaster = new FoodMaster();
		
		foodMaster.setItemId(nHit.getFields().getItem_id());
		foodMaster.setBrandId(nHit.getFields().getBrand_id());
		foodMaster.setBrandName(nHit.getFields().getBrand_name());
		foodMaster.setFoodName(nHit.getFields().getItem_name());
		foodMaster.setDescription(nHit.getFields().getItem_description());
		foodMaster.setCalories(nHit.getFields().getNf_calories());
		foodMaster.setCarbs(nHit.getFields().getNf_total_carbohydrate());
		foodMaster.setSodium(nHit.getFields().getNf_sodium());
		foodMaster.setFats(nHit.getFields().getNf_total_fat());
		foodMaster.setProtein(nHit.getFields().getNf_protein());
		foodMaster.setServingSizeUnit(nHit.getFields().getNf_serving_size_unit());
		
		return foodMaster;
	}

	public static FoodMaster initFoodMasterFromNutrionixItem(
			NutrionixItem nutrionixItem) {
		FoodMaster foodMaster = new FoodMaster();

		if (!ObjectUtils.isEmpty(nutrionixItem.getBrand_id()))
			foodMaster.setBrandId(nutrionixItem.getBrand_id());
		if (!ObjectUtils.isEmpty(nutrionixItem.getBrand_name()))
			foodMaster.setBrandName(nutrionixItem.getBrand_name());
		if (!ObjectUtils.isEmpty(nutrionixItem.getItem_description()))
			foodMaster.setDescription(nutrionixItem.getItem_description());
		if (!ObjectUtils.isEmpty(nutrionixItem.getItem_id()))
			foodMaster.setItemId(nutrionixItem.getItem_id());
		if (!ObjectUtils.isEmpty(nutrionixItem.getItem_name()))
			foodMaster.setFoodName(nutrionixItem.getItem_name());
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_calcium_dv()))
			foodMaster.setCalcium(Float.parseFloat(nutrionixItem.getNf_calcium_dv()));
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_calories()))
			foodMaster.setCalories(Integer.parseInt(nutrionixItem.getNf_calories()));
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_cholesterol()))
			foodMaster.setCholestrol(Float.parseFloat(nutrionixItem.getNf_cholesterol()));
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_dietary_fiber()))
			foodMaster.setFiber(Float.parseFloat(nutrionixItem.getNf_dietary_fiber()));
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_iron_dv()))
			foodMaster.setIron(Float.parseFloat(nutrionixItem.getNf_iron_dv()));
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_monounsaturated_fat()))
			foodMaster.setMonoUnsaturated(Float.parseFloat(nutrionixItem.getNf_monounsaturated_fat()));
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_polyunsaturated_fat()))
			foodMaster.setPolyUnsaturated(Float.parseFloat(nutrionixItem.getNf_polyunsaturated_fat()));
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_protein()))
			foodMaster.setProtein(Integer.parseInt(nutrionixItem.getNf_protein()));
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_saturated_fat()))
			foodMaster.setSaturated(Float.parseFloat(nutrionixItem.getNf_saturated_fat()));
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_serving_size_qty()))
			foodMaster.setServingSizeQuantity(nutrionixItem.getNf_serving_size_qty());
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_serving_size_unit()))
			foodMaster.setServingSizeUnit(nutrionixItem.getNf_serving_size_unit());
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_serving_weight_grams()))
			foodMaster.setServingWeightGrams(nutrionixItem.getNf_serving_weight_grams());
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_servings_per_container()))
			foodMaster.setServingPerContainer(nutrionixItem.getNf_servings_per_container());
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_sodium()))
			foodMaster.setSodium(Integer.parseInt(nutrionixItem.getNf_sodium()));
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_sugars()))
			foodMaster.setSugars(Float.parseFloat(nutrionixItem.getNf_sugars()));
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_total_carbohydrate()))
			foodMaster.setCarbs(Integer.parseInt(nutrionixItem.getNf_total_carbohydrate()));
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_total_fat()))
			foodMaster.setFats(Integer.parseInt(nutrionixItem.getNf_total_fat()));
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_trans_fatty_acid()))
			foodMaster.setTrans(Float.parseFloat(nutrionixItem.getNf_trans_fatty_acid()));
		if (!ObjectUtils.isEmpty(nutrionixItem.getNf_vitamin_a_dv()))
			foodMaster.setVitaminA(Float.parseFloat(nutrionixItem.getNf_vitamin_a_dv()));
			foodMaster.setVitaminC(Float.parseFloat(nutrionixItem.getNf_vitamin_c_dv()));
		
		return foodMaster;
	}
}
