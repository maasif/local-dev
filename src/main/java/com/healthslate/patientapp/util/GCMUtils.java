package com.healthslate.patientapp.util;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import com.google.gson.Gson;
import com.healthslate.patientapp.model.entity.Device;
import com.healthslate.patientapp.ws.LogServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class GCMUtils {

    static final Logger LOG = LoggerFactory.getLogger(GCMUtils.class);

	public static String sendGCM(List<String> androidTargets, String action, String payload) {
		Map<String, Object> msgMap = new LinkedHashMap<String, Object>();
		String SENDER_ID = AppConstants.GCMKEYS.BROWSER_KEY.getValue();
		String status = null;
		
		try {
			msgMap.put(AppConstants.GCMKEYS.ACTIONS.getValue(), action);
			if(!ObjectUtils.isEmpty(payload)){
				msgMap.put(AppConstants.GCMKEYS.PAYLOAD.getValue(), new EncryptionUtils().encrypt(payload));
			}
			String data = JsonUtil.toJson(msgMap);
			Sender sender = new Sender(SENDER_ID);
			
			Message message = new Message.Builder()
			.delayWhileIdle(false)
			.addData(AppConstants.GCMKEYS.DATA.getValue(), data)
			.build();
			MulticastResult result = sender.send(message, androidTargets, 1);
			status = result.toString();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return status;
	}

    public static boolean isGCMSent(String serviceName, String result, Device device){
        boolean isSent = false;
        try {
            if(result != null){
                String[] resultParts = result.split(",");
                String[] failMessageResultParts = resultParts[3].split("=");

                if(failMessageResultParts[0].equalsIgnoreCase("failure") && failMessageResultParts[1].equalsIgnoreCase("1")){
                    LOG.info(serviceName + ": GCM sending: Failure, macAddress: " + device.getDeviceMacAddress() + ", registrationId: " + device.getRegistrationId());
                }else{
                    isSent = true;
                    LOG.info(serviceName + ": GCM sending: Success, macAddress: " + device.getDeviceMacAddress() + ", registrationId: " + device.getRegistrationId());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isSent;
    }
}
