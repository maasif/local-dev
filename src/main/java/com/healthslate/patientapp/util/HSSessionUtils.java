package com.healthslate.patientapp.util;

/*
* ======= FILE CHANGE HISTORY =======
* [2/26/2015]: Created by __oz
* ===================================
 */

import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.entity.Facility;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.Provider;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

public class HSSessionUtils {

    public static Patient getPatientSession(Map<String, Object> session, long patientId){
        Patient patient = (Patient) session.get(AppConstants.SessionKeys.ACTIVE_PATIENT.name());
        if(patient == null){
            patient = new PatientDAO().getPatientById(patientId);
            session.put(AppConstants.SessionKeys.ACTIVE_PATIENT.name(), patient);
        }
        return patient;
    }

    public static Provider getProviderSession(Map<String, Object> session){

        return (Provider) session.get(AppConstants.SessionKeys.PROVIDER.name());
    }

    public static Facility getFacilitySession(Map<String, Object> session){

        return (Facility) session.get(AppConstants.SessionKeys.FACILITY.name());
    }

    public static Facility getFacilitySession(HttpServletRequest httpRequest, Facility requestedFacility){

        HttpSession session = httpRequest.getSession();
        Facility facility = null;
        if(session != null){
            facility = (Facility)session.getAttribute(AppConstants.SessionKeys.FACILITY.name());
            if(facility == null){
                facility = requestedFacility;
            }
            if(facility == null){
                facility = CommonUtils.getRecruitmentFacility();
            }
        }

        return facility;
    }
}
