package com.healthslate.patientapp.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;


import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by omursaleen on 7/27/2015.
 */
public class NetworkUtils {

    @SuppressWarnings({ "rawtypes", "unchecked", "resource" })
    public static String postRequest(String url, List nameValuePairs) throws ClientProtocolException, IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);

        post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        HttpResponse response = client.execute(post);
        HttpEntity responseEntity = response.getEntity();
        java.util.Scanner s = new java.util.Scanner(responseEntity.getContent()).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    @SuppressWarnings({ "rawtypes", "unchecked", "resource" })
    public static String postRequest(String url, Map<String, String> headerEntries) throws ClientProtocolException, IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);

        for(Map.Entry<String, String> entry: headerEntries.entrySet()){
            post.setHeader(entry.getKey(), entry.getValue());
        }

        HttpResponse response = client.execute(post);
        HttpEntity responseEntity = response.getEntity();
        java.util.Scanner s = new java.util.Scanner(responseEntity.getContent()).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    @SuppressWarnings({ "resource" })
    public static String getRequest(String url) throws ClientProtocolException, IOException{
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);

        HttpResponse response = client.execute(request);
        HttpEntity responseEntity = response.getEntity();
        java.util.Scanner s = new java.util.Scanner(responseEntity.getContent()).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
