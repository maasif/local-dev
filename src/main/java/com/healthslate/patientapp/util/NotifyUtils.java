package com.healthslate.patientapp.util;

import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.FacilityAdminDTO;
import com.healthslate.patientapp.model.dto.MessageGcmDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.ws.LogServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*
* ======= FILE CHANGE HISTORY =======
* [2/19/2015]: created by, __oz
* [2/23/2015]: Added notifyPatientsOfNewCoach for sending GCM when new Coach created/or assigned, __oz
* [2/27/2015]: Added notifyPatientOfSuggestedMeals for notifying patient of newly/edited suggested meals, __oz
* [4/02/2015]: Added notifyPatientOfHealthSlateInvitation for notifying patient/user of joined healthslate program, __oz
* [4/03/2015]: Added notifyMemberToEnrollInHealthSlateProgram for notifying patient/user to enroll in healthslate program, __oz
* [4/10/2015]: Added sendInAppMessage for notifying patient/user, __oz
* [7/15/2015]: Added notifyAdminOfNewMemberRequest for notifying admin of new member request, __oz
* [7/24/2015]: Added notifyMemberOfDeniedRequest for notifying member that request has been denied, __oz
* [7/27/2015]: Added notifyCoachesOfSendingSuggestedMeals for notifying food coaches to send member more suggested meals , __oz
* [9/01/2015]: Added notifyCoachOfMealError for notifying coach about meal error reported by member , __oz
* [9/01/2015]: Added notifyCoachOfUpdatedMemberTeam for notifying coach updated member team , __oz
* ===================================
 */

public class NotifyUtils {
    static final Logger LOG = LoggerFactory.getLogger(NotifyUtils.class);
    public static final String MEAL = "MEAL";
    public static final String IN_APP = "IN-APP";
    public static final String TECH_SUPPORT = "TECH-SUPPORT";

    public static void notifyCoachOfUnprocessedMealLog(HttpServletRequest httpRequest, Log log){
        try {
            Patient patient = log.getPatient();
            if(patient != null){
                LOG.info("notifyCoachOfUnprocessedMealLog: Log: "+log.toString()+"\nPatient: "+patient.toString());
                List<Provider> providers = patient.getProviders();
                if(providers != null && providers.size() > 0){
                    Facility facility = null;

                    String facIdString = (String) new PreferencesDAO().getPreference(AppConstants.PreferencesNames.HEALTHSLATE_FACILITY_ID.getValue());
                    LOG.info("notifyCoachOfUnprocessedMealLog: HealthSlate facilityId: "+facIdString);
                    int facId = Integer.parseInt(facIdString);
                    List<Provider> foodCoaches = new ArrayList<Provider>();
                    
                    for(Provider provider: providers){
                        Facility innerFacility = provider.getFacility();
                        if(innerFacility.getFacilityId() != facId){
                            facility = innerFacility;
                        }
                        if(provider.getType().equals(AppConstants.CoachTypes.FOOD_COACH.getValue())) {
                        	foodCoaches.add(provider);
                        }
                    }
                    	
                    if(facility != null && ObjectUtils.nullSafe(facility.getIsNotificationEnabled())) {
                        sendEmail(httpRequest, MEAL, AppConstants.EmailSubjects.UNPROCESSED_MEAL_LOG.getValue(), foodCoaches, 0);
                        sendSMS(httpRequest, new PreferencesDAO().getPreference(AppConstants.PreferencesNames.UNPROCESSED_COACH_SMS_TEMPLATE.getValue()), foodCoaches);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyCoachOfUnprocessedMealLog: Exception: "+e.getMessage());
        }
    }

    public static void notifyCoachOfPatientMessage(HttpServletRequest httpRequest, long patientId, Provider provider){
        try {
            LOG.info("notifyCoachOfPatientMessage: Provided data: Id:" + patientId);
            if(provider != null){
                LOG.info("Notifying provider: "+provider.toString());
                List<Provider> providers = new ArrayList<Provider>();
                providers.add(provider);
                sendEmail(httpRequest, IN_APP, AppConstants.EmailSubjects.IN_APP_MESSAGE.getValue(), providers, patientId);
                sendSMS(httpRequest, new PreferencesDAO().getPreference(AppConstants.PreferencesNames.NEW_IN_APP_MESSAGE_SMS_TEMPLATE.getValue()), providers);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyCoachOfPatientMessage: Exception: "+e.getMessage());
        }
    }

    public static void notifyTechSupportOfPatientMessage(HttpServletRequest httpRequest, long patientId){
        try {
            LOG.info("notifyTechSupportOfPatientMessage: Provided data: Id:" + patientId);
            List<Provider> techSupportList = new ProviderDAO().getAllTechSupport();
            if(techSupportList.size() > 0 ){
                LOG.info("Notifying provider: "+techSupportList.toString());
//                List<Provider> providers = new ArrayList<Provider>();
//                providers.add(provider);
                sendEmail(httpRequest, TECH_SUPPORT, AppConstants.EmailSubjects.TECH_SUPPORT_MESSAGE.getValue(), techSupportList, patientId);
                sendSMS(httpRequest, new PreferencesDAO().getPreference(AppConstants.PreferencesNames.NEW_IN_APP_MESSAGE_SMS_TEMPLATE.getValue()), techSupportList);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyTechSupportOfPatientMessage: Exception: "+e.getMessage());
        }
    }

    public static void sendEmail(HttpServletRequest httpRequest, String emailType, String emailSubject, List<Provider> providers, long patientId){
        Map<String, String> prefMap = new PreferencesDAO().map();
        try {
            //sending email
            for(Provider provider: providers){
            	if(ObjectUtils.nullSafe(provider.getIsEmailEnabled()) && !ObjectUtils.nullSafe(provider.getIsDeleted())) {
            		User user = provider.getUser();
                    if(user != null){
                    	String email = user.getEmail();
                        String urlName = ObjectUtils.getApplicationUrl(httpRequest);
                        String emailText = CommunicationUtils.getEmailTextForUnprocessedMeal(user.getFirstName(), urlName);

                        if(emailType.equalsIgnoreCase(IN_APP)){
                            urlName += "/app/educator/dashboard.action?token="+CommonUtils.getEncodedUrlString(patientId + "");
                            emailText = CommunicationUtils.getEmailTextForPatientMessage(user.getFirstName(), urlName);
                        }

                        if(emailType.equalsIgnoreCase(TECH_SUPPORT)){
                            urlName += "/app/educator/dashboard.action?token="+CommonUtils.getEncodedUrlString(patientId + "");
                            emailText = CommunicationUtils.getEmailTextForPatientMessage(user.getFirstName(), urlName);
                        }

                        LOG.info("Sending email at: "+email+", email text: "+emailText+", email Type: "+emailType+", ProviderId: "+provider.getProviderId());
                        @SuppressWarnings("unused")
    					boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
                                email,
                                prefMap.get(AppConstants.PreferencesNames.EMAIL_REPLY_ADDRESS.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                                emailSubject);

                        CommonUtils.addEmailTokenToDB("sendEmail: "+email, urlName);

                        LOG.info("Email sent to: " + email + ", sent status: " + isSentEmail + ", ProviderId: " + provider.getProviderId());
                    }
            	}
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("sendEmail: Exception: "+e.getMessage());
        }
    }

    public static void sendSMS(HttpServletRequest httpRequest, String smsText, List<Provider> providers){
        //Map<String, String> prefMap = new PreferencesDAO().map();
        try {
            //sending SMS
            for(Provider provider: providers){
            	if(ObjectUtils.nullSafe(provider.getIsSMSEnabled()) && !ObjectUtils.nullSafe(provider.getIsDeleted())) {
            		String smsNumber = provider.getUser().getPhone();
                    LOG.info("Sending SMS text: "+ smsText +"\nSMS at: "+smsNumber+"\nfor provider: "+provider.toString());
            		TwilioMessageUtils.sendSMS(smsNumber, smsText);
            	}
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("sendScheduledSMS: Exception: "+e.getMessage());
        }
    }

    //[2/23/2015]: Added notifyPatientsOfNewCoach for sending GCM when new Coach created/or assigned, __oz
    public static void notifyPatientsOfNewCoach(Provider provider, HttpServletRequest request){

        try {
            if(provider != null){
                List<Patient> patients = provider.getPatients();
                List<Device> devices = new ArrayList<Device>();
                DeviceDAO deviceDAO = new DeviceDAO();
                if(patients != null && patients.size() > 0){
                    for(Patient patient : patients){
                        Device device = deviceDAO.getDeviceByMac(patient.getDeviceMacAddress());
                        if(device != null){
                            devices.add(device);
                        }
                    }
                    //String result = GCMUtils.sendGCM(deviceTargets, AppConstants.GCMKEYS.COACHES.getValue(), "");
                    if(devices != null && devices.size() > 0){
                        String result = PushNotificationManager.pushMessage(devices, AppConstants.GCMKEYS.COACHES.getValue(), "", request);
                        LOG.info("notifyPatientsOfNewCoach: GCM sent result: "+result);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyPatientsOfNewCoach: Exception: "+e.getMessage());
        }
    }

    public static void notifyCoachesOfNewNoteAndAssessment(HttpServletRequest httpRequest, long patientId, List<Provider> coachesList, boolean isCoachNotesOrAssessment){
        try {
            Map<String, String> prefMap = new PreferencesDAO().map();
            for (Provider provider: coachesList){
                User user = provider.getUser();
                if(user != null ){
                    if(!ObjectUtils.nullSafe(provider.getIsDeleted())){
                        String email = user.getEmail();
                        String urlName = ObjectUtils.getApplicationUrl(httpRequest);
                        urlName += "/app/educator/dashboard.action?token="+CommonUtils.getEncodedUrlString(patientId + "");

                        String subject = AppConstants.EmailSubjects.NEW_COACH_ASSESSMENT.getValue();
                        String emailText = CommunicationUtils.getEmailTextForGoalAssement(user.getFirstName(), urlName);

                        if(isCoachNotesOrAssessment){
                            subject = AppConstants.EmailSubjects.NEW_COACH_NOTE.getValue();
                            emailText = CommunicationUtils.getEmailTextForNewCoachNote(user.getFirstName(), urlName);
                        }

                        LOG.info("notifyCoachesOfNewNoteAndAssessment: Email urlName: "+urlName+"\nemailText: "+emailText);
                        @SuppressWarnings("unused")
                        boolean isSentEmail = false;
                        try {
                            isSentEmail = EmailUtils.sendTextInEmail(emailText,
                                    email,
                                    prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                                    prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                                    prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                                    prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                                    subject);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        CommonUtils.addEmailTokenToDB("notifyCoachesOfNewNoteAndAssessment: "+email, urlName);

                        LOG.info("notifyCoachesOfNewNoteAndAssessment: Email: " + email +", sent Status: "+isSentEmail);
                    }

                    if(ObjectUtils.nullSafe(provider.getIsSMSEnabled()) && !ObjectUtils.nullSafe(provider.getIsDeleted())){
                        String smsBody = "New goal assessment have been added for one of your members. Please log in to Coach Portal to view.";

                        if(isCoachNotesOrAssessment){
                            smsBody = "New coach notes have been added for one of your members. Please log in to Coach Portal to view.";
                        }

                        Map<String, String> results = TwilioMessageUtils.sendSMS(user.getPhone(), smsBody);
                        String status = results.get(AppConstants.JsonConstants.STATUS.name());
                        if(status.equals(AppConstants.JsonConstants.SUCCESS.name())){
                            LOG.info("notifyCoachesOfNewNoteAndAssessment: SMS sent to: " + user.getPhone() + ", status: " +status);
                        }else{
                            LOG.info("notifyCoachesOfNewNoteAndAssessment: Unable to send SMS to: "+user.getPhone()+", status: "+status);
                        }
                    }
                }else{
                    LOG.info("notifyCoachesOfNewNoteAndAssessment: user is null");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyCoachesOfNewNoteAndAssessment: Exception: "+e.getMessage());
        }
    }

    public static void notifyFoodCoachesForNewMember(HttpServletRequest httpRequest, String facilityName ,long patientId, List<Provider> coachesList){
        try {
            Map<String, String> prefMap = new PreferencesDAO().map();
            for (Provider provider: coachesList){
                if(!ObjectUtils.nullSafe(provider.getIsDeleted())) {
                    User user = provider.getUser();
                    if (user != null) {
                        String email = user.getEmail();
                        String urlName = ObjectUtils.getApplicationUrl(httpRequest);
                        urlName += "/app/educator/dashboard.action?token="+CommonUtils.getEncodedUrlString(patientId + "");
                        String emailText = CommunicationUtils.getEmailTextForNewMember(user.getFirstName(), urlName, facilityName);
                        LOG.info("notifyFoodCoachesForNewMember: Email urlName: " + urlName + "\nemailText: " + emailText);
                        @SuppressWarnings("unused")
                        boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
                                email,
                                prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                                AppConstants.EmailSubjects.NEW_MEMBER_JOINED.getValue());

                        CommonUtils.addEmailTokenToDB("notifyFoodCoachesForNewMember: "+email, urlName);

                        LOG.info("notifyFoodCoachesForNewMember: Email sent Status: " + isSentEmail);
                        System.out.println("notifyFoodCoachesForNewMember: " + email + ", Email sent? " + isSentEmail);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyFoodCoachesForNewMember: Exception: "+e.getMessage());
        }
    }

    public static void notifyLeadCoachForNewMember(HttpServletRequest httpRequest, String facilityName, long patientId, Provider provider){
        try {
        Map<String, String> prefMap = new PreferencesDAO().map();
            if(!ObjectUtils.nullSafe(provider.getIsDeleted())) {
                User user = provider.getUser();
                if (user != null) {
                    String email = user.getEmail();
                    String urlName = ObjectUtils.getApplicationUrl(httpRequest);
                    urlName += "/app/educator/dashboard.action?token="+CommonUtils.getEncodedUrlString(patientId+"");
                    String emailText = CommunicationUtils.getEmailTextForLeadCoachOfNewMember(user.getFirstName(), urlName, facilityName);
                    LOG.info("notifyLeadCoachForNewMember: Email urlName: " + urlName + "\nemailText: " + emailText);
                    @SuppressWarnings("unused")
                    boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
                            email,
                            prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                            AppConstants.EmailSubjects.NEW_MEMBER_JOINED.getValue());

                    CommonUtils.addEmailTokenToDB("notifyLeadCoachForNewMember: "+email, urlName);
                    LOG.info("notifyLeadCoachForNewMember: Email sent Status: " + isSentEmail);
                    System.out.println("notifyLeadCoachForNewMember: " + email + ", Email sent? " + isSentEmail);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyLeadCoachForNewMember: Exception: "+e.getMessage());
        }
    }

    public static boolean remindFoodCoachesForNewMember3Days(String urlName, long patientId, List<User> coachesList){
        try {
            Map<String, String> prefMap = new PreferencesDAO().mapSchedular();
            for (User user: coachesList){
                if(user != null){
                    Provider provider = new ProviderDAO().getProviderByUserScheduler(user.getUserId());
                    if(provider != null && ObjectUtils.nullSafe(provider.getIsEmailEnabled())
                                        && !ObjectUtils.nullSafe(provider.getIsDeleted())) {
                        String email = user.getEmail();
                        Patient patient = new PatientDAO().getPatientByIdScheduler(patientId);
                        urlName += "/app/educator/dashboard.action?token="+CommonUtils.getEncodedUrlString(patientId + "");
                        Facility facility = new FacilityDAO().getFacilityByPatientIdScheduler(patient.getPatientId());
                        String facilityName = facility.getName();
                        String emailText = CommunicationUtils.getEmailTextForLeadCoachOfNewMember3Days(user.getFirstName(), urlName, facilityName);
                        LOG.info("remindFoodCoachesForNewMember3Days: Email urlName: "+urlName+"\nemailText: "+emailText);
                        @SuppressWarnings("unused")
                        boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
                                email,
                                prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                                AppConstants.EmailSubjects.SET_DINNER_CARB_CEILING.getValue());

                        CommonUtils.addEmailTokenToDB("remindFoodCoachesForNewMember3Days: "+email, urlName);
                        LOG.info("remindFoodCoachesForNewMember3Days: Email sent Status: "+isSentEmail);
                        System.out.println("remindFoodCoachesForNewMember3Days: " +email+", Email sent? "+isSentEmail);
                        return isSentEmail;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("remindFoodCoachesForNewMember3Days: Exception: "+e.getMessage());
        }
        return  false;
    }

    public static boolean remindFoodCoachesForNewMemberAfter7Days(String urlName, long patientId, List<User> coachesList){
        try {
            Map<String, String> prefMap = new PreferencesDAO().mapSchedular();
            for (User user: coachesList){
                if(user != null){
                    Provider provider = new ProviderDAO().getProviderByUserScheduler(user.getUserId());
                    if(provider != null && ObjectUtils.nullSafe(provider.getIsEmailEnabled())
                                        && !ObjectUtils.nullSafe(provider.getIsDeleted())) {
                        String email = user.getEmail();
                        urlName += "/app/educator/dashboard.action?token="+CommonUtils.getEncodedUrlString(patientId + "");
                        String emailText = CommunicationUtils.getReminderEmailTextForNewMember(user.getFirstName(), urlName);
                        LOG.info("remindFoodCoachesForNewMember3Days: Email urlName: "+urlName+"\nemailText: "+emailText);
                        @SuppressWarnings("unused")
                        boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
                                email,
                                prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                                AppConstants.EmailSubjects.FIRST_WEEK_TASKS.getValue());

                        CommonUtils.addEmailTokenToDB("remindFoodCoachesForNewMemberAfter7Days: "+email, urlName);
                        LOG.info("remindFoodCoachesForNewMemberAfter7Days: Email sent Status: "+isSentEmail);
                        System.out.println("remindFoodCoachesForNewMemberAfter7Days: " +email+", Email sent? "+isSentEmail);
                        return isSentEmail;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("remindFoodCoachesForNewMemberAfter7Days: Exception: "+e.getMessage());
        }
        return  false;
    }

    //[2/27/2015]: Added notifyPatientOfSuggestedMeals for notifying patient of newly/edited suggested meals, __oz
    public static void notifyPatientOfSuggestedMeals(Patient patient, HttpServletRequest request){

        try {
            List<String> deviceTargets = new ArrayList<String>();
            if(patient != null){
                Device device = new DeviceDAO().getDeviceByMac(patient.getDeviceMacAddress());
                if(device != null){
                    /*deviceTargets.add(device.getRegistrationId());
                    String result = GCMUtils.sendGCM(deviceTargets, AppConstants.GCMKEYS.SUGGESTED_MEALS.getValue(), "");
                    boolean isGCMSent = GCMUtils.isGCMSent("notifyPatientOfSuggestedMeals service", result, device);*/

                    List<Device> devices = new ArrayList<Device>();
                    devices.add(device);
                    String result = PushNotificationManager.pushMessage(devices, AppConstants.GCMKEYS.SUGGESTED_MEALS.getValue(), "", request);

                    LOG.info("notifyPatientOfSuggestedMeals service: GCM sent results: "+result+", sent Via GCM with result: " + result);
                }else{
                    LOG.info("notifyPatientOfSuggestedMeals service: Device not found");
                }
            }else{
                LOG.info("notifyPatientOfSuggestedMeals service: Patient not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyPatientOfSuggestedMeals: Exception: "+e.getMessage());
        }
    }

    //[4/02/2015]: Added notifyPatientOfHealthSlateInvitation for notifying patient/user of joined healthslate program, __oz
    public static void notifyPatientOfHealthSlateInvitation(Patient patient, Facility facility, HttpServletRequest httpRequest){
        Map<String, String> prefMap = new PreferencesDAO().map();
        try {
            if(patient != null){
                User user = patient.getUser();
                if(user != null){
                    String email = user.getEmail();
                    String urlName = ObjectUtils.getApplicationUrl(httpRequest);
                    urlName += "/app/TermsOfService.action?token="+patient.getUuid();
                    String emailText = CommunicationUtils.getInvitationEmailText(user.getFirstName(), facility.getEmailTextName(), urlName);
                    LOG.info("notifyPatientOfHealthSlateInvitation: Invitation Email urlName: "+urlName+"\nemailText: "+emailText);
                    @SuppressWarnings("unused")
                    boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
                            email,
                            prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                            AppConstants.EmailSubjects.JOINED_HEALTHSLATE_PROGRAM.getValue());

                    CommonUtils.addEmailTokenToDB("notifyPatientOfHealthSlateInvitation: "+email, urlName);
                    LOG.info("notifyPatientOfHealthSlateInvitation: Sending Invitation email to : " + email + ", Email sent? " + isSentEmail);
                }
            }else{
                LOG.info("notifyPatientOfHealthSlateInvitation service: Patient not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyPatientOfHealthSlateInvitation: Exception: "+e.getMessage());
        }
    }

    //[4/03/2015]: Added notifyMemberToEnrollInHealthSlateProgram for notifying patient/user to enroll in healthslate program, __oz
    public static void notifyMemberToEnrollInHealthSlateProgram(Patient patient, Facility facility, HttpServletRequest httpRequest){
        Map<String, String> prefMap = new PreferencesDAO().map();
        try {
            if(patient != null){
                User user = patient.getUser();
                if(user != null){
                    String email = user.getEmail();
                    String urlName = ObjectUtils.getApplicationUrl(httpRequest);
                    String addFacilityName = "";
                    String facilityName = facility.getName();
                    String emailSubject = AppConstants.EmailSubjects.ENROLL_HEALTHSLATE_PROGRAM.getValue();
                    //providence facility check
                    if (facility != null
                            && !ObjectUtils.isEmpty(facility.getFacilityLogo())
                            && facility.getFacilityLogo().equalsIgnoreCase("providence_solutions_logo.png")) {

                        emailSubject = "Welcome to the Providence HealthSlate program";
                    }else{
                        emailSubject = emailSubject.replace(AppConstants.StringReplaceChunks.F_NAME.getValue(), facility.getEmailTextName());
                    }

                    urlName += "/app/TermsOfService.action?token="+patient.getUuid()+"&assignedFacility="+CommonUtils.getEncodedUrlString(facilityName);
                    String emailText = CommunicationUtils.getEnrollEmailText(ObjectUtils.getApplicationUrl(httpRequest)+"/", urlName, facility);
                    LOG.info("notifyMemberToEnrollInHealthSlateProgram: Enroll In HealthSlate Program Email urlName: "+urlName+"\nemailText: "+emailText);
                    @SuppressWarnings("unused")
                    boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
                            email,
                            prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                            emailSubject);

                    CommonUtils.addEmailTokenToDB("notifyMemberToEnrollInHealthSlateProgram: "+email, urlName);
                    LOG.info("notifyMemberToEnrollInHealthSlateProgram: Sending Enroll In HealthSlate Program email to : " + email + ", Email sent? " + isSentEmail);
                }
            }else{
                LOG.info("notifyMemberToEnrollInHealthSlateProgram service: Patient not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyMemberToEnrollInHealthSlateProgram: Exception: "+e.getMessage());
        }
    }

    //[4/03/2015]: Added notifyMemberOfAppUrl for notifying patient/user to enroll in healthslate program, __oz
    public static void notifyMemberOfAppUrl(Patient patient, String appUrlAndroid, String appUrlIOS){
        Map<String, String> prefMap = new PreferencesDAO().map();
        try {
            if(patient != null){
                User user = patient.getUser();
                if(user != null){
                    String email = user.getEmail();
                    String urlName = appUrlAndroid;
                    String emailText = CommunicationUtils.getPlayStoreAppLinkEmailText(user.getFirstName(), appUrlAndroid, appUrlIOS);
                    LOG.info("notifyMemberOfAppUrl: Sending App Link urlName: "+appUrlAndroid + "\n" + appUrlIOS + "\nemailText: "+emailText);
                    @SuppressWarnings("unused")
                    boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
                            email,
                            prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                            AppConstants.EmailSubjects.HEALTHSLATE_MOBILE_APP_LINK.getValue());

                    CommonUtils.addEmailTokenToDB("notifyMemberOfAppUrl: "+email, urlName);
                    LOG.info("notifyMemberOfAppUrl: Sending App Play Store Link email to : " + email + ", Email sent? " + isSentEmail);
                }
            }else{
                LOG.info("notifyMemberOfAppUrl service: Patient not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyMemberOfAppUrl: Exception: "+e.getMessage());
        }
    }

    //[4/10/2015]: Added sendInAppMessage for notifying patient/user, __oz
    public static void sendInAppMessage(String messageText, Provider coach, Patient patient, String owner, HttpServletRequest request, boolean isPostOnElgg){
        try {

            //transform to Message GCM DTO for sending to devices
            MessageGcmDTO messageGcmDTO = new MessageGcmDTO();
            messageGcmDTO.setPatientId(patient.getPatientId());
            messageGcmDTO.setProviderId(coach.getProviderId());
            messageGcmDTO.setMessageText(messageText);
            messageGcmDTO.setOwner(owner);
            messageGcmDTO.setRead(false);
            messageGcmDTO.setTimeStamp(System.currentTimeMillis());

            LOG.info("sendInAppMessage: Message: " + messageGcmDTO.toString());

            DeviceDAO deviceDao = new DeviceDAO();
            Device device = deviceDao.getDeviceByMac(patient.getDeviceMacAddress());
            List<Device> devices = new ArrayList<Device>();
            devices.add(device);
            String result = PushNotificationManager.pushMessage(devices, AppConstants.GCMKEYS.MESSAGE.getValue(), JsonUtil.toJson(messageGcmDTO), request);

            LOG.info("sendInAppMessage: Push Notification result: " + result);

            //saving message entry in DB
            User user = coach.getUser();
            BaseDAO baseDAO = new BaseDAO();
            String messageTextForElgg = "";

            if(coach.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())){
                TechSupportMessage tsm = new TechSupportMessage();
                tsm.setPatient(patient);
                //For single thread message under Tech Support on Mobile device

                Provider defaultTS = new ProviderDAO().getDefaultTechSupport();
                if(defaultTS != null){
                    tsm.setUser(defaultTS.getUser());
                }else{
                    tsm.setUser(user);
                }

                tsm.setRepliedBy(user);
                tsm.setMessage(messageGcmDTO.getMessageText());
                tsm.setOwner(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue());
                tsm.setReadStatus(true);
                tsm.setImagePath(messageGcmDTO.getImagePath());
                tsm.setTimestamp(messageGcmDTO.getTimeStamp());
                messageTextForElgg = tsm.getMessage();
                baseDAO.save(tsm);
            }else{
                Message messageDB = new Message();
                messageDB.setPatient(patient);
                messageDB.setUser(new UserDAO().getUserById(user.getUserId()));
                messageDB.setMessage(messageGcmDTO.getMessageText());
                messageDB.setOwner(messageGcmDTO.getOwner());
                messageDB.setReadStatus(true);
                messageDB.setIsError(false);
                messageDB.setTimestamp(messageGcmDTO.getTimeStamp());
                baseDAO.save(messageDB);
                messageTextForElgg = messageDB.getMessage();
            }

            if(isPostOnElgg){
                LogServices.buildMessagePost(coach, patient, messageTextForElgg);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("sendInAppMessage: Exception: "+e.getMessage());
        }
    }

    //[7/15/2015]: Added notifyAdminOfNewMemberRequest for notifying admin of new member request, __oz
    public static void notifyAdminOfNewMemberRequest(String email, String name, HttpServletRequest httpRequest){
        Map<String, String> prefMap = new PreferencesDAO().map();
        try {
            String urlName = ObjectUtils.getApplicationUrl(httpRequest);
            urlName += "/app/facilityadmin/prospectiveMembers.action";
            String emailText = CommunicationUtils.getFacilityAdminEmailTemplateForNewMember(name, urlName);
            LOG.info("notifyAdminOfNewMemberRequest: Sending App Link urlName: "+urlName + "\nemailText: "+emailText);
            @SuppressWarnings("unused")
            boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
                    email,
                    prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                    AppConstants.EmailSubjects.NEW_MEMBER_REQUEST.getValue());

            CommonUtils.addEmailTokenToDB("notifyAdminOfNewMemberRequest: "+email, urlName);
            LOG.info("notifyAdminOfNewMemberRequest: Sending email link to : " + email + ", Email sent? " + isSentEmail);

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyAdminOfNewMemberRequest: Exception: "+e.getMessage());
        }
    }

    //[7/24/2015]: Added notifyMemberOfDeniedRequest for notifying member that request has been denied, __oz
    public static void notifyMemberOfDeniedRequest(Patient patient, HttpServletRequest httpRequest){
        Map<String, String> prefMap = new PreferencesDAO().map();
        try {
            User user = patient.getUser();
            String email = user.getEmail();
            String urlName = ObjectUtils.getApplicationUrl(httpRequest)+"/";
            Facility facility = patient.getFacilities().get(0);
            String emailText = CommunicationUtils.getDeniedRequestEmailTemplate(urlName, user.getFirstName(), facility);
            String emailSubject = AppConstants.EmailSubjects.DENIED_EMAIL_SUBJECT.getValue();
            String branding = facility.getEmailTextName();

            //providence facility check
            if (facility != null
                    && !ObjectUtils.isEmpty(facility.getFacilityLogo())
                    && facility.getFacilityLogo().equalsIgnoreCase("providence_solutions_logo.png")) {

                branding = "Providence";
            }

            emailSubject = emailSubject.replace(AppConstants.StringReplaceChunks.F_NAME.getValue(), branding);

            @SuppressWarnings("unused")
            boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
                    email,
                    prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                    emailSubject);

            CommonUtils.addEmailTokenToDB("notifyMemberOfDeniedRequest: "+email, email);
            LOG.info("notifyMemberOfDeniedRequest: Sending email link to : " + email + ", Email sent? " + isSentEmail);

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyMemberOfDeniedRequest: Exception: "+e.getMessage());
        }
    }

    //[7/27/2015]: Added notifyCoachesOfSendingSuggestedMeals for notifying food coaches to send member more suggested meals , __oz
    public static void notifyCoachesOfSendingSuggestedMeals(Patient patient, Provider coach, HttpServletRequest httpRequest){
        Map<String, String> prefMap = new PreferencesDAO().map();
        try {
            User user = patient.getUser();
            String email = coach.getUser().getEmail();
            String urlName = ObjectUtils.getApplicationUrl(httpRequest);
            urlName += "/app/educator/dashboard.action?token="+CommonUtils.getEncodedUrlString(patient.getPatientId() + "");

            String emailText = CommunicationUtils.getSendMeMoreSuggestedMealsEmailTemplate(coach.getUser().getFirstName(), urlName);

            @SuppressWarnings("unused")
            boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
                    email,
                    prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                    AppConstants.EmailSubjects.MORE_SUGGESTED_MEALS.getValue());

            CommonUtils.addEmailTokenToDB("notifyCoachesOfSendingSuggestedMeals: "+email, email);
            LOG.info("notifyCoachesOfSendingSuggestedMeals: Sending email link to : " + email + ", Email sent? " + isSentEmail);

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyCoachesOfSendingSuggestedMeals: Exception: "+e.getMessage());
        }
    }

    //[9/01/2015]: Added notifyCoachOfMealError for notifying coach about meal error reported by member , __oz
    public static void notifyCoachOfMealError(HttpServletRequest httpRequest, Log log, Provider coach){
        try {
            Map<String, String> prefMap = new PreferencesDAO().map();
            User user = coach.getUser();
            if(user != null ){
                if(ObjectUtils.nullSafe(coach.getIsEmailEnabled()) && !ObjectUtils.nullSafe(coach.getIsDeleted())){
                    String mealErrorEmailTemplate = new SmsTemplateDAO().getSmsTemplateByName(AppConstants.SmsTemplates.MEAL_ERROR_EMAIL.getValue());
                    Patient patient = log.getPatient();
                    String email = user.getEmail();
                    String urlName = ObjectUtils.getApplicationUrl(httpRequest);
                    urlName += "/app/educator/fetchLogDetail.action?logId="+log.getLogId()+"&token="+CommonUtils.getEncodedUrlString(patient.getPatientId()+"")+"&redirectToPage=myMeals&isFromError=no";
                    String emailText = CommunicationUtils.getMealErrorEmailTemplate(user.getFirstName(), mealErrorEmailTemplate, urlName);

                    LOG.info("notifyCoachOfMealError: Email urlName: "+urlName+"\nemailText: "+emailText);
                    @SuppressWarnings("unused")
                    boolean isSentEmail = false;
                    try {
                        isSentEmail = EmailUtils.sendTextInEmail(emailText,
                                email,
                                prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                                AppConstants.EmailSubjects.MEAL_ERROR.getValue());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    CommonUtils.addEmailTokenToDB("notifyCoachOfMealError: "+email, urlName);

                    LOG.info("notifyCoachOfMealError: Email: " + email +", sent Status: "+isSentEmail);
                }

                if(ObjectUtils.nullSafe(coach.getIsSMSEnabled()) && !ObjectUtils.nullSafe(coach.getIsDeleted())){
                    String mealErrorSmsTemplate = new SmsTemplateDAO().getSmsTemplateByName(AppConstants.SmsTemplates.MEAL_ERROR_SMS.getValue());
                    Map<String, String> results = TwilioMessageUtils.sendSMS(user.getPhone(), mealErrorSmsTemplate.replace(AppConstants.StringReplaceChunks.ID.getValue(), log.getLogId()));
                    String status = results.get(AppConstants.JsonConstants.STATUS.name());
                    if(status.equals(AppConstants.JsonConstants.SUCCESS.name())){
                        LOG.info("notifyCoachOfMealError: SMS sent to: " + user.getPhone() + ", status: " +status);
                    }else{
                        LOG.info("notifyCoachOfMealError: Unable to send SMS to: "+user.getPhone()+", status: "+status);
                    }
                }
            }else{
                LOG.info("notifyCoachOfMealError: user is null");
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyCoachOfMealError: Exception: "+e.getMessage());
        }
    }

    //[9/01/2015]: Added notifyCoachOfUpdatedMemberTeam for notifying coach updated member team , __oz
    public static void notifyCoachOfUpdatedMemberTeam(Patient patient, HttpServletRequest request){

        try {
            if(patient != null){
                List<Device> devices = new ArrayList<Device>();
                DeviceDAO deviceDAO = new DeviceDAO();
                Device device = deviceDAO.getDeviceByMac(patient.getDeviceMacAddress());
                if(device != null){
                    devices.add(device);
                }
                if(devices != null && devices.size() > 0){
                    String result = PushNotificationManager.pushMessage(devices, AppConstants.GCMKEYS.COACHES.getValue(), "", request);
                    LOG.info("notifyPatientsOfNewCoach: GCM sent result: "+result);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyPatientsOfNewCoach: Exception: "+e.getMessage());
        }
    }

    public static void notifyPatientOfSuggestedMealByMsgPushNotification(Log log, Provider loggedInProvider, HttpServletRequest request) {
        try {
            if (log.getPatient() != null) {
                //sending member Suggested notification
                String smsTemplateInstant = new SmsTemplateDAO().getSmsTemplateByName(AppConstants.SmsTemplates.SUGGESTED_MEAL.getValue());
                smsTemplateInstant = smsTemplateInstant.replace(AppConstants.StringReplaceChunks.COACH_NAME.getValue(), loggedInProvider.getUser().getFirstName());
                smsTemplateInstant = smsTemplateInstant.replace(AppConstants.StringReplaceChunks.MEAL_CATEGORY.getValue(), log.getFoodLogSummary().getType());

                Message suggestedMealMsg = new Message();
                suggestedMealMsg.setMessage(smsTemplateInstant);
                suggestedMealMsg.setOwner(AppConstants.SessionKeys.PROVIDER.name());
                suggestedMealMsg.setReadStatus(true);
                suggestedMealMsg.setTimestamp(System.currentTimeMillis());
                suggestedMealMsg.setPatient(log.getPatient());
                suggestedMealMsg.setIsError(false);
                suggestedMealMsg.setUser(loggedInProvider.getUser());

                new BaseDAO().save(suggestedMealMsg);
                Device device = new DeviceDAO().getDeviceByMac(log.getPatient().getDeviceMacAddress());

                if(device != null){
                    List<Device> devices = new ArrayList<Device>();
                    devices.add(device);
                    String result = PushNotificationManager.pushMessage(devices, AppConstants.GCMKEYS.MESSAGE.getValue(), "", request);

                    LOG.info("notifyPatientOfSuggestedMealsByMsgPush service: GCM sent results: "+result+", sent Via GCM with result: " + result);
                } else {
                    LOG.info("notifyPatientOfSuggestedMealsByMsgPush service: Device not found");
                }
            } else {
                LOG.info("notifyPatientOfSuggestedMealsByMsgPush service: Patient not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("notifyPatientOfSuggestedMealsByMsgPush: Exception: "+e.getMessage());
        }
    }
}
