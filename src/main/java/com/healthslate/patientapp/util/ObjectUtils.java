package com.healthslate.patientapp.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

public class ObjectUtils {

	public static String caclutateTimeDurationInHrsMin(Date dateFrom,Date dateTo){
		long secs = (dateTo.getTime() - dateFrom.getTime()) / 1000;
		long hours = secs / 3600;    
		secs = secs % 3600;
		long mins = secs / 60;
		secs = secs % 60;
		
		return String.valueOf(hours)+" hr : " +String.valueOf(mins)+" min : " +String.valueOf(secs)+" sec";
		
	}
	
	/**
	 * This method returns the current string or if null it returns an empty string.
	 * @return String s or ""
	 * @param s java.lang.String
	 */
	public static String nullSafe(String s) {
		return (s == null) ? "" : s.trim();
	}

	/**
	 * This method returns the Object.toString or if null it returns an empty string.
	 * @return String
	 * @param Object
	 */
	public static String nullSafe(Object obj) {
		return (obj == null) ? "" : obj.toString();
	}
	
	/**
	 * This method returns the boolean value or if false it returns a null value.
	 * @return boolean
	 * @param Boolean
	 */
	public static Boolean nullSafe(Boolean obj) {
		return (obj == null) ? false : obj;
	}

    public static int nullSafe(Integer obj) {
        return (obj == null) ? 0 : obj;
    }

    public static float nullSafe(Float obj) {
        return (obj == null) ? 0 : obj;
    }

	public static long nullSafeLong(Object obj) {
		return (obj == null) ? 0 : Long.parseLong(obj+"");
	}

	public static float nullSafeFloat(Object obj) {
		return (obj == null) ? 0 : Float.parseFloat(obj+"");
	}

	public static int nullSafeInteger(Object obj) {
		return (obj == null) ? 0 : Integer.parseInt(obj+"");
	}

    public static boolean nullSafeBoolean(Object obj) {
        return (obj == null) ? false : Boolean.parseBoolean(obj+"");
    }

	/**
	 * Checks whether the Value String is Empty or Not
	 * @return boolean
	 * @param String
	 */
	public static boolean isEmpty(String value) {
		if (value == null || "".equals(value.trim())) {
			return true;
		}
		return false;
	}
	
	/**
	 * Formats String value of Date in MM/DD/YYYY HH:MM:SS format into Date value
	 * @param value
	 * @return
	 */
	public static Date formatStringToDateTime(String value) {
		if (isEmpty(value)) {
			return null;
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy");
		Date date = null;
		try {
			date = (Date) simpleDateFormat.parseObject(value);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return date;
	}
	
	public static String getWebInfPath(){

    	String WEBINF = "WEB-INF";
    	String filePath = "";
        URL url = HibernateUtil.class.getResource("LoginAction.class");
        String className = url.getFile();
        filePath = className.substring(0,className.indexOf(WEBINF) + WEBINF.length());
        filePath = filePath.replaceAll("\\\\", "/");
        filePath =filePath.replaceAll("%20"," ");
        return filePath;
    }
	
	public static String getYesterDayDate(){
	    Calendar today = Calendar.getInstance();  
	    today.add(Calendar.DATE, -1);  
	    return getDateAsString(new Date(today.getTimeInMillis()));
	}
	
	public static Date getSevenDaysBeforeDate(){
	    Calendar today = Calendar.getInstance();  
	    today.add(Calendar.DATE, -7);  
	    return new Date(today.getTimeInMillis());
	}
	
	public static Date get15DaysBeforeDate(){
	    Calendar today = Calendar.getInstance();  
	    today.add(Calendar.DATE, -15);  
	    return new Date(today.getTimeInMillis());
	}
	
	public static Date get3MonthsBeforeDate(){
	    Calendar today = Calendar.getInstance();  
	    today.add(Calendar.MONTH, -3);  
	    return new Date(today.getTimeInMillis());
	}
	
	public static String getDateAsString(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(date);
	}
	

	public static boolean validateEmailSettings(String emailText2, String email,
			String from, String pass, String host) {
		if (ObjectUtils.nullSafe(emailText2).isEmpty() ||
				ObjectUtils.nullSafe(email).isEmpty() ||
					ObjectUtils.nullSafe(from).isEmpty() ||
						ObjectUtils.nullSafe(pass).isEmpty() ||
							ObjectUtils.nullSafe(host).isEmpty()){
			return false;
		}
		return true;
	}
	
	public static String getDeviceMacAddressFromXml(String xmlString, String macAddressTag){
		String macAddress = "";
		int tagStartIndex = xmlString.indexOf("<"+macAddressTag+">");
		int tagEndIndex = xmlString.indexOf("</"+macAddressTag+">");
		if (tagStartIndex != -1 && tagEndIndex != -1)
			macAddress = xmlString.substring(tagStartIndex, tagEndIndex);
		return macAddress;
	}
	
	public static void writeStreamToFile(InputStream is, File file) throws IOException{
		FileOutputStream fos = new FileOutputStream(file);
		int read = 0;
		byte[] bytes = new byte[1024];
		while ((read = is.read(bytes)) != -1) {
			fos.write(bytes, 0, read);
		}
		fos.flush();
		fos.close();
	}
	
	public static String formatDateForQuery(Date date){
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}
	
	public static Date getDateByDaysBefore(int day){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.HOUR_OF_DAY, -1 * day * 24);
		String dateInString = formatter.format(cal.getTime());
		Date date = null;
		try {
			date = formatter.parse(dateInString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;

	}
	
	public static String escapeSpecialCharacters(String str) {
		String newString = str;
		if (!ObjectUtils.isEmpty(str)){
			if(str.contains("'")){
				newString = str.replace("'", "''");
			}
		}
		return newString;
	}
	
	@SuppressWarnings("resource")
	public static String getStringFromStream (InputStream is) {
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}
	
	public static String getApplicationUrl(HttpServletRequest request) {
		return request.getScheme() + "://" + request.getServerName() + request.getContextPath();
	}

	public static String getApplicationUrlWithPort(HttpServletRequest request) {
		return request.getScheme() + "://" + request.getServerName()+":"+request.getServerPort() + request.getContextPath();
	}

    public static String getImageName() {
        return "PA"+DateUtils.getFormattedDate(System.currentTimeMillis(), DateUtils.DATE_FORMAT_IMAGE)+".png";
    }

    public static String getImageName(String initial, String uuid) {
        return initial+"_"+uuid+".png";
    }

    public static String generateLogId() {
        return "SL"+DateUtils.getFormattedDate(System.currentTimeMillis(), DateUtils.DATE_FORMAT_LOG_ID);
    }

	public static Date getBeginOfDayTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}
	
	public static Date getEndOfDayTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		return cal.getTime();
	}
	
	public static Long getBeginOfDayTime(Long time) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return cal.getTimeInMillis();
	}
	
	public static Long getEndOfDayTime(Long date) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		return cal.getTimeInMillis();
	}

}
