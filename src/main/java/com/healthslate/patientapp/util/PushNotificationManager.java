package com.healthslate.patientapp.util;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.entity.*;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by omursaleen on 4/2/2015.
 */
public class PushNotificationManager {

    static final Logger LOG = LoggerFactory.getLogger(PushNotificationManager.class);

    public static String pushMessage(List<Device> devices, String action, String payload, @Context HttpServletRequest httpRequest){

        List<String> devicesTokenDebugDevicesStaging = new ArrayList<String>();
        List<String> devicesTokenReleaseDevicesStaging = new ArrayList<String>();
        List<String> devicesTokensAndroid = new ArrayList<String>();
        List<String> devicesTokenDebugDevicesProd = new ArrayList<String>();
        List<String> devicesTokenReleaseDevicesProd = new ArrayList<String>();

        String status = null;
        try{
            for (Device device : devices){
                if(device.getDeviceType().equalsIgnoreCase(AppConstants.DeviceTypes.IOS.getValue())){
                    //IOS staging app has different debug/release certificates so check here and populate debug and release devices separately
                    String deviceName = ObjectUtils.nullSafe(device.getDeviceAppName());
                    if(deviceName.equalsIgnoreCase(AppConstants.IOS_STAGING_APP_NAME)){
                        if(device.getDeviceBuildType().equalsIgnoreCase(AppConstants.DeviceBuildTypes.DEBUG.getValue())){
                            devicesTokenDebugDevicesStaging.add(device.getRegistrationId());
                        }else{
                            devicesTokenReleaseDevicesStaging.add(device.getRegistrationId());
                        }
                    }else{
                        //IOS prod app has different debug/release certificates so check here and populate debug and release devices separately
                        if(device.getDeviceBuildType().equalsIgnoreCase(AppConstants.DeviceBuildTypes.DEBUG.getValue())){
                            devicesTokenDebugDevicesProd.add(device.getRegistrationId());
                        }else{
                            devicesTokenReleaseDevicesProd.add(device.getRegistrationId());
                        }
                    }
                }else{
                    LOG.info("pushMessage: (Android) Devices list, registrationId: " + device.getRegistrationId() + ", macAddress: " + device.getDeviceMacAddress());
                    devicesTokensAndroid.add(device.getRegistrationId());
                }
            }

            //for staging debug devices sending push notification
            if(devicesTokenDebugDevicesStaging.size() > 0) {
                String certPath = httpRequest.getServletContext().getRealPath("/WEB-INF/" + AppConstants.STAGING_DEBUG_CERTIFICATE);
                ApnsService service = APNS.newService()
                                          .withCert(certPath, AppConstants.DEV_KEY_STORE_PASSWORD)
                                          .withSandboxDestination()
                                          .build();

                String apnsPayLoad = buildMessagePayload(action, payload);
                LOG.info("iOS/staging/debug/pushMessage: DEBUG: Sending IOS Push notification, payload: " + apnsPayLoad);
                service.push(devicesTokenDebugDevicesStaging, apnsPayLoad);
                status = apnsPayLoad;
            }

            //for staging release devices sending push notification
            if(devicesTokenReleaseDevicesStaging.size() > 0) {
                String certPath = httpRequest.getServletContext().getRealPath("/WEB-INF/"+AppConstants.STAGING_RELEASE_CERTIFICATE);
                ApnsService service = APNS.newService()
                                          .withCert(certPath, AppConstants.PROD_KEY_STORE_PASSWORD)
                                          .withProductionDestination()
                                          .build();

                String apnsPayLoad = buildMessagePayload(action, payload);
                LOG.info("iOS/staging/release/pushMessage: RELEASE: Sending IOS Push notification, payload: " + apnsPayLoad);
                service.push(devicesTokenReleaseDevicesStaging, apnsPayLoad);

                status = apnsPayLoad;
            }

            //for prod debug devices sending push notification
            if(devicesTokenDebugDevicesProd.size() > 0) {
                String certPath = httpRequest.getServletContext().getRealPath("/WEB-INF/" + AppConstants.PROD_DEBUG_CERTIFICATE);
                ApnsService service = APNS.newService()
                                          .withCert(certPath, AppConstants.DEV_KEY_STORE_PASSWORD)
                                          .withSandboxDestination()
                                          .build();

                String apnsPayLoad = buildMessagePayload(action, payload);
                LOG.info("iOS/prod/debug/pushMessage: DEBUG: Sending IOS Push notification, payload: " + apnsPayLoad);
                service.push(devicesTokenDebugDevicesStaging, apnsPayLoad);
                status = apnsPayLoad;
            }

            //for prod release devices sending push notification
            if(devicesTokenReleaseDevicesProd.size() > 0) {
                String certPath = httpRequest.getServletContext().getRealPath("/WEB-INF/"+AppConstants.PROD_RELEASE_CERTIFICATE);
                ApnsService service = APNS.newService()
                                          .withCert(certPath, AppConstants.PROD_KEY_STORE_PASSWORD)
                                          .withProductionDestination()
                                          .build();

                String apnsPayLoad = buildMessagePayload(action, payload);
                LOG.info("iOS/prod/release/pushMessage: RELEASE: Sending IOS Push notification, payload: " + apnsPayLoad);
                service.push(devicesTokenReleaseDevicesStaging, apnsPayLoad);

                status = apnsPayLoad;
            }

            if(devicesTokensAndroid.size() > 0){
                LOG.info("android/pushMessage: Sending Android Push notification, action: " + action + ", payLoad: " + payload);

                Map<String, Object> msgMap = new LinkedHashMap<String, Object>();
                String SENDER_ID = AppConstants.GCMKEYS.BROWSER_KEY.getValue();

                msgMap.put(AppConstants.GCMKEYS.ACTIONS.getValue(), action);

                if(!ObjectUtils.isEmpty(payload)){
                    if(action.equalsIgnoreCase(AppConstants.GCMKEYS.CANNED_MESSAGE.getValue())){
                        Map<String, String> payLoadObj = new LinkedHashMap<String, String>();

                        String[] splitted = payload.split(AppConstants.StringReplaceChunks.HASH.getValue());
                        String coachMessage = splitted[0];
                        String coachMessageTitle = splitted[1];

                        payLoadObj.put(AppConstants.JsonKeys.TITLE.getValue(), coachMessageTitle);
                        payLoadObj.put(AppConstants.JsonKeys.MESSAGE.getValue(), coachMessage);

                        msgMap.put(AppConstants.GCMKEYS.PAYLOAD.getValue(), new EncryptionUtils().encrypt(JsonUtil.toJson(payLoadObj)));
                    } else {
                        msgMap.put(AppConstants.GCMKEYS.PAYLOAD.getValue(), new EncryptionUtils().encrypt(payload));
                    }
                }

                String data = JsonUtil.toJson(msgMap);
                Sender sender = new Sender(SENDER_ID);
                Message message = new Message.Builder().delayWhileIdle(false).addData(AppConstants.GCMKEYS.DATA.getValue(), data).build();
                MulticastResult result = sender.send(message, devicesTokensAndroid, 1);
                status = result.toString();
            }

        } catch(Exception ex){
            ex.printStackTrace();
        }

        LOG.info("PushNotificationManager/pushMessage: result: " + status);
        return status;
    }

    //for broadcasting to all members in the system
    public static String broadcast(User sharedBy, String action, String payload, @Context HttpServletRequest httpRequest){

        List<String> devicesTokenDebugDevicesStaging = new ArrayList<String>();
        List<String> devicesTokenReleaseDevicesStaging = new ArrayList<String>();
        List<String> devicesTokensAndroid = new ArrayList<String>();
        List<String> devicesTokenDebugDevicesProd = new ArrayList<String>();
        List<String> devicesTokenReleaseDevicesProd = new ArrayList<String>();

        DeviceDAO deviceDAO = new DeviceDAO();

        String status = null;
        try{

            //android devices
            devicesTokensAndroid = deviceDAO.getDevicesByManufacturer(AppConstants.DeviceTypes.ANDROID.getValue());

            //IOS staging debug/release devices
            devicesTokenDebugDevicesStaging = deviceDAO.getDevicesByManufacturer(AppConstants.DeviceTypes.IOS.getValue(), AppConstants.DeviceBuildTypes.DEBUG.getValue(), AppConstants.IOS_STAGING_APP_NAME);
            devicesTokenReleaseDevicesStaging = deviceDAO.getDevicesByManufacturer(AppConstants.DeviceTypes.IOS.getValue(), AppConstants.DeviceBuildTypes.RELEASE.getValue(), AppConstants.IOS_STAGING_APP_NAME);

            //IOS prod debug/release devices
            devicesTokenDebugDevicesProd = deviceDAO.getDevicesByManufacturer(AppConstants.DeviceTypes.IOS.getValue(), AppConstants.DeviceBuildTypes.DEBUG.getValue(), AppConstants.IOS_PROD_APP_NAME);
            devicesTokenReleaseDevicesProd = deviceDAO.getDevicesByManufacturer(AppConstants.DeviceTypes.IOS.getValue(), AppConstants.DeviceBuildTypes.RELEASE.getValue(), AppConstants.IOS_PROD_APP_NAME);

            //for staging debug devices sending push notification
            if(devicesTokenDebugDevicesStaging.size() > 0) {
                String certPath = httpRequest.getServletContext().getRealPath("/WEB-INF/" + AppConstants.STAGING_DEBUG_CERTIFICATE);
                ApnsService service = APNS.newService()
                        .withCert(certPath, AppConstants.DEV_KEY_STORE_PASSWORD)
                        .withSandboxDestination()
                        .build();

                String apnsPayLoad = buildMessagePayload(sharedBy, action, payload);

                LOG.info("iOS/staging/debug/pushMessage: DEBUG: Sending IOS Push notification, payload: " + apnsPayLoad);
                service.push(devicesTokenDebugDevicesStaging, apnsPayLoad);
                status = apnsPayLoad;
            }

            //for staging release devices sending push notification
            if(devicesTokenReleaseDevicesStaging.size() > 0) {
                String certPath = httpRequest.getServletContext().getRealPath("/WEB-INF/"+AppConstants.STAGING_RELEASE_CERTIFICATE);
                ApnsService service = APNS.newService()
                        .withCert(certPath, AppConstants.PROD_KEY_STORE_PASSWORD)
                        .withProductionDestination()
                        .build();

                String apnsPayLoad = buildMessagePayload(sharedBy, action, payload);

                LOG.info("iOS/staging/release/pushMessage: RELEASE: Sending IOS Push notification, payload: " + apnsPayLoad);
                service.push(devicesTokenReleaseDevicesStaging, apnsPayLoad);

                status = apnsPayLoad;
            }

            //for prod debug devices sending push notification
            if(devicesTokenDebugDevicesProd.size() > 0) {
                String certPath = httpRequest.getServletContext().getRealPath("/WEB-INF/" + AppConstants.PROD_DEBUG_CERTIFICATE);
                ApnsService service = APNS.newService()
                        .withCert(certPath, AppConstants.DEV_KEY_STORE_PASSWORD)
                        .withSandboxDestination()
                        .build();

                String apnsPayLoad = buildMessagePayload(sharedBy, action, payload);

                LOG.info("iOS/prod/debug/pushMessage: DEBUG: Sending IOS Push notification, payload: " + apnsPayLoad);
                service.push(devicesTokenDebugDevicesStaging, apnsPayLoad);
                status = apnsPayLoad;
            }

            //for prod release devices sending push notification
            if(devicesTokenReleaseDevicesProd.size() > 0) {
                String certPath = httpRequest.getServletContext().getRealPath("/WEB-INF/"+AppConstants.PROD_RELEASE_CERTIFICATE);
                ApnsService service = APNS.newService()
                        .withCert(certPath, AppConstants.PROD_KEY_STORE_PASSWORD)
                        .withProductionDestination()
                        .build();

                String apnsPayLoad = buildMessagePayload(sharedBy, action, payload);
                LOG.info("iOS/prod/release/pushMessage: RELEASE: Sending IOS Push notification, payload: " + apnsPayLoad);
                service.push(devicesTokenReleaseDevicesStaging, apnsPayLoad);

                status = apnsPayLoad;
            }

            if(devicesTokensAndroid.size() > 0){
                LOG.info("android/pushMessage: Sending Android Push notification, action: " + action + ", payLoad: " + payload);

                Map<String, Object> msgMap = new LinkedHashMap<String, Object>();
                String SENDER_ID = AppConstants.GCMKEYS.BROWSER_KEY.getValue();

                msgMap.put(AppConstants.GCMKEYS.ACTIONS.getValue(), action);

                if(!ObjectUtils.isEmpty(payload)){
                    if(action.equalsIgnoreCase(AppConstants.GCMKEYS.SHARED_VIDEO.getValue())){
                        String shareVideoTemplate = new SmsTemplateDAO().getSmsTemplateByName(AppConstants.SmsTemplates.SHARE_VIDEO.getValue());
                        String coachName = sharedBy.getFirstName();
                        shareVideoTemplate = shareVideoTemplate.replace(AppConstants.StringReplaceChunks.F_NAME.getValue(), coachName);
                        GroupPosts groupPost = new GroupPostsDAO().getGroupPostById(Long.parseLong(payload));
                        if(groupPost != null){
                            Provider provider = sharedBy.getProvider();
                            Map<String, String> payLoadObj = new LinkedHashMap<String, String>();
                            payLoadObj.put(AppConstants.PushNotificationConstants.LINK.getValue(), groupPost.getVideoEmbedUrl());
                            payLoadObj.put(AppConstants.PushNotificationConstants.DESCRIPTION.getValue(), groupPost.getDescription());
                            payLoadObj.put(AppConstants.PushNotificationConstants.ALERT.getValue(), shareVideoTemplate);
                            payLoadObj.put(AppConstants.PushNotificationConstants.IMAGE.getValue(), null);
                            if(provider != null){
                                payLoadObj.put(AppConstants.PushNotificationConstants.IMAGE.getValue(), ObjectUtils.nullSafe(provider.getImagePath()));
                            }
                            msgMap.put(AppConstants.GCMKEYS.PAYLOAD.getValue(), new EncryptionUtils().encrypt(JsonUtil.toJson(payLoadObj)));
                        }else{
                            LOG.info("android/pushMessage: unable to get group post by id");
                        }
                    }else{
                        msgMap.put(AppConstants.GCMKEYS.PAYLOAD.getValue(), new EncryptionUtils().encrypt(payload));
                    }
                }

                String data = JsonUtil.toJson(msgMap);
                Sender sender = new Sender(SENDER_ID);
                Message message = new Message.Builder().delayWhileIdle(false).addData(AppConstants.GCMKEYS.DATA.getValue(), data).build();
                MulticastResult result = sender.send(message, devicesTokensAndroid, 1);
                status = result.toString();
            }

        } catch(Exception ex){
            ex.printStackTrace();
        }

        LOG.info("PushNotificationManager/pushMessage: result: " + status);
        return status;
    }

    public static String buildPayLoadForIOSDevices(String action){
        Map<String, Object> aps = new LinkedHashMap<String, Object>();
        Map<String, String> apsContent = new LinkedHashMap<String, String>();

        apsContent.put(AppConstants.PushNotificationConstants.CONTENT_AVAILABLE.getValue(), "1");
        apsContent.put(AppConstants.PushNotificationConstants.EVENT.getValue(), action);
        aps.put(AppConstants.PushNotificationConstants.APS.getValue(), apsContent);

        return JsonUtil.toJson(aps);
    }

    public static String buildLoudPayLoadForIOSDevices(String action, String alertMessage, Map<String, String> payLoad){
        Map<String, Object> aps = new LinkedHashMap<String, Object>();

        Map<String, Object> alertContent = new LinkedHashMap<String, Object>();
        alertContent.put(AppConstants.PushNotificationConstants.ALERT.getValue(), alertMessage);
        alertContent.put(AppConstants.PushNotificationConstants.BADGE.getValue(), "0");
        alertContent.put(AppConstants.PushNotificationConstants.SOUND.getValue(), AppConstants.SOUND_NAME);

        aps.put(AppConstants.PushNotificationConstants.APS.getValue(), alertContent);
        aps.put(AppConstants.PushNotificationConstants.DATA.getValue(), payLoad);
        aps.put(AppConstants.PushNotificationConstants.EVENT.getValue(), action);

        return JsonUtil.toJson(aps);
    }

    private static String buildMessagePayload(String action, String payload) {
        String apnsPayLoad = "";

        if (action.equalsIgnoreCase(AppConstants.GCMKEYS.NOTIFY_COMMENT.getValue())) {
            String postCommentTemplate = new SmsTemplateDAO().getSmsTemplateByName(AppConstants.SmsTemplates.POST_COMMENT.getValue());

            Map<String, String> payLoadObj = new LinkedHashMap<String, String>();
            payLoadObj.put(AppConstants.PushNotificationConstants.POST_ID.getValue(), payload);

            apnsPayLoad = buildLoudPayLoadForIOSDevices(action, postCommentTemplate, payLoadObj);
        } else if (action.equalsIgnoreCase(AppConstants.GCMKEYS.SHARE_MEAL_PERMISSION.getValue())) {
            String shareMealTemplate = new SmsTemplateDAO().getSmsTemplateByName(AppConstants.SmsTemplates.SHARE_MEAL_CONSENT.getValue());

            Log log = new LogDAO().getLogById(payload);
            if (log != null && log.getFoodLogSummary() != null) {
                shareMealTemplate = shareMealTemplate.replace(AppConstants.StringReplaceChunks.F_NAME.getValue(), ObjectUtils.nullSafe(log.getFoodLogSummary().getMealName()));
            }

            Map<String, String> payLoadObj = new LinkedHashMap<String, String>();
            payLoadObj.put(AppConstants.PushNotificationConstants.LOG_ID.getValue(), payload);

            apnsPayLoad = buildLoudPayLoadForIOSDevices(action, shareMealTemplate, payLoadObj);
        } else if (action.equalsIgnoreCase(AppConstants.GCMKEYS.CANNED_MESSAGE.getValue())) {

            String[] splitted = payload.split(AppConstants.StringReplaceChunks.HASH.getValue());
            String coachMessage = splitted[0];
            String coachMessageTitle = splitted[1];

            Map<String, String> payLoadObj = new LinkedHashMap<String, String>();
            payLoadObj.put(AppConstants.JsonKeys.TITLE.getValue(), coachMessageTitle);
            payLoadObj.put(AppConstants.JsonKeys.MESSAGE.getValue(), coachMessage);

            apnsPayLoad = buildLoudPayLoadForIOSDevices(action, coachMessage, payLoadObj);
        } else {
            apnsPayLoad = buildPayLoadForIOSDevices(action);
        }

        return apnsPayLoad;
    }

    private static String buildMessagePayload(User sharedBy, String action, String payload) {
        String apnsPayLoad = "";

        if (action.equalsIgnoreCase(AppConstants.GCMKEYS.NOTIFY_COMMENT.getValue())) {
            String postCommentTemplate = new SmsTemplateDAO().getSmsTemplateByName(AppConstants.SmsTemplates.POST_COMMENT.getValue());

            Map<String, String> payLoadObj = new LinkedHashMap<String, String>();
            payLoadObj.put(AppConstants.PushNotificationConstants.POST_ID.getValue(), payload);

            apnsPayLoad = buildLoudPayLoadForIOSDevices(action, postCommentTemplate, payLoadObj);
        } else if (action.equalsIgnoreCase(AppConstants.GCMKEYS.SHARE_MEAL_PERMISSION.getValue())) {
            String shareMealTemplate = new SmsTemplateDAO().getSmsTemplateByName(AppConstants.SmsTemplates.SHARE_MEAL_CONSENT.getValue());

            Log log = new LogDAO().getLogById(payload);
            if (log != null && log.getFoodLogSummary() != null) {
                shareMealTemplate = shareMealTemplate.replace(AppConstants.StringReplaceChunks.F_NAME.getValue(), ObjectUtils.nullSafe(log.getFoodLogSummary().getMealName()));
            }

            Map<String, String> payLoadObj = new LinkedHashMap<String, String>();
            payLoadObj.put(AppConstants.PushNotificationConstants.LOG_ID.getValue(), payload);

            apnsPayLoad = buildLoudPayLoadForIOSDevices(action, shareMealTemplate, payLoadObj);
        } else if (action.equalsIgnoreCase(AppConstants.GCMKEYS.CANNED_MESSAGE.getValue())) {
            String[] splitted = payload.split(AppConstants.StringReplaceChunks.HASH.getValue());
            String coachMessage = splitted[0];
            String coachMessageTitle = splitted[1];

            Map<String, String> payLoadObj = new LinkedHashMap<String, String>();
            payLoadObj.put(AppConstants.JsonKeys.TITLE.getValue(), coachMessageTitle);
            payLoadObj.put(AppConstants.JsonKeys.MESSAGE.getValue(), coachMessage);

            apnsPayLoad = buildLoudPayLoadForIOSDevices(action, coachMessage, payLoadObj);

        }else if(action.equalsIgnoreCase(AppConstants.GCMKEYS.SHARED_VIDEO.getValue())){

            String shareVideoTemplate = new SmsTemplateDAO().getSmsTemplateByName(AppConstants.SmsTemplates.SHARE_VIDEO.getValue());
            String coachName = sharedBy.getFirstName();
            shareVideoTemplate = shareVideoTemplate.replace(AppConstants.StringReplaceChunks.F_NAME.getValue(), coachName);
            GroupPosts groupPost = new GroupPostsDAO().getGroupPostById(Long.parseLong(payload));
            if(groupPost != null){
                Provider provider = sharedBy.getProvider();
                Map<String, String> payLoadObj = new LinkedHashMap<String, String>();
                payLoadObj.put(AppConstants.PushNotificationConstants.LINK.getValue(), groupPost.getVideoEmbedUrl());
                payLoadObj.put(AppConstants.PushNotificationConstants.DESCRIPTION.getValue(), groupPost.getDescription());
                payLoadObj.put(AppConstants.PushNotificationConstants.IMAGE.getValue(), null);
                if(provider != null){
                    payLoadObj.put(AppConstants.PushNotificationConstants.IMAGE.getValue(), ObjectUtils.nullSafe(provider.getImagePath()));
                }
                apnsPayLoad = buildLoudPayLoadForIOSDevices(action, shareVideoTemplate, payLoadObj);
            }else{
                LOG.info("iOS/staging/release/pushMessage: unable to get group post by id");
            }
        } else {
            apnsPayLoad = buildPayLoadForIOSDevices(action);
        }

        return apnsPayLoad;
    }
}
