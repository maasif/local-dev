package com.healthslate.patientapp.util;

import java.lang.Object;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.OneToOneReminderSessionDTO;
import com.healthslate.patientapp.model.dto.ReminderSmsDTO;
import com.healthslate.patientapp.model.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
* ======= FILE CHANGE HISTORY =======
* [1/29/2015]: getting offsetKey for sending reminder from User Table now, __oz
* ===================================
 */

public class RemindersUtil {
	static final Logger LOG = LoggerFactory.getLogger(RemindersUtil.class);

	public static void sendMedicationSmsReminders() {
		List<ReminderSmsDTO> remindersList = new TargetDAO().getReminderTimes();
		try {

            //creating log in db to track when this scheduler runs
            new SchedulerRunLogDAO().saveSchedulerLog("Medication Reminder Scheduler (sendMedicationSmsReminders)", "Every 1 minute");
            LOG.info("Medication Reminder Scheduler (sendMedicationSmsReminders), run date: " + new Date());

			if(remindersList != null && remindersList.size() > 0){
				for (ReminderSmsDTO reminderSms : remindersList) {
					//__oz [1/29/2015]
					Patient patient = new PatientDAO().getPatientByIdScheduler(reminderSms.getPatientId());
                    String dateStr = "";
                    String offsetKey = "";

                    User user  = patient.getUser();
                    if(user != null){
                        offsetKey = user.getOffsetKey();
                    }

                    Long facilityTimezoneMillis = reminderSms.getFacilityTimeMillis();

                    if(facilityTimezoneMillis != null){
                        dateStr = DateUtils.getOffsetTime(facilityTimezoneMillis.intValue());
                    }else{
                        dateStr = DateUtils.getCurrentTimeByOffset(offsetKey);
                    }

					if(dateStr.equalsIgnoreCase(reminderSms.getTime())){

                        String todaysFormattedDate = DateUtils.getFormattedDate(System.currentTimeMillis(), DateUtils.DAY_FORMAT);
                        SmsTemplateDAO smsTemplateDAO = new SmsTemplateDAO();

                        String MESSAGE_BODY_DAILY = smsTemplateDAO.getSmsTemplateByNameScheduler(AppConstants.SmsTemplates.DAILY_REMINDER.getValue()),
                               MESSAGE_BODY_WEEKLY = smsTemplateDAO.getSmsTemplateByNameScheduler(AppConstants.SmsTemplates.WEEKLY_REMINDER.getValue());

						if(ObjectUtils.isEmpty(reminderSms.getDay()) || reminderSms.getDay().equalsIgnoreCase(todaysFormattedDate)) {

							String messageBody = MESSAGE_BODY_WEEKLY;
							if(ObjectUtils.isEmpty(reminderSms.getDay())) { // if empty then it means msg should be sent daily
								messageBody = MESSAGE_BODY_DAILY;
							}

                            if(CommonUtils.isValidPhoneNumber(reminderSms.getPhoneNumber())){
                                //sending SMS to user's phone number
                                LOG.info("Sending Reminder through SMS to: "+reminderSms.getPhoneNumber()+" with msg: "+messageBody);
                                Map<String, String> result = TwilioMessageUtils.sendScheduledSMS(reminderSms.getPhoneNumber(), messageBody);
                                String status = result.get(AppConstants.JsonConstants.STATUS.name());
                                LOG.info("Reminder sent to: "+reminderSms.getPhoneNumber()+", status: "+status);
                                if(status.equals(AppConstants.JsonConstants.SUCCESS.name())){
                                    SmsReply smsReply = new SmsReply();
                                    smsReply.setPatientId(reminderSms.getPatientId());
                                    smsReply.setMessageBody(messageBody);
                                    smsReply.setSentTime(System.currentTimeMillis());
                                    smsReply.setFromPhoneNumber(reminderSms.getPhoneNumber());
                                    new SmsReplyDAO().saveOrUpdateSmsReply(smsReply);
                                    LOG.info("Saved Reminder SMS Sent Log to DB: "+smsReply.toString());
                                }
                            }else{
                                LOG.info("sendMedicationSmsReminders: invalid phone number: " + reminderSms.getPhoneNumber());
                            }
						} 
					}					
				}
			}								
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("sendMedicationSmsReminders: Exception: "+e.getMessage());
		}
	}

	public static void sendNewPatientEmailReminderFor3Days() {
		try {

            //creating log in db to track when this scheduler runs
            new SchedulerRunLogDAO().saveSchedulerLog("Sending coaches email after 3 days to member joining (sendNewPatientEmailReminderFor3Days)", "Every 3 hours");
            LOG.info("Sending coaches email after 3 days to member joining (sendNewPatientEmailReminderFor3Days), run date: " + new Date());

			UserDAO userDAO = new UserDAO();
			List<User> users = userDAO.getUsersWithNewlyRegistration();
			String appPath = new PreferencesDAO().getPreferenceSchedule(AppConstants.PreferencesNames.APPLICATION_PATH.getValue());
			if (users != null && users.size() > 0) {
				for (User user : users) {
                    long []daysPassed = DateUtils.getElapsedTimes(user.getRegistrationDate().getTime(), System.currentTimeMillis());
                    if(daysPassed[0] > 3){
                        LOG.info("sendNewPatientEmailReminderFor3Days: Sending Email On 4th day");
                        Patient patient = new PatientDAO().getPatientByUserIdScheduler(user.getUserId());
                        List<User> coaches = new ArrayList<User>();
                        if(patient.getPrimaryFoodCoachId() != null && patient.getPrimaryFoodCoachId() > 0) {
                            Provider provider = new ProviderDAO().getProviderByIdScheduler(patient.getPrimaryFoodCoachId());
                            coaches.add(provider.getUser());
                        } else {
                            coaches = new ProviderDAO().getFoodCoachesByPatientIdScheduler(patient.getPatientId());
                        }
                        LOG.info("sendNewPatientEmailReminderFor3Days: Sending Email Reminder for Patient Id: "+patient.getPatientId());
                        boolean isEmailSent = NotifyUtils.remindFoodCoachesForNewMember3Days(appPath, user.getPatient().getPatientId(), coaches);
                        if(isEmailSent) {
                            patient.setIsThreeDayEmailSent(isEmailSent);
                            new BaseDAO().saveSchedular(patient);
                        }
                    }
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("sendNewPatientEmailReminderFor3Days: Exception: "+e.getMessage());
		}
	}

    public static void sendNewPatientEmailReminderFor7Days() {
        try {

            //creating log in db to track when this scheduler runs
            new SchedulerRunLogDAO().saveSchedulerLog("Sending coaches email after 7 days to member joining (sendNewPatientEmailReminderFor3Days)", "Every 4 hours");
            LOG.info("Sending coaches email after 7 days to member joining (sendNewPatientEmailReminderFor3Days), run date: " + new Date());

            UserDAO userDAO = new UserDAO();
            List<User> users = userDAO.getUsersWithNewlyRegistration();
            String appPath = new PreferencesDAO().getPreferenceSchedule(AppConstants.PreferencesNames.APPLICATION_PATH.getValue());
            if (users != null && users.size() > 0) {
                for (User user : users) {
                    long []daysPassed = DateUtils.getElapsedTimes(user.getRegistrationDate().getTime(), System.currentTimeMillis());
                    if(daysPassed[0] > 6){
                        LOG.info("sendNewPatientEmailReminderFor7Days: Sending Email On 7th day");
                        Patient patient = new PatientDAO().getPatientByUserIdScheduler(user.getUserId());
                        List<User> coaches = new ArrayList<User>();
                        if(patient.getPrimaryFoodCoachId() != null && patient.getPrimaryFoodCoachId() > 0) {
                            Provider provider = new ProviderDAO().getProviderByIdScheduler(patient.getPrimaryFoodCoachId());
                            coaches.add(provider.getUser());
                        } else {
                            coaches = new ProviderDAO().getFoodCoachesByPatientIdScheduler(patient.getPatientId());
                        }
                        LOG.info("sendNewPatientEmailReminderFor7Days: Sending Email Reminder for Patient Id: "+patient.getPatientId());
                        boolean isEmailSent = NotifyUtils.remindFoodCoachesForNewMemberAfter7Days(appPath, user.getPatient().getPatientId(), coaches);
                        if(isEmailSent) {
                            patient.setIsThreeDayEmailSent(isEmailSent);
                            new BaseDAO().saveSchedular(patient);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("sendNewPatientEmailReminderFor7Days: Exception: "+e.getMessage());
        }
    }

	public static void sendMessagesForGoalAssessment() {
		try {

            //creating log in db to track when this scheduler runs
            new SchedulerRunLogDAO().saveSchedulerLog("Goal Assessment Scheduler (sendMessagesForGoalAssessment)", "Every 15 minutes");
            LOG.info("Goal Assessment Scheduler (sendMessagesForGoalAssessment), run date: " + new Date());

			FilledFormDAO filledFormDAO =  new FilledFormDAO();
			List<Object> objects = filledFormDAO.getNonRatedGoals();
			if(objects != null) {
				for (Object object: objects) {
					Object[] objectArray = (Object []) object;
					Long patientId = 0L;
                    String deviceType = ObjectUtils.nullSafe(objectArray[6]);
					if(objectArray[1] instanceof Long) {
						patientId = (Long) objectArray[1];
					} else {
						patientId = ((BigInteger)objectArray[1]).longValue();
					}
					Long filledFormId = 0L;
					if(objectArray[3] instanceof Long) {
						filledFormId = (Long) objectArray[3];
					} else {
						filledFormId = ((BigInteger)objectArray[3]).longValue();
					}
					FilledForm filledForm = filledFormDAO.getFormByIdScheduler(filledFormId);
					String body = new SmsTemplateDAO().getSmsTemplateByNameScheduler(AppConstants.SmsTemplates.GOAL_REMINDER_MESSAGE.getValue());

                    body = body.replace(AppConstants.StringReplaceChunks.ID.getValue(), filledForm.getFilledFormId()+"");

                    if(deviceType.equalsIgnoreCase(AppConstants.DeviceTypes.IOS.getValue())){
                        body = body.replace(AppConstants.HTTP_KEY, AppConstants.IOS_SMS_KEY);
                    }

					String phoneNumber = ObjectUtils.nullSafe(objectArray[0]);
					int notificationCount  = ObjectUtils.nullSafeInteger(objectArray[4]);
					if(notificationCount == 0) {
						LOG.info("sendMessagesForGoalAssessment: Sending message to patient, patientId: "+patientId+", Phone Number: "+phoneNumber+", Body: "+body + ", goalId: "+ filledForm.getFilledFormId());
						Map<String, String> returnMap = TwilioMessageUtils.sendScheduledSMS(phoneNumber, body);
						if(returnMap.get(AppConstants.JsonConstants.STATUS.name()).equalsIgnoreCase(AppConstants.JsonConstants.SUCCESS.name())) {
							notificationCount++;
							filledForm.setNotificationCount(notificationCount);
							filledFormDAO.saveSchedular(filledForm);
						}
					} else if(notificationCount == 1) {
						// Check if msg sent time is greater than 16
						Date createdDate = (Date) objectArray[2];
						long [] timeDifference = DateUtils.getElapsedTimes(createdDate.getTime(), System.currentTimeMillis());
						if(timeDifference[0] > 8) { //16
							LOG.info("sendMessagesForGoalAssessment: Sending 2nd message to patient, patientId: "+patientId+", Phone Number: "+phoneNumber+", Body: "+body+", goalId: "+ filledForm.getFilledFormId());
							Map<String, String> returnMap = TwilioMessageUtils.sendScheduledSMS(phoneNumber, body);
							if(returnMap.get(AppConstants.JsonConstants.STATUS.name()).equalsIgnoreCase(AppConstants.JsonConstants.SUCCESS.name())) {
								notificationCount++;
								filledForm.setNotificationCount(notificationCount);
								filledFormDAO.saveSchedular(filledForm);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("sendMessagesForGoalAssessment: Exception: "+e.getMessage());
		}
	}

    public static void sendOneToOneSessionReminders(String todayOrTomorrowReminder, String schedulerOccurrence) {
        try {

            OneToOneSessionsDAO oneToOneSessionsDAO = new OneToOneSessionsDAO();
            SmsTemplateDAO smsTemplateDAO = new SmsTemplateDAO();
            int minutes = 0;
            String stringMinutes = new PreferencesDAO().getPreferenceSchedule(todayOrTomorrowReminder);
            if(!ObjectUtils.isEmpty(stringMinutes)){
                minutes = Integer.parseInt(stringMinutes);
            }

            //creating log in db to track when this scheduler runs
            new SchedulerRunLogDAO().saveSchedulerLog("1:1 Session reminders (sendOneToOneSessionReminders) minutes duration: " + stringMinutes, schedulerOccurrence);
            LOG.info("1:1 Session reminders (sendOneToOneSessionReminders), minutes duration: " + stringMinutes + ", run date: " + new Date());

            List<OneToOneReminderSessionDTO> remindersList = oneToOneSessionsDAO.getOneToOneSessionRemindersList(todayOrTomorrowReminder);

            String smsTemplate = smsTemplateDAO.getSmsTemplateByNameScheduler(AppConstants.SmsTemplates.VIDEO_CALL_TODAY_MESSAGE.getValue());

            if(remindersList != null && remindersList.size() > 0){
                for(OneToOneReminderSessionDTO sessionDTO: remindersList){

                    if(todayOrTomorrowReminder.equalsIgnoreCase(AppConstants.PreferencesNames.SECOND_ONE_TO_ONE_SESSION.getValue())){
                        smsTemplate = smsTemplateDAO.getSmsTemplateByNameScheduler(AppConstants.SmsTemplates.VIDEO_CALL_TOMORROW_MESSAGE.getValue());
                        long timeMillis = sessionDTO.getReminderSession() + sessionDTO.getFacilityOffsetTime();
                        smsTemplate = smsTemplate.replace(AppConstants.StringReplaceChunks.TIME.getValue(), DateUtils.getFormattedDate(timeMillis, DateUtils.TIME_FORMAT));
                        smsTemplate = smsTemplate.replace(AppConstants.StringReplaceChunks.DATE.getValue(), DateUtils.getFormattedDate(timeMillis, DateUtils.DATE_FORMAT_ONLY_MONTH));
                    }

                   /* boolean isSent = EmailUtils.sendOneToOneSessionEmail(sessionDTO.getEmail(), "1:1 Session Reminder", smsTemplate, true);
                    if(isSent){
                        LOG.info("sendOneToOneSessionReminders: Email sent to: "+sessionDTO.getEmail()+", status: "+isSent+ ", sessionId: " + sessionDTO.getSessionId());
                        oneToOneSessionsDAO.updateOneToOneSession(sessionDTO.getReminderSession(), sessionDTO.getPatientId(), sessionDTO.getReminderSent(), todayOrTomorrowReminder);
                    }else{
                        LOG.info("sendOneToOneSessionReminders: Unable to send SMS to: "+sessionDTO.getEmail()+", status: "+isSent);
                    }*/

                    Map<String, String> results = TwilioMessageUtils.sendScheduledSMS(sessionDTO.getPhone(), smsTemplate);
                    String status = results.get(AppConstants.JsonConstants.STATUS.name());
                    if(status.equals(AppConstants.JsonConstants.SUCCESS.name())){
                        LOG.info("sendOneToOneSessionReminders: SMS sent to: "+sessionDTO.getPhone()+", status: "+status+ ", sessionId: " + sessionDTO.getSessionId());
                        oneToOneSessionsDAO.updateOneToOneSession(sessionDTO.getReminderSession(), sessionDTO.getPatientId(), sessionDTO.getReminderSent(), todayOrTomorrowReminder);
                    }else{
                        LOG.info("sendOneToOneSessionReminders: Unable to send SMS to: "+sessionDTO.getPhone()+", status: "+status);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("sendOneToOneSessionReminders: Exception: "+e.getMessage());
        }
    }
}



