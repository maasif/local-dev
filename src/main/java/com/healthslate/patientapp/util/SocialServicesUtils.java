package com.healthslate.patientapp.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.healthslate.patientapp.model.dao.PreferencesDAO;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.ws.SocialGroupServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SocialServicesUtils {
	static final Logger LOG = LoggerFactory.getLogger(SocialServicesUtils.class);
	public static String getSocialData(String method) throws ClientProtocolException, IOException{
		String url = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.ELGG_WEB_SERVICE_PATH.getValue()) + method;
		LOG.info("getSocialData: Hitting elgg url: "+url);
		String response = null;
		try {
			response = NetworkUtils.getRequest(url);
		}
		catch (ClientProtocolException e) {
			e.printStackTrace();
			LOG.info("getSocialData: ClientProtocolException: "+e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			LOG.info("getSocialData: IOException: "+e.getMessage());
		}
		LOG.info("getSocialData: Returning response: "+response);
		return response;
	}
	
	@SuppressWarnings("unchecked")
	public static String postSocialData(String jsonString, String method) throws ClientProtocolException, IOException{
		String url = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.ELGG_WEB_SERVICE_PATH.getValue()) + method;
		LOG.info("postSocialData: Hitting elgg url: "+url+"\nwith data: "+jsonString);
		String response = null;
		
		try {
			List<NameValuePair> nameValuePairs = getDataInPairs(jsonString);
			if(nameValuePairs != null){
				response = NetworkUtils.postRequest(url, nameValuePairs);
                LOG.info("postSocialData: response from elgg: "+response);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			LOG.info("postSocialData: ClientProtocolException: "+e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			LOG.info("postSocialData: IOException: "+e.getMessage());
		}
		LOG.info("postSocialData: Returning response: "+response);
		return response;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List getDataInPairs(String jsonString){
		JSONObject object;
		List nameValuePairs = null;
		
		try {
			object = new JSONObject(jsonString);
			String[] keys = JSONObject.getNames(object);
			
			nameValuePairs = new ArrayList<NameValuePair>(keys.length);
			for (String key : keys)
			{
			    Object value = object.get(key);
			    nameValuePairs.add(new BasicNameValuePair(key, value.toString()));
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return nameValuePairs;
	}
	
	public static int patientJoinGroup(User user, int groupId) throws ClientProtocolException, IOException {
		Map<String, String> dataMap = new LinkedHashMap<String, String>();
		dataMap.put(AppConstants.SOCIALKEYS.USERNAME.getValue(), user.getEmail());
		dataMap.put(AppConstants.SOCIALKEYS.GROUP_ID.getValue(), String.valueOf(groupId));
		String jsonString = new Gson().toJson(dataMap);
		String response = SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.GROUP_JOIN.getValue());
		int status = CommonUtils.getStatusFromElgg(response);
		return status;
	}
	
	public static int patientJoinGroup(Patient pat, int groupId) throws ClientProtocolException, IOException {
		Map<String, String> dataMap = new LinkedHashMap<String, String>();
		dataMap.put(AppConstants.SOCIALKEYS.USERNAME.getValue(), pat.getUser().getEmail());
		dataMap.put(AppConstants.SOCIALKEYS.GROUP_ID.getValue(), String.valueOf(groupId));
		String jsonString = new Gson().toJson(dataMap);
		String response = SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.GROUP_JOIN.getValue());
		int status = CommonUtils.getStatusFromElgg(response);
		return status;
	}

	public static int createPatientInSocialGroup(User user) throws ClientProtocolException, IOException {
		Map<String, String> dataMap = new LinkedHashMap<String, String>();
		int socialId = -1;
		dataMap.put(AppConstants.SOCIALKEYS.USERNAME.getValue(), user.getEmail());

        String name = user.getDisplayName();
        String password = user.getPassword();

        if(ObjectUtils.isEmpty(name)){
            name = ObjectUtils.nullSafe(user.getFirstName()) + " " + ObjectUtils.nullSafe(user.getLastName());
        }

        if(ObjectUtils.isEmpty(password)){
            password = "123!!@";
        }

		dataMap.put(AppConstants.SOCIALKEYS.NAME.getValue(), name);
		dataMap.put(AppConstants.SOCIALKEYS.EMAIL.getValue(), user.getEmail());
		dataMap.put(AppConstants.SOCIALKEYS.PASSWORD.getValue(), password);
		
		String jsonString = new Gson().toJson(dataMap);
		String response = SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.CREATE_USER.getValue());
		JsonElement jelement = new JsonParser().parse(response);
        JsonObject jobject = jelement.getAsJsonObject();
        
        int statusFromElgg = jobject.get(AppConstants.SOCIALKEYS.STATUS.getValue()).getAsInt();
        
        if(statusFromElgg == 0){
        	jelement = jobject.get(AppConstants.SOCIALKEYS.RESULT.getValue());
            JsonObject object = jelement.getAsJsonObject();
            socialId = object.get(AppConstants.SOCIALKEYS.GUID.getValue()).getAsInt();
        }
		return socialId;
	}

	public static void removeUserFromElgg(final String email){

		new Thread(new Runnable() {
			@Override
			public void run() {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.USERNAME.getValue(), email));
				String method = AppConstants.SOCIALKEYS.DELETE_USER.getValue();
				String url = new PreferencesDAO().getPreferenceSchedule(AppConstants.PreferencesNames.ELGG_WEB_SERVICE_PATH.getValue()) + method;
				LOG.info("removeUserFromElgg: url to hit: "+url);
				try {
					String response = NetworkUtils.postRequest(url, nameValuePairs);
					LOG.info("removeUserFromElgg: Elgg returning response: "+response);
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
}
