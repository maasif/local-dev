package com.healthslate.patientapp.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.healthslate.patientapp.model.dao.PreferencesDAO;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.healthslate.patientapp.model.dto.TwilioMessageDTO;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Account;
import com.twilio.sdk.resource.instance.Sms;
import com.twilio.sdk.resource.list.SmsList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TwilioMessageUtils {

    static final Logger LOG = LoggerFactory.getLogger(TwilioMessageUtils.class);

    public static Map<String, String> sendSMS(String num, String body) {

        LOG.info("sendSMS: sending SMS method start");

        Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		
		/* Find your sid and token at twilio.com/user/account */
		boolean isSent = false;

        PreferencesDAO preferencesDAO = new PreferencesDAO();
        String accountSSID = preferencesDAO.getPreference(AppConstants.PreferencesNames.TWILIO_ACCOUNT_SSID.getValue());
        String authToken = preferencesDAO.getPreference(AppConstants.PreferencesNames.TWILIO_AUTH_TOKEN.getValue());
        String twilioNumber = preferencesDAO.getPreference(AppConstants.PreferencesNames.TWILIO_NUMBER.getValue());

		TwilioRestClient client = new TwilioRestClient(accountSSID, authToken);

		Account account = client.getAccount();
		MessageFactory messageFactory = account.getMessageFactory();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("To", num)); // Replace with a valid phone number for your account.
		params.add(new BasicNameValuePair("From", twilioNumber));
		params.add(new BasicNameValuePair("Body", body));
		try {
            LOG.info("sendSMS: send SMS start");
			com.twilio.sdk.resource.instance.Message sms = messageFactory.create(params);
            LOG.info("sendSMS: send SMS end");
            LOG.info("sendSMS: SMS SID: " + sms.getSid());
			returnMap.put(AppConstants.JsonConstants.DATA.name(), sms.getSid());
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
			isSent = true;
			LOG.info("sendSMS: SMS sent successfully at "+num+" with message body: "+body+", From twilioNumber: "+twilioNumber);
		} catch (TwilioRestException e) {
			e.printStackTrace();
			isSent = false;
			LOG.info("sendSMS: Sending SMS From Twilio failed with exception: "+e.getMessage());
            emailAuthorityForFundsZero(e, false);
		}

        LOG.info("sendSMS: SMS to: "+num+", SMS SENT? "+isSent);
        LOG.info("sendSMS: sending SMS method end");

		return returnMap;
	}

	public static Map<String, String> sendScheduledSMS(String num, String body) {

        LOG.info("sendScheduledSMS: sending SMS method start");

		Map<String, String> returnMap = new LinkedHashMap<String, String>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		
		/* Find your sid and token at twilio.com/user/account */
		boolean isSent = false;

        PreferencesDAO preferencesDAO = new PreferencesDAO();
        String accountSSID = preferencesDAO.getPreferenceSchedule(AppConstants.PreferencesNames.TWILIO_ACCOUNT_SSID.getValue());
        String authToken = preferencesDAO.getPreferenceSchedule(AppConstants.PreferencesNames.TWILIO_AUTH_TOKEN.getValue());
        String twilioNumber = preferencesDAO.getPreferenceSchedule(AppConstants.PreferencesNames.TWILIO_NUMBER.getValue());

		TwilioRestClient client = new TwilioRestClient(accountSSID, authToken);
		Account account = client.getAccount();
		MessageFactory messageFactory = account.getMessageFactory();
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("To", num)); // Replace with a valid phone number for your account.
		params.add(new BasicNameValuePair("From", twilioNumber));
		params.add(new BasicNameValuePair("Body", body));
		try {
            LOG.info("sendSMS: send SMS start");
            com.twilio.sdk.resource.instance.Message sms = messageFactory.create(params);
            LOG.info("sendSMS: send SMS end");

            LOG.info("sendSMS: SMS SID: " + sms.getSid());
			returnMap.put(AppConstants.JsonConstants.DATA.name(), sms.getSid());
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());		
			isSent = true;
			LOG.info("sendSMS: SMS sent successfully at "+num+" with message body: "+body+", From twilioNumber: "+twilioNumber);
		} catch (TwilioRestException e) {
			e.printStackTrace();
            isSent = false;
            LOG.info("sendScheduledSMS: Sending SMS From Twilio failed with exception: "+e.getMessage());
            emailAuthorityForFundsZero(e, true);
        }

        LOG.info("sendScheduledSMS: SMS to: "+num+", SMS SENT? "+isSent);
        LOG.info("sendScheduledSMS: sending SMS method end");

		return returnMap;
	}

    //pass empty string for parameter which you don't want to add
    public static List<TwilioMessageDTO> getTwilioSMSList(String to, String from, String dateSent) {
        PreferencesDAO preferencesDAO = new PreferencesDAO();
        String accountSSID = preferencesDAO.getPreference(AppConstants.PreferencesNames.TWILIO_ACCOUNT_SSID.getValue());
        String authToken = preferencesDAO.getPreference(AppConstants.PreferencesNames.TWILIO_AUTH_TOKEN.getValue());
        List<TwilioMessageDTO> twilioMessageDTOs = null;

        try {
            TwilioRestClient client = new TwilioRestClient(accountSSID, authToken);
            Map<String, String> filters = new HashMap<String, String>();

            if(to != ""){
                filters.put("To", to);
            }

            if(from != ""){
                filters.put("From", from);
            }

            if(dateSent != "") {
                filters.put("DateSent", dateSent);
            }

            SmsList smss = client.getAccount().getSmsMessages(filters);

            twilioMessageDTOs = new ArrayList<TwilioMessageDTO>();

            for (Sms sms: smss.getPageData()) {
                twilioMessageDTOs.add(new TwilioMessageDTO(sms.getDirection(), sms.getDateSent().getTime(), sms.getFrom(), sms.getTo(), sms.getStatus(), sms.getBody(), sms.getSid()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return twilioMessageDTOs;
    }

    public static void emailAuthorityForFundsZero(TwilioRestException e, boolean isSchedule){
        try {
            Map<String, String> prefMap = null;
            PreferencesDAO preferencesDAO = new PreferencesDAO();
            List<String> emailList = null;

            if(isSchedule){
                prefMap = preferencesDAO.mapSchedular();
            }else{
                prefMap = preferencesDAO.map();
            }

            String prefTwilioZero = prefMap.get(AppConstants.PreferencesNames.TWILIO_FUNDS_ZERO.getValue());
            emailList = CommonUtils.stringToList(prefTwilioZero);

            if(emailList != null && emailList.size() > 0){
                //20003 means: You lack permission to access the resource and method you requested.
                if(e.getErrorCode() == 20003){
                    for(String email: emailList){
                        boolean isSentEmail = EmailUtils.sendTextInEmail("Twilio Account is currently suspended due to a lack of funds",
                                email,
                                prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                                prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                                AppConstants.EmailSubjects.TWILIO_FUNDS_ZERO.getValue());

                        LOG.info("emailAuthorityForFundsZero: Sending Email Twilio Zero Funds to: " + email + ", emailSent: " + isSentEmail);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}