package com.healthslate.patientapp.ws;

import com.healthslate.patientapp.model.dao.FacilityDAO;
import com.healthslate.patientapp.model.dao.PreferencesDAO;
import com.healthslate.patientapp.model.dao.UserDAO;
import com.healthslate.patientapp.model.dto.CoachDTO;
import com.healthslate.patientapp.model.dto.FacilityDTO;
import com.healthslate.patientapp.model.entity.Facility;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.JsonUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/*
 * ======= FILE CHANGE HISTORY =======
 * [8/10/2015]: created by __oz
 * ===================================
 */

@Path("/assessment")
public class AssessmentServices {
    static final Logger LOG = LoggerFactory.getLogger(AssessmentServices.class);

    @GET
    @Path("/getAssessmentFormUrl")
    public Response getAssessmentFormUrl(@QueryParam("uuid") String uuid, @QueryParam("facilityId") long facilityId, @QueryParam("formType") String formType) {
        LOG.info("getAssessmentFormUrl service: Provided Data: uuid: " + uuid + ", facilityId: "+facilityId, ", formType: "+ formType);
        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        FacilityDAO facilityDAO = new FacilityDAO();

        try {
            FacilityDTO facility = facilityDAO.getFacilityDetailsById(facilityId);
            long formsCount = ObjectUtils.nullSafeLong(facilityDAO.getAssessmentFormsCountByFacility(facilityId));
            long completedFormsCount = ObjectUtils.nullSafeLong(facilityDAO.getCompletedAssessmentFormsByPatientId(uuid, facilityId));
            if(facility != null){

                String assessmentURL = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.FORMS_ASSESSMENT_URL.getValue());

                //no forms available
                if(formsCount == 0){
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No assessment forms available for this facility.");
                }else if(formsCount == completedFormsCount){
                    //all forms are filled
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "All forms are completed.");
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                    returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), assessmentURL);
                }
            }else{
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No facility found.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("getAssessmentFormUrl service: Exception: "+e.getMessage());
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("getAssessmentFormUrl service: Returning response: "+response);
        return Response.ok(response).build();
    }
}


