package com.healthslate.patientapp.ws;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.CoachDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Encoder;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.*;

/*
 * ======= FILE CHANGE HISTORY =======
 * [7/27/2015]: created by __oz
 * ===================================
 */

@Path("/auth")
public class AuthServices {
    static final Logger LOG = LoggerFactory.getLogger(AuthServices.class);
    private PreferencesDAO preferencesDAO = new PreferencesDAO();

    @POST
    @Path("/login")
    public Response login(@Context HttpServletRequest request) {

        Map<String, Object> returnValues = new LinkedHashMap<String, Object>();
        ServerInfoDAO serverInfoDAO = new ServerInfoDAO();
        String responseFromOtherServers = "";
        returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        returnValues.put(AppConstants.ServicesConstants.TIME_STAMP.name(), System.currentTimeMillis());
        ServerInfo currentServerInfo = getCurrentServerInfo(request);
        String creds = request.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
        LOG.info("auth/login service: Provided Data: credentials: " + creds);

        //START -- FORCE SWITCHING TO SERVERS -----------------------------------------------
        String[] extracted = CommonUtils.extractEmailFromHeader(creds);
        String email = extracted[0];
        if(email.contains("#")){

            //holds email and server name without hashed
           RequestedEmailServer requestedEmailServer = CommonUtils.extractEmailAndServerFromHashedString(email);

           //query DB for serverInfo
           ServerInfo forcedServerInfo = serverInfoDAO.getServerInfoByName(requestedEmailServer.getServer());
           if(forcedServerInfo != null){
               //if forced server is current server then don't force switch
               //re-build credentials without # string and encode them
               creds = requestedEmailServer.getEmail()+":"+extracted[1];
               creds = new BASE64Encoder().encode(creds.getBytes());
               LOG.info("auth/login service: Force switching to server:" + forcedServerInfo.toString());
               LOG.info("auth/login service: Force switching to server with creds:" + creds);
               if(forcedServerInfo.getServerName().equalsIgnoreCase(currentServerInfo.getServerName())){
                   Map<String, Object> resultMap = CommonUtils.authenticateUser(creds);
                   return sendMemberToApp(request, resultMap);
               }else{
                   return forceSwitchToServer(forcedServerInfo.getServerURL()+"/services/auth/loginLookup", creds, request);
               }
           }else{
               returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
               returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.SERVER_INFO_ERROR);
               String response = JsonUtil.toJsonExcludedNull(returnValues);
               LOG.info("auth/login service: server not found, server name is incorrect, returning response: " + response);
               return Response.ok(response).build();
           }
        }
        //end -- force switching to servers -----------------------------------------------
        Map<String, Object> resultMap = CommonUtils.authenticateUser(creds);
        String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));
        //if not found in current server, find in other servers
        if (status.equalsIgnoreCase(AppConstants.ACCESS_NOT_FOUND)) {
            //list all servers except current one, because we already checked in current server
            List<ServerInfo> servers = serverInfoDAO.listAll(currentServerInfo);
            for(ServerInfo info: servers){
                String urlToHit = info.getServerURL() + "/services/auth/loginLookup";
                Map<String, String> headerEntries = new LinkedHashMap<String, String>();
                String mac = request.getHeader(AppConstants.UserInfo.MAC_ADDRESS.getValue());
                headerEntries.put(AppConstants.UserInfo.AUTHORIZATION.getValue(), creds);
                headerEntries.put(AppConstants.UserInfo.MAC_ADDRESS.getValue(), mac);
                headerEntries.put(AppConstants.SP_REGISTRATION_ID, request.getHeader(AppConstants.SP_REGISTRATION_ID));
                headerEntries.put(AppConstants.StatusConstants.DEVICETYPE.getValue(), request.getHeader(AppConstants.StatusConstants.DEVICETYPE.getValue()));
                headerEntries.put(AppConstants.StatusConstants.DEVICE_BUILD_TYPE.getValue(), request.getHeader(AppConstants.StatusConstants.DEVICE_BUILD_TYPE.getValue()));
                headerEntries.put(AppConstants.StatusConstants.DEVICE_NAME.getValue(), request.getHeader(AppConstants.StatusConstants.DEVICE_NAME.getValue()));
                headerEntries.put(AppConstants.StatusConstants.LOG_TIME_OFFSET.getValue(), request.getHeader(AppConstants.StatusConstants.LOG_TIME_OFFSET.getValue()));
                headerEntries.put(AppConstants.StatusConstants.OFFSET_KEY.getValue(), request.getHeader(AppConstants.StatusConstants.OFFSET_KEY.getValue()));
                headerEntries.put(AppConstants.StatusConstants.USER_AGENT_STRING.getValue(), request.getHeader(AppConstants.StatusConstants.USER_AGENT_STRING.getValue()));

                //wanderer method on all servers to look for the user
                responseFromOtherServers = wanderer(urlToHit, headerEntries);
                if(!ObjectUtils.isEmpty(responseFromOtherServers) && JsonUtil.isValidJSON(responseFromOtherServers)){
                    try {
                        JSONObject jsonObject = new JSONObject(responseFromOtherServers);
                        String resultStatus = jsonObject.getString(AppConstants.ServicesConstants.STATUS.name());
                        // if we have found user any of the server break loop and return response to app
                        if(resultStatus.equalsIgnoreCase(AppConstants.ServicesConstants.SUCCESS.name())){
                            //extract server info from request and broadcast on others server to remove mac address association except this one
                            String dataObject = jsonObject.getString(AppConstants.ServicesConstants.DATA.name());
                            String serverInfoObject = new JSONObject(dataObject).getString(AppConstants.ServicesConstants.SERVER_INFO.name());
                            if(!ObjectUtils.isEmpty(serverInfoObject)){
                                ServerInfo serverInfo = (ServerInfo)JsonUtil.fromJson(serverInfoObject, ServerInfo.class);
                                if(serverInfo != null){
                                    BroadcastServices.removeDeviceAssociation(serverInfo, mac, request);
                                }
                            }
                            break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            if(ObjectUtils.isEmpty(responseFromOtherServers) || responseFromOtherServers.equalsIgnoreCase(AppConstants.ACCESS_DENIED)){
                returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL_PASSWORD);
                responseFromOtherServers = JsonUtil.toJsonExcludedNull(returnValues);
            }

            LOG.info("auth/login service: Returning response from other server: " + responseFromOtherServers);
            return Response.ok(responseFromOtherServers).build();
        } else if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {
            returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
            returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL_PASSWORD);
            String response = JsonUtil.toJsonExcludedNull(returnValues);
            LOG.info("auth/loginLookup service: Returning Response: " + response);
            return Response.ok(response).build();

        } else {
            //building maps and json for sending data to app
            return sendMemberToApp(request, resultMap);
        }
    }

    //lookup service that would return user and server info if found
    @POST
    @Path("/loginLookup")
    public Response loginLookup(@Context HttpServletRequest request) {

        Map<String, Object> returnValues = new LinkedHashMap<String, Object>();
        returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        returnValues.put(AppConstants.ServicesConstants.TIME_STAMP.name(), System.currentTimeMillis());

        String creds = request.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
        LOG.info("auth/loginLookup service: Provided Data: credentials: " + creds);
        Map<String, Object> resultMap = CommonUtils.authenticateUser(creds);
        String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));

        //if not found in lookup server then return
        if (status.equalsIgnoreCase(AppConstants.ACCESS_NOT_FOUND)) {

            returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.EMAIL_NOT_REGISTERED);
            returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
            String response = JsonUtil.toJsonExcludedNull(returnValues);
            LOG.info("auth/loginLookup service: Returning response: " + response);
            return Response.ok(response).build();

        } else if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {

            returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL_PASSWORD);
            returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
            String response = JsonUtil.toJsonExcludedNull(returnValues);
            LOG.info("auth/loginLookup service: Returning Response: " + response);
            return Response.ok(response).build();

        } else {

            //building maps and json for sending data to app
            return sendMemberToApp(request, resultMap);
        }
    }

    //this for searching user on server and return server info if user is found
    @POST
    @Path("/lookForUser")
    public Response lookForUser(@Context HttpServletRequest request){

        UserDAO userDAO = new UserDAO();
        Map<String, Object> returnValues = new LinkedHashMap<String, Object>();
        returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        ServerInfo serverInfo = getCurrentServerInfo(request);
        String email = null;
        try {
            email = new EncryptionUtils().decrypt(request.getHeader(AppConstants.UserInfo.SP_EMAIL.getValue()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        LOG.info("auth/lookForUser service: Provided data: " + email);
        if(!ObjectUtils.isEmpty(email)){
            User user = userDAO.getUserByEmail(email);
            if(user != null){
                Patient patient = user.getPatient();
                if(patient != null && user.getUserType().equalsIgnoreCase(AppConstants.PATIENT)){
                    if(user.getIsRegistrationCompleted()){
                        returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.EMAIL_ALREADY_EXIST);
                        returnValues.put(AppConstants.ServicesConstants.SERVER_INFO.name(), serverInfo);
                    }else{

                        boolean isConsentAccepted = ObjectUtils.nullSafe(patient.getIsConsentAccepted());

                        if (!isConsentAccepted) {
                            LOG.info("auth/lookForUser service: Member not yet accepted TOS from Web: " + user.toString());
                            returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                            returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.NOT_APPROVED_MESSAGE);
                            String response = JsonUtil.toJsonExcludedNull(returnValues);
                            LOG.info("auth/lookForUser service: TOS not accepted, returning response: " + response);
                            return Response.ok(response).build();
                        }

                        // [0, 1] means request is in process not approved yet
                        // 2 means denied by admin
                        // 3 means approved by admin
                        String approvedStatus = ObjectUtils.nullSafe(patient.getIsApproved());

                        if (approvedStatus.equalsIgnoreCase(AppConstants.REQ_IN_PROCESS_0) || approvedStatus.equalsIgnoreCase(AppConstants.REQ_IN_PROCESS_1)) {
                            LOG.info("auth/lookForUser service: Member not yet approved: " + user.toString());
                            returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                            returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.NOT_APPROVED_MESSAGE);
                        } else if (approvedStatus.equalsIgnoreCase(AppConstants.REQ_DENIED)) {
                            LOG.info("auth/lookForUser service: Member denied by admin: " + user.toString());
                            returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                            returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.DENIED_REQUEST_MESSAGE);
                        } else if (approvedStatus.equalsIgnoreCase(AppConstants.REQ_APPROVED)) {
                            returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS);
                        } else {
                            LOG.info("auth/lookForUser service: Member registration not completed: " + user.toString());
                            returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                            returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.EMAIL_NOT_REGISTERED);
                        }
                        returnValues.put(AppConstants.ServicesConstants.SERVER_INFO.name(), serverInfo);
                    }
                }else{
                    returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                    returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL_PASSWORD);
                }
            }else{
                returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.EMAIL_NOT_REGISTERED);
            }
        }

        String response = JsonUtil.toJsonExcludedNull(returnValues);
        LOG.info("auth/lookForUser service: Returning response: " + response);
        return Response.ok(response).build();
    }

    private void saveDevice(HttpServletRequest request, Patient patient, String deviceType, String deviceBuildType, String deviceAppName, String userAgentString) {
        DeviceDAO deviceDao = new DeviceDAO();
        PatientDAO patientDAO = new PatientDAO();
        String mac = request.getHeader(AppConstants.UserInfo.MAC_ADDRESS.getValue());

        String registrationId = request.getHeader(AppConstants.SP_REGISTRATION_ID);

        if (!ObjectUtils.isEmpty(registrationId) && !ObjectUtils.isEmpty(mac)) {

            Device device = new DeviceDAO().getDeviceByMac(mac);

            //set mac address to Patient as well
            if (patient != null) {

                //first remove all patients association with this mac
                patientDAO.removeMacAddressAssignmentFromMember(mac);

                //set new one here
                patient.setDeviceMacAddress(mac);
                patientDAO.save(patient);
            }

            if (device == null) {
                device = new Device();
                device.setDeviceMacAddress(mac);
            }

            device.setDeviceBuildType(deviceBuildType);
            device.setDeviceType(deviceType);
            device.setRegistrationId(registrationId);
            device.setDeviceAppName(deviceAppName);
            device.setDeviceManufacturerAndOS(userAgentString);
            deviceDao.save(device);
        }
    }

    //wanderer method on all servers to look for the user
    private static String wanderer(String serverUrl, Map<String, String> headerEntries){

        try {
            return NetworkUtils.postRequest(serverUrl, headerEntries);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    //getting current server info from URL
    public static ServerInfo getCurrentServerInfo(HttpServletRequest request){

        String currentServerUrl = ObjectUtils.getApplicationUrl(request);
        String serverName = "";

        if(currentServerUrl.indexOf("diabetes") > -1){
            serverName = AppConstants.ServerInfoConstants.PROD.getValue();
        } else if(currentServerUrl.indexOf("tprod") > -1){
            serverName = AppConstants.ServerInfoConstants.TPROD.getValue();
        } else if(currentServerUrl.indexOf("sandbox") > -1) {
            serverName = AppConstants.ServerInfoConstants.SANDBOX.getValue();
        }else if(currentServerUrl.indexOf("f1") > -1){
            serverName = AppConstants.ServerInfoConstants.HSTAGE_2.getValue();
        } else {
            serverName = AppConstants.ServerInfoConstants.HSTAGE.getValue();
        }

        //for dev
        if(currentServerUrl.indexOf("PatientApp") > -1){
            serverName = AppConstants.ServerInfoConstants.OZY.getValue();
        }

        ServerInfo serverInfo = new ServerInfoDAO().getServerInfoByName(serverName);
        if(serverInfo == null){
            serverInfo = new ServerInfo();
            serverInfo.setIsActive(true);
            serverInfo.setServerName(serverName);
            serverInfo.setServerURL(currentServerUrl);
        }

        return serverInfo;
    }

    //building maps and json for sending data to app
    private Response sendMemberToApp(HttpServletRequest request, Map<String, Object> resultMap){

        String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));

        //if not found in lookup server then return
        if (status.equalsIgnoreCase(AppConstants.ACCESS_NOT_FOUND)) {

            resultMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
            resultMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.EMAIL_NOT_REGISTERED);
            String response = JsonUtil.toJsonExcludedNull(resultMap);
            LOG.info("auth/loginLookup service: Returning response: " + response);
            return Response.ok(response).build();

        } else if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {

            resultMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL_PASSWORD);
            resultMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
            String response = JsonUtil.toJsonExcludedNull(resultMap);
            LOG.info("auth/loginLookup service: Returning Response: " + response);
            return Response.ok(response).build();

        } else {
            UserDAO userDAO = new UserDAO();
            PatientDAO patientDao = new PatientDAO();
            Map<String, Object> returnValues = new LinkedHashMap<String, Object>();
            returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
            returnValues.put(AppConstants.ServicesConstants.TIME_STAMP.name(), System.currentTimeMillis());

            String deviceType = request.getHeader(AppConstants.StatusConstants.DEVICETYPE.getValue());
            String deviceBuildType = ObjectUtils.nullSafe(request.getHeader(AppConstants.StatusConstants.DEVICE_BUILD_TYPE.getValue()));
            String deviceAppName = ObjectUtils.nullSafe(request.getHeader(AppConstants.StatusConstants.DEVICE_NAME.getValue()));
            String userAgentString = ObjectUtils.nullSafe(request.getHeader(AppConstants.StatusConstants.USER_AGENT_STRING.getValue()));

            long logTimeOffset = -1l;

            try {

                logTimeOffset = Long.parseLong(request.getHeader(AppConstants.StatusConstants.LOG_TIME_OFFSET.getValue()));

            } catch (NumberFormatException e) {

                e.printStackTrace();
                LOG.info("auth/login service: NumberFormatException: " + e.getMessage());
            }

            String offsetKey = request.getHeader(AppConstants.StatusConstants.OFFSET_KEY.getValue());
            Long patientId = (Long) resultMap.get(AppConstants.JsonConstants.DATA.name());
            Patient patient = patientDao.getPatientById(patientId);

            if (patient != null) {

                User user = patient.getUser();

                if (user != null && user.getUserType().equalsIgnoreCase(AppConstants.PATIENT)) {

                    if (ObjectUtils.nullSafe(user.getIsRegistrationCompleted())) {

                        LOG.info("auth/login service: logged in member UUID: " + patient.getUuid() + ", User Id: " + user.getUserId());

                        String techSupportNumber = preferencesDAO.getPreference(AppConstants.PreferencesNames.TECH_SUPPORT_NUMBER.getValue());
                        String mac = request.getHeader(AppConstants.UserInfo.MAC_ADDRESS.getValue());

                        //getting current server info from URL
                        ServerInfo serverInfo = getCurrentServerInfo(request);
                        //remove device association from other servers, if found
                        if(serverInfo != null){
                            BroadcastServices.removeDeviceAssociation(serverInfo, mac, request);
                        }

                        user.setLogTimeOffset(logTimeOffset);
                        user.setOffsetKey(offsetKey);
                        userDAO.save(user);

                        saveDevice(request, patient, deviceType, deviceBuildType, deviceAppName, userAgentString);

                        //[2/2/2015]: also add coach list to patient, __oz
                        List<CoachDTO> coachList = userDAO.getCoachesExcludeDefaultTechSupportByPatientId(patient.getPatientId(), false);
                        patient.setCoachList(coachList);

                        patient.setTarget(null);
                        Target targetFromDB = new TargetDAO().getTargetByPatientId(patient.getPatientId());
                        patient.setTarget(targetFromDB);
                        patient.setFilledForms(null);
                        patient.setIsPatientSessionPreferenceSet(false);
                        patient.setIsEatingPreferenceSet(false);

                        if (patient.getPatientSessionPreference() != null) {
                            patient.setIsPatientSessionPreferenceSet(true);
                        }

                        if (patient.getPatientEatingPreference() != null) {
                            patient.setIsEatingPreferenceSet(true);
                        }

                        Facility facility = new FacilityDAO().getFacilityByPatientId(patient.getPatientId());
                        if (facility != null) {
                            patient.setFacilityName(facility.getName());
                            patient.setFacilityId(facility.getFacilityId());
                        }

                        HashMap<String, Object> dataInsideMap = new LinkedHashMap<String, Object>();
                        dataInsideMap.put(AppConstants.SessionKeys.PATIENT.name(), patient);
                        dataInsideMap.put(AppConstants.ServicesConstants.TECH_SUPPORT_NUMBER.name(), techSupportNumber);
                        dataInsideMap.put(AppConstants.ServicesConstants.SERVER_INFO.name(), serverInfo);

                        returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                        returnValues.put(AppConstants.ServicesConstants.DATA.name(), dataInsideMap);

                    } else {

                        LOG.info("auth/login service: registration not completed,  User Id: " + user.getUserId());
                        returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                        returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.REG_NOT_COMPLETED);
                    }

                } else {

                    LOG.info("auth/login service: tried to logged in with email other than member/patient email,  User Id: " + user.getUserId());
                    returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                    returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL_PASSWORD);
                }

            } else {

                returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL_PASSWORD);
            }

            String response = JsonUtil.toJsonExcludedNull(returnValues);
            LOG.info("auth/login service: Returning response: " + response);
            return Response.ok(response).build();
        }
    }

    //force switching to server for getting user from that server
    private Response forceSwitchToServer(String serverURL, String creds, HttpServletRequest request){
        Map<String, String> headerEntries = new LinkedHashMap<String, String>();
        String mac = request.getHeader(AppConstants.UserInfo.MAC_ADDRESS.getValue());
        headerEntries.put(AppConstants.UserInfo.AUTHORIZATION.getValue(), creds);
        headerEntries.put(AppConstants.UserInfo.MAC_ADDRESS.getValue(), mac);
        headerEntries.put(AppConstants.SP_REGISTRATION_ID, request.getHeader(AppConstants.SP_REGISTRATION_ID));
        headerEntries.put(AppConstants.StatusConstants.DEVICETYPE.getValue(), request.getHeader(AppConstants.StatusConstants.DEVICETYPE.getValue()));
        headerEntries.put(AppConstants.StatusConstants.DEVICE_BUILD_TYPE.getValue(), request.getHeader(AppConstants.StatusConstants.DEVICE_BUILD_TYPE.getValue()));
        headerEntries.put(AppConstants.StatusConstants.DEVICE_NAME.getValue(), request.getHeader(AppConstants.StatusConstants.DEVICE_NAME.getValue()));
        headerEntries.put(AppConstants.StatusConstants.LOG_TIME_OFFSET.getValue(), request.getHeader(AppConstants.StatusConstants.LOG_TIME_OFFSET.getValue()));
        headerEntries.put(AppConstants.StatusConstants.OFFSET_KEY.getValue(), request.getHeader(AppConstants.StatusConstants.OFFSET_KEY.getValue()));
        headerEntries.put(AppConstants.StatusConstants.USER_AGENT_STRING.getValue(), request.getHeader(AppConstants.StatusConstants.USER_AGENT_STRING.getValue()));

        //wanderer method on all servers to look for the user
        String response = wanderer(serverURL, headerEntries);

        if(JsonUtil.isValidJSON(response)){
            try {
                JSONObject jsonObject = new JSONObject(response);
                String resultStatus = jsonObject.getString(AppConstants.ServicesConstants.STATUS.name());

                // if we have found user any of the server break loop and return response to app
                if(resultStatus.equalsIgnoreCase(AppConstants.ServicesConstants.SUCCESS.name())){

                    //extract server info from request and broadcast on others server to remove mac address association except this one
                    String dataObject = jsonObject.getString(AppConstants.ServicesConstants.DATA.name());
                    String serverInfoObject = new JSONObject(dataObject).getString(AppConstants.ServicesConstants.SERVER_INFO.name());
                    if(!ObjectUtils.isEmpty(serverInfoObject)){
                        ServerInfo serverInfo = (ServerInfo)JsonUtil.fromJson(serverInfoObject, ServerInfo.class);
                        if(serverInfo != null){
                            BroadcastServices.removeDeviceAssociation(serverInfo, mac, request);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{

            Map<String, Object> returnValues = new LinkedHashMap<String, Object>();
            returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
            returnValues.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL_PASSWORD);
            response = JsonUtil.toJsonExcludedNull(returnValues);
            LOG.info("auth/forceSwitchToServer service: Returning Response: " + response);
        }

        LOG.info("auth/forceSwitchToServer service: Returning response from other server: " + response);
        return Response.ok(response).build();
    }
}
