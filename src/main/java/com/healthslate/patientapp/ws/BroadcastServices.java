package com.healthslate.patientapp.ws;

import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.entity.Device;
import com.healthslate.patientapp.model.entity.EmailToken;
import com.healthslate.patientapp.model.entity.ServerInfo;
import com.healthslate.patientapp.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.codec.Base64;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/*
 * ======= FILE CHANGE HISTORY =======
 * [7/28/2015]: created by __oz
 * ===================================
 */

@Path("/broadcast")
public class BroadcastServices {

    static final Logger LOG = LoggerFactory.getLogger(BroadcastServices.class);

    @POST
    @Path("/removeDevice")
    public Response removeDevice(@Context HttpServletRequest request) {

        PatientDAO patientDao = new PatientDAO();
        DeviceDAO deviceDAO = new DeviceDAO();
        Map<String, Object> returnValues = new LinkedHashMap<String, Object>();
        returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());

        String deviceMacAddress = request.getHeader(AppConstants.UserInfo.MAC_ADDRESS.getValue());
        LOG.info("broadcast/removeDevice service: Provided Data: credentials: " + deviceMacAddress);

        if(!ObjectUtils.isEmpty(deviceMacAddress)){

            Device device = deviceDAO.getDeviceByMac(deviceMacAddress);
            if(device != null){

                //remove assignment from patient/member
                boolean isDeleted = patientDao.removeMacAddressAssignmentFromMember(device.getDeviceMacAddress());

                if(isDeleted){

                    //also delete this device from db device table
                    deviceDAO.delete(device);
                }
                returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
            }else{
                returnValues.put(AppConstants.ServicesConstants.REASON.name(), "Device not found");
            }
        }

        String response = JsonUtil.toJsonExcludedNull(returnValues);
        LOG.info("auth/removeDevice service: Returning response: " + response);
        return Response.ok(response).build();
    }

    @POST
    @Path("/sendEmail")
    public Response sendEmail(@Context HttpServletRequest request) {

        EmailTokenDAO emailTokenDAO = new EmailTokenDAO();
        Map<String, String> prefMap = new PreferencesDAO().map();
        Map<String, Object> returnValues = new LinkedHashMap<String, Object>();
        returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());

        String emailAddress = null;
        try {
            emailAddress = new EncryptionUtils().decrypt(request.getHeader(AppConstants.UserInfo.SP_EMAIL.getValue()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        String serverInfoFromHeader = request.getHeader(AppConstants.UserInfo.SP_SERVER_INFO.getValue());

        ServerInfo serverInfo = (ServerInfo)JsonUtil.fromJson(serverInfoFromHeader, ServerInfo.class);

        LOG.info("broadcast/sendEmail service: Provided Data: credentials: " + emailAddress + ", server info: " + serverInfoFromHeader);

        if(!ObjectUtils.isEmpty(emailAddress)){

            try {
                EmailToken emailToken = new EmailToken();
                emailToken.setUsername(emailAddress);

                String token = emailAddress + System.currentTimeMillis();
                token = new String(Base64.encode(token.getBytes()));
                emailToken.setToken(token);

                emailToken.setDateCreated(new Date());
                emailTokenDAO.saveEmailToken(emailToken);

                String resetPasswordUrl = serverInfo.getServerURL() + "/app/resetPassword.action?token=" + emailToken.getToken();
                String emailText = CommunicationUtils.getEmailTextForResetPassword(emailAddress, resetPasswordUrl);

                boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
                        emailAddress,
                        prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                        prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                        prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                        prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                        AppConstants.EmailSubjects.RESET_PASSWORD.getValue());

                if (isSentEmail) {
                    returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                    returnValues.put(AppConstants.ServicesConstants.DATA.name(), "An email has been sent to your email address.");
                } else {
                    returnValues.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
                    returnValues.put(AppConstants.JsonConstants.REASON.name(), "Unable to send email at the moment.");
                }

                LOG.info("broadcast/sendEmail service: Email sent to: "+emailAddress+", with email Text: "+emailAddress+", and email sent status: "+isSentEmail);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String response = JsonUtil.toJsonExcludedNull(returnValues);
        LOG.info("auth/sendEmail service: Returning response: " + response);
        return Response.ok(response).build();
    }

    public static void removeDeviceAssociation(ServerInfo currentServerInfo, String macAddress, HttpServletRequest request){

        LOG.info("broadcast/removeDeviceAssociation: Provided Data: " + currentServerInfo.toString() + ", macAddress: " + macAddress);

        //get the list of all servers, except passing server info, remove association in all servers
        List<ServerInfo> servers = new ServerInfoDAO().listAll(currentServerInfo);
        for(ServerInfo info: servers){
            String urlToHit = info.getServerURL() + "/services/broadcast/removeDevice";
            LOG.info("broadcast/removeDeviceAssociation: Hitting URL: " + urlToHit);
            Map<String, String> headerEntries = new LinkedHashMap<String, String>();
            headerEntries.put(AppConstants.UserInfo.MAC_ADDRESS.getValue(), macAddress);
            try {
                String response = NetworkUtils.postRequest(urlToHit, headerEntries);
                LOG.info("broadcast/removeDeviceAssociation: response from broadcast/removeDevice: " + response);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
