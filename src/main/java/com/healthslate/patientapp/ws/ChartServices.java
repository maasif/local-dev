package com.healthslate.patientapp.ws;

import com.healthslate.patientapp.model.dao.LogDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.dao.TargetDAO;
import com.healthslate.patientapp.model.dto.TookMedsDTO;
import com.healthslate.patientapp.model.entity.ActivityLog;
import com.healthslate.patientapp.model.entity.MiscLog;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.model.entity.Target;
import com.healthslate.patientapp.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.org.mozilla.javascript.internal.ObjArray;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Path("/chart")
public class ChartServices {
	static final Logger LOG = LoggerFactory.getLogger(ChartServices.class);
	@GET
	@Path("/getTookMedsChartData")
	public Response getTookMedsChartData(@QueryParam("patientId") Long patientId, @QueryParam("startDate") Long startDate, @QueryParam("endDate") Long endDate) {
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LogDAO logDAO = new LogDAO();
		LOG.info("getTookMedsChartData service: Provided data: PatientId: "+patientId+", startDate: "+startDate+", endDate: "+endDate);
		if(patientId != null && startDate != null && endDate != null){			
			TookMedsDTO tookMedDTO = logDAO.getTookMedsByPatientId(patientId, startDate, endDate);			
			returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
			returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), tookMedDTO);
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getTookMedsChartData service: Returning response: "+response);
		return Response.ok(response).build();
	}

	@GET
	@Path("/getTookMedsLineChartData")
	public Response getTookMedsLineChartData(@QueryParam("patientId") Long patientId, @QueryParam("startDate") Long startDate, @QueryParam("endDate") Long endDate) {
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LogDAO logDAO = new LogDAO();

		Patient patient = new PatientDAO().getPatientById(patientId);
		long offset = CommonUtils.getPatientFacilityTimezoneOffset(patient);
		long startTimeInUserTimezone = startDate + offset;

		startDate = DateUtils.getStartOfDayTime(startTimeInUserTimezone);
        endDate = DateUtils.getEndOfDayTime(endDate);

		LOG.info("getTookMedsLineChartData service: Provided data: PatientId: "+patientId+", startDate: "+startDate+", endDate: "+endDate);
		if(patientId != null && startDate != null && endDate != null) {
			List<Object> tookMedsDetailList = logDAO.getTookMedsDataByPatientId(patientId, startDate, endDate);
			if(tookMedsDetailList == null) {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
				tookMedsDetailList = addMissingDatesData(tookMedsDetailList, startDate, endDate);
				tookMedsDetailList = getTookMedList(tookMedsDetailList, patientId);
				returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), tookMedsDetailList);
			}
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getTookMedsLineChartData service: Returning response: "+response);
		return Response.ok(response).build();
	}

	@GET
	@Path("/getMealChartData")
	public Response getMealChartData(
			@QueryParam("patientId") Long patientId, 
			@QueryParam("startDate") Long startDate, 
			@QueryParam("endDate") Long endDate) {
		
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LOG.info("getMealChartData service: Provided data: PatientId: "+patientId+", startDate: "+startDate+", endDate: "+endDate);

		LogDAO logDAO = new LogDAO();
		if(patientId != null && startDate != null && endDate != null) {			
			Object mealSummary = logDAO.getPercentageOfMealsByPatientId(patientId, startDate, endDate);
			Object averageMealSummary = logDAO.getAverageOfEachMealSummaryChart(patientId, startDate, endDate);
			if(mealSummary == null && averageMealSummary == null){
				//means no data found
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");
			}else{
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
				Map<String, Object> underCeilingMealsPercentageMap = getMealSummaryMap(mealSummary);
				Map<String, Object> averMealSummaryMap = getAverageMealSummaryMap(averageMealSummary);
				
				Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
				dataMap.put(AppConstants.ChartServiceConstants.PERCENTAGE_UNDER_CEILING_MEALS.getValue(), underCeilingMealsPercentageMap);
				dataMap.put(AppConstants.ChartServiceConstants.AVERAGE_OF_EACH_MEALS.getValue(), averMealSummaryMap);
				returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), dataMap);
			}			
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getMealChartData service: Returning response: "+response);
		return Response.ok(response).build();
	}
	
	@GET
	@Path("/getGlucoseChartData")
	public Response getGlucoseChartData(
			@QueryParam("patientId") Long patientId,
			@QueryParam("startDate") Long startDate,
			@QueryParam("endDate") Long endDate) {
		
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LOG.info("getGlucoseChartData service: Provided data: PatientId: "+patientId+", startDate: "+startDate+", endDate: "+endDate);
		//String startDateString = DateUtils.getFormattedDate(startDate, DateUtils.DATE_FORMAT_DASHED);
	//	String endDateString = DateUtils.getFormattedDate(endDate, DateUtils.DATE_FORMAT_DASHED);
		
		LogDAO logDAO = new LogDAO();
		if(patientId != null && startDate != null && endDate != null) {			
			Object glucoseData = logDAO.getGlucoseSummaryByPatientId(patientId, startDate, endDate);
			if(glucoseData == null) {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");				
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
				Map<String, Object> glucoseDataMap = getGlucoseMap(glucoseData);
				returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), glucoseDataMap);
			}
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getGlucoseChartData service: Returning response: "+response);
		return Response.ok(response).build();
	}

    @GET
    @Path("/getGlucoseChartDataCombined")
    public Response getGlucoseChartDataCombined(
            @QueryParam("patientId") Long patientId,
            @QueryParam("startDate") Long startDate,
            @QueryParam("endDate") Long endDate) {

        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        LOG.info("getGlucoseChartDataCombined service: Provided data: PatientId: "+patientId+", startDate: "+startDate+", endDate: "+endDate);

        LogDAO logDAO = new LogDAO();
        if(patientId != null && startDate != null && endDate != null) {
            Object glucoseDataBeforeMeal = logDAO.getGlucoseSummaryBeforeMealByPatientId(patientId, startDate, endDate);
            Object glucoseDataAfterMeal = logDAO.getGlucoseSummaryAfterMealByPatientId(patientId, startDate, endDate);
            if(glucoseDataBeforeMeal == null && glucoseDataAfterMeal == null) {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");
            } else {
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                Map<String, Object> glucoseDataBeforeMealMap = getGlucoseMap(glucoseDataBeforeMeal);
                Map<String, Object> glucoseDataAfterMealMap = getGlucoseMap(glucoseDataAfterMeal);
                returnDataMap.put(AppConstants.ChartServiceConstants.BEFORE_MEAL.name(), glucoseDataBeforeMealMap);
                returnDataMap.put(AppConstants.ChartServiceConstants.AFTER_MEAL.name(), glucoseDataAfterMealMap);
            }
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("getGlucoseChartDataCombined service: Returning response: "+response);
        return Response.ok(response).build();
    }

	@GET
	@Path("/getDailyMealGlucoseChartData")
	public Response getDailyMealGlucoseChartData(
			@QueryParam("patientId") Long patientId,
			@QueryParam("startDate") Long startDate,
			@QueryParam("endDate") Long endDate) {
		
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LOG.info("getDailyMealGlucoseChartData service: Provided data: PatientId: "+patientId+", startDate: "+startDate+", endDate: "+endDate);
		LogDAO logDAO = new LogDAO();
		if(patientId != null && startDate != null && endDate != null) {			
			List<Object> glucoseLevelData = logDAO.getGlucoseValuesPerDayByPatientId(patientId, startDate, endDate);
			List<Object> carbsLevelData = logDAO.getCarbsValuesPerDayByPatientId(patientId, startDate, endDate);
			if(glucoseLevelData == null && carbsLevelData == null) {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");				
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
				Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
				List<Object> glucoseLevelDataList = getGlucoseLevelDataList(glucoseLevelData);
				List<Object> carbsLevelDataList = getCarbsLevelDataList(carbsLevelData);
				
				dataMap.put(AppConstants.ChartServiceConstants.GLUCOSE_LEVEL_DATA.getValue(), glucoseLevelDataList);
				dataMap.put(AppConstants.ChartServiceConstants.CARBS_LEVEL_DATA.getValue(), carbsLevelDataList);
				returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), dataMap);
			}
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getDailyMealGlucoseChartData service: Returning response: "+response);
		return Response.ok(response).build();
	}

	@GET
	@Path("/getActivityChartData")
	public Response getActivityChartData(
		@QueryParam("patientId") Long patientId,
		@QueryParam("startDate") Long startDate,
		@QueryParam("endDate") Long endDate) {
	
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LogDAO logDAO = new LogDAO();
		Object activityData = null;
		Object currentDaysTotalSteps = null;
		LOG.info("getActivityChartData service: Provided data: PatientId: "+patientId+", startDate: "+startDate+", endDate: "+endDate);
		if(patientId != null && startDate != null && endDate != null) {
			activityData = logDAO.getActivityDataByPatientId(patientId, startDate, endDate);
			if(activityData == null && currentDaysTotalSteps == null) {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
				Map<String, Object> valueMap = new LinkedHashMap<String, Object>();
				Object[] objectArray = (Object[]) activityData;
				valueMap.put(AppConstants.ChartServiceConstants.PERCENTAGE.getValue(), objectArray[0] == null? 0: Math.round(Double.valueOf(objectArray[0].toString()))); 
				valueMap.put(AppConstants.ChartServiceConstants.CURRENT_DAY_SUM.getValue(), objectArray[1] == null? 0: Math.round(Double.valueOf(objectArray[1].toString())));
				returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), valueMap);
			}
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getActivityChartData service: Returning response: "+response);
		return Response.ok(response).build();
	}
	
	@GET
	@Path("/getGlucoseDetailChartData")
	public Response getGlucoseDetailChartData(
			@QueryParam("patientId") Long patientId,
			@QueryParam("startDate") Long startDate,
			@QueryParam("endDate") Long endDate) {
		
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LogDAO logDAO = new LogDAO();
		LOG.info("getGlucoseDetailChartData service: Provided data: PatientId: "+patientId+", startDate: "+startDate+", endDate: "+endDate);
		if(patientId != null && startDate != null && endDate != null) {			
			List<Object> glucoseLevelData = logDAO.getGlucoseValuesPerDayByPatientId(patientId, startDate, endDate);
			if(glucoseLevelData == null) {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");				
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
				List<Object> glucoseLevelDataList = getGlucoseLevelDataList(glucoseLevelData);
				returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), glucoseLevelDataList);
			}
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getGlucoseDetailChartData service: Returning response: "+response);
		return Response.ok(response).build();
	}
	
	@GET
	@Path("/getActivityDetailChartData")
	public Response getActivityDetailChartData(
			@QueryParam("patientId") Long patientId,
			@QueryParam("startDate") Long startDate,
			@QueryParam("endDate") Long endDate) {
		
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LogDAO logDAO = new LogDAO();
		LOG.info("getActivityDetailChartData service: Provided data: PatientId: "+patientId+", startDate: "+startDate+", endDate: "+endDate);

		if(patientId != null && startDate != null && endDate != null) {			
			List<Object> activityDetailList = logDAO.getActivityDetailData(patientId, startDate, endDate);
			if(activityDetailList == null) {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");				
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
				activityDetailList = getActivityDataList(activityDetailList);
				returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), activityDetailList);
			}
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getActivityDetailChartData service: Returning response: "+response);
		return Response.ok(response).build();
	}

	@GET
	@Path("/getComboActivityChartData")
	public Response getComboActivityChartData(
			@QueryParam("patientId") Long patientId,
			@QueryParam("startDate") Long startDate,
			@QueryParam("endDate") Long endDate,
			@QueryParam("chartType") String chartType) {

		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LogDAO logDAO = new LogDAO();
		LOG.info("getComboActivityChartData service: Provided data: PatientId: " + patientId + ", startDate: " + startDate + ", endDate: " + endDate + ", chartType: " + chartType);

		if (patientId != null && startDate != null && endDate != null) {
			if (chartType == null || chartType.equalsIgnoreCase("daily")) {
				List<ActivityLog> activityLogList = logDAO.getMinutesActivityDetailData(patientId, startDate, endDate);
				//CommonUtils.addTimeOffsetInActivityLog(activityLogList, patientId);
				List<Object> misfitHourlyLogs = logDAO.getMisfitActivityHourlyLogsByPatientId(patientId, startDate, endDate);
				Map<String, Object> dataMap = new LinkedHashMap<String, Object>();

				if (activityLogList == null && misfitHourlyLogs != null) {
					returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");
				} else {
					List<Object> finalMisfitHourlyLogsList = getActivityLogsDataListCombo(misfitHourlyLogs, patientId);
					returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
					dataMap.put(AppConstants.ChartServiceConstants.ACTIVITY_LOGS.getValue(), activityLogList);
					dataMap.put(AppConstants.ChartServiceConstants.MISFIT_ACTIVITY.getValue(), finalMisfitHourlyLogsList);
					returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), dataMap);
				}
			} else if (chartType.equalsIgnoreCase("weekly")){
				List<Object> activityLogList = logDAO.getActivityDetailDataObjects(patientId, startDate, endDate);
				activityLogList = getActivityMinutesLogsDataList(activityLogList);
				List<Object> misfitHourlySumLogs = logDAO.getMisfitActivityHourlyLogsSumByPatientId(patientId, startDate, endDate);

				Map<String, Object> dataMap = new LinkedHashMap<String, Object>();

				if (activityLogList == null && misfitHourlySumLogs != null) {
					returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");
				} else {
					List<Object> finalMisfitHourlyLogsList = getActivityLogsDataListCombo(misfitHourlySumLogs, patientId);
					returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
					dataMap.put(AppConstants.ChartServiceConstants.ACTIVITY_LOGS.getValue(), activityLogList);
					dataMap.put(AppConstants.ChartServiceConstants.MISFIT_ACTIVITY.getValue(), finalMisfitHourlyLogsList);
					returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), dataMap);
				}
			}
		}
		String response = JsonUtil.toJsonExcludedNull(returnDataMap);
		LOG.info("getComboActivityChartData service: Returning response: " + response);
		return Response.ok(response).build();
	}

	@GET
	@Path("/getWeightChartData")
	public Response getWeightChartData(
			@QueryParam("patientId") Long patientId,
			@QueryParam("startDate") Long startDate,
			@QueryParam("endDate") Long endDate) {
		
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LogDAO logDAO = new LogDAO();
		LOG.info("getWeightChartData service: Provided data: PatientId: "+patientId+", startDate: "+startDate+", endDate: "+endDate);

		if(patientId != null && startDate != null && endDate != null) {			
			List<MiscLog> activityDetailList = logDAO.getWeightDataForTimeInterval(patientId, startDate, endDate);
			Target target = new TargetDAO().getTargetByPatientId(patientId);
			Map<String, Object> map = getCalculatedWeightChart(startDate, endDate, activityDetailList, target);
			if(activityDetailList == null) {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");				
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
				returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), map);
			}
		}
		String response = JsonUtil.toJsonExcludedNull(returnDataMap);
		LOG.info("getActivityDetailChartData service: Returning response: "+response);
		return Response.ok(response).build();
	}

    @GET
	@Path("/getWeightDetailChartData")
	public Response getWeightDetailChartData(
			@QueryParam("patientId") Long patientId,
			@QueryParam("startDate") Long startDate,
			@QueryParam("endDate") Long endDate) {
		
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LogDAO logDAO = new LogDAO();
		LOG.info("getWeightDetailChartData service: Provided data: PatientId: "+patientId+", startDate: "+startDate+", endDate: "+endDate);
		if(patientId != null && startDate != null && endDate != null) {			
			List<Object> weightDataList = logDAO.getWeightValuesPerDayByPatientId(patientId, startDate, endDate);
			
			if(weightDataList == null) {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");				
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
				weightDataList = getWeightDataList(weightDataList);
				returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), weightDataList);
			}
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getWeightDetailChartData service: Returning response: "+response);
		return Response.ok(response).build();
	}

    @GET
    @Path("/getDailyComboChartData")
    public Response getDailyComboChartData(
            @QueryParam("patientId") Long patientId,
            @QueryParam("startDate") Long startDate,
            @QueryParam("endDate") Long endDate) {

        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        LOG.info("getDailyComboChartData service: Provided data: PatientId: "+patientId+", startDate: "+startDate+", endDate: "+endDate);
        LogDAO logDAO = new LogDAO();
        if(patientId != null && startDate != null && endDate != null) {
            List<Object> glucoseLevelData = logDAO.getGlucoseValuesPerDayByPatientId(patientId, startDate, endDate);
            List<Object> carbsLevelData = logDAO.getCarbsValuesPerDayByPatientId(patientId, startDate, endDate);
            List<Object> tookMedsDetailList = logDAO.getTookMedsDataByPatientId(patientId, startDate, endDate);
            List<ActivityLog> activityLogList = logDAO.getMinutesActivityDetailData(patientId, startDate, endDate);
            //CommonUtils.addTimeOffsetInActivityLog(activityLogList, patientId);
            List<Object> misfitHourlyLogs = logDAO.getMisfitActivityHourlyLogsByPatientId(patientId, startDate, endDate);

            if(glucoseLevelData == null
                    && carbsLevelData == null
                    && tookMedsDetailList == null
                    && activityLogList == null
                    && misfitHourlyLogs == null) {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");
            } else {
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
                List<Object> glucoseLevelDataList = getGlucoseLevelDataList(glucoseLevelData);
                List<Object> carbsLevelDataList = getCarbsLevelDataList(carbsLevelData);
                tookMedsDetailList = addMissingDatesData(tookMedsDetailList, startDate, endDate);
                tookMedsDetailList = getTookMedList(tookMedsDetailList, patientId);
                List<Object> finalMisfitHourlyLogsList = getActivityLogsDataList(misfitHourlyLogs, patientId);
                dataMap.put(AppConstants.ChartServiceConstants.GLUCOSE_LEVEL_DATA.getValue(), glucoseLevelDataList);
                dataMap.put(AppConstants.ChartServiceConstants.CARBS_LEVEL_DATA.getValue(), carbsLevelDataList);
                dataMap.put(AppConstants.ChartServiceConstants.TOOK_MED_DATA.getValue(), tookMedsDetailList);
                dataMap.put(AppConstants.ChartServiceConstants.ACTIVITY_LOGS.getValue(), activityLogList);
                dataMap.put(AppConstants.ChartServiceConstants.MISFIT_ACTIVITY.getValue(), finalMisfitHourlyLogsList);
                returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), dataMap);
            }
        }
        String response = JsonUtil.toJsonExcludedNull(returnDataMap);
        LOG.info("getDailyComboChartData service: Returning response: "+response);
        return Response.ok(response).build();
    }

	private Map<String, Object> getMealSummaryMap(Object mealSummary) {
		Object[] objArray = (Object[]) mealSummary;
		
		Map<String, Object> mealSummaryMap = new LinkedHashMap<String, Object>();
        if(objArray != null && objArray.length > 0) {
            mealSummaryMap.put(AppConstants.ChartServiceConstants.TOTAL.getValue(), objArray[0] == null ? 0 : objArray[0]);
            mealSummaryMap.put(AppConstants.ChartServiceConstants.SUM.getValue(), objArray[1] == null ? 0 : objArray[1]);
            mealSummaryMap.put(AppConstants.ChartServiceConstants.PERCENTAGE.getValue(), objArray[2] == null ? 0 : Math.round(Double.valueOf(objArray[2].toString())));
        }
		return mealSummaryMap;
	}
	
	private Map<String, Object> getAverageMealSummaryMap(Object averageMealSummary) {
		Object[] objArray = (Object[]) averageMealSummary;
		
		Map<String, Object> mealSummaryMap = new LinkedHashMap<String, Object>();
        if(objArray != null && objArray.length > 0) {
            mealSummaryMap.put(AppConstants.ChartServiceConstants.TOTAL.getValue(), objArray[0] == null ? 0 : objArray[0]);
            mealSummaryMap.put(AppConstants.ChartServiceConstants.SNACK_AVERAGE.getValue(), objArray[1] == null ? 0 : Math.round(Double.parseDouble(objArray[1].toString())));
            mealSummaryMap.put(AppConstants.ChartServiceConstants.BREAKFAST_AVERAGE.getValue(), objArray[2] == null ? 0 : Math.round(Double.parseDouble(objArray[2].toString())));
            mealSummaryMap.put(AppConstants.ChartServiceConstants.LUNCH_AVERAGE.getValue(), objArray[3] == null ? 0 : Math.round(Double.parseDouble(objArray[3].toString())));
            mealSummaryMap.put(AppConstants.ChartServiceConstants.DINNER_AVERAGE.getValue(), objArray[4] == null ? 0 : Math.round(Double.parseDouble(objArray[4].toString())));
        }
		return mealSummaryMap;
	}
	
	private Map<String, Object> getGlucoseMap(Object glucoseData) {
		Map<String, Object> glucoseMap = new LinkedHashMap<String, Object>();
		Object[] objArray = (Object[]) glucoseData;
        if(objArray != null && objArray.length > 0){
            glucoseMap.put(AppConstants.ChartServiceConstants.AVG.getValue(),   objArray[0] == null? 0 : Math.round(Double.parseDouble(objArray[0].toString())));
            glucoseMap.put(AppConstants.ChartServiceConstants.UNDER.getValue(),  objArray[1] == null? 0 : Math.round(Double.parseDouble(objArray[1].toString())));
            glucoseMap.put(AppConstants.ChartServiceConstants.IN_TARGET.getValue(), objArray[2] == null? 0 : Math.round(Double.parseDouble(objArray[2].toString())));
            glucoseMap.put(AppConstants.ChartServiceConstants.OVER.getValue(),  objArray[3] == null? 0 : Math.round(Double.parseDouble(objArray[3].toString())));
            glucoseMap.put(AppConstants.ChartServiceConstants.TOTAL.getValue(), objArray[4] == null? 0 : Math.round(Double.parseDouble(objArray[4].toString())));
        }
		return glucoseMap;
	}
	
	private List<Object> getWeightDataList(List<Object> weightDataList) {
		List<Object> finalGlucoseLevelData = new ArrayList<Object>();
		for (Object object : weightDataList) {
			Object [] objArray = (Object [])object;
            if(objArray != null && objArray.length > 0) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put(AppConstants.ChartServiceConstants.LEVEL.getValue(), objArray[1] == null ? 0 : Math.round(Double.parseDouble(objArray[1].toString())));
                map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), objArray[2] == null ? 0 : objArray[2]);
                map.put(AppConstants.ChartServiceConstants.ID.getValue(), objArray[3] == null ? 0 : objArray[3]);
                finalGlucoseLevelData.add(map);
            }
		}
		return finalGlucoseLevelData;
	}

	private List<Object> getTookMedList(List<Object> tookMedDataList, long patientId) {
		List<Object> finalActivityDataList = new ArrayList<Object>();

		Patient patient = new PatientDAO().getPatientById(patientId);
		long offset = CommonUtils.getPatientFacilityTimezoneOffset(patient);

		for (Object object : tookMedDataList) {
			Object [] objArray = (Object [])object;
            if(objArray != null && objArray.length > 0) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();

                Long timestamp = 0L;
                if(objArray[1] instanceof Long) {
                    timestamp = (Long)objArray[1];
                } else if (objArray[1] instanceof BigInteger) {
                    timestamp = ((BigInteger)objArray[1]).longValue();
                }
                timestamp = DateUtils.getStartOfDayTime(timestamp) + offset;

                map.put(AppConstants.ChartServiceConstants.MEDS_TAKEN.getValue(), objArray[0] == null ? 0 : objArray[0]);
                map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), timestamp);
                finalActivityDataList.add(map);
            }
		}
		return finalActivityDataList;
	}

	private List<Object> getGlucoseLevelDataList(List<Object> glucoseLevelData) {
		List<Object> finalGlucoseLevelData = new ArrayList<Object>();
		for (Object object : glucoseLevelData) {
			Object [] objArray = (Object [])object;
            if(objArray != null && objArray.length > 0) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put(AppConstants.ChartServiceConstants.LEVEL.getValue(), objArray[1] == null ? 0 : Integer.parseInt(objArray[1].toString()));
                map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), objArray[2] == null ? 0 : objArray[2]);
                map.put(AppConstants.ChartServiceConstants.ID.getValue(), objArray[3] == null ? 0 : objArray[3]);
                map.put(AppConstants.ChartServiceConstants.GLUCOSE_TIME.getValue(), objArray[4] == null ? 0 : ObjectUtils.nullSafe(objArray[4]));
                finalGlucoseLevelData.add(map);
            }
		}
		return finalGlucoseLevelData;
	}
	
	private List<Object> getActivityDataList(List<Object> activityDataList) {
		List<Object> finalActivityDataList = new ArrayList<Object>();
		for (Object object : activityDataList) {
			Object [] objArray = (Object [])object;
            if(objArray != null && objArray.length > 0) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put(AppConstants.ChartServiceConstants.STEPS_PER_DAY.getValue(), objArray[0] == null ? 0 : objArray[0]);
                map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), objArray[1] == null ? 0 : objArray[1]);
                map.put(AppConstants.ChartServiceConstants.DATE.getValue(), objArray[2] == null ? 0 : objArray[2]);
                finalActivityDataList.add(map);
            }
		}
		return finalActivityDataList;
	}

    private List<Object> getActivityLogsDataListCombo(List<Object> misfitLogDataList, long patientId) {
        List<Object> finalMisfitLogsDataList = new ArrayList<Object>();
        for (Object object : misfitLogDataList) {
            Object [] objArray = (Object [])object;
            if(objArray != null && objArray.length > 0) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();

                Long timestamp = 0L;
                if (objArray[1] instanceof Long) {
                    timestamp = (Long) objArray[1];
                } else if (objArray[1] instanceof BigInteger) {
                    timestamp = ((BigInteger) objArray[1]).longValue();
                }
                //timestamp = timestamp + CommonUtils.getPatientTimeZoneOffset(patientId);

                map.put(AppConstants.ChartServiceConstants.STEPS.getValue(), objArray[0] == null ? 0 : objArray[0]);
                map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), objArray[1] == null ? 0 : timestamp);
                finalMisfitLogsDataList.add(map);
            }
        }
        return finalMisfitLogsDataList;
    }

	private List<Object> getActivityMinutesLogsDataList(List<Object> acitivityMinList) {
		List<Object> finalMisfitLogDataList = new ArrayList<Object>();
		for (Object object : acitivityMinList) {
			Object [] objArray = (Object [])object;
			if(objArray != null && objArray.length > 0) {
				Map<String, Object> map = new LinkedHashMap<String, Object>();

				Long timestamp = 0L;
				if (objArray[1] instanceof Long) {
					timestamp = (Long) objArray[1];
				} else if (objArray[1] instanceof BigInteger) {
					timestamp = ((BigInteger) objArray[1]).longValue();
				}

				map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), objArray[1] == null ? 0 : timestamp);
				map.put(AppConstants.ChartServiceConstants.ACTIVITY_MINUTES_PERFORMED.getValue(), objArray[0]);
				map.put(AppConstants.ChartServiceConstants.TYPE.getValue(), objArray[4]);

				finalMisfitLogDataList.add(map);
			}
		}
		return finalMisfitLogDataList;
	}

	private List<Object> getActivityLogsDataList(List<Object> misfitLogDataList, long patientId) {
		List<Object> finalMisfitLogsDataList = new ArrayList<Object>();
		for (Object object : misfitLogDataList) {
			Object [] objArray = (Object [])object;
            if(objArray != null && objArray.length > 0) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();

                Long timestamp = 0L;
                if (objArray[1] instanceof Long) {
                    timestamp = (Long) objArray[1];
                } else if (objArray[1] instanceof BigInteger) {
                    timestamp = ((BigInteger) objArray[1]).longValue();
                }
                timestamp = timestamp + CommonUtils.getPatientTimeZoneOffset(patientId);

                map.put(AppConstants.ChartServiceConstants.STEPS.getValue(), objArray[0] == null ? 0 : objArray[0]);
                map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), objArray[1] == null ? 0 : timestamp);
                finalMisfitLogsDataList.add(map);
            }
		}
		return finalMisfitLogsDataList;
	}

	private List<Object> getCarbsLevelDataList(List<Object> carbsLevelData) {
		List<Object> finalCarbsLevelData = new ArrayList<Object>();
		for (Object object : carbsLevelData) {
			Object [] objArray = (Object [])object;
            if(objArray != null && objArray.length > 0) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put(AppConstants.ChartServiceConstants.CARBS.getValue(), objArray[1] == null ? 0 : Math.round(Double.parseDouble(objArray[1].toString())));
                map.put(AppConstants.ChartServiceConstants.TYPE.getValue(), objArray[2] == null ? 0 : objArray[2]);
                map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), objArray[3] == null ? 0 : objArray[3]);
                map.put(AppConstants.ChartServiceConstants.HAS_MISSING_FOOD.getValue(), objArray[4] == null ? 0 : objArray[4]);
                map.put(AppConstants.ChartServiceConstants.ID.getValue(), objArray[5] == null ? 0 : objArray[5]);
                finalCarbsLevelData.add(map);
            }
		}
		return finalCarbsLevelData;
	}
	
	private Map<String, Object> getCalculatedWeightChart(Long startDate, Long endDate, List<MiscLog> miscLogList, Target target) {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		int weekStartWeight = 0; 
		int weekEndWeight = 0;
		int weightLostPercent = 0;
		int weightLost = 0;
		int weightToLoose = 0;
		if(miscLogList != null && miscLogList.size() > 0) {
			weekStartWeight = (int) miscLogList.get((miscLogList.size() -1)).getWeight();
			weekEndWeight = (int) miscLogList.get(0).getWeight();
		}
		weightLostPercent = (int) (target.getStartingWeight() -  weekEndWeight);
		weightLost = weekStartWeight -  weekEndWeight;
		weightToLoose = (int) (target.getStartingWeight() - target.getTargetWeight());
		float value2 = ((float)weightLostPercent/weightToLoose);
		float weightLostPercentage = ((float)value2 * 100);
		if(weightLostPercent <= 0) {
			weightLostPercentage = 0;
		}
		
		if(weightLostPercentage > 100) {
			weightLostPercentage = 100;
		}
		map.put(AppConstants.ChartServiceConstants.PERCENTAGE.getValue(), (int)Math.round(weightLostPercentage));
		map.put(AppConstants.ChartServiceConstants.WEIGHT_LOST.getValue(), weightLost);
		map.put(AppConstants.ChartServiceConstants.CURRENT_WEIGHT.getValue(), weekEndWeight);
		
		return map;
	}
	
	public String buildJsonResponse(String[] keys, String data[]){		
		Map<String, String> responseMaps = new LinkedHashMap<String, String>();
		for (int i = 0; i < keys.length; i++) {			
			responseMaps.put(keys[i], data[i]);
		}
		return JsonUtil.toJson(responseMaps);
	}

	private List<Object> addMissingDatesData(List<Object> list, Long dateFrom, Long dateTo) {
		List<Object> completeList  = new ArrayList<Object>();
		int totalDayRecord = DateUtils.getDaysBetween(dateFrom, dateTo) + 1; // +1 means including today date
		if(list.size() >= totalDayRecord) {
			return list;
		} else {
			for (int index = 0; index < totalDayRecord; index++) {
				Long recordDay = (index * DateUtils.ONE_DAY_MILLIS + dateFrom);
				if(list.size() != 0) {
					Object [] objArray = (Object []) list.get(0);
					Long timeToCheck = ((BigInteger) objArray[1]).longValue();
					if(DateUtils.isExistInRDay(timeToCheck, recordDay)) {
						completeList.add(objArray);
						list.remove(0);
					} else {
						Object[] newObjArray = new Object[]{0, recordDay, 0};
						completeList.add(newObjArray);
					}
				} else {
					Object[] newObjArray = new Object[]{0, recordDay, 0};
					completeList.add(newObjArray);
				}
			}
		}
		return completeList;
	}
}
