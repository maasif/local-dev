package com.healthslate.patientapp.ws;

import com.healthslate.patientapp.util.RemindersUtil;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class EmailScheduler {
	public final static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    @SuppressWarnings("unused")
	public static void schedulerReminders3DaysAfterNewMemberJoined() {
		final Runnable reminders = new Runnable() {
			public synchronized void run() {
				try {
					RemindersUtil.sendNewPatientEmailReminderFor3Days();
				} catch (Exception e) {
					e.printStackTrace();	
				}	
			}
		};
		final ScheduledFuture processHandler = scheduler.scheduleAtFixedRate(reminders, 60, 60 * 3, TimeUnit.MINUTES);
	}

    public static void schedulerReminders7DaysAfterNewMemberJoined() {
        final Runnable reminders = new Runnable() {
            public synchronized void run() {
                try {
                    RemindersUtil.sendNewPatientEmailReminderFor7Days();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        final ScheduledFuture processHandler = scheduler.scheduleAtFixedRate(reminders, 80, 60 * 4, TimeUnit.MINUTES);
    }
}