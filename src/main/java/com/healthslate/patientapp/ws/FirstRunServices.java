package com.healthslate.patientapp.ws;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.amazonaws.services.opsworks.model.App;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.CoachDTO;
import com.healthslate.patientapp.model.dto.FacilityAdminDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.google.gson.Gson;
import com.healthslate.patientapp.model.dto.SignUpDTO;

/*
* ======= FILE CHANGE HISTORY =======
* [2/12/2015]: when user registration is completed the set date, __oz
* [2/23/2015]: set starting/target weight to 0F , __oz
* ===================================
 */

@Path("/signUp")
public class FirstRunServices {
	static final Logger LOG = LoggerFactory.getLogger(FirstRunServices.class);

    @POST
    @Path("/patientSignUp")
    public Response patientSignUp(String signUpPatientString, @Context HttpServletRequest httpRequest) {

        signUpPatientString = CommonUtils.extractDataFromRequest(signUpPatientString, httpRequest);

        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        Patient patient = new Patient();
        PatientDAO patientDAO = new PatientDAO();
        User user = new User();
        FacilityDAO facilityDao = new FacilityDAO();
        ProviderDAO providerDao = new ProviderDAO();
        UserDAO userDao = new UserDAO();
        BaseDAO baseDao = new BaseDAO();
        LOG.info("patientSignUp service: Provided data: " + signUpPatientString);
        SignUpDTO signUpDto = (SignUpDTO) JsonUtil.fromJson(signUpPatientString, SignUpDTO.class);

        if (signUpDto != null) {
            //this condition is for if member sends request then Coach/Admin loads the page from 'Add Member' button from members list
            if (!ObjectUtils.isEmpty(signUpDto.getCallFrom()) && signUpDto.getCallFrom().equalsIgnoreCase(AppConstants.ADD_REQUESTED_MEMBER)) {
                User userFromDB = userDao.getUserByEmail(signUpDto.getEmail());
                Long facilityId = userFromDB.getPatient().getFacilities().get(0).getFacilityId();
                if (userFromDB != null) {
                    if(!patientDAO.isMRNAlreadyAssigned(signUpDto.getMrn(), facilityId)) {
                        List<Provider> selectedProviders = getSelectedProvidersByIds(signUpDto, providerDao);
                        Patient patientFromUser = userFromDB.getPatient();
                        Facility patientFacility = patientFromUser.getFacilities().get(0);
                        patientFromUser.setMrn(ObjectUtils.nullSafe(signUpDto.getMrn()));
                        patientFromUser.setDob(new Date(signUpDto.getDobString()));
                        patientFromUser.setGender(signUpDto.getGender());

                        //if facility default lead coach is set then set to patient as well when facility admin tries to enter member info from 'Add Member' button
                        long leadCoach = signUpDto.getLeadCoach();
                        if (leadCoach == 0) {
                            leadCoach = ObjectUtils.nullSafeLong(patientFacility.getLeadCoach());
                        }
                        patientFromUser.setIsApproved(AppConstants.REQ_APPROVED);
                        patientFromUser.setLeadCoachId(leadCoach);
                        patientFromUser.setPrimaryFoodCoachId(signUpDto.getPrimaryCoach());

                        populateUserForSignUp(userFromDB, signUpDto);
//                    boolean isSaved = baseDao.save(userFromDB);

                        for (Provider provider : selectedProviders) {
                            if (provider.getPatients() != null) {
                                List<Patient> patients = provider.getPatients();
                                patients.add(patientFromUser);
                                provider.setPatients(patients);
                            }
                        }

                        patientFromUser.setProviders(selectedProviders);
                        userFromDB.setPatient(patientFromUser);
                        boolean isSaved = baseDao.save(userFromDB);

                        //if facility have skipped consent enable then email app URL to member, else send TOS page
                        if (patientFacility.getIsSkipConsent()) {
                            patientFromUser.setIsConsentAccepted(true);
                            baseDao.save(patientFromUser);
                            CommonUtils.sendAppLinks(patientFromUser);
                            LOG.info("patientSignUp service: Sending member app links: " + user.toString());
                        } else {
                            Facility facility = HSSessionUtils.getFacilitySession(httpRequest, patientFacility);
                            NotifyUtils.notifyMemberToEnrollInHealthSlateProgram(patientFromUser, facility, httpRequest);
                            LOG.info("patientSignUp service: Notifying member to get enroll in healthslate program: " + user.toString());
                        }

                        if (isSaved) {
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                            LOG.info("patientSignUp service: User Saved: " + user.toString());
                        } else {
                            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SIGNUP);
                            LOG.info("patientSignUp service: User not saved: " + user.toString());
                        }
                    } else {
                        LOG.info("patientSignUp service: MRN_ALREADY_ASSIGNED");
                        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.MRN_ALREADY_ASSIGNED);
                    }
                }
            } else {
                if (!userDao.isUserExist(signUpDto.getEmail())) {
                    Facility requestedFacility = facilityDao.getFacilityByName(signUpDto.getFacility());
                    Long facilityId = requestedFacility.getFacilityId();
                    if (!patientDAO.isMRNAlreadyAssigned(signUpDto.getMrn(), facilityId)) {
                        // Facility requestedFacility = facilityDao.getFacilityByName(signUpDto.getFacility());
                        List<Provider> selectedProviders = getSelectedProvidersByIds(signUpDto, providerDao);

                        Facility atozFacility = facilityDao.getAtoZFacility();
                        // List<Provider> requestedProvidersList = providerDao.getProvidersByFacilityId(requestedFacility.getFacilityId());
                        List<Provider> atozProvidersList = providerDao.getRecruitmentProvidersByFacilityId(atozFacility.getFacilityId());

                        populateUserForSignUp(user, signUpDto);
                        patient.setUser(user);

                        List<Facility> facilities = new ArrayList<Facility>();
                        facilities.add(requestedFacility);
                        facilities.add(atozFacility);
                        for (Facility facility : facilities) {
                            if (facility.getPatients() != null) {
                                List<Patient> patients = facility.getPatients();
                                patients.add(patient);
                                facility.setPatients(patients);
                            }
                        }
                        patient.setFacilities(facilities);
                        patient.setUseInsulin(signUpDto.isUseInsulin());
                        patient.setPrimaryFoodCoachId(signUpDto.getPrimaryCoach());

                        List<Provider> providers = new ArrayList<Provider>();
                        providers.addAll(selectedProviders);
                        providers.addAll(atozProvidersList);
                        for (Provider provider : providers) {
                            if (provider.getPatients() != null) {
                                List<Patient> patients = provider.getPatients();
                                patients.add(patient);
                                provider.setPatients(patients);
                            }
                        }

                        patient.setIsInvited(false);
                        patient.setIsApproved("1");
                        patient.setPrimaryFoodCoachId(signUpDto.getPrimaryCoach());

                        if (!ObjectUtils.isEmpty(signUpDto.getDobString())) {
                            patient.setDob(new Date(signUpDto.getDobString()));
                        }

                        //it means facility admin sending invites to members
                        if (signUpDto.isInvitation()) {
                            patient.setIsInvited(true);
                            patient.setIsApproved(AppConstants.REQ_APPROVED);
                            patient.setInvitationSentDate(System.currentTimeMillis());
                            patient.setUseInsulin(null);
                        } else {
                            patient.setIsApproved(AppConstants.REQ_IN_PROCESS_0);
                            patient.setRequestReceivedDate(System.currentTimeMillis());
                        }

                        long leadCoach = signUpDto.getLeadCoach();
                        if (leadCoach == 0) {
                            leadCoach = ObjectUtils.nullSafeLong(requestedFacility.getLeadCoach());
                        }
                        patient.setLeadCoachId(leadCoach);
                        patient.setProviders(providers);
                        patient.setTarget(CommonUtils.getDefaultTarget(patient));
                        patient.setUuid(CommonUtils.generateUUID());
                        patient.setIsThreeDayEmailSent(false);
                        patient.setMrn(ObjectUtils.nullSafe(signUpDto.getMrn()));
                        patient.setGender(signUpDto.getGender());
                        user.setPatient(patient);

                        UserRole userRole = new UserRole();
                        userRole.setAuthority(AppConstants.Roles.ROLE_PATIENT.name());
                        userRole.setUser(user);
                        user.setIsEnabled(true);
                        boolean isSaved = baseDao.save(user);
                        new UserRoleDAO().save(userRole);

                        if (ObjectUtils.isEmpty(signUpDto.getCallFrom())) {
                            //if facility have skipped consent enable then email app URL to member, else send TOS page
                            if (requestedFacility.getIsSkipConsent()) {
                                patient.setIsConsentAccepted(true);
                                patient.setIsApproved(AppConstants.REQ_APPROVED);
                                baseDao.save(patient);
                                LOG.info("patientSignUp service: Sending member app links: " + user.toString());
                                CommonUtils.sendAppLinks(patient);
                            } else {
                                Facility facility = HSSessionUtils.getFacilitySession(httpRequest, requestedFacility);
                                NotifyUtils.notifyMemberToEnrollInHealthSlateProgram(patient, facility, httpRequest);
                                LOG.info("patientSignUp service: Notifying member to get enroll in healthslate program: " + user.toString());
                            }
                        } else {
                            //notifying facility admin for the new member request
                            Facility facility = HSSessionUtils.getFacilitySession(httpRequest, requestedFacility);
                            List<FacilityAdminDTO> facilityAdmins = facilityDao.listFacilityAdmins(facility.getFacilityId());

                            if (facilityAdmins != null && facilityAdmins.size() > 0) {
                                for (FacilityAdminDTO adminDTO : facilityAdmins) {
                                    NotifyUtils.notifyAdminOfNewMemberRequest(adminDTO.getEmail(), adminDTO.getFirstName(), httpRequest);
                                }
                                LOG.info("patientSignUp service: Notifying facility admin for new member: " + user.toString());
                            } else {
                                //if facility doesn't have facility admin, then notify facility coaches about new member
                                if (selectedProviders != null && selectedProviders.size() > 0) {
                                    for (Provider pro : selectedProviders) {
                                        User proUser = pro.getUser();
                                        if (proUser != null) {
                                            NotifyUtils.notifyAdminOfNewMemberRequest(proUser.getEmail(), proUser.getFirstName(), httpRequest);
                                        } else {
                                            LOG.info("patientSignUp service: Notifying facility coaches null exception");
                                        }
                                    }
                                    LOG.info("patientSignUp service: Notifying facility coaches for new member: " + user.toString());
                                }
                            }
                        }

                        if (isSaved) {
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                            LOG.info("patientSignUp service: User Saved: " + user.toString());
                        } else {
                            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SIGNUP);
                            LOG.info("patientSignUp service: User not saved: " + user.toString());
                        }
                    } else {
                        LOG.info("patientSignUp service: MRN_ALREADY_ASSIGNED");
                        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.MRN_ALREADY_ASSIGNED);
                    }
                } else {
                    LOG.info("patientSignUp service: PATIENT_ALREADY_EXISTS");
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.PATIENT_ALREADY_EXISTS);
                }
            }
        } else {
            LOG.info("patientSignUp service: Provided data is null");
            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.NO_DATA_FOUND_IN_REQ);
        }
        String response = JsonUtil.toJsonExcludedNull(returnDataMap);
        LOG.info("patientSignUp service: Returning response: " + response);
        return Response.ok(response).build();
    }

	@POST
	@Path("/patientSignUpComplete")
	public Response patientSignUpComplete(String signUpPatientString, @Context HttpServletRequest httpRequest) {

		signUpPatientString = CommonUtils.extractDataFromRequest(signUpPatientString, httpRequest);

		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		Patient patient = new Patient();
		UserDAO userDao = new UserDAO();
		User user = new User();
		PatientDAO patientDao = new PatientDAO();
		SignUpDTO signUpDto = new Gson().fromJson(signUpPatientString, SignUpDTO.class);
		if(signUpDto != null) {
			LOG.info("patientSignUpComplete service: Provided data: SignUpDTO: " + signUpDto.toString());
			if(userDao.isUserExist(signUpDto.getEmail())) {
				user = userDao.getUserByEmail(signUpDto.getEmail());
				if(user.getIsRegistrationCompleted()) {
					returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.EMAIL_ALREADY_EXIST);
				}else {
					//populateUserForSignUp(user, signUpDto, false);
					patient = patientDao.getPatientByUserId(user.getUserId());

                    boolean isConsentAccepted = ObjectUtils.nullSafe(patient.getIsConsentAccepted());

                    if(!isConsentAccepted){
                        LOG.info("patientSignUpComplete service: Member not yet accepted TOS from Web: " + user.toString());
                        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.NOT_APPROVED_MESSAGE);
                        String response = JsonUtil.toJsonExcludedNull(returnDataMap);
                        LOG.info("patientSignUpComplete service: TOS not accepted, returning response: "+response);
                        return Response.ok(response).build();
                    }

                    String approvedStatus = ObjectUtils.nullSafe(patient.getIsApproved());
                    // [0, 1] means request is in process not approved yet
                    // 2 means denied by admin
                    // 3 means approved by admin
                    if (approvedStatus.equalsIgnoreCase(AppConstants.REQ_IN_PROCESS_0) || approvedStatus.equalsIgnoreCase(AppConstants.REQ_IN_PROCESS_1)) {
                        LOG.info("patientSignUpComplete service: Member not yet approved: " + user.toString());
                        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.NOT_APPROVED_MESSAGE);
                    } else if (approvedStatus.equalsIgnoreCase(AppConstants.REQ_DENIED)) {
                        LOG.info("patientSignUpComplete service: Member denied by admin: " + user.toString());
                        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.DENIED_REQUEST_MESSAGE);
                    } else {
                        user.setPatient(patient);
                        user.setPhone(CommonUtils.formatPhoneNumber(signUpDto.getPhoneNumber()));
                        patient.setUser(user);
                        boolean isSavedOnElgg = updateElggProfile(patient, user, httpRequest);
                        LOG.info("patientSignUpComplete service: patient profile saved on elgg: " + isSavedOnElgg);
                        if (isSavedOnElgg) {
                            patient.setIsConsentAccepted(true);
                            patient.setIsApproved(AppConstants.REQ_APPROVED);
                            patient.setIsArchived(true);
                            user.setIsRegistrationCompleted(true);
                            user.setPassword(new BCryptPasswordEncoder().encode(signUpDto.getPassword()));
                            boolean isSaved = new BaseDAO().save(user);
                            List<Provider> foodCoachesList = CommonUtils.getFoodCoachesList(patient.getProviders());
                            Facility patientFacility = patient.getFacilities().get(0);
                            NotifyUtils.notifyFoodCoachesForNewMember(httpRequest, patientFacility.getName(), patient.getPatientId(), foodCoachesList);
                            Provider provider = CommonUtils.getLeadCoach(patient);
                            if(provider != null) {
                                NotifyUtils.notifyLeadCoachForNewMember(httpRequest, patientFacility.getName(), patient.getPatientId(), provider);
                            }
                            LOG.info("patientSignUpComplete service: User saved successfully: " + user.toString());
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                        } else {
                            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SIGNUP);
                            LOG.info("patientSignUpComplete service: Unable to save patient on elgg: " + user.toString());
                        }
                    }
				}
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.EMAIL_NOT_REGISTERED);
			}
        } else {
            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.NO_DATA_FOUND_IN_REQ);
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("patientSignUpComplete service: Returning response: "+response);
		return Response.ok(response).build();
	}

	@POST
	@Path("/verifyEmail")
	public Response emailVerify(String emailString, @Context HttpServletRequest httpRequest) {
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
		UserDAO userDao = new UserDAO();
		User user = new User();

		emailString = CommonUtils.extractDataFromRequest(emailString, httpRequest);

		LOG.info("verifyEmail service: email to verify: "+emailString);
		if(!ObjectUtils.isEmpty(emailString)) {
			if(userDao.isUserExist(emailString)) {
				user = userDao.getUserByEmail(emailString);
                if(user.getIsRegistrationCompleted()) {
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.EMAIL_ALREADY_EXIST);
                }else {
                    Patient patient = user.getPatient();
                    if (patient != null && user.getUserType().equalsIgnoreCase(AppConstants.PATIENT)) {
                        boolean isConsentAccepted = ObjectUtils.nullSafe(patient.getIsConsentAccepted());

                        if (!isConsentAccepted) {
                            LOG.info("patientSignUpComplete service: Member not yet accepted TOS from Web: " + user.toString());
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.NOT_APPROVED_MESSAGE);
                            String response = JsonUtil.toJsonExcludedNull(returnDataMap);
                            LOG.info("patientSignUpComplete service: TOS not accepted, returning response: " + response);
                            return Response.ok(response).build();
                        }

                        if (user.getIsRegistrationCompleted()) {
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.EMAIL_ALREADY_EXIST);
                        } else {
                            // [0, 1] means request is in process not approved yet
                            // 2 means denied by admin
                            // 3 means approved by admin
                            String approvedStatus = ObjectUtils.nullSafe(patient.getIsApproved());

                            if (approvedStatus.equalsIgnoreCase(AppConstants.REQ_IN_PROCESS_0) || approvedStatus.equalsIgnoreCase(AppConstants.REQ_IN_PROCESS_1)) {
                                LOG.info("verifyEmail service: Member not yet approved: " + user.toString());
                                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.NOT_APPROVED_MESSAGE);
                            } else if (approvedStatus.equalsIgnoreCase(AppConstants.REQ_DENIED)) {
                                LOG.info("verifyEmail service: Member denied by admin: " + user.toString());
                                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.DENIED_REQUEST_MESSAGE);
                            } else {
                                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS);
                            }
                        }
                    } else {
                        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL_PASSWORD);
                    }
                }
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.EMAIL_NOT_REGISTERED);
			}
		} else {
			returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL);
		}
		String response = JsonUtil.toJsonExcludedNull(returnDataMap);
		LOG.info("verifyEmail service: Returning response: "+response);
		return Response.ok(response).build();
	}

	@POST
	@Path("/emailTOS")
	public Response emailTOS(String emailString , @Context HttpServletRequest httpRequest) {

		emailString = CommonUtils.extractDataFromRequest(emailString, httpRequest);

		Map<String, String> prefMap = new PreferencesDAO().map();
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
		LOG.info("emailTOS service: email: "+emailString);
		if(!ObjectUtils.isEmpty(emailString)) {
			try {
				InputStream inputStream = httpRequest.getServletContext().getResourceAsStream(AppConstants.FILE_TOS);
				String emailData = CommonUtils.convertStreamToString(inputStream);
				boolean isSentEmail = EmailUtils.sendTextInEmail(emailData, emailString,
						prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
						prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
						prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
						prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
						AppConstants.EmailSubjects.TOS.getValue());
				if(isSentEmail) {
					returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS);
				} else {
					returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SEND_TOS);	
				}
			} catch (Exception e) {
				e.printStackTrace();
				LOG.info("emailTOS service: exception: "+e.getMessage());
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SEND_TOS);
			}
		} else {
			returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL);
		}
		String response = JsonUtil.toJsonExcludedNull(returnDataMap);
		LOG.info("emailTOS service: Returning Response: "+response);
		return Response.ok(response).build();
	}

	private boolean updateElggProfile(Patient patient, User user, HttpServletRequest request) {

        Map<String, String> prefMap = new PreferencesDAO().map();
        int groupId = 0;

		// Send ELGG Request for creating a user
		try {

			if(user.getSocialId() == 0) {
				int socialId = SocialServicesUtils.createPatientInSocialGroup(user);
				if(socialId == -1) {
					return false;
				} else {
					user.setSocialId(socialId);
				}
			}

            Facility facilityFromDB = patient.getFacilities().get(0);
            if(facilityFromDB != null){
                groupId = facilityFromDB.getGroupId();
            }

            if(user.getGroupId() == 0) {
				int status = SocialServicesUtils.patientJoinGroup(patient, groupId);
				if(status == -1) {
					return false;
				} else {
					user.setGroupId(groupId);
				}
			}

			Long leadCoachId = patient.getLeadCoachId();
			if(leadCoachId != null) {
				Provider provider = new ProviderDAO().getProviderById(leadCoachId);
                CoachSessionPreference coachSessionPreference = provider.getCoachSessionPreference();
                String firstName = user.getFirstName();
                String welcomeMessage = null;
                if(coachSessionPreference == null){
                    welcomeMessage = new SmsTemplateDAO().getSmsTemplateByName(AppConstants.SmsTemplates.WELCOME_MEMBER_MESSAGE_WITHOUT_PREFS.getValue());
                    welcomeMessage = welcomeMessage.replace(AppConstants.StringReplaceChunks.F_NAME.getValue(), firstName);
                }else{
                    welcomeMessage = new SmsTemplateDAO().getSmsTemplateByName(AppConstants.SmsTemplates.WELCOME_MEMBER_MESSAGE.getValue());
                    welcomeMessage = welcomeMessage.replace(AppConstants.StringReplaceChunks.F_NAME.getValue(), firstName);
                    String sessionPrefs = "<br/>"+ coachSessionPreference.getTimes() + "<br/>" + coachSessionPreference.getDays() + "<br/>";
                    welcomeMessage = welcomeMessage.replace(AppConstants.StringReplaceChunks.PREFS.getValue(), sessionPrefs);
                }

                welcomeMessage = welcomeMessage.replace(AppConstants.StringReplaceChunks.C_NAME.getValue(), provider.getUser().getFirstName() + " " + provider.getUser().getLastName());

				String status = LogServices.createInfoPost(provider, patient, AppConstants.ELGG_MSG_COACH);

				if(status.equalsIgnoreCase(AppConstants.SUCCESS)){

					status = LogServices.createInfoPost(provider, patient, AppConstants.ELGG_MSG_TOPICS);

					if(status.equalsIgnoreCase(AppConstants.SUCCESS)){

						status = LogServices.buildMessagePost(provider, patient, welcomeMessage);

						if(status.equalsIgnoreCase(AppConstants.SUCCESS)){

							status = LogServices.createInfoPost(provider, patient, AppConstants.ELGG_MSG_MENU);

							if(status.equalsIgnoreCase(AppConstants.SUCCESS)){

								LogServices.createInfoPost(provider, patient, AppConstants.ELGG_MSG_ADD_LOG);
							}
						}
					}
				}
			}
			
			if(patient.getImage() != null && !patient.getImage().isEmpty()) {
				SocialGroupServices groupServices = new SocialGroupServices();
				int status = groupServices.uploadProfileImagePost(patient);
				if(status == -1) {
					return false;
				}
			}  
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("updateElggProfile: exception: "+e.getMessage());
			return false;
		}
	}

	private static String extractOnlyFirstName(String name){
		String newName = name;
		Pattern pattern = Pattern.compile("\\s");
		Matcher matcher = pattern.matcher(newName);
		boolean found = matcher.find();

		if(found){
			newName = name.split(" ")[0];
		}

		return newName;
	}

	private void populateUserForSignUp(User user, SignUpDTO signUpDto) {

        String firstName = ObjectUtils.nullSafe(signUpDto.getFirstName()).trim();
        String lasName = ObjectUtils.nullSafe(signUpDto.getLastName()).trim();

        user.setFirstName(firstName);
        user.setLastName(lasName);
        user.setDisplayName(CommonUtils.formatPatientName(firstName, lasName, ObjectUtils.nullSafe(user.getDisplayName())));

        user.setEmail(signUpDto.getEmail());
        user.setPhone(CommonUtils.formatPhoneNumber(signUpDto.getPhoneNumber()));
        user.setAddress(ObjectUtils.nullSafe(signUpDto.getAddress()));
        user.setCity(ObjectUtils.nullSafe(signUpDto.getCity()));
        user.setState(ObjectUtils.nullSafe(signUpDto.getState()));
        user.setZipCode(ObjectUtils.nullSafe(signUpDto.getZipCode()));

        user.setIsRegistrationCompleted(false);
		user.setRegistrationDate(new Date());
		user.setPasswordModifiedTime(System.currentTimeMillis());

        user.setUserType(AppConstants.PATIENT);

		if(!ObjectUtils.isEmpty(signUpDto.getGender())) {
			user.setGender(signUpDto.getGender());
		}
	}

    private List<Long>  getProvidersIdsFromStringIds(String providersIds){
        String[] ids = providersIds.split(", ");
        List<Long> completeProviderList = new ArrayList<Long>();

        for(String id : ids){
            completeProviderList.add(Long.parseLong(id));
        }

        return completeProviderList;
    }

    private List<Provider> getSelectedProvidersByIds(SignUpDTO signUpDTO, ProviderDAO providerDAO){
        List<Provider> providers = new ArrayList<Provider>();
        List<Long> providersIds = new ArrayList<Long>();

        if(signUpDTO.getSelectedProviders() != null){
            providersIds = getProvidersIdsFromStringIds(signUpDTO.getSelectedProviders());
            providers = providerDAO.getProvidersByIds(providersIds);
        }
        return providers;
    }
}