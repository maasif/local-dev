package com.healthslate.patientapp.ws;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.healthslate.patientapp.util.JsonUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.proxy.HibernateProxy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.healthslate.patientapp.model.dao.FoodLogSummaryDAO;
import com.healthslate.patientapp.model.dao.LogDAO;
import com.healthslate.patientapp.model.entity.FoodLogDetail;
import com.healthslate.patientapp.model.entity.FoodLogSummary;
import com.healthslate.patientapp.model.entity.FoodMaster;
import com.healthslate.patientapp.model.entity.Log;
import com.healthslate.patientapp.util.AppConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/log")
public class FoodLogSummaryServices {
    static final Logger LOG = LoggerFactory.getLogger(FoodLogSummaryServices.class);
	@GET
	@Path("/getFoodLogSummary")
    public Response getFoodLogSummary(@Context HttpServletRequest request, @QueryParam("logId") String logId) {
        System.out.println("FOOD-LOG-SUMMARY: Log Id " + logId);
		String logJson = null;
		FoodLogSummaryDAO logSummaryDAO = new FoodLogSummaryDAO();
		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(),AppConstants.ServicesConstants.ERROR.name());
		LOG.info("getFoodLogSummary service: LogId: "+logId);
		LogDAO LogDAO =new LogDAO();
		Log log = null;
        try {
            if(!ObjectUtils.isEmpty(logId)){
             log = LogDAO.getLogById(logId);
                if(log != null && log.getFoodLogSummary() != null  && log.getFoodLogSummary().getFoodLogDetails() != null && log.getFoodLogSummary().getFoodLogDetails().size() > 0){
                    LOG.info("getFoodLogSummary service: Found Log from DB: "+log.toString());
                    System.out.println("FOOD-LOG-SUMMARY: Log found");
                    FoodLogSummary summary = logSummaryDAO.getSummaryById(log.getFoodLogSummary().getFoodLogSummaryId());
                    if(summary != null){
                        LOG.info("getFoodLogSummary service: FoodLogSummary: "+summary.toString());
                        System.out.println("FOOD-LOG-SUMMARY: Food log summary found");
                        List<FoodLogDetail> foodLogDetails = summary.getFoodLogDetails();
                        if(foodLogDetails != null){
                            System.out.println("FOOD-LOG-SUMMARY: detail is not null");
                            for(FoodLogDetail detail: foodLogDetails){
                                LOG.info("getFoodLogSummary service: detail: "+detail.toString());
                                System.out.println("FOOD-LOG-SUMMARY: Detail ID:" + detail.getFoodLogDetailId());
                                Hibernate.initialize(detail);
                                System.out.println("FOOD-LOG-SUMMARY: Detail ID:" + detail.getFoodLogDetailId() + " Initialized");
                                FoodMaster detailedFm = detail.getFoodMaster();
                                Hibernate.initialize(detailedFm);
                                System.out.println("FOOD-LOG-SUMMARY: Food Master ID:" + detailedFm.getItemId() + " Initialized");
                                if(detailedFm instanceof HibernateProxy) {
                                    System.out.println("FOOD-LOG-SUMMARY: Detail Instance of Hibernate Proxy");
                                    HibernateProxy proxy = (HibernateProxy)detailedFm;
                                    FoodMaster master = (FoodMaster)proxy.getHibernateLazyInitializer().getImplementation();
                                    detail.setFoodMaster(master);
                                }
                            }
                        }
                        log.setLogMoods(null);
                        System.out.println("FOOD-LOG-SUMMARY: Detail Initialized");
                        log.setFoodLogSummary(summary);
                        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                        logJson = JsonUtil.toJsonExcludedNull(log);
                        logJson = logJson.replace("\\u0026", "&");
                        System.out.println("FOOD-LOG-SUMMARY: return to device: " + logJson);
                        returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), logJson);
                    }
                    /*for(FoodLogDetail detail: summary.getFoodLogDetails()){
                        Hibernate.initialize(detail.getFoodMaster());
                        HibernateProxy proxy = (HibernateProxy)detail.getFoodMaster();
                        FoodMaster master = (FoodMaster)proxy.getHibernateLazyInitializer().getImplementation();
                        detail.setFoodMaster(master);
                    }*/
                }else{
                    LOG.info("getFoodLogSummary service: FoodLogSummary not found against LogId: "+logId);
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No FoodLogSummery Found");
                }
            } else{
                LOG.info("getFoodLogSummary service: Incorrect LogId: " + logId);
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Incorrect Log Id");
            }
        } catch (HibernateException e) {
            System.out.println("FOOD-LOG-SUMMARY: Exception: " + e.getMessage());
            LOG.info("getFoodLogSummary service: HibernateException: "+e.getMessage());
            e.printStackTrace();
        }

        System.out.println("FOOD-LOG-SUMMARY: Response: " + JsonUtil.toJson(returnDataMap));
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("getFoodLogSummary service: Returning Response: "+response);
        return Response.ok(response).build();
	}
}
