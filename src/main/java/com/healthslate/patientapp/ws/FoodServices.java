package com.healthslate.patientapp.ws;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.healthslate.patientapp.model.entity.FoodMaster;
import com.healthslate.patientapp.model.entity.NutrionixHit;
import com.healthslate.patientapp.model.entity.NutrionixItem;
import com.healthslate.patientapp.util.AppConstants.NUTRIONIX;
import com.healthslate.patientapp.util.FoodUtils;
import com.healthslate.patientapp.util.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/food")
public class FoodServices {
	static final Logger LOG = LoggerFactory.getLogger(FoodServices.class);
	@GET
	@Path("/search")
	public Response search (@Context HttpServletRequest request, @QueryParam("phrase") String phrase) {
		try {
			String searchUrl = NUTRIONIX.SEARCH_URL.getValue()+
										phrase+"?"+
										NUTRIONIX.APP_KEY_URL_KEY.getValue()+"="+
										NUTRIONIX.APP_KEY.getValue()+"&"+
										NUTRIONIX.APP_ID_URL_KEY.getValue()+"="+
										NUTRIONIX.APP_ID.getValue()+"&"+
										NUTRIONIX.RESULTS_URL_KEY.getValue()+"="+
										"0:20"+"&"+
										NUTRIONIX.FIELDS_URL_KEY.getValue()+"="+
										"brand_id,brand_name,item_id,item_name,item_description,nf_calories,nf_sodium,nf_total_carbohydrate,nf_total_fat,nf_protein,nf_serving_size_unit";
			LOG.info("search food service: searching URL: "+searchUrl);
			URLConnection urlConnection = (HttpURLConnection)new URL(searchUrl).openConnection();
			
			Gson gson= new Gson();
			JsonElement element = new JsonParser().parse(ObjectUtils.getStringFromStream(urlConnection.getInputStream()));
			JsonObject jsonObject = element.getAsJsonObject();
			List<NutrionixHit> nutrionixHit = gson.fromJson(jsonObject.get("hits"), new TypeToken<List<NutrionixHit>>() {}.getType());
			List<FoodMaster> foodMasters = FoodUtils.getFoodMasterListFromNutrionixHitList(nutrionixHit);
			String response = gson.toJson(foodMasters);
			LOG.info("search food service: Returning response: "+response);
			return Response.ok(response).build();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			LOG.info("search food service: MalformedURLException: "+e.getMessage());
			return Response.serverError().build();
		} catch (IOException e) {
			e.printStackTrace();
			LOG.info("search food service: IOException: "+e.getMessage());
			return Response.serverError().build();
		}
	}
	
	@GET
	@Path("/getItemDetail")
	public Response getItemDetail (@Context HttpServletRequest request, @QueryParam("itemId") String itemId) {
		try {
			String searchUrl = NUTRIONIX.ITEM_URL.getValue()+"?"+
					NUTRIONIX.APP_KEY_URL_KEY.getValue()+"="+
					NUTRIONIX.APP_KEY.getValue()+"&"+
					NUTRIONIX.APP_ID_URL_KEY.getValue()+"="+
					NUTRIONIX.APP_ID.getValue()+"&"+
					NUTRIONIX.ID_URL_KEY.getValue()+"="+
					itemId;
			LOG.info("getItemDetail food service: searching URL: "+searchUrl);
			URLConnection urlConnection = (HttpURLConnection)new URL(searchUrl).openConnection();
			Gson gson= new Gson();
			JsonElement element = new JsonParser().parse(ObjectUtils.getStringFromStream(urlConnection.getInputStream()));
			JsonObject jsonObject = element.getAsJsonObject();
			NutrionixItem nutrionixItem = gson.fromJson(jsonObject, NutrionixItem.class);
			FoodMaster foodMaster = FoodUtils.initFoodMasterFromNutrionixItem(nutrionixItem);
			String response = gson.toJson(foodMaster);
			LOG.info("getItemDetail food service: Returning response: "+response);
			return Response.ok(response).build();
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
			LOG.info("search food service: JsonSyntaxException: "+e.getMessage());
			return Response.serverError().build();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			LOG.info("search food service: MalformedURLException: "+e.getMessage());
			return Response.serverError().build();
		} catch (IOException e) {
			e.printStackTrace();
			LOG.info("search food service: IOException: "+e.getMessage());
			return Response.serverError().build();
		}
	}
}