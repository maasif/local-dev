package com.healthslate.patientapp.ws;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.google.gson.reflect.TypeToken;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.MessageGcmDTO;
import com.healthslate.patientapp.model.dto.PatientDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import net.sf.json.JSON;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.json.JSONUtil;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.healthslate.patientapp.sync.SyncLog;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
* ======= FILE CHANGE HISTORY =======
* [2/19/2015]: notifyCoachOfPatientMessage() added by __oz
* [7/27/2015]: sendMeMoreSuggestedMeals() added by __oz
* [8/05/2015]: getMisfitLogs() added by __oz
* ===================================
 */

@Path("/log")
public class LogServices {
	static final Logger LOG = LoggerFactory.getLogger(LogServices.class);
	@SuppressWarnings({ "unchecked", "unused" })
	@POST
	@Path("saveLog")
	public Response saveLog(String logJsonString , @Context HttpServletRequest httpRequest){

		logJsonString = CommonUtils.extractDataFromRequest(logJsonString, httpRequest);

		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		GlucoseLogTypeDAO glucoseLogTypeDAO = new GlucoseLogTypeDAO();
		MiscLogDAO miscLogDAO = new MiscLogDAO();
		MessageDAO messageDAO = new MessageDAO();
		PatientDAO patientDAO = new PatientDAO();
		DeviceDAO deviceDAO = new DeviceDAO();
		LogDAO logDAO = new LogDAO();
		GlucoseLogDAO glucoseLogDAO = new GlucoseLogDAO();
		LOG.info("saveLog service: Provided logJsonString: "+logJsonString);
		Log log = (Log) JsonUtil.fromJson(logJsonString, Log.class);
		Log logFromServer = logDAO.getLogById(log.getLogId());

		SocialGroupServices services = new SocialGroupServices();
		
		if(log != null){
			LOG.info("saveLog service started: LOG ID: "+log.getLogId());
			String mac = httpRequest.getHeader(AppConstants.UserInfo.MAC_ADDRESS.getValue());
			LOG.info("saveLog service: device mac address: "+mac);
			Device device = deviceDAO.getDeviceByMac(mac);
			if(device != null){
				if(log.getPatient() != null && log.getPatient().getPatientId() != null){
					Patient patient = patientDAO.getPatientById(log.getPatient().getPatientId());
					if(patient != null){
						LOG.info("saveLog service: get patient from log, Patient: "+patient.toString());
						log.setPatient(patient);
						LOG.info("saveLog service: logType: "+log.getLogType());
						//if Glucose log
						if(log.getLogType().equalsIgnoreCase(AppConstants.LOG_TYPE_GLUCOSE)){
							try {
								LOG.info("saveLog service: Deleting glucose log from DB for logId: "+log.getLogId());
								glucoseLogDAO.removeglucoseLog(log.getLogId());

                                glucoseLogDAO.removeGlucoseLogByPatientAndMeterName(log.getGlucoseLog().getTimestamp(),
                                                        log.getGlucoseLog().getMeterSerialNumber(),
                                                        log.getPatient().getPatientId());
							} catch (Exception e){
								e.printStackTrace();
								LOG.info("saveLog service: Removing glucose log Exception: "+e.getMessage());
							}
							log.getGlucoseLog().setLog(log);
							
							if(log.getGlucoseLog().getGlucoseLogType() != null){
								GlucoseLogType type = glucoseLogTypeDAO.getGlucoseLogTypeByDescription(log.getGlucoseLog().getGlucoseLogType().getDescription());
								log.getGlucoseLog().setGlucoseLogType(type);
							}
						} else if(log.getLogType().equalsIgnoreCase(AppConstants.LOG_TYPE_MEAL)) {
							LOG.info("saveLog service: Deleting existing meal log from DB for logId: "+log.getLogId());
							dataMap = deleteExistingSummary(log);
							
							log.getFoodLogSummary().setLog(log);
							LOG.info("saveLog service: Populating food log details if not null");
							if(log.getFoodLogSummary().getFoodLogDetails() != null && log.getFoodLogSummary().getFoodLogDetails().size() > 0){
								LOG.info("saveLog service: Food log details size: "+log.getFoodLogSummary().getFoodLogDetails().size());
								for(FoodLogDetail foodLogDetail: log.getFoodLogSummary().getFoodLogDetails()){
									foodLogDetail.setFoodLogSummary(log.getFoodLogSummary());
								}
							}

							LOG.info("saveLog service: Populating food Image if not null");
							if(log.getFoodLogSummary().getFoodImage() != null){
								FoodImage image = (FoodImage) dataMap.get(AppConstants.IMAGE);
								if(image == null){
									image = log.getFoodLogSummary().getFoodImage();
								}					
								
								if(image != null){									
									Date date = new Date();
									image.setTimestamp(date.getTime());								
									image.setFoodLogSummary(log.getFoodLogSummary());
									log.getFoodLogSummary().setFoodImage(image);
								}
								LOG.info("saveLog service: populated food Image data");
							}

							LOG.info("saveLog service: Populating food Audio if not null");
							if(log.getFoodLogSummary().getFoodAudios() != null && log.getFoodLogSummary().getFoodAudios().size() > 0){
								List<FoodAudio> oldAudios =(List<FoodAudio>) dataMap.get(AppConstants.AUDIO) ;
								File fileDir = new File(CommonUtils.getProjectPath() + AppConstants.LOG_AUDIO_FOLDER);
								if (!fileDir.exists()){
									fileDir.mkdir();
								}

								LOG.info("saveLog service: Food audio size: "+log.getFoodLogSummary().getFoodAudios().size());
								for(FoodAudio foodAudio: log.getFoodLogSummary().getFoodAudios()){
									if(foodAudio != null){
										String uploadedFileLocation = fileDir.getAbsolutePath() + "\\" + foodAudio.getAudioName();
										foodAudio.setServerAudioPath(uploadedFileLocation);
										foodAudio.setFoodLogSummary(log.getFoodLogSummary());
										Date date = new Date();
										foodAudio.setTimestamp(date.getTime());
									}
								}
							}
						}  else if(log.getLogType().equalsIgnoreCase(AppConstants.LOG_TYPE_ACTIVITY_MINUTE)) {
							LOG.info("saveLog service: populating activityLog if not null");
							if(log.getActivityLog() != null){
                                try{
                                   new ActivityLogDAO().removeActivityLog(log.getLogId());
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
								log.getActivityLog().setLog(log);
							}
						} else if(log.getLogType().equalsIgnoreCase(AppConstants.LOG_TYPE_EXERCISE)) {
							LOG.info("saveLog service: populating exerciseLog if not null");
							if(log.getExerciseLogs() != null ){
								LOG.info("saveLog service: populating exerciseLog: size: "+log.getExerciseLogs().size());
								for(ExerciseLog exerciseLog: log.getExerciseLogs()){
									exerciseLog.setLog(log);
								}
							}	
						} else {
							if(log.getMiscLog() != null){
								try {
									LOG.info("saveLog service: Removing miscLog from db for logId: "+log.getLogId());
									miscLogDAO.removeMiscLog(log.getLogId());
								} catch (Exception e){	
									e.printStackTrace();
									LOG.info("saveLog service: Removing miscLog from db exception: "+e.getMessage());
								}
								log.getMiscLog().setLog(log);
							}	
						}
						LOG.info("saveLog service: Removing logTimeOffSet from LogTime: offset value: "+log.getLogTimeOffset());

						Long time = log.getLogTime();
						time = time - log.getLogTimeOffset();
						log.setLogTime(time);
						
						log.setIsProcessed(false);						
						if(log.getStatus().equalsIgnoreCase(AppConstants.PROCESSED)){
							log.setIsProcessed(true);	
						}

						log.setDeviceMacAddress(mac);
						log.setSyncStatus(AppConstants.LogSyncStatus.UNSYNCHRONIZED.name());
						log.setLastModified(System.currentTimeMillis());
						log.setIsRemoved(false);
						log.setIsSuggested(false);

						if(dataMap.size() > 0){
							List<Message> messages = (List<Message>) dataMap.get(AppConstants.MESSAGES);
							if(messages != null && messages.size() > 0){
								for(Message m: messages){
									m.setFoodLogSummary(log.getFoodLogSummary());
									messageDAO.save(m);
									LOG.info("saveLog service: message saved in DB: "+m.toString());
								}
							}							
						}
						LOG.info("saveLog service: Populating LogMoods if not null");
						if(log.getLogMoods() != null && log.getLogMoods().size() > 0) {
							LOG.info("saveLog service: Populating LogMoods size: "+log.getLogMoods().size());
							for (LogMood logMood : log.getLogMoods()) {
								logMood.setLog(log);
							}
						}
						
						log.setUploadTime(System.currentTimeMillis());
						
						boolean isSaved = logDAO.save(log);
						LOG.info("saveLog service: Saved log on server: status: "+isSaved+", log: "+log.toString());
						saveLogInFile(patient.getPatientId(), logJsonString, isSaved);
						if(isSaved) {
							returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
							returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), String.valueOf(log.getLogId()));
							//stop sending Weight logs to Elgg, _oz[31/08/2015]:PA-2012
							if(!log.getLogType().equalsIgnoreCase(AppConstants.LOG_TYPE_WEIGHT)){
								LOG.info("saveLog service: posting log to elgg other than Weight log");
								//if its a new log then send elgg for new post
								if(logFromServer == null) {
									LOG.info("saveLog service: Sending new log entry to elgg");
									sendNewPost(log);
								}else {
									FoodLogSummary foodLogSummary = log.getFoodLogSummary();
									if(foodLogSummary != null) {
										LOG.info("saveLog service: Sending updated log entry to elgg");
										services.sendUpdatedPost(log, foodLogSummary.getType(), null);
									}else{
										LOG.info("saveLog service: Sending log entry to elgg with foodLogSummary null");
										if(log.getActivityLog() != null){
											services.sendUpdatedPost(log, null, log.getActivityLog().getType());
										}else{
											services.sendUpdatedPost(log, null, null);
										}
									}
								}
							}
						} else {
							returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SAVE_LOG);
						}
					} else {
						returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No patient found");
					}
				} else {
					returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No patient found");
				}
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Device not registered");
			}
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("saveLog service: Returning response: "+response);
		return Response.ok(response).build();
	}	

	private void saveLogInFile(Long patientId, String jsonString, boolean isSaved) {
		try {
			String path = CommonUtils.getProjectPath() + AppConstants.LOG_DEBUG_FOLDER;
			File dirtory = new File(path);
			if(!dirtory.exists()) {
				dirtory.mkdirs();
			}
			File file = new File(path + AppConstants.FILE_SAVE_LOG_REQUEST);
			if(!file.exists()) {
				file.createNewFile();
			}
			LOG.info("saveLog service: Saving log to server debug file at: "+file.getAbsolutePath());
			LOG.info("saveLog service: Saving log to server debug file with JsonString: "+jsonString);
			String data = "PatientId: "+ patientId + AppConstants.NEW_LINE 
					+"Date: " + new Date() + AppConstants.NEW_LINE 
					+"IsLogSaved: " + isSaved + AppConstants.NEW_LINE 
					+"Data: "+ jsonString + AppConstants.NEW_LINE 
					+ "--------------------------------------------------" + AppConstants.NEW_LINE;
			PrintWriter printWriter = new PrintWriter(new FileOutputStream(file, true));
			printWriter.append(data);
			printWriter.flush();
			printWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
			LOG.info("saveLog service: Saving log to server debug file IOException: "+e.getMessage());
		}
	}
	
	private Map<String, Object> deleteExistingSummary(Log log) {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		FoodLogSummaryDAO foodLogSummaryDAO = new FoodLogSummaryDAO();
		FoodLogDetailDAO detailDAO = new FoodLogDetailDAO();
		MessageDAO messageDAO = new MessageDAO();
		LogDAO logDAO = new LogDAO();
		
		if(log != null){
			Log existingLog = logDAO.getLogById(log.getLogId());
			if(existingLog != null){
				if(existingLog.getFoodLogSummary() != null){
					
					FoodLogSummary summary = existingLog.getFoodLogSummary();
					existingLog.setFoodLogSummary(null);
					logDAO.save(existingLog);					
					
					FoodImage image = summary.getFoodImage(); /*here*/
					dataMap.put(AppConstants.IMAGE, image) ;
					List<FoodAudio> audios = null;
					List<Message> messages = null;

					try {
						Hibernate.initialize(summary.getMessages());
						messages = summary.getMessages();
						Hibernate.initialize(summary.getFoodAudios());
						audios = summary.getFoodAudios();
					} catch (HibernateException e) {

					}

					dataMap.put(AppConstants.AUDIO, audios);
					List<FoodLogDetail> details = summary.getFoodLogDetails();					
					if(messages != null && messages.size() > 0){
						for(Message m: messages){
							m.setFoodLogSummary(null);
							messageDAO.save(m);
						}
						
						dataMap.put(AppConstants.MESSAGES, messages);
					}
					
					if(details != null && details.size() > 0){
						for(FoodLogDetail detail: details){
							detail.setFoodLogSummary(null);
							detailDAO.save(detail);
							detailDAO.delete(detail);
						}
					}
					
					summary.setFoodImage(null);
					summary.setFoodAudios(null);
					summary.setFoodLogDetails(null);
					summary.setMessages(null);
					summary.setLog(null);
					foodLogSummaryDAO.save(summary);										
					foodLogSummaryDAO.delete(summary);
				}
			}
		}
		
		return dataMap;
	}

	//send new post to elgg
	public void sendNewPost(Log log) {
		SocialGroupServices groupServices = new SocialGroupServices();
		try {
			String jsonString = "";
			FoodLogSummary foodLogSummary = log.getFoodLogSummary();
			if(foodLogSummary != null) {
				if(foodLogSummary.getHasImage() !=null && foodLogSummary.getHasImage()) {
					if(log.getIsProcessed() || log.getFoodLogSummary().isAteAgain()) {
						groupServices.sendImagePost(log);
					}
				}else {
					groupServices.sendImagePost(log);
				}
				return;
			}
			
			if(log.getGlucoseLog() != null){
				buildMiscLogForSendingToElgg(log, log.getGlucoseLog().getGlucoseLevel() + " mg/dl", "", AppConstants.NEWSFEED.GLUCOSE.getValue(), log.getIsShareWithGroup());
			}
			
			if(log.getMiscLog() != null) {
				if(log.getLogType().equalsIgnoreCase(AppConstants.LOG_TYPE_WEIGHT)){
					buildMiscLogForSendingToElgg(log, log.getMiscLog().getWeight()+"", "", AppConstants.NEWSFEED.WEIGHT.getValue(), log.getIsShareWithGroup());
				} else if(log.getLogType().equalsIgnoreCase(AppConstants.LOG_TYPE_ACTIVITY)){
					buildMiscLogForSendingToElgg(log, log.getMiscLog().getStepsPerDay()+" steps", "", AppConstants.NEWSFEED.ACTIVITY.getValue(),log.getIsShareWithGroup());
				} else if(log.getLogType().equalsIgnoreCase(AppConstants.LOG_TYPE_MEDICATION)) {
					buildMiscLogForSendingToElgg(log, "Took Medication", "", AppConstants.NEWSFEED.MEDICATION.getValue(),log.getIsShareWithGroup());
				}
			} else if(log.getActivityLog() != null) { // we are sending type ACTIVITY in this case too
				buildMiscLogForSendingToElgg(log, log.getActivityLog().getMinutesPerformed()+" mins", log.getActivityLog().getType(), AppConstants.NEWSFEED.ACTIVITY.getValue(),log.getIsShareWithGroup());
			}
			
			if(log.getExerciseLogs() != null ){
				
				for(ExerciseLog exerciseLog: log.getExerciseLogs()){
					if(exerciseLog.getType().equalsIgnoreCase(AppConstants.NEWSFEED.CARDIO.getValue())){
						
						Map<String, String> dataMap = new LinkedHashMap<String, String>();
						
						dataMap.put(AppConstants.NEWSFEED.USERNAME.getValue(), log.getPatient().getUser().getEmail());
						dataMap.put(AppConstants.NEWSFEED.TITLE.getValue(),(int) exerciseLog.getMinutesPerformed() + " minutes");
						dataMap.put(AppConstants.NEWSFEED.POST_TYPE.getValue(), AppConstants.NEWSFEED.ACTIVITY_CARDIO.getValue());
						dataMap.put(AppConstants.NEWSFEED.TAGS.getValue(), AppConstants.NEWSFEED.ACTIVITY_CARDIO.getValue());
						dataMap.put(AppConstants.NEWSFEED.LOG_TIME.getValue(), log.getCreatedOn().toString());
						
						if(log.getIsShareWithGroup() != null && log.getIsShareWithGroup() == true){
							dataMap.put(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.GROUP.getValue());
						}
						else if(log.getIsShareWithGroup() != null && log.getIsShareWithGroup() == false){
							dataMap.put(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.PRIVATE.getValue());
						}
						
						jsonString = JsonUtil.toJson(dataMap);
						SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.UPLOADPOST.getValue());
					}
					else if(exerciseLog.getType().equalsIgnoreCase(AppConstants.NEWSFEED.STRENGTH.getValue())){
						
						Map<String, String> dataMap = new LinkedHashMap<String, String>();
						
						dataMap.put(AppConstants.NEWSFEED.USERNAME.getValue(), log.getPatient().getUser().getEmail());
						dataMap.put(AppConstants.NEWSFEED.TITLE.getValue(),(int) exerciseLog.getMinutesPerformed() + " minutes");
						dataMap.put(AppConstants.NEWSFEED.POST_TYPE.getValue(), AppConstants.NEWSFEED.ACTIVITY_STRENGTH.getValue());
						dataMap.put(AppConstants.NEWSFEED.TAGS.getValue(), AppConstants.NEWSFEED.ACTIVITY_STRENGTH.getValue());
						dataMap.put(AppConstants.NEWSFEED.LOG_TIME.getValue(), log.getCreatedOn().toString());
						
						if(log.getIsShareWithGroup() != null && log.getIsShareWithGroup() == true){
							dataMap.put(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.GROUP.getValue());
						}
						else if(log.getIsShareWithGroup() != null && log.getIsShareWithGroup() == false){
							dataMap.put(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.PRIVATE.getValue());
						}
						
						jsonString = JsonUtil.toJson(dataMap);
						SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.UPLOADPOST.getValue());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("saveLog service: sendNewPost: Sending log to elgg exception: "+e.getMessage());
		}
	}
	
	public static void buildMiscLogForSendingToElgg(Log log, String title, String description, String type, Boolean isShared){
		Map<String, String> dataMap = new LinkedHashMap<String, String>();
		try {
			dataMap.put(AppConstants.NEWSFEED.CUSTOM_ID.getValue(), log.getLogId());
			dataMap.put(AppConstants.NEWSFEED.USERNAME.getValue(), log.getPatient().getUser().getEmail());
			dataMap.put(AppConstants.NEWSFEED.TITLE.getValue(), title);

            if(!ObjectUtils.isEmpty(description)){
                dataMap.put(AppConstants.NEWSFEED.DESCRIPTION.getValue(), description);
            }
			dataMap.put(AppConstants.NEWSFEED.POST_TYPE.getValue(), type);
			dataMap.put(AppConstants.NEWSFEED.LOG_TIME.getValue(), log.getCreatedOn().toString());

			if(isShared!= null && isShared){
				dataMap.put(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.GROUP.getValue());
			} else {
				dataMap.put(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.PRIVATE.getValue());
			}

			String jsonString = JsonUtil.toJson(dataMap);
			SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.UPLOADPOST.getValue());

		} catch (IOException e) {
			e.printStackTrace();
			LOG.info("saveLog service: buildMiscLogForSendingToElgg: IOException: "+e.getMessage());
		}
	}

	public static String buildMessagePost(Provider provider, Patient patient, String message ){

		Map<String, String> dataMap = new LinkedHashMap<String, String>();
		try {
			dataMap.put(AppConstants.NEWSFEED.CUSTOM_ID.getValue(), provider.getProviderId()+"");
			dataMap.put(AppConstants.NEWSFEED.USERNAME.getValue(), patient.getUser().getEmail());
			dataMap.put(AppConstants.NEWSFEED.TITLE.getValue(), provider.getUser().getFirstName() + " " + provider.getUser().getLastName() );
			dataMap.put(AppConstants.NEWSFEED.DESCRIPTION.getValue(), message);
			dataMap.put(AppConstants.NEWSFEED.POST_TYPE.getValue(), AppConstants.SOCIALKEYS.MESSAGE.getValue());

			dataMap.put(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.PRIVATE.getValue());

			String jsonString = JsonUtil.toJson(dataMap);
			String response = SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.UPLOADPOST.getValue());
			JsonElement jelement = new JsonParser().parse(response);
			JsonObject jobject = jelement.getAsJsonObject();

			int statusFromElgg = jobject.get(AppConstants.SOCIALKEYS.STATUS.getValue()).getAsInt();
			if(statusFromElgg != -1){
				return AppConstants.SUCCESS;
			}
		} catch (IOException e) {
			e.printStackTrace();
			LOG.info("buildMessagePost: IOException: "+e.getMessage());
		}
		return AppConstants.ERROR;

	}

	public static String buildMessagePost(Provider provider, List<String> emails, String message ){

		Map<String, String> dataMap = new LinkedHashMap<String, String>();
		try {
			dataMap.put(AppConstants.NEWSFEED.LIST_OF_USERS.getValue(), StringUtils.join(emails, " "));
			dataMap.put(AppConstants.NEWSFEED.TITLE.getValue(), provider.getUser().getFirstName() + " " + provider.getUser().getLastName() );
			dataMap.put(AppConstants.NEWSFEED.DESCRIPTION.getValue(), message);
			dataMap.put(AppConstants.NEWSFEED.POST_TYPE.getValue(), AppConstants.SOCIALKEYS.MESSAGE.getValue());
			dataMap.put(AppConstants.NEWSFEED.CUSTOM_ID.getValue(), provider.getProviderId()+"");

			String jsonString = JsonUtil.toJson(dataMap);
			LOG.info("buildMessagePostForUsers: sendingResponse: "+jsonString);
			String response = SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.UPLOAD_POST_FOR_USERS.getValue());
			JsonElement jelement = new JsonParser().parse(response);
			JsonObject jobject = jelement.getAsJsonObject();

			int statusFromElgg = jobject.get(AppConstants.SOCIALKEYS.STATUS.getValue()).getAsInt();
			if(statusFromElgg != -1){
				return AppConstants.SUCCESS;
			}
		} catch (IOException e) {
			e.printStackTrace();
			LOG.info("buildMessagePost: IOException: "+e.getMessage());
		}
		return AppConstants.ERROR;

	}

	public static String buildMessagePost(Provider provider, Patient patient, String message, String videoLink){

		Map<String, String> dataMap = new LinkedHashMap<String, String>();
		try {
			dataMap.put(AppConstants.NEWSFEED.CUSTOM_ID.getValue(), provider.getProviderId()+"");
			dataMap.put(AppConstants.NEWSFEED.USERNAME.getValue(), patient.getUser().getEmail());
			dataMap.put(AppConstants.NEWSFEED.TITLE.getValue(), provider.getUser().getFirstName() + " " + provider.getUser().getLastName() );
			dataMap.put(AppConstants.NEWSFEED.DESCRIPTION.getValue(), message);
			dataMap.put(AppConstants.NEWSFEED.POST_TYPE.getValue(), AppConstants.NEWSFEED.VIDEOURL.getValue());
			dataMap.put(AppConstants.NEWSFEED.FILE_URL.getValue(), videoLink);
			dataMap.put(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.PRIVATE.getValue());

			String jsonString = JsonUtil.toJson(dataMap);
			String response = SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.UPLOADPOST.getValue());
			JsonElement jelement = new JsonParser().parse(response);
			JsonObject jobject = jelement.getAsJsonObject();

			int statusFromElgg = jobject.get(AppConstants.SOCIALKEYS.STATUS.getValue()).getAsInt();
			if(statusFromElgg != -1){
				return AppConstants.SUCCESS;
			}
		} catch (IOException e) {
			e.printStackTrace();
			LOG.info("buildMessagePost: IOException: "+e.getMessage());
		}
		return AppConstants.ERROR;

	}

    public static String createInfoPost(Provider provider, Patient patient, String message){

        Map<String, String> dataMap = new LinkedHashMap<String, String>();
        try {
            dataMap.put(AppConstants.NEWSFEED.USERNAME.getValue(), patient.getUser().getEmail());
            dataMap.put(AppConstants.NEWSFEED.DESCRIPTION.getValue(), message);
            dataMap.put(AppConstants.NEWSFEED.POST_TYPE.getValue(), AppConstants.SOCIALKEYS.INFO.getValue());

            dataMap.put(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.PRIVATE.getValue());

            String jsonString = JsonUtil.toJson(dataMap);
            String response = SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.UPLOADPOST.getValue());

			JsonElement jelement = new JsonParser().parse(response);
			JsonObject jobject = jelement.getAsJsonObject();

			int statusFromElgg = jobject.get(AppConstants.SOCIALKEYS.STATUS.getValue()).getAsInt();
			if(statusFromElgg != -1){
				return AppConstants.SUCCESS;
			}
        } catch (IOException e) {
            e.printStackTrace();
			LOG.info("createInfoPost: IOException: "+e.getMessage());
        }

		return AppConstants.ERROR;
    }

    @GET
	@Path("getProcessedLogsByDate")
	public Response getProcessedLogByDate(
			@QueryParam("patientId") Long patientId,
			@QueryParam("date") Long dateFrom,
			@Context HttpServletRequest httpRequest) {

		LogDAO logDAO = new LogDAO();
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LOG.info("getProcessedLogsByDate service: Provided data: patientId: "+patientId+", DateFrom: "+dateFrom);
		if(patientId != null && dateFrom != null) {
			Long dateTo = System.currentTimeMillis(); 
			List<SyncLog> logs = logDAO.getProcessedMealLogs(patientId, dateFrom, dateTo);
			if(logs != null && logs.size() > 0) {
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
				returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), logs);
				returnDataMap.put(AppConstants.SessionKeys.DATE_TO.name(), dateTo);
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");
			}
		} else {
			returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Provided data cannot be null");
		}
		String response = JsonUtil.toJsonExcludedNull(returnDataMap);
		LOG.info("getProcessedLogsByDate service: Returning response: "+response);
		return Response.ok(response).build();
	}
	
    /**
     * Depricated 
    */
	@POST
	@Path("getLogByDates")
	public Response getLogByDates(String jsonString, @Context HttpServletRequest httpRequest) {
		LOG.info("getLogByDates POST service: Provided data: Provided jsonString: "+jsonString);

		jsonString = CommonUtils.extractDataFromRequest(jsonString, httpRequest);

		JsonElement jelement = new JsonParser().parse(jsonString);
			 JsonObject jobject = jelement.getAsJsonObject() ;
			 Long patientId = jobject.get("patientId").getAsLong();
			 Long requiredDate = jobject.get("date").getAsLong();
			 int days = jobject.get("days").getAsInt();
		
		LogDAO logDAO = new LogDAO();
		PatientDAO patientDAO = new PatientDAO();
		Calendar cal = Calendar.getInstance();
		
		Long start = null;
		Long end = null;
		
		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		Map<String, String> logsAudiosImages = new LinkedHashMap<String, String>();
		
		Date registrationDate = patientDAO.getPatientById(patientId).getUser().getRegistrationDate();					
		
		requiredDate = Math.abs(requiredDate);
		cal.setTimeInMillis(registrationDate.getTime());
				
		if ( requiredDate == 0 || requiredDate <= cal.getTimeInMillis() || days == 0 ) { // if  given time is 0 or less than user's registration time then start
													 // time will be user's registration time and end time will be next 10 days
			start = registrationDate.getTime();			
			cal.setTimeInMillis(start);
			end = new Date().getTime();			
		} else if ( requiredDate >= cal.getTimeInMillis() ) { // if  given time is greater than user's registration time then 
			//start time will be given time and end time will be next 10 days
			start = DateUtils.getStartDate(requiredDate, days);
			cal.setTimeInMillis(start);
			end = requiredDate;
		}
		LOG.info("getLogByDates POST service: Getting logs for date range: dateFrom: "+start+", dateEnd: "+end);
		List<SyncLog> logs = logDAO.getLogsByDateFilters(patientId, start, end);
		
		logsAudiosImages.put("logs", JsonUtil.toJsonExcludedNull(logs));
		
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
		returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), JsonUtil.toJson(logsAudiosImages));
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getLogByDates POST service: Returning respone: "+response);
		return Response.ok(response).build();
	}
	
	@GET
	@Path("getLogByDates")
	public Response getLogByDates(
			@QueryParam("patientId") Long patientId,
			@QueryParam("date") Long dateFrom,
			@QueryParam("days") Integer days,
			@Context HttpServletRequest httpRequest) {				
		LOG.info("getLogByDates GET service: Provided data: PatientId: "+patientId+", dateFrom: "+dateFrom+", days: "+days);
		LogDAO logDAO = new LogDAO();
		PatientDAO patientDAO = new PatientDAO();
		Calendar cal = Calendar.getInstance();
		
		Long start = null;
		Long end = null;
		
		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		Map<String, String> logsAudiosImages = new LinkedHashMap<String, String>();

		Patient patient = patientDAO.getPatientById(patientId);
		if(patient != null) {
			User user = patient.getUser();
			if(user != null) {
				Date registrationDate = user.getRegistrationDate();
				if(registrationDate != null) {
					dateFrom = Math.abs(dateFrom);
					cal.setTimeInMillis(registrationDate.getTime());

					if ( dateFrom == 0 || dateFrom <= cal.getTimeInMillis() || days == 0 ) { // if  given time is 0 or less than user's registration time then start
						// time will be user's registration time and end time will be next 10 days
						start = registrationDate.getTime();
						cal.setTimeInMillis(start);
						end = new Date().getTime();
					} else if ( dateFrom >= cal.getTimeInMillis() ) { // if  given time is greater than user's registration time then
						//start time will be given time and end time will be next 10 days
						start = DateUtils.getStartDate(dateFrom, days);
						cal.setTimeInMillis(start);
						end = dateFrom;
					}
					LOG.info("getLogByDates GET service: Getting logs for date range: dateFrom: "+start+", dateEnd: "+end);
					List<SyncLog> logs = logDAO.getLogsByDateFilters(patientId, start, end);
					List<MessageGcmDTO> messagesMessageGcmDTOs = new ArrayList<MessageGcmDTO>();
					messagesMessageGcmDTOs.addAll(new MessageDAO().getAllPatientProviderMessages(patientId));
					messagesMessageGcmDTOs.addAll(new TechSupportMessageDAO().getAllPatientTechSupportMessages(patientId));
//					List<MessageGcmDTO> messagesMessageGcmDTOs = new MessageDAO().getAllPatientProviderMessages(patientId);
					/*List<Log> suggestedLogs = new SuggestedLogDAO().getAllSuggestedLogsByPatient(patientId);
					List<Log> favoriteLogs = logDAO.getAllFavoriteLogsByPatient(patientId);*/
					logsAudiosImages.put("logs", JsonUtil.toJsonExcludedNull(logs));
					logsAudiosImages.put("messages", JsonUtil.toJson(messagesMessageGcmDTOs));
					/*logsAudiosImages.put("suggestedLogs", JsonUtil.toJsonExcludedNull(suggestedLogs));
					logsAudiosImages.put("favoriteLogs", JsonUtil.toJsonExcludedNull(favoriteLogs));*/

					returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
					returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), JsonUtil.toJson(logsAudiosImages));

				} else {
					returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Registration Date not found");
				}
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "User not found");
			}
		} else {
			returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Patient not found");
		}
		String respone = JsonUtil.toJson(returnDataMap);
		LOG.info("getLogByDates GET service: Returning response: "+respone);
		return Response.ok(respone).build();
	}

	@POST
	@Path("updateFavoriteLog")
	public Response updateFavoriteLog(String jsonString, @Context HttpServletRequest httpRequest) {
		jsonString = CommonUtils.extractDataFromRequest(jsonString, httpRequest);
		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(),AppConstants.ServicesConstants.ERROR.name());
		LOG.info("updateFavoriteLog POST service: Provided data: Provided jsonString: "+jsonString);
		JsonElement jelement = new JsonParser().parse(jsonString);
		JsonObject jobject = jelement.getAsJsonObject() ;
		String logId = jobject.get("logId").getAsString();
		Boolean isFavorite = jobject.get("status").getAsBoolean();

		LogDAO logDAO = new LogDAO();
		if (ObjectUtils.isEmpty(logId) || isFavorite == null) {
			returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Provided data cannot be null");
		} else {
			LOG.info("updateFavoriteLog POST service: ");
			Log log = logDAO.getLogById(logId);
			if(log != null) {
				log.setIsFavorite(isFavorite);
				boolean isSave = new BaseDAO().save(log);
				LOG.info("updateFavoriteLog POST service: Log is updated with status: "+isSave+", and log: "+log.toString());
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
			} else {
				LOG.info("updateFavoriteLog POST service: Log not found. LogId: "+logId);
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Log not found");
			}
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getLogByDates POST service: Returning respone: "+response);
		return Response.ok(response).build();
	}

	public FormDataMultiPart getMultipartFormData(String logsJson, String imagesJson, String audiosJson) {	
		
		FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
		
		FormDataBodyPart formDataBodyPart = new FormDataBodyPart(AppConstants.MultiPartKeys.LOGS.toString(), logsJson);
		formDataMultiPart.bodyPart(formDataBodyPart);
		
		formDataBodyPart = new FormDataBodyPart(AppConstants.MultiPartKeys.IMAGES.toString(), imagesJson);
		formDataMultiPart.bodyPart(formDataBodyPart);
	
		formDataBodyPart = new FormDataBodyPart(AppConstants.MultiPartKeys.AUDIOS.toString(), audiosJson);
		formDataMultiPart.bodyPart(formDataBodyPart);
		
		return formDataMultiPart;
	}

	@GET
	@Path("getLog")
	public Response getLog(@QueryParam("patientId") Long patientId , @Context HttpServletRequest httpRequest){
		LOG.info("getLog service: Provided Data: PatientId: "+patientId);
		FoodLogSummaryDAO logSummaryDAO = new FoodLogSummaryDAO();
		LogDAO logDAO = new LogDAO();
		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		
		Date date = new Date();
		List<Log> logs = logDAO.getLogByPatientId(patientId);
		logs = CommonUtils.getUniqueResults(logs);

		LOG.info("getLog: Logs sync time: "+date.getTime());
		//update log sync time
		for(Log log: logs){
			log.setSyncTime(date.getTime());
			log.setSyncStatus(AppConstants.LogSyncStatus.SYNCHRONIZED.name());
			if(log != null && log.getFoodLogSummary() != null &&
					log.getFoodLogSummary().getFoodLogDetails() != null && log.getFoodLogSummary().getFoodLogDetails().size() > 0){

				FoodLogSummary summary = logSummaryDAO.getSummaryById(log.getFoodLogSummary().getFoodLogSummaryId());
				log.setFoodLogSummary(summary);
			}
			logDAO.save(log);
			LOG.info("getLog service: Saved log: "+log.toString());
		}
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
		returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(logs));
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getLog: Returning Response: "+response);
		return Response.ok(response).build();
	}
	
	@POST
	@Path("/syncLog")
    public Response syncLog(String jsonString, @Context HttpServletRequest httpRequest){

		jsonString = CommonUtils.extractDataFromRequest(jsonString, httpRequest);

		LOG.info("syncLog service: Provided JsonString: "+jsonString);
		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(),AppConstants.ServicesConstants.ERROR.name());
		LogDAO logDAO = new LogDAO();
		
		if(!jsonString.isEmpty()){
			
			JsonElement jelement = new JsonParser().parse(jsonString);
			JsonObject jobject = jelement.getAsJsonObject();
			
			String message = jobject.get(AppConstants.ServicesConstants.STATUS.name()).getAsString();
			Log log = logDAO.getLogById(jobject.get(AppConstants.GCMKEYS.LOG_ID.getValue()).getAsString());
				
			if(message.equalsIgnoreCase(AppConstants.ServicesConstants.SUCCESS.name())){
				if(log != null){
					log.setSyncStatus(AppConstants.LogSyncStatus.SYNCHRONIZED.name());
					Date today = new Date();
					log.setSyncTime(today.getTime());
				}
			} else{
				if(log != null){
					log.setSyncStatus(AppConstants.LogSyncStatus.SYNC_FAILED.name());
					Date today = new Date();
					log.setSyncTime(today.getTime());
				}
			}
			logDAO.save(log);
			LOG.info("syncLog service: Saved log: "+log.toString());
			returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("syncLog service: Returning respone: "+response);
		return Response.ok(response).build();
	}
	
	@POST
	@Path("/removeLog")
    public Response removeLog(String logId, @Context HttpServletRequest httpRequest){

		logId = CommonUtils.extractDataFromRequest(logId, httpRequest);

		LOG.info("removeLog service: Provided data: logId: "+logId);
        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(),AppConstants.ServicesConstants.SUCCESS.name());
		LogDAO logDAO = new LogDAO();
		
		if(!ObjectUtils.isEmpty(logId)){
			Log log = logDAO.getLogById(logId);
			if(log != null){
				try{
					log.setIsRemoved(true);
					Date today = new Date();
					log.setRemoveDate(today);
					logDAO.save(log);
					/*Modification for DeletePost*/
					JsonObject jobject = new JsonObject();
					jobject.addProperty(AppConstants.NEWSFEED.USERNAME.getValue(), log.getPatient().getUser().getEmail());
					jobject.addProperty(AppConstants.NEWSFEED.CUSTOM_ID.getValue(), log.getLogId());
					
					String jsonString = JsonUtil.toJson(jobject);
					String response = SocialServicesUtils.postSocialData(jsonString, AppConstants.SOCIALKEYS.DELETEPOST.getValue());
				} catch(Exception e){
					e.printStackTrace();
					LOG.info("removeLog service: Exception: "+e.getMessage());
					returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
					returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Error in removing log");
				}
			}
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("removeLog service: Returning response: "+response);
		return Response.ok(response).build();
	}
	
	@POST
	@Path("/saveSmsReply")
	public Response saveSmsReply(@Context HttpServletRequest httpRequest) {
        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
        returnDataMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());

	  try {
		  String body =	httpRequest.getParameter("Body").trim();
		  String from =	httpRequest.getParameter("From");
		  //String to = httpRequest.getParameter("To");
		  //String ssid = httpRequest.getParameter("MessageSid");
		  LOG.info("saveSmsReply service: provided data: body: "+body+", from: "+from);
		  if(body.length() > 10) {
			  String largetMessageString = "Please use the app for communicating with your team.";
			  TwilioMessageUtils.sendSMS(from, largetMessageString);
		  } 
		  SmsReplyDAO smsReplyDAO = new SmsReplyDAO();
		  SmsReply smsReply = smsReplyDAO.getLatestMessageByPhoneNumber(from);
		  
		  if(smsReply == null){
			  smsReply = new SmsReply();
		  }
		  
		  smsReply.setIsTookMed(false);
		  
		  if(body.equalsIgnoreCase("y") || ObjectUtils.isEmpty(body)){
			  smsReply.setIsTookMed(true);
		  }
		  
		  //smsReply.setMessageBody(body);
		  smsReply.setRepliedTime(System.currentTimeMillis());
		  smsReply.setMessageReplyBody(body);
		  
		  smsReplyDAO.saveOrUpdateSmsReply(smsReply);
		  LOG.info("saveSmsReply service: save/update smsReply: "+smsReply.toString());
		  if(smsReply.getIsTookMed()){
              //send to device via GCM
              returnDataMap = sendToDeviceViaGCM(smsReply, httpRequest);
		  }  
		} catch (Exception e) {
		  e.printStackTrace();
		  LOG.info("saveSmsReply service: Exception: "+e.getMessage());
		}

        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("saveSmsReply service: Returned data: " + response);
        return Response.ok().build();
	}

	@POST
	@Path("/saveMisfitActivityLog")
	public Response saveMisfitActivityLog(String misFitActivityJsonString, @Context HttpServletRequest httpRequest) {
		misFitActivityJsonString = CommonUtils.extractDataFromRequest(misFitActivityJsonString, httpRequest);
		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		try {
			LOG.info("saveMisfitActivityLog service: provided data: "+misFitActivityJsonString);
			if(ObjectUtils.isEmpty(misFitActivityJsonString)) {
				returnDataMap.put(AppConstants.JsonConstants.REASON.name(), "No data provided");
			} else {
				List<MisfitActivityLog> misfitActivityLogs = JsonUtil.fromJsonList(misFitActivityJsonString, new TypeToken<List<MisfitActivityLog>>(){}.getType());
				BaseDAO baseDAO = new BaseDAO();
				if(misfitActivityLogs != null && misfitActivityLogs.size() > 0) {
					for (MisfitActivityLog misfitActivityLog: misfitActivityLogs) {
						misfitActivityLog.setUploadTime(System.currentTimeMillis());
						Long misfitTimeStamp = misfitActivityLog.getTimestamp();
						Long misfitLogOffset = misfitActivityLog.getLogTimeOffset() != null ? misfitActivityLog.getLogTimeOffset() : 0l;
						misfitTimeStamp = misfitTimeStamp - misfitLogOffset;
						misfitActivityLog.setTimestamp(misfitTimeStamp);
						baseDAO.saveNew(misfitActivityLog);
					}
					returnDataMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
				} else {
					returnDataMap.put(AppConstants.JsonConstants.REASON.name(), "Parsing error");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("saveMisfitActivityLog service: Exception: "+e.getMessage());
		}

		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("saveMisfitActivityLog service: response: "+response);
		return Response.ok(response).build();
	}

	public static Map<String, String> sendToDeviceViaGCM(SmsReply smsReply, HttpServletRequest request){
		PatientDAO patientDAO = new PatientDAO();
		String macAddress = patientDAO.getMacAddressByPatientIdScheduler(smsReply.getPatientId());
        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
        returnDataMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
		try {
			if(!ObjectUtils.isEmpty(macAddress)){

                  LOG.info("saveSmsReply service: GCM sending started");

				  DeviceDAO deviceDAO = new DeviceDAO();
				  Device device = deviceDAO.getDeviceByMacScheduler(macAddress);
			  
				  if(device != null){			  
					  Log log = new Log();
					  log.setLogType(AppConstants.LOG_TYPE_MEDICATION);
					  
					  Patient patient = new Patient();
					  patient.setPatientId(smsReply.getPatientId());
					  log.setPatient(patient);
					  
					  MiscLog miscLog = new MiscLog();
					  miscLog.setTookMeds(true);
					  
					  log.setMiscLog(miscLog);

					  String dataToSend = JsonUtil.toJson(log);

                      List<Device> devices = new ArrayList<Device>();
                      devices.add(device);
                      String result = PushNotificationManager.pushMessage(devices, AppConstants.GCMKEYS.MEDICATION_LOG_INFO.getValue(), dataToSend, request);
                      LOG.info("sendToDeviceViewGCM: GCM sent results: " + result);

                      /*String result = GCMUtils.sendGCM(deviceTargets, AppConstants.GCMKEYS.MEDICATION_LOG_INFO.getValue(), dataToSend);
                      boolean isGCMSent = GCMUtils.isGCMSent("saveSmsReply service", result, device);
					  LOG.info("saveSmsReply service: GCM sent results: "+dataToSend+", sent Via GCM with result: " + result + ", GCM sent: " + isGCMSent);

					  if(isGCMSent){
                          returnDataMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
                          returnDataMap.put(AppConstants.JsonConstants.DATA.name(), "GCM sent successfully.");
					  }else{
                          returnDataMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
                          returnDataMap.put(AppConstants.JsonConstants.REASON.name(), "Unable to send GCM.");
                      }*/
			    }

                LOG.info("saveSmsReply service: GCM sending ended");
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("saveSmsReply service: GCM sending exception: "+e.getMessage());
		}

        return returnDataMap;
	}

    @GET
    @Path("getSuggestedMealLogs")
    public Response getSuggestedMealLogs(@QueryParam("patientId") Long patientId, @Context HttpServletRequest httpRequest){
        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        try {
            if (patientId != null) {
                LOG.info("getSuggestedMealLogs service: Provided Data: patient Id: " + patientId);
                List<Log> suggestedLogs = new SuggestedLogDAO().getAllSuggestedLogsByPatient(patientId);
                if(suggestedLogs != null && suggestedLogs.size() > 0){
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                    returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), suggestedLogs);
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                    returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), "No Record");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("getSuggestedMealLogs service: Exception: "+e.getMessage());
        }
        String response = JsonUtil.toJsonExcludedNull(returnDataMap);
        LOG.info("getSuggestedMealLogs service: Returning response: "+response);
        return Response.ok(response).build();
    }

	@GET
	@Path("getFavoriteMealLogs")
	public Response getFavoriteMealLogs(@QueryParam("patientId") Long patientId, @Context HttpServletRequest httpRequest){
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		try {
			if (patientId != null) {
				LOG.info("getFavoriteMealLogs service: Provided Data: patient Id: " + patientId);
				List<Log> favoriteLogs = new LogDAO().getAllFavoriteLogsByPatient(patientId);
				if(favoriteLogs != null && favoriteLogs.size() > 0){
					returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
					returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), favoriteLogs);
				}else{
					returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
					returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), "No Record");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("getFavoriteMealLogs service: Exception: "+e.getMessage());
		}
		String response = JsonUtil.toJsonExcludedNull(returnDataMap);
		LOG.info("getFavoriteMealLogs service: Returning response: "+response);
		return Response.ok(response).build();
	}

    //[7/27/2015]: sendMeMoreSuggestedMeals() added by __oz
    @POST
    @Path("sendMeMoreSuggestedMeals")
    public Response sendMeMoreSuggestedMeals(String jsonString, @Context HttpServletRequest httpRequest) {
        jsonString = CommonUtils.extractDataFromRequest(jsonString, httpRequest);
        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(),AppConstants.ServicesConstants.ERROR.name());
        LOG.info("sendMeMoreSuggestedMeals POST service: Provided data logId: "+jsonString);
        JsonElement jelement = new JsonParser().parse(jsonString);
        JsonObject jobject = jelement.getAsJsonObject() ;
        String logId = jobject.get("logId").getAsString();

        LogDAO logDAO = new LogDAO();
        if (ObjectUtils.isEmpty(logId)) {
            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Provided data cannot be null");
        } else {
            Log log = logDAO.getLogById(logId);
            if(log != null) {
                LOG.info("sendMeMoreSuggestedMeals POST service: Log : "+log.toString());
                Patient patient = log.getPatient();
                List<Provider> coaches = patient.getProviders();
                List<Provider> foodCoaches = CommonUtils.getFoodCoachesList(coaches);
                if(foodCoaches != null && foodCoaches.size() > 0){

                }
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
            } else {
                LOG.info("sendMeMoreSuggestedMeals POST service: Log not found. LogId: "+logId);
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Log not found");
            }
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("sendMeMoreSuggestedMeals POST service: Returning response: "+response);
        return Response.ok(response).build();
    }

	//[8/05/2015]: getMisfitLogs() added by __oz
	@GET
	@Path("getMisfitLogs")
	public Response getMisfitLogs(@QueryParam("patientId") Long patientId, @Context HttpServletRequest httpRequest){
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		try {
			if (patientId != null) {
				LOG.info("getMisfitLogs service: Provided Data: patient Id: " + patientId);
				List<MisfitActivityLog> misfitActivityLogs = new MisfitActivityLogDAO().getMisfitLogsByPatientId(patientId);
				if(misfitActivityLogs != null && misfitActivityLogs.size() > 0){
					returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
					returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), misfitActivityLogs);
				}else{
					returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
					returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), "No Record");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("getMisfitLogs service: Exception: "+e.getMessage());
		}
		String response = JsonUtil.toJsonExcludedNull(returnDataMap);
		LOG.info("getMisfitLogs service: Returning response: "+response);
		return Response.ok(response).build();
	}
}
