package com.healthslate.patientapp.ws;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.healthslate.patientapp.model.dao.TechSupportMessageDAO;
import com.healthslate.patientapp.model.entity.TechSupportMessage;
import org.apache.commons.fileupload.FileUpload;
import org.glassfish.jersey.internal.util.Base64;

import com.healthslate.patientapp.model.dao.MessageDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.dao.ProviderDAO;
import com.healthslate.patientapp.model.dto.MessageGcmDTO;
import com.healthslate.patientapp.model.entity.Message;
import com.healthslate.patientapp.model.entity.Provider;
import com.healthslate.patientapp.model.entity.User;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.CommonUtils;
import com.healthslate.patientapp.util.DateUtils;
import com.healthslate.patientapp.util.JsonUtil;
import com.healthslate.patientapp.util.NotifyUtils;
import com.healthslate.patientapp.util.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
* ======= FILE CHANGE HISTORY =======
* [2/4/2015]: Refactored reply service __oz
* [2/19/2015]: getPatientMessagesByDate() added by __ad
* [2/19/2015]: notifyCoachOfPatientMessage() added by __oz
* ===================================
 */

@Path("/message")
public class MessageServices {
	static final Logger LOG = LoggerFactory.getLogger(MessageServices.class);
	@POST
	@Path("/reply")
	public Response patientReply(String jsonString, @Context HttpServletRequest httpRequest) {

		jsonString = CommonUtils.extractDataFromRequest(jsonString, httpRequest);

		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		MessageDAO messageDAO = new MessageDAO();
		ProviderDAO providerDAO = new ProviderDAO();
		TechSupportMessageDAO tsmDAO = new TechSupportMessageDAO();
		LOG.info("patientReply service: provider jsonString: " + jsonString);
		try {
			MessageGcmDTO messageGcmDTO = (MessageGcmDTO) JsonUtil.fromJson(jsonString, MessageGcmDTO.class);
			Provider provider = providerDAO.getJoinedProviderWithProviderId(messageGcmDTO.getProviderId());
			if (messageGcmDTO != null) {
				if (provider.getType().equalsIgnoreCase(AppConstants.PROVIDERTYPE.TECH_SUPPORT.getValue())) {
					TechSupportMessage tsm = new TechSupportMessage();
					tsm.setMessage(messageGcmDTO.getMessageText());
					tsm.setOwner(messageGcmDTO.getOwner());
					tsm.setTimestamp(messageGcmDTO.getTimeStamp());
					tsm.setPatient(new PatientDAO().getPatientById(messageGcmDTO.getPatientId()));
					tsm.setNotifiedTechsupport(getNotifiedTechSupportIds());
					tsm.setReadStatus(false);
					if (provider != null) {
						User user = provider.getUser();
						if (user != null) {
							tsm.setUser(user);
						}
					}
					updateImagePath(messageGcmDTO, tsm, null, provider.getType());

					tsmDAO.save(tsm);
					LOG.info("techSupportMessage service: Saved message in DB: " + tsm.toString());
					NotifyUtils.notifyTechSupportOfPatientMessage(httpRequest, messageGcmDTO.getPatientId());
					returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
					returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), messageGcmDTO);
				} else {
					Message messageDB = new Message();
					messageDB.setReadStatus(false);
					messageDB.setMessage(messageGcmDTO.getMessageText());
					messageDB.setOwner(messageGcmDTO.getOwner());
					messageDB.setTimestamp(messageGcmDTO.getTimeStamp());
					messageDB.setPatient(new PatientDAO().getPatientById(messageGcmDTO.getPatientId()));
					messageDB.setIsError(false);

					//Provider provider = new ProviderDAO().getJoinedProviderWithProviderId(messageGcmDTO.getProviderId());
					if (provider != null) {
						User user = provider.getUser();
						if (user != null) {
							messageDB.setUser(user);
						}
					}
					updateImagePath(messageGcmDTO, null, messageDB, provider.getType());

					messageDAO.save(messageDB);
					LOG.info("patientReply service: Saved message in DB: " + messageDB.toString());

					//[3/9/2015]: notifyCoachOfPatientMessage() modified by __ad
					NotifyUtils.notifyCoachOfPatientMessage(httpRequest, messageGcmDTO.getPatientId(), provider);

					returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
					returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), messageGcmDTO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("patientReply service: Exception: " + e.getMessage());
			returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.ResponseErrorMessages.ERROR_MESSAGE.getValue());
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("patientReply service: Returning response: " + response);
		return Response.ok(response).build();
	}

    //[2/19/2015]: getPatientMessagesByDate() added by __ad
	@GET
	@Path("getPatientMessagesByDate")
	public Response getPatientMessagesByDate(
			@QueryParam("patientId") Long patientId, 
			@QueryParam("date") Long dateFrom) {
		
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LOG.info("getPatientMessagesByDate service: patientId: "+patientId+", dateFrom: "+dateFrom);

		if(patientId != null && dateFrom != null) {
			Long dateTo = System.currentTimeMillis();
			LOG.info("getPatientMessagesByDate service: Fetching patient messages from Date:"+dateFrom+" to Date: "+dateTo+" with patientId: "+patientId);
			List<MessageGcmDTO> messages = new ArrayList<MessageGcmDTO>();
			messages.addAll(new MessageDAO().getAllMessagesByDate(patientId, dateFrom, dateTo));
			messages.addAll(new TechSupportMessageDAO().getAllMessagesByDate(patientId, dateFrom, dateTo));

//			List<MessageGcmDTO> messages = new MessageDAO().getAllMessagesByDate(patientId, dateFrom, dateTo);
			if(messages != null && messages.size() > 0) {
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
				returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), messages);
				returnDataMap.put(AppConstants.SessionKeys.DATE_TO.name(), dateTo);
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No new message");
			}
		} else {
			returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Provided data cannot be null");
		}
		String response = JsonUtil.toJsonExcludedNull(returnDataMap);
		LOG.info("getPatientMessagesByDate service: Returning response: "+response);
		return Response.ok(response).build();
	}

	private void updateImagePath(MessageGcmDTO messageGcmDTO, TechSupportMessage tsm, Message messageDB, String providerType){
		try {
			if (!ObjectUtils.isEmpty(messageGcmDTO.getImageContent())) {
                byte[] btDataFile = new sun.misc.BASE64Decoder().decodeBuffer(messageGcmDTO.getImageContent());

                File fileDir = new File(CommonUtils.getProjectPath() + AppConstants.LOG_IMAGE_FOLDER);
                if (!fileDir.exists()) {
                    fileDir.mkdir();
                }
                String imageName = AppConstants.MSG + "_" + DateUtils.getFormattedDate(System.currentTimeMillis(), DateUtils.DATE_FORMAT_IMAGE) + ".jpg";
                String uploadedFileLocation = fileDir.getAbsolutePath() + "\\" + imageName;
                File of = new File(uploadedFileLocation);
                FileOutputStream osf = new FileOutputStream(of);
                osf.write(btDataFile);
                osf.flush();
                LOG.info("patientReply service: Image saved on server at: " + uploadedFileLocation);
                messageGcmDTO.setImagePath(AppConstants.LOG_IMAGE_FOLDER + imageName);
                if(providerType.equalsIgnoreCase("Tech Support")){
					tsm.setImagePath(AppConstants.LOG_IMAGE_FOLDER + imageName);
				} else {
					messageDB.setImagePath(AppConstants.LOG_IMAGE_FOLDER + imageName);
				}
                messageGcmDTO.setImageContent("");
            }
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getNotifiedTechSupportIds() {
		List<Provider> allTechSupport = new ProviderDAO().getAllTechSupport();
		String notifiedTechSupport = "";
		for (Provider ts : allTechSupport) {
			if(notifiedTechSupport.isEmpty()){
				notifiedTechSupport = ts.getProviderId().toString();
			} else {
				notifiedTechSupport += "," + ts.getProviderId();
			}
		}
		return notifiedTechSupport;
	}
}
