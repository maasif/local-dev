package com.healthslate.patientapp.ws;

import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.ExistingPatientSignUpDTO;
import com.healthslate.patientapp.model.dto.PALitePatientSignupDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.message.internal.DataSourceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.codec.Base64;
import sun.misc.BASE64Decoder;

import javax.activation.DataHandler;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/*
* ======= FILE CHANGE HISTORY =======
* [07/09/2015]: Created by arslan.khan
* [07/18/2015]: Chart Methods copied by irfan.nasim
* [08/12/2015]: Code refactored removed extra code, and following changes made:
*               [1] Meter Association to Patient Entity
*               [2] While creating patient added meterSerialNumber to DB as well by __oz
* ===================================
 */

@Path("/palite")
public class PALiteServices {

    static final Logger LOG = LoggerFactory.getLogger(PALiteServices.class);

    private final String PATIENT_ALREADY_EXISTS = "Member already exist";
    private final String NO_DATA_FOUND_IN_REQ = "No data found in request";
    private final String NO_PATIENT_FOUND_IN_REQ = "No patient found. Please enter valid email";
    private PreferencesDAO preferencesDAO = new PreferencesDAO();
    private PatientDAO patientDAO = new PatientDAO();
    private TargetDAO targetDAO = new TargetDAO();

    @POST
    @Path("/login")
    public Response login(@Context HttpServletRequest httpRequest) {
        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        String credentials = httpRequest.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
        LOG.info("palite/login service: Provided data:" + credentials);

        if (!ObjectUtils.isEmpty(credentials)) {
            credentials = CommonUtils.removeBasicFromString(credentials);
            String[] credsArray = CommonUtils.decode(credentials);
            String userName = credsArray[0];
            String password = credsArray[1];

            try {
                returnDataMap = CommonUtils.authenticateCoach(userName, password);
                if (returnDataMap.get(AppConstants.JsonConstants.STATUS.name()).equals(AppConstants.ACCESS_GRANTED)) {
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                } else {
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                }
            } catch (Exception e) {
                e.printStackTrace();
                LOG.info("palite/login service: Exception: " + e.getMessage());
            }
        }

        String response = JsonUtil.toJsonExcludedNull(returnDataMap);
        LOG.info("palite/login service: Returning response: " + response);
        return Response.ok(response).build();
    }

    @GET
    @Path("/getPatientByMeterSerialNumber")
    public Response getPatientByMeterSerialNumber(@QueryParam("meterSerialNumber") String meterSerialNumber, @Context HttpServletRequest httpRequest) {

        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        LOG.info("palite/getPatientByMeterSerialNumber service: Provided data: " + meterSerialNumber);
        try {
            UserDAO userDAO = new UserDAO();
            Map<String, String> map = userDAO.getPatientByMeterSerialNumber(meterSerialNumber);
            if (map != null) {
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), map);
            } else {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No member associated with this meter.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("palite/getPatientByMeterSerialNumber service: Exception: " + e.getMessage());
        }
        String response = JsonUtil.toJsonExcludedNull(returnDataMap);
        LOG.info("palite/getPatientByMeterSerialNumber service: Returning response: " + response);
        return Response.ok(response).build();
    }

    @POST
    @Path("/patientSignUp")
    public Response createPatient(String signUpPatientString, @Context HttpServletRequest httpRequest) {

        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());

        try {

            signUpPatientString = CommonUtils.extractDataFromRequest(signUpPatientString, httpRequest);
            BASE64Decoder base64Decoder = new BASE64Decoder();
            signUpPatientString = new String(base64Decoder.decodeBuffer(signUpPatientString));

            Patient patient = new Patient();
            User user = new User();
            FacilityDAO facilityDao = new FacilityDAO();
            ProviderDAO providerDao = new ProviderDAO();
            UserDAO userDao = new UserDAO();
            BaseDAO baseDao = new BaseDAO();
            LOG.info("palite/patientSignUp service: Provided data: " + signUpPatientString);
            PALitePatientSignupDTO signUpDto = (PALitePatientSignupDTO) JsonUtil.fromJson(signUpPatientString, PALitePatientSignupDTO.class);
            if (signUpDto != null) {
                if (!userDao.isUserExist(signUpDto.getEmail())) {
                    User requestCoach = userDao.getUserByEmail(signUpDto.getEmailCoach());
                    if (requestCoach != null) {
                        Facility requestedFacility = facilityDao.getFacilityByName(requestCoach.getProvider().getFacility().getName());
                        Facility atozFacility = facilityDao.getAtoZFacility();
                        List<Provider> requestedProvidersList = providerDao.getProvidersByFacilityId(requestedFacility.getFacilityId());
                        List<Provider> atozProvidersList = providerDao.getProvidersByFacilityId(atozFacility.getFacilityId());

                        populateUserForSignUp(user, signUpDto, true);
                        patient.setUser(user);

                        List<Facility> facilities = new ArrayList<Facility>();
                        facilities.add(requestedFacility);
                        //facilities.add(atozFacility);
                        for (Facility facility : facilities) {
                            if (facility.getPatients() != null) { // why null
                                List<Patient> patients = facility.getPatients();
                                patients.add(patient);
                                facility.setPatients(patients);
                            }
                        }
                        patient.setFacilities(facilities);

                        List<Provider> providers = new ArrayList<Provider>();
                        providers.addAll(requestedProvidersList);
                        providers.addAll(atozProvidersList);
                        for (Provider provider : providers) {
                            if (provider.getPatients() != null) {
                                List<Patient> patients = provider.getPatients();
                                patients.add(patient);
                                provider.setPatients(patients);
                            }
                        }

                        patient.setProviders(providers);
                        patient.setTarget(CommonUtils.getDefaultTarget(patient));
                        patient.setUuid(CommonUtils.generateUUID());
                        patient.setIsThreeDayEmailSent(false);
                        patient.setMeterSerialNumber(signUpDto.getMeterSerialNumber());
                        patient.setDeviceMacAddress(signUpDto.getDeviceMacAddress());
                        user.setPatient(patient);

                        UserRole userRole = new UserRole();
                        userRole.setAuthority(AppConstants.Roles.ROLE_PATIENT.name());
                        userRole.setUser(user);
                        boolean isSaved = baseDao.save(user);

                        saveDevice(signUpDto.getDeviceMacAddress());

                        if (isSaved) {
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());

                            Map<String, Object> map = new HashMap<String, Object>();
                            map.put("patientId", user.getPatient().getPatientId());
                            map.put("patientName", user.getFirstName() + " " + user.getLastName());
                            map.put("patientEmail", user.getEmail());
                            map.put("coachEmail", signUpDto.getEmailCoach());
                            returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), map);

                            LOG.info("palite/patientSignUp service: User Saved: " + user.toString());
                        } else {
                            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SIGNUP);
                            LOG.info("palite/patientSignUp service: User not saved: " + user.toString());
                        }
                    } else {
                        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_FIND_COACH);
                        LOG.info("palite/patientSignUp service: Coach not found: " + user.toString());
                    }
                } else {
                    LOG.info("palite/patientSignUp service: PATIENT_ALREADY_EXISTS");
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), PATIENT_ALREADY_EXISTS);
                }
            } else {
                LOG.info("palite/patientSignUp service: Provided data is null");
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), NO_DATA_FOUND_IN_REQ);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String response = JsonUtil.toJsonExcludedNull(returnDataMap);
        LOG.info("palite/patientSignUp service: Returning response: " + response);
        return Response.ok(response).build();
    }

    @SuppressWarnings({"unchecked", "unused"})
    @POST
    @Path("/saveLog")
    public Response saveLog(String logJsonString, @Context HttpServletRequest httpRequest) {

        logJsonString = CommonUtils.extractDataFromRequest(logJsonString, httpRequest);

        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
        Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        GlucoseLogTypeDAO glucoseLogTypeDAO = new GlucoseLogTypeDAO();
        MiscLogDAO miscLogDAO = new MiscLogDAO();
        MessageDAO messageDAO = new MessageDAO();
        PatientDAO patientDAO = new PatientDAO();
        DeviceDAO deviceDAO = new DeviceDAO();
        LogDAO logDAO = new LogDAO();
        GlucoseLogDAO glucoseLogDAO = new GlucoseLogDAO();
        LOG.info("palite/saveLog service: Provided logJsonString: " + logJsonString);
        Log log = (Log) JsonUtil.fromJson(logJsonString, Log.class);
        Log logFromServer = logDAO.getLogById(log.getLogId());
        SocialGroupServices services = new SocialGroupServices();

        if (log != null) {
            String mac = httpRequest.getHeader(AppConstants.UserInfo.MAC_ADDRESS.getValue());
            LOG.info("palite/saveLog service: device mac address: " + mac);
            Device device = deviceDAO.getDeviceByMac(mac);
            if (device != null) {
                if (log.getPatient() != null && log.getPatient().getPatientId() != null) {
                    Patient patient = patientDAO.getPatientById(log.getPatient().getPatientId());
                    if (patient != null) {
                        LOG.info("palite/saveLog service: get patient from log, Patient: " + patient.toString());
                        System.out.println("<LOG_CHECK> Patient id: " + patient.getPatientId());
                        log.setPatient(patient);
                        LOG.info("palite/saveLog service: logType: " + log.getLogType());

                        GlucoseLog glucoseLogFromRequest = log.getGlucoseLog();

                        try {
                            LOG.info("palite/saveLog service: Deleting glucose log from DB for logId: " + log.getLogId());
                            glucoseLogDAO.removeglucoseLog(log.getLogId());
                            LOG.info("palite/saveLog service: Removing duplicate glucose log from DB for logId: " + log.getLogId());
                            glucoseLogDAO.removeGlucoseLogByPatientAndMeterName(glucoseLogFromRequest.getTimestamp(),  glucoseLogFromRequest.getMeterSerialNumber(), log.getPatient().getPatientId());
                        } catch (Exception e) {
                            e.printStackTrace();
                            LOG.info("palite/saveLog service: Removing glucose log Exception: " + e.getMessage());
                        }

                        Target target = targetDAO.getTargetByPatientId(patient.getPatientId());
                        Map<String, String> targetsMealTime = CommonUtils.getMealTargetsWithKeysAndTime(target);
                        Map<String, Long> targetsMealTimeMilli = DateUtils.getCurrentDateInMilliMap(targetsMealTime);

                        glucoseLogFromRequest.setMealName(CommonUtils.getMealTypeByTime(targetsMealTimeMilli, glucoseLogFromRequest.getTimestamp()));
                        glucoseLogFromRequest.setLog(log);

                        if (glucoseLogFromRequest.getGlucoseLogType() != null) {
                            GlucoseLogType type = glucoseLogTypeDAO.getGlucoseLogTypeByDescription(glucoseLogFromRequest.getGlucoseLogType().getDescription());
                            glucoseLogFromRequest.setGlucoseLogType(type);
                        }

                        LOG.info("palite/saveLog service: Removing logTimeOffSet from LogTime: offset value: " + log.getLogTimeOffset());
                        Long time = log.getLogTime();
                        time = time - log.getLogTimeOffset();
                        log.setLogTime(time);
                        log.setDeviceMacAddress(mac);
                        log.setSyncStatus(AppConstants.LogSyncStatus.UNSYNCHRONIZED.name());
                        log.setLastModified(System.currentTimeMillis());
                        log.setIsRemoved(false);
                        log.setIsSuggested(false);

                        log.setUploadTime(System.currentTimeMillis());

                        boolean isSaved = logDAO.save(log);
                        LOG.info("palite/saveLog service: Saved log on server: status: " + isSaved + ", log: " + log.toString());
                        if (isSaved) {
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                            returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), String.valueOf(log.getLogId()));
                        } else {
                            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SAVE_LOG);
                        }
                    } else {
                        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No patient found");
                    }
                } else {
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No patient found");
                }
            } else {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Device not registered");
            }
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("palite/saveLog service: Returning response: " + response);
        return Response.ok(response).build();
    }

    @POST
    @Path("/existingPatientSignUp")
    public Response existingPatientSignUp(String existingPatientSignUpString, @Context HttpServletRequest httpRequest) {

        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());

        try {
            existingPatientSignUpString = CommonUtils.extractDataFromRequest(existingPatientSignUpString, httpRequest);
            BASE64Decoder base64Decoder = new BASE64Decoder();
            existingPatientSignUpString = new String(base64Decoder.decodeBuffer(existingPatientSignUpString));

            UserDAO userDao = new UserDAO();
            LOG.info("palite/existingPatientSignUp service: Provided data: " + existingPatientSignUpString);
            ExistingPatientSignUpDTO signUpDto = (ExistingPatientSignUpDTO) JsonUtil.fromJson(existingPatientSignUpString, ExistingPatientSignUpDTO.class);
            if (signUpDto != null) {
                if (userDao.isUserExist(signUpDto.getEmail())) {
                    User user = userDao.getUserByEmail(signUpDto.getEmail());
                    if (user != null) {
                        Map<String, Object> map = new HashMap<String, Object>();
                        map.put("patientId", user.getPatient().getPatientId().toString());
                        map.put("patientName", user.getFirstName() + " " + user.getLastName());
                        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                        returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), map);
                    }
                } else {
                    LOG.info("palite/existingPatientSignUp service: Patient not exist with email");
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), NO_PATIENT_FOUND_IN_REQ);
                }
            } else {
                LOG.info("palite/existingPatientSignUp service: Provided data is null");
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), NO_DATA_FOUND_IN_REQ);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String response = JsonUtil.toJsonExcludedNull(returnDataMap);
        LOG.info("palite/existingPatientSignUp service: Returning response: " + response);
        return Response.ok(response).build();
    }

    @GET
    @Path("/getDailyMealGlucoseChartData")
    public Response getDailyMealGlucoseChartData(
            @QueryParam("patientId") Long patientId,
            @QueryParam("startDate") Long startDate,
            @QueryParam("endDate") Long endDate) {

        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        LOG.info("palite/getDailyMealGlucoseChartData service: Provided data: PatientId: " + patientId + ", startDate: " + startDate + ", endDate: " + endDate);
        LogDAO logDAO = new LogDAO();
        if (patientId != null && startDate != null && endDate != null) {

            startDate = DateUtils.getStartOfDayTime(startDate);
            endDate = DateUtils.getEndOfDayTime(endDate);

            Patient patient = patientDAO.getPatientById(patientId);
            long facilityTimezoneOffset =  patient.getFacilities().get(0).getTimezoneOffsetMillis();

            List<Object> glucoseLevelData = logDAO.getGlucoseValuesPerDayByPatientId(patientId, startDate, endDate);
            List<Object> carbsLevelData = logDAO.getCarbsValuesPerDayByPatientId(patientId, startDate, endDate);
            if (glucoseLevelData == null && carbsLevelData == null) {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");
            } else {
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
                List<Object> glucoseLevelDataList = getGlucoseLevelDataList(glucoseLevelData, facilityTimezoneOffset);
                List<Object> carbsLevelDataList = getCarbsLevelDataList(carbsLevelData);

                dataMap.put(AppConstants.ChartServiceConstants.GLUCOSE_LEVEL_DATA.getValue(), glucoseLevelDataList);
                dataMap.put(AppConstants.ChartServiceConstants.CARBS_LEVEL_DATA.getValue(), carbsLevelDataList);
                returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), dataMap);
            }
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("palite/getDailyMealGlucoseChartData service: Returning response: " + response);
        return Response.ok(response).build();
    }

    @GET
    @Path("/getGlucoseChartData")
    public Response getGlucoseChartData(
            @QueryParam("patientId") Long patientId,
            @QueryParam("startDate") Long startDate,
            @QueryParam("endDate") Long endDate) {

        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        LOG.info("palite/getGlucoseChartData service: Provided data: PatientId: " + patientId + ", startDate: " + startDate + ", endDate: " + endDate);
        //String startDateString = DateUtils.getFormattedDate(startDate, DateUtils.DATE_FORMAT_DASHED);
        //	String endDateString = DateUtils.getFormattedDate(endDate, DateUtils.DATE_FORMAT_DASHED);

        LogDAO logDAO = new LogDAO();
        if (patientId != null && startDate != null && endDate != null) {
            Object glucoseData = logDAO.getGlucoseSummaryByPatientId(patientId, startDate, endDate);
            if (glucoseData == null) {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");
            } else {
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                Map<String, Object> glucoseDataMap = getGlucoseMap(glucoseData);
                returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), glucoseDataMap);
            }
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("palite/getGlucoseChartData service: Returning response: " + response);
        return Response.ok(response).build();
    }

    @POST
    @Path("/forgotPassword")
    public Response forgotPassword(String emailString, @Context HttpServletRequest httpRequest) {

        emailString = CommonUtils.extractDataFromRequest(emailString, httpRequest);

        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        UserDAO userDao = new UserDAO();
        LOG.info("palite/forgotPassword service: Provided Data: emailString: "+emailString);
        try {
            if (userDao.isUserExist(emailString)) {
                User user = userDao.getUserByEmail(emailString);
                if(user != null) {
                    boolean isSentEmail = EmailUtils.sendResetPasswordEmail(user, httpRequest, true);
                    if (isSentEmail) {
                        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                        returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), "An email has been sent to your email address.");
                    } else {
                        returnDataMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
                        returnDataMap.put(AppConstants.JsonConstants.REASON.name(), "Unable to send email at the moment.");
                    }
                } else {
                    returnDataMap.put(AppConstants.JsonConstants.REASON.name(), "Your sign up process is not completed.");
                }
            } else {
                returnDataMap.put(AppConstants.JsonConstants.REASON.name(), "Email address is not valid.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("palite/forgotPassword service: Exception: "+e.getMessage());
            returnDataMap.put(AppConstants.JsonConstants.REASON.name(), AppConstants.UBABLE_TO_SIGNUP);
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("palite/forgotPassword service: Returning response: "+response);
        return Response.ok(response).build();
    }

    @POST
    @Path("/saveFeedback")
    public Response saveFeedback(String feedbackJsonString, @Context HttpServletRequest httpRequest) {

        feedbackJsonString = CommonUtils.extractDataFromRequest(feedbackJsonString, httpRequest);

        Map<String, String> prefMap = preferencesDAO.map();
        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        LOG.info("palite/saveFeedback service: Provided Data: feedbackJsonString: "+feedbackJsonString);
        try {
            Feedback feedback = (Feedback) JsonUtil.fromJson(feedbackJsonString, Feedback.class);
            if(feedback != null) {
                feedback.setTimestamp(System.currentTimeMillis());

                Provider provider = new ProviderDAO().getProviderById(feedback.getProvider().getProviderId());
                feedback.setProvider(provider);
                User user = provider.getUser();
                EmailToken emailToken = new EmailToken();
                emailToken.setUsername(user.getEmail());

                String token = user.getEmail() + System.currentTimeMillis();
                token = new String(Base64.encode(token.getBytes()));
                emailToken.setToken(token);

                emailToken.setDateCreated(new Date());
                new EmailTokenDAO().saveEmailToken(emailToken);
                LOG.info("palite/saveFeedback service: Saved Email Token in db: "+emailToken.toString());
                String url = ObjectUtils.getApplicationUrl(httpRequest)+"/app/admin/Feedback.action";
                String emailText = CommunicationUtils.getEmailTextForFeedback(url);
                String emailStrings = ObjectUtils.nullSafe(prefMap.get(AppConstants.PreferencesNames.FEED_BACK_EMAIL_ID.getValue()));
                String[] emails = emailStrings.split(",");
                for (String email : emails) {
                    boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
                            email,
                            prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                            AppConstants.EmailSubjects.MEMBER_FEEDBCK.getValue());
                    LOG.info("palite/saveFeedback service: Sent email to: "+email+", with emailText: "+emailText+", with sent status: "+isSentEmail);
                }
                boolean isSave = new BaseDAO().save(feedback);
                LOG.info("palite/saveFeedback service: Saved Feedback in db: "+feedback.toString());
                if(isSave) {
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                } else {
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SAVE_FEEDBACK);
                }
            } else {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SAVE_FEEDBACK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("palite/saveFeedback service: Exception: "+e.getMessage());
            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SAVE_FEEDBACK);
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("palite/saveFeedback service: Returning response: "+response);
        return Response.ok(response).build();
    }

    @GET
    @Path("/getGlucoseDetailChartData")
    public Response getGlucoseDetailChartData(
            @QueryParam("patientId") Long patientId,
            @QueryParam("startDate") Long startDate,
            @QueryParam("endDate") Long endDate) {

        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        LogDAO logDAO = new LogDAO();
        LOG.info("palite/getGlucoseDetailChartData service: Provided data: PatientId: "+patientId+", startDate: "+startDate+", endDate: "+endDate);
        if(patientId != null && startDate != null && endDate != null) {
            List<Object> glucoseLevelData = logDAO.getGlucoseValuesPerDayByPatientId(patientId, startDate, endDate);
            if(glucoseLevelData == null) {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");
            } else {
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                List<Object> glucoseLevelDataList = getGlucoseLevelDataList(glucoseLevelData);
                returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), glucoseLevelDataList);
            }
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("palite/getGlucoseDetailChartData service: Returning response: "+response);
        return Response.ok(response).build();
    }

    @GET
    @Path("/getGlucoseChartDataCombined")
    public Response getGlucoseChartDataCombined(
            @QueryParam("patientId") Long patientId,
            @QueryParam("startDate") Long startDate,
            @QueryParam("endDate") Long endDate) {

        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        LOG.info("palite/getGlucoseChartDataCombined service: Provided data: PatientId: "+patientId+", startDate: "+startDate+", endDate: "+endDate);

        LogDAO logDAO = new LogDAO();
        if(patientId != null && startDate != null && endDate != null) {
            Object glucoseDataBeforeMeal = logDAO.getGlucoseSummaryBeforeMealByPatientId(patientId, startDate, endDate);
            Object glucoseDataAfterMeal = logDAO.getGlucoseSummaryAfterMealByPatientId(patientId, startDate, endDate);
            if(glucoseDataBeforeMeal == null && glucoseDataAfterMeal == null) {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No record found");
            } else {
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                Map<String, Object> glucoseDataBeforeMealMap = getGlucoseMap(glucoseDataBeforeMeal);
                Map<String, Object> glucoseDataAfterMealMap = getGlucoseMap(glucoseDataAfterMeal);
                returnDataMap.put(AppConstants.ChartServiceConstants.BEFORE_MEAL.name(), glucoseDataBeforeMealMap);
                returnDataMap.put(AppConstants.ChartServiceConstants.AFTER_MEAL.name(), glucoseDataAfterMealMap);
            }
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("palite/getGlucoseChartDataCombined service: Returning response: "+response);
        return Response.ok(response).build();
    }

    @POST
    @Path("/emailDebugFile")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response emailDebugFile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("toEmail") String toEmail,
            @FormDataParam("ccEmail") String ccEmail,
            @FormDataParam("emailBody") String emailBody,
            @Context HttpServletRequest httpRequest) {

        Map<String, String> prefMap = preferencesDAO.map();
        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());

        try {
            String fileName =  "PALite" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + "TEXT";
            // can be replaced with emailBody
            String emailText = ""; //CommunicationUtils.getEmailTextForDebugFile();

            // Converting CC Emails String list to Internet Address Array
            InternetAddress[] ccEmails = CommonUtils.stringToInternetAddressArray(ccEmail);

            // Creating inputStream to ByteArray
            byte[] bytes = IOUtils.toByteArray(uploadedInputStream);
            InputStream myInputStream = new ByteArrayInputStream(bytes);

            // Body Part of the Email, First Body Part Attachement File
            MimeBodyPart bp = new MimeBodyPart();
            List<MimeBodyPart> bodyParts = new ArrayList<MimeBodyPart>();
            ByteArrayDataSource ds = null;
            ds = new ByteArrayDataSource(myInputStream, "text/plain");
            bp.setDataHandler(new DataHandler(ds));
            bp.setFileName(fileName);

            // Adding body part into BodyParts List
            bodyParts.add(bp);

            // 2nd Body part Plain Text in email
            MimeBodyPart mbp = new MimeBodyPart();
            mbp.setContent(emailBody, "text/html");

            // Adding body part into BodyParts List
            bodyParts.add(mbp);

            // Sending Email
            boolean isSentEmail = EmailUtils.sendEmailWithAttachment(emailText, toEmail,
                    prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                    prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                    AppConstants.EmailSubjects.DEBUG_EMAIL.getValue(),
                    fileName,
                    ccEmails,
                    bodyParts);

            LOG.info("palite/sendDebugEmail service: Sent email to: "+toEmail+", with sent status: "+isSentEmail);

            if(isSentEmail) {
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
            } else {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SEND_EMAIL);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("upload Service: Returning with response: "+response);
        return Response.ok(response).build();
    }

    //------------------------------------------ utility methods --------------------------------------

    private void saveDevice(String macAddress) {
        DeviceDAO deviceDao = new DeviceDAO();

        if (!ObjectUtils.isEmpty(macAddress)) {

            Device device = deviceDao.getDeviceByMac(macAddress);
            if (device == null) {
                device = new Device();
                device.setDeviceMacAddress(macAddress);
            }

            device.setDeviceType(AppConstants.DeviceTypes.ANDROID.getValue());
            deviceDao.save(device);
        }
    }

    private List<Object> getGlucoseLevelDataList(List<Object> glucoseLevelData) {
        List<Object> finalGlucoseLevelData = new ArrayList<Object>();
        for (Object object : glucoseLevelData) {
            Object[] objArray = (Object[]) object;
            if (objArray != null && objArray.length > 0) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put(AppConstants.ChartServiceConstants.LEVEL.getValue(), objArray[1] == null ? 0 : Integer.parseInt(objArray[1].toString()));
                map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), objArray[2] == null ? 0 : objArray[2].toString());
                map.put(AppConstants.ChartServiceConstants.ID.getValue(), objArray[3] == null ? 0 : objArray[3]);
                map.put(AppConstants.ChartServiceConstants.GLUCOSE_TIME.getValue(), objArray[4] == null ? 0 : ObjectUtils.nullSafe(objArray[4]));
                finalGlucoseLevelData.add(map);
            }
        }
        return finalGlucoseLevelData;
    }

    private List<Object> getGlucoseLevelDataList(List<Object> glucoseLevelData, Long facilityTimezoneOffset) {
        List<Object> finalGlucoseLevelData = new ArrayList<Object>();
        for (Object object : glucoseLevelData) {
            Object[] objArray = (Object[]) object;
            if (objArray != null && objArray.length > 0) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put(AppConstants.ChartServiceConstants.LEVEL.getValue(), objArray[1] == null ? 0 : Integer.parseInt(objArray[1].toString()));
                map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), objArray[2] == null ? 0 : Long.parseLong(objArray[2].toString()) + facilityTimezoneOffset);
                map.put(AppConstants.ChartServiceConstants.ID.getValue(), objArray[3] == null ? 0 : objArray[3]);
                map.put(AppConstants.ChartServiceConstants.GLUCOSE_TIME.getValue(), objArray[4] == null ? 0 : ObjectUtils.nullSafe(objArray[4]));
                finalGlucoseLevelData.add(map);
            }
        }
        return finalGlucoseLevelData;
    }

    private List<Object> getCarbsLevelDataList(List<Object> carbsLevelData) {
        List<Object> finalCarbsLevelData = new ArrayList<Object>();
        for (Object object : carbsLevelData) {
            Object[] objArray = (Object[]) object;
            if (objArray != null && objArray.length > 0) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put(AppConstants.ChartServiceConstants.CARBS.getValue(), objArray[1] == null ? 0 : Math.round(Double.parseDouble(objArray[1].toString())));
                map.put(AppConstants.ChartServiceConstants.TYPE.getValue(), objArray[2] == null ? 0 : objArray[2]);
                map.put(AppConstants.ChartServiceConstants.TIMESTAMP.getValue(), objArray[3] == null ? 0 : objArray[3]);
                map.put(AppConstants.ChartServiceConstants.HAS_MISSING_FOOD.getValue(), objArray[4] == null ? 0 : objArray[4]);
                map.put(AppConstants.ChartServiceConstants.ID.getValue(), objArray[5] == null ? 0 : objArray[5]);
                finalCarbsLevelData.add(map);
            }
        }
        return finalCarbsLevelData;
    }

    private Map<String, Object> getGlucoseMap(Object glucoseData) {
        Map<String, Object> glucoseMap = new LinkedHashMap<String, Object>();
        Object[] objArray = (Object[]) glucoseData;
        if (objArray != null && objArray.length > 0) {
            glucoseMap.put(AppConstants.ChartServiceConstants.AVG.getValue(), objArray[0] == null ? 0 : Math.round(Double.parseDouble(objArray[0].toString())));
            glucoseMap.put(AppConstants.ChartServiceConstants.UNDER.getValue(), objArray[1] == null ? 0 : Math.round(Double.parseDouble(objArray[1].toString())));
            glucoseMap.put(AppConstants.ChartServiceConstants.IN_TARGET.getValue(), objArray[2] == null ? 0 : Math.round(Double.parseDouble(objArray[2].toString())));
            glucoseMap.put(AppConstants.ChartServiceConstants.OVER.getValue(), objArray[3] == null ? 0 : Math.round(Double.parseDouble(objArray[3].toString())));
            glucoseMap.put(AppConstants.ChartServiceConstants.TOTAL.getValue(), objArray[4] == null ? 0 : Math.round(Double.parseDouble(objArray[4].toString())));
        }
        return glucoseMap;
    }

    private void populateUserForSignUp(User user, PALitePatientSignupDTO signUpDto, boolean shouldOverride) {
        String firstName = ObjectUtils.nullSafe(signUpDto.getFirstName()).trim();
        String lasName = ObjectUtils.nullSafe(signUpDto.getLastName()).trim();
        user.setFirstName(firstName);
        user.setLastName(lasName);
        user.setDisplayName(CommonUtils.formatPatientName(firstName, lasName, ObjectUtils.nullSafe(user.getDisplayName())));
        user.setEmail(signUpDto.getEmail());
        user.setUserType(AppConstants.PATIENT);
        user.setIsRegistrationCompleted(false);
        Date date = new Date();
        user.setRegistrationDate(date);
        user.setPasswordModifiedTime(date.getTime());
    }
}