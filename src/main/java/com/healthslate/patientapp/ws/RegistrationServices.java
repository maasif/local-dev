package com.healthslate.patientapp.ws;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.healthslate.patientapp.model.dao.DeviceDAO;
import com.healthslate.patientapp.model.entity.Device;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.CommonUtils;
import com.healthslate.patientapp.util.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/register")
public class RegistrationServices {
	static final Logger LOG = LoggerFactory.getLogger(RegistrationServices.class);
	@POST
	@Path("saveRegistration")
	public Response saveRegistration(String registrationId , @Context HttpServletRequest httpRequest){

		registrationId = CommonUtils.extractDataFromRequest(registrationId, httpRequest);

		DeviceDAO deviceDao = new DeviceDAO();
		Map<String, String> resultMap = new LinkedHashMap<String, String>();
		resultMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		String mac = httpRequest.getHeader(AppConstants.UserInfo.MAC_ADDRESS.getValue());
		LOG.info("saveRegistration Service: provided RegistrationId: "+registrationId+", mac address: "+mac);

		if(!registrationId.isEmpty() && !mac.isEmpty()){
			Device device = deviceDao.getDeviceByMac(mac);
			if(device == null || (device != null && (device.getDeviceMacAddress().isEmpty() || device.getRegistrationId().isEmpty()))){
				device = new Device();
				if(device != null && device.getDeviceId() != null){
					device.setDeviceId(device.getDeviceId());
				}
				if(device == null || (device != null && device.getDeviceMacAddress() == null)){
					device.setDeviceMacAddress(mac);
				}
				if(device == null || (device != null && device.getRegistrationId() == null)){
					device.setRegistrationId(registrationId);
				}
				deviceDao.save(device);
				LOG.info("saveRegistration Service: Saved device: "+device.toString());
			}
			resultMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
		}
		String response = JsonUtil.toJson(resultMap);
		LOG.info("saveRegistration Service: Returning response: "+response);
		return Response.ok().build();
	}
}
