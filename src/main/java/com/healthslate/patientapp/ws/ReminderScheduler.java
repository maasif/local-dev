package com.healthslate.patientapp.ws;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import com.healthslate.patientapp.schedulers.MedicationReminderScheduleJob;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.RemindersUtil;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class ReminderScheduler {
	public final static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	
	@SuppressWarnings("unused")
	public static void schedulerMedicationReminders() {

        /*try {
            JobDetail jd = JobBuilder.newJob(MedicationReminderScheduleJob.class)
                    .withIdentity("1 minute reminder for medication").build();

            Trigger trigger = TriggerBuilder.newTrigger().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(1)
                    .repeatForever())
                    .build();
            SchedulerFactory schFactory = new StdSchedulerFactory();

            Scheduler sch = schFactory.getScheduler();
            sch.start();
            sch.scheduleJob(jd, trigger);

        } catch(SchedulerException e ) {
            e.printStackTrace();
        }*/

		/*final Runnable medReminderThread = new Runnable() {
			public synchronized void run() {
				try {
					RemindersUtil.sendMedicationSmsReminders();
				} catch (Exception e) {
					e.printStackTrace();	
				}	
			}
		};
		@SuppressWarnings("rawtypes")
		final ScheduledFuture processHandler = scheduler.scheduleAtFixedRate(medReminderThread, 1, 1, TimeUnit.MINUTES);*/
	}

	public static void schedulerGoalAssessmentReminders() {
		final Runnable goalRemindersThread = new Runnable() {
			public synchronized void run() {
				try {
					RemindersUtil.sendMessagesForGoalAssessment();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
        @SuppressWarnings("rawtypes")
		final ScheduledFuture processHandler = scheduler.scheduleAtFixedRate(goalRemindersThread, 1, 15, TimeUnit.MINUTES);
	}

    public static void schedulerOneToOneSessionReminders24Hours() {
        final Runnable oneToOneSessionReminderThread = new Runnable() {
            public synchronized void run() {
                try {
                    RemindersUtil.sendOneToOneSessionReminders(AppConstants.PreferencesNames.SECOND_ONE_TO_ONE_SESSION.getValue(), "Every 20 Minutes");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        @SuppressWarnings("rawtypes")
        final ScheduledFuture processHandler = scheduler.scheduleAtFixedRate(oneToOneSessionReminderThread, 5, 20, TimeUnit.MINUTES);
    }

    public static void schedulerOneToOneSessionRemindersToday() {
        final Runnable oneToOneSessionReminderThread = new Runnable() {
            public synchronized void run() {
                try {
                    RemindersUtil.sendOneToOneSessionReminders(AppConstants.PreferencesNames.FIRST_ONE_TO_ONE_SESSION.getValue(), "Every 5 Minutes");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        @SuppressWarnings("rawtypes")
        final ScheduledFuture processHandler = scheduler.scheduleAtFixedRate(oneToOneSessionReminderThread, 1, 5, TimeUnit.MINUTES);
    }
}