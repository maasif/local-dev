package com.healthslate.patientapp.ws;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import org.apache.struts2.json.JSONUtil;
import org.hibernate.HibernateException;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.healthslate.patientapp.model.dao.LogDAO;
import com.healthslate.patientapp.model.dao.MessageDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.dao.ProviderDAO;
import com.healthslate.patientapp.model.dao.ReportErrorDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/error")
public class ReportErrorService {
	static final Logger LOG = LoggerFactory.getLogger(ReportErrorService.class);
	@POST
	@Path("/reportError")
    public Response reportError(String jsonString, @Context HttpServletRequest httpRequest) {

		jsonString = CommonUtils.extractDataFromRequest(jsonString, httpRequest);

		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		
		LogDAO LogDAO = new LogDAO();
		ReportErrorDAO reporterrorDAO =new ReportErrorDAO();
		ReportError reporterror =new ReportError();
		LOG.info("reportError Service: provided data: "+jsonString);
		if(jsonString != null && !jsonString.isEmpty()){
			JsonElement jelement = new JsonParser().parse(jsonString);
			JsonObject jobject = jelement.getAsJsonObject();
			String logId = jobject.get(AppConstants.GCMKEYS.LOG_ID.getValue()).getAsString();
			if(logId != null && !logId.isEmpty() ){
				Log log = LogDAO.getLogById(logId);
				if(log != null){
					Date date = new Date();
					reporterror.setTimestamp(date.getTime());
					reporterror.setLog(log);
					reporterror.setDescription(jobject.get(AppConstants.DESCRIPTION).getAsString());

					try{
						reporterrorDAO.save(reporterror);
						Patient patient = log.getPatient();

						LOG.info("reportError Service: Saved Report Error: "+reporterror.toString());

						//[9/1/2015]: click here to view added by _oz
						String urlName = ObjectUtils.getApplicationUrl(httpRequest);
						urlName += "/app/educator/fetchLogDetail.action?logId="+log.getLogId()+"&patientId="+patient.getPatientId()+"&redirectToPage=inAppMessages&isFromError=no";
						String clickHere = "<br/>Click <a href='"+urlName+"'> here </a> to view the error.";

						//[3/3/2015]: save in messages too and notify added by __ad
						Message messageDB = new Message();
						messageDB.setReadStatus(false);
						String description = log.getFoodLogSummary().getMealName() + "<br/>" + jobject.get(AppConstants.DESCRIPTION).getAsString();
						messageDB.setMessage(description + clickHere);
						messageDB.setOwner(AppConstants.PATIENT);
						messageDB.setTimestamp(System.currentTimeMillis());
						messageDB.setPatient(patient);
						messageDB.setIsError(true);

						Provider provider = new ProviderDAO().getProviderByUser(log.getUserId());
						if(provider != null){
							User user = provider.getUser();
							if(user != null){
								messageDB.setUser(user);
							}
						}

						if(log.getLogType().equals(AppConstants.LOG_TYPE_MEAL) && log.getFoodLogSummary().getHasImage()) {
							messageDB.setImagePath(AppConstants.LOG_IMAGE_FOLDER +log.getFoodLogSummary().getFoodImage().getImageName());
						}
						messageDB.setFoodLogSummary(log.getFoodLogSummary());
						new MessageDAO().save(messageDB);
						LOG.info("reportError Service: Saved Message: "+messageDB.toString());
						NotifyUtils.notifyCoachOfMealError(httpRequest, log, provider);
						returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
					} catch (HibernateException ex) {
						LOG.info("reportError Service: hibernate exception:"+ ex.getMessage());
						returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
						returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Error in saving record");
					}
				} else{
					LOG.info("reportError Service: Log not found in db for logId: "+logId);
					returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
					returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No Log Found");
				}
			} else{
				LOG.info("reportError Service: logId is null");
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Incorrect Log Id");
			}
		} else{
			LOG.info("reportError Service: Invalid JSON");
			returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Invalid JSON");
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("reportError Service: Returning response: "+response);
		return Response.ok(response).build();
	}
}
