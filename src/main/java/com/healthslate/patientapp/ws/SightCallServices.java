package com.healthslate.patientapp.ws;

import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.google.gson.Gson;
import com.healthslate.patientapp.model.dao.PreferencesDAO;
import com.healthslate.patientapp.model.entity.UserInfo;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.JsonUtil;
import com.healthslate.patientapp.util.ObjectUtils;
import com.weemo.auth.WeemoException;
import com.weemo.auth.WeemoServerAuth;
import org.apache.struts2.json.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/sightcall")
public class SightCallServices {
	static final Logger LOG = LoggerFactory.getLogger(SightCallServices.class);
    // Enter below your path to certificate files, to RTCC Auth Server and your credentials
    String CA_FILE        = "";
    String AUTH_URL       = "https://auth.rtccloud.net/auth/";
    String P12_FILE       = "";
	String P12_PASS       = "XnyexbUF";
	String CLIENT_ID      = "3e90ab3913df781c463db84b96dfec";
	String CLIENT_SECRET  = "d4a4228755f1f819c01532247be1b0";

	// Initialize WeemoServerAuth Object
	WeemoServerAuth auth = null;

    private PreferencesDAO preferencesDAO = new PreferencesDAO();
	@GET
	@Path("getToken")
	public Response getToken(@Context HttpServletRequest httpRequest, @QueryParam("uid") String uid){
        CA_FILE = httpRequest.getServletContext().getRealPath("/WEB-INF/authCA.crt");
        P12_FILE = httpRequest.getServletContext().getRealPath("/WEB-INF/client.p12");
        P12_PASS = preferencesDAO.getPreference(AppConstants.PreferencesNames.SIGHTCALL_P12_PASS.getValue());
        CLIENT_ID = preferencesDAO.getPreference(AppConstants.PreferencesNames.SIGHTCALL_CLIENT_ID.getValue());
        CLIENT_SECRET = preferencesDAO.getPreference(AppConstants.PreferencesNames.SIGHTCALL_CLIENT_SECRET.getValue());
		LOG.info("getToken Service: given UID: "+uid+", P12_PASS: "+P12_PASS+", CLIENT_ID: "+CLIENT_ID+", CLIENT_SECRET: "+CLIENT_SECRET);
		try {
			auth = new WeemoServerAuth(AUTH_URL, CA_FILE, P12_FILE, P12_PASS, CLIENT_ID, CLIENT_SECRET);
			LOG.info("getToken Service: Authenticated through WeemoServer successfully");
		} catch (WeemoException e) {
			e.printStackTrace();
			LOG.info("getToken Service: Authenticated through WeemoServer Failed: Returning exception: "+e.getMessage());
			return Response.ok(e.getMessage()).build();
		}

		// The JSON response we will send back
		String response;

		// Extract Query and Post parameters and place in a single dict called 'params'
		String domain = "diabetes.healthslate.com";         // group of users
		String profile = "standard";               // premium profile

		if (uid == null) {
			System.out.println("No UID found in request");
			response = "{ \"error\" : \"UID\", \"error_description\" : \"No UID found in request\" }";
		} else {

			try {

				String authToken = auth.getAuthToken(uid, domain, profile);
				System.out.println("AuthToken:" + authToken);
				response = authToken;
			}catch (WeemoException e) {
				e.printStackTrace();
				LOG.info("getToken Service: Getting AuthToken Error: "+e.getMessage());
				response = "{ \"error\" : \"AUTH\", \"error_description\" : \"An error occurred\" }";
			}
			System.out.println("Response:" + response);
		}
		LOG.info("getToken Service: Returning Response: "+response);
		ResponseBuilder rb = Response.ok(response).header("Content-Type", "application/json");
		rb.header("Access-Control-Allow-Origin", "*");
		return rb.build();
	}

	@GET
	@Path("getSightCallAppId")
	public Response getSightCallAppId(@Context HttpServletRequest httpRequest){
		String response = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.SIGHT_CALL_APP_ID.getValue());
		LOG.info("getSightCallAppId Service: SightCall App Id from DB: "+response);
		String appVersion = ObjectUtils.nullSafe(httpRequest.getHeader(AppConstants.StatusConstants.DEVICETYPE.getValue()));
		if(appVersion.equalsIgnoreCase(AppConstants.DeviceTypes.IOS.getValue())){
			Map<String, String> returnMap = new LinkedHashMap<String, String>();
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
			if(ObjectUtils.isEmpty(response)) {
				returnMap.put(AppConstants.JsonConstants.REASON.name(), "Key not found");
			}else{
				returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
				returnMap.put(AppConstants.JsonConstants.DATA.name(), response);
			}
			response = JsonUtil.toJson(returnMap);
		}
		LOG.info("getSightCallAppId Service: Returning response: "+response);
		return Response.ok(response).build();
	}
}
