package com.healthslate.patientapp.ws;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.healthslate.patientapp.model.dao.DeviceDAO;
import com.healthslate.patientapp.model.dao.LogDAO;
import com.healthslate.patientapp.model.dao.PreferencesDAO;
import com.healthslate.patientapp.model.dao.UserDAO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.codec.Base64;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/*
* ======= FILE CHANGE HISTORY =======
* [2/11/2015]: Added getImage webservice, __oz
* ===================================
 */

@Path("/social")
@SuppressWarnings("deprecation")
public class SocialGroupServices {
	static final Logger LOG = LoggerFactory.getLogger(SocialGroupServices.class);
	static final String JPG = ".jpg";

	@GET
	@Path("/getSocialData")
	public Response getSocialData(@Context HttpServletRequest httpRequest) {

		String method = httpRequest.getHeader(AppConstants.METHOD);
		String values = httpRequest.getHeader(AppConstants.VALUES);
		if(values == null){
			values = "";
		}

		String url = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.ELGG_WEB_SERVICE_PATH.getValue()) + method + values;
		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LOG.info("getSocialData Service: Hitting elgg URL: "+url);
		try {
			String response = NetworkUtils.getRequest(url);
			LOG.info("GetSocialData Service: Elgg Response: "+response);
			returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
			returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), response);
		}
		catch (ClientProtocolException e) {
			e.printStackTrace();
			LOG.info("getSocialData Service: Error in calling elgg service: "+e.getMessage());
			returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Error in calling elgg service");
		}
		catch (IOException e) {
			e.printStackTrace();
			LOG.info("getSocialData Service: Error in calling elgg service: "+e.getMessage());
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getSocialData Service: Returing response: "+response);
		return Response.ok(response).build();
	}

	@POST
	@Path("/postSocialData")
	@SuppressWarnings({"rawtypes" })
	public Response postSocialData(String jsonString, @Context HttpServletRequest httpRequest) {

		jsonString = CommonUtils.extractDataFromRequest(jsonString, httpRequest);

		String method = httpRequest.getHeader(AppConstants.METHOD);
		LOG.info("postSocialData Service: elgg method: "+method);

		//if post a comment then notify owner of this post that he/she has received comment
		if(method.equalsIgnoreCase(AppConstants.NEWSFEED.POST_COMMENT.getValue())){
			LOG.info("postSocialData Service: notifying comment");
			try {
				JSONObject jsonObject = new JSONObject(jsonString);
				String commentOnUserName = jsonObject.getString(AppConstants.NEWSFEED.OWNER_USERNAME.getValue());
				String userName = jsonObject.getString(AppConstants.NEWSFEED.USERNAME.getValue());
				String postId = jsonObject.getString(AppConstants.NEWSFEED.POST_ID.getValue());
				if( !ObjectUtils.isEmpty(commentOnUserName) && !(userName.equalsIgnoreCase(commentOnUserName)) ){
					User user = new UserDAO().getUserByEmail(commentOnUserName);
					if(user != null){
						Patient patient = user.getPatient();
						if(patient != null){
							Device device = new DeviceDAO().getDeviceByMac(patient.getDeviceMacAddress());
							if(device != null){
								//send notification here
								List<Device> newDevices = new ArrayList<Device>();
								newDevices.add(device);
								LOG.info("postSocialData Service: sending push notification to member UUID: "+patient.getUuid() + ", device mac address: " + device.getDeviceMacAddress());
								String result = PushNotificationManager.pushMessage(newDevices, AppConstants.GCMKEYS.NOTIFY_COMMENT.getValue(), postId, httpRequest);
								LOG.info("postSocialData Service: push notification result: " +result);
							}else{
								LOG.info("postSocialData Service: member device not found UUID: "+patient.getUuid());
							}
						}else{
							LOG.info("postSocialData Service: member not found email: "+user.getEmail());
						}
					}else{
						LOG.info("postSocialData Service: member not found email: "+commentOnUserName);
					}
				}else{
					LOG.info("postSocialData Service: unable to extract owner user name: "+commentOnUserName);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		String url = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.ELGG_WEB_SERVICE_PATH.getValue()) + method;
		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LOG.info("postSocialData Service: Hitting elgg URL: "+url);
		try {
			List nameValuePairs = getDataInPairs(jsonString);
			if(nameValuePairs != null){
				String response = NetworkUtils.postRequest(url, nameValuePairs);
				LOG.info("GetSocialData Service: Elgg Response: "+response);
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
				returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), response);
			} else{
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Invalid json");
				LOG.info("postSocialData Service: Invalid JSON");
			}
		}
		catch (ClientProtocolException e) {
			e.printStackTrace();
			LOG.info("postSocialData Service: Error in service: "+e.getMessage());
		}
		catch (IOException e) {
			e.printStackTrace();
			LOG.info("postSocialData Service: Error in service: "+e.getMessage());
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("postSocialData Service: Returing response: "+response);
		return Response.ok(response).build();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List getDataInPairs(String jsonString){
		JSONObject object;
		List nameValuePairs = null;

		try {
			object = new JSONObject(jsonString);
			String[] keys = JSONObject.getNames(object);

			nameValuePairs = new ArrayList<List>(keys.length);
			for (String key : keys)	{
				Object value = object.get(key);
				nameValuePairs.add(new BasicNameValuePair(key, value.toString()));
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return nameValuePairs;
	}

	public void sendUpdatedPost(Log log, String title, String description) {
		String url =new PreferencesDAO().getPreference(AppConstants.PreferencesNames.ELGG_WEB_SERVICE_PATH.getValue()) + AppConstants.SOCIALKEYS.UPDATEPOST.getValue();
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.USERNAME.getValue(), log.getPatient().getUser().getEmail()));
		nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.Q_CUSTOM_ID.getValue(), log.getLogId()));

		if(!ObjectUtils.isEmpty(title)){
			nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.TITLE.getValue(), title));
		}

        if(!ObjectUtils.isEmpty(description)){
            nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.DESCRIPTION.getValue(), description));
        }
		nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.LOG_TIME.getValue(), log.getCreatedOn().toString()));
		try {
			if(nameValuePairs != null){
				String response = NetworkUtils.postRequest(url, nameValuePairs);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			LOG.info("saveLog service: sendUpdatedPost: ClientProtocolException: "+e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			LOG.info("saveLog service: sendUpdatedPost: IOException: "+e.getMessage());
		}
	}

	//send food log post
	public void sendImagePost(Log log) {
		LogDAO logDAO = new LogDAO();
		String method = AppConstants.SOCIALKEYS.UPLOADPOST.getValue();
		String url = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.ELGG_WEB_SERVICE_PATH.getValue()) + method;
		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());

		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.USERNAME.getValue(), log.getPatient().getUser().getEmail()));
			nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.TITLE.getValue(), log.getFoodLogSummary().getType()));
			nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.POST_TYPE.getValue(), AppConstants.NEWSFEED.FOODLOG.getValue()));
			nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.CUSTOM_ID.getValue(), log.getLogId()));
			nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.GUESS_CARBS.getValue(), log.getFoodLogSummary().getGuessCarbs()+""));
            nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.LOG_TIME.getValue(), log.getCreatedOn().toString()));

			FoodLogSummary foodLogSummary = log.getFoodLogSummary();
			String foodLogStatus = log.getStatus();

			//if log status is Processed and has food log summary in it then add to list to send to social, __oz
			if(foodLogSummary != null && !ObjectUtils.isEmpty(foodLogStatus) && foodLogStatus.equalsIgnoreCase(AppConstants.PROCESSED)) {
				if(!ObjectUtils.isEmpty(foodLogSummary.getMealName())){
					nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.MEAL_NAME.getValue(), foodLogSummary.getMealName()));
				}

				if(foodLogSummary.getCarbs() != null){
					nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.CARBS.getValue(), String.valueOf(foodLogSummary.getCarbs())));
				}

				if(foodLogSummary.getHasMissingFood() != null && foodLogSummary.getHasMissingFood())	{
					nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.HAS_MISSING_FOOD.getValue(), AppConstants.TRUE));
				}else{
					nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.HAS_MISSING_FOOD.getValue(), AppConstants.FALSE));
				}
			} else {
				if(!ObjectUtils.isEmpty(foodLogSummary.getNotes())){
					nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.MEAL_NAME.getValue(), foodLogSummary.getNotes()));
				}
			}

			if(log.getIsShareWithGroup() != null && log.getIsShareWithGroup() == true){
				nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.GROUP.getValue()));
			} else{
				nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.POST_ACCESS.getValue(), AppConstants.NEWSFEED.PRIVATE.getValue()));
			}

			if(log.getFoodLogSummary().getHasImage()!= null && log.getFoodLogSummary().getHasImage()){
				nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.FILENAME.getValue(), log.getFoodLogSummary().getFoodImage().getImageName()));
				String imageName = log.getFoodLogSummary().getFoodImage().getImageName();
				String[] imageNameParts = imageName.split("\\.");
				if(imageNameParts.length > 1){

					nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.FILETYPE.getValue(), "image/"+imageNameParts[1]));

					BufferedImage image = ImageIO.read(new File(CommonUtils.getProjectPath() + AppConstants.LOG_IMAGE_FOLDER + imageName));
					/*Changed By Zeeshan*/
					if(image.getHeight()!=image.getWidth()){
						int scale = image.getHeight();
						int x = (image.getWidth() - scale ) /2;  ;
						int y = 0;
						if(image.getWidth() < image.getHeight()){
							scale = image.getWidth();
							y= (image.getHeight() - scale ) /2;  
							x = 0 ;
						}

						image = image.getSubimage(x,y , scale, scale);
					}
					/*End Changed*/
					ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
					ImageIO.write(image, imageNameParts[1], baos); 
					String encodedFoodImage = new String(Base64.encode(baos.toByteArray()));

					nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.ENCODEDCONTENT.getValue(), encodedFoodImage));
				}
			}

			if(nameValuePairs != null){
				int postId = 0;
				LOG.info("Hitting Elgg service for image upload at URL: "+url);
				String response = NetworkUtils.postRequest(url, nameValuePairs);
				LOG.info("Elgg Image Uplaod Response: "+response);

				try {
					JsonElement jelement = new JsonParser().parse(response);
					JsonObject jobject = jelement.getAsJsonObject();
					JsonArray jsonarray = new JsonArray() ;

					int statusFromElgg = jobject.get(AppConstants.SOCIALKEYS.STATUS.getValue()).getAsInt();

					if(statusFromElgg != -1){

						if(jobject.get(AppConstants.SOCIALKEYS.RESULT.getValue()).isJsonArray()){
							jsonarray = jobject.get(AppConstants.SOCIALKEYS.RESULT.getValue()).getAsJsonArray();
							JsonElement jelementInner=jsonarray.get(0);
							jobject = jelementInner.getAsJsonObject();
						}
						else{
							jobject = jobject.get(AppConstants.SOCIALKEYS.RESULT.getValue()).getAsJsonObject();
						}

						postId = jobject.get("postId").getAsInt();
					}


					log.setPostId(postId);
					logDAO.save(log);

				} catch (Exception e) {
					e.printStackTrace();
					LOG.info("Elgg Image Upload Exception: "+e.getMessage());
				}
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
				returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), response);
			}
		}
		catch (ClientProtocolException e) {
			e.printStackTrace();
			LOG.info("Elgg Image Upload ClientProtocolException: "+e.getMessage());
		}
		catch (IOException e) {
			e.printStackTrace();
			LOG.info("Elgg Image Upload IOException: "+e.getMessage());
		}
	}

	public int uploadProfileImagePost(Patient patient) {
		int status = -1;
		if(patient != null && patient.getImage() != null && !patient.getImage().isEmpty()) {
			String method = AppConstants.SOCIALKEYS.UPLOAD_PROFILE_PIC.getValue();
			String url = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.ELGG_WEB_SERVICE_PATH.getValue()) + method;

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.USERNAME.getValue(), patient.getUser().getEmail()));
			nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.TYPE.getValue(), "image/"+JPG));
			nameValuePairs.add(new BasicNameValuePair(AppConstants.NEWSFEED.ENCODEDCONTENT.getValue(), patient.getImage()));
			try {
				LOG.info("uploadProfileImagePost: Hitting elgg URL: "+url+"\nwith patientId: "+patient.getPatientId());
				String response = NetworkUtils.postRequest(url, nameValuePairs);
				LOG.info("uploadProfileImagePost: elgg response: "+response);
				status = CommonUtils.getStatusFromElgg(response);
			} catch (Exception e) {
				e.printStackTrace();
				LOG.info("uploadProfileImagePost: Exception: "+e.getMessage());
			}
		}
		return status;
	}
/*

	@SuppressWarnings({ "rawtypes", "unchecked", "resource" })
	public String postRequest(String url, List nameValuePairs) throws ClientProtocolException, IOException{
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);

		post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		HttpResponse response = client.execute(post);
		HttpEntity responseEntity = response.getEntity();
		java.util.Scanner s = new java.util.Scanner(responseEntity.getContent()).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

	@SuppressWarnings({ "resource" })
	public String getRequest(String url) throws ClientProtocolException, IOException{
		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(url);

		HttpResponse response = client.execute(request);
		HttpEntity responseEntity = response.getEntity();
		java.util.Scanner s = new java.util.Scanner(responseEntity.getContent()).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}
*/

	//[2/11/2015]: Added by __oz
	@GET
	@Path("/getImage")
	public Response getImageFromElgg(@QueryParam("imagePath") String imagePath, @Context HttpServletRequest httpRequest) {
		URLConnection urlConnection = null;
		int response = -1;
		InputStream inputStream = null;
		byte[] imageBytes = null;
		@SuppressWarnings("unused")
		String contentType = "image/png";
		try {
			LOG.info("getImage Service: requesting for image: "+imagePath);
			urlConnection = new URL(imagePath).openConnection();
			HttpURLConnection httpConn = (HttpURLConnection) urlConnection;
			httpConn.setRequestMethod("GET");
			httpConn.connect();
			response = httpConn.getResponseCode();
			LOG.info("getImage Service: Connection response for image: "+response);
			if (response == HttpURLConnection.HTTP_OK) {
				LOG.info("getImage Service: Sending image");
				inputStream = httpConn.getInputStream();
				imageBytes = IOUtils.toByteArray(inputStream);
			} else {
				LOG.info("getImage Service: Sending null as image");
			}

			contentType = httpConn.getHeaderField("Content-Type");
			httpConn.disconnect();
			return Response.ok(imageBytes, MediaType.APPLICATION_OCTET_STREAM).build();

		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("getImage Service: exception in getting image from elgg:  "+e.getMessage());
		}

		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Unable to get image.");
		LOG.info("getImage Service: returning response, unable to get image");
		return Response.ok(returnDataMap).build();
	}

	public UserActivity getUserActivityCounts(String userName){
		String url = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.ELGG_WEB_SERVICE_PATH.getValue()) + AppConstants.SOCIALKEYS.USER_ACTIVITY_COUNTS.getValue();
		UserActivity userActivity = new UserActivity();
		try {
			url += "&userName="+userName;
            LOG.info("getUserActivityCounts Service: provided data email: " + url);
			String response = NetworkUtils.getRequest(url);
            LOG.info("getUserActivityCounts Service: response from elgg: " + response);
			if(JsonUtil.isValidJSON(response)){
				JSONObject jsonObject = new JSONObject(response);
				if(jsonObject.getInt("status") > -1){
					JSONObject activityObj = jsonObject.getJSONObject(AppConstants.StatusConstants.RESULT.getValue());
					userActivity = (UserActivity) JsonUtil.fromJson(activityObj.toString(), UserActivity.class);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return userActivity;
	}

    public UserActivity getUserActivityCounts(String userName, long startDate, long endDate){
        String url = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.ELGG_WEB_SERVICE_PATH.getValue()) + AppConstants.SOCIALKEYS.USER_ACTIVITY_COUNTS_BY_DATE.getValue();
        UserActivity userActivity = new UserActivity();
        try {
            url += "&userName="+userName+"&startDate="+startDate+"&endDate="+endDate;
            LOG.info("getUserActivityCountsByDate Service: provided data email: " + url);
            String response = NetworkUtils.getRequest(url);
            LOG.info("getUserActivityCountsByDate Service: response from elgg: " + response);
            if(JsonUtil.isValidJSON(response)){
                JSONObject jsonObject = new JSONObject(response);
                if(jsonObject.getInt("status") > -1){
                    JSONObject activityObj = jsonObject.getJSONObject(AppConstants.StatusConstants.RESULT.getValue());
                    userActivity = (UserActivity) JsonUtil.fromJson(activityObj.toString(), UserActivity.class);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return userActivity;
    }

	@GET
	@Path("/getDataByPostId")
	public Response getDataByPostId(@QueryParam("postId") String postId, @QueryParam("userName") String userName, @Context HttpServletRequest httpRequest) {
		String method = AppConstants.SOCIALKEYS.GET_DATA_BY_POST_ID.getValue();
		String values = "&"+AppConstants.SOCIALKEYS.POST_GUID.getValue()+"="+postId+"&"+AppConstants.NEWSFEED.USERNAME.getValue()+"="+userName;

		String url = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.ELGG_WEB_SERVICE_PATH.getValue()) + method + values;
		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LOG.info("getDataByPostId service: Hitting elgg URL: "+url);
		try {
			String response = NetworkUtils.getRequest(url);
			LOG.info("getDataByPostId service: Elgg Response: "+response);
			returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
			returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), response);
		}catch (ClientProtocolException e) {
			e.printStackTrace();
			LOG.info("getDataByPostId service: Error in calling elgg service: "+e.getMessage());
			returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Error in calling elgg service");
		}catch (IOException e) {
			e.printStackTrace();
			LOG.info("getDataByPostId service: Error in calling elgg service: "+e.getMessage());
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getDataByPostId Service: returning response: "+response);
		return Response.ok(response).build();
	}
}