package com.healthslate.patientapp.ws;

import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.healthslate.patientapp.util.JsonUtil;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.gson.Gson;
import com.healthslate.patientapp.model.dao.LogDAO;
import com.healthslate.patientapp.model.dao.PatientDAO;
import com.healthslate.patientapp.model.entity.FoodAudio;
import com.healthslate.patientapp.model.entity.FoodImage;
import com.healthslate.patientapp.model.entity.FoodLogSummary;
import com.healthslate.patientapp.model.entity.Log;
import com.healthslate.patientapp.model.entity.Patient;
import com.healthslate.patientapp.util.AppConstants;
import com.healthslate.patientapp.util.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/file")
public class UploadServices {
	static final Logger LOG = LoggerFactory.getLogger(UploadServices.class);
	private final String PATIENT_NOT_FOUND = "Patient not found";
	private final String DASH = "-";
	private final String COLON = ":";
	
	@POST
	@Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(
        @FormDataParam("file") InputStream uploadedInputStream,
        @FormDataParam("file_detail") String fileDetail,
        @Context HttpServletRequest httpRequest) {
		
		LogDAO logDAO = new LogDAO();
		PatientDAO patientDAO = new PatientDAO();
		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
 		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LOG.info("upload Service: File Detail: "+fileDetail);
 		String[] data = null;
 		
 		if(fileDetail.contains(COLON)){
 			data = fileDetail.split(COLON);
 		} else if(fileDetail.contains(DASH)) {
 			data = fileDetail.split(DASH);
 		}
		
		if(data.length > 3) { // for saving meal image/audio 
	        //get log
	        Log log = logDAO.getLogById(data[1]);
	        System.out.println("<LOG_CHECK> IMAGE UPLOAD SERVICE STARTED");
	        if(log != null){
				LOG.info("upload Service: Found Log with LogId: "+log.getLogId());
	        	if(data[2].contains(AppConstants.ServicesConstants.IMAGE.name())){
	        		if(saveLogImage(log, data, uploadedInputStream)){
	        			returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
						System.out.println("<LOG_CHECK> SENDING REQUEST TO ELGG IMAGE UPLOADED, LOG ID:" + log.getLogId());
						LOG.info("upload Service: Sending Image on Elgg with logId: "+log.getLogId());
						SocialGroupServices groupServices = new SocialGroupServices();
	        			groupServices.sendImagePost(log);
	        			System.out.println("<LOG_CHECK> REQUEST COMPLETED FROM ELGG");
	        		}
	        	} else if(data[2].contains(AppConstants.ServicesConstants.AUDIO.name())){
	        		if(saveAudio(log, data, uploadedInputStream)){
	        			returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
	        		}
	        	}
	        } else {
				LOG.info("upload Service: Log not found: ");
			}
		} else if(data.length == 3 ) { // For Patient image on elgg
			Long patientId =  Long.parseLong(data[1]);
			Patient patient = patientDAO.getPatientById(patientId);
			if(patient == null) {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), PATIENT_NOT_FOUND);
			} else {
				/*if(data[2].contains(AppConstants.ServicesConstants.IMAGE.name())){
					if(saveProfileImage(patient, data, uploadedInputStream)){
						System.out.println("<LOG_CHECK> SENDING REQUEST TO ELGG IMAGE UPLOADED, Patient ID:" + patient.getPatientId());
						SocialGroupServices groupServices = new SocialGroupServices();
	        			String response = groupServices.uploadProfileImagePost(patient);
	        			try {
							JSONObject jObject = new JSONObject(response);
							int status = jObject.getInt(AppConstants.SOCIALKEYS.STATUS.getValue());
							if(response.equalsIgnoreCase(AppConstants.SERVER_ERROR) || status == -1) {
		        				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), IMAGE_NOT_UPLOADED);
		        			} else {
		        				System.out.println("<LOG_CHECK> REQUEST COMPLETED FROM ELGG");
		        				patient.setRegistrationCompleted(true);
		        				new BaseDAO().save(patient);
								returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
		        			}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}*/
			}
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("upload Service: Returning with response: "+response);
		return Response.ok(response).build();
    }

	private boolean saveAudio(Log log, String[] data, InputStream uploadedInputStream) {
		boolean status = false;
		try{
			//save audio
			File fileDir = new File(CommonUtils.getProjectPath() + AppConstants.LOG_AUDIO_FOLDER);
			if (!fileDir.exists()){
				fileDir.mkdir();
			}
	    	String uploadedFileLocation = fileDir.getAbsolutePath() + "\\" + data[0];
			CommonUtils.saveToFile(uploadedInputStream, uploadedFileLocation);
	        status = true;
		}
		catch(Exception e){
			e.printStackTrace();
			LOG.info("Saving Audio failed with exception: "+e.getMessage());
		}
		return status;
	}

	private boolean saveLogImage(Log log, String[] data, InputStream uploadedInputStream) {
		
		boolean status = false;
		LogDAO logDAO = new LogDAO();
		FoodImage image = new FoodImage();
		FoodLogSummary foodLogSummary = new FoodLogSummary();
		
		try{
			//save image
			File fileDir = new File(CommonUtils.getProjectPath() + AppConstants.LOG_IMAGE_FOLDER);
			if (!fileDir.exists()){
				fileDir.mkdir();
			}
	    	String uploadedFileLocation = fileDir.getAbsolutePath()+ "//" + data[0];
			CommonUtils.saveToFile(uploadedInputStream, uploadedFileLocation);
	        image.setImageName(data[0]);
			image.setClientImagePath(data[3]);
			image.setServerImagePath(uploadedFileLocation);
			
			Date date = new Date();
			image.setTimestamp(date.getTime());
	        
			if(log.getFoodLogSummary() != null){
				foodLogSummary = log.getFoodLogSummary();
				
				if(foodLogSummary.getFoodImage() == null){
					image.setFoodLogSummary(foodLogSummary);
					foodLogSummary.setFoodImage(image);
					log.setFoodLogSummary(foodLogSummary);
					logDAO.save(log);
					LOG.info("upload Service: Log Saved in DB with logID: "+log.getLogId());
				}
				status = true;
			}
		}
		catch(Exception e){
			e.printStackTrace();
			LOG.info("upload Service: Saving image failed with exception: "+e.getMessage());
		}
		return status;
	}

	/*private boolean saveProfileImage(Patient patient, String[] data, InputStream uploadedInputStream) {
		boolean status = false;
		try{
			//save image
			File fileDir = new File(CommonUtils.getProjectPath() + AppConstants.PROFILE_IMAGE_FOLDER);
			if (!fileDir.exists()){
				fileDir.mkdir();
			}
	        
	    	String uploadedFileLocation = fileDir.getAbsolutePath()+ "\\" + data[0];
	        CommonUtils.saveToFile(uploadedInputStream, uploadedFileLocation);
	        patient.setImage(data[0]);
	        status = new BaseDAO().save(patient);
		} catch(Exception e){
			e.printStackTrace();
		}
		return status;
	}*/
}
