package com.healthslate.patientapp.ws;

import com.amazonaws.util.json.JSONObject;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.CoachDTO;
import com.healthslate.patientapp.model.dto.GoalDTO;
import com.healthslate.patientapp.model.dto.PatientSessionPreferenceDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import sun.plugin.liveconnect.SecurityContextHelper;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/*
 * ======= FILE CHANGE HISTORY =======
 * [1/29/2015]: Added logTimeOffset and offsetKey in User Table while login, __oz
 * [2/02/2015]: Added coachList to send to patient device while login, __oz
 * [2/09/2015]: Added forgotPassword service, __ad
 * [2/11/2015]: Added changePassword service, __oz
 * [2/13/2015]: Modify authenticate patient service, __ze.
 * [2/23/2015]: Added getCoachesList service, __oz.
 * [3/12/2015]: Added getMedicationsList, savePatientMedication service, __oz.
 * [3/16/2015]: Added getPatientUUID service, __oz.
 * [3/20/2015]: Added saveGCMId service, __oz
 * [3/25/2015]: Added savePatientPhone service, __oz
 * [3/31/2015]: Added getCurriculumPinnedMessages service, __oz.
 * [4/01/2015]: Added getGoalPinnedMessages service, __oz.
 * [4/15/2015]: Added logout service to delete patient device, __oz
 * [05/06/2015]: Added upload motivation image service, __oz
 * [05/07/2015]: Added delete motivation image service, __oz
 * [05/07/2015]: Added upload profile image service, __oz
 * [05/07/2015]: Added get profile image service, __oz
 * [05/15/2015]: Added upload session preference service, __oz
 * [05/15/2015]: Added upload eating preference service, __oz
 * [06/15/2015]: Added getLoginFacilities service, __oz
 * [07/13/2015]: Added getEatingAndSessionPreferences service, __oz
 * ===================================
 */

@Path("/user")
public class UserServices {
	static final Logger LOG = LoggerFactory.getLogger(UserServices.class);
	private final String REG_NOT_COMPLETED = "Your Sign Up process is not completed. Please contact customer service at (888) 291-7245.";
	private PreferencesDAO preferencesDAO = new PreferencesDAO();

	@POST
	@Path("/authenticatePatient")
	public Response authenticatePatient(@Context HttpServletRequest request) {
		System.out.println("<LOGIN_CHECK>  SERVICE CALLED");
		UserDAO userDAO = new UserDAO();
		PatientDAO patientDao = new PatientDAO();
		Map<String, Object> returnValues = new LinkedHashMap<String, Object>();
		returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		returnValues.put(AppConstants.ServicesConstants.TIME_STAMP.name(), System.currentTimeMillis());

		String creds = request.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
		LOG.info("authenticatePatient service: Provided Data: credentials: "+creds);
		Map<String, Object> resultMap = CommonUtils.authenticateUser(creds);
		String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));
		if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {
			System.out.println("<LOGIN_CHECK> Not Authenticated");
			returnValues.put(AppConstants.ServicesConstants.REASON.name(), "Invalid username/password");
			LOG.info("authenticatePatient service: Returning Response: "+status);
			return Response.ok(status).build();
		}else{
			System.out.println("<LOGIN_CHECK> User Authenticated Successfully");
			String deviceType = request.getHeader(AppConstants.StatusConstants.DEVICETYPE.getValue());
            String deviceBuildType = ObjectUtils.nullSafe(request.getHeader(AppConstants.StatusConstants.DEVICE_BUILD_TYPE.getValue()));

			long logTimeOffset = -1l;

			try {
				logTimeOffset = Long.parseLong(request.getHeader(AppConstants.StatusConstants.LOG_TIME_OFFSET.getValue()));
			} catch (NumberFormatException e) {
				e.printStackTrace();
				LOG.info("authenticatePatient service: NumberFormatException: "+e.getMessage());
			}
			String offsetKey = request.getHeader(AppConstants.StatusConstants.OFFSET_KEY.getValue());
			Long patientId = (Long) resultMap.get(AppConstants.JsonConstants.DATA.name());
			Patient patient = patientDao.getPatientById(patientId);
			if (patient != null) {
                User user = patient.getUser();
                if (user != null && user.getUserType().equalsIgnoreCase(AppConstants.PATIENT)) {

                    if(ObjectUtils.nullSafe(user.getIsRegistrationCompleted())){

                        LOG.info("authenticatePatient service: logged in member UUID: "+patient.getUuid() + ", User Id: " + user.getUserId());

                        String techSupportNumber = preferencesDAO.getPreference(AppConstants.PreferencesNames.TECH_SUPPORT_NUMBER.getValue());

                        user.setLogTimeOffset(logTimeOffset);
                        user.setOffsetKey(offsetKey);
                        userDAO.save(user);

                        saveDevice(request, patient, deviceType, deviceBuildType);

                        //[2/2/2015]: also add coach list to patient, __oz
                        List<CoachDTO> coachList = userDAO.getCoachesByPatientId(patient.getPatientId(), false);
                        patient.setCoachList(coachList);

                        patient.setTarget(null);
                        Target targetFromDB = new TargetDAO().getTargetByPatientId(patient.getPatientId());
                        patient.setTarget(targetFromDB);
                        patient.setFilledForms(null);
                        patient.setIsPatientSessionPreferenceSet(false);
                        patient.setIsEatingPreferenceSet(false);

                        if(patient.getPatientSessionPreference() != null){
                            patient.setIsPatientSessionPreferenceSet(true);
                        }

                        if(patient.getPatientEatingPreference() != null){
                            patient.setIsEatingPreferenceSet(true);
                        }

                        returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                        returnValues.put(AppConstants.ServicesConstants.DATA.name(), patient);
                        returnValues.put(AppConstants.ServicesConstants.TECH_SUPPORT_NUMBER.name(), techSupportNumber);
                    } else {
                        LOG.info("authenticatePatient service: registration not completed,  User Id: " + user.getUserId());
                        returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                        returnValues.put(AppConstants.ServicesConstants.REASON.name(), REG_NOT_COMPLETED);
                    }
                } else {
                    LOG.info("authenticatePatient service: tried to logged in with email other than member/patient email,  User Id: " + user.getUserId());
                    returnValues.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                    returnValues.put(AppConstants.ServicesConstants.REASON.name(), "Invalid username/password");
                }
			} else {
				returnValues.put(AppConstants.ServicesConstants.REASON.name(), "Invalid username/password");
			}
			String response = JsonUtil.toJsonExcludedNull(returnValues);
			LOG.info("authenticatePatient service: Returning response: "+response);
			return Response.ok(response).build();
		}
	}

	//[2/09/2015]: Added forgotPassword service, __ad
	@POST
	@Path("/forgotPassword")
	public Response forgotPassword(String emailStirng, @Context HttpServletRequest httpRequest) {

		emailStirng = CommonUtils.extractDataFromRequest(emailStirng, httpRequest);

		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		UserDAO userDao = new UserDAO();
		EmailTokenDAO emailTokenDAO = new EmailTokenDAO();
		Map<String, String> prefMap = preferencesDAO.map();
		LOG.info("forgotPassword service: Provided Data: emailStirng: "+emailStirng);
		try {
			if (userDao.isUserExist(emailStirng)) {
				User user = userDao.getUserByEmail(emailStirng);
				if(user.getIsRegistrationCompleted()) {
					EmailToken emailToken = new EmailToken();
					emailToken.setUsername(user.getEmail());

					String token = user.getEmail() + System.currentTimeMillis();
					token = new String(Base64.encode(token.getBytes()));
					emailToken.setToken(token);

					emailToken.setDateCreated(new Date());
					emailTokenDAO.saveEmailToken(emailToken);

					String resetPasswordUrl = ObjectUtils.getApplicationUrl(httpRequest);

					resetPasswordUrl += "/app/resetPassword.action?token=" + emailToken.getToken();
					String emailText = CommunicationUtils.getEmailTextForResetPassword(user.getEmail(), resetPasswordUrl);

					boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
							user.getEmail(),
							prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
							prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
							prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
							prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
							AppConstants.EmailSubjects.RESET_PASSWORD.getValue());
					LOG.info("forgotPassword service: Email sent to: "+user.getEmail()+", with email Text: "+emailText+", and email sent status: "+isSentEmail);
					if (isSentEmail) {
						returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
						returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), "An email has been sent to your email address.");
					} else {
						returnDataMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
						returnDataMap.put(AppConstants.JsonConstants.REASON.name(), "Unable to send email at the moment.");
					}
				} else {
					returnDataMap.put(AppConstants.JsonConstants.REASON.name(), "Your sign up process is not completed.");
				}
			} else {
				returnDataMap.put(AppConstants.JsonConstants.REASON.name(), "Email address is not valid.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("forgotPassword service: Exception: "+e.getMessage());
			returnDataMap.put(AppConstants.JsonConstants.REASON.name(), AppConstants.UBABLE_TO_SIGNUP);
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("forgotPassword service: Returning response: "+response);
		return Response.ok(response).build();
	}

	//[2/11/2015]: Added changePassword service, __oz
	@POST
	@Path("/changePassword")
	public Response changePassword(String jsonString, @Context HttpServletRequest httpRequest) {

		jsonString = CommonUtils.extractDataFromRequest(jsonString, httpRequest);

		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		UserDAO userDAO = new UserDAO();
		PatientDAO patientDao = new PatientDAO();
		try {
			com.amazonaws.util.json.JSONObject jsonObject = new com.amazonaws.util.json.JSONObject(jsonString);
            String email = jsonObject.getString(AppConstants.JsonKeys.EMAIL_ADDRESS.getValue());
			String oldPassword = jsonObject.getString(AppConstants.JsonKeys.OLD_PASSWORD.getValue());
			String newPassword = jsonObject.getString(AppConstants.JsonKeys.NEW_PASSWORD.getValue());

			User user = userDAO.getUserByEmail(email);
			if (user != null) {
				Patient patient = user.getPatient();
				if (patient != null) {
					if (BCrypt.checkpw(oldPassword, user.getPassword())) {
                        long passwordExpiryTime = DateUtils.addDayInTime(System.currentTimeMillis(), 60);
						patient.setPasswordExpiryTime(passwordExpiryTime);
						user.setPassword(new BCryptPasswordEncoder().encode(newPassword));
						userDAO.save(user);
						returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.JsonConstants.SUCCESS.name());
						returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), AppConstants.ServiceResponseMessages.PASSWORD_CHANGE_SUCCESS.getValue());
                        returnDataMap.put(AppConstants.ServicesConstants.PASSWORD_EXPIRY.name(), passwordExpiryTime+"");
					} else {
						returnDataMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
						returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.ServiceResponseMessages.OLD_PASSWORD_INCORRECT.getValue());
					}
				} else {
					//else user
					returnDataMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.ServiceResponseMessages.PATIENT_NOT_FOUND.getValue());
				}
			} else {
				//else patient
				returnDataMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.ServiceResponseMessages.USER_NOT_FOUND.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("changePassword service: Exception: "+e.getMessage());
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("changePassword service: response: "+response);
		return Response.ok(response).build();
	}

	@POST
	@Path("/saveSettings")
	public Response saveSettings(String settingsJsonString, @Context HttpServletRequest httpRequest) {

		settingsJsonString = CommonUtils.extractDataFromRequest(settingsJsonString, httpRequest);

		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LOG.info("saveSettings service: Provided Data: settingsJsonString: "+settingsJsonString);
		Target targetFromRequest = null;
		BaseDAO baseDAO = new BaseDAO();
		PatientDAO patientDAO = new PatientDAO();
		TargetDAO targetDAO = new TargetDAO();

		try {
			//parse int to Target entity from jsonString
			targetFromRequest = (Target)JsonUtil.fromJson(settingsJsonString, Target.class);
			//if target is successfully parsed
			if (targetFromRequest != null) {
				Patient patientFromRequest = targetFromRequest.getPatient();
				if (patientFromRequest != null) {
					Patient patientFromDB = patientDAO.getPatientById(patientFromRequest.getPatientId());
					//if patient found
					if (patientFromDB != null) {
						//set source to 'Patient' when from device
						targetFromRequest.setSource(AppConstants.TARGETSETTINGS.PATIENT.getValue());
						//get targets from db against patient id from request
						Target targetFromDb = patientFromDB.getTarget();
						if (targetFromDb != null) {
							targetFromRequest.setTargetId(targetFromDb.getTargetId());
							patientFromDB.setTarget(null);
							//first delete old Reminders targets
							targetDAO.deleteOldRemindersByTargetId(targetFromDb.getTargetId());
							//first delete old meal targets
							targetDAO.deleteOldMealTargetsByTargetId(targetFromDb.getTargetId());
							//first delete old medication targets
							targetDAO.deleteOldMedicationTargetsByTargetId(targetFromDb.getTargetId());
						}
						//parsing meal targets from request and set to target object
						List<TargetMeal> meals = targetFromRequest.getMealTargets();
						if (meals != null && meals.size() > 0) {
							for (TargetMeal targetMeal : meals) {
								targetMeal.setTarget(targetFromRequest);
							}
						}
						//parsing medication targets from request and set to target object
						List<TargetMedication> medications = targetFromRequest.getMedTargets();
						if (medications != null && medications.size() > 0) {
							for (TargetMedication medication : medications) {
								medication.setTarget(targetFromRequest);
							}
						}
						Reminder reminder = targetFromRequest.getReminder();
						if (reminder != null) {
							reminder.setTarget(targetFromRequest);
						}
						//if newly created Target, then also set patient in it
						targetFromRequest.setPatient(patientFromDB);
						//set target to patient
						patientFromDB.setTarget(targetFromRequest);

                        try {
                            //save/update target from request
                            baseDAO.save(targetFromRequest);
                            LOG.info("saveSettings service: save/update target settings: "+targetFromRequest.toString());
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                        } catch (Exception e) {
                            e.printStackTrace();
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                        }

					} else { //else patient
						returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No patient found in databases");
					}
				} else { //else patient from request
					returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "No patient found from request");
				}
			} else {//else target null
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Unable to parse target from request");
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("saveSettings service: Provided Data: Exception: "+e.getMessage());
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("saveSettings service: Returning Response: "+response);
		return Response.ok(response).build();
	}

	@POST
	@Path("/sendSupportEmail")
	public Response sendSupportEmail(String emailText, @Context HttpServletRequest httpRequest) {

		emailText = CommonUtils.extractDataFromRequest(emailText, httpRequest);

		LOG.info("sendSupportEmail service: Provided Data: emailText: "+emailText);
		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());

		try {
			Map<String, String> prefMap = preferencesDAO.map();
			emailText = "Email from HealthSlate Patient App<br/><br/> " + emailText.replaceAll("\n", "<br/>");
			String email = prefMap.get(AppConstants.PreferencesNames.SUPPORT_EMAIL_ADDRESS.getValue());
			boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
					email,
					prefMap.get(AppConstants.PreferencesNames.EMAIL_REPLY_ADDRESS.getValue()),
					prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
					prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
					prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
					AppConstants.EmailSubjects.CUSTOMER_SUPPORT.getValue());
			LOG.info("sendSupportEmail service: Sent email to: "+email+", with emailText: "+emailText+", with sent status: "+isSentEmail);
			if (isSentEmail) {
				returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("sendSupportEmail service: Exception: "+e.getMessage());
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("sendSupportEmail service: Returning response: "+response);
		return Response.ok(response).build();
	}

	//[2/16/2015]: Added forgotPassword service, __ad
	@POST
	@Path("/saveFeedback")
	public Response saveFeedback(String feedbackJsonString, @Context HttpServletRequest httpRequest) {

		feedbackJsonString = CommonUtils.extractDataFromRequest(feedbackJsonString, httpRequest);

		Map<String, String> prefMap = preferencesDAO.map();
		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LOG.info("saveFeedback service: Provided Data: feedbackJsonString: "+feedbackJsonString);
		try {
			Feedback feedback = (Feedback) JsonUtil.fromJson(feedbackJsonString, Feedback.class);
			if(feedback != null) {
				feedback.setTimestamp(System.currentTimeMillis());

				Patient patient = new PatientDAO().getPatientById(feedback.getPatient().getPatientId());
				User user = patient.getUser();
				EmailToken emailToken = new EmailToken();
				emailToken.setUsername(user.getEmail());

				String token = user.getEmail() + System.currentTimeMillis();
				token = new String(Base64.encode(token.getBytes()));
				emailToken.setToken(token);

				emailToken.setDateCreated(new Date());
				new EmailTokenDAO().saveEmailToken(emailToken);
				LOG.info("saveFeedback service: Saved Email Token in db: "+emailToken.toString());
				String url = ObjectUtils.getApplicationUrl(httpRequest)+"/app/admin/Feedback.action";
				String emailText = CommunicationUtils.getEmailTextForFeedback(url);
				String emailStrings = ObjectUtils.nullSafe(prefMap.get(AppConstants.PreferencesNames.FEED_BACK_EMAIL_ID.getValue()));
				String[] emails = emailStrings.split(",");
				for (String email : emails) {
					boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,  
							email,
							prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
							prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
							prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
							prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
							AppConstants.EmailSubjects.MEMBER_FEEDBCK.getValue());
					LOG.info("saveFeedback service: Sent email to: "+email+", with emailText: "+emailText+", with sent status: "+isSentEmail);
				}
				boolean isSave = new BaseDAO().save(feedback);
				LOG.info("saveFeedback service: Saved Feedback in db: "+feedback.toString());
				if(isSave) {
					returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
				} else {
					returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SAVE_FEEDBACK);
				}
			} else {
				returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SAVE_FEEDBACK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("saveFeedback service: Exception: "+e.getMessage());
			returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.UBABLE_TO_SAVE_FEEDBACK);
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("saveFeedback service: Returning response: "+response);
		return Response.ok(response).build();
	}

	private void saveDevice(HttpServletRequest request, Patient patient, String deviceType, String deviceBuildType) {
		DeviceDAO deviceDao = new DeviceDAO();
		String mac = request.getHeader(AppConstants.UserInfo.MAC_ADDRESS.getValue());
		System.out.println("<LOGIN CHECK> MAC ADDRESS: " + mac);

		String registrationId = request.getHeader(AppConstants.SP_REGISTRATION_ID);
		System.out.println("<LOGIN CHECK> REGISTRATION ID: " + registrationId);

		if (!ObjectUtils.isEmpty(registrationId) && !ObjectUtils.isEmpty(mac)) {

			Device device = deviceDao.getDeviceByMac(mac);
			System.out.println("<LOGIN CHECK> DEVICE: " + device);

			//set mac address to Patient as well
			if (patient != null) {
				patient.setDeviceMacAddress(mac);
				new PatientDAO().save(patient);
			}

			if (device == null) {
				device = new Device();
				device.setDeviceMacAddress(mac);
			}

            device.setDeviceBuildType(deviceBuildType);
			device.setDeviceType(deviceType);
			device.setRegistrationId(registrationId);
			deviceDao.save(device);
		}
	}

    //[2/23/2015]: Added getCoachesList service, __oz.
    @GET
    @Path("/getCoachesList")
    public Response getCoachesList(@QueryParam("patientId") String patientId, @Context HttpServletRequest request) {
		LOG.info("getCoachesList service: Provided Data: patientId: "+patientId);
        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        try {
			List<CoachDTO> coachList = null;
			String idOrUUID = patientId;

			try {
				//for old apk, which call via patientId support for them
				long memberId = Long.parseLong(idOrUUID);
				coachList = new UserDAO().getCoachesExcludeDefaultTechSupportByPatientId(memberId, false);
			} catch (Exception e) {
				//for new apks get from authorization
				String authorization = request.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
				Map<String, Object> resultMap = CommonUtils.authenticateUser(authorization);
				String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));
				if(status.equalsIgnoreCase(AppConstants.ACCESS_GRANTED)){
					long authPatientId = (Long) resultMap.get(AppConstants.JsonConstants.DATA.name());
					coachList = new UserDAO().getCoachesExcludeDefaultTechSupportByPatientId(authPatientId, false);
				}else{
					return Response.status(Response.Status.FORBIDDEN).entity(status).build();
				}
			}

			if(coachList != null && coachList.size() > 0){
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), coachList);
            }else{
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), "No coaches found.");
            }
        } catch (Exception e) {
            e.printStackTrace();
			LOG.info("getCoachesList service: Exception: "+e.getMessage());
        }
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getCoachesList service: Returning response: "+response);
        return Response.ok(response).build();
    }

    //[3/12/2015]: Added getMedications List service, __oz.
    @GET
    @Path("/getMedicationsList")
    public Response getMedicationsList() {
		LOG.info("getMedicationsList service:");
        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        try {
            List<MedicationMaster> medications = new MedicationMasterDAO().list();
            if(medications != null && medications.size() > 0){
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), medications);
            }else{
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), "No medications found.");
            }
        } catch (Exception e) {
            e.printStackTrace();
			LOG.info("getMedicationsList service: Exception: "+e.getMessage());
        }
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("getCoachesList service: Returning response: "+response);
		return Response.ok(response).build();
    }

    //[3/12/2015]: Added savePatientMedication service, __oz
    /*@POST
    @Path("/savePatientMedication")
    public Response savePatientMedication(String medications, @Context HttpServletRequest httpRequest) {
        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		LOG.info("savePatientMedication service: Provided Data: patientId: "+patientId+", medications: "+medications);
        try {
            if(patientId != null && !ObjectUtils.isEmpty(medications)){
                new PatientDiabDietMedDAO().removeMedicationByPatientId(patientId);
                Patient patient = new Patient();
                patient.setPatientId(patientId);

                //Medications
                PatientMedication patientMedication = new PatientMedication();
                patientMedication.setPatient(patient);
                patientMedication.setMedicationsName(medications);
                new BaseDAO().save(patientMedication);
				LOG.info("savePatientMedication service: Saved patientMedication: "+patientMedication,toString());
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
            }
        } catch (Exception e) {
            e.printStackTrace();
			LOG.info("savePatientMedication service: Exception: "+e.getMessage());
        }
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("savePatientMedication service: Returning response: "+response);
		return Response.ok(response).build();
    }*/

    //[3/16/2015]: Added getPatientUUID service, __oz.
    @GET
    @Path("/getPatientUUID")
    public Response getPatientUUID(@Context HttpServletRequest request, final @Context SecurityContext securityContext) {

        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());

        try {
            /*  String credentials = request.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
            LOG.info("getPatientUUID service: Provided Data: credentials: " + credentials);
            Map<String, Object> resultMap = CommonUtils.authenticateUser(credentials);*/

        	User loadUser = loadUserFromSecurityContext(securityContext);
			LOG.info("getPatientUUID service: Provided Data: credentials: " + loadUser);

			Map<String, Object> resultMap = authenticateUser(loadUser);

			String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));

            if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {
                LOG.info("getPatientUUID service: Returning Response: " + status);
                return Response.ok(status).build();
            }else{
                Long patientId = (Long) resultMap.get(AppConstants.JsonConstants.DATA.name());
                Patient patient = new PatientDAO().getPatientById(patientId);
                if(patient != null){
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                    returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), patient.getUuid());
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Patient not found.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("getPatientUUID service: Exception: "+e.getMessage());
        }

        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("getPatientUUID service: Returning response: "+response);
        return Response.ok(response).build();
    }

    //[3/20/2015]: Added saveGCMId service, __oz
    @POST
    @Path("/saveGCMId")
    public Response saveGCMId(String jsonString, @Context HttpServletRequest httpRequest) {

		jsonString = CommonUtils.extractDataFromRequest(jsonString, httpRequest);

        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        LOG.info("saveGCMId service: Provided Data: "+jsonString);
        try {

            JSONObject jsonObject = new JSONObject(jsonString);

            String macAddress = jsonObject.getString("macAddress");
            String registrationId = jsonObject.getString("registrationId");

            LOG.info("saveGCMId service: mac address: "+macAddress+", registrationId: "+registrationId);

            if(!ObjectUtils.isEmpty(registrationId) && !ObjectUtils.isEmpty(macAddress)){
                Device device = new DeviceDAO().getDeviceByMac(macAddress);
                if(device != null){
                    device.setRegistrationId(registrationId);
                }
                new BaseDAO().save(device);
                LOG.info("saveGCMId service: Saved GCM registrationId: "+registrationId);
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("saveGCMId service: Exception: "+e.getMessage());
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("saveGCMId service: Returning response: "+response);
        return Response.ok(response).build();
    }

    //[3/25/2015]: Added savePatientPhone service, __oz
    @POST
    @Path("/savePatientPhone")
    public Response savePatientPhone(String phoneNumber, @Context HttpServletRequest httpRequest) {

		phoneNumber = CommonUtils.extractDataFromRequest(phoneNumber, httpRequest);

        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        PatientDAO patientDAO = new PatientDAO();
        try {

            String credentials = httpRequest.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
            LOG.info("savePatientPhone service: Provided Data: credentials: " + credentials);

            Map<String, Object> resultMap = CommonUtils.authenticateUser(credentials);
            String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));
            if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), status);
                LOG.info("savePatientPhone service: Returning Response: " + status);
                return Response.ok(status).build();
            }else{
                long patientId = (Long) resultMap.get(AppConstants.JsonConstants.DATA.name());
                Patient patient = patientDAO.getPatientById(patientId);
                if(patient != null){
                    User user = patient.getUser();
                    if(user != null){
                        user.setPhone(phoneNumber);
                        new BaseDAO().save(user);
                        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                    }else{
                        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "User not found.");
                    }
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Patient not found.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("savePatientPhone service: Exception: "+e.getMessage());
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("savePatientPhone service: Returning response: "+response);
        return Response.ok(response).build();
    }

    //[3/31/2015]: Added getCurriculumPinnedMessages service, __oz.
    @GET
    @Path("/getCurriculumPinnedMessages")
    public Response getCurriculumPinnedMessages(@QueryParam("timestamp") String timestamp, @Context HttpServletRequest request) {

        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        String curriculumAPI = new PreferencesDAO().getPreference(AppConstants.PreferencesNames.CURRICULUM_API.getValue());
        final String LEARNING = "learning";
        final String ACTIVITIES = "activities";

        try {

            String credentials = request.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
            LOG.info("getCurriculumPinnedMessages service: Provided Data: credentials: " + credentials);
            Map<String, Object> resultMap = CommonUtils.authenticateUser(credentials);
            String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));

            if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {
                LOG.info("getCurriculumPinnedMessages service: Returning Response: " + status);
                return Response.ok(status).build();
            }else{
                Long patientId = (Long) resultMap.get(AppConstants.JsonConstants.DATA.name());
                Patient patient = new PatientDAO().getPatientById(patientId);

                if(patient != null){

                    String url = curriculumAPI.concat(AppConstants.CURRICULUM_CONTENT_USERS.replace("$$uuid$$", patient.getUuid()));
                    String response = NetworkUtils.getRequest(url);
                    PatientCurriculum curriculum = (PatientCurriculum)JsonUtil.fromJson(response, PatientCurriculum.class);

                    //if timestamp is empty or users curriculum not yet started
                    if(ObjectUtils.isEmpty(timestamp) && (curriculum != null && ObjectUtils.isEmpty(curriculum.getStart()))){
                        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                        returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), "");
                        returnDataMap.put(AppConstants.ServicesConstants.TIME_STAMP.name(), "");
                    }else{

                        Date currentDate = new Date();
                        Date curriculumStartDate = DateUtils.parseDate(curriculum.getStart(), DateUtils.DATE_FORMAT_CURRICULUM);
                        Long startOfDayToday = DateUtils.getStartOfDayTime(currentDate.getTime());
                        Long startOfCurriculumDay = DateUtils.getStartOfDayTime(curriculumStartDate.getTime());
                        int daysDifference = DateUtils.getDaysBetween(startOfCurriculumDay, startOfDayToday);
                        int weekHalf = ( (daysDifference % 7) <= 2 ) ? 1 : 2;

                        String localTimestamp = curriculum.getWeek() + "-" + weekHalf;

                        //after calculation if timestamp matches with the provided one then do following
                        if(localTimestamp.equalsIgnoreCase(timestamp)){
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                            returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), "");
                            returnDataMap.put(AppConstants.ServicesConstants.TIME_STAMP.name(), localTimestamp);
                        }else{
                            url = curriculumAPI.concat(AppConstants.CURRICULUM_CONTENT_API.replace("$$week$$", curriculum.getWeek()+""));
                            response = NetworkUtils.getRequest(url);
                            JSONObject jsonObject = new JSONObject(response);
                            String learningActivities = jsonObject.getString(LEARNING);

                            if(weekHalf == 2){
                                learningActivities = jsonObject.getString(ACTIVITIES);
                            }else{
                                try {
                                    int index = org.apache.commons.lang.StringUtils.ordinalIndexOf(learningActivities, "-", 4);
                                    learningActivities = learningActivities.substring(0, index);
                                } catch (StringIndexOutOfBoundsException si) {
                                    LOG.info("getCurriculumPinnedMessages service: StringIndexOutOfBoundsException: "+si.getMessage());
                                }
                            }

                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                            returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), learningActivities);
                            returnDataMap.put(AppConstants.ServicesConstants.TIME_STAMP.name(), localTimestamp);
                        }
                    }
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Patient not found.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("getCurriculumPinnedMessages service: Exception: "+e.getMessage());
        }

        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("getCurriculumPinnedMessages service: Returning response: "+response);
        return Response.ok(response).build();
    }

    //[4/01/2015]: Added getGoalPinnedMessages service, __oz.
    @GET
    @Path("/getGoalPinnedMessages")
    public Response getGoalPinnedMessages(@Context HttpServletRequest request) {

        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());

        try {

            String credentials = request.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
            LOG.info("getGoalPinnedMessages service: Provided Data: credentials: " + credentials);
            Map<String, Object> resultMap = CommonUtils.authenticateUser(credentials);
            String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));

            if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {
                LOG.info("getGoalPinnedMessages service: Returning Response: " + status);
                return Response.ok(status).build();
            }else{
                Long patientId = (Long) resultMap.get(AppConstants.JsonConstants.DATA.name());
                if(patientId != null){
                    GoalDTO goal = new FilledFormDAO().getLatestActiveGoalByPatient(patientId);
                    if(goal != null){
                        returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), JsonUtil.toJson(goal));
                    }else{
                        returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), "");
                    }
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Patient not found.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("getGoalPinnedMessages service: Exception: "+e.getMessage());
        }

        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("getGoalPinnedMessages service: Returning response: "+response);
        return Response.ok(response).build();
    }

    //[4/07/2015]: Added getTarget service, __oz.
    @GET
    @Path("/getTarget")
    public Response getTarget(@Context HttpServletRequest request) {

        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());

        try {

            String credentials = request.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
            LOG.info("getTarget service: Provided Data: credentials: " + credentials);
            Map<String, Object> resultMap = CommonUtils.authenticateUser(credentials);
            String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));

            if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {
                LOG.info("getTarget service: Returning Response: " + status);
                return Response.ok(status).build();
            }else{
                Long patientId = (Long) resultMap.get(AppConstants.JsonConstants.DATA.name());
                if(patientId != null){
                    Patient patient = new PatientDAO().getPatientById(patientId);
                    if(patient != null){
                        Target targetFromDB = patient.getTarget();
                        if(targetFromDB != null){
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                            returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), targetFromDB);
                        }
                    }else{
                        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Patient not found.");
                    }
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Patient not found.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("getTarget service: Exception: "+e.getMessage());
        }

        String response = JsonUtil.toJsonExcludedNull(returnDataMap);
        LOG.info("getTarget service: Returning response: "+response);
        return Response.ok(response).build();
    }

    //[04/15/2015]: Added logout service to delete patient device, __oz
    @POST
    @Path("/logout")
    public Response logout(@Context HttpServletRequest request) {
        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        PatientDAO patientDAO = new PatientDAO();
        try {

            String credentials = request.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
            LOG.info("logout service: Provided Data: credentials: " + credentials);

            Map<String, Object> resultMap = CommonUtils.authenticateUser(credentials);
            String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));
            if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), status);
                LOG.info("logout service: Returning Response: " + status);
                return Response.ok(status).build();
            }else{
                long patientId = (Long) resultMap.get(AppConstants.JsonConstants.DATA.name());
                Patient patient = patientDAO.getPatientById(patientId);
                if(patient != null){
                    BaseDAO baseDAO = new BaseDAO();
                    Device device = new DeviceDAO().getDeviceByMac(patient.getDeviceMacAddress());
                    if(device != null){
                        baseDAO.delete(device);
                        patient.setDeviceMacAddress("");
                        baseDAO.save(patient);
                        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                    }
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Patient not found.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("logout service: Exception: "+e.getMessage());
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("logout service: Returning response: "+response);
        return Response.ok(response).build();
    }

    //[05/06/2015]: Added upload motivation image service, __oz
    @POST
    @Path("/uploadMotivationImage")
    public Response uploadMotivationImage(String jsonString, @Context HttpServletRequest httpRequest){

        jsonString = CommonUtils.extractDataFromRequest(jsonString, httpRequest);

        PatientMotivationImageDAO patientMotivationImageDAO = new PatientMotivationImageDAO();
        BaseDAO baseDAO = new BaseDAO();
        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(),AppConstants.ServicesConstants.ERROR.name());
        LOG.info("uploadMotivationImage service: request data jsonString: "+jsonString);
        try {

            PatientMotivationImage patientMotivationImage = (PatientMotivationImage)JsonUtil.fromJson(jsonString, PatientMotivationImage.class);
            if(patientMotivationImage != null){
                Patient patient = patientMotivationImage.getPatient();
                if(patient != null){
                    LOG.info("uploadMotivationImage service: patient motivation image: "+patientMotivationImage.toString());
                    PatientMotivationImage motivationImageFromDB = patientMotivationImageDAO.getPatientMotivationImageByPatientId(patient.getPatientId());
                    Patient patientFromDB = new PatientDAO().getPatientById(patient.getPatientId());
                    String imageName = patientMotivationImage.getImageName();
                    String motivationFolderName = CommonUtils.getProjectPath() + AppConstants.MOTIVATION_IMAGE_FOLDER;
                    patientMotivationImage.setPatient(null);
                    if(motivationImageFromDB == null) {
                        motivationImageFromDB = new PatientMotivationImage();
                        motivationImageFromDB.setPatient(null);
                    }
                    motivationImageFromDB.setDescription(patientMotivationImage.getDescription());
                    motivationImageFromDB.setPatient(patientFromDB);
                    if(!ObjectUtils.isEmpty(patientMotivationImage.getImageContent())){
                        boolean isSaved = CommonUtils.saveImageToDisc(motivationFolderName, imageName, patientMotivationImage.getImageContent());
                        if(isSaved){
                            motivationImageFromDB.setImageName(imageName);
                            LOG.info("uploadMotivationImage service: upload motivation image successfully");
                            motivationImageFromDB.setIsPublic(patientMotivationImage.getIsPublic());
                            baseDAO.save(motivationImageFromDB);
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                            returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), JsonUtil.toJsonExcludedNull(motivationImageFromDB));
                        }else{
                            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Unable to save motivation image.");
                        }
                    }else{
                        LOG.info("uploadMotivationImage service: description saved successfully");
                        motivationImageFromDB.setIsPublic(patientMotivationImage.getIsPublic());
                        baseDAO.save(motivationImageFromDB);
                        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                        returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), JsonUtil.toJsonExcludedNull(motivationImageFromDB));
                    }
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Patient not found.");
                }
            }else{
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Unable to parse json.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("uploadMotivationImage service: Exception: "+e.getMessage());
            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(),AppConstants.ResponseErrorMessages.ERROR_MESSAGE.getValue());
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("uploadMotivationImage service: Returning response: "+response);
        return Response.ok(response).build();
    }

    //[05/07/2015]: Added delete motivation image service, __oz
    @POST
    @Path("/deleteMotivationImage")
    public Response deleteMotivationImage(String jsonString, @Context HttpServletRequest request) {

        jsonString = CommonUtils.extractDataFromRequest(jsonString, request);

        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        PatientDAO patientDAO = new PatientDAO();
        try {
            LOG.info("deleteMotivationImage service: Provided Data: id: " + jsonString);
            if (!ObjectUtils.isEmpty(jsonString)) {
                long patientId = Long.parseLong(jsonString);
                Patient patient = patientDAO.getPatientById(patientId);
                if(patient != null){
                    PatientMotivationImage motivationImageFromDB = new PatientMotivationImageDAO().getPatientMotivationImageByPatientId(patient.getPatientId());
                    if(motivationImageFromDB != null){
                        String imageName = motivationImageFromDB.getImageName();
                        String motivationFolderName = CommonUtils.getProjectPath() + AppConstants.MOTIVATION_IMAGE_FOLDER;
                        boolean isDeleted = CommonUtils.deleteFileFromDisc(motivationFolderName, imageName);
                        if(isDeleted){
                            LOG.info("deleteMotivationImage service: delete motivation image successfully");
                            new PatientMotivationImageDAO().removePatientMotivationImageByPatientId(patient.getPatientId());
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                        }else{
                            LOG.info("deleteMotivationImage service: unable to delete motivation image");
                            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Unable to delete motivation image.");
                        }
                    }else{
                        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                    }
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Patient not found.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("deleteMotivationImage service: Exception: "+e.getMessage());
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("deleteMotivationImage service: Returning response: "+response);
        return Response.ok(response).build();
    }

    //[05/07/2015]: Added upload profile image service, __oz
    @POST
    @Path("/uploadProfileImage")
    public Response uploadProfileImage(String jsonString, @Context HttpServletRequest httpRequest){

        jsonString = CommonUtils.extractDataFromRequest(jsonString, httpRequest);

        BaseDAO baseDAO = new BaseDAO();
        PatientDAO patientDAO = new PatientDAO();
        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        try {

            String credentials = httpRequest.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
            LOG.info("uploadProfileImage service: Provided Data: credentials: " + credentials);

            Map<String, Object> resultMap = CommonUtils.authenticateUser(credentials);
            String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));
            if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), status);
                LOG.info("uploadProfileImage service: Returning Response: " + status);
                return Response.ok(status).build();
            }else{
                long patientId = (Long) resultMap.get(AppConstants.JsonConstants.DATA.name());
                Patient patient = patientDAO.getPatientById(patientId);
                if(patient != null){
                    String profileImagesFolder = CommonUtils.getProjectPath() + AppConstants.PROFILE_IMAGE_FOLDER;
                    String fileName = ObjectUtils.getImageName("PA", patient.getUuid());
                    String profileImagePath = AppConstants.PROFILE_IMAGE_FOLDER + fileName;
                    boolean isSaved = CommonUtils.saveImageToDisc(profileImagesFolder, fileName, jsonString);
                    if(isSaved){
                        LOG.info("uploadProfileImage service: profile image saved successfully");
                        patient.setImagePath(profileImagePath);
                        baseDAO.save(patient);
                        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                        returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), profileImagePath);
                    }else{
                        LOG.info("uploadProfileImage service: unable to save profile image");
                        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Unable to save profile image.");
                    }
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Patient not found.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("uploadProfileImage service: Exception: "+e.getMessage());
            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(),AppConstants.ResponseErrorMessages.ERROR_MESSAGE.getValue());
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("uploadProfileImage service: Returning response: "+response);
        return Response.ok(response).build();
    }

    //[05/07/2015]: Added get profile image service, __oz
    @GET
    @Path("/getProfileImage")
    public Response getProfileImage(@QueryParam("userName") String userName, @Context HttpServletRequest httpRequest) {
        InputStream inputStream = null;
        byte[] imageBytes = null;
        @SuppressWarnings("unused")
        String contentType = "image/png";
        try {
            LOG.info("getProfileImage Service: requesting for user profile image: "+userName);
            File file = new File(httpRequest.getServletContext().getRealPath("/resources/css/images/default_profile_image.gif"));
            User user = new UserDAO().getUserByEmail(userName);
            if(user != null){
                Patient patient = user.getPatient();
                if(patient != null && !ObjectUtils.isEmpty(patient.getImagePath())){
                    String profileImagesFolder = CommonUtils.getProjectPath() + patient.getImagePath();
                    file = new File(profileImagesFolder);
                }
            }
            if(file.exists()){
                imageBytes = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
            }

            return Response.ok(imageBytes, MediaType.APPLICATION_OCTET_STREAM).build();

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("getProfileImage Service: exception in getting image from elgg:  "+e.getMessage());
        }

        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Unable to get image.");
        LOG.info("getProfileImage Service: returning response, unable to get image");
        return Response.ok(returnDataMap).build();
    }

    //[05/15/2015]: Added upload session preference service, __oz
    @POST
    @Path("/uploadSessionPreference")
    public Response uploadSessionPreference(String jsonString, @Context HttpServletRequest httpRequest){

        jsonString = CommonUtils.extractDataFromRequest(jsonString, httpRequest);

        BaseDAO baseDAO = new BaseDAO();
        PatientDAO patientDAO = new PatientDAO();
        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        try {

            String credentials = httpRequest.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
            LOG.info("uploadSessionPreference service: Provided Data: credentials: " + credentials);

            Map<String, Object> resultMap = CommonUtils.authenticateUser(credentials);
            String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));
            if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), status);
                LOG.info("uploadSessionPreference service: Returning Response: " + status);
                return Response.ok(status).build();
            }else{

                PatientSessionPreferenceDTO patientSessionPreferenceDTO = (PatientSessionPreferenceDTO)JsonUtil.fromJson(jsonString, PatientSessionPreferenceDTO.class);

                if(patientSessionPreferenceDTO != null){
                    long patientId = patientSessionPreferenceDTO.getPatientId();
                    Patient patient = patientDAO.getPatientById(patientId);
                    if(patient != null){
                        PatientSessionPreference patientSessionPreference = new PatientSessionPreferenceDAO().getSessionPreferenceByPatientId(patient.getPatientId());
                        if(patientSessionPreference == null){
                            patientSessionPreference = new PatientSessionPreference();
                        }
                        List<String> daysList = patientSessionPreferenceDTO.getDaysList();
                        List<String> timesList = patientSessionPreferenceDTO.getTimesList();

                        if(daysList != null && daysList.size() > 0){
                            String days = CommonUtils.listToCommaSeparatedString(daysList, ", ");
                            patientSessionPreference.setDays(days);
                        }

                        if(timesList != null && timesList.size() > 0){
                            String times = CommonUtils.listToCommaSeparatedString(timesList, ", ");
                            patientSessionPreference.setTimes(times);
                        }

                        patient.setPatientSessionPreference(patientSessionPreference);
                        patientSessionPreference.setPatient(patient);
                        baseDAO.save(patientSessionPreference);
                        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                    }else{
                        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Patient not found.");
                    }
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Unable to parse.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("uploadSessionPreference service: Exception: "+e.getMessage());
            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(),AppConstants.ResponseErrorMessages.ERROR_MESSAGE.getValue());
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("uploadSessionPreference service: Returning response: "+response);
        return Response.ok(response).build();
    }

    //[05/15/2015]: Added upload eating preference service, __oz
    @POST
    @Path("/uploadEatingPreference")
    public Response uploadEatingPreference(String jsonString, @Context HttpServletRequest httpRequest){

        jsonString = CommonUtils.extractDataFromRequest(jsonString, httpRequest);

        BaseDAO baseDAO = new BaseDAO();
        PatientDAO patientDAO = new PatientDAO();
        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        try {

            String credentials = httpRequest.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
            LOG.info("uploadEatingPreference service: Provided Data: credentials: " + credentials);

            Map<String, Object> resultMap = CommonUtils.authenticateUser(credentials);
            String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));
            if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), status);
                LOG.info("uploadEatingPreference service: Returning Response: " + status);
                return Response.ok(status).build();
            }else{

                PatientEatingPreference patientEatingPreference = (PatientEatingPreference)JsonUtil.fromJson(jsonString, PatientEatingPreference.class);

                if(patientEatingPreference != null){
                    long patientId = (Long) resultMap.get(AppConstants.JsonConstants.DATA.name());
                    Patient patient = patientDAO.getPatientById(patientId);
                    if(patient != null){

                        PatientEatingPreference patientEatingPreferenceFromDB = new PatientEatingPreferenceDAO().getEatingPreferenceByPatientId(patient.getPatientId());

                        if(patientEatingPreferenceFromDB == null){
                            patientEatingPreferenceFromDB = new PatientEatingPreference();
                        }

                        patientEatingPreferenceFromDB.setRestriction(patientEatingPreference.getRestriction());

						//default settings if mobile app don't chose anything, so clear old ones
						patientEatingPreferenceFromDB.setIsAlcohol(false);
						patientEatingPreferenceFromDB.setAlcoholType("");
						patientEatingPreferenceFromDB.setIsColdBeverage(false);
						patientEatingPreferenceFromDB.setColdBeverageType("");
						patientEatingPreferenceFromDB.setIsHotBeverage(false);
						patientEatingPreferenceFromDB.setHotBeverageTeaspoonAdded("");
						patientEatingPreferenceFromDB.setHotBeverageSize("");

                        if(patientEatingPreference.getIsAlcohol()){
                            patientEatingPreferenceFromDB.setIsAlcohol(true);
                            patientEatingPreferenceFromDB.setAlcoholType(patientEatingPreference.getAlcoholType());
                        }

                        if(patientEatingPreference.getIsColdBeverage()){
                            patientEatingPreferenceFromDB.setIsColdBeverage(true);
                            patientEatingPreferenceFromDB.setColdBeverageType(patientEatingPreference.getColdBeverageType());
                        }

                        if(patientEatingPreference.getIsHotBeverage()){
                            patientEatingPreferenceFromDB.setIsHotBeverage(true);
                            patientEatingPreferenceFromDB.setHotBeverageTeaspoonAdded(patientEatingPreference.getHotBeverageTeaspoonAdded());
                            patientEatingPreferenceFromDB.setHotBeverageSize(patientEatingPreference.getHotBeverageSize());
                        }

                        patient.setPatientEatingPreference(patientEatingPreferenceFromDB);
                        patientEatingPreferenceFromDB.setPatient(patient);
                        baseDAO.save(patientEatingPreferenceFromDB);
                        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                    }else{
                        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Patient not found.");
                    }
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), "Unable to parse.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("uploadEatingPreference service: Exception: "+e.getMessage());
            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(),AppConstants.ResponseErrorMessages.ERROR_MESSAGE.getValue());
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("uploadEatingPreference service: Returning response: "+response);
        return Response.ok(response).build();
    }

    //[07/13/2015]: Added getEatingAndSessionPreferences service, __oz
    @GET
    @Path("/getEatingAndSessionPreferences")
    public Response getEatingAndSessionPreferences(@Context HttpServletRequest httpRequest) {

        PatientDAO patientDAO = new PatientDAO();
        Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());

        try {

            String credentials = httpRequest.getHeader(AppConstants.UserInfo.AUTHORIZATION.getValue());
            LOG.info("getEatingAndSessionPreferences service: Provided Data: credentials: " + credentials);

            Map<String, Object> resultMap = CommonUtils.authenticateUser(credentials);
            String status = String.valueOf(resultMap.get(AppConstants.JsonConstants.STATUS.name()));
            if (status.equalsIgnoreCase(AppConstants.ACCESS_DENIED)) {
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), status);
                LOG.info("getEatingAndSessionPreferences service: Returning Response: " + status);
                return Response.ok(status).build();
            }else{
                long patientId = (Long) resultMap.get(AppConstants.JsonConstants.DATA.name());
                Patient patient = patientDAO.getPatientById(patientId);
                PatientEatingPreference patientEatingPreference = patient.getPatientEatingPreference();
                PatientSessionPreference patientSessionPreference = patient.getPatientSessionPreference();

                if(patientEatingPreference != null){
                    returnDataMap.put(AppConstants.ServicesConstants.EATING_PREFERENCE.name(), patientEatingPreference);
                }

                if(patientSessionPreference != null){
                    returnDataMap.put(AppConstants.ServicesConstants.SESSION_PREFERENCE.name(), patientSessionPreference);
                }

                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("getLoginFacilities Service: Exception:  "+e.getMessage());
        }

        String response = JsonUtil.toJsonExcludedNull(returnDataMap);
        LOG.info("getLoginFacilities service: Returning response: "+response);
        return Response.ok(response).build();
    }

	protected User loadUserFromSecurityContext(SecurityContext securityContext) {

		OAuth2Authentication requestingUser = (OAuth2Authentication) (securityContext).getUserPrincipal();
		String principal = requestingUser.getUserAuthentication().getName();
		User user = null;
		user = new UserDAO().getUserPatientByUsername(principal);

		return user;
	}


	protected  Map<String, Object> authenticateUser(User user) {
		long patientId = 0;
		Map<String, Object> returnMap = new HashMap<String, Object>();
		returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.ACCESS_DENIED);
		returnMap.put(AppConstants.JsonConstants.DATA.name(), patientId);

		if(user == null){
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.ACCESS_NOT_FOUND);
		}else{
			patientId = user.getPatient().getPatientId();
			returnMap.put(AppConstants.JsonConstants.DATA.name(), patientId);
			returnMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.ACCESS_GRANTED);
		}

		return returnMap;
	}
}
