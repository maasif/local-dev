package com.healthslate.patientapp.ws.v2;

import com.amazonaws.util.json.JSONObject;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import com.healthslate.patientapp.ws.AuthServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.*;

/*
 * ======= FILE CHANGE HISTORY =======
 * [07/29/2015]: Created by __oz
 * ===================================
 */


@Path("/signUp/v2")
public class FirstRunServices extends com.healthslate.patientapp.ws.FirstRunServices {
	static final Logger LOG = LoggerFactory.getLogger(FirstRunServices.class);

	@POST
	@Path("/verifyEmail")
	public Response emailVerify(String emailString, @Context HttpServletRequest httpRequest) {
		Map<String, Object> returnDataMap = new LinkedHashMap<String, Object>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
		UserDAO userDao = new UserDAO();
		User user = new User();
        ServerInfoDAO serverInfoDAO = new ServerInfoDAO();
        ServerInfo currentServerInfo = AuthServices.getCurrentServerInfo(httpRequest);
		emailString = CommonUtils.extractDataFromRequest(emailString, httpRequest);

		LOG.info("v2/verifyEmail service: email to verify: "+emailString);

        String email = emailString;

		if(!ObjectUtils.isEmpty(emailString)) {

            //if force email address then go direct to that server
            if(email.contains("#")) {

                //holds email and server name without hashed
                RequestedEmailServer requestedEmailServer = CommonUtils.extractEmailAndServerFromHashedString(email);

                //query DB for serverInfo
                ServerInfo forcedServerInfo = serverInfoDAO.getServerInfoByName(requestedEmailServer.getServer());

                if (forcedServerInfo != null) {
                    try {
                        //if forced server is current server then don't force switch
                        if(forcedServerInfo.getServerName().equalsIgnoreCase(currentServerInfo.getServerName())){
                            return sendResponseToMemberApp(requestedEmailServer.getEmail(), returnDataMap, httpRequest);
                        }else{
                            String urlToHit = forcedServerInfo.getServerURL() + "/services/auth/lookForUser";
                            Map<String, String> headerEntries = new LinkedHashMap<String, String>();
                            headerEntries.put(AppConstants.UserInfo.SP_EMAIL.getValue(), new EncryptionUtils().encrypt(requestedEmailServer.getEmail()));
                            String apiResponse = NetworkUtils.postRequest(urlToHit, headerEntries);
                            LOG.info("v2/verifyEmail service: Returning response: " + apiResponse);
                            return Response.ok(apiResponse).build();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.SERVER_INFO_ERROR);
                    String response = JsonUtil.toJsonExcludedNull(returnDataMap);
                    LOG.info("auth/login service: server not found, server name is incorrect, returning response: " + response);
                    return Response.ok(response).build();
                }
            } else if(userDao.isUserExist(emailString)) {
                return sendResponseToMemberApp(emailString, returnDataMap, httpRequest);
			} else {
                //list all servers except current one, because we already checked in current server
                List<ServerInfo> servers = serverInfoDAO.listAll(currentServerInfo);
                String apiResponse = null;
                for(ServerInfo info: servers) {
                    String urlToHit = info.getServerURL() + "/services/auth/lookForUser";
                    Map<String, String> headerEntries = new LinkedHashMap<String, String>();
                    try {
                        headerEntries.put(AppConstants.UserInfo.SP_EMAIL.getValue(), new EncryptionUtils().encrypt(emailString));
                        apiResponse = NetworkUtils.postRequest(urlToHit, headerEntries);
                        if(!ObjectUtils.isEmpty(apiResponse) && JsonUtil.isValidJSON(apiResponse)) {
                            JSONObject jsonObject = new JSONObject(apiResponse);
                            String resultStatus = jsonObject.getString(AppConstants.ServicesConstants.STATUS.name());
                            // if we have found user any of the server break loop and return response to app
                            if (resultStatus.equalsIgnoreCase(AppConstants.ServicesConstants.SUCCESS.name()) || resultStatus.equalsIgnoreCase(AppConstants.EMAIL_ALREADY_EXIST)) {
                                break;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if(ObjectUtils.isEmpty(apiResponse) || apiResponse.equalsIgnoreCase(AppConstants.ACCESS_DENIED)){
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.EMAIL_NOT_REGISTERED);
                    apiResponse = JsonUtil.toJsonExcludedNull(returnDataMap);
                }

                LOG.info("v2/verifyEmail service: Returning response: " + apiResponse);
                return Response.ok(apiResponse).build();
			}
		} else {
			returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL);
		}
		String response = JsonUtil.toJsonExcludedNull(returnDataMap);
		LOG.info("v2/verifyEmail service: Returning response: "+response);
		return Response.ok(response).build();
	}

    public Response sendResponseToMemberApp(String emailString, Map<String, Object> returnDataMap, HttpServletRequest request){
        try {
            UserDAO userDao = new UserDAO();
            User user = userDao.getUserByEmail(emailString);
            ServerInfo serverInfo = AuthServices.getCurrentServerInfo(request);
            if(user == null){
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.EMAIL_NOT_REGISTERED);
            }else{
                if(user.getIsRegistrationCompleted() != null && user.getIsRegistrationCompleted()) {
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.EMAIL_ALREADY_EXIST);
                }else {
                    Patient patient = user.getPatient();
                    if (patient != null && user.getUserType().equalsIgnoreCase(AppConstants.PATIENT)) {
                        boolean isConsentAccepted = ObjectUtils.nullSafe(patient.getIsConsentAccepted());

                        if (!isConsentAccepted) {
                            LOG.info("v2/verifyEmail service: Member not yet accepted TOS from Web: " + user.toString());
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.NOT_APPROVED_MESSAGE);
                            String response = JsonUtil.toJsonExcludedNull(returnDataMap);
                            LOG.info("v2/verifyEmail service: TOS not accepted, returning response: " + response);
                            return Response.ok(response).build();
                        }

                        if (user.getIsRegistrationCompleted() != null && user.getIsRegistrationCompleted()) {
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.EMAIL_ALREADY_EXIST);
                        } else {
                            // [0, 1] means request is in process not approved yet
                            // 2 means denied by admin
                            // 3 means approved by admin
                            String approvedStatus = ObjectUtils.nullSafe(patient.getIsApproved());

                            if (approvedStatus.equalsIgnoreCase(AppConstants.REQ_IN_PROCESS_0) || approvedStatus.equalsIgnoreCase(AppConstants.REQ_IN_PROCESS_1)) {
                                LOG.info("v2/verifyEmail service: Member not yet approved: " + user.toString());
                                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.NOT_APPROVED_MESSAGE);
                            } else if (approvedStatus.equalsIgnoreCase(AppConstants.REQ_DENIED)) {
                                LOG.info("v2/verifyEmail service: Member denied by admin: " + user.toString());
                                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR);
                                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.DENIED_REQUEST_MESSAGE);
                            } else {
                                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS);
                            }
                        }

                        returnDataMap.put(AppConstants.ServicesConstants.SERVER_INFO.name(), serverInfo);
                    } else {
                        returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL_PASSWORD);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        String response = JsonUtil.toJsonExcludedNull(returnDataMap);
        LOG.info("v2/verifyEmail service: Returning response: "+response);
        return Response.ok(response).build();
    }
}