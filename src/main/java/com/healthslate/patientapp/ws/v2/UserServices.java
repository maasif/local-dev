package com.healthslate.patientapp.ws.v2;

import com.amazonaws.util.json.JSONObject;
import com.healthslate.patientapp.model.dao.*;
import com.healthslate.patientapp.model.dto.CoachDTO;
import com.healthslate.patientapp.model.entity.*;
import com.healthslate.patientapp.util.*;
import com.healthslate.patientapp.ws.AuthServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.codec.Base64;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/*
 * ======= FILE CHANGE HISTORY =======
 * [07/28/2015]: Created by __oz
 * ===================================
 */

@Path("/user/v2")
public class UserServices extends com.healthslate.patientapp.ws.UserServices {
	static final Logger LOG = LoggerFactory.getLogger(UserServices.class);
	private PreferencesDAO preferencesDAO = new PreferencesDAO();

	@POST
	@Path("/forgotPassword")
	public Response forgotPassword(String emailString, @Context HttpServletRequest httpRequest) {

        emailString = CommonUtils.extractDataFromRequest(emailString, httpRequest);

		Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
		returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
		UserDAO userDao = new UserDAO();
		EmailTokenDAO emailTokenDAO = new EmailTokenDAO();
		Map<String, String> prefMap = preferencesDAO.map();
        ServerInfoDAO serverInfoDAO = new ServerInfoDAO();
        ServerInfo currentServerInfo = AuthServices.getCurrentServerInfo(httpRequest);
		LOG.info("v2/forgotPassword service: Provided Data: emailString: "+emailString);
		try {

            String email = emailString;

            //if force email address then go direct to that server
            if(email.contains("#")) {

                //holds email and server name without hashed
                RequestedEmailServer requestedEmailServer = CommonUtils.extractEmailAndServerFromHashedString(email);

                //query DB for serverInfo
                ServerInfo forcedServerInfo = serverInfoDAO.getServerInfoByName(requestedEmailServer.getServer());

                if (forcedServerInfo != null) {
                    //if forced server is current server then don't force switch
                    if(forcedServerInfo.getServerName().equalsIgnoreCase(currentServerInfo.getServerName())){
                        returnDataMap = sendForgotPasswordEmail(requestedEmailServer.getEmail(), httpRequest);
                        String response = JsonUtil.toJson(returnDataMap);
                        LOG.info("v2/forgotPassword service: Returning response: "+response);
                        return Response.ok(response).build();
                    }else{
                        String urlToHit = forcedServerInfo.getServerURL() + "/services/auth/lookForUser";
                        Map<String, String> headerEntries = new LinkedHashMap<String, String>();
                        headerEntries.put(AppConstants.UserInfo.SP_EMAIL.getValue(), new EncryptionUtils().encrypt(requestedEmailServer.getEmail()));
                        String apiResponse = NetworkUtils.postRequest(urlToHit, headerEntries);
                        if(JsonUtil.isValidJSON(apiResponse)){
                            JSONObject jsonObject = new JSONObject(apiResponse);
                            String resultStatus = jsonObject.getString(AppConstants.ServicesConstants.STATUS.name());
                            // if we have found user any of the server break loop and return response to app
                            if(resultStatus.equalsIgnoreCase(AppConstants.ServicesConstants.SUCCESS.name()) || resultStatus.equalsIgnoreCase(AppConstants.EMAIL_ALREADY_EXIST)){
                                headerEntries = new LinkedHashMap<String, String>();
                                headerEntries.put(AppConstants.UserInfo.SP_EMAIL.getValue(), new EncryptionUtils().encrypt(requestedEmailServer.getEmail()));
                                headerEntries.put(AppConstants.UserInfo.SP_SERVER_INFO.getValue(), JsonUtil.toJson(forcedServerInfo));
                                String responseFromEmailService = NetworkUtils.postRequest(forcedServerInfo.getServerURL() + "/services/broadcast/sendEmail", headerEntries);
                                LOG.info("v2/forgotPassword service: Returning response: "+responseFromEmailService);
                                return Response.ok(responseFromEmailService).build();
                            }else{
                                LOG.info("v2/forgotPassword service: API Response: " + apiResponse);
                                return Response.ok(apiResponse).build();
                            }
                        }else{
                            LOG.info("v2/forgotPassword service: API response: " + apiResponse);
                            returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL_PASSWORD);
                            String response = JsonUtil.toJsonExcludedNull(returnDataMap);
                            LOG.info("v2/forgotPassword service: Returning Response: " + response);
                            return Response.ok(response).build();
                        }
                    }
                }else{
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.SERVER_INFO_ERROR);
                    String response = JsonUtil.toJsonExcludedNull(returnDataMap);
                    LOG.info("auth/login service: server not found, server name is incorrect, returning response: " + response);
                    return Response.ok(response).build();
                }
            }else if (userDao.isUserExist(emailString)) {
                returnDataMap = sendForgotPasswordEmail(emailString, httpRequest);
			} else {
                //list all servers except current one, because we already checked in current server
                List<ServerInfo> servers = serverInfoDAO.listAll(currentServerInfo);
                String apiResponse = null;
                for(ServerInfo info: servers){
                    String urlToHit = info.getServerURL() + "/services/auth/lookForUser";
                    Map<String, String> headerEntries = new LinkedHashMap<String, String>();
                    headerEntries.put(AppConstants.UserInfo.SP_EMAIL.getValue(), new EncryptionUtils().encrypt(emailString));
                    apiResponse = NetworkUtils.postRequest(urlToHit, headerEntries);
                    if(!ObjectUtils.isEmpty(apiResponse) && JsonUtil.isValidJSON(apiResponse)) {
                        JSONObject jsonObject = new JSONObject(apiResponse);
                        String resultStatus = jsonObject.getString(AppConstants.ServicesConstants.STATUS.name());
                        // if we have found user any of the server break loop and return response to app
                        if(resultStatus.equalsIgnoreCase(AppConstants.ServicesConstants.SUCCESS.name()) || resultStatus.equalsIgnoreCase(AppConstants.EMAIL_ALREADY_EXIST)){
                            headerEntries = new LinkedHashMap<String, String>();
                            headerEntries.put(AppConstants.UserInfo.SP_EMAIL.getValue(), new EncryptionUtils().encrypt(emailString));
                            headerEntries.put(AppConstants.UserInfo.SP_SERVER_INFO.getValue(), JsonUtil.toJson(info));
                            apiResponse = NetworkUtils.postRequest(info.getServerURL() + "/services/broadcast/sendEmail", headerEntries);
                            break;
                        }
                    }
                }

                if(ObjectUtils.isEmpty(apiResponse) || apiResponse.equalsIgnoreCase(AppConstants.ACCESS_DENIED)){
                    returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                    returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL_PASSWORD);
                    apiResponse = JsonUtil.toJsonExcludedNull(returnDataMap);
                }

                LOG.info("v2/forgotPassword service: Returning response: "+apiResponse);
                return Response.ok(apiResponse).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.info("v2/forgotPassword service: Exception: " + e.getMessage());
            returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.INVALID_EMAIL_PASSWORD);
		}
		String response = JsonUtil.toJson(returnDataMap);
		LOG.info("v2/forgotPassword service: Returning response: "+response);
		return Response.ok(response).build();
	}

    @POST
    @Path("/saveShareMealPermissions")
    public Response saveShareMealPermissions(String shareMealPermissionsJsonString, @Context HttpServletRequest httpRequest) {
        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
        try {
            LOG.info("saveShareMealPermissions service: Provided Data: shareMealPermissionsJsonString: " + shareMealPermissionsJsonString);
            LogDAO logDAO = new LogDAO();
            BaseDAO baseDAO = new BaseDAO();
            ShareMealPermissionsDAO shareMealPermissionsDAO = new ShareMealPermissionsDAO();
            JSONObject obj = new JSONObject(shareMealPermissionsJsonString);
            String logId = obj.getString("logId");
            String permissions = obj.getString("permissions");

            if (!logId.isEmpty() && !permissions.isEmpty()) {
                Log log = logDAO.getLogById(logId);
                ShareMealPermissions smp;

                smp = shareMealPermissionsDAO.getShareMealPermissionsByLogId(log.getLogId());

                smp.setPermissionsGrantedDate(System.currentTimeMillis());
                smp.setPermissions(permissions);
                smp.setLogId(log.getLogId());

                baseDAO.save(smp);
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
            }

        } catch (Exception e) {
            e.printStackTrace();
            LOG.info("saveShareMealPermissions service: Provided Data: Exception: " + e.getMessage());
        }
        String response = JsonUtil.toJson(returnDataMap);
        LOG.info("saveShareMealPermissions service: Returning Response: " + response);
        return Response.ok(response).build();
    }

    private Map<String, String> sendForgotPasswordEmail(String emailString, HttpServletRequest httpRequest){
        Map<String, String> returnDataMap = new LinkedHashMap<String, String>();
        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());

        try {
            UserDAO userDao = new UserDAO();
            EmailTokenDAO emailTokenDAO = new EmailTokenDAO();
            Map<String, String> prefMap = preferencesDAO.map();

            User user = userDao.getUserByEmail(emailString);
            if(user == null){
                returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.ERROR.name());
                returnDataMap.put(AppConstants.ServicesConstants.REASON.name(), AppConstants.EMAIL_NOT_REGISTERED);
            }else{
                if(ObjectUtils.nullSafe(user.getIsRegistrationCompleted())) {
                    EmailToken emailToken = new EmailToken();
                    emailToken.setUsername(user.getEmail());

                    String token = user.getEmail() + System.currentTimeMillis();
                    token = new String(Base64.encode(token.getBytes()));
                    emailToken.setToken(token);

                    emailToken.setDateCreated(new Date());
                    emailTokenDAO.saveEmailToken(emailToken);

                    String resetPasswordUrl = ObjectUtils.getApplicationUrl(httpRequest);

                    resetPasswordUrl += "/app/resetPassword.action?token=" + emailToken.getToken();
                    String emailText = CommunicationUtils.getEmailTextForResetPassword(user.getEmail(), resetPasswordUrl);

                    boolean isSentEmail = EmailUtils.sendTextInEmail(emailText,
                            user.getEmail(),
                            prefMap.get(AppConstants.PreferencesNames.EMAIL_ID.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.USERNAME.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.PASS.getValue()),
                            prefMap.get(AppConstants.PreferencesNames.HOST.getValue()),
                            AppConstants.EmailSubjects.RESET_PASSWORD.getValue());
                    LOG.info("v2/forgotPassword service: Email sent to: "+user.getEmail()+", with email Text: "+emailText+", and email sent status: "+isSentEmail);
                    if (isSentEmail) {
                        returnDataMap.put(AppConstants.ServicesConstants.STATUS.name(), AppConstants.ServicesConstants.SUCCESS.name());
                        returnDataMap.put(AppConstants.ServicesConstants.DATA.name(), "An email has been sent to your email address.");
                    } else {
                        returnDataMap.put(AppConstants.JsonConstants.STATUS.name(), AppConstants.JsonConstants.ERROR.name());
                        returnDataMap.put(AppConstants.JsonConstants.REASON.name(), "Unable to send email at the moment.");
                    }
                } else {
                    returnDataMap.put(AppConstants.JsonConstants.REASON.name(), "Your sign up process is not completed.");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnDataMap;
    }
}
