<!doctype html>
<html>
<head>
	<title>Access Denied</title>
	<meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0" />
	<meta http-equiv="x-ua-compatible" content="IE=Edge"/>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/header.style.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation.min.css" />
    <script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
</head>

<body>

<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid padding-right-2px">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <s:a><img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" style="top:9px;left:-5px;position:relative;width:200px" /></s:a>
        </div>
    </div>
</nav>

<div class="access-denied">
    <div class="inner-access">
        <div class="row" role="alert">
            <div class="columns medium-12">
                <h3>Access denied</h3>
                <div class="error-message">
                    <h4>Access to the requested page has been denied. </h4>
                    <p>Please contact the administrator if there has been an error.</p>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>