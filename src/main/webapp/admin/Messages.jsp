<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>
	<title>My Patients</title>
	<style type="text/css">
		td {
		    border: 1px solid #ccc !important;
		}
		body{
			background-color: #eee;
		}
		textarea{resize:vertical;}
		.main-page{
			box-shadow:none;
			border-radius:2px;
		}
	</style>
</head>
<body>
	<br/><br/>
	
	<div class="new-group-style">
		<button onclick="createMessage()" class="border-radius-3p">Create Message</button>
	</div>
	
	<div class="breadcrumb-top align-breadcrumb">
		<ul class="breadcrumbs">  
		  	<li>  
		    	<a href="viewDashBoard.action">Dashboard</a>  
		  	</li> 
	  		<li class="current">Messages</li>
		</ul>
	</div>
	
	<div id="infoSuccess" class=" alert success alert-message"></div>
	<div id="infoError" class=" alert error alert-message"></div>
	
	<div id="tableDiv" class="main-page" style="width:92%;margin-bottom:20px;">
		<div>
			<label class="log-header">Messages</label>
		</div>
		<display:table name="messageDTOs" requestURI="" class="table" uid="row">
			<display:setProperty name="basic.empty.showtable" value="true" />
			
			<display:column title="Group Name" property="groupName" class="width-10-right-align"/>
			<display:column title="User Display Name" property="userDisplayName" class="width-20-perc"/>
			<display:column title="Message" property="message" class="width-10-right-align"/>
			<display:column title="Time" property="time" class="width-10-right-align"/>
			
		</display:table>
		
	</div>
	<s:actionerror />
	<s:form id="sForm" theme="simple" action="SendMessage.action">
		<div id="messageModal" class="modal hide-dialog">
		    <div class="modal-dialog color-white">
		        <div class="tent">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal">x</button>
		                <h4 class="modal-title">Messages</h4>
		            </div>
		            <div class="modal-body padding-bottom-0">
	        			<div>
	        				<ul class="button-group">
		        				<li><label class="item-style inline">	Group Name:</label></li>
		        				<li class="margin-left-44p"><s:select list="groupMap" id="groupSelector" cssClass="color-black"></s:select><li>
	        				</ul>
	        				<ul class="button-group">
		        				<li><label class="item-style inline">	User Name:</label></li>
		        				<li class="margin-left-44p"><s:select list="UserMap" name="sendTo" id ="userSelector" cssClass="color-black"></s:select></li>
	        				</ul>
	        				<ul class="button-group">
		        				<li><label class="item-style inline ">	Message    :</label></li>
		        				<li class="margin-left-5p">
		        					<s:textarea id="messageContents" name="messageContents" cssClass="text-area-style"></s:textarea>
		        				</li>
	        				</ul>
		            	</div>
		            </div>
		            
		            <div class="align-center new-group-success"></div>
	            	<div class="align-center new-group-error"></div>
	            	
		            <div class="modal-footer footer-style">
		                <button type="submit" class="modal-butt button radius margin-bottom-0 saveFoodBtn">Send</button>
		                <button type="button" class="button radius secondary margin-bottom-0" data-dismiss="modal">Close</button>
		            </div>
		        </div>
		    </div>
		</div>
		<input type="hidden"  id="reciverUserNameKey" name="reciverUserNameKey" >
		<input type="hidden"  id="GroupIdTogetUser" name="GroupIdTogetUser" >
		
	</s:form>
	
	
	
	<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
 	<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>
	
	<script type="text/javascript">
		$(function(){
			
			$('#messagesTab').addClass('selected-tab');
			
			if("${statusMessage}".indexOf("error") != -1){
				$("#infoError").html("${statusMessage}");
				$("#infoError").slideDown();
				hideElement($("#infoError"));
			}
			else if("${statusMessage}".indexOf("successfully") != -1){
				$("#infoSuccess").html("${statusMessage}");
				$("#infoSuccess").slideDown();
				hideElement($("#infoSuccess"));
			}
			try {
				$.fn.dataTableExt.sErrMode = 'throw';
				initDataTable("row",{"sorting": [[0, "asc" ]]});
			}
			catch(err) {console.log(err);}
			$('.row').css('max-width','100%');
		});
		
		function hideElement(element){
			setTimeout(function(){
				$(element).slideUp("slow");
			}, 10000);
		}
		
		function createMessage(){
			
			$("#messageContents").val("");
			$("#messageModal").modal('show');
			
		}
		$("#groupSelector").change(function () {
           var selected = $("#groupSelector").val();
         /*   alert($('#groupSelector option:selected').text());
           $("#sendTo").val($('#groupSelector option:selected').text()); */
          $("#GroupIdTogetUser").val($("#groupSelector").val());
			populateUserSelector(selected)
        });
		
		$("#userSelector").change(function () {
	           var selected = $("#userSelector").val();
	          
	            $("#reciverUserNameKey").val(selected); 
				
	        });
		
		function populateUserSelector(selected){
			 var actionUrl = "PopulateUserSelector.action"
					, dataToSend = {
						"GroupIdTogetUser":selected,
						
					}
				 	, postRequestActions = {
						"requestType" : "GET",
						"successCallBack" : UserSelector,
				 	  	"loading": $("#loading")
				 	  	
						
				 	};
			 	sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
		}
		function UserSelector(data){
			var mySelect = $('#userSelector');
			$.each(data, function (i, val) {
			    console.log("key: " +i+ " Value: " + val);
			    mySelect.append(
			            $('<option></option>').val(i).html(val)
			        );
			});
		}
	</script>
</body>
</html>