<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Join HealthSlate Program</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/>
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->

	<!--[if lt IE 9]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">
	<![endif]-->

	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css?v=0.5" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/recruitment.css?v=0.3" />

   <!--[if lt IE 9]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

    <script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>

    <style type="text/css">
        .footer{
            /*position: absolute;*/
            bottom: 0px;
        }

        small.error{
            margin-bottom: 0 !important;
        }

        .err-transparent{
            background-color: transparent !important;
            background: transparent !important;
            color: #fff !important;
        }

        .align-gender-fields{
            margin-top: 7px;
            display: block;
        }
    </style>
  </head>

  <body ng-app="AddMemberModule">

      <div id="loading" class="loader position-fixed">
          <div class="image-loader">
              <p class="progress-please-wait">Please wait...</p>
              <div class="progress progress-striped active">
                  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                  </div>
              </div>
              <p id="progressTxt" class="progress-text">We're saving data</p>
          </div>
      </div>

      <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>

      <div class="bg-white">

          <div class="row section-padding remove-border">
              <div class="columns medium-12">
                  <div class="grey-area-section remove-border" ng-controller="AddMemberController">
                      <h2 class="meal-summary-text">Add Member</h2>

                      <form id="formAddMember" name="formAddMember" novalidate method="POST" ng-submit="submitForm(patient)">

                              <div class="row row-auto">

                                    <div class="columns medium-6 padding-left-0">
                                        <label class="inline margin-bottom-0 meal-summary-text">
                                            <span class="meal-summary-text font-bold">First Name</span>
                                            <input type="text" name="firstName" tabindex="1" ng-model="patient.firstName" class="margin-bottom-0 rounded" placeholder="first name" />
                                            <small class="error err-transparent" ng-show="submitted && formAddMember.firstName.$error.required">Required.</small>
                                        </label>
                                    </div>

                                    <div class="columns medium-6 ">
                                        <label class="inline margin-bottom-0 meal-summary-text">
                                            <span class="meal-summary-text font-bold">Last Name</span>
                                            <input type="text" name="lastName" tabindex="2" ng-model="patient.lastName" class="margin-bottom-0 rounded" placeholder="last name" />
                                            <small class="error err-transparent" ng-show="submitted && formAddMember.lastName.$error.required">Required.</small>
                                        </label>
                                    </div>

                              </div>

                          <div class="row row-auto">

                              <div class="columns medium-6 padding-left-0">
                                  <label class="inline margin-bottom-0 meal-summary-text">
                                      <span class="meal-summary-text font-bold">MRN</span>
                                      <input id="txtMRN" type="text" tabindex="3" class="margin-bottom-0 rounded" name="mrn" maxlength="16" placeholder="mrn" ng-model="patient.mrn" />
                                  </label>
                              </div>

                              <div class="columns medium-6 ">
                                  <label class="inline margin-bottom-0 meal-summary-text">
                                      <span class="meal-summary-text font-bold">Confirm MRN</span>
                                      <input id="txtConfirmMRN" tabindex="4" type="text" class="margin-bottom-0 rounded" name="confirmMRN" placeholder="confirm mrn" maxlength="16" ng-model="patient.confirmMRN" />
                                      <small class="error" style="display:none">Confirm MRN should match.</small>
                                  </label>
                              </div>

                          </div>

                          <div class="row row-auto">

                              <div class="columns medium-6 padding-left-0">
                                  <label class="inline meal-summary-text margin-bottom-0">
                                      <span class="meal-summary-text font-bold">Email address</span>
                                      <input type="email" name="email" tabindex="5" ng-model="patient.email" placeholder="email address" class="rounded margin-bottom-0" ng-pattern="/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i" required />
                                      <small class="error err-transparent" ng-show="submitted && formAddMember.email.$error.pattern">Invalid email address.</small>
                                      <small class="error err-transparent" ng-show="submitted && formAddMember.email.$error.required">Required.</small>
                                  </label>
                              </div>

                              <div class="columns medium-6">
                                  <div class="row row-auto">

                                      <div class="columns medium-6 padding-left-0">
                                          <label class="inline margin-bottom-0">
                                              <span class="meal-summary-text font-bold">Date of Birth</span>
                                              <input id="txtDOB" name="dobString" tabindex="6" datepicker ng-model="patient.dobString" type="tel" placeholder="date of birth" class="rounded margin-bottom-0" />
                                              <small class="error err-transparent" ng-show="submitted && formAddMember.dobString.$error.required">Required.</small>
                                          </label>
                                      </div>

                                      <div class="columns medium-6">
                                          <label class="inline margin-bottom-0 meal-summary-text">
                                              <span class="meal-summary-text font-bold">Gender</span><br/>
                                              <span class="align-gender-fields">
                                                  <input type="radio" name="gender" tabindex="7" value="Male" ng-model="patient.gender" /><span class="text-black"> Male </span>
                                                  &nbsp;<input type="radio" name="gender" value="Female" ng-model="patient.gender" /><span class="text-black"> Female </span>
                                              </span>
                                              <small class="error err-transparent" ng-show="submitted && formAddMember.gender.$error.required">Required.</small>
                                          </label>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="row row-auto">

                              <div class="columns medium-6 padding-left-0">
                                  <label class="inline margin-bottom-0 meal-summary-text">
                                      <span class="meal-summary-text font-bold">Address</span>
                                      <textarea id="txtAddress" type="text" name="address" rows="4" tabindex="8" ng-model="patient.address" class="margin-bottom-0 rounded" placeholder="street address"></textarea>
                                      <small class="error err-transparent" ng-show="submitted && formAddMember.address.$error.required">Required.</small>
                                  </label>
                              </div>

                              <div class="columns medium-6">
                                  <label class="inline margin-bottom-0 meal-summary-text">
                                      <span class="meal-summary-text font-bold">City</span>
                                      <input id="txtCity" type="text" name="city" tabindex="9" ng-model="patient.city" class="margin-bottom-0 rounded" placeholder="city" />
                                      <small class="error err-transparent" ng-show="submitted && formAddMember.city.$error.required">Required.</small>
                                  </label>

                              </div>
                          </div>

                              <div class="row row-auto">

                                  <div class="columns medium-6 padding-left-0">
                                      <label class="inline margin-bottom-0 meal-summary-text">
                                          <span class="meal-summary-text font-bold">State</span>
                                          <select class="large-dropdown margin-bottom-0 rounded" name="state" tabindex="10" ng-model="patient.state"
                                                  ng-options="state as state for state in stateList track by state">

                                              <option value="" selected disabled>(select)</option>

                                          </select>
                                          <small class="error err-transparent" ng-show="submitted && formAddMember.state.$error.required">Required.</small>
                                      </label>
                                  </div>

                                  <div class="columns medium-6">
                                      <label class="inline margin-bottom-0 meal-summary-text">
                                          <span class="meal-summary-text font-bold">Zip code</span>
                                          <input id="txtZipcode" type="text" name="zipCode" tabindex="11" ng-model="patient.zipCode" class="margin-bottom-0 rounded" placeholder="00000-0000" />
                                          <small class="error err-transparent" ng-show="submitted && formAddMember.zipCode.$error.required">Required.</small>
                                          <small class="error err-transparent" ng-show="submitted && formAddMember.zipCode.$error.pattern">
                                              Please enter valid zip code
                                          </small>
                                      </label>
                                  </div>
                              </div>

                              <div class="row row-auto" ng-show="!uuid">

                                  <div class="columns medium-6 padding-left-0" >
                                      <span class="meal-summary-text font-bold">Facility</span>
                                      <select id="ddFacility" tabindex="12" ng-change="getFacilityCoaches(patient.facility)" class="large-dropdown margin-bottom-0 rounded height-48" name="facility" ng-model="patient.facility"
                                              ng-options="item.name for item in facilityList track by item.facilityId" ng-required="!uuid">

                                          <option value="" selected disabled>(select)</option>

                                      </select>
                                      <small class="error" ng-show="submitted && formAddMember.facility.$error.required">Required.</small>
                                  </div>
                              </div>

                          <div class="row row-auto">

                              <div class="columns medium-6 padding-left-0">

                                  <label class="inline margin-bottom-0 meal-summary-text" ng-show="patient.facility">
                                      <span class="meal-summary-text font-bold">Choose Lead Coach</span>
                                      <select id="ddFacilityCoaches" tabindex="13" class="large-dropdown margin-bottom-0 rounded height-48" name="leadCoach" ng-model="patient.leadCoach"
                                              ng-options="l as (l.firstName + ' ' + l.lastName) for l in coachesList track by l.providerId" ng-required="!uuid">

                                          <option value="" selected disabled>(select)</option>

                                      </select>
                                      <small class="error" ng-show="submitted && formAddMember.leadCoach.$error.required">Required.</small>
                                  </label>

                                  <label class="inline margin-bottom-0 meal-summary-text" ng-show="patient.facility">
                                      <span class="meal-summary-text font-bold">Choose Primary Food Coach</span>
                                      <select id="primaryCoaches" tabindex="14" class="large-dropdown margin-bottom-0 rounded height-48" name="primaryCoach" ng-model="patient.primaryCoach"
                                              ng-options="l as (l.firstName + ' ' + l.lastName) for l in primaryCoachesList track by l.providerId">

                                          <option value="" selected disabled>(select)</option>

                                      </select>
                                  </label>

                              </div>

                          </div>

                          <div class="row row-auto">
                              <br />
                              <div class="columns medium-12 padding-left-0">
                                  <span class="meal-summary-text font-bold">Select Member's Team</span>
                                  <br />
                                  <div class="selected-providers-list">

                                      <div ng-if="coachesList.length" class="row row-auto">
                                          <div class="columns medium-6 large-6 six">
                                              <label class="inline margin-bottom-0 label-dialog meal-summary-text">Food Coaches</label>
                                              <ul ng-if="getFoodCoaches(coachesList).length" class="small-block-grid-2 prov-ul">
                                                  <li ng-repeat="coach in getFoodCoaches(coachesList)">
                                                      <div class="row row-auto">
                                                          <div class="columns medium-2 large-2">
                                                              <input tabindex="14" type="checkbox" name="selectedProviders" class="selected-providers large-radio" value="{{coach.providerId}}">
                                                          </div>
                                                          <div class="columns medium-10 large-10 padding-left-0">
                                                              <p class="sel-pro-lbl align-pro-label">{{coach.firstName + ' ' + coach.lastName}}</p>
                                                          </div>
                                                      </div>
                                                  </li>
                                              </ul>
                                              <ul ng-if="!getFoodCoaches(coachesList).length" class="side-nav prov-ul">
                                                  <li>
                                                      <div>
                                                          No Food Coaches
                                                      </div>
                                                  </li>
                                              </ul>
                                          </div>
                                          <div class="columns medium-6 large-6 six">
                                              <label class="inline margin-bottom-0 label-dialog meal-summary-text">Coaches</label>
                                              <ul ng-if="getCoaches(coachesList).length" class="small-block-grid-2 large-block-grid-2 prov-ul">
                                                  <li ng-repeat="coach in getCoaches(coachesList)">
                                                      <div class="row row-auto">
                                                          <div class="columns medium-2 large-2">
                                                            <input tabindex="14" type="checkbox" name="selectedProviders" class="selected-providers large-radio" value="{{coach.providerId}}">
                                                          </div>
                                                          <div class="columns medium-10 large-10 padding-left-0">
                                                            <p class="sel-pro-lbl align-pro-label">{{coach.firstName + ' ' + coach.lastName}}</p>
                                                          </div>
                                                      </div>
                                                  </li>
                                              </ul>
                                              <ul ng-if="!getCoaches(coachesList).length" class="side-nav prov-ul">
                                                  <li>
                                                      <div>
                                                          No Coaches
                                                      </div>
                                                  </li>
                                              </ul>
                                          </div>
                                      </div>

                                      <ul ng-if="!coachesList.length" class="side-nav prov-ul padding-bottom-0 margin-bottom-0">
                                          <li>
                                              <div class="align-center">
                                                  No data
                                              </div>
                                          </li>
                                      </ul>

                                      <small id="errorMemberTeam" class="error hide">Required.</small>
                                      <small id="errorMemberTeamCoach" class="error hide">Please select your coach in Selected Member's Team.</small>
                                  </div>
                              </div>
                          </div>

                          <hr />

                               <div class="row row-auto">

                                  <div class="columns medium-12 padding-left-0">
                                      <input type="submit" tabindex="14" class="button radius btn-teal min-width-250" value="Done" />
                                  </div>

                              </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>

    <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
    <![endif]-->
    <!--[if (gte IE 9) | (!IE)]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
    <!--<![endif]-->

    <!-- Foundation 3 for IE 8 and earlier -->
    <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
    <![endif]-->

    <!-- Foundation 4 for IE 9 and later -->
    <!--[if gt IE 8]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
    <!--<![endif]-->

      <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/jquery.mask.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
      <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>

      <script type="text/javascript">

        var BASE_URL = "${pageContext.request.contextPath}/",
            ENROLL_EMAIL = '${email}',
            TOKEN = '${token}',
            FACILITY_NAME = '${facilityName}',
            SHOULD_SKIP_CONSENT = '${shouldSkipConsent}',
            FACILITY_LIST = ("${facilitiesListString}") ? $.parseJSON("${facilitiesListString}") : undefined,
            COACHES_LIST = ("${providerListString}") ? $.parseJSON("${providerListString}") : undefined,
            PATIENT_FACILITY_ID = '${patientFacilityId}',
            PATIENT_FACILITY_NAME = '${patientFacilityName}',
            ATOZ_FACILITY_ID = '${superFacilityId}',
            UUID = '${uuid}', EMAIL = '${email}', FIRST_NAME = '${firstName}', LAST_NAME = '${lastName}', DOB = '${dobString}', PHONE = '${phone}', MRN = '${mrn}';

        $(function(){
            $("#inviteMembersTab").addClass("selected-tab").parents("ul").prev("li").addClass("selected-tab");
		    $(document).foundation();
            $("#txtPhone").mask('(000) 000-0000');
            $("#txtZipcode").mask('00000-0000');
            $("small.error").removeClass("err-transparent");
        });

      </script>

      <script src="${pageContext.request.contextPath}/resources/js/controllers/add_member_controller.js?v=0.9"></script>
  </body>
</html>