<head>      
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Coaches List</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/facility.css" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/hs-datatable.css" />
	<script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>

  </head>
  
  <body>
	
	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>			
			<p id="progressTxt" class="progress-text">We're processing</p>
		</div>
	</div>

    <div id="confirmationRemoveMemberDialog" class="reveal-modal small" data-reveal>
        <h3 class="meal-summary-text">Remove Coach</h3>
        <p class="lead dialog-para">Are you sure you want to remove this Coach?</p>
        <a id="closeConfirmDialog" class="close-reveal-modal">&#215;</a>
        <div class="align-right">
            <a href="javascript:;" id="btnConfirmDelete" class="button radius btn-teal margin-top-10 margin-bottom-0">Yes</a>
            <a href="javascript:;" class="button secondary radius margin-top-10 margin-bottom-0" onclick="$('#closeConfirmDialog').click(); return false;">No</a>
        </div>
    </div>

	<div class="page-container max-width-1129">
	
	  <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>
	
      <div class="settings-container" ng-controller="ProviderPatientController">

      	<form id="providerForm" name="providerForm" method="POST" class="margin-bottom-0 remove-margin" novalidate ng-submit="submitForm(provider)">

			<div id="providerListDialog" class="reveal-modal large" data-reveal>
				<h3 class="dialog-header-green"> <img src="${pageContext.request.contextPath}/resources/css/images/provider.png" width="32" /> All Coaches </h3>
				<div id="dialogArea" class="dialog-area hs-table">
					<table id="tableProviders" class="table datatable border-none">
						<thead>
						<tr>
                            <th>Coach ID</th>
                            <th>Coach Name</th>
							<th>Type</th>
							<th>Assigned Facility</th>
							<th>Lead Coach</th>
						</tr>
						</thead>
						<tbody>
							<tr ng-repeat="pro in providerList">
                                <td>
                                    {{pro.providerId}}
                                </td>
								<td>
									{{pro.lastName}}, {{pro.firstName}}
								</td>
								<td>
									{{pro.type}}
								</td>
								<td>
									{{pro.facilityName}}
								</td>
								<td>
									<input type="checkbox" ng-model="pro.leadCoachId" ng-checked="{{ pro.providerId == pro.leadCoachId ? 'true': 'false' }}" ng-change="saveLeadCoach(pro.providerId);" class="checkbox-large margin-bottom-0" />
								</td>
							</tr>
							<tr ng-show="providerList == undefined || providerList.length == 0 || providerList == null">
								<td colspan="5" class="align-center">No coaches found.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<a id="closeProviderListDialog" class="close-reveal-modal">&#215;</a>

				<a href="#" class="button small radius hs-btns secondary right margin-bottom-0" onclick="$('#closeProviderListDialog').click();">Close</a>
			</div>

			<input id="txtHiddenProviderId" type="hidden" name="providerId" ng-model="providerId" />
			<input id="txtHiddenPatientId" type="hidden" name="patientId" ng-model="patientId" />

			<div class="row row-full">
                    <div class="medium-8 columns">
                        <div class="border-bt-heading padding-left-0 hs-page-heading">
                            <h2>Facilities</h2>
                        </div>
                    </div>
                    <div class="medium-4 columns align-right">
                        <a href="facility.action?facilityId=0" class="button radius small hs-btns btn-teal margin-bottom-0">Add Facility</a>
                    </div>
				</div>
	    	  			  		 		  	 
	    	  	<div class="page-sections row row-full main-page-settings bordered-white-bg green-bordered">

					<table id="table" class="table hs-table border-none">
						<thead>
                            <tr>
                                <th>Facility ID</th>
                                <th>Facility Name</th>
                                <th>Contact Person Name</th>
                                <th>Contact Number</th>
                                <th>Notification Enabled?</th>
                                <th>Skip Recruitment Consent Form</th>
                                <th>Facility Admin Name</th>
                                <th>Facility Admin Email</th>
								<th>Action</th>
                            </tr>
						</thead>
					</table>
	      	  	</div>        	  		   
      	  	</form>    	  
    	</div>    	
    </div>

	  <!--[if lt IE 9]>
	    	<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
		<![endif]-->
		<!--[if (gte IE 9) | (!IE)]><!-->
		    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>				       
		    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
		<!--<![endif]-->       
	        
	    <!-- Foundation 3 for IE 8 and earlier -->
		<!--[if lt IE 9]>	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
		<![endif]-->
		
		<!-- Foundation 4 for IE 9 and later -->
		<!--[if gt IE 8]><!-->	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
		<!--<![endif]-->	
	   	   	  
       <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
       
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/jquery.mask.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>

       <script type="text/javascript">

		   $.fn.dataTableExt.sErrMode = 'throw';

		   var oTable,
               userActivities = [],
               UUID = "",
               AJAXING = Object.create(Ajaxing),
               ALERT_MESSAGING = Object.create(AlertMessage).init(),
               SELECTED_ROW_INDEX = -1;

           function removeCountryCode(phone){
               if(phone){
                   var countryCode = phone.indexOf("+1");

                   if(countryCode > -1){
                       phone = phone.substring(countryCode+3, phone.length);
                   }

                   return phone;
               }
           }

			$(function(){
				$(document).foundation();
                $("#facilityTab").addClass("selected-tab");

				$("#loading").show();

				oTable =  $('#table').dataTable({

					"sDom" : "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'spanPagination'p>>",
					"sPaginationType" : "bootstrap",
					"oLanguage" : {
						"sLengthMenu" : "_MENU_ records per page" // how many records you want to display per page
					},
					"bServerSide": true ,
					"fnInitComplete": function (oSettings, json) {
						//remove sorting class from td, __oz
						$(".sorting_1").removeClass("sorting_1 sorting_1");
					},
					"sAjaxSource": "fetchFacilitiesData.action", // return jsonresponse with aaData json object
					"fnServerParams": function ( aoData ) {
					},
					"fnDrawCallback": function( oSettings ) {
						$(".sorting_1").removeClass("sorting_1 sorting_1");
                        formatTablePhoneNumber();
					},
					"bRetrieve" : true, // retrieve datatble object
					"bPaginate" : true, // enable disable pagination
					"bStateSave" : false, // saveState in cookie
					"bSort" : true,
					"bFilter":true,
                    stateSave: true,
					"aaSorting": [[ 0, "desc" ]],
					"bProcessing": true, // processing text in while processing
					"aoColumns": [
                        { "mDataProp": function(data, type, full){
                            return data.facilityId;
                        }},
						{ "mDataProp": function(data, type, full){
							return data.facilityName;
						}},
                        { "mDataProp": function(data, type, full){
                            return "<span class='word-break'>"+ data.contactPersonName + "</span>";
                        }},
                        { "mDataProp": function(data, type, full){
                            return "<span class='phone-number'>"+ data.contactInfo + "</span>";
                        }},
                        { "mDataProp": function(data, type, full){
                            if(data.isNotificationEnabled){
                                return "Yes";
                            }

                            return "No";
                        },"bSortable": false},
                        { "mDataProp": function(data, type, full){
                            if(data.isSkipConsentForm){
                                return "Yes";
                            }

                            return "No";
                        },"bSortable": false},
                        { "mDataProp": function(data, type, full){
                            if(data.facilityAdminName){
                                return data.facilityAdminName;
                            }
                            return "N/A";
                        }},
                        { "mDataProp": function(data, type, full){
                            if(data.facilityAdminEmail){
                                return data.facilityAdminEmail;
                            }
                            return "N/A";
                        }},
						{ "mDataProp": function(data, type, full){
							return "<a href='facility.action?facilityId="+data.facilityId+"'><img class='table-images-icon edit' src='${pageContext.request.contextPath}/resources/css/images/edit-new.png' title='Edit' /></a>";
						},"bSortable": false}
					]
				});

				fixedPaginationStyling();

                $(".sorting_1").removeClass("sorting_1 sorting_1");
				$("#loading").hide();
			});

		   function fixedPaginationStyling(){
			   var $ul = $(".dataTables_paginate ul");
			   $ul.each(function(){
				   var $this = $(this);
				   if($this && !$this.hasClass("pagination")){
					   $this.addClass("pagination");
				   }
			   });
		   }

           function formatTablePhoneNumber(){

               $(".phone-number").each(function(index, phone){

                   var $phoneLabel = $(phone),
                           phoneText = $phoneLabel.text();

                   formatPhoneNumber($phoneLabel, phoneText);
               });
           }

       </script>
  </body>          
</html>