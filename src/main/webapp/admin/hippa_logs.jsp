<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>
<head>

	<link rel="stylesheet"	href="${pageContext.request.contextPath}/resources/css/queue.style.css" />
	<title>HIPPA Logs</title>
	
	<style type="text/css">

        .input-field {
            width: 35% !important;
        }

        .ui-datepicker-trigger {
            margin: 0px;
            height: 37px;
            padding-top: 11px;
            width: 5px;
            padding-left: 1rem;
            background-color: #428bca;
        }

        .date-style {
            width: 82%;
            display: inline-block !important;
        }

        .header-style {
            width: 60%;
            padding: 0;
            top: 10px;
        }

        .ui-datepicker-trigger {
            margin: 0px;
            height: 37px;
            padding-top: 17px;
            width: 5px;
            padding-left: 1rem;
        }

        .dataTables_processing {
            position: absolute;
            top: 2% !important;
            left: 50%;
            width: 250px;
            height: 30px;
            margin-left: -125px;
            margin-top: -15px;
            padding: 14px 0 2px 0;
            border: 0px !important;
            text-align: center;
            color: #999;
            font-size: 14px;
            background: inherit !important;
            font-weight: bold;
            color: brown;
        }
        th {
            font-family: inherit !important;
        }

        .not-started-status{
            color: red;
        }

        .in-process-status{
            color: orange;
        }

        .filter-div-2 {
			height:auto;
			background-color: #fff;
			width: 100%;
			padding-top: 5px;
			margin-bottom: 18px;
			padding-left: 20px;
			text-align: left;
		}

        .container{
            width: 100%;
        }

        .margin-top-50px{
            margin-top: 50px;;
        }
	</style>
</head>

<body>

	<div id="loading" class="loader" style="display: block;">
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>
			<div class="progress progress-striped active">
				<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
		</div>
	</div>

	<div id="infoSuccess" class=" alert success alert-new-message"></div>
	<input type="hidden" name="maxCount" id="maxCount" />

    <div class="max-width-1129 margin-top-50px">
        <div class="row">
            <div class="columns medium-12">
                <div class="hs-page-heading">
                    <h2>
                        HIPPA Logs
                    </h2>
                </div>
                <div id="tableDiv" class="main-page padding-top-28 top-0 green-bordered">
                    <div class="container" id="unprocessedTable">
                        <table id="table" class="table border-none">
                            <thead>
                            <tr>
                                <th>Action Performed</th>
                                <th>Description</th>
                                <th>Logged Date</th>
                                <th>Logged By</th>
                                <th>Role</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<script	src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
		
	<script type="text/javascript">

        $.fn.dataTableExt.sErrMode = 'throw';

		var oTable,
			showRowCount = 0;
		
		$(document).ready(function(){
			
			$('#hippaLogsTab').addClass('selected-tab').parents("ul").prev("li").addClass("selected-tab");;

			  oTable =  $('#table').dataTable({

				"sDom" : "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'spanPagination'p>>",
				"sPaginationType" : "bootstrap",
				"oLanguage" : {
					"sLengthMenu" : "_MENU_ records per page" // how many records you want to display per page
				},
				"bServerSide": true ,
                  "fnInitComplete": function (oSettings, json) {
                      //remove sorting class from td, __oz
                      $(".sorting_1").removeClass("sorting_1 sorting_1");
                  },
                  "fnDrawCallback": function( oSettings ) {
                      $(".sorting_1").removeClass("sorting_1 sorting_1");
                  },
		 		"sAjaxSource": "fetchHippaLogs.action", // return jsonresponse with aaData json object
				"fnServerParams": function ( aoData ) {
				},  
				
				"bRetrieve" : true, // retrieve datatble object
				"bPaginate" : true, // enable disable pagination
				"bStateSave" : false, // saveState in cookie
				"bSort" : true, 
				"aaSorting": [[ 2, "desc" ]],
				"bProcessing": true, // processing text in while processing
	 			"aoColumns": [
                        { "mDataProp": "actionName"},
                        { "mDataProp": "description"},
                        { "mDataProp": "loggedDate"},
                        { "mDataProp": "loggedUserName"},
                        { "mDataProp": "userRole"}
                    ]
			  });

            fixedPaginationStyling();
            $('.row').css({'max-width':'100%', 'margin-top':'-9'});
			$("#loading").hide();
		}); 

		//fixed paging styling
		function fixedPaginationStyling() {
            var $ul = $(".dataTables_paginate ul");
            $ul.each(function () {
                var $this = $(this);
                if ($this && !$this.hasClass("pagination")) {
                    $this.addClass("pagination");
                }
            });
        }

		function refreshTable(){
			$("#infoSuccess").slideUp("slow"); 
			oTable.fnDraw();
			$("#loading").hide();			
		}

	</script>
</body>
</html>