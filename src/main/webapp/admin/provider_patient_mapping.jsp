<head>      
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Assign Lead Coach</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/facility.css" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/hs-datatable.css" />
	<script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>

    <style type="text/css">
		tbody tr td:nth-child(8), tbody tr td:nth-child(9), tbody tr td:nth-child(10){
			background-color: #EDFAFA !important;
			border: 1px solid #fff !important;
			text-align: center;
		}
	</style>

  </head>
  
  <body ng-app="ProviderPatientModule">
	
	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>			
			<p id="progressTxt" class="progress-text">We're processing</p>
		</div>
	</div>

    <div id="confirmationRemoveMemberDialog" class="reveal-modal small" data-reveal>
        <h3 class="meal-summary-text">Remove Member</h3>
        <p class="lead dialog-para">Are you sure you want to remove this Member?</p>
        <a id="closeConfirmDialog" class="close-reveal-modal">&#215;</a>
        <div class="align-right">
            <a href="javascript:;" id="btnConfirmDelete" class="button radius btn-teal margin-top-10 margin-bottom-0">Yes</a>
            <a href="javascript:;" class="button secondary radius margin-top-10 margin-bottom-0" onclick="$('#closeConfirmDialog').click(); return false;">No</a>
        </div>
    </div>

	<div class="page-container max-width-1129">
	
	  <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>
	
      <div class="settings-container" ng-controller="ProviderPatientController">

      	<form id="providerForm" name="providerForm" method="POST" class="margin-bottom-0 remove-margin" novalidate ng-submit="submitForm(provider)">

			<div id="providerListDialog" class="reveal-modal large" data-reveal>
				<h3 class="dialog-header-green"> <img src="${pageContext.request.contextPath}/resources/css/images/provider.png" width="32" /> All Coaches </h3>
				<div id="dialogArea" class="dialog-area hs-table">
					<table id="tableProviders" class="table datatable border-none">
						<thead>
						<tr>
                            <th>Coach ID</th>
                            <th>Coach Name</th>
							<th>Type</th>
							<th>Assigned Facility</th>
							<th>Lead Coach</th>
						</tr>
						</thead>
						<tbody>
							<tr ng-repeat="pro in providerList">
                                <td>
                                    {{pro.providerId}}
                                </td>
								<td>
									{{pro.lastName}}, {{pro.firstName}}
								</td>
								<td>
									{{pro.type}}
								</td>
								<td>
									{{pro.facilityName}}
								</td>
								<td>
									<input type="checkbox" ng-model="pro.leadCoachId" ng-checked="{{ pro.providerId == pro.leadCoachId ? 'true': 'false' }}" ng-change="saveLeadCoach(pro.providerId);" class="checkbox-large margin-bottom-0" />
								</td>
							</tr>
							<tr ng-show="providerList == undefined || providerList.length == 0 || providerList == null">
								<td colspan="5" class="align-center">No coaches found.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<a id="closeProviderListDialog" class="close-reveal-modal">&#215;</a>

				<a href="#" class="button small radius hs-btns secondary right margin-bottom-0" onclick="$('#closeProviderListDialog').click();">Close</a>
			</div>

			<input id="txtHiddenProviderId" type="hidden" name="providerId" ng-model="providerId" />
			<input id="txtHiddenPatientId" type="hidden" name="patientId" ng-model="patientId" />

			<div class="row row-full">
   	  				<div class="medium-12 columns">
   	  					<div class="border-bt-heading padding-left-0 hs-page-heading">
			         		<h2>Members Data</h2>
			         	</div>
   	  				</div>
				</div>
	    	  			  		 		  	 
	    	  	<div class="page-sections row row-full main-page-settings bordered-white-bg green-bordered">

					<table id="table" class="table hs-table border-none">
						<thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Registration Date</th>
                                <th>Mac Address</th>
                                <th>Device/App Version</th>
                                <th>Facility Name</th>
                                <th>Total Logs</th>
								<th>Last Post</th>
								<th>Posts / Shares / Likes</th>
								<th class="th-action">Action</th>
                            </tr>
						</thead>
					</table>
	      	  	</div>        	  		   
      	  	</form>    	  
    	</div>    	
    </div>

    <!-- Member Details Dialog -->
    <div id="viewMemberDetailsDialog" class="reveal-modal small confirm-dialog">
        <div class="columns small-12 member-details-dialog">
            <h3 class="meal-summary-text">Member Details</h3>
            <div class="grey-black-pair">
                <p class="margin-bottom-0 grey"> <img src="${pageContext.request.contextPath}/resources/css/images/email-black.png" width="20"> Email</p>
                <p id="pMemberEmail" class="black">__</p>
            </div>

            <div class="grey-black-pair">
                <p class="margin-bottom-0 grey"><img src="${pageContext.request.contextPath}/resources/css/images/phone-black.png" width="20"> Phone</p>
                <p id="pMemberPhone" class="black">__</p>
            </div>

            <div class="grey-black-pair">
                <p class="margin-bottom-0 grey"> <img src="${pageContext.request.contextPath}/resources/css/images/address-black.png" width="20"> Address</p>
                <p id="pMemberAddress" class="black">__</p>
            </div>

            <div class="grey-black-pair">
                <p class="margin-bottom-0 grey"> <img src="${pageContext.request.contextPath}/resources/css/images/home-black.png" width="20"> City</p>
                <p id="pMemberCity" class="black">__</p>
            </div>

            <div class="grey-black-pair">
                <p class="margin-bottom-0 grey"> <img class="last-image" src="${pageContext.request.contextPath}/resources/css/images/state-black.png" width="18"> State</p>
                <p id="pMemberState" class="black">__</p>
            </div>

            <div class="grey-black-pair">
                <p class="margin-bottom-0 grey"> <img class="exclude-image" src="${pageContext.request.contextPath}/resources/css/images/zip-black.png" width="20"> Zip</p>
                <p id="pMemberZip" class="black">__</p>
            </div>

        </div>
        <a class="close-reveal-modal">&#215;</a>
    </div>

	  <!--[if lt IE 9]>
	    	<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
		<![endif]-->
		<!--[if (gte IE 9) | (!IE)]><!-->
		    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>				       
		    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
		<!--<![endif]-->       
	        
	    <!-- Foundation 3 for IE 8 and earlier -->
		<!--[if lt IE 9]>	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
		<![endif]-->
		
		<!-- Foundation 4 for IE 9 and later -->
		<!--[if gt IE 8]><!-->	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
		<!--<![endif]-->	
	   	   	  
       <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
       
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/jquery.mask.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>

       <script type="text/javascript">

		   $.fn.dataTableExt.sErrMode = 'throw';

		   var PROVIDER_LIST = ("${providerListString}") ? $.parseJSON("${providerListString}") : undefined,
			   oTable,
               userActivities = [],
               UUID = "",
               AJAXING = Object.create(Ajaxing),
               ALERT_MESSAGING = Object.create(AlertMessage).init(),
               SELECTED_ROW_INDEX = -1;

			$(function(){
				$(document).foundation();
                $("#providerPatientMapTab").addClass("selected-tab");

				$("#loading").show();

				oTable =  $('#table').dataTable({

					"sDom" : "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'spanPagination'p>>",
					"sPaginationType" : "bootstrap",
					"oLanguage" : {
						"sLengthMenu" : "_MENU_ records per page" // how many records you want to display per page
					},
					"bServerSide": true ,
					"fnInitComplete": function (oSettings, json) {
						//remove sorting class from td, __oz
						$(".sorting_1").removeClass("sorting_1 sorting_1");
					},
					"sAjaxSource": "fetchMembersData.action", // return jsonresponse with aaData json object
					"fnServerParams": function ( aoData ) {
					},
					"fnDrawCallback": function( oSettings ) {
						$(".sorting_1").removeClass("sorting_1 sorting_1");
                        loadUserActivity();
					},
					"bRetrieve" : true, // retrieve datatble object
					"bPaginate" : true, // enable disable pagination
					"bStateSave" : false, // saveState in cookie
					"bSort" : true,
					"bFilter":true,
                    stateSave: true,
					"aaSorting": [[ 0, "desc" ]],
					"bProcessing": true, // processing text in while processing
					"aoColumns": [
                        { "mDataProp": function(data, type, full){
                            return data.patientId;
                        }},
						{ "mDataProp": function(data, type, full){
							return data.firstName + " "+ data.lastName;
						}},
                        { "mDataProp": function(data, type, full){
                            return "<span class='word-break'>"+ data.email + "</span>";
                        }},
						{ "mDataProp": "registrationDate"},
						{ "mDataProp": "macAddress"},
                        { "mDataProp": function(data, type, full){
                            return data.versionNo;
                        },"bSortable": false},
                        { "mDataProp": "facilityName"},
                        { "mDataProp": "totalLogs"},
						{ "mDataProp": function(data, type, full){
                            var logTimeMillis = parseInt( (data.lastLogTimeMillis)? data.lastLogTimeMillis : 0);

                            var logTime = (logTimeMillis != 0) ? HSDateUtils.formatDateFromTicks(logTimeMillis) : "N/A";
							var logType = (data.lastLogType)? data.lastLogType : "N/A";
							if(logTime == "N/A" && logType == "N/A") {
								return "N/A"
							}
							return logTime + " / "+ logType;
						},"bSortable": false},
						{ "mDataProp": function(data, type, full){
							return "<img class='loading-table' src='${pageContext.request.contextPath}/resources/css/images/dot_loader.gif' />";
						},"bSortable": false},
						{ "mDataProp": function(data, type, full){
                            var detailsObject = {
                                "email": nullSafeJS(data.email),
                                "phone": nullSafeJS(data.phone),
                                "address": nullSafeJS(data.address),
                                "city" : nullSafeJS(data.city),
                                "state": nullSafeJS(data.state),
                                "zip": nullSafeJS(data.zip)
                            };
							return "<a href='javascript:;' onclick='openProviderDialog("+data.patientId+")'><img class='table-images-icon change' src='${pageContext.request.contextPath}/resources/css/images/provider.png' title='Change Coach' /></a>" +
                                    "<a href='javascript:;' class='view-details' data-details='"+JSON.stringify(detailsObject)+"'><img class='table-images-icon view' src='${pageContext.request.contextPath}/resources/css/images/view-new.png' title='View Details' /></a>" +
                                    "<a href='javascript:;' class='remove-member' data-id='"+data.uuid+"'><img class='table-images-icon cross' src='${pageContext.request.contextPath}/resources/css/images/cross.svg' title='Remove' /></a>" ;
						},"bSortable": false}
					]
				});

				fixedPaginationStyling();

                $(".sorting_1").removeClass("sorting_1 sorting_1");
				$("#loading").hide();
			});

		   function fixedPaginationStyling(){
			   var $ul = $(".dataTables_paginate ul");
			   $ul.each(function(){
				   var $this = $(this);
				   if($this && !$this.hasClass("pagination")){
					   $this.addClass("pagination");
				   }
			   });
		   }

           function openProviderDialog(patientId){
               var $scope = angular.element("#table").scope();
               $scope.patientId = patientId;
               $scope.refreshProviderList(patientId);
               $("#providerListDialog").foundation("reveal", "open");
           }

           function loadUserActivity(){
               $("tbody tr").each(function(){
                   var $this = $(this),
                       email = $this.children("td:eq(2)").text(),
                       dataToSend = { "email": email };

                   if(email) {
                       $.post("getUserActivityCount.action", dataToSend).done(function(data) {
                           try{
                               data = JSON.parse(data);
                           }catch(err){
                               //console.log(err.message);
                           }
                           onGettingUserActivityCount(data, $this);
                       }).fail(function(jqXHR, textStatus, errorThrown){
                           //status 500: Server side error
                           //status 404: not found
                           console.log("ERROR", jqXHR);
                       });
                    }
               });

               $(".remove-member").off("click").on("click", function(e){
                   e.preventDefault();
                   var $this = $(this);
                   UUID = $this.data("id");
                   SELECTED_ROW_INDEX = $this.closest("tr").index();
                   $("#confirmationRemoveMemberDialog").foundation("reveal", "open");
               });

               $(".view-details").off("click").on("click", function(e){
                   e.preventDefault();
                   var $this = $(this),
                        detailsObject = $this.data("details");
                   if(detailsObject){
                        $("#pMemberEmail").text(detailsObject.email);
                        $("#pMemberAddress").text(detailsObject.address);
                        $("#pMemberCity").text(detailsObject.city);
                        $("#pMemberState").text(detailsObject.state);

                        formatZipCode($("#pMemberZip"), detailsObject.zip);
                        formatPhoneNumber($("#pMemberPhone"), detailsObject.phone);
                   }
                   $("#viewMemberDetailsDialog").foundation("reveal", "open");
               });

               $("#btnConfirmDelete").off("click").on("click", function(e){
                   e.preventDefault();
                   //send via ajax call
                   var dataToSend = {"uuid": UUID},
                       postRequestActions = {
                           "loading": $("#loading"),
                           "error": $("#errMessage"),
                           "successCallBack": onRemoveMember
                       };

                   AJAXING.sendPostRequest("removeMember.action", dataToSend, postRequestActions);
               });
           }

           function onGettingUserActivityCount(data, $tr){
               var userActivity = data.DATA;
               if(userActivity){
                   $tr.find("img.loading-table")
                           .hide()
                           .end()
                           .children("td:eq(9)")
                           .text(userActivity.totalPosts + "/" + userActivity.sharedPost + "/" + userActivity.likes);
               }
           }

           function onRemoveMember(data){
               var $error = $("#errMessage");
               $("#confirmationRemoveMemberDialog").foundation("reveal", "close");
               if(data.STATUS == "SUCCESS"){
                   ALERT_MESSAGING.showAlertMessage($error, "Member removed successfully.", ALERT_MESSAGING.SUCCESS);
                   oTable.fnDraw();
               }else{
                   AJAXING.showErrorOrRedirect(data, $error, "Unable to remove Member. Please try again later.");
               }
           }
       </script>

		<script src="${pageContext.request.contextPath}/resources/js/provider_patient_controller.js"></script>
  </body>          
</html>