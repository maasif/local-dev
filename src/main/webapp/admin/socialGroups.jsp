<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>
	<title>My Patients</title>
	<style type="text/css">

		body{
			background-color: #eee;
		}
		td {
		    border: 1px solid #ccc !important;
		}
		textarea{resize:vertical;}
		.main-page{
			box-shadow:none;
			border-radius:2px;
		}
	</style>
</head>
<body >
	<br/><br/>
	
	<div class="new-group-style">
		<button onclick="createGroup()" class="border-radius-3p">Create Group</button>
	</div>
	
	<div class="breadcrumb-top align-breadcrumb" style="display: none;">
		<ul class="breadcrumbs">  
		  	<li>  
		    	<a href="viewDashBoard.action">Dashboard</a>  
		  	</li> 
	  		<li class="current">Social Groups</li>
		</ul>
	</div>
	
	<div id="infoSuccess" class=" alert success alert-message"></div>
	<div id="infoError" class=" alert error alert-message"></div>
	
	<div id="tableDiv" class="main-page" style="width:92%;margin-bottom:20px;">
		<div>
			<label class="log-header">Social Groups</label>
		</div>
		<display:table name="groupsDTOs" requestURI="" class="table" uid="row">
			<display:setProperty name="basic.empty.showtable" value="true" />
			
			<display:column title="Group ID" property="groupId" class="width-10-right-align"/>
			<display:column title="Name" property="groupName" class="width-20-perc"/>
			<display:column title="Group Members" property="groupMembers" class="width-10-right-align"/>
			<display:column title="Likes" property="likes" class="width-10-right-align"/>
			<display:column title="Description" property="description"/>
		</display:table>
		
	</div>
	
	<s:form id="sForm" theme="simple" action="addNewGroup.action">
		<div id="groupModal" class="modal hide-dialog">
		    <div class="modal-dialog color-white">
		        <div class="tent">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal">x</button>
		                <h4 class="modal-title">Social Group</h4>
		            </div>
		            <div class="modal-body padding-bottom-0">
	        			<div>
	        				<ul class="button-group">
		        				<li><label class="item-style inline">	Group Name:</label></li>
		        				<li class="margin-left-44p"><s:textfield id="groupName" name="groupName" class="width-107p"></s:textfield></li>
	        				</ul>
	        				<ul class="button-group">
		        				<li><label class="item-style inline ">	Group Description:</label></li>
		        				<li class="margin-left-5p">
		        					<s:textarea id="groupDescription" name="groupDescription" cssClass="text-area-style"></s:textarea>
		        				</li>
	        				</ul>
	        				
		            	</div>
		            </div>
		            
		            <div class="align-center new-group-success"></div>
	            	<div class="align-center new-group-error"></div>
	            	
		            <div class="modal-footer footer-style">
		                <button type="submit" class="modal-butt button radius margin-bottom-0 saveFoodBtn">Save</button>
		                <button type="button" class="button radius secondary margin-bottom-0" data-dismiss="modal">Close</button>
		            </div>
		        </div>
		    </div>
		</div>
	</s:form>
	
	<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
 	<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>
	
	<script type="text/javascript">
		$(function(){
			
			$('#groupsTab').addClass('selected-tab');
			
			if("${statusMessage}".indexOf("error") != -1){
				$("#infoError").html("${statusMessage}");
				$("#infoError").slideDown();
				hideElement($("#infoError"));
			}
			else if("${statusMessage}".indexOf("successfully") != -1){
				$("#infoSuccess").html("${statusMessage}");
				$("#infoSuccess").slideDown();
				hideElement($("#infoSuccess"));
			}
			try {
				$.fn.dataTableExt.sErrMode = 'throw';
				initDataTable("row",{"sorting": [[0, "asc" ]]});
			}
			catch(err) {console.log(err);}
			$('.row').css('max-width','100%');
		});
		
		function hideElement(element){
			setTimeout(function(){
				$(element).slideUp("slow");
			}, 10000);
		}
		
		function createGroup(){
			$("#groupName").val("");
			$("#groupDescription").val("");
			$("#groupModal").modal('show');
			$("#groupName").focus();
		}
	</script>
</body>
</html>