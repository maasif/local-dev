/**
 * Created by jaredsaul on 3/10/15.
 */
'use strict';
var app = angular.module('dashboard', ['ui.router','dashboard.services','dashboard.filters']);

app.controller('overviewCtrl', ['$scope', 'data', 'colorize', function($scope, data, colorize) {
    //data
    $scope.colorize = colorize.color;  //color map by categories
    $scope.user = data.user;
    $scope.curriculum = data.curriculum;
    $scope.selectedIndex = -1;

    //find current week relative to start date
    var current_date = new Date();

    //if user is null it was breaking here accessing the user.startDate, so i check user null here, __oz.
    var start_date = !$scope.user ? new Date() : new Date($scope.user.startDate);
    $scope.week = !$scope.user ? 1 : Math.round(Math.abs(current_date - start_date) / (1000 * 60 * 60 * 24 * 7)) + 1;

    //default sessions list to current week
    $scope.displaySessions = $scope.curriculum[$scope.week-1].sessions;

    //functions
    //selected index functionality added for highlighting current selected item by _oz.
    $scope.showSessions = function(index, week){
        $scope.selectedIndex = index;
        $scope.displaySessions = $scope.curriculum[week-1].sessions;
    }

    $scope.next = function(e){
        e.preventDefault();
        $scope.week = $scope.week < $scope.curriculum.length ? $scope.week + 1 : $scope.week;

        var curriculumCoaching = $scope.curriculum[$scope.week];

        if(curriculumCoaching){
            curriculumCoaching = curriculumCoaching.coaching
        }

        //calling a function to update coach todo list when week is changed, __oz.
        parent.displayCoachTodoList(curriculumCoaching, $scope.week);
    }

    $scope.previous = function(e){
        e.preventDefault();
        $scope.week = $scope.week > 1 ? $scope.week - 1 : $scope.week;

        //calling a function to update coach todo list when week is changed, __oz.
        parent.displayCoachTodoList($scope.curriculum[$scope.week].coaching, $scope.week);
    }

    //calling a function to update coach todo list when week is changed, __oz.
    setTimeout(function(){
        parent.displayCoachTodoList($scope.curriculum[$scope.week-1].coaching, $scope.week);
    }, 1500);

}]);

app.constant("apiEndpoint",{
    "production":"http://ec2-54-174-154-235.compute-1.amazonaws.com:8080/api/users/",
    "development":"http://localhost:8080/api/users/",
    //"current":"development"
    "current":"production"
});

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider ) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('overview', {url:'/overview?user',
            templateUrl: 'dashboard.jsp',
            controller:'overviewCtrl',
            resolve: {
                data: function(userService, $stateParams){
                     return userService.read($stateParams.user, 'overview').then(function(results){
                         return results.data;
                     });
                 }
            }
        })
}]);

