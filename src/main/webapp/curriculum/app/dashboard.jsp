
    <!-- Information covered, Activities area -->
    <div class="row row-full bg-white-topics">

        <!-- Left white area -->
        <div class="columns medium-12 bg-white summary-area">

            <!-- Current Topic, See all Topics etc. -->
            <div class="row curriculum-header">
                <div class="columns medium-1">
                    <label class="inline margin-bottom-0"><img src="css/images/ic_progress_weight_2.png" /></label>
                </div>
                <div class="columns medium-2 padding-left-0">
                    <h3> Topics </h3>
                </div>
                <div class="columns medium-6 padding-left-0">
                    <h3 class="week"> Week {{week}} - {{curriculum[week-1].title}} </h3>
                </div>

                <div class="columns medium-3">
                    <a href="#" ng-click="previous($event)" class="prev-nav"><img class="align-image" src="css/images/purple_back.png" title="Previous Week" /></a>
                    <h3 class="percentage">{{user.weeks[week].percentage||0}}% <span>complete</span></h3>
                    <a href="#" ng-click="next($event)" class="next-nav"><img class="align-image" src="css/images/purple_next.png" title="Next Week" /></a>
                </div>
            </div>

            <div class="grey-divider">

                <div class="row">

                    <div class="columns medium-1">
                        &nbsp;
                    </div>
                    <div class="columns medium-5">

                        <div class="topic-section">

                            <h3>Information covered</h3>
                            <div class="side-nav" ng-bind-html="curriculum[week-1].learning|markdown"></div>

                        </div>

                    </div>

                    <div class="columns medium-5">

                        <div class="topic-section">

                            <h3>Activities & Challenges</h3>
                            <div class="side-nav" ng-bind-html="curriculum[week-1].activities|markdown"></div>

                        </div>

                    </div>

                    <div class="columns medium-1">
                        &nbsp;
                    </div>

                </div>

            </div>

            <!-- Curriculum area -->
            <div class="curriculum-area">

                <!-- Green Header -->
                <div class="row curriculum-green-header">
                    <div class="columns medium-6">
                        <label class="inline margin-bottom">Member Progress</label>
                    </div>
                    <div class="columns medium-4">
                        <label class="inline margin-bottom">
                            <span class="light-grey">Last Activity</span> <span class="black">{{ user.last.activityDate|datestring}}</span>
                        </label>
                    </div>

                </div>
                <!-- Content listview -->
                <div class="row bg-grey-light curriculum-scrollable">
                    <div class="columns medium-6 padding-left-0">
                        <div class="content margin-right-8">
                            <ul id="topicsMainList" class="list-group grey-listview topics-main-listing">
                                <!-- ng-repeat to list to populate dynamically -->
                                <li class="list-group-item" ng-class="{completed: $index == selectedIndex}" ng-repeat="week in curriculum">

                                    <div class="grey-item" ng-click='showSessions($index, week.weekNumber)'>
                                        <div class="vertical-text {{colorize(week.category).color}}">
                                            <p>{{week.category}}</p>
                                        </div>
                                        <span class="week-text">{{week.title}}</span>

                                        <p class="topic-heading">{{week.numberSessions}} Sessions</p>

                                        <div class="img-perc-container has-img">
                                            <img src="${pageContext.request.contextPath}/curriculum/app/css/images/tick-selected.png" class="tick-image"
                                                 ng-if="user.weeks[week.weekNumber].percentage==100"/>
                                            <span class="badge completed-percentage"
                                                  ng-if="user.weeks[week.weekNumber].percentage!=100">{{user.weeks[week.weekNumber].percentage||0}}%</span>

                                        </div>
                                    </div>

                                </li>


                            </ul>
                        </div>
                    </div>

                    <div class="columns medium-6 padding-left-0">

                        <ul class="side-nav progress-list">
                            <li ng-repeat='session in displaySessions'>
                                <div class="progress-item">
                                    <div class="row">
                                        <div class="columns medium-9">
                                            <p class="session-text">{{session.title}}</p>

                                            <p class="quiz-name">{{session.numberUnits}} units </p>
                                        </div>
                                        <div class="columns medium-3">
                                            <p class="session-text">&nbsp;</p>

                                            <p class="margin-bottom-0 status">
                                                {{user.sessions[session.id].percentage||0}}% completed</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

    </div>