/**
 * Created by jaredsaul on 11/26/14.
 */
/**
 * Created by jaredsaul on 11/24/14.
 */
'use strict';

angular.module('dashboard.filters', [])

    .filter('markdown', function ($sce) {
        var converter = new Showdown.converter();
        return function (value) {
            var html = converter.makeHtml(value || '');
            return $sce.trustAsHtml(html);
        };
    })

    .filter('datestring',function(){
        return function(indate){
            if(indate){
                var d = new Date(indate).toDateString();
                return d;
            }

            return "N/A";
        }
    });