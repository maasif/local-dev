<%@ taglib prefix="s" uri="/struts-tags" %>

<!--[if lt IE 7]>      <html lang="en" ng-app="dashboard" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" ng-app="dashboard" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" ng-app="dashboard" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" ng-app="dashboard" class="no-js"> <!--<![endif]-->


<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <title>Topics Overview</title>
    <meta http-equiv="x-ua-compatible" content="IE=Edge"/>

    <link href="${pageContext.request.contextPath}/curriculum/app/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/curriculum/app/css/foundation.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/curriculum/app/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/curriculum/app/css/patient.dashboard.css" />
    <link href="${pageContext.request.contextPath}/curriculum/app/css/colors.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/curriculum/app/css/curriculum.css" rel="stylesheet" />
</head>

<body>

<div ui-view></div>

<script type="text/javascript">
    var APP_PATH = "${pageContext.request.contextPath}/";

</script>
<!-- packages -->

<script src="${pageContext.request.contextPath}/curriculum/app/bower_components/angular/angular.js"></script>
<script src="${pageContext.request.contextPath}/curriculum/app/bower_components/angular-ui-router/release/angular-ui-router.js"></script>
<script src="${pageContext.request.contextPath}/curriculum/app/bower_components/lodash/lodash.min.js"></script>
<script src="${pageContext.request.contextPath}/curriculum/app/bower_components/showdown/src/showdown.js"></script>

<!-- app -->
<script src="${pageContext.request.contextPath}/curriculum/app/dashboard.js"></script>
<script src="${pageContext.request.contextPath}/curriculum/app/services.js"></script>
<script src="${pageContext.request.contextPath}/curriculum/app/filter.js"></script>


</body>
</html>
