/**
 * Created by jaredsaul on 3/10/15.
 */

'use strict';
angular.module('dashboard.services', [])

    .factory("userService",function($http,apiEndpoint){
        var _userId=null;
        return {
            read:function(userId,method) {
                var getString=method ? userId+'/'+method : userId;
                return  $http.get(apiEndpoint[apiEndpoint.current]+getString);
            }
        }
    })


    .factory("colorize",function(){
        var colormap={
            'stress-sleep':{
                background: 'stress-sleep',
                border: 'stress-sleep-border',
                color: 'stress-sleep',
                text: 'stress-sleep-text',
                textlight: 'stress-sleep-text-light',
                dark: 'stress-sleep-dark',
                light: 'stress-sleep-light',
                pattern: 'teal-pattern',
                backArrow: 'css/images/teal_back.png',
                colorName:'teal'
            },
            'eating': {
                background: 'eating',
                border: 'eating-border',
                color: 'eating',
                text: 'eating-text',
                textlight: 'eating-text-light',
                dark: 'eating-dark',
                light: 'eating-light',
                pattern: 'green-pattern',
                backArrow: 'css/images/green_back.png',
                colorName: 'green'
            },
            'medication': {
                background: 'medication',
                border: 'medication-border',
                color: 'medication',
                text: 'medication-text',
                textlight: 'medication-text-light',
                dark: 'medication-dark',
                light: 'medication-light',
                pattern: 'red-pattern',
                backArrow: 'css/images/red_back.png',
                colorName: 'red'
            },
            'general': {
                background: 'general',
                border: 'general-border',
                color: 'general',
                text: 'general-text',
                textlight: 'general-text-light',
                dark: 'general-dark',
                light: 'general-light',
                pattern: 'grey-pattern',
                backArrow: 'css/images/grey_back.png',
                colorName: 'grey'
            },
            'physical-activity': {
                background: 'physical-activity',
                border: 'physical-activity-border',
                color: 'physical-activity',
                text: 'physical-activity-text',
                textlight: 'physical-activity-text-light',
                dark: 'physical-activity-dark',
                light: 'physical-activity-light',
                pattern: 'yellow-pattern',
                backArrow: 'css/images/yellow_back.png',
                colorName: 'yellow'
            },
            'family-relationship': {
                background: 'family-relationship',
                border: 'family-relationship-border',
                color: 'family-relationship',
                text: 'family-relationship-text',
                textlight: 'family-relationship-text-light',
                dark: 'family-relationship-dark',
                light: 'family-relationship-light',
                pattern: 'blue-pattern',
                backArrow: 'css/images/blue_back.png',
                colorName: 'blue'
            },
            'work-money': {
                background: 'work-money',
                border: 'work-money-border',
                color: 'work-money',
                text: 'work-money-text',
                textlight: 'work-money-text-light',
                dark: 'work-money-dark',
                light: 'work-money-light',
                pattern: 'purple-pattern',
                backArrow: 'css/images/purple_back.png',
                colorName: 'purple'
            }
        };

        return {
            color: function(category) { return colormap[category]; }
        }
    })

