 <head>
        
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>View | Content</title>    
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   
   <link rel="stylesheet" href="resources/css/common.style.css" />
      
   <style type="text/css">     
	
	#mainPage{
		max-width: 990px;
		top:15px;
		border:1px solid #ccc;
		border-radius:5px;
		box-shadow: 0px 0px 5px #ccc;
	}
   </style>

 </head>
 <body>   

   <div class="logo">      
      <img src="resources/css/images/logo.png" />      
    </div>

	<div id="loading" class="ajax-loader"><img src="resources/css/images/ajax-loader.gif" /></div>
	<div id="ajaxError" class="error-bootstrap-style">
        <span id="spText">Error !!! Please try later</span>        
     </div> 
	 <div id="mainPage" class="overflow-fix main-page">	  	            		             		      
		 <div id="loadHtmlDiv"></div>	  	
	 </div>
   
   <script src="resources/js/jquery.js"></script>
   <script src="resources/js/common.js"></script>
   
   <script>         
	   	
   	    function showError(errText){
   	    	var $error = $("#ajaxError").show();
   	    	$error.children("#spText").html(errText);	
   	    }
   	    
		$(function() {
			
			var $loading = $("#loading").show(),				
				$mainPage = $("#mainPage").show(); 
				
			var htmlFilePath = getQuerystringParameterByName('src');
			
			if(htmlFilePath === ""){
				$mainPage.hide();
				$loading.hide();
				showError("<strong>Error:</strong> Unable to load file");	      			      			    
			}else{
				$("#loadHtmlDiv").load(htmlFilePath, function(responseTxt, statusTxt, xhr){
					$loading.hide();
					if(statusTxt == "error"){
					  $mainPage.hide();
				      if(xhr.status === 404){	
				    	  showError("<strong>Error:</strong> Unable to load file");				    	  
				      }			      
				    }			    
			  	});
			}
		});
		
	</script>					
 </body>
