<!-- This file is used by the caller, used name is caller_uid, and it calls the callee named calle_uid -->
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Video Call</title>
    <script type="text/javascript" src="https://download.rtccloud.net/js/webappid/${callAppId}"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/caller.css?v=0.1" />
    <script src="${pageContext.request.contextPath}/resources/js/jquery.mask.min.js"></script>
</head>
<body>

    <div id="terminatedMessage" class="alert-box alert alert-message-abs top-124" style="display: none;">______________________</div>

    <!-- BreadCrumb -->
    <div class="breadcrumb-green">
        <div class="max-width-1129">
            <ul class="breadcrumbs">
                <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                    <li>
                        <a href="unProcessedMeals.action">Meals</a>
                    </li>
                </s:if>
                <li>
                    <a href="members.action">My Members</a>
                </li>
                <li>
                    <a href="dashboard.action?patientId=${patientId}">Member Dashboard</a>
                </li>
                <li class="current">Video Call</li>
                <li class="current"><b>Member Name:</b> <s:property value='%{patient.user.displayName}' /></li>
                <li class="current"><b>Phone:</b> <span id="lblPhone" class="text-black"><s:property value='%{patient.user.phone}' /></span></li>
            </ul>
        </div>
    </div>

    <div id="error">
        <h1>An error occurred:</h1>
        <div id="error-content"></div>

        <input type="hidden" name="patientId" />
        <input type="hidden" name="callAppId" />
        <!-- <input type="hidden" name="patientAddress" /> -->
        <input type="hidden" name="providerAddress" />

    </div>

    <div id="divMemberNavArea"></div>
    <div class="dashboard-container margin-bottom-20">
        <div class="row max-width-1129">

            <div class="coach-area-expand">
                <a href="#" id="btnExpColCoachArea"><img src="${pageContext.request.contextPath}/resources/css/images/arrow-left.png" title="Hide" /></a>
            </div>

            <div class="columns medium-12">

                <!-- Charts, goals and Coach Notes area -->
                <div class="row bg-white row-auto" data-equalizer>

                    <!-- Left white area -->
                    <div class="columns medium-9 padding-lr-0 summary-area equal-areas" data-equalizer-watch>
                          <div class="padding-10">
                            <label id="refreshStatusLabel" class="inline online italic font-bold" style="display:none"> <img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Refreshing...</label>

                            <h4 id="stat"><img src="${pageContext.request.contextPath}/resources/css/images/video_call.png" width="48" /> Video Call</h4>
                            <div class="row row-auto">
                                <div class="columns small-6 padding-left-0">
                                    <label class="inline italic margin-bottom-0" id="connecting"><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Preparing for call...</label>
                                </div>
                                <div id="divStatusCallButton" class="columns small-6 padding-right-0" style="display:none;">
                                    <div class="row row-auto">
                                        <div class="small-8 columns padding-right-0">
                                            <label class="inline align-right margin-bottom-0">
                                                <a href="#" id="buttonRefresh" class="refresh-btn"> <img src="${pageContext.request.contextPath}/resources/css/images/refresh_messages.png" width="28" title="Refresh Member Status" /></a>
                                                Member Status: <span id="call" class="lbl-status">__</span> </label>
                                        </div>
                                        <div class="small-4 columns padding-left-0 align-right">
                                            <label class="inline margin-bottom-0">
                                                <a href="#" id="buttonCall" class="align-call-btn"> <img src="${pageContext.request.contextPath}/resources/css/images/video_call.png" width="48" />  Click to call</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="videoContainer">
                            </div>
                        </div>
                    </div>

                    <!-- Left grey area -->
                    <div id="divCoachesAreaLeft" class="columns medium-3 bg-grey equal-areas" data-equalizer-watch>
                        <script>
                            var patId = '${patientId}',
                                loggedInUserID = '${session.PROVIDER.providerId}';
                            $(document).ready(function(){
                                $("#divCoachesAreaLeft").load("${pageContext.request.contextPath}/educator/coachesArea.jsp");
                            });
                        </script>
                    </div>

                </div>
             </div>
        </div>
    </div>

    <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/hs.video-call.js"></script>

    <script type="text/javascript">

        var patId = '${patientId}',
            UUID = '${uuid}',
            APP_VERSION_OF_PATIENT = '${appVersion}', //the app version the patient currently using
            patId_UUID = patId,
            MEMBER_DEVICE_TYPE = '${memberDeviceType}',
            loggedInUserID = '${session.PROVIDER.providerId}',
            VALIDATE_CALL_AGAIN = false,
            VALIDATE_CALL = false;

        $(document).ready(function(){

            //here we are parsing app version, if parsed then use UUID for video call else ID
            try {
                if(MEMBER_DEVICE_TYPE == "Android"){
                    var tempAppVersion = parseFloat(APP_VERSION_OF_PATIENT);
                    if(tempAppVersion >= 0.84){
                        patId_UUID = UUID;
                    }
                }else{

                    var splitted = APP_VERSION_OF_PATIENT.split(" "),
                        version = splitted[0], restString = splitted[1];

                    version = parseFloat(version);
                    if(version > 0.52){
                        patId_UUID = UUID;
                    }
                }
            } catch (err) {
                patId_UUID = patId;
            }

            //written in common.js
            formatPhoneNumber($("#lblPhone"), $("#lblPhone").text());

            var config = {
              "callerId": '${providerAddress}',
              "calleeId": patId_UUID,
              "appId": '${callAppId}',
              "baseUrl": '${baseUrl}',
              "coachType": '${session.PROVIDER.type}'
            };

            HSVideoCall.init(config);

            Ajaxing.loadExternalPage($("#divMemberNavArea"), "${pageContext.request.contextPath}/educator/memberNavArea.jsp", function(){
                $("#divMemberNavArea ul").css("top", "10px");
            });
        });

        </script>
</body>
</html>