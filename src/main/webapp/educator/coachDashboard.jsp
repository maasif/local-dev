<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
  <%--<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />--%>
   <title>Coach Dashboard</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css?v=0.16" />
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/coach.dashboard.css?v=0.14" />

   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->
	
  </head>
  
  <body>

	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>			
			<p id="progressTxt" class="progress-text">We're working</p>
		</div>
	</div>
	
	<div class="dashboard-container margin-bottom-20">
	
	  <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>
          <div class="row max-width-1129">

               <div class="columns large-12">
                   <div class="align-right">
                       <a href="#" id="btnEditCoachPreferences" class="button tiny margin-bottom-20 white-bordered-btn remove-width">
                            <span>
                                <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/edit-info.png">
                            </span>
                           My 1:1 Session Preferences
                       </a>
                   </div>

                    <ul class="large-block-grid-3 medium-block-grid-2 small-block-grid-1">

                  <li>
                      <div class="c-dashboard-item item-us">
                          <h4><img src='${pageContext.request.contextPath}/resources/css/images/upcoming.png' width='28'/> Upcoming 1:1 Sessions</h4>

                          <ul class="side-nav c-dashboard-list-header">
                            <li><a href="javascript:;"><div class="row"><div class="columns medium-6 large-6">Member Name</div><div class="columns medium-6 large-6>"> Scheduled Date</div></div></a></li>
                          </ul>

                          <div class="c-dashboard-item-scrollable">
                              <div class="c-dashboard-list-container">
                                  <ul id="listUpcomingSessions" class="side-nav c-dashboard-list">

                                  </ul>
                                  <div class="more"><a href="#">more</a></div>
                              </div>
                          </div>
                          <div id="loadingUpcomingSessions" class="c-dashboard-loading">
                              <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                          </div>
                      </div>
                  </li>

                  <li>
                      <div class="c-dashboard-item item-mm">
                          <h4><img src='${pageContext.request.contextPath}/resources/css/images/member_message.png' width='28'/> Member Messages</h4>
                          <ul class="side-nav c-dashboard-list-header">
                              <li><a href="javascript:;"><div class="row"><div class="columns medium-6 large-6">Member Name</div><div class="columns medium-6 large-6>"> Received Date</div></div></a></li>
                          </ul>
                          <div class="c-dashboard-item-scrollable">
                              <div class="c-dashboard-list-container">
                                  <ul id="listMemberMessages" class="side-nav c-dashboard-list">

                                  </ul>
                                  <div class="more"><a href="#">more</a></div>
                              </div>
                          </div>
                          <div id="loadingMemberMessages" class="c-dashboard-loading">
                              <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                          </div>
                      </div>
                  </li>

                  <li>
                      <div class="c-dashboard-item item-cm">
                          <h4> <img src='${pageContext.request.contextPath}/resources/css/images/coach_message.png' width='28'/> New Coach Notes</h4>
                          <ul class="side-nav c-dashboard-list-header">
                              <li><a href="javascript:;"><div class="row"><div class="columns medium-6 large-6">Member Name</div><div class="columns medium-6 large-6>"> Notes Date</div></div></a></li>
                          </ul>
                          <div class="c-dashboard-item-scrollable">
                              <div class="c-dashboard-list-container">
                                  <ul id="listCoachMessages" class="side-nav c-dashboard-list">

                                  </ul>
                                  <div class="more"><a href="#">more</a></div>
                              </div>
                          </div>
                          <div id="loadingCoachMessages" class="c-dashboard-loading">
                              <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                          </div>
                      </div>
                  </li>

                  <li>
                      <div class="c-dashboard-item item-ws">
                          <h4><img src='${pageContext.request.contextPath}/resources/css/images/waiting.png' width="28"/> Last Contact With Member</h4>
                          <ul class="side-nav c-dashboard-list-header">
                              <li><a href="javascript:;"><div class="row"><div class="columns medium-6 large-6">Member Name</div><div class="columns medium-6 large-6>"> Last Contact Date</div></div></a></li>
                          </ul>
                          <div class="c-dashboard-item-scrollable">
                              <div class="c-dashboard-list-container">
                                  <ul id="listMemberMessagesLastContactOn" class="side-nav c-dashboard-list">
                                  </ul>
                                  <div class="more"><a href="#">more</a></div>
                              </div>
                          </div>
                          <div id="loadingMemberMessagesLastContactOn" class="c-dashboard-loading">
                              <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                          </div>
                      </div>
                  </li>

                  <li>
                      <div class="c-dashboard-item item-nm">
                          <h4><img src='${pageContext.request.contextPath}/resources/css/images/new_members.png' width='28'/> My New Members</h4>

                          <ul class="side-nav c-dashboard-list-header">
                              <li><a href="javascript:;"><div class="row"><div class="columns medium-6 large-6">Member Name</div><div class="columns medium-6 large-6>"> Joined Date</div></div></a></li>
                          </ul>

                          <div class="c-dashboard-item-scrollable">
                              <div class="c-dashboard-list-container">
                                  <ul id="listMyNewMembers" class="side-nav c-dashboard-list">

                                  </ul>
                                  <div class="more"><a href="#">more</a></div>
                              </div>
                          </div>
                          <div id="loadingMyMembers" class="c-dashboard-loading">
                              <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                          </div>
                      </div>
                  </li>

                  <li>
                      <div class="c-dashboard-item item-wl">
                          <h4><img src='${pageContext.request.contextPath}/resources/css/images/inactive.png' width='28'/> Inactivity Watch List</h4>
                          <ul class="side-nav c-dashboard-list-header">
                              <li><a href="javascript:;"><div class="row"><div class="columns medium-6 large-6">Member Name</div><div class="columns medium-6 large-6>"> Last Active Date</div></div></a></li>
                          </ul>
                          <div class="c-dashboard-item-scrollable">
                              <div class="c-dashboard-list-container">
                                  <ul id="listInactivityWatch" class="side-nav c-dashboard-list">

                                  </ul>
                                  <div class="more"><a href="#">more</a></div>
                              </div>
                          </div>
                          <div id="loadingInactiveMembers" class="c-dashboard-loading">
                              <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                          </div>
                      </div>
                  </li>
              </ul>
               </div>
          </div>
    </div>

    <!-- Coach Eating/Session Preferences Dialog -->
    <div id="coachEatingPreferencesDialog" class="reveal-modal medium">
        <h3 class="meal-summary-text">My 1:1 Session Preferences</h3>
        <div id="coachErrorSchedulingPreference" class="alert-box alert alert-message-abs" style="display: none;">______________________</div>
        <div class="dialog-area">
            <div class="session-days-container">
                <h4 class="teal-text">Days <span class="general-text font-size-12 right">*(Check all that apply)</span> </h4>
                <ul class="medium-block-grid-3">
                    <li>
                        <div class="row row-auto">
                            <div class="columns medium-1 padding-left-0">
                                <input type="checkbox" value="Weekdays" class="large-radio session-days" />
                            </div>
                            <div class="columns medium-11">
                                <label class="inline">Weekdays</label>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row row-auto">
                            <div class="columns medium-1 padding-left-0">
                                <input type="checkbox" value="Saturdays" class="large-radio session-days" />
                            </div>
                            <div class="columns medium-11">
                                <label class="inline">Saturdays</label>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row row-auto">
                            <div class="columns medium-1 padding-left-0">
                                <input type="checkbox" value="Sundays" class="large-radio session-days" />
                            </div>
                            <div class="columns medium-11">
                                <label class="inline">Sundays</label>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="session-times-container">
                <h4 class="teal-text">Times</h4>
                <ul class="medium-block-grid-3">
                    <li>
                        <div class="row row-auto">
                            <div class="columns medium-1 padding-left-0">
                                <input type="checkbox" value="Before 9 AM" class="large-radio session-times" />
                            </div>
                            <div class="columns medium-11">
                                <label class="inline">Before 9 AM </label>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row row-auto">
                            <div class="columns medium-1 padding-left-0">
                                <input type="checkbox" value="9 AM - noon" class="large-radio session-times" />
                            </div>
                            <div class="columns medium-11">
                                <label class="inline">9 AM - noon</label>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row row-auto">
                            <div class="columns medium-1 padding-left-0">
                                <input type="checkbox" value="Lunch" class="large-radio session-times" />
                            </div>
                            <div class="columns medium-11">
                                <label class="inline">Lunch</label>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row row-auto">
                            <div class="columns medium-1 padding-left-0">
                                <input type="checkbox" value="Afternoon" class="large-radio session-times" />
                            </div>
                            <div class="columns medium-11">
                                <label class="inline">Afternoon</label>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row row-auto">
                            <div class="columns medium-1 padding-left-0">
                                <input type="checkbox" value="Evenings" class="large-radio session-times" />
                            </div>
                            <div class="columns medium-11">
                                <label class="inline">Evenings</label>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
        <div class="align-right">
            <a href="#" id="btnSaveCoachSchedulingPreferences" class="button small radius btn-teal">Save</a>
        </div>
    </div>

    <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
    <![endif]-->
    <!--[if (gte IE 9) | (!IE)]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
    <!--<![endif]-->

    <!-- Foundation 3 for IE 8 and earlier -->
    <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
    <![endif]-->

    <!-- Foundation 4 for IE 9 and later -->
    <!--[if gt IE 8]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
    <!--<![endif]-->
   <script src="${pageContext.request.contextPath}/resources/js/date.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
   <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
   <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/hs.coach.dashboard.js?v=0.5"></script>

      <script type="text/javascript">

        var loggedInUserID = '${session.PROVIDER.providerId}',
            providerId = '${session.PROVIDER.providerId}',
            COACH_SESSION_PREFERENCE;

        $(function(){
		    $(document).foundation();
            HSCoachDashboard.init(providerId);
        });
      </script>
  </body>          
</html>