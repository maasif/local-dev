<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>In-app Messages</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/>
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->

	<!--[if lt IE 9]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">
	<![endif]-->

	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/timepicker/jquery.timepicker.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/timepicker/bootstrap-datepicker.css" />

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css?v=0.4" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/twilio_messages.css?v=0.1" />
   <!--[if lt IE 9]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

	<style type="text/css">
		.page-container{
			max-width: 69% !important;
		}
        /*.footer{
            position: fixed;
            bottom: 0;
        }*/

        .in-app-top-shadow{
            top:151px !important;
        }
	</style>
  </head>

  <body>

  <!-- BreadCrumb -->
  <div class="breadcrumb-green">
      <div class="max-width-1129">
          <ul class="breadcrumbs">
              <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                  <li>
                      <a href="unProcessedMeals.action">Meals</a>
                  </li>
              </s:if>
              <li>
                  <a href="members.action">My Members</a>
              </li>
              <li>
                  <a href="dashboard.action?patientId=${patientId}">Member Dashboard</a>
              </li>
              <li class="current">In-app Messages</li>
              <li class="current"><b>Member Name:</b> <s:property value='%{patient.user.displayName}' /></li>
          </ul>
      </div>
  </div>

	<div id="loading" class="loader">
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
			  </div>
			</div>
		</div>
	</div>

  <div id="divMemberNavArea"></div>
  <div class="dashboard-container margin-bottom-20">
      <div class="row max-width-1129">

          <div class="coach-area-expand">
              <a href="#" id="btnExpColCoachArea"><img src="${pageContext.request.contextPath}/resources/css/images/arrow-left.png" title="Hide" /></a>
          </div>

          <div class="columns medium-12">

              <!-- Charts, goals and Coach Notes area -->
              <div class="row bg-white row-auto" data-equalizer>

                  <!-- Left white area -->
                  <div class="columns medium-9 padding-lr-0 summary-area equal-areas" data-equalizer-watch>
                      <div class="padding-10">

                          <input type="hidden" name="patientId" />
                          <s:form id="formUpload" method="POST" enctype="multipart/form-data" cssClass="margin-bottom-0 hide">
                              <s:file type="file" id="uploadFile" accept="image/*" name="uploadFile" cssClass="hide"></s:file>
                              <s:hidden id="txtHiddenFileName" name="fileName"></s:hidden>
                          </s:form>

                          <div id="errMessageCoach" class="alert-box alert alert-message-abs">______________________</div>

                          <div class="settings-container">

                              <div class="row row-auto">
                                  <div class="large-12 columns padding-left-0">
                                      <div class="border-bt-heading padding-left-0">
                                          <h4><img src="${pageContext.request.contextPath}/resources/css/images/message.png" width="32" />&nbsp;In-app Messages</h4>
                                      </div>
                                  </div>
                              </div>

                              <div class="row row-auto">
                                  <div class="large-4 columns padding-left-0">
                                      <span id="spChatList" class="visibility-hidden">Filter messages by Coach:
                                        <select id="chatProviderList" class="large-dropdown"></select>
                                      </span>
                                  </div>
                                  <div class="large-6 align-right columns padding-right-0">
                                      <div class="sms-buttons">
                                          <a href="#" id="btnInAppRefreshMessages" class="refresh-messages in-app-refresh"><img src="${pageContext.request.contextPath}/resources/css/images/refresh_messages.png"> Refresh</a>
                                          <a href="#" id="twilioDialogOpen" data-phonenumber="<s:property value='%{patient.user.phone}' ></s:property>" class="refresh-messages in-app-refresh padding-lr-10"><span><img src="${pageContext.request.contextPath}/resources/css/images/messages.png"></span> Text Messages</a>
                                          <a class="refresh-messages in-app-refresh padding-lr-10" href='coachPatientMessagesAll.action?patientId=${patientId}'><img src='${pageContext.request.contextPath}/resources/css/images/notes.png' width='28'/>View All Messages</a>
                                      </div>
                                  </div>
                              </div>

                              <div id="coachThreadView" class="dialog-area bg-lightgrey twilio-dialog">
                                  <div id="coachThreadViewShadow" class="top-shadow in-app-top-shadow"></div>
                                  <div class="twilio-messages-threaded-view">
                                      <div class="thread-empty coach-thread-empty">No Messages found.</div>
                                      <ul id="coachThreadViewList" class="large-block-grid-1 threaded-list">

                                      </ul>
                                  </div>
                              </div>
                              <div class="row row-auto margin-top-10" >

                                  <div id="msg-send-txt-drpdwn" class="large-12 columns padding-lr-0">
                                      <div class="row collapse" style="display:block">
                                          <div class="small-10 columns">

                                               <textarea id="txtCoachMessage" rows="4" cols="" maxlength="1500" placeholder="Type here..." class="textarea-message with-attachment"></textarea>

                                              <span class="max-characters">1500 characters left</span>
                                              <a href="#" class="attachment-icon" onclick="$('#uploadFile').val(''); $('#uploadFile').click();"><img title="Attach file" src="${pageContext.request.contextPath}/resources/css/images/attachment.png" width="32" /></a>
                                              <p id="pFileAttached" style="display: none;">File Attached: <span id="spFileName"></span> <a id="btnDeleteAttachedFile" href="#"> Delete </a></p>
                                          </div>
                                          <div class="small-2 columns">
                                              <input id="btnSendCoachMessage" type="button" class="button radius btn-teal postfix" value="Send" />
                                          </div>
                                      </div>
                                      <s:if test="%{#session.PROVIDER.type != 'Tech Support'}">
                                          <div>
                                              <ul class="inline-list">
                                                  <li>
                                                      <input type="checkbox" id="chkSendBroadMsg" class="large-radio margin-bottom-0" />
                                                  </li>
                                                  <li class="margin-left-5">
                                                      <label class="inline"> Inform All My Members </label>
                                                  </li>
                                              </ul>
                                          </div>
                                      </s:if>
                                  </div>

                              </div>
                          </div>
                      </div>
                  </div>

                  <!-- Left grey area -->
                  <div id="divCoachesAreaLeft" class="columns large-3 bg-grey equal-areas" data-equalizer-watch>
                      <script>
                          var patId = '${patientId}',
                              loggedInUserID = '${session.PROVIDER.providerId}';
                          $(document).ready(function(){
                              $("#divCoachesAreaLeft").load("${pageContext.request.contextPath}/educator/coachesArea.jsp");
                          });
                      </script>
                  </div>

              </div>
          </div>
      </div>
  </div>

  <!-- Twilio Messages Dialog -->
  <input type="hidden" id="txtMessageTo" name="messageTo" />
  <div id="twilioMessageDialog" class="reveal-modal medium twilio-dialog">

      <div id="loadingTwilio" class="loader">
          <div class="image-loader">
              <p class="progress-please-wait">Please wait...</p>
              <div class="progress progress-striped active">
                  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                  </div>
              </div>
          </div>
      </div>

      <div id="errMessageTwilio" class="alert-box alert">______________________</div>
      <h3 class="dialog-header"> <img src="${pageContext.request.contextPath}/resources/css/images/messages.png" /> Text Messages <a href="#" id="btnRefreshMessages" class="refresh-messages"><img src="${pageContext.request.contextPath}/resources/css/images/refresh_messages.png">Refresh</a> </h3>
      <div id="dialogArea" class="dialog-area">
          <div id="topShadow" class="top-shadow"></div>
          <div class="twilio-messages-threaded-view">
              <div class="thread-empty">No Messages found.</div>
              <ul id="threadViewList" class="large-block-grid-1" style="display:none"></ul>
          </div>
      </div>
      <a id="closeTwilioDialog" class="close-reveal-modal" onclick="closeTwilioDialog();">&#215;</a>

      <div class="row row-full margin-top-10">
          <div class="large-12 columns padding-lr-0">
              <div class="row collapse" style="display:block">
                  <div id="ddSmsContainer" class="small-8 columns">
                      <select id="ddSmsTemplates" name="messageBody" class="large-dropdown">
                          <option value="" selected disabled>(select)</option>
                      </select>
                  </div>
                  <div id="ddTimeContainer" class="small-2 columns">
                      <input type="text" id="txtTime" placeholder="-- Select --" class="margin-bottom-0 height-49" />
                  </div>
                  <div class="small-2 columns">
                      <a href="#" id="btnSendMessage" class="button radius postfix btn-twilio-send btn-teal">Send</a>
                  </div>
              </div>
          </div>
      </div>
  </div>

      <div id="inAppMessageDialog" class="reveal-modal medium">
          <h3 class="dialog-header meal-summary-text"> Full size image </h3>
          <img id="inAppMessageDialogImage" src="">
          <a class="close-reveal-modal">&#215;</a>
      </div>

	  <!--[if lt IE 9]>
	    	<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
		<![endif]-->
		<!--[if (gte IE 9) | (!IE)]><!-->
		    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
		    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
		<!--<![endif]-->

	    <!-- Foundation 3 for IE 8 and earlier -->
		<!--[if lt IE 9]>
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
		<![endif]-->

		<!-- Foundation 4 for IE 9 and later -->
		<!--[if gt IE 8]><!-->
		    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
		<!--<![endif]-->

        <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/js/date.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/jquery.autosize.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>

        <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
        <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/timepicker/jquery.timepicker.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/js/coach_patient_messages.js?v=0.13"></script>
        <script src="${pageContext.request.contextPath}/resources/js/twilio_messages.js"></script>

      <script type="text/javascript">

        var coachMessages = null,
			patId = "${patientId}",
            loggedInUserID = '${session.PROVIDER.providerId}',
            SMS_TEMPLATES = ("${smsTemplateList}") ? $.parseJSON("${smsTemplateList}"): undefined,
        	SMS_LIST = ("${coachMessagesList}") ? $.parseJSON("${coachMessagesList}") : undefined,
            validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"],
            isAllowUploading = true;
            loggedIn_UserId = '${session.USER.userId}',
            LOCAL_STORAGE_MESSAGE_KEY = "messageTxtStorage";

        function getFileExtension(filename) {
            return typeof filename != "undefined" ? filename.substring(filename.lastIndexOf("."), filename.length).toLowerCase() : false;
        }

        $(function(){
		    $(document).foundation();

            coachMessages = Object.create(CoachMessages)
                            .init("${pageContext.request.contextPath}", patId).buildThreadedView(SMS_LIST);

            $("#uploadFile").on("change", function(){
               var $pAttached = $("#pFileAttached").hide(),
                   file = $(this)[0].files[0],
                   fileName = file.name;

                if($(this).val().length > 0){
                    $pAttached.show().children("#spFileName").text(fileName);
                }

                var extractFileExtension = getFileExtension(fileName);

                isAllowUploading = true;
                if(validFileExtensions.indexOf(extractFileExtension) == -1){
                    isAllowUploading = false;
                    alert("Only image files uploading supported");
                }
            });

            $("#btnDeleteAttachedFile").off("click").on("click", function(e){
                e.preventDefault();
                $("#pFileAttached").hide();
                isAllowUploading = true;
                $("#uploadFile").val("");
            });

            Ajaxing.loadExternalPage($("#divMemberNavArea"), "${pageContext.request.contextPath}/educator/memberNavArea.jsp", function(){
                $("#divMemberNavArea ul").css("top", "10px");
            });
        });

        function closeTwilioDialog(){
            //clearing refresh timer instance, once the dialog is closed
            clearInterval(TWILIO_REFRESH_TIMER);
            SHOULD_SHOW_LOADING = true;
            $("#twilioMessageDialog").foundation("reveal", "close");
        }
      </script>
  </body>
</html>