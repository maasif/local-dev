<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <title>In-app Messages</title>
    <meta http-equiv="x-ua-compatible" content="IE=Edge"/>
    <!--[if (gte IE 9) | (!IE)]><!-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />
    <script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
    <!--<![endif]-->

    <!--[if lt IE 9]>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">
    <![endif]-->

    <!--[if gt IE 8]>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
    <![endif]-->

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/timepicker/jquery.timepicker.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/timepicker/bootstrap-datepicker.css" />

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css?v=0.4" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/twilio_messages.css?v=0.1" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
    <![endif]-->

    <style type="text/css">
        .page-container{
            max-width: 69% !important;
        }
        /*.footer{
            position: fixed;
            bottom: 0;
        }*/

        .in-app-top-shadow{
            top:151px !important;
            z-index: 1;
        }

        .patient-image {
            border-radius: 50%;
            border: 4px solid #53B6BD !important;
            display: inline-block;
            width: 48px !important;
            height: 48px !important;
            position: absolute;
            top: 1px !important;
            left: 5px !important;
            z-index: 1;
        }

        .bubble-wrapper {
            position: relative;
        }

        .bubble-wrapper .bubble-teal {
            padding-left: 60px;
        }

        .meal-message {
            background: #DDEEFF !important;
        }

        .info-color {
            color: #A19F9F !important;
        }

        .width-60perc{
            width: 50% !important;
        }

        .padding--bottom-10{
            padding-bottom: 10px;
        }

        .in-app-image{
            cursor: pointer;
            opacity: 0.8;
        }

        .status{
            color: #fff;
            padding: 5px;
            min-width: 120px;
            font-size: 12px;
            border-radius: 2px;
            display: inline-block;
            font-weight: bold;
            text-transform: uppercase;
        }

        .member{
            color: #8AB329;
        }

        .btn-filter-messages{
            position: relative;
            top: 16px;
            left: -17px;
        }

        .dt-desc-image{
            position: relative;
            top: -3px;
        }

        .btn-to-messages{
            position: absolute;
            left: 50%;
            margin-left: -61px;
            top:10px;
        }
    </style>
</head>

<body>

<!-- BreadCrumb -->
<div class="breadcrumb-green">
    <div class="max-width-1129">
        <ul class="breadcrumbs">
            <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                <li>
                    <a href="unProcessedMeals.action">Meals</a>
                </li>
            </s:if>
            <li>
                <a href="members.action">My Members</a>
            </li>
            <li>
                <a href="dashboard.action?patientId=${patientId}">Member Dashboard</a>
            </li>
            <li class="current">In-app Messages</li>
            <li class="current"><b>Member Name:</b> <s:property value='%{patient.user.displayName}' /></li>
        </ul>
    </div>
</div>

<div id="loading" class="loader">
    <div class="image-loader">
        <p class="progress-please-wait">Please wait...</p>
        <div class="progress progress-striped active">
            <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            </div>
        </div>
    </div>
</div>

<div id="divMemberNavArea"></div>
<div class="dashboard-container margin-bottom-20">
    <div class="row max-width-1129">

        <div class="coach-area-expand">
            <a href="#" id="btnExpColCoachArea"><img src="${pageContext.request.contextPath}/resources/css/images/arrow-right.png" title="Show" /></a>
        </div>

        <div class="columns medium-12">

            <!-- Charts, goals and Coach Notes area -->
            <div class="row bg-white row-auto" data-equalizer>

                <!-- Left white area -->
                <div class="columns medium-12 padding-lr-0 summary-area equal-areas" data-equalizer-watch>
                    <div class="padding-10">

                        <input type="hidden" name="patientId" />
                        <s:form id="formUpload" method="POST" enctype="multipart/form-data" cssClass="margin-bottom-0 hide">
                            <s:file type="file" id="uploadFile" accept="image/*" name="uploadFile" cssClass="hide"></s:file>
                            <s:hidden id="txtHiddenFileName" name="fileName"></s:hidden>
                        </s:form>

                        <div id="errMessageCoach" class="alert-box alert alert-message-abs">______________________</div>

                        <div class="settings-container">

                            <div class="row row-auto">
                                <div class="large-12 columns padding-left-0">
                                    <div class="border-bt-heading padding-left-0">
                                        <h4><img src="${pageContext.request.contextPath}/resources/css/images/notes.png" width="32" />&nbsp;All In-app Messages</h4>

                                        <%--<a href="coachPatientMessages.action?patientId=${patientId}" class="button margin-bottom-0 button btn-to-messages radius btn-grey small">Go back To Messages</a>--%>
                                    </div>
                                </div>
                            </div>

                            <div class="row row-auto padding--bottom-10">
                                <div class="large-8 columns padding-left-0">
                                    <div class="row row-auto">
                                        <div class="large-5 align-right columns padding-lr-0">
                                            <label class="inline align-left margin-bottom-0">From
                                                <input type="text" id="messagesStartDate" class="margin-bottom-0" placeholder="Select Start Date"/>
                                            </label>
                                        </div>
                                        <div class="large-5 align-right columns padding-right-0">
                                            <label class="inline align-left margin-bottom-0">To
                                                <input type="text" id="messagesEndDate" class="margin-bottom-0" placeholder="Select End Date"/>
                                            </label>
                                        </div>
                                        <div class="large-2 align-right columns padding-right-0">
                                            <label class="inline margin-bottom-0">
                                                <input type="button" id="messagesSearch" class="margin-bottom-0 button radius btn-teal small btn-filter-messages" value="Search"/>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="large-4 align-right columns padding-right-0">
                                    <div class="sms-buttons">
                                        <a href="#" id="btnInAppRefreshMessages" class="refresh-messages in-app-refresh"><img src="${pageContext.request.contextPath}/resources/css/images/refresh_messages.png"> Refresh</a>
                                    </div>
                                </div>
                            </div>

                            <div id="coachThreadView" class="bg-lightgrey twilio-dialog green-bordered">

                                <div class="thread-empty coach-thread-empty">No Messages found.</div>
                                <table id="coachThreadViewList" class="table datatable hide">
                                    <thead>
                                        <th class="width-60perc">Message</th>
                                        <th class="date-received-sort" data-sort="desc">DateTime Received <span class="dt-desc-image"><img src="${pageContext.request.contextPath}/resources/css/images/sort_desc.png" /></span> </th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Meal Message?</th>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Left grey area -->
                <div id="divCoachesAreaLeft" class="columns bg-grey equal-areas" style="display: none;" data-equalizer-watch>
                    <script>
                        var patId = '${patientId}',
                                loggedInUserID = '${session.PROVIDER.providerId}';
                        $(document).ready(function(){
                            $("#divCoachesAreaLeft").load("${pageContext.request.contextPath}/educator/coachesArea.jsp");
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="inAppMessageDialog" class="reveal-modal medium">
    <h3 class="dialog-header meal-summary-text"> Full size image </h3>
    <img id="inAppMessageDialogImage" src="">
    <a class="close-reveal-modal">&#215;</a>
</div>

<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
<![endif]-->
<!--[if (gte IE 9) | (!IE)]><!-->
<script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
<!--<![endif]-->

<!-- Foundation 3 for IE 8 and earlier -->
<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
<![endif]-->

<!-- Foundation 4 for IE 9 and later -->
<!--[if gt IE 8]><!-->
<script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
<!--<![endif]-->

<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>

<script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
<script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/coach_patient_messages_all.js?v=0.3"></script>

<script type="text/javascript">

    var coachMessages = null,
        patId = "${patientId}",
        loggedInUserID = '${session.PROVIDER.providerId}',
        SMS_LIST = ("${coachMessagesList}") ? $.parseJSON("${coachMessagesList}") : undefined,
        loggedIn_UserId = '${session.USER.userId}';

    $(function(){
        $(document).foundation();

        coachMessages = Object.create(CoachMessagesAll).init("${pageContext.request.contextPath}", patId).buildThreadedView(SMS_LIST);

        //load coaches area via ajax
        //Ajaxing.loadExternalPage($("#divCoachesAreaLeft"), "${pageContext.request.contextPath}/educator/coachesArea.jsp", loadExpCollapse);
        Ajaxing.loadExternalPage($("#divMemberNavArea"), "${pageContext.request.contextPath}/educator/memberNavArea.jsp");
    });

</script>
</body>
</html>