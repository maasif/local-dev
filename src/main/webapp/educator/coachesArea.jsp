<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/chosen_v1.4.0/chosen.min.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/timepicker/jquery.timepicker.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/timepicker/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css?v=0.16" />

    <style>
        .ui-timepicker-wrapper{
            width: 9.5em !important;
        }
    </style>
</head>
  
  <body>

  <%--<div class="coach-section-loading">
    <label class="inline margin-bottom-0 meal-summary-text"><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
  </div>--%>

  <!-- Primary Food Coach area -->
  <div id="divPrimaryFoodCoach" class="grey-area-section hide primary-coach-glow">
      <div class="row row-auto">
          <div class="green-header">
              <div class="columns medium-12 padding-left-0">
                  <h5>Primary Food Coach</h5>
              </div>
          </div>
      </div>

      <div class="grey-section-data">
          <a id="btnAssignToMe" href="javascript:;" data-patid="-1" class="button radius tiny btn-teal margin-top-0 assign-to-me">Assign to me</a>
      </div>
  </div>

  <!-- Coaches To-Do-List area -->
  <div class="grey-area-section">

      <div class="row row-auto">
          <div class="green-header">
              <div class="columns medium-12 padding-left-0">
                  <h5>Coach To-Do List</h5>
              </div>
              <%--<div class="columns medium-4 align-right padding-right-0">
                  <a id="btnViewAllTodoList" href="#" class="teal-text">View all ></a>
              </div>--%>
          </div>
      </div>

          <div class="grey-section-data">

              <div id="coachSessionWeek" class="coach-name">
                  <span class="sp-coach-todo-week">N/A - N/A</span>
                  <p class="coach-todo-nav hide">
                      <a id="btnPrevTodo" data-nav="prev" class="coach-todo-disable" disabled="true" href="javascript:;"><img src="${pageContext.request.contextPath}/resources/css/images/small-left.png" title="Previous Week To-dos"></a>
                      <a id="btnNextTodo" data-nav="next" href="javascript:;"><img src="${pageContext.request.contextPath}/resources/css/images/small-right.png" title="Next Week To-dos"></a>
                  </p>
              <div class="to-do-list-scrollable">
                  <ul id="coachTodoList" class="side-nav session-checklist">

                  </ul>

                  <div id="loadingCoachTodoList" class="coach-todo-loading">
                      <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                  </div>
              </div>
          </div>
      </div>
</div>

  <!-- Member Key Facts area -->
  <div class="grey-area-section">

      <div class="grey-section-data">
          <div class="row row-auto">
              <div class="green-header">
                  <div class="columns medium-8 padding-left-0">
                      <h5>Member's Key Facts</h5>
                  </div>
                  <div class="columns medium-4 align-right padding-right-0">
                      <a id="btnViewAllMemberKeyFacts" href="#" class="teal-text">View all ></a>
                  </div>
              </div>
          </div>
          <p id="pMemberKeyFactCoachDate" class="coach-name hide">
              N/A - N/A
          </p>
          <p id="pMemberKeyFacts" class="coach-notes hide">
              __________
          </p>
          <p id="emptyMemberKeyFacts" class="align-center hide">No member key facts added</p>
          <textarea id="txtMemberKeyFacts" class="margin-bottom-0" rows="6" placeholder="Type key facts..."></textarea>
          <small id="errMemberKeyFacts" class="error" style="display: none">Please add key facts.</small>
      </div>

      <div class="grey-section-buttons margin-top-10">
          <a href="#" id="btnAddMemberKeyFacts">
              <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/add_food_icon.png" width="24"/>
              <span class="teal-text">ADD KEY FACTS</span>
          </a>
      </div>
  </div>

  <!-- Coaches Notes area -->
  <div class="grey-area-section">

      <div class="row row-auto">
          <div class="green-header">
              <div class="columns medium-8 padding-left-0">
                  <h5>Coaches Notes</h5>
              </div>
              <div class="columns medium-4 align-right padding-right-0">
                  <a id="btnViewAllCoachNotes" href="#" class="teal-text">View all ></a>
              </div>
          </div>
      </div>

      <div class="grey-section-data">

          <p id="coachNotesNameDate" class="coach-name hide">
              N/A - N/A
          </p>
          <p id="coachNotes" class="coach-notes hide">
              ____
          </p>

          <p id="emptyCoachNotes" class="align-center hide">No notes</p>
      </div>

      <div class="grey-section-buttons">
          <a id="btnAddNote" href="#">
              <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/add_food_icon.png" width="24"/>
              <span class="teal-text">ADD NOTE</span>
          </a>
      </div>
  </div>

  <!-- 1:1 Session area -->
  <div class="grey-area-section">

      <div class="row row-auto">
          <div class="green-header">
              <div class="columns medium-12 padding-left-0">
                  <h5>1:1 Session</h5>
              </div>
          </div>
      </div>

      <div class="row row-auto">
          <div class="green-header">
              <div class="columns medium-8 padding-left-0">
                  <h5 class="font-size-16">Next Session</h5>
              </div>
              <div class="columns medium-4 align-right padding-right-0">
                  <a id="btnViewAllOneToOneSession" href="#" class="teal-text">View all ></a>
              </div>
          </div>
      </div>

      <div class="grey-section-data">

          <%--<p id="nextSession" class="coach-name hide">
              Next Session
          </p>--%>
          <p id="nextSessionDetails" class="coach-notes hide">
              <span id="sessionDate" class="sp-black"> N/A </span>
              <span class="uppercase coach-name">With</span>
              <span id="sessionCoachName" class="sp-black"> N/A</span>
              <span id="sessionCoachType" class="sp-black italic"> N/A</span>
          </p>

          <p id="emptyOneToOneSession" class="align-center hide">No sessions</p>
      </div>

      <div class="grey-section-buttons border-bottom-grey">
          <a id="btnAddOneToOneSession" href="#">
              <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/add_food_icon.png" width="24"/>
              <span class="teal-text">ADD SESSION</span>
          </a>
      </div>

      <div class="row row-auto">
          <div class="green-header">
              <div class="columns medium-8 padding-left-0">
                  <h5 class="font-size-16">Member Scheduling Preferences</h5>
              </div>
              <div class="columns medium-4 align-right padding-right-0">
                  <a id="btnEditSchedulingPreferences" href="#" class="edit-info"><span><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/edit-info.png" title="Edit"></span></a>
              </div>
          </div>
      </div>

      <div class="grey-section-data">
          <div id="divMemberSessionPreference" class="hide">
              <p class="coach-name">
                  Days
              </p>
              <p id="pMemberSessionPreferenceDays" class="coach-notes margin-bottom-0">
                  N/A
              </p>

              <p class="coach-name">
                  Times
              </p>
              <p id="pMemberSessionPreferenceTimes" class="coach-notes">
                  N/A
              </p>
          </div>
          <p id="emptyMemberSessionPreference" class="align-center">No scheduling preferences set</p>
      </div>

  </div>

  <!-- Patient Coaches area -->
  <div class="grey-area-section">
      <div class="row row-auto">
          <div class="green-header">
              <div class="columns medium-12 padding-left-0">
                  <h5>Member's Coaches</h5>
              </div>
          </div>
      </div>

      <div class="grey-section-data">

          <div class="row row-auto">
                <div class="padding-10 align-center">
                    <a href="#" id="btnPatientCoachesDialog">View all > </a>
                </div>
          </div>

      </div>
  </div>

  <!-- Diabetes Info area -->
  <div class="grey-area-section">
      <div class="row row-auto">
          <div class="green-header">
              <div class="columns medium-8 padding-left-0">
                  <h5>Diabetes Info</h5>
              </div>
              <div class="columns medium-4 align-right padding-right-0">
                  <a href="#" class="edit-info"><span><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/edit-info.png" title="Edit"></span></a>
              </div>
          </div>
      </div>

      <div class="grey-section-data">
          <div class="row row-auto">
              <div class="columns medium-12 padding-left-0">
                  <div class="grey-label-group">
                      <label class="inline uppercase margin-bottom-0">Diagnosis</label>
                      <label id="caDiagnosis" class="inline margin-bottom-0">N/A</label>
                  </div>
              </div>
          </div>
          <div class="row row-auto">
              <div class="columns medium-12 padding-left-0">
                  <div class="grey-label-group">
                      <label class="inline uppercase margin-bottom-0">Year of Diagnosis</label>
                      <label id="caDiagnosisDate" class="inline margin-bottom-0">N/A</label>
                  </div>
              </div>
          </div>
          <div class="row row-auto">
              <div class="columns medium-12 padding-left-0">
                  <div class="grey-label-group">
                      <label class="inline uppercase margin-bottom-0">Time since last Diabetes Education</label>
                      <label id="caTimeSinceLastEdu" class="inline margin-bottom-0">N/A</label>
                  </div>
              </div>
          </div>
          <div class="row row-auto">
              <div class="columns medium-12 padding-left-0">
                  <div class="grey-label-group">
                      <label class="inline uppercase margin-bottom-0">Diabetes Support</label>
                      <label id="caDiabetesSupport" class="inline margin-bottom-0">N/A</label>
                  </div>
              </div>
          </div>
          <div class="row row-auto hide">
              <div class="columns medium-12 padding-left-0">
                  <div class="grey-label-group">
                      <label class="inline uppercase margin-bottom-0">Medications</label>
                      <label id="caMedications" class="inline margin-bottom-0">N/A</label>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <!-- Dietary Information area -->
  <div class="grey-area-section">
      <div class="row row-auto">
          <div class="green-header">
              <div class="columns medium-8 padding-left-0">
                  <h5>Dietary Info</h5>
              </div>
              <div class="columns medium-4 align-right padding-right-0">
                  <a href="#" class="edit-info"><span><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/edit-info.png" title="Edit"></span></a>
              </div>
          </div>
      </div>

      <div class="grey-section-data">
          <div class="row row-auto">
              <div class="columns medium-12 padding-left-0">
                  <div class="grey-label-group">
                      <label class="inline uppercase margin-bottom-0">Dietary Restrictions</label>
                      <label id="caDietaryRestrictions" class="inline margin-bottom-0">N/A</label>
                  </div>
              </div>
          </div>
          <div class="row row-auto">
              <div class="columns medium-12 padding-left-0">
                  <div class="grey-label-group">
                      <label class="inline uppercase margin-bottom-0">Primary Meal Preparer</label>
                      <label id="caMealPreparer" class="inline margin-bottom-0">N/A</label>
                  </div>
              </div>
          </div>
      </div>

      <div class="grey-section-data medication-area-border">
          <div class="row row-auto">
              <div class="green-header">
                  <div class="columns medium-8 padding-left-0">
                      <h5>Medications</h5>
                  </div>
                  <div class="columns medium-4 align-right padding-right-0">
                      <%--<a id="btnViewAllMedications" href="#" class="teal-text">View all ></a>--%>
                  </div>
              </div>
          </div>

          <div class="medication-add-area">
              <ul id="medicationsList" class="side-nav margin-tb-0 padding-tb-0">

              </ul>
          </div>

          <div class="row row-auto margin-top-10">
              <div class="columns large-1 padding-left-0">
                  <label class="inline margin-bottom-0">
                      <input id="chkMemberConsent" class="large-radio margin-bottom-0" type="checkbox" />
                  </label>
              </div>
              <div class="columns large-11">
                  <label class="inline margin-bottom-0">
                    The member confirmed it's what they are taking
                  </label>
              </div>
          </div>

          <div class="medication-add-area remove-border">
              <label class="inline margin-bottom-0">
                  Select medications
                  <select id="ddMedications" autocomplete="off" class="margin-bottom-0"></select>
              </label>
              <label class="inline margin-bottom-0">
                  Dosage
                  <input type="text" id="txtDosage" class="margin-bottom-0"/>
                  <small id="errMedications" class="error" style="display: none">Please select medication and its dosage.</small>
              </label>

              <div class="grey-section-buttons">
                  <a href="#" id="btnAddMedication">
                      <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/add_food_icon.png" width="24"/>
                      <span class="teal-text">ADD MEDICATION</span>
                  </a>
              </div>
              <%--<div id="divOverlayMedication" class="overlay-layer"></div>--%>
          </div>

      </div>

      <div class="grey-section-data">
          <div class="row row-auto">
              <div class="green-header">
                  <div class="columns medium-8 padding-left-0">
                      <h5>Dietary Notes</h5>
                  </div>
                  <div class="columns medium-4 align-right padding-right-0">
                      <a id="btnViewAllDietaryNotes" href="#" class="teal-text">View all ></a>
                  </div>
              </div>
          </div>
          <p id="pDietaryCoach" class="coach-name hide">
              N/A - N/A
          </p>
          <p id="pDietaryCoachNotes" class="coach-notes hide">
              __________
          </p>
          <p id="emptyDietaryNotes" class="align-center hide">No notes</p>
          <textarea id="txtDietaryNotes" class="margin-bottom-0" rows="6" placeholder="Type note..."></textarea>
          <small id="errAddDietaryNotes" class="error" style="display: none">Please add notes.</small>
      </div>

      <div class="grey-section-buttons margin-top-10">
          <a href="#" id="btnAddDietaryNotes">
              <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/add_food_icon.png" width="24"/>
              <span class="teal-text">ADD NOTE</span>
          </a>
      </div>

  </div>

  <!-- Demographics area -->
  <div class="grey-area-section">
      <div class="row row-auto">
          <div class="green-header">
              <div class="columns medium-8 padding-left-0">
                  <h5>Demographics</h5>
              </div>
              <div class="columns medium-4 align-right padding-right-0">
                  <a href="#" class="edit-info"><span><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/edit-info.png" title="Edit"></span></a>
              </div>
          </div>
      </div>

      <div class="grey-section-data">
          <div class="row row-auto">
              <div class="columns medium-6 padding-left-0">
                  <div class="grey-label-group">
                      <label class="inline uppercase margin-bottom-0">Gender</label>
                      <label id="caGender" class="inline margin-bottom-0">N/A</label>
                  </div>
              </div>
              <div class="columns medium-6">
                  <div class="grey-label-group">
                      <label class="inline uppercase margin-bottom-0">dob</label>
                      <label id="caDob" class="inline margin-bottom-0">N/A</label>
                  </div>
              </div>
          </div>
          <div class="row row-auto">
              <div class="columns medium-12 padding-left-0">
                  <div class="grey-label-group">
                      <label class="inline uppercase margin-bottom-0">Race</label>
                      <label id="caRace" class="inline margin-bottom-0">N/A</label>
                  </div>
              </div>
          </div>
          <div class="row row-auto">
              <div class="columns medium-12 padding-left-0">
                  <div class="grey-label-group">
                      <label class="inline uppercase margin-bottom-0">Education</label>
                      <label id="caEducation" class="inline margin-bottom-0">N/A</label>
                  </div>
              </div>
          </div>
      </div>

  </div>

  <!-- HB-A1C Logging area -->
  <div class="grey-area-section remove-border">

      <div class="grey-section-data">
          <div class="row row-auto">
              <div class="green-header">
                  <div class="columns medium-8 padding-left-0">
                      <h5>Hemoglobin A1C</h5>
                  </div>
                  <div class="columns medium-4 align-right padding-right-0">
                      <a id="btnViewAllCoachLogs" href="#" class="teal-text">View all ></a>
                  </div>
              </div>
          </div>
          <p id="coachLogDate" class="coach-name hide">
              N/A - N/A
          </p>

          <p id="coachLog" class="coach-notes hide">
              __________
          </p>

          <p id="emptyCoachLog" class="align-center hide">No Hemoglobin A1C added</p>
          <textarea id="txtCoachLog" class="margin-bottom-10" rows="6" placeholder="Type Hemoglobin A1C..."></textarea>
          <input type="text" id="txtCoachLogDate" class="margin-bottom-0" placeholder="Select Date Taken"/>
          <small id="errCoachLog" class="error" style="display: none">Please add Hemoglobin A1C.</small>
          <small id="errCoachLogDate" class="error" style="display: none">Please select date.</small>
      </div>

      <div class="grey-section-buttons margin-top-10">
          <a href="#" id="btnAddCoachLogs">
              <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/add_food_icon.png" width="24"/>
              <span class="teal-text">Add Hemoglobin A1C</span>
          </a>
      </div>
  </div>

  <!-- Add Notes Dialog -->
  <div id="addNotesDialog" class="reveal-modal medium">
      <div class="columns small-12">
          <div id="addNotesDialogError" class="alert-box alert alert-message-abs" style="display: none;">______________________</div>
          <h3 class="meal-summary-text">Add Notes</h3>
          <textarea id="txtAddNotes" rows="12" placeholder="Add notes..."></textarea>
          <label class="inline margin-bottom-0 font-bold width-400">
              Notify Coaches <br/>
              <select id="ddNotifyCoaches" data-placeholder="(select)" multiple></select>
          </label>

          <label id="spLeadPrimaryDefaulText" class="inline margin-bottom-0">
            <span class="red-text font-bold">Note:</span> Lead Coach <span id="spLeadCoachName" class="label warning coach-designation-label primary-coach coach-name-notes"></span> & Primary Food Coach <span id="spPrimaryFoodCoachName" class="label warning coach-designation-label lead-coach coach-name-notes"></span> are notified by default.
          </label>
          <div class="align-right">
            <a href="#" id="btnAddNotesDialog" class="button small radius btn-teal">Save</a>
          </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- All Coach Notes Dialog -->
  <div id="allCoachNotesDialog" class="reveal-modal medium">
      <div class="columns small-12">
          <h3 class="meal-summary-text">Coaches Notes</h3>
          <div class="dialog-area">
              <ul id="dialogCoachList" class="side-nav teal-bullets">

              </ul>
          </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- All Dietary Notes Dialog -->
  <div id="allDietaryNotesDialog" class="reveal-modal medium">
      <div class="columns small-12">
          <h3 class="meal-summary-text">Dietary Notes</h3>
          <div class="dialog-area">
              <ul id="dietaryNotesList" class="side-nav teal-bullets">

              </ul>
          </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- All Member Key Facts Dialog -->
  <div id="memberKeyFactsDialog" class="reveal-modal medium">
      <div class="columns small-12">
          <h3 class="meal-summary-text">Member's Key Facts</h3>
          <div class="dialog-area">
              <ul id="memberKeyFactsList" class="side-nav teal-bullets">

              </ul>
          </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- Patient Coaches Dialog -->
  <div id="patientCoachesDialog" class="reveal-modal medium">
      <div class="columns small-12">
          <h3 class="meal-summary-text">Member's Coaches</h3>
          <div class="dialog-area">
              <table id="tableMyCoaches" class="table border-none coach-session">
                  <thead>
                  <tr>
                      <th>Coach Name</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Type</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
              </table>
          </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- Add 1:1 Session Dialog -->
  <div id="addOneToOneSessionDialog" class="reveal-modal small">
      <div class="columns small-12">
          <div id="addOneToOneSessionDialogError" class="alert-box alert alert-message-abs" style="display: none;">______________________</div>
          <h3 class="meal-summary-text">Add 1:1 Session</h3>
          <label class="inline margin-bottom-0">
            Choose Coach
            <select id="ddCoachesList"></select>
          </label>
          <label class="inline margin-bottom-0">
              Select Date
              <input type="text" id="txtAddSessionDate" class="margin-bottom-0" />
          </label>
          <label class="inline margin-bottom-0">
              Select Time
              <input type="text" id="txtAddSessionTime" placeholder="-- Select --" class="margin-bottom-0" />
          </label>
          <div class="align-right">
              <a href="#" id="btnAddOneToOneSessionDialog" class="button small radius btn-teal">Save</a>
              <a href="#" id="btnCancelOneToOneSessionDialog" class="button radius small secondary hide">Cancel</a>
          </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- All 1:1 Sessions List Dialog -->
  <div id="allOneToOneSessionsDialog" class="reveal-modal medium">
      <div class="columns small-12">
          <h3 class="meal-summary-text">1:1 Sessions</h3>
          <div class="dialog-area">
              <table id="dialogOneToOneSessionList" class="table hs-table border-none dataTable">
                <thead>
                    <tr>
                        <th>Sessions</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody></tbody>
              </table>
          </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- All Coach To-do list Dialog -->
  <div id="coachTodoListDialog" class="reveal-modal medium">
      <div class="columns small-12">
          <h3 class="meal-summary-text">Coach To-Do List</h3>
          <div class="dialog-area">
              <ul id="dialogCoachTodoList" class="side-nav session-checklist">

              </ul>
          </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- All Medications Dialog -->
  <div id="patientMedicationsDialog" class="reveal-modal medium">
      <div class="columns small-12">
          <h3 class="meal-summary-text">Member's Medications</h3>
          <div class="dialog-area">
              <table id="tableMedications" class="table border-none coach-session">
                  <thead>
                  <tr>
                      <th>Medication Name</th>
                      <th>Dosage</th>
                      <th>Date</th>
                      <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
              </table>
          </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- All Coach Log Dialog -->
  <div id="allCoachLogsDialog" class="reveal-modal medium">
      <div class="columns small-12">
          <h3 class="meal-summary-text">Hemoglobin A1C</h3>

          <div class="dialog-area">
              <ul id="dialogCoachLogList" class="side-nav teal-bullets">

              </ul>
          </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- Member Eating/Session Preferences Dialog -->
  <div id="memberEatingPreferencesDialog" class="reveal-modal medium">
      <h3 class="meal-summary-text">Member's Scheduling Preferences</h3>
      <div id="errorSchedulingPreference" class="alert-box alert alert-message-abs" style="display: none;">______________________</div>
      <div class="dialog-area">
          <div class="session-days-container">
              <h4 class="teal-text">Days <span class="general-text font-size-12 right">*(Check all that apply)</span> </h4>
              <ul class="medium-block-grid-3">
                  <li>
                      <div class="row row-auto">
                          <div class="columns medium-1 padding-left-0">
                              <input type="checkbox" value="Weekdays" class="large-radio session-days" />
                          </div>
                          <div class="columns medium-11">
                              <label class="inline">Weekdays</label>
                          </div>
                      </div>
                  </li>
                  <li>
                      <div class="row row-auto">
                          <div class="columns medium-1 padding-left-0">
                              <input type="checkbox" value="Saturdays" class="large-radio session-days" />
                          </div>
                          <div class="columns medium-11">
                              <label class="inline">Saturdays</label>
                          </div>
                      </div>
                  </li>
                  <li>
                      <div class="row row-auto">
                          <div class="columns medium-1 padding-left-0">
                              <input type="checkbox" value="Sundays" class="large-radio session-days" />
                          </div>
                          <div class="columns medium-11">
                              <label class="inline">Sundays</label>
                          </div>
                      </div>
                  </li>
              </ul>
          </div>
          <div class="session-times-container">
              <h4 class="teal-text">Times</h4>
              <ul class="medium-block-grid-3">
                  <li>
                      <div class="row row-auto">
                          <div class="columns medium-1 padding-left-0">
                              <input type="checkbox" value="Before 9 AM" class="large-radio session-times" />
                          </div>
                          <div class="columns medium-11">
                              <label class="inline">Before 9 AM </label>
                          </div>
                      </div>
                  </li>
                  <li>
                      <div class="row row-auto">
                          <div class="columns medium-1 padding-left-0">
                              <input type="checkbox" value="9 AM - noon" class="large-radio session-times" />
                          </div>
                          <div class="columns medium-11">
                              <label class="inline">9 AM - noon</label>
                          </div>
                      </div>
                  </li>
                  <li>
                      <div class="row row-auto">
                          <div class="columns medium-1 padding-left-0">
                              <input type="checkbox" value="Lunch" class="large-radio session-times" />
                          </div>
                          <div class="columns medium-11">
                              <label class="inline">Lunch</label>
                          </div>
                      </div>
                  </li>
                  <li>
                      <div class="row row-auto">
                          <div class="columns medium-1 padding-left-0">
                              <input type="checkbox" value="Afternoon" class="large-radio session-times" />
                          </div>
                          <div class="columns medium-11">
                              <label class="inline">Afternoon</label>
                          </div>
                      </div>
                  </li>
                  <li>
                      <div class="row row-auto">
                          <div class="columns medium-1 padding-left-0">
                              <input type="checkbox" value="Evenings" class="large-radio session-times" />
                          </div>
                          <div class="columns medium-11">
                              <label class="inline">Evenings</label>
                          </div>
                      </div>
                  </li>
              </ul>
          </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
      <div class="align-right">
          <a href="#" id="btnSaveSchedulingPreferences" class="button small radius btn-teal">Save</a>
      </div>
  </div>

  <div id="medicationDeleteDialog" class="reveal-modal small" data-reveal>
      <h3 class="meal-summary-text">Remove Medication</h3>
      <p class="lead dialog-para">Are you sure you want to remove this Medication?</p>
      <a id="closeConfirmMedicationDialog" class="close-reveal-modal">&#215;</a>
      <div class="align-right">
          <a href="javascript:;" id="btnConfirmMedDelete" class="button radius small btn-teal margin-top-10 margin-bottom-0">Yes</a>
          <a href="javascript:;" class="button secondary radius margin-top-10 small margin-bottom-0" onclick="$('#closeConfirmMedicationDialog').click(); return false;">No</a>
      </div>
  </div>

  <div id="oneToOneSessionDeleteDialog" class="reveal-modal small" data-reveal>
      <h3 id="o2oDialogHeader" class="meal-summary-text">Cancel Session</h3>
      <p id="o2oDialogDescription" class="lead dialog-para">Are you sure you want to cancel this Session?</p>
      <a id="closeConfirmSessionDialog" class="close-reveal-modal">&#215;</a>
      <div class="align-right">
          <a href="javascript:;" id="btnConfirmMedSessionDelete" class="button small radius btn-teal margin-top-10 margin-bottom-0">Yes</a>
          <a href="javascript:;" class="button secondary radius margin-top-10 small margin-bottom-0" onclick="$('#closeConfirmSessionDialog').click(); return false;">No</a>
      </div>
  </div>

   <script src="${pageContext.request.contextPath}/resources/chosen_v1.4.0/chosen.jquery.min.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/timepicker/jquery.timepicker.min.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/jquery.mask.min.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/hs.coaches.area.js?v=0.26"></script>

      <script type="text/javascript">

        var ONE_TO_ONE_SESSIONS_LISTING,
            LATEST_ONE_TO_ONE_SESSION,
            COACH_NOTES_LISTING,
            COACH_LOGS_LISTING,
            DIETARY_NOTES_LISTING,
            COACHES_LISTING,
            MY_COACHES_LISTING,
            PATIENT_DETAILS,
            EXCLUDED_ME_COACH,
            LEAD_COACH_ID,
            PRIMARY_FOOD_COACH_ID,
            PATIENT_ID = patId,
            LOGGED_IN_USER_ID = loggedInUserID,
            CURRICULUM_API_ADDRESS = CURRICULUM_API_ADDRESS,
            MEDICATIONS_LISTING,
            PATIENT_MEDICATIONS,
            PATIENT_SESSION_PREFERENCE,
            MEMBER_KEY_FACTS,
            COACH_TYPE;

        $(function(){
		    $(document).foundation();
            setTimeout(function(){
                HSCoachesArea.init(PATIENT_ID, LOGGED_IN_USER_ID, "${pageContext.request.contextPath}");
            }, 3000);
        });

      </script>
  </body>          
</html>