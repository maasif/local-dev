<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Topics Overview</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css?v=0.14" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/colors.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/curriculum.css" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

    <style>

        .patient-nav-btn-list{
            left:-73px !important;
            width:700px;
        }
        .pat-exp-col{
            margin-left: -180px !important;
        }
    </style>
  </head>
  
  <body>

  <!-- BreadCrumb -->
   <div class="breadcrumb-green">
      <div class="max-width-1129">
          <ul class="breadcrumbs">
              <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                  <li>
                      <a href="unProcessedMeals.action">Meals</a>
                  </li>
              </s:if>
              <li>
                  <a href="members.action">My Members</a>
              </li>
              <li>
                  <a href="dashboard.action?patientId=${patientId}">Member Dashboard</a>
              </li>
              <li class="current">Topic Overview</li>
          </ul>
      </div>
   </div>

	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>			
			<p id="progressTxt" class="progress-text">We're saving data</p>
		</div>
	</div>

  <%--<div class="pat-exp-col">
      <div class="exp-col">
          <a href="#" id="btnExpColPatInfo"><img src="${pageContext.request.contextPath}/resources/css/images/arrow-up.png" title="Hide member information" width="16" /> <span>Hide member information</span></a>
      </div>
  </div>--%>

	<div class="dashboard-container margin-bottom-20">
	
	  <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>

          <input type="hidden" id="txtDateString" name="dashboardDateString">

          <div class="row max-width-1129">

              <div class="coach-area-expand top-132">
                  <a href="#" id="btnExpColCoachArea"><img src="${pageContext.request.contextPath}/resources/css/images/arrow-left.png" title="Hide" /></a>
              </div>

            <div class="columns medium-12">

                  <!-- Name, ID and settings row -->
                  <div class="row row-auto">

                      <div id="divEmpty" class="columns large-1">
                          &nbsp;
                      </div>
                      <div id="divPatNameId" class="columns large-5">
                          <div class="hs-page-heading">
                              <h2> <span id="divPatName"> <span id="spDisplayName"> <s:property value="%{patient.user.displayName}" ></s:property></span> | </span> <span id="divPatID" data-id="<s:property value='%{patient.patientId}' />"><s:property value='%{patient.patientId}' /></span> </h2>
                          </div>
                      </div>
                      <div class="columns medium-6 align-right">

                          <!-- messages, video call buttons etc. -->
                          <ul class="inline-list patient-nav-btn-list">
                              <li>
                                  <a href="fetchReportPage.action?patientId=${patientId}">
                                  <span>
                                      <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/more-charts.png" width="24">
                                  </span>Summary
                                  </a>
                              </li>
                              <li>
                                  <a href="coachPatientMessages.action?patientId=${patientId}">
                                      <span>
                                          <img src="${pageContext.request.contextPath}/resources/css/images/message.png" width="24">
                                      </span>Messages
                                      <span id="spUnreadCount" class="unread-count"><s:property value='%{messagesCount}'></s:property></span>
                                  </a>
                                  <div class="nav-divider"></div>
                              </li>
                              <li>
                                  <a href="caller.action?patientId=${patientId}">
                                    <span><img src="${pageContext.request.contextPath}/resources/css/images/video_call.png" width="24"></span>Video Call
                                  </a>
                                  <div class="nav-divider"></div>
                              </li>
                              <li>
                                  <a href="fetchLogbook.action?patientId=${patientId}">
                                      <span> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_logbook_2.png" width="24" /></span> Log Book
                                  </a>
                                  <div class="nav-divider"></div>
                              </li>
                              <li>
                                  <a id="btnEditMembersTeamNav" href="#">
                                      <span><img src="${pageContext.request.contextPath}/resources/css/images/provider.png" width="24"></span>Member's Team
                                  </a>
                                  <div class="nav-divider"></div>
                              </li>
                              <li>
                                  <a href="settingsPage.action?patientId=${patientId}">
                                      <span><img src="${pageContext.request.contextPath}/resources/css/images/settings.png" width="24"></span>Settings
                                  </a>
                                  <div class="nav-divider"></div>
                              </li>
                          </ul>

                      </div>
                  </div>

                <!-- Patient image, email and phone row -->
                <div id="divPatientInfo" class="row row-auto bg-brown">
                    <div class="columns large-1">
                        <s:if test="%{patient.imagePath != null}">
                            <img src="${pageContext.request.contextPath}${patient.imagePath}" class="patient-image" />
                        </s:if>
                        <s:else>
                            <img src="${pageContext.request.contextPath}/resources/css/images/default_profile_image.gif" class="patient-image" />
                        </s:else>
                    </div>
                    <div class="columns large-2">
                        <div class="label-group padding-left-94">
                            <label class="inline uppercase margin-bottom-0">Full Name</label>
                            <label id="patFullName" class="inline margin-bottom-0"><s:property value="%{patient.user.firstName}" ></s:property><br/><s:property value="%{patient.user.lastName}" ></s:property></label>
                        </div>
                    </div>
                    <div class="columns large-8">
                        <ul class="small-block-grid-5 member-details-banner">
                            <li>
                                <div class="label-group">
                                    <s:if test="%{patient.mrn != null}">
                                        <label class="inline uppercase margin-bottom-0">MRN</label>
                                        <label class="inline margin-bottom-0"><s:property value="%{patient.mrn}" ></s:property></label>
                                    </s:if>
                                    <s:else>
                                        <label class="inline uppercase margin-bottom-0">MRN</label>
                                        <label class="inline margin-bottom-0">N/A</label>
                                    </s:else>
                                </div>
                            </li>
                            <li>
                                <div class="label-group">
                                    <s:if test="%{meterSerialNumber == 'N/A'}">
                                        <label class="inline uppercase margin-bottom-0">Birth date</label>
                                        <label class="inline margin-bottom-0"><s:property value="%{memberDOB}" ></s:property></label>
                                    </s:if>
                                    <s:else>
                                        <label class="inline uppercase margin-bottom-0">Meter Number</label>
                                        <label class="inline margin-bottom-0"><s:property value="%{meterSerialNumber}"></s:property></label>
                                    </s:else>
                                </div>
                            </li>
                            <li>
                                <div class="label-group">
                                    <label class="inline uppercase margin-bottom-0">Phone Number</label>
                                    <s:if test="%{patient.user.phone != null}">
                                        <label id="dashboardPhone" class="inline margin-bottom-0"><s:property value="%{patient.user.phone}" ></s:property></label>
                                    </s:if>
                                    <s:else>
                                        <label class="inline margin-bottom-0">N/A</label>
                                    </s:else>
                                </div>
                            </li>
                            <li>
                                <div class="label-group">
                                    <label class="inline uppercase margin-bottom-0">Phone / App Version</label>
                                    <label class="inline margin-bottom-0"><s:property value="%{phoneVersion}" ></s:property></label>
                                </div>
                            </li>
                            <li>
                                <div class="label-group">
                                    <label class="inline uppercase margin-bottom-0">Email</label>
                                    <label class="inline margin-bottom-0 word-break"><s:property value="%{patient.user.email}" ></s:property></label>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="columns large-2 align-right">
                        <label class="inline margin-bottom-0 padding-top-13"><a href="addEditPatient.action?patientId=${patientId}" class="button tiny radius btn-teal margin-bottom-0">Edit Member Details</a></label>
                    </div>
                </div>

                  <!-- Curriculum descriptions, details -->
                  <div class="row bg-white row-auto" data-equalizer>

                      <!-- Left white area -->
                      <div class="columns medium-9 padding-lr-0 summary-area equal-areas bg-white" data-equalizer-watch>

                          <div class="curriculum-loading">
                              <label class="inline italic margin-bottom-0 meal-summary-text" id="connecting"><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                          </div>

                          <!-- Information covered, Activities area, curriculum -->
                          <iframe id="divCurriculumArea" src="${pageContext.request.contextPath}/curriculum/app/index.jsp#/overview?user=${uuid}" width="100%" height="100%" class="bg-white remove-border curriculum-iframe" onload="$('.curriculum-loading').hide()">

                          </iframe>

                      </div>

                      <!-- Left grey area -->
                      <div id="divCoachesAreaLeft" class="columns medium-3 bg-grey equal-areas" data-equalizer-watch>
                          <script>
                              var patId = '${patientId}',
                                      loggedInUserID = '${session.PROVIDER.providerId}';
                              $(document).ready(function(){
                                  $("#divCoachesAreaLeft").load("${pageContext.request.contextPath}/educator/coachesArea.jsp");
                              });
                          </script>
                      </div>

                  </div>
            </div>
          </div>
    </div>

  <!-- Goal Details Dialog -->
  <div id="goalDetailsDialog" class="reveal-modal medium confirm-dialog">
      <div class="columns small-12">
          <h3>GOAL DETAILS</h3>
          <p class="margin-bottom-0 grey">How important is this to me?</p>
          <div class="ratings-container">
              <ul id="importantScale" class="large-block-grid-5 small-block-grid-5 scales create-scales goal-progress-rating-dialog disabled">
                  <li class="position-relative" data-scale="1"><label class="inline align-center label-align-left scale-text not-at-all">Not at all</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>1</span></label></li>
                  <li class="position-relative" data-scale="2"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>2</span></label></li>
                  <li class="position-relative" data-scale="3"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>3</span></label></li>
                  <li class="position-relative" data-scale="4"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>4</span></label></li>
                  <li class="position-relative" data-scale="5"><label class="inline align-center label-align-left scale-text reaching-all">All the time</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>5</span></label></li>
              </ul>
          </div>

          <p class="margin-bottom-0 grey">How often do I expect to stick to my Action Plan?</p>
          <div class="ratings-container">
              <ul id="actionPlanScale" class="large-block-grid-5 small-block-grid-5 scales create-scales goal-progress-rating-dialog disabled">
                  <li class="position-relative" data-scale="1"><label class="inline align-center label-align-left scale-text not-at-all">Not at all</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>1</span></label></li>
                  <li class="position-relative" data-scale="2"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>2</span></label></li>
                  <li class="position-relative" data-scale="3"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>3</span></label></li>
                  <li class="position-relative" data-scale="4"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>4</span></label></li>
                  <li class="position-relative" data-scale="5"><label class="inline align-center label-align-left scale-text reaching-all">All the time</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>5</span></label></li>
              </ul>
          </div>

          <div class="grey-black-pair">
              <p class="margin-bottom-0 grey">Challenge</p>
              <p id="myChallenge" class="black">__</p>
          </div>

          <div class="grey-black-pair">
              <p class="margin-bottom-0 grey">Overcoming Challenge</p>
              <p id="overcomeChallenge" class="black">__</p>
          </div>

          <div class="grey-black-pair">
              <p class="margin-bottom-0 grey">Reward</p>
              <p id="myReward" class="black">__</p>
          </div>

      </div>
      <a id="closeGoalDetailsDialog" class="close-reveal-modal">&#215;</a>
  </div>

  <!-- Edit Member Team Dialog -->
  <div id="editMemberTeamDialog" class="reveal-modal medium">

      <div id="errMessageMemberTeamDialog" class="alert-box alert alert-message-abs">______________________</div>

      <h3 class="meal-summary-text remove-bold">Edit Member's Team</h3>
      <div class="selected-providers-list">
          <div class="row row-auto">
              <div class="columns medium-6 large-6 six">
                  <label class="inline margin-bottom-0 label-dialog">Food Coaches</label>
                  <ul id="lstFoodCoaches" class="side-nav prov-ul"></ul>
              </div>
              <div class="columns medium-6 large-6 six">
                  <label class="inline margin-bottom-0 label-dialog">Coaches</label>
                  <ul id="lstCoaches" class="side-nav prov-ul"></ul>
              </div>
          </div>
          <small id="errorMemberTeam" class="error hide">Required.</small>
      </div>
      <div class="row row-auto">
          <div class="columns small-12">
              <ul class="side-nav teal-bullets-diag padding-left-0">
                  <li>
                      <label id="lblLeadCoach" class="hide">
                          <span id="spLeadCoach"></span><span class='label warning coach-designation-label lead-coach'>Lead Coach</span>
                      </label>
                  </li>
                  <li>
                      <label id="lblPrimaryCoach" class="hide">
                          <span id="spPrimaryFoodCoach"></span><span class='label warning coach-designation-label primary-coach'>Primary Food Coach</span>
                      </label>
                  </li>
              </ul>
          </div>
      </div>
      <br/>
      <div class="align-right">
          <input id="btnSaveMemberTeam" type="submit" class="button radius btn-teal min-width-250 small" value="Save" />
      </div>

      <a id="closeDialogMemberTeam" class="close-reveal-modal">&#215;</a>

      <div id="dialogLoadingSpinner" class="dialog-loading">
          <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Processing...</label>
      </div>

  </div>

    <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
    <![endif]-->
    <!--[if (gte IE 9) | (!IE)]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
    <!--<![endif]-->

    <!-- Foundation 3 for IE 8 and earlier -->
    <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
    <![endif]-->

    <!-- Foundation 4 for IE 9 and later -->
    <!--[if gt IE 8]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
    <!--<![endif]-->
   <script src="${pageContext.request.contextPath}/resources/js/date.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>

   <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/jquery.mask.min.js"></script>
   <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
   <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/hs.patient.dashboard.js?v=0.12"></script>
  <script src="${pageContext.request.contextPath}/resources/js/hs.edit.members.team.js?v=0.4"></script>

      <script type="text/javascript">

        var patientDashboard = Object.create(HSPatientDashboard),
            CURRICULUM_API_ADDRESS = '${CURRICULUM_API_ADDRESS}',
            patId = '${patientId}',
            loggedInUserID = '${session.PROVIDER.providerId}'

        $(function(){
		    $(document).foundation();
            $("#patientsTab").addClass("selected-tab");
            $("#txtDateString").val("${dashboardDateString}");

            var curriculumParams = {
                patientId: "${patientId}",
                imagesPath: "${pageContext.request.contextPath}",
                uuid: "${uuid}",
                isFromDashboard: false,
                week: "${week}"
            };

            patientDashboard.init(curriculumParams);
            EditMembersTeam.init(${patientId}, "${pageContext.request.contextPath}");
        });

        function closeTwilioDialog(){
            //clearing refresh timer instance, once the dialog is closed
            clearInterval(TWILIO_REFRESH_TIMER);
            SHOULD_SHOW_LOADING = true;
            $("#twilioMessageDialog").foundation("reveal", "close");
        }

        //for calling from inside iframe angular controller next/prev
        function displayCoachTodoList(todoListString, week){

            var that = this,
                $latestTodoList = $("#coachTodoList").empty(),
                $coachSessionWeek = $("#coachSessionWeek span"),
                $dialogCoachTodoList = $("#dialogCoachTodoList").empty(),
                html = "",
                coachToDoList = [];

            if(todoListString){
                var coaching = todoListString.replace(/\*/g, '').replace(/\-/g, '').trim().split("\n");
                coachToDoList = coaching;
            }

            if(coachToDoList && coachToDoList.length > 0){
                $coachSessionWeek.text("Week " + week + " - Item 1 of " + coachToDoList.length);

                //$latestTodoList.text(coachToDoList[0]);

                //dialog coach to do list rendering
                $.each(coachToDoList, function(index, todo){
                    if(todo) {

                        html += "<li>"
                        + "<div class='row'>"
                        + "<div class='columns small-2'>"
                        + "<img src='${pageContext.request.contextPath}/resources/css/images/goal_icons/tick-selected.png'/>"
                        + "</div>"
                        + "<div class='columns small-10 padding-left-0'>"
                        + todo
                        + "</div>"
                        + "</div>"
                        + "</li>";
                    }
                });
                $latestTodoList.append(html);
            }else{
                $coachSessionWeek.text("Week " + week + " - Item 0 of 0");
                $latestTodoList.append("<p class='align-center'>No coach todo list.</p>");
                $dialogCoachTodoList.append("<li class='align-center'>No coach todo list.<li>");
            }
        }

      </script>
  </body>          
</html>