<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>
	<title>My Patients</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/timepicker/jquery.timepicker.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/timepicker/bootstrap-datepicker.css" />

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/logDetail.css" />
	
	<style type="text/css">
		.bs-example{
			margin: 20px;
		}
		.tab-content{
			display: block;
		}
		.heading-style{
			background-color: rgb(238, 238, 238);
		}
		
		body{
			background-color: #eee !important;
		}

		.footer{
			margin-top: 150px;
		}

		.main-page{
			box-shadow:none;
			border-radius:2px;			
		}
		
		.white-area{
			background-color: #fff;
			border: 1px solid #d8d8d8;
			margin-bottom: 20px !important;
		}
		
		.past-meals-btn{
			position: absolute;
			top: -35px;
		}

		.align-page{
			position: relative;
			height: 100%;
		}

        #footer{
            display: none;
        }

        .font-size-20{
            font-size: 20px;
        }

        .logbook-summary-text{
            color:#999999;
        }
	</style>
</head>
<body>

    <!-- BreadCrumb -->
    <div class="breadcrumb-green">
        <div class="max-width-1129">
            <ul class="breadcrumbs">
                <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                    <li>
                        <a href="unProcessedMeals.action">Meals</a>
                    </li>
                </s:if>
                <li>
                    <a href="members.action">My Members</a>
                </li>
                <li>
                    <a href="dashboard.action?patientId=${patientId}">Member Dashboard</a>
                </li>
                <li class="current">Log Book</li>
                <li class="current"><b>Member Name:</b> <s:property value='%{patient.user.displayName}' /></li>
            </ul>
        </div>
    </div>



	<div id="loading" class="loader" style="display: block;">
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>
			<div class="progress progress-striped active">
				<div class="progress-bar" role="progressbar" aria-valuenow="100"
					aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<!-- <p id="progressTxt" class="progress-text">We're uploading your content!!!</p> -->
		</div>
	</div>

    <div id="divMemberNavArea"></div>
    <div class="dashboard-container margin-bottom-20">
        <div class="row max-width-1129">

        <div class="coach-area-expand">
            <a href="#" id="btnExpColCoachArea"><img src="${pageContext.request.contextPath}/resources/css/images/arrow-right.png" title="Show" /></a>
        </div>

        <div class="columns medium-12">

            <!-- Charts, goals and Coach Notes area -->
            <div class="row bg-white row-auto" data-equalizer>

                <!-- Left white area -->
                <div class="columns medium-12 padding-lr-0 summary-area equal-areas" data-equalizer-watch>
	                 <div class="align-page">
		                <div class="padding-10">
                            <div class="row row-auto">

                                <div class="columns medium-6 wDate-lbl padding-left-0"><label class="font-30 inline logbook-summary-text"><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_logbook_2.png" width="49" /> Glucose Log Book</label>
                                    <a href="fetchLogbook.action?patientId=${patientId}" class="button radius btn-teal">Back to Logbook</a>
                                </div>

                                <div class="columns medium-6 wDate-lbl padding-left-0">
                                </div>
                                <div class="columns medium-6 padding-right-0">
                                    <div class="collapse row">
                                        <div class="small-2 columns"><button onclick="getPreWeek()" class="button btn-teal prefix">Prev</button></div>
                                        <div class="small-8 columns">
                                            <s:textfield id="selWeeklyGlucoseLogsDate" data-calId="btnDateFrom"
                                                cssClass="patient-detail-text date-style" name="weeklyGlucoseReportDateString"
                                                readonly="true"></s:textfield>
                                        </div>
                                        <div class="small-2 columns"><button id="logDateNext" onclick="getNextWeek()" class="button btn-teal postfix">Next</button> </div>
                                    </div>
                                 </div>
                            </div>
                            <div class="columns medium-12 padding-lr-0">
                                <table id="tblGlucoseLogs" class="table datatable-bordered">
                                    <thead>
                                        <th></th>
                                        <th>00:00</th>
                                        <th>02:00</th>
                                        <th>04:00</th>
                                        <th>06:00</th>
                                        <th>08:00</th>
                                        <th>10:00</th>
                                        <th>12:00</th>
                                        <th>14:00</th>
                                        <th>16:00</th>
                                        <th>18:00</th>
                                        <th>20:00</th>
                                        <th>22:00</th>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>

		                <s:hidden id="patientId" name="patientId"></s:hidden>

	                </div>
                </div>

                <!-- Left grey area -->
                <div id="divCoachesAreaLeft" class="columns medium-3 bg-grey equal-areas" style="display:none" data-equalizer-watch>
                    <script>
                        var patId = '${patientId}',
                                loggedInUserID = '${session.PROVIDER.providerId}';
                        $(document).ready(function(){
                            $("#divCoachesAreaLeft").load("${pageContext.request.contextPath}/educator/coachesArea.jsp");
                        });
                    </script>
                </div>

            </div>
         </div>
    </div>
    </div>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
 	<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>


	<script type="text/javascript">
	
		var oldLogsDate,
            //[2/10/2015]: added by, __oz
            FORMAT_DATE = "mm/dd/yy",
            TARGETS = ("${targetString}") ? $.parseJSON("${targetString}") : undefined,
            patId = '${patientId}',
            loggedInUserID = '${session.PROVIDER.providerId}',
            BASE_URL = '${pageContext.request.contextPath}/';
		
		$(function(){
            $('#unProcessedMeals').addClass('selected-tab');

            initLogBook();

            //load coaches area via ajax
            //Ajaxing.loadExternalPage($("#divCoachesAreaLeft"), "${pageContext.request.contextPath}/educator/coachesArea.jsp", loadExpCollapse);
            Ajaxing.loadExternalPage($("#divMemberNavArea"), "${pageContext.request.contextPath}/educator/memberNavArea.jsp");
        });

	</script>

    <script src="${pageContext.request.contextPath}/resources/js/hs.glucoselogbook.js"></script>
</body>
</html>