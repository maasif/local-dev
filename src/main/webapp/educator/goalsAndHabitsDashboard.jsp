<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	         
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->

    <title>Coach Assessment</title>
    <link rel="stylesheet"	href="${pageContext.request.contextPath}/resources/css/queue.style.css" />
    <link rel="stylesheet"	href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/goals_habits_dashboard.css" />

</head>

<body>

    <!-- BreadCrumb -->
    <div class="breadcrumb-green">
        <div class="max-width-1129">
            <ul class="breadcrumbs">
                <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                    <li>
                        <a href="unProcessedMeals.action">Meals</a>
                    </li>
                </s:if>
                <li>
                    <a href="members.action">My Members</a>
                </li>
                <li>
                    <a href="dashboard.action?patientId=${patientId}">Member Dashboard</a>
                </li>
                <li class="current">Coach Assessment</li>
                <li class="current"><b>Member Name:</b> <s:property value='%{patient.user.displayName}' /></li>
            </ul>
        </div>
    </div>

	<div id="loading" class="loader" style="display: block;">
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>
			<div class="progress progress-striped active">
				<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
		</div>
	</div>
	
	<!-- Coach Notes Dialog -->
	<div id="coachNotesDialog" class="reveal-modal medium confirm-dialog" data-reveal>   
		<div id="errMessageDialog" class="alert-box alert alert-message-abs alert-message-abs-dialog full-width-message" style="display:none">______________________</div>   			
   		<div class="dialog-area columns small-12">
	   		<h3>CDE/COACH'S ASSESSMENT</h3> 
	   		
	   		<div class="previous-assessments-container" style="display:none">		   		
		   		<div id="divPreviousAssessments" class="previous-assessments-list">
		   		
		   		</div>	   		
		   		<label id="btnLoadPreviousAssessments" class="inline show-previous"><a href="#">SHOW PREVIOUS ASSESSMENTS</a></label>
	   		</div>
	   			   		
   			<form id="formCoachAssessment" class="margin-bottom-0">
	   			<p>
	   				GOAL ACHIEVED:
	   				<select id="ddStatus" class="font-24 margin-bottom-0 large-dropdown" name="status">
	   					<option value="" selected="selected" disabled="disabled">(select)</option>
	   					<option value="Yes">Yes</option>
	   					<option value="No">No</option>
	   					<option value="Not Yet">Not Yet</option>
	   				</select>
	   			</p>  			    	
	    		<p class="margin-bottom-0">ADD NOTES:</p>
	    		<textarea id="txtNotes" name="notes" rows="6" cols=""></textarea>
	    		<a id="btnSaveCoachAssessment" href="#" class="button small margin-bottom-0 right radius btn-teal">SAVE</a>
    		</form>	
   		</div>   		
		<a id="closeGoalRatingDialog" class="close-reveal-modal">&#215;</a>	
		
	</div>
	
	<!-- Goal Details Dialog -->
	<div id="goalDetailsDialog" class="reveal-modal medium confirm-dialog" data-reveal>   
   		<div class="columns small-12">
   			<h3>GOAL DETAILS</h3>
    		<p class="margin-bottom-0 grey">How important is this to me?</p>    		
    		<div class="ratings-container">
	    		<ul id="importantScale" class="large-block-grid-5 small-block-grid-5 scales create-scales goal-progress-rating-dialog disabled">
	    			<li class="position-relative" data-scale="1"><label class="inline align-center label-align-left scale-text not-at-all">Not at all</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>1</span></label></li>
    				<li class="position-relative" data-scale="2"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>2</span></label></li>
    				<li class="position-relative" data-scale="3"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>3</span></label></li>
    				<li class="position-relative" data-scale="4"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>4</span></label></li>
    				<li class="position-relative" data-scale="5"><label class="inline align-center label-align-left scale-text reaching-all">All the time</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>5</span></label></li>
   				</ul>  		
    		</div>
    		    	
    		<p class="margin-bottom-0 grey">How often do I expect to stick to my Action Plan?</p>    		
    		<div class="ratings-container">
	    		<ul id="actionPlanScale" class="large-block-grid-5 small-block-grid-5 scales create-scales goal-progress-rating-dialog disabled">
	    			<li class="position-relative" data-scale="1"><label class="inline align-center label-align-left scale-text not-at-all">Not at all</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>1</span></label></li>
    				<li class="position-relative" data-scale="2"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>2</span></label></li>
    				<li class="position-relative" data-scale="3"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>3</span></label></li>
    				<li class="position-relative" data-scale="4"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>4</span></label></li>
    				<li class="position-relative" data-scale="5"><label class="inline align-center label-align-left scale-text reaching-all">All the time</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>5</span></label></li>
   				</ul>  		
    		</div>
    		
    		<div class="grey-black-pair">
	    		<p class="margin-bottom-0 grey">Challenge</p>
	    		<p id="myChallenge" class="black">__</p>
    		</div>
    		
    		<div class="grey-black-pair">
	   			<p class="margin-bottom-0 grey">Overcoming Challenge</p>
	   			<p id="overcomeChallenge" class="black">__</p>
   			</div>
   			
   			<div class="grey-black-pair">
	    		<p class="margin-bottom-0 grey">Reward</p>
	    		<p id="myReward" class="black">__</p>
    		</div>
    		    		
    		<a href="#" onclick="$('#closeGoalDetailsDialog').click()" class="button small secondary right margin-bottom-0 radius">CLOSE</a>	
   		</div>   		
		<a id="closeGoalDetailsDialog" class="close-reveal-modal">&#215;</a>	
		
	</div>
	
	<div id="infoSuccess" class=" alert success alert-new-message"></div>
	<input type="hidden" name="patientId" id="txtPatientId" />

	<div id="divMemberNavArea"></div>
    <div class="dashboard-container margin-bottom-20">
      <div class="row max-width-1129">

        <div class="coach-area-expand">
            <a href="#" id="btnExpColCoachArea"><img src="${pageContext.request.contextPath}/resources/css/images/arrow-right.png" title="Show" /></a>
        </div>

        <div class="columns medium-12">

            <!-- Charts, goals and Coach Notes area -->
            <div class="row bg-white row-auto" data-equalizer>

                <!-- Left white area -->
                <div class="columns medium-12 padding-lr-0 summary-area equal-areas" data-equalizer-watch>
                    <h4 class="coach-assessment-heading">Coach Assessment</h4>
                    <div id="tableDiv" class="main-page padding-top-28 top-0">
                        <div class="container" id="unprocessedTable">
                            <table id="table" class="table border-none">
                                <thead>
                                <tr>
                                    <th class="unprocessedTableColumn">Goal Category</th>
                                    <th>Goal</th>
                                    <th>Action Plan</th>
                                    <th>Goal Details</th>
                                    <th>Date Created</th>
                                    <th>Member's Self-Assessment <br/> (I'm reaching my goal)</th>
                                    <th>Last Rated On</th>
                                    <th>Rating History</th>
                                    <th>CDE/Coach's Assessment</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="align-center">
                        <a href="dashboard.action?patientId=${patientId}" class="button radius btn-teal">Go to Member Dashboard</a>
                    </div>
                </div>

                <!-- Left grey area -->
                <div id="divCoachesAreaLeft" class="columns medium-3 bg-grey equal-areas" style="display: none;" data-equalizer-watch>
					<script>
						var patId = '${patientId}',
								loggedInUserID = '${session.PROVIDER.providerId}';
						$(document).ready(function(){
							$("#divCoachesAreaLeft").load("${pageContext.request.contextPath}/educator/coachesArea.jsp");
						});
					</script>
                </div>

            </div>
         </div>
    </div>
    </div>

    <!-- Goals Rating Dialog -->
    <div id="goalRatingDialog" class="reveal-modal small" data-reveal>
        <div class="columns small-12">
            <h3 id="h3GoalName" class="meal-summary-text">Rating History</h3>
            <div class="dialog-area">
                <ul id="dialogGoalRatingList" class="side-nav goal-ratings-history">

                </ul>
            </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
    </div>

	<script	src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
    <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
    <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
    
	<script type="text/javascript">
		
		$.fn.dataTableExt.sErrMode = 'throw';
		
		var oTable,
			showRowCount = 0,
			ALERT_MESSAGES = Object.create(AlertMessage).init(),
			AJAXING = Object.create(Ajaxing),
			selectedFilledFormId = "",
            patId = '${patientId}',
            loggedInUserID = '${session.PROVIDER.providerId}',
            GOAL_RATING_HISTORY = ('${goalRatingsHistory}') ? $.parseJSON('${goalRatingsHistory}') : undefined,
            GOALS_RATING_BUNDLE = [];
		
		$(document).ready(function(){
			
			$(document).foundation();

			$('#patientsTab').addClass('selected-tab');
            loadGoalRatingsHistory();

			  oTable = $('#table').dataTable(
			  {
				"sDom" : "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'spanPagination'p>>",
				"sPaginationType" : "bootstrap",
				"oLanguage" : {
					"sLengthMenu" : "_MENU_ records per page" // how many records you want to display per page
				},
				"bServerSide": true,
				
		 		"sAjaxSource": "fetchGoalsDashboardData.action?patientId="+patId, // return jsonresponse with aaData json object
				"fnServerParams": function ( aoData ) {					      
				},
				"bRetrieve" : true, // retrieve datatble object
				"bPaginate" : true, // enable disable pagination
				"bStateSave" : true, // saveState in cookie
                "bFilter": false,
				"bSort" : true, 
				"aaSorting": [[ 4, "desc" ]],
				"bProcessing": true, // processing text in while processing
				"fnInitComplete": function(oSettings, json) {
                    $(".sorting_1").removeClass("sorting_1 sorting_1");
					loadRating();
					bindGoalDetailsDialog();
                    loadGoalRatingsHistory();
				 },
                  "fnDrawCallback": function( oSettings ) {
                      $(".sorting_1").removeClass("sorting_1 sorting_1");
                      loadRating();
                      bindGoalDetailsDialog();
                      loadGoalRatingsHistory();
                  },
	 			"aoColumns": [
                                { "mDataProp": function (data, type, full){
                                    if(data.goalCategoryName){
                                        return data.goalCategoryName;
                                    }

                                    return "N/A";
                                },"bSortable": false},
                                { "mDataProp": function (data, type, full){
                                    if(data.goalName){
                                        return data.goalName;
                                    }

                                    return "N/A";
                                },"bSortable": false},
                                    { "mDataProp": function (data, type, full){
                                        if(data.actionPlan){
                                            return data.actionPlan;
                                        }

                                    return "N/A";
                                },"bSortable": false},
			                   { "mDataProp": function (data, type, full){
			                	   return "<a href='javascript:;' data-formdata='"+JSON.stringify(data.formDataList)+"' class='button expand tiny radius btn-teal'>View</button></a>";
							   },"bSortable": false},

                                { "mDataProp": function (data, type, full){
                                    if(data.goalCreatedDate){
                                        return data.goalCreatedDate;
                                    }

                                    return "N/A";
                                }},
							   
							    { "mDataProp": function (data, type, full){			                					                	   
			                	   var rating = "", goalDetails = buildGoalDetailsArray(data.formDataList);
			                	   
			                	   if(data.goalRating > -1){
			                		   rating = "data-goalrating="+data.goalRating;
			                	   }

                                    if(data.goalRating == 0 || goalDetails.goalRating){
                                        rating = "data-goalrating="+goalDetails.goalRating;
                                    }

							    	return '<ul '+rating+' class="large-block-grid-5 small-block-grid-5 scales create-scales goal-progress-rating">'
						    				+ '<li class="position-relative" data-scale="1"><label class="inline align-center label-align-left scale-text not-at-all">Not<br/>at all</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>1</span></label></li>'
					    					+ '<li class="position-relative" data-scale="2"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>2</span></label></li>'
					    					+ '<li class="position-relative" data-scale="3"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>3</span></label></li>'
					    					+ '<li class="position-relative" data-scale="4"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>4</span></label></li>'
					    					+ '<li class="position-relative" data-scale="5"><label class="inline align-center label-align-left scale-text reaching-all">All the<br/>time</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>5</span></label></li>'
				   							+ '</ul>';
			                	   
							   }, 'bSortable': false},

                                { "mDataProp": function (data, type, full){
                                    if(data.goalRatedOn){
                                        return data.goalRatedOn;
                                    }

                                    return "N/A";
                                }},

                                { "mDataProp": function (data, type, full){

                                    var ratingHistory = GOALS_RATING_BUNDLE[data.filledFormId];
                                    if(ratingHistory){
                                        return "<a href='javascript:;' data-id='"+data.filledFormId+"' class='button btn-teal tiny radius margin-bottom-0 view-goal-rating-history'>View All Ratings</a>";
                                    }

                                    return "N/A";

                                }, 'bSortable': false},

							   { "mDataProp": function (data, type, full){
								   
								    var coachAssessments = data.coachGoalAssessments,
								    	html = "";
								    if(coachAssessments && coachAssessments.length > 0){
								    	
								    	var coach = coachAssessments[0];
								    	
								    	html += "<div class='single-assess'><div class='grey-black-pair small-pair'><p class='margin-bottom-0 grey'>Goal Achieved</p><p class='black'>" +coach.status+ "</p></div>"
							    			 + "<div class='grey-black-pair small-pair'><p class='margin-bottom-0 grey'>Notes</p><p class='black'>"+ coach.notes + "</p></div>"
							    			 + "<div class='grey-black-pair small-pair'><p class='margin-bottom-0 grey'>Date</p><p class='black'>"+ coach.assessedDateString + "</p></div>"
							    			 + "<div class='grey-black-pair small-pair'><p class='margin-bottom-0 grey'>Assessed By</p><p class='black'>"+ coach.assessedByName + "</p></div></div>";						    			 
								    }
								   
								   	return html + "<a href='#' data-id="+data.filledFormId+" data-coachassessments='"+JSON.stringify(data.coachGoalAssessments)+"' class='button tiny margin-bottom-0 add-notes radius btn-teal'>Add Assessment & Notes</a>";
							   },"bSortable": false}
			                 ]
			  }); 

			//fixed paging styling
			fixedPaginationStyling();

			//load coaches area via ajax
			//Ajaxing.loadExternalPage($("#divCoachesAreaLeft"), "${pageContext.request.contextPath}/educator/coachesArea.jsp", loadExpCollapse);
			Ajaxing.loadExternalPage($("#divMemberNavArea"), "${pageContext.request.contextPath}/educator/memberNavArea.jsp");

			$("#loading").hide();			
		}); 
			
		function bindGoalDetailsDialog(){
			var $a = $("a[data-formdata]").off("click");
			
			$a.on("click", function(){
				
				var formDataString = $(this).data("formdata");
				
				$.each(formDataString, function(index, formData){
					var $control = $("#"+ formData.fieldName);
					if($control){
						$control.text(formData.fieldValue);
					}
					
					if(formData.fieldName == "important"){
						checkUncheckScale($("#importantScale img"), formData.fieldValue);
					}
					
					if(formData.fieldName == "confident"){
						checkUncheckScale($("#actionPlanScale img"), formData.fieldValue);
					}
				});		
				
				$("#goalDetailsDialog").foundation("reveal", "open");
			});
		}

        function buildGoalDetailsArray(dataList){
            var goalDetails = {};

            //converting fieldname and fieldValue to single goalDetailsObject
            $.each(dataList, function(index, fd){
                var fieldName = fd.fieldName;
                goalDetails[fieldName] = fd.fieldValue;
            });

            return goalDetails;
        }

		function fillRating($scales, key){
			
			$scales.each(function(index, scale){
				var rating = $(scale).data(key),
					$children = $(scale).children("li");
				
				$children.each(function(index, child){
					
					if(index < rating){
						$(child).find("img").attr("src", "${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/checked.png");
					}
				});				
			});
		}
		
		function checkUncheckScale($items, scale){

	        $items.attr({"src": "${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png", "data-ischecked": false}); 
	        
	        $.each($items, function(index, img){
	        	if(index < scale){
	        		$(img).attr({"src": "${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/checked.png", "data-ischecked": true});
	        	}
	        });        
	    }
		
		function loadRating(){
			
			//goal rating
			var $goalRating = $("[data-goalrating]");			
			fillRating($goalRating, "goalrating");
			
			$(".add-notes").off("click").on("click", function(e){	
				e.preventDefault();
				selectedFilledFormId = $(this).data("id");
				
				var coachAssessments = $(this).data("coachassessments"),
					$previousAssessments = $(".previous-assessments-container").hide(),
					$container = $("#divPreviousAssessments").empty(),
					$btnLoadPreviousAssessments = $("#btnLoadPreviousAssessments"),
					html = "";
								
				$btnLoadPreviousAssessments.children("a").text("SHOW PREVIOUS ASSESSMENTS");
				
				if(coachAssessments && coachAssessments.length > 0){
					
					$previousAssessments.show();				
				    $container.hide().empty();
					
				    var headerHtml = "<div class='row'><div class='large-3 columns padding-left-0'><p class='grey margin-bottom-0'>GOAL ACHIEVED</p></div><div class='large-3 columns'><p class='grey margin-bottom-0'>NOTES</p></div><div class='large-3 columns'><p class='grey margin-bottom-0'>DATE</p></div><div class='large-3 columns'><p class='grey margin-bottom-0'>ASSESSED BY</p></div></div>";
				    				    
					$.each(coachAssessments, function(index, coach){
						html += "<div class='row single-assess grey-black-pair small-pair'><div class='large-3 columns padding-left-0'><p class='black'>" +coach.status+ "</p></div>"
			    			 + "<div class='large-3 columns'><p class='black'>"+ coach.notes + "</p></div>"
			    			 + "<div class='large-3 columns'><p class='black'>"+ coach.assessedDateString + "</p></div>"
			    			 + "<div class='large-3 columns'><p class='black'>"+ coach.assessedByName + "</p></div></div>";
					});	
					
					$container.append(headerHtml+html);
					
					$btnLoadPreviousAssessments.off("click").on("click", function(e){
						e.preventDefault();
						
						var $a = $(this).children("a"),
							hideShow = $a.text();
						
						if(hideShow == "SHOW PREVIOUS ASSESSMENTS"){
							$a.text("HIDE PREVIOUS ASSESSMENTS");
							$container.show();
						}else{
							$a.text("SHOW PREVIOUS ASSESSMENTS");
							$container.hide();
						}						
					});
				}
				
				$("#coachNotesDialog").foundation("reveal", "open");
			});
			
			$("#btnSaveCoachAssessment").off("click").on("click", function(e){
				e.preventDefault();
				
				var notes = $("#txtNotes").val(),
					status = $("#ddStatus").val(),
					$error = $("#errMessageDialog");
				
				if(!status){
					ALERT_MESSAGES.showAlertMessage($error, "Please choose 'GOAL ACHIEVED' status", ALERT_MESSAGES.ERROR);
					return;
				}

				if($.trim(notes).length == 0){
					ALERT_MESSAGES.showAlertMessage($error, "Please enter notes.", ALERT_MESSAGES.ERROR);
					return;
				}
				
	        	//ajax parimeters    	
	        	var dataToSend = { "notes" : notes, "status": status, "formId": selectedFilledFormId, "patientId":patId},
	        		postRequestActions = {    			
	        			"loading": $("#loading"),
	        			"error": $error,
	        			"successCallBack": onSaveGoalAssessment
	        		};    	
	        	        	
	        	AJAXING.sendPostRequest("saveCoachAssessment.action", dataToSend, postRequestActions);
			});
		}
		
		function onSaveGoalAssessment(data){
			
			if(data.STATUS == "SUCCESS"){
				$("#coachNotesDialog").foundation("reveal", "close");
				window.location = "goalsHabitsDashboard.action?patientId="+patId;
			}else{
				ALERT_MESSAGES.showAlertMessage($("#errMessageDialog"), "Unable to save coach's assessment, please try later.", ALERT_MESSAGES.ERROR);
			}
		}
		
		//fixed paging styling
		function fixedPaginationStyling(){
			var $ul = $(".dataTables_paginate ul");			
			$ul.each(function(){
				var $this = $(this);
				if($this && !$this.hasClass("pagination")){
					$this.addClass("pagination");
				}
			});
		}
					
		function refreshTable(){
			$("#infoSuccess").slideUp("slow"); 
			oTable.fnDraw();
			$("#loading").hide();			
		}

        function loadGoalRatingsHistory(){
            //buckle app goal ratings group by formIds
            GOALS_RATING_BUNDLE = [];
            if(GOAL_RATING_HISTORY && GOAL_RATING_HISTORY.length > 0){
                $.each(GOAL_RATING_HISTORY, function(index, goalRating){
                    var formId = goalRating.formId;
                    if(!GOALS_RATING_BUNDLE[formId]){
                        GOALS_RATING_BUNDLE[formId] = [];
                        GOALS_RATING_BUNDLE[formId].push(goalRating);
                    }else{
                        GOALS_RATING_BUNDLE[formId].push(goalRating);
                    }
                });
            }

            $(".view-goal-rating-history").off("click").on("click", function(e){
                e.preventDefault();
                var formId = $(this).data("id");
                loadGoalRatingHistoryInDialog(formId);
            });
        }

        function loadGoalRatingHistoryInDialog(filledFormId){
            var $ul = $("#dialogGoalRatingList").empty(),
                html = "",
                goalRatingsFromList = GOALS_RATING_BUNDLE[filledFormId];

            if(goalRatingsFromList){

                $.each(goalRatingsFromList, function(index, goalRating){

                    html += "<li><label class='inline margin-bottom-0'><span class='font-bold'>Rated On: </span>"+ goalRating.ratedOn + "</label><div class='grey-section-data'> <ul class='large-block-grid-5 small-block-grid-5 scales goal-progress-rating' data-goalrating='"+goalRating.rating+"'>"
                    + "<li class='position-relative'><label class='inline'><img src='${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png' /><span>1</span></label></li>"
                    + "<li class='position-relative'><label class='inline'><img src='${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png' /><span>2</span></label></li>"
                    + "<li class='position-relative'><label class='inline'><img src='${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png' /><span>3</span></label></li>"
                    + "<li class='position-relative'><label class='inline'><img src='${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png' /><span>4</span></label></li>"
                    + "<li class='position-relative'><label class='inline'><img src='${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png' /><span>5</span></label></li>"
                    + "</ul></div></li>";
                });

                $ul.append(html);

                var $scales = $("[data-goalrating]");
                fillRating($scales, "goalrating");

            }else{
                $ul.append("<li class='remove-border'><p class='align-center'>No goal ratings</p></li>");
            }

            $("#goalRatingDialog").foundation("reveal", "open");
        }
	</script>
</body>
</html>