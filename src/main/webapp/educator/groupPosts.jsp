<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>

	<!--[if (gte IE 9) | (!IE)]><!-->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />
	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/queue.style.css" />
	<title>DYK</title>
     <style type="text/css">
         .margin-bottom-20{
             margin-bottom: 30px;
         }

		 #postVideoDialog small.error{
			 margin-top: -16px;
		 }

		 .patient-nav-btn-list{
			 top: 15px;
			 position: relative;
		 }

		 .input-field {
			 width: 35% !important;
		 }

		 .ui-datepicker-trigger {
			 margin: 0px;
			 height: 37px;
			 padding-top: 11px;
			 width: 5px;
			 padding-left: 1rem;
			 background-color: #428bca;
		 }

		 .date-style {
			 width: 82%;
			 display: inline-block !important;
		 }

		 .header-style {
			 width: 60%;
			 padding: 0;
			 top: 10px;
		 }

		 .ui-datepicker-trigger {
			 margin: 0px;
			 height: 37px;
			 padding-top: 17px;
			 width: 5px;
			 padding-left: 1rem;
		 }

		 .dataTables_processing {
			 position: absolute;
			 top: 2% !important;
			 left: 50%;
			 width: 250px;
			 height: 30px;
			 margin-left: -125px;
			 margin-top: -15px;
			 padding: 14px 0 2px 0;
			 border: 0px !important;
			 text-align: center;
			 color: #999;
			 font-size: 14px;
			 background: inherit !important;
			 font-weight: bold;
			 color: brown;
		 }

		 .width-20perc{
			 width: 20%;
		 }
		 .alert-message-abs{
			 display: none;
			 top:82px;
			 position: absolute !important;
		 }

		 .video-link{
			 background-color: #04AEDA;
			 text-transform: uppercase;
		 }

		 .dyk{
			 background-color: #999;
		 }
		 .hyperlink{
			 background-color: #60A4FF;
			 text-transform: uppercase;
		 }

		 table tbody tr td:first-child{
			 text-align: left;
		 }
     </style>
</head>
<body>
<div id="infoSuccess" class=" alert success alert-new-message"></div>
<div id="infoError" class=" alert error alert-new-message"></div>
<br/>

<div id="loadingTable" class="loader">
	<div class="image-loader">
		<p class="progress-please-wait">Please wait...</p>
		<div class="progress progress-striped active">
			<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
			</div>
		</div>
		<p id="progressTxt" class="progress-text">We're working</p>
	</div>
</div>

<div class="max-width-1129 margin-bottom-20">
    <div class="row max-width-1129">
        <div class="columns medium-12">
            <div class="hs-page-heading">
                <h2>
                    Group Posts
					<ul class="inline-list patient-nav-btn-list right">
						<li>
							<a href="#" id="btnPostHyperLinkDialogOpen">
								<span><img src="${pageContext.request.contextPath}/resources/css/images/webpages.png" width="24"></span>Post Link
							</a>
						</li>
						<li>
							<a href="#" id="btnPostVideoDialogOpen">
								<span><img src="${pageContext.request.contextPath}/resources/css/images/video_call.png" width="24"></span>Post Video
							</a>
						</li>
						<li>
							<a href="#" id="btnPostDYKDialogOpen">
								<span> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_logbook_2.png" width="24" /></span> Post DYK
							</a>
							<div class="nav-divider"></div>
						</li>
					</ul>
                </h2>
            </div>
            <div id="tableDiv" class="main-page padding-top-28 top-0 green-bordered">
				<div class="container" id="unprocessedTable">
					<table id="tableGroupPosts" class="table border-none">
						<thead>
						<tr>
							<th class="width-20perc">Description</th>
							<th>Post Type</th>
							<th class="width-20perc">Link</th>
							<th>Posted By</th>
							<th>Last<br/>Posted<br/>Date</th>
							<th>Action</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
            </div>
        </div>
    </div>
</div>

<div id="postHyperlinkDialog" class="reveal-modal small" data-reveal>
	<h3 class="meal-summary-text remove-bold">Post Link</h3>
	<p class="lead dialog-para">
		<label>
			Description
			<textarea id="txtHyperlinkDescription" rows="4" placeholder="Add description" maxlength="250"></textarea>
			<small class="error hide">Required.</small>
		</label>
		<label>
			Link
			<input type="url" id="txtHyperlink" placeholder="http://www.abc.com">
			<small class="error hide">Required.</small>
		</label>
	</p>
	<a id="closePostHyperlinkDialog" class="close-reveal-modal">&#215;</a>
	<div class="align-right">
		<a href="javascript:;" id="btnPostHyperlinkDialog" class="button radius small btn-teal margin-top-10 margin-bottom-0">Post</a>
		<a href="javascript:;" id="btnCancelHyperlinkDialog" class="button secondary radius margin-top-10 small margin-bottom-0" onclick="$('#closePostHyperlinkDialog').click(); return false;">Cancel</a>
	</div>
</div>

<div id="postVideoDialog" class="reveal-modal small" data-reveal>
	<h3 class="meal-summary-text remove-bold">Post Video</h3>
	<p class="lead dialog-para">
		<label>
			Description
			<textarea id="txtVideoDescription" rows="4" placeholder="Add description" maxlength="250"></textarea>
			<small class="error hide">Required.</small>
		</label>
		<label>
			Video Link
			<input type="url" id="txtVideoLink" placeholder="http://www.abc.com" maxlength="250">
			<small class="error hide">Required.</small>
		</label>
	</p>
	<a id="closePostVideoDialog" class="close-reveal-modal">&#215;</a>
	<div class="align-right">
		<a href="javascript:;" id="btnPostVideoDialog" class="button radius small btn-teal margin-top-10 margin-bottom-0">Post</a>
		<a href="javascript:;" id="btnCancelVideoDialog" class="button secondary radius margin-top-10 small margin-bottom-0" onclick="$('#closePostVideoDialog').click(); return false;">Cancel</a>
	</div>
</div>

<div id="postDYKDialog" class="reveal-modal large" data-reveal>
	<h3 class="meal-summary-text remove-bold">Post DYK</h3>
	<div class="dialog-area">
		<display:table name="groupPostsList" requestURI="" class="table border-none" uid="gpRow" style="width: 100%;">
			<display:setProperty name="basic.empty.showtable" value="true" />
			<display:column property="adviceContent" title="Post"  style=" text-align:left;" />

			<display:column title="" style="width:10%; text-align:center; ">

				<button onclick="sendGroupPost(this, '<s:property value="#attr.gpRow.adviceId"/>')" class="button tiny margin-bottom-0 radius btn-teal expand send-btn">Send</button>

			</display:column>
		</display:table>
	</div>
	<a id="closePostDYKDialog" class="close-reveal-modal">&#215;</a>
	<div class="align-right">
		<a href="javascript:;" class="button secondary radius margin-top-10 small margin-bottom-0" onclick="$('#closePostDYKDialog').click(); return false;">Close</a>
	</div>
</div>

<div id="videoPlayerDialog" class="reveal-modal medium large" data-reveal aria-labelledby="videoModalTitle" aria-hidden="true" role="dialog">
	<h4 id="videoDialogHeader" class="meal-summary-text">Video Player</h4>
	<iframe id="divIframe" src="about:blank" frameborder="0" width="100%" height="500"></iframe>
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

	<script src="${pageContext.request.contextPath}/resources/js/hs.validator.js?v=0.1"></script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
 	<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>

<script type="text/javascript">

	$.fn.dataTableExt.sErrMode = 'throw';

	var AdviceIdToSend, $btnSend, oTable;
	
	$(function(){

		$(document).foundation();

		$('#DYKTab').addClass('selected-tab');
		bindGroupPostVideoEventListener();

		$("#btnPostHyperLinkDialogOpen").on("click", function(e){
			e.preventDefault();
			$("#postHyperlinkDialog").foundation("reveal", "open");
		});

		$("#btnPostVideoDialogOpen").on("click", function(e){
			e.preventDefault();
			$("#postVideoDialog").foundation("reveal", "open");
		});

		$("#btnPostDYKDialogOpen").on("click", function(e){
			e.preventDefault();
			$("#postDYKDialog").foundation("reveal", "open");
		});

		try {
			initDataTable("gpRow",{"sorting": [[0, "asc" ]]});

			oTable=  $('#tableGroupPosts').dataTable(
					{
						"sDom" : "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'spanPagination'p>>",
						"sPaginationType" : "bootstrap",
						"oLanguage" : {
							"sLengthMenu" : "_MENU_ records per page" // how many records you want to display per page
						},
						"fnInitComplete": function (oSettings, json) {
							//remove sorting class from td, __oz
							$(".sorting_1").removeClass("sorting_1 sorting_1");
						},
						"fnDrawCallback": function( oSettings ) {
							$(".sorting_1").removeClass("sorting_1 sorting_1");
							bindRowElementsClickEventListener();
						},
						"bServerSide": true ,

						"sAjaxSource": "fetchGroupPosts.action", // return jsonresponse with aaData json object
						"fnServerParams": function ( aoData ) {
						},

						"bRetrieve" : true, // retrieve datatble object
						"bPaginate" : true, // enable disable pagination
						"bStateSave" : false, // saveState in cookie
						"bSort" : true,
						"aaSorting": [[ 4, "desc" ]],
						"bProcessing": true, // processing text in while processing
						"aoColumns": [
							{ "mDataProp": function (data, type, full){
								var desc = data.description;

								if(desc.length > 50){
									desc = desc.substring(0, 50);
									return "<span title='"+data.description+"'>"+desc +"... </span>";
								}
								return desc;
							}},
							{ "mDataProp": function (data, type, full){
								if(data.postType){
									switch (data.postType){
										case "VIDEOURL":
										case "VIDEO":
											return "<span class='in-process-status label video-link'>Video</span>";
										case "DYK":
											return "<span class='in-process-status label dyk'>DYK</span>";
										case "HYPERLINK":
											return "<span class='in-process-status label hyperlink'>Link</span>";
									}
								}
								return "N/A";
							}},
							{ "mDataProp": function (data, type, full){
								var returnedString = "";

								switch (data.postType){
									case "VIDEOURL":
									case "VIDEO":
										var description = data.description;

										if(description.length > 30){
											var newDescription = description.substring(0, 30) + " ...";
											description = newDescription;
										}

										returnedString = "<a href='#' data-text='"+description+"' class='g-video-player' data-url='"+data.videoLink+"'><img src='"+data.videoThumbnail+"' width='60'> Click to Play</a>";
										break;
									case "DYK":
										returnedString =  "N/A";
										break;
									case "HYPERLINK":
										if(data.videoLink){
											var link = data.videoLink;

											if(!link.startsWith("http") && !link.startsWith("https")){
												link = "http://"+link;
											}

											returnedString = "Click <a href='"+link+"' target='_blank' />here</a> to view";
										}
										break;
								}

								return returnedString;
							}},
							{ "mDataProp": function (data, type, full){
								if(data.coachName){
									return data.coachName;
								}
								return "N/A";
							}},
							{ "mDataProp": function (data, type, full){
								if(data.postDate){
									return HSDateUtils.formatDateFromTicks(data.postDate);
								}
								return "N/A";
							}},
							{ "mDataProp": function ( data, type, full ){
								if(data.postType){
									switch (data.postType){
										case "VIDEOURL":
										case "VIDEO":
											return "<button data-id='"+data.groupPostId+"' class='button radius margin-bottom-0 btn-teal display-inline-block tiny expand v-link-btn'>Post Again</button>";
										case "DYK":
											return "<button data-id='"+data.linkId+"' class='button radius margin-bottom-0 btn-teal display-inline-block tiny expand dyk-link-btn'>Post Again</button>";
										case "HYPERLINK":
											return "<button data-id='"+data.groupPostId+"' class='button radius margin-bottom-0 btn-teal display-inline-block tiny expand hyperlink-link-btn'>Post Again</button>";
									}
								}
							}
							,'bSortable': false}
						]
					});

			//remove sorting class from td, __oz
            $(".sorting_1").removeClass("sorting_1 sorting_1");
		}catch(err) {
			console.log(err);
		}

		fixedPaginationStyling();
		$('.dykrow').css('max-width','100%');
	});

	//fixed paging styling
	function fixedPaginationStyling(){
		var $ul = $(".dataTables_paginate ul");
		$ul.each(function(){
			var $this = $(this);
			if($this && !$this.hasClass("pagination")){
				$this.addClass("pagination");
			}
		});
	}

	function bindGroupPostVideoEventListener(){

		var $txtVideoDescription = $("#txtVideoDescription"),
			$txtVideoLink = $("#txtVideoLink"),
			clearPostVideoForm = function(){
				$txtVideoDescription.val("");
				$txtVideoLink.val("");
				$txtVideoDescription.next().addClass("hide");
				$txtVideoLink.next().text("Required").addClass("hide");
			};

		$("#postVideoDialog").on({
			opened: function(){
				clearPostVideoForm.call();
			},
			closed: function(){
				clearPostVideoForm.call();
			}
		});

		var $txtHyperlink = $("#txtHyperlink"),
			$txtHyperlinkDescription = $("#txtHyperlinkDescription"),
			clearPostHyperlinkForm = function(){
				$txtHyperlinkDescription.val("");
				$txtHyperlink.val("");
				$txtHyperlinkDescription.next().addClass("hide");
				$txtHyperlink.next().text("Required").addClass("hide");
			};

		$("#postHyperlinkDialog").on({
			opened: function(){
				clearPostHyperlinkForm.call();
			},
			closed: function(){
				clearPostHyperlinkForm.call();
			}
		});

		$("#videoPlayerDialog").on({
			closed: function(){
				$("#divIframe").prop("src", "about:blank");
			}
		});

		$txtVideoDescription.add($txtVideoLink).on("keydown", function(){
			if($(this).val().length > 0){
				$(this).next().addClass("hide");
			}
		});

		$("#btnPostHyperlinkDialog").off("click").on("click", function(e){
			e.preventDefault();
			postHyperlinkToNewsFeed();
		});

		$("#btnPostVideoDialog").off("click").on("click", function(e){
			e.preventDefault();
			extractThumbnailFromVideoAndPostToNewsFeed();
		});
	}

	function sendGroupPost(sendBtn, groupPostId){
		
		$btnSend = $(sendBtn).addClass("disabled").attr("disabled", "disabled");
		
		var	actionUrl = "sendGroupPost.action",
			dataToSend = {"adviceIdToSend": groupPostId },
			postRequestActions = {
				"requestType": "GET",
		 	  	"successCallBack": onSuccessTextPosted,
		 	  	"loading": $("#loading")
			};
		
		sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);	    		
	}
	
	function onSuccessTextPosted(data)	{
		if(data == 0) {
			$("#infoSuccess").html("<div class='On-success-advice'>Group Post sent successfully.</div>");
			$("#infoSuccess").slideDown();
			setInterval(function () {$("#infoSuccess").slideUp();}, 3000);
		}else{
			$("#infoError").html("<div class='On-success-advice'>Error Sending Group Post.</div>");
			$("#infoError").slideDown();
			setInterval(function () {$("#infoError").slideUp();}, 3000);			
		}

		//refresh table once you perform any action
		oTable.fnDraw();
		$("#postDYKDialog").foundation("reveal", "close");

		setTimeout(function(){
			if($btnSend){
				$btnSend.removeClass("disabled").removeAttr("disabled");
			}
		}, 1000);
	}

	function onSuccessVideoPosted(data)	{
		$("#closePostVideoDialog").click();
		if(data == 0) {
			$("#infoSuccess").html("<div class='On-success-advice'>Video posted successfully</div>");
			$("#infoSuccess").slideDown();
			setInterval(function () {$("#infoSuccess").slideUp();}, 3000);
		}else{
			$("#infoError").html("<div class='On-success-advice'>Error posting video.</div>");
			$("#infoError").slideDown();
			setInterval(function () {$("#infoError").slideUp();}, 3000);
		}

		//refresh table once you perform any action
		oTable.fnDraw();
		$("#postVideoDialog").foundation("reveal", "close");

		setTimeout(function(){

			$("#btnPostVideoDialog").text("Post").removeClass("disabled").removeAttr("disabled")
					.off("click")
					.on("click", function(e){
						e.preventDefault();
						extractThumbnailFromVideoAndPostToNewsFeed();
					});

			$("#btnCancelVideoDialog").removeClass("disabled").removeAttr("disabled")
					.off("click")
					.on("click", function(e){
						e.preventDefault();
						$("#postVideoDialog").foundation("reveal", "close");
					});
		}, 500);
	}

	function onSuccessHyperlinkPosted(data)	{
		$("#closePostHyperlinkDialog").click();
		if(data == 0) {
			$("#infoSuccess").html("<div class='On-success-advice'>Hyperlink posted successfully</div>");
			$("#infoSuccess").slideDown();
			setInterval(function () {$("#infoSuccess").slideUp();}, 3000);
		}else{
			$("#infoError").html("<div class='On-success-advice'>Error posting Hyperlink.</div>");
			$("#infoError").slideDown();
			setInterval(function () {$("#infoError").slideUp();}, 3000);
		}

		//refresh table once you perform any action
		oTable.fnDraw();
		$("#postHyperlinkDialog").foundation("reveal", "close");

		setTimeout(function(){

			$("#btnPostHyperlinkDialog").text("Post")
					.removeClass("disabled")
					.removeAttr("disabled")
					.off("click").on("click", function(e){
						e.preventDefault();
						postHyperlinkToNewsFeed();
					});

			$("#btnCancelHyperlinkDialog").removeClass("disabled").removeAttr("disabled")
					.off("click")
					.on("click", function(e){
						e.preventDefault();
						$("#postHyperlinkDialog").foundation("reveal", "close");
					});

		}, 1000);
	}

	function extractThumbnailFromVideoAndPostToNewsFeed() {

		var $txtVideoDescription = $("#txtVideoDescription"),
			$txtVideoLink = $("#txtVideoLink"),
			valLinkDescription  = $.trim($txtVideoDescription.val()),
			valLink = $.trim($txtVideoLink.val());

		$txtVideoDescription.next().addClass("hide");
		$txtVideoLink.next().text("Required").addClass("hide");

		if(valLinkDescription.length == 0 && valLink.length == 0){
			$txtVideoDescription.next().removeClass("hide");
			$txtVideoLink.next().removeClass("hide");
			return;
		}

		if(!valLinkDescription || valLinkDescription.length == 0){
			$txtVideoDescription.next().removeClass("hide").end().val("");
			return;
		}

		if(!valLink || valLink.length == 0){
			$txtVideoLink.next().removeClass("hide").end().val("");
			return;
		}

		if(valLink && !HSValidator.init().validateFieldDataByType("url", valLink)){
			$txtVideoLink.next().text("Please enter valid video link").removeClass("hide");
			return;
		}

		$("#btnPostVideoDialog").off("click").text("Posting...").addClass("disabled").attr("disabled", "disabled");
		$("#btnCancelVideoDialog").off("click").addClass("disabled").attr("disabled", "disabled");

		var videoURL = valLink,
			mediasURL = "https://api.wistia.com/v1/medias/",
			videoProvider = getVideoProvider(videoURL),
			videoID = extractVideoIdFromUrl(videoURL, videoProvider),
			apiUrl = "https://noembed.com/embed",
			params = {format: 'json', url: videoURL};

		if(videoProvider === "WISTIA"){
			apiUrl = mediasURL + videoID + ".json?api_password=${token}";
			params = null;
		}

		$.getJSON(apiUrl, params)
		.done(function(videoObject) {

			if(videoObject){

				var thumbnailURL = "",
					videoURLAddedWithBin = "",
					questionMark = "";

				switch (videoProvider){
					case "WISTIA":
						thumbnailURL = videoObject.thumbnail.url;
						videoURLAddedWithBin = videoObject.assets[0].url;
						questionMark = thumbnailURL.lastIndexOf("?");
						thumbnailURL = thumbnailURL.substring(0, questionMark);
						break;
					case "YOUTUBE":
						thumbnailURL = "https://i.ytimg.com/vi/"+videoID+"/default.jpg";// videoObject.thumbnail_url;
						videoURLAddedWithBin = "https://www.youtube.com/embed/"+videoID+"?feature=oembed";
						break;
					case "VIMEO":
						thumbnailURL = videoObject.thumbnail_url;
						videoURLAddedWithBin = "https://player.vimeo.com/video/"+videoID;
						break;
				}

				var	actionUrl = "sendGroupPostVideo.action",
					dataToSend = {
						"videoLink": videoURL,
						"videoDescription": valLinkDescription,
						"videoThumbnail": thumbnailURL,
						"videoEmbedUrl": videoURLAddedWithBin
					},
					postRequestActions = {
						"requestType": "GET",
						"successCallBack": onSuccessVideoPosted,
						"loading": $("#loading")
					};

				sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
			}else{
				$("#infoError").html("<div class='On-success-advice'>Error posting video.</div>");
				$("#infoError").slideDown();
				setInterval(function () {$("#infoError").slideUp();}, 3000);
			}

		}).fail(function( jqxhr, textStatus, error ) {
			console.log(jqxhr, textStatus, error);
			$("#infoError").html("<div class='On-success-advice'>Error posting video.</div>");
			$("#infoError").slideDown();
			setInterval(function () {$("#infoError").slideUp();}, 3000);
		});
	}

	function postHyperlinkToNewsFeed() {

		var $txtHyperlinkDescription = $("#txtHyperlinkDescription"),
			$txtHyperlink = $("#txtHyperlink"),
			valLinkDescription  = $.trim($txtHyperlinkDescription.val()),
			valLink  = $.trim($txtHyperlink.val());

		$txtHyperlinkDescription.next().addClass("hide");
		$txtHyperlink.next().text("Required").addClass("hide");

		if(valLinkDescription.length === 0 && valLink.length === 0){
			$txtHyperlinkDescription.next().removeClass("hide");
			$txtHyperlink.next().removeClass("hide");
			return;
		}

		if(!valLinkDescription || valLinkDescription.length === 0){
			$txtHyperlinkDescription.next().removeClass("hide").end().val("");
			return;
		}

		if(!valLink || valLink.length == 0){
			$txtHyperlink.next().removeClass("hide").end().val("");
			return;
		}

		if(valLink && !HSValidator.init().validateFieldDataByType("url", valLink)){
			$txtHyperlink.next().text("Please enter valid Hyperlink").removeClass("hide");
			return;
		}

		$("#btnPostHyperlinkDialog").off("click").text("Posting...").addClass("disabled").attr("disabled", "disabled");
		$("#btnCancelHyperlinkDialog").off("click").addClass("disabled").attr("disabled", "disabled");

		if($txtHyperlink.val()){
			var	actionUrl = "sendGroupPostHyperlink.action",
					dataToSend = {
						"hyperlink": valLink,
						"hyperlinkDescription": valLinkDescription
					},
					postRequestActions = {
						"requestType": "GET",
						"successCallBack": onSuccessHyperlinkPosted,
						"loading": $("#loading")
					};

			sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
		} else {
			$("#infoError").html("<div class='On-success-advice'>Error posting hyperlink.</div>");
			$("#infoError").slideDown();
			setInterval(function () {$("#infoError").slideUp();}, 3000);
		}
	}

	function extractVideoIdFromUrl(url, videoProvider){

		var videoIdIndex = url.lastIndexOf("/")+1;

		if(videoProvider === "YOUTUBE"){
			videoIdIndex = url.lastIndexOf("?v=")+3;
		}

		return url.substring(videoIdIndex, url.length);
	}

	function bindRowElementsClickEventListener(){

		$(".g-video-player").off("click").on("click", function(e){
			e.preventDefault();
			$("#videoPlayerDialog").foundation("reveal", "open");

			var videoProvider = getVideoProvider($(this).data("url")),
				videoHashedId = extractVideoIdFromUrl($(this).data("url"), videoProvider),
				videoUrl = "";

			switch (videoProvider){
				case "WISTIA":
					videoUrl = "//fast.wistia.net/embed/iframe/"+videoHashedId;
					break;
				case "YOUTUBE":
					videoUrl = "https://www.youtube.com/embed/"+videoHashedId+"?feature=oembed";
					break;
				case "VIMEO":
					videoUrl = "https://player.vimeo.com/video/"+videoHashedId;
					break;
			}

			$("#videoDialogHeader").text($(this).data("text"));
			$("#divIframe").prop("src", videoUrl);
		});

		$(".v-link-btn").off("click").on("click", function(e){
			e.preventDefault();

			var	actionUrl = "sendGroupPostVideo.action",
					dataToSend = {
						"token": $(this).data("id")
					},
					postRequestActions = {
						"requestType": "GET",
						"successCallBack": onSuccessVideoPosted,
						"loading": $("#loadingTable")
					};

			sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
		});

		$(".hyperlink-link-btn").off("click").on("click", function(e){
			e.preventDefault();

			var	actionUrl = "sendGroupPostHyperlink.action",
					dataToSend = {
						"token": $(this).data("id")
					},
					postRequestActions = {
						"requestType": "GET",
						"successCallBack": onSuccessHyperlinkPosted,
						"loading": $("#loadingTable")
					};

			sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
		});

		$(".dyk-link-btn").off("click").on("click", function(e){
			e.preventDefault();

			var	actionUrl = "sendGroupPost.action",
				dataToSend = {"token": $(this).data("id") },
				postRequestActions = {
					"requestType": "GET",
					"successCallBack": onSuccessTextPosted,
					"loading": $("#loadingTable")
				};

			sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
		});
	}

	function getVideoProvider(videoUrl){

		if(videoUrl.indexOf("wistia") > -1){
			return "WISTIA";
		}else if(videoUrl.indexOf("youtube") > -1){
			return "YOUTUBE";
		}else{
			return "VIMEO";
		}
	}

	</script>
</body>
</html>