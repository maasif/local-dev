<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>
    <title>My Patients</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/logDetail.css?v=0.5" />
</head>
<body>

<!-- BreadCrumb -->
<div class="breadcrumb-green">
    <div class="max-width-1129">
        <ul class="breadcrumbs">
            <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                <li>
                    <a href="unProcessedMeals.action">Meals</a>
                </li>
            </s:if>
            <li>
                <a href="members.action">My Members</a>
            </li>
            <li>
                <a href="dashboard.action?patientId=${patientId}">Member Dashboard</a>
            </li>
            <li class="current">Summary</li>
            <li class="current"><b>Member Name:</b> <s:property value='%{patient.user.displayName}' /></li>
        </ul>
    </div>
</div>

<br/>
<div id="loading" class="loader" style="display: none;">
    <div class="image-loader">
        <p class="progress-please-wait">Please wait...</p>
        <div class="progress progress-striped active">
            <div class="progress-bar" role="progressbar" aria-valuenow="100"
                 aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
        </div>
        <!-- <p id="progressTxt" class="progress-text">We're uploading your content!!!</p> -->
    </div>
</div>

<div id="divMemberNavArea"></div>
<div class="dashboard-container margin-bottom-20">
    <div class="row max-width-1129">

        <%--<div class="coach-area-expand">
            <a href="#" id="btnExpColCoachArea"><img src="${pageContext.request.contextPath}/resources/css/images/arrow-right.png" title="Show" height="25" /></a>
        </div>--%>

        <div class="columns medium-12">
            <!-- Charts, goals and Coach Notes area -->
            <div class="row bg-white row-auto" data-equalizer>

                <!-- Left white area -->
                <div class="columns medium-12 padding-lr-0 summary-area equal-areas" data-equalizer-watch>
                    <div class="padding-10">
                        <ul id="tabWeeklyDaily" class="nav nav-tabs sec-b">
                            <li data-tab="daily"><a data-toggle="tab" href="#sectionDaily" onclick="loadDailyTabData();">Daily</a></li>
                            <li data-tab="threeDays" id="tabThreeDays" ><a data-toggle="tab" href="#sectionThreeDays" onclick="loadThreeDaysTabData();">3 Days</a></li>
                            <li data-tab="weekly" id="tabWeekly" ><a data-toggle="tab" href="#sectionWeekly" onclick="loadWeeklyTabData();">Weekly</a></li>
                            <li class="active" data-tab="fourteenDays" id="tabFourteenDays" ><a data-toggle="tab" href="#sectionFourteenDays" onclick="loadFourteenDaysTabData();">14 Days</a></li>
                            <li data-tab="monthly" ><a data-toggle="tab" href="#sectionMonthly" onclick="loadMonthlyTabData();">Monthly</a></li>
                            <li class="right">
                                <a href="myMealsPage.action?patientId=<s:property value="%{patientId}"/>&mealType=All" class="button radius tiny btn-teal margin-bottom-0"> Show Meals</a>
                            </li>
                        </ul>

                        <div class="tab-content">

                            <!-- Daily section -->
                            <div id="sectionDaily" class="tab-pane fade sec-b">

                                <div class="chart-nav-buttons">
                                    <ul id="dailyNavBtns" class="button-group round">
                                        <li>
                                            <a href="#" class="button tiny g-button" data-jumpto="dGlucoseLevelsLabel">
                                                <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/post_icon_glucose.png"/>
                                                <span>Glucose Levels</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="button tiny m-button" data-jumpto="dCarbIntakeLabel">
                                                <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_food_summary.png" />
                                                <span>Carb Intake</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="button tiny a-button" data-jumpto="dailyPhysicalActivityLabel">
                                                <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_activity_summary.png" />
                                                <span>Physical Activity</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="row align-center hide" >
                                    <ul class="button-group radius trend-btns-switch">
                                        <li><button id="dailyTrendsBtn" class="active-trend trend-btns" onclick="showHideTrends('daily')"><img id="dailyTrendsImg" title="Daily Trend" src="${pageContext.request.contextPath}/resources/images/glucose_white.png"></button></li>
                                        <li><button id="weeklyTrendsBtn" class="inactive-trend trend-btns" onclick="showHideTrends('weekly')"><img id="weeklyTrendsImg" title="Weekly Trend" src="${pageContext.request.contextPath}/resources/images/meal_white.png"></button></li>
                                    </ul>
                                </div>
                                <div id="dailyTrends">
                                    <div class="row">
                                        <div class="columns medium-6 wDate-lbl"><label class="font-30 margin-bottom-11 inline">Daily Trend</label></div>
                                        <div class="columns medium-6">
                                            <div class="collapse row">
                                                <div class="small-2 columns"><button onclick="getPreDay()" class="prefix btn-teal button">Prev</button></div>
                                                <div class="small-8 columns">
                                                    <s:textfield id="selDate" data-calId="btnDateFrom"
                                                                 cssClass="patient-detail-text date-style" name="dailyReportDate"
                                                                 readonly="true"></s:textfield>
                                                </div>
                                                <div class="small-2 columns"><button id="dailyDateNext" onclick="getNextDay()" class="button btn-teal postfix">Next</button> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row daily-trends max-width-1129">
                                        <div class="container-div shadow width-100">
                                            <div class="columns medium-12 common-style height-470">
                                                <div class="shadow padding-buttom-10">
                                                    <label id="dGlucoseLevelsLabel" class="chart-section-header bg-glucose"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/post_icon_glucose.png" height="24" /> Glucose levels</label>
                                                    <div class="glucose-legend">
                                                        <table class="glucose-table" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <th colspan="4">
                                                                    <span>
                                                                        <span class="before-meal-symbol before-meal-symbol-under"></span>
                                                                        <span class="before-meal-symbol before-meal-symbol-in"></span>
                                                                        <span class="before-meal-symbol before-meal-symbol-over"></span>
                                                                    </span>
                                                                    Before Meal
                                                                </th>
                                                                <th colspan="4">
                                                                    <span>
                                                                        <span class="after-meal-symbol after-meal-symbol-under"></span>
                                                                        <span class="after-meal-symbol after-meal-symbol-in"></span>
                                                                        <span class="after-meal-symbol after-meal-symbol-over"></span>
                                                                    </span>
                                                                    After Meal
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Total Tests: </b><span class="before-meal-tests">N/A</span></td>
                                                                <td><b>Target BG: </b><span class="before-meal-targets">N/A</span></td>
                                                                <td><b>Avg BG: </b><span class="before-meal-avg">N/A</span></td>
                                                                <td><b>STD: </b><span class="before-meal-std">N/A</span></td>
                                                                <td class="tbl-sep"><b>Total Tests: </b><span class="after-meal-tests">N/A</span></td>
                                                                <td><b>Target BG: </b><span class="after-meal-targets">N/A</span></td>
                                                                <td><b>Avg BG: </b><span class="after-meal-avg">N/A</span></td>
                                                                <td><b>STD: </b><span class="after-meal-std">N/A</span></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div id="containerGlucose" class = "columnScaterchart"></div>
                                                    <div class="blank-div1 blank-div-left-scatter-rotate">12 AM</div>
                                                    <div class="blank-div1 blank-div-right-scatter-rotate">12 PM</div>
                                                </div>
                                                <div id="clDailyGlucoseChart" class="chart-loading">
                                                    <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row max-width-1129">
                                        <div class="columns medium-12 common-style height-405 container-margin-top">
                                            <div class="shadow">
                                                <label id="dCarbIntakeLabel" class="chart-section-header bg-meal"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_food_summary.png" height="24" /> Carb intake</label>
                                                <div id="containerCarbs" class = "columnScaterchart"></div>
                                            </div>
                                            <div id="clDailyCarbIntakeChart" class="chart-loading">
                                                <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row max-width-1129">
                                        <div class="columns medium-12 container-margin-top">
                                            <div class="shadow">
                                                <label id="dailyPhysicalActivityLabel" class="chart-section-header bg-activity"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_activity_summary.png" height="24" /> Physical Activity</label>
                                                <div id="container-DailyActivityCombine" class="height-200"></div>
                                                <div id="container-DailyActivityMinutes" class="height-200"></div>
                                                <div id="dailyStepsLbl" class="daily-steps-title"></div>

                                            </div>
                                            <div id="clDailyActivityCombineChart" class="chart-loading">
                                                <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="weeklyTrends" class="display-none">
                                    <div class="row max-width-1129">
                                        <div class="columns medium-6 wDate-lbl"><label class="font-30 margin-bottom-11 inline">Weekly Trend</label></div>
                                        <div class="columns medium-6">
                                            <div class="collapse row">
                                                <div class="small-2 columns"><button onclick="getPreWeek('weekly-a')" class="button btn-teal prefix">Prev</button></div>
                                                <div class="small-8 columns">
                                                    <s:textfield id="selWeekTrendDate" data-calId="btnDateFrom"
                                                                 cssClass="patient-detail-text date-style" name="weeklyTrendDateString"
                                                                 readonly="true"></s:textfield>
                                                </div>
                                                <div class="small-2 columns"><button id="weekTrendDateNext" onclick="getNextWeek('weekly-a')" class="button btn-teal postfix">Next</button> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row weekly-trends max-width-1129">
                                        <a href="#meals" style="display:none"></a>
                                        <div class="container-div width-100">
                                            <div class="columns medium-6 common-style height-42">
                                                <div class="shadow">
                                                    <label class="heading-style">Breakfast</label>
                                                    <div id="container-dailyBreakfastMeal" class="meal-log-2 margin-zero"></div>
                                                </div>
                                            </div>
                                            <div class="columns medium-6 common-style height-42">
                                                <div class="shadow">
                                                    <label class="heading-style">Lunch</label>
                                                    <div id="container-dailyLunchMeal" class="meal-log-2 margin-zero"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container-div shadow width-100">
                                            <div class="columns medium-6 common-style height-42">
                                                <div class="shadow">
                                                    <label class="heading-style">Snacks</label>
                                                    <div id="container-dailySnakMeal" class="meal-log-2 margin-zero"></div>
                                                </div>
                                            </div>
                                            <div class="columns medium-6 common-style height-42">
                                                <div class="shadow">
                                                    <label class="heading-style">Dinner</label>
                                                    <div id="container-dailyDinnerMeal" class="meal-log-2 margin-zero"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Three days section -->
                            <div id="sectionThreeDays" class="tab-pane fade sec-b">

                                <div class="row max-width-1129">
                                    <div class="chart-nav-buttons">
                                        <ul id="threeDaysNavBtns" class="button-group round">
                                            <li>
                                                <a href="#" class="button tiny g-button" data-jumpto="3GlucoseLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/post_icon_glucose.png"/>
                                                    <span>Glucose</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny m-button" data-jumpto="3MealLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_food_summary.png" />
                                                    <span>Meal</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny med-button" data-jumpto="3MedicationLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_meds.png" />
                                                    <span>Medication</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny a-button" data-jumpto="3PhysicalActivityLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_activity_summary.png" />
                                                    <span>Physical Activity</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny w-button" data-jumpto="3WeightLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_weight.png" />
                                                    <span>Weight</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="columns medium-6 wDate-lbl"><label class="font-30 margin-bottom-11 inline">3 Days Summary</label></div>
                                    <div class="columns medium-6">
                                        <div class="collapse row">
                                            <div class="small-2 columns"><button onclick="getThreeDaysTime('threePrev')" class="button btn-teal prefix">Prev</button></div>
                                            <div class="small-8 columns">
                                                <s:textfield id="selThreeDaysDate" data-calId="btnDateFrom"
                                                             cssClass="patient-detail-text date-style"
                                                             name="threeDaysDateString" readonly="true"></s:textfield>
                                            </div>
                                            <div class="small-2 columns"><button id="threeDaysDateNext" onclick="getThreeDaysTime('threeNext')" class="button btn-teal postfix">Next</button> </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="container-div width-100" >
                                        <div class="row max-width-1129 row-auto">
                                            <div class="container-div shadow width-100">
                                                <div class="columns medium-12 common-style height-470">
                                                    <div class="shadow padding-buttom-10">
                                                        <label id="3GlucoseLabel" class="chart-section-header bg-glucose"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/post_icon_glucose.png" height="24" /> Glucose levels</label>
                                                        <div class="glucose-legend">
                                                            <table class="glucose-table" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <th colspan="4">
                                                                         <span>
                                                                            <span class="before-meal-symbol before-meal-symbol-under"></span>
                                                                            <span class="before-meal-symbol before-meal-symbol-in"></span>
                                                                            <span class="before-meal-symbol before-meal-symbol-over"></span>
                                                                        </span>
                                                                        Before Meal
                                                                    </th>
                                                                    <th colspan="4">
                                                                         <span>
                                                                            <span class="after-meal-symbol after-meal-symbol-under"></span>
                                                                            <span class="after-meal-symbol after-meal-symbol-in"></span>
                                                                            <span class="after-meal-symbol after-meal-symbol-over"></span>
                                                                        </span>
                                                                        After Meal
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>Total Tests: </b><span class="before-meal-tests">N/A</span></td>
                                                                    <td><b>Target BG: </b><span class="before-meal-targets">N/A</span></td>
                                                                    <td><b>Avg BG: </b><span class="before-meal-avg">N/A</span></td>
                                                                    <td><b>STD: </b><span class="before-meal-std">N/A</span></td>
                                                                    <td class="tbl-sep"><b>Total Tests: </b><span class="after-meal-tests">N/A</span></td>
                                                                    <td><b>Target BG: </b><span class="after-meal-targets">N/A</span></td>
                                                                    <td><b>Avg BG: </b><span class="after-meal-avg">N/A</span></td>
                                                                    <td><b>STD: </b><span class="after-meal-std">N/A</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="container-ThreeDaysGlucose" class = "columnScaterchart"></div>
                                                        <div class="blank-div blank-div-left-scatter">12 AM</div>
                                                        <div class="blank-div blank-div-right-scatter">12 PM</div>
                                                    </div>
                                                    <div id="clThreeDaysGlucoseChart" class="chart-loading">
                                                        <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <a href="#meals" style="display:none"></a>
                                            <label id="3MealLabel" class="chart-section-header bg-meal"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_food_summary.png" height="24" /> Meal</label>
                                            <div id="container-ThreeDaysMeal" class="align-center center-align-chart-small" ></div>
                                        </div>
                                        <div id="clThreeDaysMealChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <label id="3MedicationLabel" class="chart-section-header bg-med"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_meds.png" height="24" /> Medication</label>
                                            <div id="container-ThreeDaysMedication" class="align-center center-align-chart-small"></div>
                                        </div>
                                        <div id="clThreeDaysMedicationChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <label id="3PhysicalActivityLabel" class="chart-section-header bg-activity"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_activity_summary.png" height="24" /> Physical Activity</label>
                                            <div id="container-ThreeDaysActivityCombine" class="height-200"></div>
                                            <div id="container-ThreeDaysActivityMinutes" class="height-200"></div>
                                            <div id="activityLineCombineFirst" class="chart-line div-activity-combine-line div-first-left"> </div>
                                            <div id="activityLineCombineSecond" class="chart-line div-activity-combine-line div-second-left"> </div>
                                            <div id="activityLineStepsFirst" class="chart-line div-activity-steps-line div-first-left"> </div>
                                            <div id="activityLineStepsSecond" class="chart-line div-activity-steps-line div-second-left"> </div>
                                            <div id="firstStepsTitle" class="first-steps-title"></div>
                                            <div id="secondStepsTitle" class="second-steps-title"></div>
                                            <div id="thirdStepsTitle" class="third-steps-title"></div>
                                            <div class="blank-div blank-div-left">12 AM</div>
                                            <div class="blank-div blank-div-right">12 PM</div>

                                        </div>
                                        <div id="clThreeDaysActivityCombineChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <label id="3WeightLabel" class="chart-section-header bg-weight"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_weight.png" height="24" /> Weight Loss</label>
                                            <label class="chart-info-div_weight" id="threeDaysTargetWeights"></label>
                                            <div id="container-ThreeDaysWeightLoss" class="center-align-chart-small"></div>
                                        </div>
                                        <div id="clThreeDaysWeightLossChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129 hide">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <label id="threeDaysCombineChartLabel" class="chart-section-header bg-weight"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_weight.png" height="24" /> Combine Charts</label>
                                            <div id="container-ThreeDaysCombineActivityChart" class="height-200"></div>
                                            <div id="container-ThreeDaysCombineGlucoseChart" class="height-200" ></div>
                                            <div id="container-ThreeDaysCombineCarbsChart" style="height: 50px"></div>
                                            <div id="container-ThreeDaysCombineMedicationChart" style="height: 100px"></div>
                                            <div id="medsLineFirst" class="chart-line div-first-day-meds div-first-left"> </div>
                                            <div id="medsLineSecond" class="chart-line div-second-day-meds div-second-left"> </div>
                                            <div id="glucoseLineFirst" class="chart-line div-first-day-glucose div-first-left"> </div>
                                            <div id="glucoseLineSecond" class="chart-line div-second-day-glucose div-second-left"> </div>
                                            <div id="activityLineFirst" class="chart-line div-first-day-activity div-first-left"> </div>
                                            <div id="activityLineSecond" class="chart-line div-second-day-activity div-second-left"> </div>
                                        </div>
                                        <div id="clThreeDaysCombineActivityChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Weekly section -->
                            <div id="sectionWeekly" class="tab-pane fade sec-b padding-bottom-10">

                                <div class="row max-width-1129">

                                    <div class="chart-nav-buttons">
                                        <ul id="weeklyNavBtns" class="button-group round">
                                            <li>
                                                <a href="#" class="button tiny g-button" data-jumpto="glucoseDiv">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/post_icon_glucose.png"/>
                                                    <span>Glucose</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny m-button" data-jumpto="mealsDiv">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_food_summary.png" />
                                                    <span>Meal</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny med-button" data-jumpto="medicationDiv">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_meds.png" />
                                                    <span>Medication</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny a-button" data-jumpto="weeklyPhysicalActivityLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_activity_summary.png" />
                                                    <span>Physical Activity</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="columns medium-6 wDate-lbl">
                                        <label class="font-30 margin-bottom-11 inline">Weekly Summary</label>
                                    </div>
                                    <div class="columns medium-6">
                                        <div class="collapse row">
                                            <div class="small-2 columns"><button onclick="getPreWeek('weekly')" class="button btn-teal prefix">Prev</button></div>
                                            <div class="small-8 columns">
                                                <s:textfield id="selWeekDate" data-calId="btnDateFrom"
                                                             cssClass="patient-detail-text date-style" name="weeklyReportDateString"
                                                             readonly="true"></s:textfield>
                                            </div>
                                            <div class="small-2 columns"><button id="weekSummDateNext" onclick="getNextWeek('weekly')" class="button btn-teal postfix">Next</button> </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="row max-width-1129 row-auto">
                                        <div class="container-div shadow width-100">
                                            <div class="columns medium-12 common-style height-470">
                                                <div class="shadow padding-buttom-10">
                                                    <label id="glucoseDiv" class="chart-section-header bg-glucose"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/post_icon_glucose.png" height="24" /> Glucose levels</label>
                                                    <div class="glucose-legend">
                                                        <table class="glucose-table" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <th colspan="4">
                                                                     <span>
                                                                        <span class="before-meal-symbol before-meal-symbol-under"></span>
                                                                        <span class="before-meal-symbol before-meal-symbol-in"></span>
                                                                        <span class="before-meal-symbol before-meal-symbol-over"></span>
                                                                    </span>
                                                                    Before Meal
                                                                </th>
                                                                <th colspan="4">
                                                                     <span>
                                                                        <span class="after-meal-symbol after-meal-symbol-under"></span>
                                                                        <span class="after-meal-symbol after-meal-symbol-in"></span>
                                                                        <span class="after-meal-symbol after-meal-symbol-over"></span>
                                                                    </span>
                                                                    After Meal
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Total Tests: </b><span class="before-meal-tests">N/A</span></td>
                                                                <td><b>Target BG: </b><span class="before-meal-targets">N/A</span></td>
                                                                <td><b>Avg BG: </b><span class="before-meal-avg">N/A</span></td>
                                                                <td><b>STD: </b><span class="before-meal-std">N/A</span></td>
                                                                <td class="tbl-sep"><b>Total Tests: </b><span class="after-meal-tests">N/A</span></td>
                                                                <td><b>Target BG: </b><span class="after-meal-targets">N/A</span></td>
                                                                <td><b>Avg BG: </b><span class="after-meal-avg">N/A</span></td>
                                                                <td><b>STD: </b><span class="after-meal-std">N/A</span></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div id="container-weeklyGlucose" class = "columnScaterchart"></div>
                                                    <div class="blank-div blank-div-left-scatter">12 AM</div>
                                                    <div class="blank-div blank-div-right-scatter">12 PM</div>
                                                </div>
                                                <div id="clWeeklyGlucoseChart" class="chart-loading">
                                                    <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <label id="mealsDiv" class="chart-section-header bg-meal"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_food_summary.png" height="24" /> Meal</label>
                                            <div id="container-weeklyMeal" class="align-center" style="width: 750px; margin: 0 auto"></div>
                                        </div>
                                        <div id="clWeeklyMealChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <label id="medicationDiv" class="chart-section-header bg-med"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_meds.png" height="24" /> Medication</label>
                                            <div id="container-weeklyMedLog" class="align-center center-align-chart-small"></div>
                                        </div>
                                        <div id="clWeeklyMedLogChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <label id="weeklyPhysicalActivityLabel" class="chart-section-header bg-activity"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_activity_summary.png" height="24" /> Physical Activity</label>
                                            <div id="container-weeklyActivity" class="height-200"></div>
                                            <div id="container-WeeklyActivityMinutes" class="height-200"></div>
                                        </div>
                                        <div id="clWeeklyActivityChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Fourteen days section -->
                            <div id="sectionFourteenDays" class="tab-pane active fade in sec-b">

                                <div class="row max-width-1129">
                                    <div class="chart-nav-buttons">
                                        <ul id="fourteenDaysNavBtns" class="button-group round">
                                            <li>
                                                <a href="#" class="button tiny g-button" data-jumpto="14GlucoseLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/post_icon_glucose.png"/>
                                                    <span>Glucose</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny m-button" data-jumpto="14MealLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_food_summary.png" />
                                                    <span>Meal</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny med-button" data-jumpto="14MedicationLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_meds.png" />
                                                    <span>Medication</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny a-button" data-jumpto="14PhysicalActivityLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_activity_summary.png" />
                                                    <span>Physical Activity</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny w-button" data-jumpto="14WeightLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_weight.png" />
                                                    <span>Weight</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="columns medium-6 wDate-lbl"><label class="font-30 margin-bottom-11 inline">14 Days Summary</label></div>
                                    <div class="columns medium-6">
                                        <div class="collapse row">
                                            <div class="small-2 columns"><button onclick="getFourteenDaysTime('prev')" class="button btn-teal prefix">Prev</button></div>
                                            <div class="small-8 columns">
                                                <s:textfield id="selFourteenDaysDate" data-calId="btnDateFrom"
                                                             cssClass="patient-detail-text date-style"
                                                             name="fourteenDaysReportDateString" readonly="true"></s:textfield>
                                            </div>
                                            <div class="small-2 columns"><button id="fourteenDaysDateNext" onclick="getFourteenDaysTime('next')" class="button btn-teal postfix">Next</button> </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="container-div shadow width-100">
                                        <div class="columns medium-12 common-style height-470">
                                            <div class="shadow padding-buttom-10">
                                                <label id="fourteenDaysGlucoseLabel" class="chart-section-header bg-glucose"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/post_icon_glucose.png" height="24" /> Glucose levels</label>
                                                <div class="glucose-legend">
                                                    <table class="glucose-table" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <th colspan="4">
                                                                 <span>
                                                                    <span class="before-meal-symbol before-meal-symbol-under"></span>
                                                                    <span class="before-meal-symbol before-meal-symbol-in"></span>
                                                                    <span class="before-meal-symbol before-meal-symbol-over"></span>
                                                                </span>
                                                                Before Meal
                                                            </th>
                                                            <th colspan="4">
                                                                 <span>
                                                                    <span class="after-meal-symbol after-meal-symbol-under"></span>
                                                                    <span class="after-meal-symbol after-meal-symbol-in"></span>
                                                                    <span class="after-meal-symbol after-meal-symbol-over"></span>
                                                                </span>
                                                                After Meal
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Total Tests: </b><span class="before-meal-tests">N/A</span></td>
                                                            <td><b>Target BG: </b><span class="before-meal-targets">N/A</span></td>
                                                            <td><b>Avg BG: </b><span class="before-meal-avg">N/A</span></td>
                                                            <td><b>STD: </b><span class="before-meal-std">N/A</span></td>
                                                            <td class="tbl-sep"><b>Total Tests: </b><span class="after-meal-tests">N/A</span></td>
                                                            <td><b>Target BG: </b><span class="after-meal-targets">N/A</span></td>
                                                            <td><b>Avg BG: </b><span class="after-meal-avg">N/A</span></td>
                                                            <td><b>STD: </b><span class="after-meal-std">N/A</span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="container-fourteenDaysGlucose" class = "columnScaterchart"></div>
                                                <div class="blank-div blank-div-left-scatter">12 AM</div>
                                                <div class="blank-div blank-div-right-scatter">12 PM</div>
                                            </div>
                                            <div id="clFourteenDaysGlucoseChart" class="chart-loading">
                                                <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <a href="#meals" style="display:none"></a>
                                            <label id="14MealLabel" class="chart-section-header bg-meal"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_food_summary.png" height="24" /> Meal</label>
                                            <div id="container-FourteenDaysMeal" class="align-center" style="width: 750px; margin: 0 auto"></div>
                                        </div>
                                        <div id="clFourteenDaysMealChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <label id="14MedicationLabel" class="chart-section-header bg-med"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_meds.png" height="24" /> Medication</label>
                                            <div id="container-FourteenDaysMedication"></div>
                                        </div>
                                        <div id="clFourteenDaysMedicationChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <label id="14PhysicalActivityLabel" class="chart-section-header bg-activity"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_activity_summary.png" height="24" /> Physical Activity</label>
                                            <div id="container-FourteenDaysActivity" class="height-200"></div>
                                            <div id="container-FourteenDaysActivityMinutes" class="height-200"></div>
                                            <label id="14PhysicalActivityStepsLabel" class="fourteendays-steps-title"></label>
                                            <div class="blank-div blank-div-left-activity">12 AM</div>
                                            <div class="blank-div blank-div-right-activity">12 PM</div>
                                        </div>
                                        <div id="clFourteenDaysActivityChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <label id="14WeightLabel" class="chart-section-header bg-weight"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_weight.png" height="24" /> Weight Loss</label>
                                            <label class="chart-info-div_weight" id="fourteenDaysTargetWeights"></label>
                                            <div id="container-FourteenDaysWeightLoss" ></div>
                                        </div>
                                        <div id="clFourteenDaysWeightLossChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- Monthly section -->
                            <div id="sectionMonthly" class="tab-pane fade sec-b padding-bottom-10">

                                <div class="row max-width-1129">
                                    <div class="chart-nav-buttons">
                                        <ul id="monthlyNavBtns" class="button-group round">
                                            <li>
                                                <a href="#" class="button tiny g-button" data-jumpto="mGlucoseLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/post_icon_glucose.png"/>
                                                    <span>Glucose</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny m-button" data-jumpto="mMealLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_food_summary.png" />
                                                    <span>Meal</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny med-button" data-jumpto="mMedicationLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_meds.png" />
                                                    <span>Medication</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny a-button" data-jumpto="mPhysicalActivityLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_activity_summary.png" />
                                                    <span>Physical Activity</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="button tiny w-button" data-jumpto="mWeightLabel">
                                                    <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_weight.png" />
                                                    <span>Weight</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="columns medium-6 wDate-lbl"><label class="font-30 margin-bottom-11 inline">Monthly Summary</label></div>
                                    <div class="columns medium-6">
                                        <div class="collapse row">
                                            <div class="small-2 columns"><button onclick="getMonthTime('monthPrev')" class="button btn-teal prefix">Prev</button></div>
                                            <div class="small-8 columns">
                                                <s:textfield id="selMonthDate" data-calId="btnDateFrom"
                                                             cssClass="patient-detail-text date-style"
                                                             name="monthlyReportDateString" readonly="true"></s:textfield>
                                            </div>
                                            <div class="small-2 columns"><button id="monthlyDateNext" onclick="getMonthTime('monthNext')" class="button btn-teal postfix">Next</button> </div>
                                        </div>
                                        <%-- <div class="collapse row">
                                            <div class="small-12 columns">
                                                <s:textfield id="selMonthDate"
                                                     data-calId="btnDateFrom" cssClass="patient-detail-text date-style month-picker-dt"
                                                     name="monthlyReportDateString" readonly="true"></s:textfield>
                                            </div>
                                        </div> --%>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="row max-width-1129 row-auto">
                                        <div class="container-div shadow width-100">
                                            <div class="columns medium-12 common-style height-470">
                                                <div class="shadow padding-buttom-10">
                                                    <label id="monthlyDaysGlucoseLabel" class="chart-section-header bg-glucose"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/post_icon_glucose.png" height="24" /> Glucose levels</label>
                                                    <div class="glucose-legend">
                                                        <table class="glucose-table" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <th colspan="4">
                                                                    <span>
                                                                        <span class="before-meal-symbol before-meal-symbol-under"></span>
                                                                        <span class="before-meal-symbol before-meal-symbol-in"></span>
                                                                        <span class="before-meal-symbol before-meal-symbol-over"></span>
                                                                    </span>
                                                                    Before Meal
                                                                </th>
                                                                <th colspan="4">
                                                                     <span>
                                                                        <span class="after-meal-symbol after-meal-symbol-under"></span>
                                                                        <span class="after-meal-symbol after-meal-symbol-in"></span>
                                                                        <span class="after-meal-symbol after-meal-symbol-over"></span>
                                                                    </span>
                                                                    After Meal
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Total Tests: </b><span class="before-meal-tests">N/A</span></td>
                                                                <td><b>Target BG: </b><span class="before-meal-targets">N/A</span></td>
                                                                <td><b>Avg BG: </b><span class="before-meal-avg">N/A</span></td>
                                                                <td><b>STD: </b><span class="before-meal-std">N/A</span></td>
                                                                <td class="tbl-sep"><b>Total Tests: </b><span class="after-meal-tests">N/A</span></td>
                                                                <td><b>Target BG: </b><span class="after-meal-targets">N/A</span></td>
                                                                <td><b>Avg BG: </b><span class="after-meal-avg">N/A</span></td>
                                                                <td><b>STD: </b><span class="after-meal-std">N/A</span></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div id="container-monthlyDaysGlucose" class = "columnScaterchart"></div>
                                                    <div class="blank-div blank-div-left-scatter">12 AM</div>
                                                    <div class="blank-div blank-div-right-scatter">12 PM</div>
                                                </div>
                                                <div id="clMonthlyDaysGlucoseChart" class="chart-loading">
                                                    <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <label id="mMealLabel" class="chart-section-header bg-meal"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_food_summary.png" height="24" /> Meal</label>
                                            <div id="container-monthlyMeal" class="align-center" style="width: 750px; margin: 0 auto"></div>
                                        </div>
                                        <div id="clMonthlyMealChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <label id="mMedicationLabel" class="chart-section-header bg-med"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_meds.png" height="24" /> Medication</label>
                                            <div id="container-monthlyMedLog"></div>
                                        </div>
                                        <div id="clMonthlyMedLogChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <label id="mPhysicalActivityLabel" class="chart-section-header bg-activity"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_activity_summary.png" height="24" /> Physical Activity</label>
                                            <div id="containerMonthlyActivity" class="height-200"></div>
                                            <div id="container-MonhtlyActivityMinutes" class="height-200"></div>
                                            <label id="mPhysicalActivityStepsLabel" class="monthly-steps-title"></label>
                                            <div class="blank-div-left-activity-monthly">12 AM</div>
                                            <div class="blank-div-monthly blank-div-right-activity-monthly">12 PM</div>
                                        </div>
                                        <div id="clMonthlyActivityChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row max-width-1129">
                                    <div class="columns medium-12 container-margin-top">
                                        <div class="shadow">
                                            <label id="mWeightLabel" class="chart-section-header bg-weight"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_weight.png" height="24" /> Weight Loss</label>
                                            <label class="chart-info-div_weight" id="targetWeights"></label>
                                            <div id="containerMonthlyWeightLoss" ></div>
                                        </div>
                                        <div id="clMonthlyWeightLossChart" class="chart-loading">
                                            <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <s:hidden id="patientId" name="patientId"></s:hidden>
                </div>

                <!-- Right grey area -->
                <%--<div id="divCoachesAreaLeft" style="display: none;" data-equalizer-watch>

                </div>--%>
            </div>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.weekpicker.js" ></script>
<script src="${pageContext.request.contextPath}/resources/js/date.js" ></script>

<script src="${httpOrHttps}://code.highcharts.com/highcharts.js"></script>
<script src="${httpOrHttps}://code.highcharts.com/modules/exporting.js"></script>
<script src="${httpOrHttps}://code.highcharts.com/highcharts-more.js"></script>
<script src="${httpOrHttps}://code.highcharts.com/modules/solid-gauge.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/hs.charts.js?v=0.16"></script>

<script type="text/javascript">

    var GlucoseChart, CarbsChart;
    var oldWeelSummDate, oldDailyDate, oldWeekTrendDate, oldMonthlyDate, callForWeek = false;
    var patId = '${patientId}',
        BASE_URL = "${pageContext.request.contextPath}/",
        loggedInUserID = '${session.PROVIDER.providerId}';

    $(function(){
        scrollToChart();
        initCharts();
        bindChartJumpToScroll();

        //load coaches area via ajax
        //Ajaxing.loadExternalPage($("#divCoachesAreaLeft"), "${pageContext.request.contextPath}/educator/coachesArea.jsp", loadExpCollapse);
        Ajaxing.loadExternalPage($("#divMemberNavArea"), "${pageContext.request.contextPath}/educator/memberNavArea.jsp", function(){
            $("#divMemberNavArea ul").addClass("left-align-11").removeClass("left-align-52");
        });
    });

    function bindChartJumpToScroll(){

        //when scroll then fixed/sticky chart nav buttons to top, __oz
        $(window).on("scroll", function(){
            var y = $(this).scrollTop(),
                    $chartSticky = $(".chart-nav-buttons").removeClass("chart-buttons-sticky");

            if( y > 0 ){
                $chartSticky.addClass("chart-buttons-sticky");
            } else {
                $chartSticky.removeClass("chart-buttons-sticky");
            }
        });

        $("[data-jumpto]").on("click", function(e){
            $(this).blur();
            var $elem = $("#"+$(this).data("jumpto"));
            if($elem && $elem.length > 0){
                $('body,html').animate({
                    scrollTop : $elem.offset().top
                }, 100);
            }
        });
    }

    function scrollToChart(){
        //scroll to chart, __oz
        var $elem = $("#"+window.location.hash.replace('#', '')+"Div");
        if($elem && $elem.length > 0) {
            $("#tabWeekly a").trigger("click");
            setTimeout(function(){
                $('body,html').animate({
                    scrollTop : $elem.offset().top
                }, 70);
            }, 200);
        }
    }

</script>
</body>
</html>