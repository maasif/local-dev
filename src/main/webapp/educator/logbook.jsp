<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>
	<title>My Patients</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/timepicker/jquery.timepicker.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/timepicker/bootstrap-datepicker.css" />

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/logDetail.css?0.3" />
	
	<style type="text/css">
		.bs-example{
			margin: 20px;
		}
		.tab-content{
			display: block;
		}
		.heading-style{
			background-color: rgb(238, 238, 238);
		}
		
		body{
			background-color: #eee !important;
		}

		.footer{
			margin-top: 150px;
		}

		.main-page{
			box-shadow:none;
			border-radius:2px;			
		}
		
		.white-area{
			background-color: #fff;
			border: 1px solid #d8d8d8;
			margin-bottom: 20px !important;
		}
		
		.past-meals-btn{
			position: absolute;
			top: -35px;
		}

		.align-page{
			position: relative;
			height: 100%;
		}

        #footer{
            display: none;
        }

        .font-size-20{
            font-size: 20px;
        }

        .logbook-summary-text{
            color:#999999;
        }
	</style>
</head>
<body>

    <!-- BreadCrumb -->
    <div class="breadcrumb-green">
        <div class="max-width-1129">
            <ul class="breadcrumbs">
                <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                    <li>
                        <a href="unProcessedMeals.action">Meals</a>
                    </li>
                </s:if>
                <li>
                    <a href="members.action">My Members</a>
                </li>
                <li>
                    <a href="dashboard.action?patientId=${patientId}">Member Dashboard</a>
                </li>
                <li class="current">Log Book</li>
                <li class="current"><b>Member Name:</b> <s:property value='%{patient.user.displayName}' /></li>
            </ul>
        </div>
    </div>

	<br/>
	<div id="loading" class="loader" style="display: block;">
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>
			<div class="progress progress-striped active">
				<div class="progress-bar" role="progressbar" aria-valuenow="100"
					aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<!-- <p id="progressTxt" class="progress-text">We're uploading your content!!!</p> -->
		</div>
	</div>

    <div id="divMemberNavArea"></div>
    <div class="dashboard-container margin-bottom-20">
        <div class="row max-width-1129">

        <div class="coach-area-expand">
            <a href="#" id="btnExpColCoachArea"><img src="${pageContext.request.contextPath}/resources/css/images/arrow-left.png" title="Hide" /></a>
        </div>

        <div class="columns medium-12">

            <!-- Charts, goals and Coach Notes area -->
            <div class="row bg-white row-auto" data-equalizer>

                <!-- Left white area -->
                <div class="columns medium-9 padding-lr-0 summary-area equal-areas" data-equalizer-watch>
	                 <div class="align-page">
		                <div class="padding-10">
                            <div class="row row-auto">

                                <div class="columns medium-6 wDate-lbl padding-left-0"><label class="font-30 inline logbook-summary-text"><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_logbook_2.png" width="49" /> Log Book</label>
                                    <a href="fetchGlucoseLogbook.action?patientId=${patientId}" class="button radius btn-teal">Glucose Logbook</a>
                                </div>

                                <div class="columns medium-6 padding-right-0">
                                    <div class="collapse row">
                                        <div class="small-2 columns"><button onclick="getPreWeek()" class="button btn-teal prefix">Prev</button></div>
                                        <div class="small-8 columns">
                                            <s:textfield id="selWeeklyLogsDate" data-calId="btnDateFrom"
                                                cssClass="patient-detail-text date-style" name="weeklyReportDateString"
                                                readonly="true"></s:textfield>
                                        </div>
                                        <div class="small-2 columns"><button id="logDateNext" onclick="getNextWeek()" class="button btn-teal postfix">Next</button> </div>
                                    </div>

                                    <div class="collapse row">
                                        <div class="small-2 columns">
                                            <label class="inline"> Filter by: </label>
                                        </div>
                                        <div class="small-10 columns">
                                            <select id="ddFilterBy">
                                                <option value="">All</option>
                                                <option value="ActivityMinute">Activity</option>
                                                <option value="Breakfast">Breakfast</option>
                                                <option value="Beverage">Beverage</option>
                                                <option value="Lunch">Lunch</option>
                                                <option value="Snack">Snack</option>
                                                <option value="Dinner">Dinner</option>
                                                <option value="Glucose">Glucose</option>
                                                <option value="Medication">Medication</option>
                                                <option value="Weight">Weight</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="columns medium-12 padding-lr-0">

                                    <label id="last-1-day-lbl" class="heading-style">last 1st day</label>
                                    <ul id="last-1-day" class="margin-zero-margin-left inline-list-logbook"><li class="centered-li">No log found</li></ul>

                                    <label id="last-2-day-lbl" class="heading-style">last 2nd day</label>
                                    <ul id="last-2-day" class="margin-zero-margin-left inline-list-logbook"><li class="centered-li">No log found</li></ul>

                                    <label id="last-3-day-lbl" class="heading-style">last 3rd day</label>
                                    <ul id="last-3-day" class="margin-zero-margin-left inline-list-logbook"><li class="centered-li">No log found</li></ul>

                                    <label id="last-4-day-lbl" class="heading-style">last 4th day</label>
                                    <ul id="last-4-day" class="margin-zero-margin-left inline-list-logbook"><li class="centered-li">No log found</li></ul>

                                    <label id="last-5-day-lbl" class="heading-style">last 5th day</label>
                                    <ul id="last-5-day" class="margin-zero-margin-left inline-list-logbook"><li class="centered-li">No log found</li></ul>

                                    <label id="last-6-day-lbl" class="heading-style">last 6th day</label>
                                    <ul id="last-6-day" class="margin-zero-margin-left inline-list-logbook"><li class="centered-li">No log found</li></ul>

                                    <label id="last-7-day-lbl" class="heading-style">last 7th day</label>
                                    <ul id="last-7-day" class="margin-zero-margin-left inline-list-logbook padding-bottom-12"><li class="centered-li">No log found</li></ul>

                            </div>
                        </div>

		                <s:hidden id="patientId" name="patientId"></s:hidden>

	                </div>
                </div>

                <!-- Left grey area -->
                <div id="divCoachesAreaLeft" class="columns medium-3 bg-grey equal-areas" data-equalizer-watch>
                    <script>
                        var patId = '${patientId}',
                                loggedInUserID = '${session.PROVIDER.providerId}';
                        $(document).ready(function(){
                            $("#divCoachesAreaLeft").load("${pageContext.request.contextPath}/educator/coachesArea.jsp");
                        });
                    </script>
                </div>

            </div>
         </div>
    </div>
    </div>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
 	<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>

	<script type="text/javascript">
	
		var oldLogsDate,
            //[2/10/2015]: added by, __oz
            FORMAT_DATE = "mm/dd/yy",
            TARGETS = ("${targetString}") ? $.parseJSON("${targetString}") : undefined,
            patId = '${patientId}',
            loggedInUserID = '${session.PROVIDER.providerId}',
            BASE_URL = '${pageContext.request.contextPath}/';
		
		$(function(){
            $('#unProcessedMeals').addClass('selected-tab');

            initLogBook();

            //load coaches area via ajax
            //Ajaxing.loadExternalPage($("#divCoachesAreaLeft"), "${pageContext.request.contextPath}/educator/coachesArea.jsp", loadExpCollapse);
            Ajaxing.loadExternalPage($("#divMemberNavArea"), "${pageContext.request.contextPath}/educator/memberNavArea.jsp", function(){
                $("#divMemberNavArea ul").css("top", "10px");
            });
        });

	</script>

    <script src="${pageContext.request.contextPath}/resources/js/hs.logbook.js?v=0.3"></script>
</body>
</html>