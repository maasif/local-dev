<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Patient Dashboard</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/>
   <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/member-nav-area.css" />

  </head>
  <body>

  <!-- messages, video call buttons etc. -->
  <ul class="inline-list patient-nav-btn-list nav-list-width left-align-52">
      <li>
          <a href="fetchReportPage.action?patientId=">
              <span>
                  <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/more-charts.png" width="24">
              </span>Summary
          </a>
      </li>
      <li>
          <a href="coachPatientMessages.action?patientId=">
              <span>
                  <img src="${pageContext.request.contextPath}/resources/css/images/message.png" width="24">
              </span>Messages
              <span id="spUnreadCountStatic" class="unread-count">0</span>
          </a>
          <div class="nav-divider"></div>
      </li>
      <li>
          <a href="caller.action?patientId=">
              <span><img src="${pageContext.request.contextPath}/resources/css/images/video_call.png" width="24"></span>Video Call
          </a>
          <div class="nav-divider"></div>
      </li>
      <li>
          <a href="fetchLogbook.action?patientId=">
              <span> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_logbook_2.png" width="24" /></span> Log Book
          </a>
          <div class="nav-divider"></div>
      </li>
      <li>
          <a id="btnEditMembersTeamNav" href="#">
              <span><img src="${pageContext.request.contextPath}/resources/css/images/provider.png" width="24"></span>Member's Team
          </a>
          <div class="nav-divider"></div>
      </li>
      <li>
          <a href="settingsPage.action?patientId=">
              <span><img src="${pageContext.request.contextPath}/resources/css/images/settings.png" width="24"></span>Settings
          </a>
          <div class="nav-divider"></div>
      </li>
  </ul>

  <!-- Edit Member Team Dialog -->
  <div id="editMemberTeamDialog" class="reveal-modal medium">

      <div id="errMessageMemberTeamDialog" class="alert-box alert alert-message-abs">______________________</div>

      <h3 class="meal-summary-text remove-bold">Edit Member's Team</h3>
      <div class="selected-providers-list">
          <div class="row row-auto">
              <div class="columns medium-6 large-6 six">
                  <label class="inline margin-bottom-0 label-dialog">Food Coaches</label>
                  <ul id="lstFoodCoaches" class="side-nav prov-ul"></ul>
              </div>
              <div class="columns medium-6 large-6 six">
                  <label class="inline margin-bottom-0 label-dialog">Coaches</label>
                  <ul id="lstCoaches" class="side-nav prov-ul"></ul>
              </div>
          </div>
          <small id="errorMemberTeam" class="error hide">Required.</small>
      </div>
      <div class="row row-auto">
          <div class="columns small-12">
              <ul class="side-nav teal-bullets-diag padding-left-0">
                  <li>
                      <label id="lblLeadCoach" class="hide">
                          <span id="spLeadCoach"></span><span class='label warning coach-designation-label lead-coach'>Lead Coach</span>
                      </label>
                  </li>
                  <li>
                      <label id="lblPrimaryCoach" class="hide">
                          <span id="spPrimaryFoodCoach"></span><span class='label warning coach-designation-label primary-coach'>Primary Food Coach</span>
                      </label>
                  </li>
              </ul>
          </div>
      </div>
      <br/>
      <div class="align-right">
          <input id="btnSaveMemberTeam" type="submit" class="button radius btn-teal min-width-250 small" value="Save" />
      </div>

      <a id="closeDialogMemberTeam" class="close-reveal-modal">&#215;</a>

      <div id="dialogLoadingSpinner" class="dialog-loading">
          <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Processing...</label>
      </div>

  </div>

  <script src="${pageContext.request.contextPath}/resources/js/hs.edit.members.team.js?v=0.4"></script>

  <script type="text/javascript">

      var MessageUnread = {

          refreshUnreadCount: function(){

              window.messageUnreadObject = this;

              var that = window.messageUnreadObject,
                      dataToSend = { "patientId" : patId },
                      postRequestActions = {
                          "successCallBack": that.onGettingMessagesCount
                      };

              Ajaxing.sendPostRequest("getUnreadMessagesCount.action", dataToSend, postRequestActions);
          },

          onGettingMessagesCount: function(data){
              var that = window.messageUnreadObject;
              $("#spUnreadCountStatic").text(data.DATA);
              that.alignUnreadCount();
          },

          alignUnreadCount: function(){
              var $span = $("#spUnreadCountStatic"),
                      unreadCount = parseInt($span.removeClass("left-5").text()),
                      leftClass = "";

              if(unreadCount > 10){
                  leftClass = "left-5";
              }

              $span.addClass(leftClass).text(unreadCount);
          }
      };

      $(function(){

          $(".patient-nav-btn-list a").each(function(index, a){
              $(a).attr("href", $(a).attr("href")+patId);
          });

          MessageUnread.refreshUnreadCount();

          EditMembersTeam.init(patId, "${pageContext.request.contextPath}");
      });

  </script>

  </body>          
</html>