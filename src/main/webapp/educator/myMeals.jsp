<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>

<head>      
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
  <title>My Meals</title>    
  <meta http-equiv="x-ua-compatible" content="IE=Edge"/>
   
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->
 	
 	
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/pagination/css/jPages.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/pagination/css/animate.css">
  
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/my_meals.css?v=0.1" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

  </head>
  
  <body>

      <!-- BreadCrumb -->
      <div class="breadcrumb-green">
          <div class="max-width-1129">
              <ul class="breadcrumbs">
                  <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                      <li>
                          <a href="unProcessedMeals.action">Meals</a>
                      </li>
                  </s:if>
                  <li>
                      <a href="members.action">My Members</a>
                  </li>
                  <li>
                      <a href="dashboard.action?patientId=${patientId}">Member Dashboard</a>
                  </li>
                  <li class="current">Past/Suggested Meals</li>
                  <li class="current"><b>Member Name:</b> <s:property value='%{patient.user.displayName}' /></li>
              </ul>
          </div>
      </div>

	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>			
			<p id="progressTxt" class="progress-text">We're loading data</p>
		</div>
	</div>

      <div id="loadingRemove" class="loader">
          <div class="image-loader">
              <p class="progress-please-wait">Please wait...</p>
              <div class="progress progress-striped active">
                  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                  </div>
              </div>
              <p class="progress-text">We're removing</p>
          </div>
      </div>

      <div id="divMemberNavArea"></div>
      <div class="dashboard-container margin-bottom-20">

          <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>

          <div class="row max-width-1129">

              <div class="coach-area-expand">
                  <a href="#" id="btnExpColCoachArea"><img src="${pageContext.request.contextPath}/resources/css/images/arrow-left.png" title="Hide" /></a>
              </div>

              <div class="columns medium-12">

                  <!-- Charts, goals and Coach Notes area -->
                  <div class="row bg-white row-auto" data-equalizer>

                      <!-- Right white area -->
                      <div class="columns medium-9 padding-lr-0 summary-area equal-areas" data-equalizer-watch>
                        <div>

                            <input type="hidden" name="patientId" id="txtPatientId" />
                            <input type="hidden" name="mealType" />

                            <div id="divFavoriteMeals">

                                <div class="border-bt-heading row">
                                    <div class="columns large-8 padding-left-0">
                                        <h4 class="padding-left-13">Favorite Meals</h4>
                                    </div>
                                    <div class="columns large-4 align-right">
                                        <select name="mealType" id="ddLogType" class="display-inline-block">
                                            <option value="0">All</option>
                                            <option value="1">Breakfast</option>
                                            <option value="2">Lunch</option>
                                            <option value="3">Snack</option>
                                            <option value="4">Dinner</option>
                                            <option value="4">Beverage</option>
                                        </select>
                                    </div>
                                </div>

                                <ul id="itemContainerFavoriteMeals" class="margin-0 item-container" data-equalizer>

                                </ul>

                                <div id="holderFavoriteMeals" class="holder"></div>

                            </div>

                            <div id="divSuggestedMeals">

                                <div class="border-bt-heading row">
                                    <div class="columns large-12 padding-left-0">
                                        <h4 class="padding-left-13">Suggested Meals</h4>
                                    </div>
                                </div>

                                <ul id="itemContainerSuggestedMeals" class="margin-0 item-container" data-equalizer>

                                </ul>

                                <div id="holderSuggestedMeals" class="holder"></div>

                            </div>

                            <div id="divPastMeals">

                                <div class="border-bt-heading row">
                                    <div class="columns large-8 padding-left-0">
                                        <h4 id="pageHeadingText" class="padding-left-13">Past Meals</h4>
                                    </div>
                                </div>

                                <ul id="itemContainer" class="margin-0 item-container" data-equalizer>

                                </ul>

                                <div id="holderPastMeals" class="holder"></div>

                            </div>

                        </div>
                      </div>

                      <!-- Left grey area -->
                      <div id="divCoachesAreaLeft" class="columns medium-3 bg-grey equal-areas" data-equalizer-watch>
                          <script>
                              var patId = '${patientId}',
                                      loggedInUserID = '${session.PROVIDER.providerId}';
                              $(document).ready(function(){
                                  $("#divCoachesAreaLeft").load("${pageContext.request.contextPath}/educator/coachesArea.jsp");
                              });
                          </script>
                      </div>

                  </div>
              </div>
           </div>
      </div>

      <div id="confirmationAlert" class="reveal-modal small" data-reveal>
          <h3 class="meal-summary-text">Remove Suggested Meal</h3>
          <p class="lead dialog-para">Are you sure you want to remove this meal?</p>
          <a id="closeConfirmDialog" class="close-reveal-modal">&#215;</a>
          <div class="align-right">
              <a href="#" id="btnConfirmDelete" class="button radius btn-teal margin-top-10 margin-bottom-0">Yes</a>
              <a href="#" class="button secondary radius margin-top-10 margin-bottom-0" onclick="$('#closeConfirmDialog').click(); return false;">No</a>
          </div>
      </div>

	  <!--[if lt IE 9]>
	    	<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
		<![endif]-->
		<!--[if (gte IE 9) | (!IE)]><!-->
		    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>				       
		    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
		<!--<![endif]-->       
	        
	    <!-- Foundation 3 for IE 8 and earlier -->
		<!--[if lt IE 9]>	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
		<![endif]-->
		
		<!-- Foundation 4 for IE 9 and later -->
		<!--[if gt IE 8]><!-->	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
		<!--<![endif]-->

      <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
       
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/pagination/js/highlight.pack.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/pagination/js/tabifier.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/pagination/js/js.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/pagination/js/jPages.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/pagination/js/jquery.lazyload.js"></script>
	       
      <script type="text/javascript">
		
      	var RECENT_MEALS_DATA = ("${jsonData}") ? $.parseJSON("${jsonData}") : undefined,
            SUGGESTED_MEALS_DATA = ("${suggestedMealsData}") ? $.parseJSON("${suggestedMealsData}") : undefined,
     		ajaxing = Object.create(Ajaxing),
       		alertMessage = Object.create(AlertMessage).init(),
            patId = '${patientId}',
            loggedInUserID = '${session.PROVIDER.providerId}',
            mealType = '${mealType}', logId;
      
        $(function(){
            $(document).foundation();   
            $("#unProcessedMeals").addClass("selected-tab");

            var $ddLogType = $("#ddLogType");
            $ddLogType.find("option:contains('${mealType}')").prop("selected", "selected")
                    .end()
                    .on("change", function(){
                        var selectedMealType = $ddLogType.find("option:selected").text();
                        getMealsFromServer(selectedMealType);
                });

            getMealsFromServer(mealType);

            Ajaxing.loadExternalPage($("#divMemberNavArea"), "${pageContext.request.contextPath}/educator/memberNavArea.jsp", function(){
                $("#divMemberNavArea ul").css("top", "10px");
            });
        });

        function getMealsFromServer(selectedMealType){
            //ajax parameters
            var dataToSend = { "patientId" : "${patientId}", "mealType": selectedMealType},
                    postRequestActions = {
                        "loading": $("#loading"),
                        "successCallBack": onReceiveLogs
                    };

            ajaxing.sendPostRequest("getMealsByType.action", dataToSend, postRequestActions);
        }

        function onReceiveLogs(result){
        	if(result.STATUS == "SUCCESS"){        		
        		renderRecentMeals($.parseJSON(result.DATA));
                renderSuggestedMeals($.parseJSON(result.SUGGESTED_MEALS));
                renderFavoriteMeals($.parseJSON(result.FAVORITE_MEALS));
        	}else{        		
        		//ajaxing.showErrorOrRedirect(result, $("#errMessage"), "Unable to get meals. Please try again later.");
        		renderRecentMeals(undefined);
                renderSuggestedMeals(undefined);
                renderFavoriteMeals(undefined);
        	}
        }
        
        function renderRecentMeals(mealsData){
        	var $ul = $("#itemContainer").removeAttr("style").empty(),
        		html = "";
        	
        	if(mealsData){
	        	$.each(mealsData, function(index, meal){
	        		
	        		var classLabel = "warning",
	        			labelText = "Not Processed",
	        			mealName = "<span class='meal-name'> <b>Meal Name: </b> N/A </span>",
                        processedBy = "";

	        		if(meal.isProcessed == 'true' || meal.isProcessed == true){
	        			classLabel = "success";
	        			labelText = Math.round(meal.carbs) + "g";
	        			
		        		if(meal.hasMissingFood == 'true' || meal.hasMissingFood == true){
		        			labelText = Math.round(meal.carbs) + "+g";
		        			classLabel = "success carbs-label";
		        		}

                        if(meal.unit){
                            labelText += " / " + meal.unit;
                        }
                        processedBy = "<div class=\"processed-by\"><span>Processed By: </span>" + meal.processedBy + "</div>";
	        		}
	        		
	        		if(meal.mealName){
	        			mealName = "<span class='meal-name'> <b>Meal Name: </b>" + ((meal.mealName) ? meal.mealName : "") + "</span>";
	        		}
	        		
	        		var imageSrc = meal.imageSrc;

	        		if(imageSrc == "${pageContext.request.contextPath}/logImages/"){
	        			imageSrc = "${pageContext.request.contextPath}/resources/css/images/no_image_available.svg";
	        		}
	        		var processedLabel = "<span class='label process-label "+classLabel+"'>" + labelText + "</span>";

	        		html += "<li data-equalizer-watch><a href='fetchLogDetail.action?logId="+meal.logId+"&amp;patientId=${patientId}&amp;isFromError=no&amp;redirectToPage=myMeals'><div class='recent-list-item'><img src='" + imageSrc + "' />" + mealName + "<span class='meal-type'><b>Meal Type: </b>" + meal.type + "</span><span class='date-time'>"+ meal.logTime + "</span>" + processedLabel + processedBy + "</div></a></li>";
	        	});  
	        	
	        	$ul.append(html);
	        	
	        	$("#holderPastMeals").jPages({
	                containerID : "itemContainer",
	                previous : "Previous",
	                next : "Next",
	                perPage: 10
	            });
        	}else{
                $("#holderPastMeals").html("<p class='align-center'>No data found</p>");
        	}
        }

        function renderSuggestedMeals(mealsData){
            var $ul = $("#itemContainerSuggestedMeals").removeAttr("style").empty(),
                    html = "";

            if(mealsData){
                $.each(mealsData, function(index, meal){

                    var classLabel = "success",
                        labelText = "Not Processed",
                        mealName = "<span class='meal-name'> <b>Meal Name: </b> N/A </span>",
                        minorName = (meal.minorFoodName) ? meal.minorFoodName : "",
                        mealNameWithMinor = ((meal.mealName) ? meal.mealName : "") + " " + minorName,
                        isDrafted = meal.isDrafted;

                    labelText = Math.round(meal.carbs) + "g";

                    if(meal.unit){
                        labelText += " / " + meal.unit;
                    }

                    if(meal.mealName){
                        mealName = "<span class='meal-name'> <b>Meal Name: </b>" + mealNameWithMinor + "</span>";
                    }

                    var imageSrc = meal.imageSrc;

                    if(imageSrc == "${pageContext.request.contextPath}/logImages/"){
                        imageSrc = "${pageContext.request.contextPath}/resources/css/images/no_image_available.svg";
                    }
                    var processedLabel = "<span class='label process-label "+classLabel+"'>" + labelText + "</span>",
                        draftedLabel = "";

                    if(!isDrafted){
                        draftedLabel = "<span class='label warning drafted-label'>Draft</span>";
                    }

                    html += "<li data-equalizer-watch>"
                                + "<a href='suggestedMealDetails.action?suggestedLogId="+meal.suggestedLogId+"&amp;patientId=${patientId}&amp;redirectToPage=myMeals' title='"+mealNameWithMinor+"'>"
                                +    "<div class='recent-list-item'><img src='" + imageSrc + "' />" + mealName + "<span class='meal-type'><b>Meal Type: </b>" + meal.type + "</span><span class='date-time'>"+ meal.logTime + "</span>" + processedLabel + draftedLabel + "<br /><span class='processed-by'>Created By: " + meal.createdBy + "</span>" + "</div>"
                                + "</a>"
                                + "<a href='javascript:;' class='button expand tiny btn-red margin-bottom-0 remove-suggested-meal' data-mealtype='"+ $("#ddLogType").find("option:selected").text() +"' data-logid="+meal.logId+"> Remove </a>"
                            + "</li>";
                });

                $ul.append(html);

                $("#holderSuggestedMeals").jPages({
                    containerID : "itemContainerSuggestedMeals",
                    previous : "Previous",
                    next : "Next",
                    perPage: 10
                });
            }else{
                $("#holderSuggestedMeals").html("<p class='align-center'>No data found</p>");
            }

            bindRemoveSuggestedMeal();
        }

        function renderFavoriteMeals(mealsData){
            var $ul = $("#itemContainerFavoriteMeals").removeAttr("style").empty(),
                    html = "";

            if(mealsData){
                $.each(mealsData, function(index, meal){

                    var classLabel = "warning",
                            labelText = "Not Processed",
                            mealName = "<span class='meal-name'> <b>Meal Name: </b> N/A </span>",
                            processedBy = "";

                    if(meal.isProcessed == 'true' || meal.isProcessed == true){
                        classLabel = "success";
                        labelText = Math.round(meal.carbs) + "g";

                        if(meal.hasMissingFood == 'true' || meal.hasMissingFood == true){
                            labelText = Math.round(meal.carbs) + "+g";
                            classLabel = "success carbs-label";
                        }

                        processedBy = "<div class=\"processed-by\"><span>Processed By: </span>" + meal.processedBy + "</div>";
                    }

                    if(meal.unit){
                        labelText += " / " + meal.unit;
                    }

                    if(meal.mealName){
                        mealName = "<span class='meal-name'> <b>Meal Name: </b>" + ((meal.mealName) ? meal.mealName : "") + "</span>";
                    }

                    var imageSrc = meal.imageSrc;

                    if(imageSrc == "${pageContext.request.contextPath}/logImages/"){
                        imageSrc = "${pageContext.request.contextPath}/resources/css/images/no_image_available.svg";
                    }
                    var processedLabel = "<span class='label process-label "+classLabel+"'>" + labelText + "</span>";

                    html += "<li data-equalizer-watch><a href='fetchLogDetail.action?logId="+meal.logId+"&amp;patientId=${patientId}&amp;isFromError=no&amp;redirectToPage=myMeals'><div class='recent-list-item'><img src='" + imageSrc + "' />" + mealName + "<span class='meal-type'><b>Meal Type: </b>" + meal.type + "</span><span class='date-time'>"+ meal.logTime + "</span>" + processedLabel + processedBy + "</div></a></li>";
                });

                $ul.append(html);

                $("#holderFavoriteMeals").jPages({
                    containerID : "itemContainerFavoriteMeals",
                    previous : "Previous",
                    next : "Next",
                    perPage: 10
                });
            }else{
                $("#holderFavoriteMeals").html("<p class='align-center'>No data found</p>");
            }
        }

        function bindRemoveFavoriteMeal(){

            $("#btnConfirmDelete").off("click").on("click", function(e){
                e.preventDefault();

                //ajax parameters
                var dataToSend = { "patientId": "${patientId}", "mealType": mealType, "logId": logId},
                        postRequestActions = {
                            "loading": $("#loadingRemove"),
                            "successCallBack": onRemoveFavoriteMealLog
                        };

                ajaxing.sendPostRequest("removeFavoriteMeal.action", dataToSend, postRequestActions);
            });

            $(".remove-suggested-meal").off("click").on("click", function(){

                var $this = $(this);

                mealType = $this.data("mealtype");
                logId = $this.data("logid");

                $("#confirmationAlert").foundation("reveal", "open");
            });
        }

        function bindRemoveSuggestedMeal(){

            $("#btnConfirmDelete").off("click").on("click", function(e){
                e.preventDefault();

                //ajax parameters
                var dataToSend = { "patientId": "${patientId}", "mealType": mealType, "logId": logId},
                    postRequestActions = {
                        "loading": $("#loadingRemove"),
                        "successCallBack": onRemoveSuggestedMealLog
                    };

                ajaxing.sendPostRequest("removeSuggestedMeal.action", dataToSend, postRequestActions);
            });

            $(".remove-suggested-meal").off("click").on("click", function(){

                var $this = $(this);

                mealType = $this.data("mealtype");
                logId = $this.data("logid");

                $("#confirmationAlert").foundation("reveal", "open");
            });
        }

        function onRemoveSuggestedMealLog(result){

            var $message = $("#errMessage");

            $("#confirmationAlert").foundation("reveal", "close");

            if(result && result.STATUS == "SUCCESS"){
                alertMessage.showAlertMessage($message, "Suggested meal removed successfully.", alertMessage.SUCCESS);
                renderSuggestedMeals($.parseJSON(result.DATA));
            }else{
                alertMessage.showAlertMessage($message, "Unable to remove Suggested meal.", alertMessage.ERROR);
            }
        }

        function onRemoveFavoriteMealLog(result){

            var $message = $("#errMessage");

            $("#confirmationAlert").foundation("reveal", "close");

            if(result && result.STATUS == "SUCCESS"){
                alertMessage.showAlertMessage($message, "Meal unfavorite successfully.", alertMessage.SUCCESS);
                renderSuggestedMeals($.parseJSON(result.DATA));
            }else{
                alertMessage.showAlertMessage($message, "Unable to unfavorite meal.", alertMessage.ERROR);
            }
        }

      </script>
  </body>          
</html>