<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Patient Dashboard</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css?v=0.16" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

    <script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>

  </head>
  
  <body>

  <!-- BreadCrumb -->
   <div class="breadcrumb-green">
      <div class="max-width-1129">
          <ul class="breadcrumbs">
              <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                  <li>
                      <a href="unProcessedMeals.action">Meals</a>
                  </li>
              </s:if>
              <li>
                  <a href="members.action">My Members</a>
              </li>
              <li class="current">Member Dashboard</li>
          </ul>
      </div>
   </div>

	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>			
			<p id="progressTxt" class="progress-text">We're working</p>
		</div>
	</div>

    <div class="pat-exp-col">
      <div class="exp-col">
          <a href="#" id="btnExpColPatInfo"><img src="${pageContext.request.contextPath}/resources/css/images/arrow-up.png" title="Hide member information" width="16" /> <span>Hide member information</span></a>
      </div>
    </div>

	<div class="dashboard-container margin-bottom-20">

	  <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>

          <input type="hidden" id="txtDateString" name="dashboardDateString">

          <div class="row max-width-1129">

              <div class="coach-area-expand top-132">
                  <a href="#" id="btnExpColCoachArea"><img src="${pageContext.request.contextPath}/resources/css/images/arrow-left.png" height="25" title="Hide" /></a>
              </div>

            <div class="columns large-12">

                  <!-- Name, ID and settings row -->
                  <div class="row row-auto">

                      <div id="divEmpty" class="columns large-1">
                          &nbsp;
                      </div>
                      <div id="divPatNameId" class="columns large-4">
                          <div class="hs-page-heading">
                              <h2> <span id="divPatName"> <span id="spDisplayName"> <s:property value="%{patient.user.displayName}" ></s:property></span> | </span> <span id="divPatID" data-id="<s:property value='%{patient.patientId}' />"><s:property value='%{patient.patientId}' /></span> <a id="btnSecureLink" href="javascript:;"><img src="${pageContext.request.contextPath}/resources/css/images/secure-link.png" title="Secure Link" width="40" ></a> </h2>
                          </div>
                      </div>
                      <div class="columns large-6 align-right">

                          <!-- messages, video call buttons etc. -->
                          <ul class="inline-list patient-nav-btn-list">
                              <li>
                                  <a href="coachPatientMessages.action?patientId=${patientId}">
                                      <span>
                                          <img src="${pageContext.request.contextPath}/resources/css/images/message.png" width="24">
                                      </span>Messages
                                      <span id="spUnreadCount" class="unread-count"><s:property value='%{messagesCount}'></s:property></span>
                                  </a>
                              </li>
                              <li>
                                  <a href="caller.action?patientId=${patientId}">
                                    <span><img src="${pageContext.request.contextPath}/resources/css/images/video_call.png" width="24"></span>Video Call
                                  </a>
                                  <div class="nav-divider"></div>
                              </li>
                              <li>
                                  <a href="fetchLogbook.action?patientId=${patientId}">
                                    <span> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_logbook_2.png" width="24" /></span> Log Book
                                  </a>
                                  <div class="nav-divider"></div>
                              </li>
                              <li>
                                  <a id="btnEditMembersTeamNav" href="#">
                                      <span><img src="${pageContext.request.contextPath}/resources/css/images/provider.png" width="24"></span>Member's Team
                                  </a>
                                  <div class="nav-divider"></div>
                              </li>
                              <li>
                                  <a href="settingsPage.action?patientId=${patientId}">
                                      <span><img src="${pageContext.request.contextPath}/resources/css/images/settings.png" width="24"></span>Settings
                                  </a>
                                  <div class="nav-divider"></div>
                              </li>
                              <%--<li>
                                  <a href="fetchLogs.action?patientId=${patientId}">
                                      <span><img src="${pageContext.request.contextPath}/resources/css/images/logs_icon.png" width="24"></span>Logs
                                  </a>
                                  <div class="nav-divider"></div>
                              </li>--%>
                          </ul>

                      </div>
                  </div>

                  <!-- Patient image, email and phone row -->
                  <div id="divPatientInfo" class="row row-auto bg-brown">
                      <div class="columns large-1">
                          <s:if test="%{patient.imagePath != null}">
                              <img src="${pageContext.request.contextPath}${patient.imagePath}" class="patient-image" />
                          </s:if>
                          <s:else>
                              <img src="${pageContext.request.contextPath}/resources/css/images/default_profile_image.gif" class="patient-image" />
                          </s:else>
                      </div>
                      <div class="columns large-2">
                          <div class="label-group padding-left-94">
                              <label class="inline uppercase margin-bottom-0">Full Name</label>
                              <label id="patFullName" class="inline margin-bottom-0"><s:property value="%{patient.user.firstName}" ></s:property><br/><s:property value="%{patient.user.lastName}" ></s:property></label>
                          </div>
                      </div>
                      <div class="columns large-8">
                          <ul class="small-block-grid-5 member-details-banner">
                              <li>
                                  <div class="label-group">
                                      <s:if test="%{patient.mrn != null}">
                                          <label class="inline uppercase margin-bottom-0">MRN</label>
                                          <label class="inline margin-bottom-0"><s:property value="%{patient.mrn}" ></s:property></label>
                                      </s:if>
                                      <s:else>
                                          <label class="inline uppercase margin-bottom-0">MRN</label>
                                          <label class="inline margin-bottom-0">N/A</label>
                                      </s:else>
                                  </div>
                              </li>
                              <li>
                                  <div class="label-group">
                                      <s:if test="%{meterSerialNumber == 'N/A'}">
                                          <label class="inline uppercase margin-bottom-0">Birth date</label>
                                          <label class="inline margin-bottom-0"><s:property value="%{memberDOB}" ></s:property></label>
                                      </s:if>
                                      <s:else>
                                          <label class="inline uppercase margin-bottom-0">Meter Number</label>
                                          <label class="inline margin-bottom-0"><s:property value="%{meterSerialNumber}"></s:property></label>
                                      </s:else>
                                  </div>
                              </li>
                              <li>
                                  <div class="label-group">
                                      <label class="inline uppercase margin-bottom-0">Phone Number</label>
                                      <s:if test="%{patient.user.phone != null}">
                                          <label id="dashboardPhone" class="inline margin-bottom-0"><s:property value="%{patient.user.phone}" ></s:property></label>
                                      </s:if>
                                      <s:else>
                                          <label class="inline margin-bottom-0">N/A</label>
                                      </s:else>
                                  </div>
                              </li>
                              <li>
                                  <div class="label-group">
                                      <label class="inline uppercase margin-bottom-0">Phone / App Version</label>
                                      <label class="inline margin-bottom-0"><s:property value="%{phoneVersion}" ></s:property></label>
                                  </div>
                              </li>
                              <li>
                                  <div class="label-group">
                                      <label class="inline uppercase margin-bottom-0">Email</label>
                                      <label class="inline margin-bottom-0 word-break"><s:property value="%{patient.user.email}" ></s:property></label>
                                  </div>
                              </li>
                          </ul>
                      </div>
                      <div class="columns large-2 align-right">
                            <label class="inline margin-bottom-0 padding-top-13"><a href="addEditPatient.action?patientId=${patientId}" class="button tiny radius btn-teal margin-bottom-0">Edit Member Details</a></label>
                      </div>
                  </div>

                  <!-- Charts, goals and Coach Notes area -->
                  <div class="row bg-white row-auto" data-equalizer>

                      <!-- Right white area -->
                      <div class="columns large-9 padding-lr-0 summary-area equal-areas" data-equalizer-watch>

                          <div class="row row-auto margin-top-10">
                              <div class="columns large-3 border-right-grey">
                                  <div class="patient-motivation align-center">
                                      <h4>Motivation</h4>
                                      <s:if test="%{patientMotivationImage != null && patientMotivationImage.isPublic}">
                                          <a id="btnMotivationImage" title="Click to enlarge" href="javascript:;"><img title="Click to enlarge" src="${pageContext.request.contextPath}/motivationImages/${patientMotivationImage.imageName}" class="patient-motivation-image" /></a>
                                          <p><s:property value="%{patientMotivationImage.description}"></s:property></p>
                                      </s:if>
                                      <s:else>
                                          <img src="${pageContext.request.contextPath}/resources/css/images/no_image_available.svg" class="patient-motivation-image" />
                                      </s:else>
                                  </div>
                              </div>
                              <div class="columns large-9">
                                  <h4>Goals & Habits <a id="btnSeeAllGoalsBtn" href="javascript:;" class="see-all-goals logbook-summary-text right">See all goals</a> </h4>
                                  <div class="goals-habits-data">
                                      <div class="row row-auto goals-header">
                                          <div class="columns large-1 padding-lr-0">
                                              <label class="inline">Type</label>
                                          </div>
                                          <div class="columns large-5 padding-left-0">
                                              <label class="inline">Goal</label>
                                          </div>
                                          <div class="columns large-6">
                                              <label class="inline">Member Self Assessment</label>
                                          </div>
                                      </div>

                                      <div class="goals-habits-scrollable-top">
                                          <ul id="goalListTop" class="listing large-block-grid-1">
                                          </ul>
                                          <div id="loadingGoalsHabitsTop" class="chart-loading">
                                              <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="row row-auto">
                              <div class="grey-divider">

                                  <!-- Glucose chart area -->
                                  <div class="columns large-4 padding-lr-0">
                                      <div class="chart-section glucose-summary">
                                          <div class="chart-header">
                                              <%--<a href="fetchReportPage.action?patientId=${patientId}">--%>
                                                  <ul class="inline-list">
                                                       <li>
                                                          <span class="image-span">
                                                               <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/post_icon_glucose.png" width="43" />
                                                           </span>
                                                       </li>
                                                      <li>
                                                          <div class="label-group label-group-chart">
                                                              <label class="inline uppercase margin-bottom-0">Glucose Summary</label>
                                                              <label class="inline margin-bottom-0"><s:property value="%{formattedDateString}" ></s:property></label>
                                                          </div>
                                                      </li>
                                                  </ul>
                                              <%--</a>--%>
                                              <a href="fetchReportPage.action?patientId=${patientId}#glucose" class="open-new"><img src="${pageContext.request.contextPath}/resources/css/images/open_in_new.png" width="24" title="Open details" /></a>

                                          </div>
                                          <div class="chart-data">
                                              <div id="glucoseChartContainer" class="glucose-chart">
                                                  <label id="glucoseChartNoData" class="no-data glucose-summary-text">No data.</label>
                                              </div>
                                              <div class="total-glucose-tests">
                                                  <div class="grey-label-group">
                                                      <label class="inline uppercase margin-bottom-0">Total Tests</label>
                                                      <label id="lblGlucoseTotal" class="inline margin-bottom-0">0</label>
                                                  </div>
                                              </div>
                                              <div class="avg-target-glucose">
                                                  <div class="columns large-6">
                                                      <div class="grey-label-group">
                                                          <label class="inline uppercase margin-bottom-0">Average</label>
                                                          <label id="avgGlucose" class="inline margin-bottom-0">N/A</label>

                                                      </div>
                                                  </div>
                                                  <div class="columns large-6">
                                                      <div class="grey-label-group">
                                                          <label class="inline uppercase margin-bottom-0">Target</label>
                                                          <label class="inline margin-bottom-0"><s:property value="%{patient.target.glucoseMin2}"/>-<s:property value="%{patient.target.glucoseMax2}"/> mg/dl</label>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="chart-loading only-chart-loading">
                                          <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                      </div>
                                      <div class="chart-divider"></div>
                                  </div>

                                  <!-- Meal chart area -->
                                  <div class="columns large-4 padding-lr-0">
                                      <div class="chart-section meal-summary">
                                          <div class="chart-header">
                                              <%--<a href="fetchReportPage.action?patientId=${patientId}">--%>
                                                  <ul class="inline-list">
                                                      <li>
                                                          <span class="image-span">
                                                               <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_food_summary.png" width="42" />
                                                           </span>
                                                      </li>
                                                      <li>
                                                          <div class="label-group label-group-chart">
                                                              <label class="inline uppercase margin-bottom-0">Meal Summary</label>
                                                              <label class="inline margin-bottom-0"><s:property value="%{formattedDateString}" ></s:property></label>
                                                          </div>
                                                      </li>
                                                  </ul>
                                              <%--</a>--%>
                                              <a href="fetchReportPage.action?patientId=${patientId}#meals" class="open-new"><img src="${pageContext.request.contextPath}/resources/css/images/open_in_new.png" width="24" title="Open details" /></a>
                                          </div>
                                          <div class="chart-data">
                                              <h2 class="align-center margin-bottom-0 meal-summary-text"><span id="spMealsAverage"></span><span class="steps-text"> gm/meal</span></h2>
                                              <label class="inline uppercase margin-bottom-0 align-center logbook-summary-text padding-top-0">&nbsp;</label>

                                              <h2 class="align-center margin-bottom-0 meal-summary-text"><span id="spMealsPercentage"></span></h2>
                                              <label class="inline margin-bottom-0 align-center logbook-summary-text padding-top-0 " id="lblWeeklyPercentage">meals below carb ceiling</label>

                                              <div class="align-center">
                                                <a href="myMealsPage.action?patientId=<s:property value="%{patientId}"/>&mealType=All" class="align-center button radius tiny btn-teal margin-bottom-0"> Show Meals</a>
                                              </div>

                                          </div>

                                          <%--<div class="chart-data">--%>
                                               <%--<div id="mealChartContainer" class="meal-chart">--%>
                                                   <%--<label id="mealChartNoData" class="no-data meal-summary-text">No data.</label>--%>
                                               <%--</div>--%>
                                               <%--<label id="lblMealsUnder" class="meals-under hide">meals under <br/> carb ceilings</label>--%>
                                          <%--</div>--%>
                                      </div>
                                      <div class="chart-loading only-chart-loading">
                                          <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                      </div>
                                      <div class="chart-divider chart-divider-height-115"></div>
                                  </div>

                                  <!-- Activity steps area -->
                                  <div class="columns large-4 padding-lr-0">
                                      <div class="chart-section activity-summary">
                                          <div class="chart-header">
                                              <ul class="inline-list">
                                                  <li>
                                                      <span class="image-span">
                                                           <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_activity_summary.png" width="56" />
                                                       </span>
                                                  </li>
                                                  <li>
                                                      <div class="label-group label-group-chart">
                                                          <label class="inline uppercase margin-bottom-0">Activity Summary</label>
                                                          <label class="inline margin-bottom-0"><s:property value="%{formattedDateString}" ></s:property></label>
                                                      </div>
                                                  </li>
                                              </ul>
                                          </div>
                                          <div class="chart-data">
                                              <h2 id="divAvgSteps" class="align-center margin-bottom-0 activity-summary-text">0<span class="steps-text"> steps</span></h2>
                                              <label class="inline uppercase margin-bottom-0 align-center logbook-summary-text padding-top-0">DAILY AVERAGE</label>

                                              <h2 class="align-center margin-bottom-0 activity-summary-text"><s:property value="%{targetSteps}" ></s:property><span class="steps-text"> steps</span></h2>
                                              <label class="inline uppercase margin-bottom-0 align-center logbook-summary-text padding-top-0">DAILY GOAL</label>

                                              <div id="divMisfitBatteryStatus" class="misfit-battery-status align-center">
                                                  <span>Misfit Battery Status:</span> <span class="label warning"><s:property value="%{misfitBatteryStatus}"/>%</span>
                                              </div>
                                          </div>
                                          <div id="loadingMisfitActivity" class="chart-loading">
                                              <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                          </div>
                                      </div>

                                  </div>

                              </div>

                              <div class="grey-divider">

                                <!-- Topics area -->
                                <div class="columns large-4 padding-lr-0">
                                      <div class="chart-section topic-summary">
                                          <div class="chart-header">
                                              <%--<a href="topicsOverview.action?patientId=${patientId}">--%>
                                                  <ul class="inline-list">
                                                      <li>
                                                          <span class="image-span">
                                                               <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_weight_2.png" width="47" />
                                                           </span>
                                                      </li>
                                                      <li>
                                                          <div class="label-group label-group-chart">
                                                              <label class="inline uppercase margin-bottom-0">Topics</label>
                                                              <label class="inline margin-bottom-0"><s:property value="%{formattedDateString}" ></s:property></label>
                                                          </div>
                                                      </li>
                                                  </ul>
                                              <%--</a>--%>
                                              <a id="curriculumLink" href="topicsOverview.action?patientId=${patientId}&uuid=${uuid}&week=1" class="open-new"><img src="${pageContext.request.contextPath}/resources/css/images/open_in_new.png" width="24" title="Open details" /></a>
                                          </div>
                                          <div class="chart-data align-center">
                                              <label class="inline uppercase margin-bottom-0 logbook-summary-text padding-bottom-0"> Most recent content viewed</label>
                                              <h3 class="margin-bottom-0 topic-summary-text">Week <span id="progressSummaryWeek"> 0 </span></h3>

                                              <label class="inline uppercase margin-bottom-0 logbook-summary-text padding-bottom-0"> Started on <span id="lblWeekStartedOn">N/A</span> </label>
                                              <%--<h3 class="margin-bottom-0 topic-summary-text"><span id="lblWeeksAgo"> 0 </span> week(s) ago</h3>--%>
                                          </div>
                                          <div id="loadingCurriculumTopics" class="chart-loading">
                                              <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                          </div>
                                      </div>
                                     <div class="chart-divider chart-divider-height-115"></div>
                                  </div>

                                <!-- Meds Taken area -->
                                <div class="columns large-4 padding-lr-0">
                                      <div class="chart-section logbook-summary">
                                          <div class="chart-header">
                                              <%--<a href="fetchLogbook.action?patientId=${patientId}">--%>
                                                  <ul class="inline-list">
                                                      <li>
                                                          <span class="image-span">
                                                               <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_meds.png" width="49" />
                                                           </span>
                                                      </li>
                                                      <li>
                                                          <div class="label-group label-group-chart">
                                                              <label class="inline uppercase margin-bottom-0">Meds Taken Summary</label>
                                                              <label class="inline margin-bottom-0"><s:property value="%{formattedDateString}" ></s:property></label>
                                                          </div>
                                                      </li>
                                                  </ul>
                                              <%--</a>--%>
                                              <a href="fetchReportPage.action?patientId=${patientId}#medication" class="open-new"><img src="${pageContext.request.contextPath}/resources/css/images/open_in_new.png" width="24" title="Open details" /></a>
                                          </div>
                                          <div class="chart-data">
                                              <div id="medsChartContainer" class="meds-chart">
                                                  <label id="medsChartNoData" class="no-data glucose-summary-text">No data.</label>
                                              </div>
                                          </div>
                                          <div class="chart-loading only-chart-loading">
                                              <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                          </div>
                                      </div>
                                    <div class="chart-divider"></div>
                                  </div>

                                <!-- Group Activity area -->
                                <div class="columns large-4 padding-lr-0">
                                      <div class="chart-section group-activity-summary">
                                          <div class="chart-header">
                                              <ul class="inline-list">
                                                  <li>
                                                      <span class="image-span">
                                                           <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_group.png" width="49" />
                                                       </span>
                                                  </li>
                                                  <li>
                                                      <div class="label-group label-group-chart">
                                                          <label class="inline uppercase margin-bottom-0">Group Activity</label>
                                                          <label class="inline margin-bottom-0"><s:property value="%{formattedDateString}" ></s:property></label>
                                                      </div>
                                                  </li>
                                              </ul>
                                          </div>
                                          <div class="chart-data">
                                                <ul class="side-nav group-activity-list padding-top-0">
                                                    <li>
                                                        <a href="javascript:void(0)">
                                                            <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/family.png" width="32" />
                                                            <span id="spSharedPosts" class="count">0</span> <span class="text"> SHARED POSTS </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">
                                                            <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/post_icon_like_dis.png" width="32" />
                                                            <span id="spLikes" class="count">0</span> <span class="text"> LIKES </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0)">
                                                            <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/icon_post_comment_dis.png" width="32" />
                                                            <span id="spComments" class="count">0</span> <span class="text"> COMMENTS </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                          </div>
                                          <div id="loadingActivityUser" class="chart-loading">
                                              <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                          </div>
                                      </div>

                                </div>

                              </div>

                              <!-- More charts button -->
                              <div class="min-height-80">

                                  <div class="more-charts">

                                      <div class="columns large-4">
                                          <a href="fetchLogbook.action?patientId=${patientId}" id="logEntries">
                                               <span class="image-span">
                                                   <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_logbook_2.png" width="49" />
                                                   <span id="spLogEntriesCount" class="align-center margin-bottom-0 logbook-summary-text align-log-count"><s:property value="%{logEntriesCount}" ></s:property></span>
                                                   <span class="inline uppercase margin-bottom-0 align-center logbook-summary-text padding-top-0 align-log-text">LOG ENTRIES <span id="spLogEntriesDate" class="font-size-12">(<s:property value="%{formattedDateString}" ></s:property>)</span> </span>
                                               </span>
                                          </a>
                                          <div class="chart-loading log-weight-loading">
                                              <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                          </div>
                                      </div>

                                      <div class="columns large-4">
                                          <a href="javascript:;">
                                               <span class="image-span">
                                                   <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/ic_progress_weight.png" width="49" />

                                                   <s:if test="%{patient.target != null}">
                                                       <p class="starting-weight">
                                                           <span class="align-center margin-bottom-0 align-log-count topic-summary-text"><s:property value="%{startingWeight}" ></s:property></span>
                                                           <span class="inline uppercase margin-bottom-0 align-center logbook-summary-text padding-top-0 align-log-text font-size-12">STARTING WEIGHT </span>
                                                       </p>
                                                       <p class="current-weight">
                                                           <span id="spCurrentWeight" class="align-center margin-bottom-0 align-log-count topic-summary-text"><s:property value="%{currentWeight}" ></s:property></span>
                                                           <span class="inline uppercase margin-bottom-0 align-center logbook-summary-text padding-top-0 align-log-text font-size-12">CURRENT WEIGHT </span>
                                                       </p>
                                                       <p class="target-weight">
                                                           <span class="align-center margin-bottom-0 align-log-count topic-summary-text"><s:property value="%{goalWeight}" ></s:property></span>
                                                           <span class="inline uppercase margin-bottom-0 align-center logbook-summary-text padding-top-0 align-log-text font-size-12">TARGET WEIGHT </span>
                                                       </p>
                                                   </s:if>
                                                   <s:else>
                                                       <p class="starting-weight">
                                                           <span class="align-center margin-bottom-0 align-log-count topic-summary-text">0</span>
                                                           <span class="inline uppercase margin-bottom-0 align-center logbook-summary-text padding-top-0 align-log-text font-size-12">STARTING WEIGHT </span>
                                                       </p>
                                                       <p class="current-weight">
                                                           <span id="spCurrentWeight" class="align-center margin-bottom-0 align-log-count topic-summary-text">0</span>
                                                           <span class="inline uppercase margin-bottom-0 align-center logbook-summary-text padding-top-0 align-log-text font-size-12">CURRENT WEIGHT </span>
                                                       </p>
                                                       <p class="target-weight">
                                                           <span class="align-center margin-bottom-0 align-log-count topic-summary-text">0</span>
                                                           <span class="inline uppercase margin-bottom-0 align-center logbook-summary-text padding-top-0 align-log-text font-size-12">TARGET WEIGHT </span>
                                                       </p>
                                                   </s:else>
                                               </span>
                                          </a>
                                          <div class="chart-loading log-weight-loading">
                                              <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                                          </div>
                                      </div>

                                      <div class="columns large-4 align-right">
                                           <a href="fetchReportPage.action?patientId=${patientId}">
                                               <span class="image-span">
                                                   <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/more-charts.png" width="56" /> MORE CHARTS
                                               </span>
                                           </a>
                                          <%--<div class="align-center">
                                              <a id="btnViewGroupFeed" href="javascript:;" class="align-center button radius tiny btn-teal margin-bottom-0"> View Group Feed </a>
                                          </div>--%>
                                      </div>
                                  </div>
                              </div>

                          </div>
                      </div>

                      <!-- Left grey area -->
                      <div id="divCoachesAreaLeft" class="columns large-3 bg-grey equal-areas" data-equalizer-watch>
                            <script>
                                var patId = '${patientId}',
                                    loggedInUserID = '${session.PROVIDER.providerId}';
                                $(document).ready(function(){
                                    $("#divCoachesAreaLeft").load("${pageContext.request.contextPath}/educator/coachesArea.jsp");
                                });
                            </script>
                      </div>

                  </div>
            </div>
          </div>
    </div>

  <!-- Goal Details Dialog -->
  <div id="goalDetailsDialog" class="reveal-modal medium confirm-dialog">
      <div class="columns small-12">
          <h3 class="meal-summary-text">GOAL DETAILS</h3>
          <p class="margin-bottom-0 grey">How important is this to me?</p>
          <div class="ratings-container">
              <ul id="importantScale" class="large-block-grid-5 small-block-grid-5 scales create-scales goal-progress-rating-dialog disabled">
                  <li class="position-relative" data-scale="1"><label class="inline align-center label-align-left scale-text not-at-all">Not at all</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>1</span></label></li>
                  <li class="position-relative" data-scale="2"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>2</span></label></li>
                  <li class="position-relative" data-scale="3"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>3</span></label></li>
                  <li class="position-relative" data-scale="4"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>4</span></label></li>
                  <li class="position-relative" data-scale="5"><label class="inline align-center label-align-left scale-text reaching-all">All the time</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>5</span></label></li>
              </ul>
          </div>

          <p class="margin-bottom-0 grey">How often do I expect to stick to my Action Plan?</p>
          <div class="ratings-container">
              <ul id="actionPlanScale" class="large-block-grid-5 small-block-grid-5 scales create-scales goal-progress-rating-dialog disabled">
                  <li class="position-relative" data-scale="1"><label class="inline align-center label-align-left scale-text not-at-all">Not at all</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>1</span></label></li>
                  <li class="position-relative" data-scale="2"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>2</span></label></li>
                  <li class="position-relative" data-scale="3"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>3</span></label></li>
                  <li class="position-relative" data-scale="4"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>4</span></label></li>
                  <li class="position-relative" data-scale="5"><label class="inline align-center label-align-left scale-text reaching-all">All the time</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>5</span></label></li>
              </ul>
          </div>

          <div class="grey-black-pair">
              <p class="margin-bottom-0 grey">Challenge</p>
              <p id="myChallenge" class="black">__</p>
          </div>

          <div class="grey-black-pair">
              <p class="margin-bottom-0 grey">Overcoming Challenge</p>
              <p id="overcomeChallenge" class="black">__</p>
          </div>

          <div class="grey-black-pair">
              <p class="margin-bottom-0 grey">Reward</p>
              <p id="myReward" class="black">__</p>
          </div>

      </div>
      <a id="closeGoalDetailsDialog" class="close-reveal-modal">&#215;</a>
  </div>

  <!-- All Goals and Habits Dialog -->
  <div id="allGoalsAndHabitsDialog" class="reveal-modal large confirm-dialog">
      <div class="columns small-12">
          <h3 class="meal-summary-text">Goals & Habits</h3>

          <div class="goals-habits-data">

              <div class="row row-auto goals-header">
                  <div class="columns large-1 padding-lr-0">
                      <label class="inline">Type</label>
                  </div>
                  <div class="columns large-5 padding-left-0">
                      <label class="inline">Goal/Habit</label>
                  </div>
                  <div class="columns large-6">
                      <label class="inline">Member Self Assessment</label>
                  </div>
              </div>

              <div class="goals-habits-scrollable">
                  <ul id="goalListDialog" class="listing large-block-grid-1">

                  </ul>

                  <ul id="habitListDialog" class="listing large-block-grid-1">

                  </ul>
              </div>
          </div>

      </div>
      <a id="closeGoalsAndHabitsDialog" class="close-reveal-modal">&#215;</a>
  </div>

  <!-- Motivation Image Dialog -->
  <div id="motivationImageDialog" class="reveal-modal medium confirm-dialog">
      <div class="columns small-12">
          <h3 class="meal-summary-text">Motivation Image</h3>
          <s:if test="%{patientMotivationImage != null}">
              <img title="Click to enlarge" src="${pageContext.request.contextPath}/motivationImages/${patientMotivationImage.imageName}" />
          </s:if>
      </div>
      <a id="closeMotivationImageDialog" class="close-reveal-modal">&#215;</a>
  </div>

  <!-- Goals Rating Dialog -->
  <div id="goalRatingDialog" class="reveal-modal small" data-reveal>
      <div class="columns small-12">
          <h3 id="h3GoalName" class="meal-summary-text">Rating History</h3>
          <div class="dialog-area">
              <ul id="dialogGoalRatingList" class="side-nav goal-ratings-history">

              </ul>
          </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- Secure Link Dialog -->
  <div id="secureLinkDialog" class="reveal-modal small" data-reveal>
      <div class="columns small-12">
          <div id="secureLinkDialogError" class="alert-box alert alert-message-abs" style="display: none;">______________________</div>
          <h3 class="meal-summary-text">Secure Link</h3>
          <div class="dialog-area">
              <textarea id="txtSecureLink" readonly value="${secureLink}" class="secure-link-text" ></textarea>
          </div>
          <div class="align-right">
              <a href="#" id="btnCopySecureLink" class="button radius small btn-teal margin-top-10 margin-bottom-0">Copy Link</a>
          </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- Edit Member Team Dialog -->
  <div id="editMemberTeamDialog" class="reveal-modal medium">

      <div id="errMessageMemberTeamDialog" class="alert-box alert alert-message-abs">______________________</div>

      <h3 class="meal-summary-text remove-bold">Edit Member's Team</h3>
      <div class="selected-providers-list">
          <div class="row row-auto">
              <div class="columns medium-6 large-6 six">
                  <label class="inline margin-bottom-0 label-dialog">Food Coaches</label>
                  <ul id="lstFoodCoaches" class="side-nav prov-ul"></ul>
              </div>
              <div class="columns medium-6 large-6 six">
                  <label class="inline margin-bottom-0 label-dialog">Coaches</label>
                  <ul id="lstCoaches" class="side-nav prov-ul"></ul>
              </div>
          </div>
          <small id="errorMemberTeam" class="error hide">Required.</small>
      </div>
      <div class="row row-auto">
          <div class="columns small-12">
              <ul class="side-nav teal-bullets-diag padding-left-0">
                  <li>
                      <label id="lblLeadCoach" class="hide">
                          <span id="spLeadCoach"></span><span class='label warning coach-designation-label lead-coach'>Lead Coach</span>
                      </label>
                  </li>
                  <li>
                      <label id="lblPrimaryCoach" class="hide">
                          <span id="spPrimaryFoodCoach"></span><span class='label warning coach-designation-label primary-coach'>Primary Food Coach</span>
                      </label>
                  </li>
              </ul>
          </div>
      </div>
      <br/>
      <div class="align-right">
          <input id="btnSaveMemberTeam" type="submit" class="button radius btn-teal min-width-250 small" value="Save" />
      </div>

      <a id="closeDialogMemberTeam" class="close-reveal-modal">&#215;</a>

      <div id="dialogLoadingSpinner" class="dialog-loading">
          <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Processing...</label>
      </div>

  </div>


    <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
    <![endif]-->
    <!--[if (gte IE 9) | (!IE)]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
    <!--<![endif]-->

    <!-- Foundation 3 for IE 8 and earlier -->
    <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
    <![endif]-->

    <!-- Foundation 4 for IE 9 and later -->
    <!--[if gt IE 8]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
    <!--<![endif]-->
   <script src="${pageContext.request.contextPath}/resources/js/date.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>

   <script src="${httpOrHttps}://code.highcharts.com/highcharts.js"></script>
   <script src="${httpOrHttps}://code.highcharts.com/modules/exporting.js"></script>
   <script src="${httpOrHttps}://code.highcharts.com/highcharts-more.js"></script>
   <script src="${httpOrHttps}://code.highcharts.com/modules/solid-gauge.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/jquery.mask.min.js"></script>
   <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
   <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/hs.edit.members.team.js?v=0.4"></script>
   <script src="${pageContext.request.contextPath}/resources/js/hs.patient.dashboard.js?v=0.11"></script>

      <script type="text/javascript">

        var GOALS_LISTING = ("${goalsList}") ? $.parseJSON("${goalsList}"): undefined,
            HABITS_LISTING = ("${habitsList}") ? $.parseJSON("${habitsList}"): undefined,
            GOAL_RATINGS = undefined,
            patientDashboard = Object.create(HSPatientDashboard),
            CURRICULUM_API_ADDRESS = '${CURRICULUM_API_ADDRESS}',
            EMAIL = '${patient.user.email}',
            patId = '${patientId}',
            loggedInUserID = '${session.PROVIDER.providerId}',
            BASE_URL = "${pageContext.request.contextPath}";

        $(function(){
		    $(document).foundation();
            $("#txtSecureLink").val('${secureLink}');
            $("#patientsTab").addClass("selected-tab");
            $("#txtDateString").val("${dashboardDateString}");

            var dashboardParams = {
                patientId: "${patientId}",
                imagesPath: "${pageContext.request.contextPath}",
                uuid: "${uuid}",
                isFromDashboard: true
            };
            patientDashboard.init(dashboardParams);

            EditMembersTeam.init(${patientId}, BASE_URL);
        });

      </script>

  </body>          
</html>