<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>

<title>Patient Detail</title>
	
	<style type="text/css">
		.ui-datepicker-trigger{
			margin: 0px;
			height: 37px;
			padding-top: 17px;
			width: 5px;
			padding-left: 1rem;
		}
	</style>
</head>
<body style="background: #fff;">
	<br/>
	
	<div style="text-align: center; cursor: default;">
		<label class="log-header">Patient Detail</label>
	</div>
	<br/>
	<s:actionmessage id="actionMessage" name="actionMessage" cssClass="alert-success notification-style"/>
	<s:actionerror id="actionError" name="actionError" cssClass="alert-error notification-style"/>
	
	<div class="breadcrumb-top-2">
		<ul class="breadcrumbs breadcrum-css">
			<li>  
		    	<a href="members.action">Patients</a>
		  	</li>   
		  	<li class="current">Patient Detail</li>  
		</ul>
	</div>
	
	<div class="container-div">
	
		<s:if test="%{patient == null}">
			<div class="tab-content current" style="text-align: center">
				No associate patient found
			</div>
		</s:if>
		<s:else>
			<div class="tab-content current" style="padding-bottom: 1px;">
				<s:form theme="simple" action="updatePatientDetail">
					<s:hidden name="patient.patientId"></s:hidden>
					<s:hidden name="patient.isDiabetic"></s:hidden>
					<s:hidden name="patient.user.userId"></s:hidden>
					<s:hidden name="patient.user.password"></s:hidden>
					<s:hidden name="patient.user.isEnabled"></s:hidden>
					<s:hidden name="patient.user.usertype"></s:hidden>
					<s:hidden name="patient.provider.providerId"></s:hidden>
					<s:hidden name="patient.cohort.cohortId"></s:hidden>
				
					<table class="section-style">
			      		<tr>
			      			<td class="first-column">First Name:</td>
			      			<td style="width:35%"><s:textfield cssClass="patient-detail-text" name="patient.user.firstName"></s:textfield> </td>
			      			<td class="second-column padding-left-45p">Last Name: </td>
			      			<td><s:textfield cssClass="patient-detail-text" name="patient.user.lastName"></s:textfield></td>
			      		</tr>
			      		<tr>
			      			<td class="first-column padding-bottom-25p">Date of Birth:</td>
			      			<td>
			      				<div class="row collapse" style="display:block">
								    <div class="small-10 columns">
								    	<s:textfield id="pat-dob" data-calId="btnPatDob" cssClass="year-style patient-detail-text date-style" name="patient.dob" readonly="true"></s:textfield>
								    </div>
								    <div class="small-2 columns">
								    	<a id="btnPatDob" href="#" class="button postfix" style="background-color: #357ebd;"><img alt="" style="padding-top: 11px;" src="${pageContext.request.contextPath}/resources/images/calendar.gif"></a>
								    </div>
							    </div>
		      				</td>
			      			<td class="second-column padding-left-45p">Gender: </td>
			      			<td>
			      				<s:radio id="gender" name="patient.gender" list="genders" value="%{patient.gender}" onchange=""/>
			      			</td>
			      		</tr>
			      		<tr>
			      			<td class="first-column">Phone:</td>
			      			<td style="width:35%"><s:textfield cssClass="patient-detail-text" name="patient.user.phone"></s:textfield> </td>
			      			<td class="second-column padding-left-45p">Email: </td>
			      			<td><s:textfield cssClass="patient-detail-text" name="patient.user.email"></s:textfield></td>
			      		</tr>
			      		
			      		<tr>
			      			<td class="first-column">MAC Address:</td>
			      			<td style="width:35%"><s:textfield cssClass="patient-detail-text" name="patient.deviceMacAddress"></s:textfield> </td>
			      			<td class="second-column padding-left-45p">MRN: </td>
			      			<td><s:textfield cssClass="patient-detail-text" name="patient.mrn"></s:textfield></td>
			      		</tr>
			      		<tr>
			      			<td class="first-column padding-bottom-25p">Registration Date:</td>
			      			<td>
			      				<div class="row collapse" style="display:block">
								    <div class="small-10 columns">
								    	<s:textfield id="pat-registration-date" data-calId="btnPatRegistrationDate" cssClass="year-style patient-detail-text date-style" name="patient.user.registrationDate" readonly="true"></s:textfield>
								    </div>
								    <div class="small-2 columns">
								    	<a id="btnPatRegistrationDate" href="#" class="button postfix" style="background-color: #357ebd;"><img alt="" style="padding-top: 11px;" src="${pageContext.request.contextPath}/resources/images/calendar.gif"></a>
								    </div>
							    </div>
			      			</td>
			      			<td class="second-column padding-left-45p padding-bottom-25p">Social Group:</td>
			      			<td><s:select list="groupMap" name="patient.user.groupId"></s:select></td>
			      		</tr>
			      	</table>
			      	<div style="text-align: center; display:block;max-width: 100%;" class="row collapse">						
				          <s:submit cssClass="btn btn-primary" value="Save"></s:submit>
				          <a href="members.action?providerId=<s:property value='providerId'/>" class="btn btn-default">Cancel</a>
				    </div>
		      	</s:form>
		      	
			</div>
		</s:else>
		<a href="${pageContext.request.contextPath}" id="appPath" style="display:none;"></a>
	</div>
	
	<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
	
	<script type="text/javascript">
	
		$(function() {
			
			$('#patientsTab').addClass('selected-tab');
			
			var appPath = $('#appPath').prop('href');
			
			initDatePicker("pat-dob");
			initDatePicker("pat-registration-date");
			
			//initDatePicker("pat-insulin-date", appPath); 
			
			setTimeout(function(){
				$("#actionMessage").slideUp();
			}, 3000);
			
			setTimeout(function(){
				$("#actionError").slideUp();
			}, 3000);
		});
		
			
	</script>
</body>
</html>