<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/logDetail.css?v=0.3" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/select2-3.5.2/select2.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/twilio_messages.css" />

    <title>Patient Log Detail</title>

    <style type="text/css">
        .coach-notes {
            color: #8AB329 !important;
        }

        .coach-name {
            color: #444 !important;
        }

        .sp-last-request-date{
            display: none;
            font-size: 14px;
            font-weight: bold;
            color: #444;
        }

        .sp-last-request-date span{
            font-weight: normal;
        }

        .stop-horizontal-scroll{
            resize: vertical;
            min-height: 100px;
        }
    </style>
</head>
<body>

<s:set var="logType" value="log.logType" />
<%-- <s:property value="%{log.isProcessed"/> --%>

<!-- BreadCrumb -->
<div class="breadcrumb-green">
    <div class="max-width-1129">
        <ul class="breadcrumbs">
            <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                <li>
                    <a href="unProcessedMeals.action">Meals</a>
                </li>
                <s:if test="%{redirectToPage == 'shareableMeals'}">
                    <li>
                        <a href="shareableMeals.action">Shareable Meals</a>
                    </li>
                </s:if>
            </s:if>
            <li>
                <a href="members.action">My Members</a>
            </li>
            <s:if test="%{isFromError == 'yes'}">
                <li>
                    <a href="viewReportErrors.action">Member Reported Error</a>
                </li>
            </s:if>
            <s:else>
                <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                    <li>
                        <a href="fetchLogs.action?patientId=<s:property value='patientId'/>">Logs</a>
                    </li>
                </s:if>
            </s:else>

            <li class="current">LOG DETAILS</li>
            <li class="current"><b>Member Name:</b> <s:property value='%{patient.user.displayName}' /></li>

            <s:if test="%{log.patient != null && log.patient.gender != null && log.patient.gender != ''}">
                <s:if test="%{log.patient != null && log.patient.age != null && log.patient.age != ''}">
                    <li class="current"><s:property value='%{log.patient.gender}' /> (<s:property value='%{log.patient.age}' /> years)</li>
                </s:if>
                <s:else>
                    <li class="current"><s:property value='%{log.patient.gender}' /></li>
                </s:else>
            </s:if>

            <s:if test="%{log.isRemoved == 0}">
                <li class="current"><b>Log Time:</b> <s:property value='@com.healthslate.patientapp.util.CommonUtils@timeInUserTimezone(log.getLogTime(), log.getLogTimeOffset(), log.getOffsetKey())' /></li>
                <s:if test="%{#logType == 'Meal'}">
                    <li class="current"><b>Log Type:</b> <s:property value='%{log.foodLogSummary.type}'/> </li>
                </s:if>
                <s:else>
                    <li class="current"><b>Log Type:</b> <s:property value='%{#logType}'/></li>
                </s:else>
            </s:if>
        </ul>
    </div>
</div>

<div id="confirmationAlert" class="reveal-modal small" data-reveal>
    <h3 class="meal-summary-text remove-bold">Delete food</h3>
    <p class="lead dialog-para">Are you sure you want to delete this food?</p>
    <a id="closeConfirmDialog" class="close-reveal-modal">&#215;</a>
    <div class="align-right">
        <button id="btnConfirmDelete" type="button" class="button radius btn-teal margin-top-10 margin-bottom-0">Yes</button>
        <button type="button" class="button secondary radius margin-top-10 margin-bottom-0" onclick="$('#closeConfirmDialog').click(); return false;">No</button>
    </div>
</div>

<div id="infoError" class=" alert error alert-new-message"></div>
<div id="loading" class="loader">
    <div class="image-loader">
        <p class="progress-please-wait">Please wait...</p>
        <div class="progress progress-striped active">
            <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            </div>
        </div>
        <!-- <p id="progressTxt" class="progress-text">We're uploading your content!!!</p> -->
    </div>
</div>
<div id="divMemberNavArea"></div>
<div class="dashboard-container margin-bottom-20 margin-top-0">

    <div class="row max-width-1129">

        <div class="coach-area-expand">
            <a href="#" id="btnExpColCoachArea"><img src="${pageContext.request.contextPath}/resources/css/images/arrow-right.png" title="Show" /></a>
        </div>

        <div class="columns medium-12">

            <!-- Charts, goals and Coach Notes area -->
            <div class="row bg-white row-auto" data-equalizer>
                <!-- Right white area -->
                <div class="columns medium-12 padding-lr-0 summary-area equal-areas" data-equalizer-watch>

                  <div id="main-panel">

    <div id="waiting-div" style="text-align: center; display: none;">
        <img alt="" src="${pageContext.request.contextPath}/resources/images/indicator.gif">
        <br/>
    </div>
    <s:actionmessage id="actionMessage" cssClass="alert success common-class"/>
    <div class="max-width-1129">
        <s:if test="%{isFromError == 'yes'}">

        </s:if>
        <s:else>
            <s:if test="%{#logType == 'Meal' }">
                <div class="row row-auto padding-top-10">
                    <div class="columns large-6 padding-left-7">
                        <div class="patHeadDiv common-class">
                            <label class="heading-blue">Meal Name
                                <s:textfield id="logNameFromFood" name="logNameFromFood" maxlength="100" required="required" size="20" cssClass="capital log-name-food meal-name-field"  placeholder="Meal name"></s:textfield>
                            </label>
                        </div >
                    </div>
                    <div class="columns large-6 align-right padding-right-8">
                        <s:if test="%{isRoleAdmin == false && log.logType == 'Meal' && isFromError == 'no' && log.isProcessed == true}">
                            <button onclick="saveAsSuggestedLog(this);" class="button success radius left small margin-bottom-0 btn-green margin-right-10">Create Suggested Meal</button>
                        </s:if>
                        <s:if test="%{redirectToPage == 'fetchlogbook' && log.logType == 'Meal' && isRoleAdmin == false}">
                            <s:if test="%{log.isProcessed == 1}">
                                <button onclick="saveAsSuggestedLog(this);" class="button success small left radius margin-bottom-0 btn-green margin-right-10">Create Suggested Meal</button>
                            </s:if>
                        </s:if>

                        <a href="myMealsPage.action?patientId=<s:property value="%{patientId}"/>&mealType=<s:property value="%{log.foodLogSummary.type}"/>" class="button radius small btn-teal right margin-bottom-0"> Show Meals</a>

                        <a id="btnMemberFoodRestrictions" href="#" class="button radius small btn-grey right margin-bottom-0 margin-right-10"> Member's Food Restrictions</a>

                    </div>
                </div>
            </s:if>
        </s:else>

        <!-- Member's Food Restrictions Dialog -->
        <div id="memberEatingPreferenceDialog" class="reveal-modal medium">
            <div class="columns small-12">
                <h3 class="meal-summary-text remove-bold">Member's Food Restrictions</h3>
                <div class="dialog-area">

                    <s:if test="%{patientEatingPreference == null || patientEatingPreference == ''}">
                        <p class="align-center">No food restriction defined</p>
                    </s:if>
                    <s:else>

                        <div id="divColdBeverage">
                            <p class="coach-notes margin-bottom-0">
                                Restrictions and Preferences
                            </p>
                            <p class="coach-name remove-uppercase">
                                <s:property value="%{patientEatingPreference.restriction}"></s:property>
                            </p>
                        </div>

                        <s:if test="%{patientEatingPreference.isColdBeverage == true}">
                            <div id="divColdBeverage">
                                <p class="coach-notes margin-bottom-0 margin-top-10">
                                    Soda pop/Cola, Iced Tea, Energy Drinks
                                </p>
                                <p id="pColdBeverage" class="coach-name remove-uppercase">
                                   <s:property value="%{patientEatingPreference.coldBeverageType}"></s:property>
                                </p>
                            </div>
                        </s:if>
                        <s:if test="%{patientEatingPreference.isHotBeverage == true}">
                            <div id="divHotBeverage">
                                <p class="coach-notes margin-bottom-0 margin-top-10">
                                    Coffee or Tea with sugar (non-diet) sweetener(s) or flavoring(s):
                                </p>
                                <p class="coach-notes margin-bottom-0 remove-uppercase">
                                    How many teaspoons of sugar sweetener or flavorings (not Splenda, Equal, diet flavorings, etc.) teaspoons added
                                </p>
                                <p id="pTeaspoon" class="coach-name remove-uppercase">
                                    <s:property value="%{patientEatingPreference.hotBeverageTeaspoonAdded}"></s:property>
                                </p>

                                <p class="coach-notes margin-bottom-0 margin-top-10">
                                    Size
                                </p>
                                <s:if test="%{patientEatingPreference.hotBeverageSize != ''}">
                                    <p id="pSize" class="coach-name remove-uppercase">
                                        <s:property value="%{patientEatingPreference.hotBeverageSize}"></s:property>
                                    </p>
                                </s:if>
                                <s:else>
                                    <p id="pSize" class="coach-name remove-uppercase">
                                        N/A
                                    </p>
                                </s:else>
                            </div>
                        </s:if>
                        <s:if test="%{patientEatingPreference.isAlcohol == true}">
                            <div id="divAlcohol">
                                <p class="coach-notes margin-bottom-0 margin-top-10">
                                    Hard alcohol/Spirits
                                </p>
                                <p id="pAlcoholType" class="coach-name remove-uppercase">
                                    <s:property value="%{patientEatingPreference.alcoholType}"></s:property>
                                </p>
                            </div>
                        </s:if>
                    </s:else>
                </div>
            </div>
            <a class="close-reveal-modal">&#215;</a>
        </div>

        <s:hidden id="isRemoved" value="%{log.isRemoved}"></s:hidden>
        <s:if test="%{log.isRemoved == 0}">
            <div>
                <div class="tab-content current custom-tc">
                    <div class="switch centered common-class">
                        <ul id="switch" data-content="switch-elements" class="button-group radius hide centered">
                            <li><a id="food-details" href="#" data-attached-elem="foodDetails" class="button">Food</a></li>
                            <li><a id="other-details" href="#" data-attached-elem="otherDetails" class="button secondary">Other</a></li>
                        </ul>
                    </div>
                    <div class="switch-container">

                        <label id="aMsg" class="alert success alert-new-message"></label>
                        <label id="logSuccess" class="alert success alert-new-message"></label>
                        <label id="logError" class="alert error alert-new-message"></label>
                        <label id="infoSuccess" class=" alert success alert-new-message"></label>
                        <label id="infoError" class="alert error alert-new-message"></label>

                        <s:if test="%{errorObjsForLogId != null && errorObjsForLogId.size() > 0}">
                            <div class="patHeadDiv common-class">
                                <label class="heading-red">Member Reported Error</label>
                                <display:table name="errorObjsForLogId" requestURI="" class="table datatable"                                            uid="row" style="width: 100%;">
                                    <display:setProperty name="basic.empty.showtable" value="true" />
                                    <display:column title="Description" property="[0]" />
                                    <display:column title="Reported Date" style="width:25%">
                                        <s:property
                                                value="@com.healthslate.patientapp.util.DateUtils@getFormattedDate(#attr.row[1], 'MM/dd/yyyy hh:mm a')" />
                                    </display:column>
                                </display:table>
                            </div>
                            <br/>
                        </s:if>

                        <div id="foodDetails" >
                            <s:if test="%{#logType=='Meal'}">
                                <s:actionerror id="actionError" name="actionMessage" cssClass="alert error alert-enhancment"/>
                                <form id="msgForm" action="sendMessage.action" class="hide">
                                    <s:hidden id="message" name="message"></s:hidden>
                                    <s:hidden id="logId" name="logId"></s:hidden>
                                </form>
                                <div class="row row-auto">

                                    <div id="foodModal" class="hide-dialog">

                                        <s:if test="%{log.foodLogSummary == null ||
							      					  log.foodLogSummary.hasImage == null ||
							      					  log.foodLogSummary.hasImage == false || 
							      					  encodedFoodImage == null}"></s:if>
                                        <s:else>
                                            <div class="linker link-top modal-con"></div>
                                            <div class="linker link-bottom modal-con"></div>
                                        </s:else>

                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button id="btnCloseFoodDialogTop" type="button" class="close">&times;</button>
                                                    <h4 class="modal-title">Add Food</h4>
                                                </div>
                                                <div class="modal-body padding-bottom-0">
                                                    <div class="modal-con">
                                                        <ul class="button-group modal-con">
                                                            <li><label class="item-style inline modal-con">	Food:</label></li>
                                                            <li class="margin-left-5p"><s:textfield id="newFoodName" cssClass="modal-con food-name-style width-107p" maxlength="50" required="required"></s:textfield></li>
                                                        </ul>
                                                    </div>
                                                    <div class="modal-con">
                                                        <ul class="button-group modal-con">
                                                            <li><label class="item-style modal-con margin-top-9p">Serving: </label></li>
                                                            <li>
                                                                <select class="modal-con inline-block width-65p margin-left-5p margin-right-5p" id="servingSize">
                                                                    <option value="0.25">1/4</option>
                                                                    <option value="0.3">1/3</option>
                                                                    <option value="0.5">1/2</option>
                                                                   	<option value="0.6">2/3</option> 
                                                                    <option value="0.75">3/4</option>
                                                                    <option value="1">1</option>
                                                                    <option value="1.25">1.25</option>
                                                                    <option value="1.5">1.5</option>
                                                                    <option value="1.75">1.75</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                </select>
                                                            </li>
                                                            <li><s:select cssClass="modal-con width-145p" id="servingUnit" list="servingUnitMap" name="servingUnit"></s:select></li>
                                                        </ul>
                                                    </div>
                                                    <div class="modal-con">


                                                        <div class="modal-con inline-block margin-left-20">
                                                            <label class="item-style modal-con">Fats: </label>
                                                        </div>
                                                        <div class="modal-con inline-block">
                                                            <input type="number" id="fats" min="0" max="999" step="0.1" class="modal-con width-65p fat-style" onkeydown="return isNumber(event)">
                                                        </div>
                                                        <div class="modal-con inline-block margin-left-33">
                                                            <label class="item-style modal-con">Carbs: </label>
                                                        </div>
                                                        <div class="modal-con inline-block">
                                                            <input type="number" id="carbs" min="0" max="999" step="0.1" class="modal-con width-65p carbs-style" onkeydown="return isNumber(event)">
                                                        </div>

                                                        <div class="modal-con inline-block">
                                                            <label class="item-style modal-con">Protein: </label>
                                                        </div>
                                                        <div class="modal-con inline-block">
                                                            <input type="number" id="protein" min="0" max="999" step="0.1" class="modal-con width-65p protein-style" onkeydown="return isNumber(event)">
                                                        </div>
                                                    </div>
                                                    <div class="modal-con total_sum-style">
                                                        <div class="inline-block modal-con">
                                                            <label class="item-style modal-con">Total: </label>
                                                        </div>


                                                        <div class="inline-block margin-10-width-135per modal-con">
                                                            <s:label id="totalFoodFats" cssClass="modal-con total-sum-style"></s:label>
                                                        </div>
                                                        <div class="inline-block width-18 margin-52p-left modal-con">
                                                            <s:label id="totalFoodCarbs" cssClass="modal-con total-sum-style"></s:label>
                                                        </div>
                                                        <div class="inline-block width-14per margin-left-75 modal-con">
                                                            <s:label id="totalFoodProtein" cssClass="modal-con total-sum-style"></s:label>
                                                        </div>
                                                    </div>
                                                    <div class="modal-con align-center new-food-success"></div>
                                                    <div class="modal-con align-center new-food-error"></div>
                                                </div>
                                                <div class="modal-footer footer-style">
                                                    <button id="btnSaveAndAddMore" type="button" class="modal-butt button radius margin-bottom-0 saveFoodBtn btn-teal">Save and Add More Food</button>
                                                    <button id="btnSaveFood" type="button" class="modal-butt button radius margin-bottom-0 saveFoodBtn btn-teal">Save</button>
                                                    <button id="btnCloseFoodDialog" type="button" class="button radius secondary margin-bottom-0">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="EditNameDiv" class="modal fade bs-example-modal-sm"
                                         tabindex="-1" role="dialog"
                                         aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-sm">

                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                        <h4 class="modal-title">Edit Log Name</h4>
                                                    </div>
                                                    <div class="modal-body padding-bottom-0">
                                                        <div>
                                                            <ul class="button-group modal-con">
                                                                <li><label class="item-style inline modal-con">
                                                                    Log Name:</label></li>
                                                                <li class="margin-left-5p"><s:textfield
                                                                        id="newLogName"

                                                                        maxlength="50" required="required" ></s:textfield></li>
                                                            </ul>
                                                        </div>

                                                        <div class="modal-con align-center log-error"></div>

                                                    </div>
                                                    <div class="modal-footer footer-style">


                                                        <button type="button"
                                                                class="modal-butt button radius margin-bottom-0 editFoodBtn"
                                                                onclick="editName(document.getElementById('newLogName').value)">Save</button>
                                                        <button class="modal-butt button radius margin-bottom-0 editFoodBtn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="columns medium-6 padding-left-0 common-class">
                                        <s:if test="%{mealCeilings.get(log.foodLogSummary.type) <= 0}">
                                            <div class="patHeadDiv common-class"><label class="heading-blue">Food Detail <span class="remove-bold label warning font-20"><span class="font-bold"> No Ceiling set </span> </span></label></div>
                                        </s:if>
                                        <s:else>
                                            <div class="patHeadDiv common-class"><label class="heading-blue">Food Detail <span class="remove-bold label warning font-20">Carb ceiling: <span class="font-bold"><s:property value='%{mealCeilings.get(log.foodLogSummary.type)}'/></span>g </span></label></div>
                                        </s:else>

                                        <br/>
                                        <label class="heading-style">Nutrition Facts

                                            <span class="right">
                                                <s:if test="%{log.foodLogSummary.hasMissingFood == true}">
                                                    <input type="checkbox" id="missingFood" checked="checked"/>
                                                </s:if>
                                                <s:else>
                                                    <input type="checkbox" id="missingFood" />
                                                </s:else>
                                                <span>Missing Food</span>
                                            </span>
                                        </label>
                                        <div id="nutriDetailDiv" class="section-style div-1-main">

                                            <form id="nutritionInfoForm" action="sendFoodNutritionInfo.action" class="hide">
                                                <s:hidden id="logId" name="logId"></s:hidden>
                                                <s:hidden id="patientId" name="patientId"></s:hidden>
                                                <s:hidden id="newNote" name="note"></s:hidden>
                                                <s:hidden id="maxCount" name="maxCount"></s:hidden>
                                                <s:hidden id="isAdvSearch" name="isAdvSearch"></s:hidden>
                                                <s:hidden id="isFromError" name="isFromError"></s:hidden>
                                                <s:hidden id="redirectToPage" name="redirectToPage"></s:hidden>
                                                <s:hidden id="hasMissingFood" name="hasMissingFood"></s:hidden>
                                                <s:hidden id="foodLogName" name="foodLogName"></s:hidden>
                                                <s:hidden id="isFromLogbook" name="isFromLogbook"></s:hidden>
                                                <s:hidden id="logMealName" name="logNameFromFood"></s:hidden>
                                                <s:hidden id="txtHiddenMealName" name="logName"></s:hidden>
                                            </form>

                                            <div id="nutritionMessage" class="success-msg-style"></div>
                                            <s:actionerror id="nutritionError" name="actionMessage" cssClass="alert error alert-enhancment"/>

                                            <s:form id="mealFoodForm" theme="simple" action="addMealFood.action" cssClass="hide">
                                                <s:hidden id="logId" name="logId"></s:hidden>
                                                <s:hidden id="foodId" name="foodId"></s:hidden>
                                                <s:hidden id="foodName" name="foodName"></s:hidden>
                                                <s:hidden id="foodCarbs" name="foodCarbs"></s:hidden>
                                                <s:hidden id="foodFats" name="foodFats"></s:hidden>
                                                <s:hidden id="foodProtein" name="foodProtein"></s:hidden>
                                                <s:hidden id="foodServingSize" name="servingSize"></s:hidden>
                                                <s:hidden id="foodServingUnit" name="servingUnit"></s:hidden>
                                                <s:hidden id="logMealName" name="logNameFromFood"></s:hidden>
                                            </s:form>
                                            <s:form id="delFoodForm" theme="simple" action="deleteMealFood.action" cssClass="hide">
                                                <s:hidden id="logId" name="logId"></s:hidden>
                                                <s:hidden id="delMealId" name="mealId"></s:hidden>
                                                <s:hidden id="delFoodId" name="foodId"></s:hidden>
                                                <s:hidden id="logMealNameDel" name="logNameFromFood"></s:hidden>
                                            </s:form>

                                            <div style="background: #E6E6E6;">
                                                <table style="width: 100%;margin-bottom: 0px;border: 0px !important;">
                                                    <tr style="background: #E6E6E6;">
                                                        <td class="first-col"><label style="font-weight: bold; cursor: default;">Food</label></td>
                                                        <td class="second-col"><label style="font-weight: bold; cursor: default;">Serving</label></td>
                                                        <td class="second-col"><label style="font-weight: bold; cursor: default;">Unit</label></td>
                                                        <td class="third-col"><label style="font-weight: bold; cursor: default;">Fat</label></td>
                                                        <td class="forth-col"><label style="font-weight: bold; cursor: default;">Carbs</label></td>
                                                        <td class="fifth-col"><label style="font-weight: bold; cursor: default;">Protein</label></td>
                                                        <td class="sixth-col foodAction">
                                                                <%-- <s:if test="%{log.isProcessed == null || log.isProcessed == 0}"> --%>
                                                            <label style="font-weight: bold; cursor: default;">Action</label>
                                                                <%-- </s:if> --%>
                                                        </td>
                                                    </tr>
                                                    <tr class="extra-tr">
                                                        <td class="first-col"></td>
                                                        <td class="second-col"></td>
                                                        <td colspan="4" class="third-col">
                                                            <label class="per-serving">(per serving)</label>
                                                        </td>
                                                        <td class="sixth-col foodAction">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <div class="div-1-food-detail">
                                                <s:if test="%{log.foodLogSummary == null ||
									      						log.foodLogSummary.foodLogDetails.size() == 0}">

                                                    <div class="nutriFoodSpan">No Food Added</div>
                                                    <div class="foodDiv"><table id="nutTable" style="width: 100%;margin-bottom: 0px;border: 0px !important;"></table></div>
                                                </s:if>
                                                <s:else>
                                                    <div class="nutriFoodSpan" style="display: none;">No Food Added</div>
                                                    <div class="foodDiv">
                                                        <table id="nutTable" style="width: 100%;margin-bottom: 0px;border: 0px !important;">

                                                            <s:iterator value="%{log.foodLogSummary.foodLogDetails}" var="foodLogDetail">
                                                                <!-- Store here -->
                                                                <tr class="nutTableRow<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>" data-trid="<s:property value='#foodLogDetail.foodMaster.foodMasterId'/>">
                                                                    <td class="first-col capital newFoodName<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>">
                                                                        <s:property value="#foodLogDetail.foodMaster.foodName"/>
                                                                    </td>
                                                                    <td class="second-col foodServing<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>">
                                                                        <s:property value="@com.healthslate.patientapp.util.CommonUtils@getCalculatedSize(#foodLogDetail.numberOfServings,'')" />
                                                                    </td>
                                                                    <td class="second-col foodServing<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>">
                                                                        <s:property value="#foodLogDetail.foodMaster.servingSizeUnit"/>
                                                                    </td>

                                                                    <td class="third-col foodFats<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>">
                                                                        <s:property value="@com.healthslate.patientapp.util.CommonUtils@getCalculatedSize(#foodLogDetail.foodMaster.fats,'')" />
                                                                    </td>
                                                                    <td class="forth-col foodCarbs<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>">
                                                                        <s:property value="@com.healthslate.patientapp.util.CommonUtils@getCalculatedSize(#foodLogDetail.foodMaster.carbs,'')" />
                                                                    </td>
                                                                    <td class="fifth-col foodProtein<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>">
                                                                        <s:property value="@com.healthslate.patientapp.util.CommonUtils@getCalculatedSize(#foodLogDetail.foodMaster.protein,'')" />
                                                                    </td>


                                                                    <td class="foodAction sixth-col">
                                                                        <s:if test="%{isFromError == 'yes'}">
                                                                            <div id="foodActionInError">
                                                                                <img
                                                                                        data-id="<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>"
                                                                                        alt="Edit Food" title="Edit Food" class="nav-icons edit-icon"
                                                                                        src="${pageContext.request.contextPath}/resources/images/Edit.png">
                                                                                <img
                                                                                        data-id="<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>"
                                                                                        alt="Delete Food" title="Delete Food" class="nav-icons del-icon"
                                                                                        src="${pageContext.request.contextPath}/resources/images/delete.png">
                                                                            </div>
                                                                        </s:if>
                                                                            <%-- <s:if test="%{log.isProcessed == null || log.isProcessed == 0}"> --%>
                                                                        <div>
                                                                            <img
                                                                                    data-id="<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>"
                                                                                    alt="Edit Food" title="Edit Food" class="nav-icons edit-icon"
                                                                                    src="${pageContext.request.contextPath}/resources/images/Edit.png">
                                                                            <img
                                                                                    data-id="<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>"
                                                                                    alt="Delete Food" title="Delete Food" class="nav-icons del-icon"
                                                                                    src="${pageContext.request.contextPath}/resources/images/delete.png">
                                                                        </div>
                                                                            <%-- </s:if> --%>
                                                                    </td>
                                                                </tr>

                                                            </s:iterator>
                                                        </table>
                                                    </div>
                                                </s:else>
                                            </div>
                                            <div class="total-div-style">
                                                <table class="total-table-style">
                                                    <tr class="total-tr-style">
                                                        <td class="first-col"><span class="total-td-style" style="font-weight: bold">Total (grams)</span></td>
                                                        <td class="total-td-style second-col"></td>
                                                        <td class="total-td-style second-col"></td>

                                                        <td class="total-td-style third-col"><label class="total-td-style" id="totalFats" class="bold-style"></label></td>
                                                        <td class="total-td-style forth-col"><label class="total-td-style" id="totalCarbs" class="bold-style"></label></td>
                                                        <td class="total-td-style fifth-col"><label class="total-td-style" id="totalProtein" class="bold-style"></label></td>
                                                        <td class="total-td-style sixth-col"><button id="btnAddFood" class="modal-show button success small radius btn-teal margin-bottom-0 expand">Add Food</button></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="background: #E6E6E6;">
                                                <table style="width: 100%;margin-bottom: 0px;border: 0px !important;">
                                                    <tr style="background: #E6E6E6;">
                                                        <td class="first-col"><span style="font-weight: bold;">Calories (%)</span></td>
                                                        <td class="second-col"></td>
                                                        <td class="second-col"></td>
                                                        <td class="third-col"><label id="percentageOfCalsInFats" ></label></td>
                                                        <td class="forth-col"><label id="percentageOfCalsInCarbs" ></label></td>
                                                        <td class="fifth-col"><label id="percentageOfCalsInProtein" ></label></td>
                                                        <td class="sixth-col">
                                                            <label class="inline-block" id="totalSum"></label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="total-cal-style">
                                                <label>(Total Cal)</label>
                                            </div>
                                        </div>
                                        <h5 class="show-hide-chat"><a href="#" id="btnHideShowMessagesView"><span>Member Messaging</span> <img src="${pageContext.request.contextPath}/resources/css/images/arrow-down.png" class="bg-green-arrow" title="Member Messaging" /> </a></h5>
                                    </div>
                                    <div class="columns medium-6 div-2 padding-right-0">
                                        <s:if test="%{log.foodLogSummary == null ||
							      					  log.foodLogSummary.hasImage == null ||
							      					  log.foodLogSummary.hasImage == false || 
							      					  foodImageLocation == null}">

                                            <div class="margin-bottom-11">
                                                <label class="heading-style common-class">Member Food Notes</label>
                                                <div class="section-style common-class" style="padding: 10px; cursor: default;">
                                                    <span>
                                                        <s:if test="%{log.foodLogSummary == null || log.foodLogSummary.notes == null}">Not Available</s:if>
                                                        <s:else><s:property value="%{log.foodLogSummary.notes}"/></s:else>
                                                    </span>
                                                </div>
                                            </div>

                                            <s:if test="%{log.foodLogSummary != null &&
							      					  foodAudioLocations != null}">

                                                <label class="heading-style margin-top-5per">Food Audio</label>
                                                <s:iterator value="%{foodAudioLocations}" var="foodAudioLocation">

                                                    <div class="height-7per-position-relative">

                                                        <div class="img-container">
                                                            <audio controls style="width: 100%;">
                                                                <source src="<s:property value='%{foodAudioLocation}'/>" type="audio/mpeg">
                                                            </audio>
                                                        </div>
                                                    </div>
                                                </s:iterator>
                                            </s:if>
                                        </s:if>
                                        <s:else>
                                            <label class="heading-style modal-con">Food Image</label>
                                            <div class="responsive-container modal-con">
                                                <div class="dummy modal-con"></div>

                                                <div class="img-container modal-con">
                                                        <%-- <img id="foodImage" src="data:image/<s:property value='%{encodedFoodImage}'/>" title="<s:property value='%{log.foodLogSummary.foodImage.imageName}'/>" class="images width-100-height-100 modal-con"/> --%>
                                                    <img style="height:470px; cursor: pointer;" onclick="showFullSizedFoodImage()" id="foodImage" src="<s:property value='%{foodImageLocation}'/>" title="<s:property value='%{log.foodLogSummary.foodImage.imageName}'/>" class="images width-100-height-100 modal-con"/>

                                                </div>
                                            </div>
                                            <div id="foodImageDialog" class="reveal-modal large">
                                                <h3 class="dialog-header meal-summary-text"> Full size image </h3>
                                                <img id="foodImageDialogImage" src="">
                                                <a class="close-reveal-modal">&#215;</a>
                                            </div>
                                        </s:else>
                                    </div>
                                </div>


                                <!-- 						      	<br/><br/> -->
                                <s:actionerror id="messageError" name="actionMessage" cssClass="alert error align-center"/>


                                <div id="modalFavoriteQuestions" class="reveal-modal medium" data-reveal>
                                    <h3>Frequently asked questions</h3>
                                    <div class="dialog-area">
                                        <ul class="freq-questions">
                                            <s:iterator value="favoriteQuestions">
                                                <li><a href="#" class="selected-question"><s:property/></a><br /></li>
                                            </s:iterator>
                                        </ul>
                                    </div>
                                    <a id="closeQuestionDialog" class="close-reveal-modal">&#215;</a>
                                    <div class="align-right">
                                        <a href="#" onclick="$('#closeQuestionDialog').click(); return false;" class="button secondary radius margin-top-10 margin-bottom-0">Close</a>
                                    </div>
                                </div>
                                <div class="common-class">
                                    <div class="columns medium-6 padding-left-0">
                                        <div id="divMessagesView" style="display:none">
                                            <s:if test="%{favoriteButton == true}">
                                                <div class="align-right">
                                                    <input id="btnOpenQuestionsDialog" type="button" value="Frequently asked questions" class="button radius new-msg-btn btn-teal margin-bottom-10"/>
                                                </div>
                                            </s:if>
                                            <label class="heading-style">
                                                <span class="display-inline-block ">Question</span>

                                                <select id="select2" style="width:400px" class="Question display-inline-block  margin-bottom-0p"   data-placeholder="Select Question">
                                                    <option></option>



                                                </select>
                                                <span class="display-inline-block reload-span">
                                                    <s:if test="%{isRoleAdmin == false}">
                                                        <img id="btnInAppRefreshMessages" class="reload" alt="Refresh the conversation" title="Refresh the conversation" src="${pageContext.request.contextPath}/resources/images/refresh.png" />

                                                    </s:if>
                                                </span>
                                            </label>
                                            <div class="section-style common-section padding-lr-0">
                                                <div id="errMessageCoach" class="alert-box alert alert-message-abs">______________________</div>
                                                <div style="height: 370PX;">
                                                    <div id="coachThreadView" class="dialog-area bg-lightgrey twilio-dialog">
                                                        <div class="twilio-messages-threaded-view">
                                                            <div class="thread-empty coach-thread-empty">No Messages found.</div>
                                                            <ul id="coachThreadViewList" class="large-block-grid-1 threaded-list threaded-list-log">

                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <br/><br/>
                                                    <s:if test="%{isFromError == 'yes'}">
                                                        <div class="new-msg-panel hide row row-auto">
                                                            <div class="large-12 columns padding-lr-0">
                                                                <div class="row collapse" style="display:block">
                                                                    <div class="small-10 columns">
                                                                        <textarea id="txtCoachMessage" rows="7" cols="" placeholder="Ask Question" maxlength="1500" class="new-msg-box"></textarea>
                                                                        <span class="max-characters max-characters-bottom-12">1500 characters left</span>
                                                                    </div>
                                                                    <div class="small-2 columns">
                                                                        <a href="#" id="btnSendCoachMessage" class="button radius expand new-msg-btn postfix line-height-49">Send</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </s:if>
                                                    <s:if test="%{isRoleAdmin == false}">
                                                        <div class="new-msg-panel row row-auto">
                                                            <div class="large-12 columns padding-lr-0">
                                                                <div class="row collapse" style="display:block">
                                                                    <div class="small-10 columns">
                                                                        <textarea id="txtCoachMessage" rows="7" cols="" placeholder="Ask Question" maxlength="1500" class="new-msg-box"></textarea>
                                                                        <span class="max-characters max-characters-bottom-12">1500 characters left</span>
                                                                    </div>
                                                                    <div class="small-2 columns">
                                                                        <a href="#" id="btnSendCoachMessage" class="button radius expand new-msg-btn postfix btn-teal line-height-49">Send</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </s:if>
                                                </div>
                                            </div>

                                        </div>

                                        <s:if test="%{log.isProcessed == true}">
                                            <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                                                <fieldset class="share-meal-container">
                                                    <div id="errMessagePage" class="alert-box alert alert-message-abs">______________________</div>
                                                    <legend>Meal Sharing</legend>
                                                    <s:if test="%{memberPermission == 'DONT_SHARE'}">
                                                        <p class="text-red">Member has denied permission of sharing this meal</p>
                                                    </s:if>
                                                    <s:elseif test="%{memberPermission == 'SHARE' || memberPermission == 'SHARE_ANONYMOUSLY'}">
                                                        <button id="btnMealShared" class="button radius margin-bottom-0 btn-green display-inline-block small">Share this Meal with Group</button>
                                                        <span class="sp-last-request-date">Last Meal Shared Date: <span id="spLastMealSharedDate"></span></span>
                                                    </s:elseif>
                                                    <s:else>
                                                        <button onclick="askMemberPermission()" class="button radius margin-bottom-0 btn-grey display-inline-block small">Request Member Permission</button>
                                                        <span class="sp-last-request-date">Last Request Date: <span id="spLastRequestDate"></span></span>
                                                    </s:else>
                                                </fieldset>
                                            </s:if>
                                        </s:if>
                                    </div>
                                    <div class="columns medium-6 padding-right-0">

                                        <s:if test="%{log.foodLogSummary == null ||
							      					  log.foodLogSummary.hasImage == null ||
							      					  log.foodLogSummary.hasImage == false || 
							      					  foodImageLocation == null}"></s:if>
                                        <s:else>

                                            <s:if test="%{log.foodLogSummary != null && log.foodLogSummary.notes != null && log.foodLogSummary.notes != ''}">
                                                <div class="margin-bottom-11">
                                                    <label class="heading-style">Member Food Notes</label>
                                                    <div class="section-style" style="padding: 10px; cursor: default;">
                                                        <span>
                                                            <s:property value="%{log.foodLogSummary.notes}"/>
                                                        </span>
                                                    </div>
                                                </div>
                                            </s:if>

                                            <s:if test="%{log.foodLogSummary != null &&
							      					  foodAudioLocations != null}">

                                                <label class="heading-style margin-top-5per">Food Audio</label>
                                                <s:iterator value="%{foodAudioLocations}" var="foodAudioLocation">

                                                    <div class="height-7per-position-relative">
                                                        <div class="dummy"></div>

                                                        <div class="img-container">
                                                            <audio controls style="width: 100%;">
                                                                <source src="<s:property value='%{foodAudioLocation}'/>" type="audio/mpeg">

                                                            </audio>
                                                        </div>
                                                    </div>
                                                </s:iterator>
                                            </s:if>
                                        </s:else>
                                    </div>
                                </div>
                                <br/><br/>
                            </s:if>

                            <s:else>
                                <div class="patHeadDiv"><label class="heading-blue">Detail</label></div>
                                <table class="section-style">

                                    <!-- Log type : Glucose -->
                                    <s:if test="%{#logType == 'Glucose'}">
                                        <tr>
                                            <td class="first-column">Glucose:</td>
                                            <td style="width:35%">
                                                <s:if test="%{log.glucoseLog == null}">N/A</s:if>
                                                <s:else>
                                                    <s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.glucoseLog.glucoseLevel,'mg/dl')" />
                                                </s:else>
                                            </td>
                                            <td class="second-column white-space-nowrap">After Meal: </td>
                                            <td>
                                                <s:if test="%{log.glucoseLog != null && log.glucoseLog.glucoseTime == 'before meal'}">
                                                    No
                                                </s:if>
                                                <s:else>
                                                    <s:property value="%{log.glucoseLog.mealName}" />
                                                </s:else>

                                            </td>
                                        </tr>
                                    </s:if>

                                    <!-- Log type : Activity -->
                                    <s:if test="%{#logType == 'Activity'}">
                                        <tr>
                                            <td class="first-column">Steps per day:</td>
                                            <td style="width:35%">
                                                <s:if test="%{log.miscLog == null}">N/A</s:if>
                                                <s:else>
                                                    <s:if test="%{log.miscLog.stepsPerDay == null}">N/A</s:if>
                                                    <s:else>
                                                        <s:property value="%{log.miscLog.stepsPerDay}" />
                                                    </s:else>
                                                </s:else>
                                            </td>
                                            <td class="second-column">Intensity:</td>
                                            <td>
                                                <s:if test="%{log.miscLog == null}">N/A</s:if>
                                                <s:else>
                                                    <s:if test="%{log.miscLog.intensity == null}">N/A</s:if>
                                                    <s:else>
                                                        <s:property value="%{log.miscLog.intensity}" />
                                                    </s:else>
                                                </s:else>
                                            </td>
                                        </tr>
                                    </s:if>

                                    <!-- Log type : Weight -->
                                    <s:if test="%{#logType == 'Weight'}">
                                        <tr>
                                            <td class="first-column">Weight:</td>
                                            <td>
                                                <s:if test="%{log.miscLog == null}">N/A</s:if>
                                                <s:else>
                                                    <s:if test="%{log.miscLog.weight == null}">N/A</s:if>
                                                    <s:else>
                                                        <s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.miscLog.weight,'lbs')" />
                                                    </s:else>
                                                </s:else>
                                            </td>
                                        </tr>
                                    </s:if>

                                    <!-- Log type : Blood_Pressure -->
                                    <s:if test="%{#logType == 'Blood_Pressure'}">
                                        <tr>
                                            <td class="first-column">Blood Pressure:</td>
                                            <td>
                                                <s:if test="%{log.miscLog == null}">N/A</s:if>
                                                <s:else>
                                                    <s:if test="%{log.miscLog == null}">N/A</s:if>
                                                    <s:elseif test="%{log.miscLog.bloodPressureSystolic == 0}">N/A</s:elseif>
                                                    <s:else>
                                                        <s:property	value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.miscLog.bloodPressureSystolic,'/')" />
                                                        <s:property	value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.miscLog.bloodPressureDiastolic,'mmHg')" />
                                                    </s:else>
                                                </s:else>
                                            </td>
                                        </tr>
                                    </s:if>

                                    <!-- Log type : Took_Medication -->
                                    <s:if test="%{#logType == 'Took_Medication'}">
                                        <tr>
                                            <td class="first-column">Took Meds:</td>
                                            <td>
                                                <s:if test="%{log.miscLog == null}">N/A</s:if>
                                                <s:else>
                                                    <s:if test="%{log.miscLog.getTookMeds() != '' && log.miscLog.getTookMeds() == true}">
                                                        Yes
                                                    </s:if>
                                                    <s:else>
                                                        No
                                                    </s:else>
                                                </s:else>
                                            </td>
                                        </tr>
                                    </s:if>

                                    <!-- Log type : Medication -->
                                    <s:if test="%{#logType == 'Medication'}">
                                        <tr>
                                            <td class="first-column">Took Meds:</td>
                                            <td>
                                                <s:if test="%{log.miscLog == null}">N/A</s:if>
                                                <s:else>
                                                    <s:if test="%{log.miscLog.getTookMeds() != '' && log.miscLog.getTookMeds() == true}">
                                                        Yes
                                                    </s:if>
                                                    <s:else>
                                                        No
                                                    </s:else>
                                                </s:else>
                                            </td>
                                        </tr>
                                    </s:if>

                                    <s:if test="%{#logType == 'Sleep'}">
                                        <tr>
                                            <td class="first-column">Sleep:</td>
                                            <td>
                                                <s:if test="%{log.miscLog == null}">N/A</s:if>
                                                <s:else>
                                                    <s:if test="%{log.miscLog == null}">N/A</s:if>
                                                    <s:else>
                                                        <s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.miscLog.sleepHours,'hrs')" />
                                                        <s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.miscLog.sleepMinuts,'min')" />
                                                    </s:else>
                                                </s:else>
                                            </td>
                                        </tr>
                                    </s:if>

                                    <!-- Log type : ActivityMinute -->
                                    <s:if test="%{#logType == 'ActivityMinute'}">
                                        <tr>
                                            <td class="first-column">Type:</td>
                                            <td>
                                                <s:if test="%{log.activityLog.type == null}">N/A</s:if>
                                                <s:else>
                                                    <s:property value="%{log.activityLog.type}" />
                                                </s:else>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="first-column">Minutes Performed:</td>
                                            <td>
                                                <s:if test="%{log.activityLog.minutesPerformed == null}">N/A</s:if>
                                                <s:else>
                                                    <s:property value="%{log.activityLog.minutesPerformed}" /> mins
                                                </s:else>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="first-column">Intensity:</td>
                                            <td>
                                                <s:if test="%{log.activityLog.intensity == null}">N/A</s:if>
                                                <s:else>
                                                    <s:property value="%{log.activityLog.intensity}" />
                                                </s:else>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="first-column">Notes:</td>
                                            <td>
                                                <s:if test="%{log.activityLog.notes == null}">N/A</s:if>
                                                <s:elseif test="%{log.activityLog.notes == ''}">
                                                    N/A
                                                </s:elseif>
                                                <s:else>
                                                    <s:property value="%{log.activityLog.notes}" />
                                                </s:else>
                                            </td>
                                        </tr>
                                    </s:if>

                                    <tr>
                                        <td class="first-column">Log Time:</td>
                                        <td>
                                            <s:property value="@com.healthslate.patientapp.util.CommonUtils@timeInUserTimezone(log.getLogTime(), log.getLogTimeOffset(), log.getOffsetKey())" />
                                        </td>
                                        <td class="second-column">
                                            <s:if test="%{#logType == 'Meal' }">
                                                Processed:
                                            </s:if>
                                        </td>
                                        <td colspan="3">
                                            <s:if test="%{#logType == 'Meal' }">
                                                <s:if test="%{log.isProcessed == 1}">
                                                    <span class="bold-green-font">Yes</span>
                                                </s:if>
                                                <s:else>
                                                    <span class="bold-red-font">No</span>
                                                </s:else>
                                            </s:if>
                                        </td>
                                    </tr>
                                </table>
                                <br/><br/>
                            </s:else>
                        </div>
                    </div>
                </div>
                <s:form theme="simple" id="commentForm" action="addComments.action" cssClass="margin-bottom-0">
                    <s:hidden id="logId" name="logId"></s:hidden>
                    <s:hidden id="patientId" name="patientId"></s:hidden>
                    <s:hidden id="providerId" name="providerId"></s:hidden>
                    <s:hidden id="note" name="note"></s:hidden>
                </s:form>

                <s:form theme="simple" id="updatelogForm" action="processLog.action" cssClass="margin-bottom-0">
                    <s:hidden id="logId" name="logId"></s:hidden>
                    <s:hidden id="logProcessStatus" name="logProcessStatus"></s:hidden>
                    <s:hidden id="professionalNote" name="note"></s:hidden>

                </s:form>
                <div class="row row-auto log-details-nav-btns">
                    <div class="medium-12 columns padding-right-8">
                        <button id="sendNutInfoBtn" onclick="sendFoodNutritionInfo()" class="button success radius margin-bottom-0 btn-teal display-inline-block hide">Archive Log</button>
                        <div id="logDetailButtons" class="submit-btns">
                            <s:if test="%{isFromError == 'yes' && isRoleAdmin == false}">
                                <button id="logUnprocessedBtn" onclick="makeUnprocessed()" class="button success radius margin-bottom-0 btn-teal display-inline-block">Make Log Unprocessed</button>
                                <button id="sendNutInfoBtn" onclick="sendFoodNutritionInfo()" class="button success radius margin-bottom-0 btn-teal display-inline-block hide">Save & Send Nutrition Info</button>
                            </s:if>
                            <s:if test="%{isRoleAdmin == false && log.logType == 'Meal' && isFromError == 'no' && log.isProcessed == true}">
                                <button onclick="sendFoodNutritionInfo()" class="button success radius margin-bottom-0 display-inline-block btn-teal">Send Nutrition Info Again</button>
                            </s:if>
                            <s:elseif test="%{isRoleAdmin == false && log.logType == 'Meal' && isFromError == 'no' && log.isProcessed == false}">
                                <button onclick="sendFoodNutritionInfo()" class="button success radius margin-bottom-0 display-inline-block btn-teal">Save & Send Nutrition Info</button>
                            </s:elseif>

                            <s:if test="%{redirectToPage == 'fetchlogbook' && log.logType == 'Meal' && isRoleAdmin == false}">
                                <button onclick="sendFoodNutritionInfo()" class="button success radius margin-bottom-0 display-inline-block btn-teal">Save & Send Nutrition Info</button>
                            </s:if>
                            <s:if test="%{isFromError == 'yes'}">
                                <a class="button radius margin-bottom-0 secondary" href="viewReportErrors.action">Close</a> <span class="divider"></span>
                            </s:if>
                            <s:else>
                                <a class="button radius margin-bottom-0 secondary" onclick="closeLogDetailsPage();" href="#">Close</a> <span class="divider"></span>
                            </s:else>
                            <button onclick="archiveLog()" class="button success radius margin-bottom-0 display-inline-block btn-teal">Archive Log</button>
                        </div>
                    </div>
                </div>
            </div>

        </s:if>
        <s:else>
            <div class="remove-log">This log has been removed</div>
        </s:else>
    </div>
</div>

                 </div>

                <!-- Left grey area -->
                <div id="divCoachesAreaLeft" style="display: none;" data-equalizer-watch>
                    <script>
                        var patId = '${patientId}',
                                loggedInUserID = '${session.PROVIDER.providerId}';
                        $(document).ready(function(){
                            $("#divCoachesAreaLeft").load("${pageContext.request.contextPath}/educator/coachesArea.jsp");
                        });
                    </script>
                </div>
             </div>
         </div>
     </div>
 </div>
<s:form id="newMessageForm" theme="simple" action="findNewMessages.action">
    <s:hidden id="logIdval" name="logId"></s:hidden>
    <s:hidden id="maxTime" name="maxTime"></s:hidden>
</s:form>

<s:form id="Archivethislog" theme="simple" action="archive.action">
    <s:hidden id="logIdval" name="logId"></s:hidden>
    <s:hidden id="patientIdVal" name="patientId"></s:hidden>
    <s:hidden id="redirectToPage" name="redirectToPage"></s:hidden>
</s:form>

<s:form id="newLogForm" theme="simple" action="findNewLog.action">
    <s:hidden id="patientIdVal" name="patientId"></s:hidden>
    <s:hidden id="maxCount" name="maxCount"></s:hidden>
</s:form>

<s:form id="closeLogDetailsForm" theme="simple" action="fetchPatientLogs.action">
    <s:hidden name="logId"></s:hidden>
    <s:hidden name="patientId"></s:hidden>
    <s:hidden id="redirectToPage" name="redirectToPage"></s:hidden>
    <s:hidden id="foodLogName" name="foodLogName"></s:hidden>
    <s:hidden id="logMealName" name="logNameFromFood"></s:hidden>
    <s:hidden id="txtHiddenMealName" name="logName"></s:hidden>
</s:form>

<div id="inAppMessageDialog" class="reveal-modal medium">
    <h3 class="dialog-header meal-summary-text"> Full size image </h3>
    <img id="inAppMessageDialogImage" src="">
    <a class="close-reveal-modal">&#215;</a>
</div>

<div id="shareMealNotesDialog" class="reveal-modal medium" data-reveal>
    <h3 class="meal-summary-text remove-bold">Add Notes</h3>
    <p class="lead dialog-para">
        <label>
            Notes
            <textarea id="txtNotes" rows="7" placeholder="Add notes..." class="stop-horizontal-scroll"></textarea>
            <small class="error hide">Required.</small>
        </label>
    </p>
    <a id="closeShareMealNotesDialog" class="close-reveal-modal">&#215;</a>
    <div class="align-right">
        <a href="javascript:;" id="btnPostShareMealDialog" class="button radius small btn-teal margin-top-10 margin-bottom-0">Post</a>
        <a href="javascript:;" class="button secondary radius margin-top-10 small margin-bottom-0" onclick="$('#closeShareMealNotesDialog').click(); return false;">Cancel</a>
    </div>
</div>

<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
<![endif]-->
<!--[if (gte IE 9) | (!IE)]><!-->
<script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
<!--<![endif]-->

<!-- Foundation 3 for IE 8 and earlier -->
<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
<![endif]-->

<!-- Foundation 4 for IE 9 and later -->
<!--[if gt IE 8]><!-->
<script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
<!--<![endif]-->


<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/select2-3.5.2/select2.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap-typeahead.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/date.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.autosize.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
<script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js?v=0.1"></script>
<script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/nutrition_facts.js?v=0.11"></script>
<script src="${pageContext.request.contextPath}/resources/js/coach_patient_messages.js?v=0.10"></script>

<script type="text/javascript">

    $(document).foundation();

    var foodName ;
    var mealOptions = [], srcMealId = "", foodSuggestions = [], foodId = "",mealJson = "", foodNameSuggest = "", foodJson = "", mealNamePart1 = "",mealNamePart2 = "";
    var deleteFoodId ;
    var foodSummaryAddedList = [] ;

    /*Repacement Constants */
    var portionSizeR = "(portion size)";
    var foodR = "(food)";
    var theEachR = "the / each";
    var drinkR = "(drink)";
    var dash = "______";
    var foodNamesList = new Array();

    //__oz
    var FIRST_TIME_REFRESH = false,
        msgsTotalCount = "${messagesTotalCount}",
        globalFoodId, LABEL_COACH = "Coach",
        coachMessages = null,
        patId = "${patientId}",
        SMS_LIST = ("${coachMessagesList}") ? $.parseJSON("${coachMessagesList}") : undefined,
        TOTALS_CFPC = {
            "carbs": "${totalCarbs}",
            "fats": "${totalFats}",
            "proteins": "${totalProtein}",
            "calories": "${totalCalories}"
        },
        nutritionFacts = Object.create(NutritionFacts),
        patId = '${patientId}',
        loggedInUserID = '${session.PROVIDER.providerId}',
        CEILINGS = $.parseJSON('${targetString}'),
        CURRENT_MEAL_CEILING = 0,
        loggedIn_UserId = '${session.USER.userId}',
        LOCAL_STORAGE_MESSAGE_KEY = "messageTxtStorage",
        MEAL_ID = "";

    function replaceQuotes(val){
        return val.replace(/"/g, '\'');
    }

    function removeDuplicateRows(){
        var idArray = $('[data-trid]').map(function(index, r){
            var c = $(r).attr("class"),
                start = "nutTableRow".length;

            return c.substring(start, c.length);
        });

        if(idArray && idArray.length > 0){
            $.each(idArray, function(index, id){

                var $tr = $("tr.nutTableRow"+id);
                if($tr.length > 1){
                    $tr.not(":last").remove();
                }
            });
        }
    }

    $(function() {

        $("#loading").show();

        removeDuplicateRows();

        //show last request date in Meal sharing
        if('${lastSharedDate}'){
            $("#spLastMealSharedDate").text(HSDateUtils.formatDateTimeFromTicks(parseFloat('${lastSharedDate}'))).parent().show();
            $("#btnMealShared").val("Share Again").text("Share Again");
        }else if('${lastRequestDate}'){
            $("#spLastRequestDate").text(HSDateUtils.formatDateTimeFromTicks(parseFloat('${lastRequestDate}'))).parent().show();
        }

        if(CEILINGS['${log.foodLogSummary.type}']){
            CURRENT_MEAL_CEILING = CEILINGS['${log.foodLogSummary.type}'];
        }

        $("#btnPostShareMealDialog").off("click").on("click", function(e){
            e.preventDefault();

            var $txtNotes = $("#txtNotes");

            $txtNotes.next().addClass("hide");

            if(!$txtNotes.val()){
                $txtNotes.next().removeClass("hide");
                return;
            }

            if(MEAL_ID){
                generateNewsFeedPost(MEAL_ID, $txtNotes.val());
            }
        });

        $("#btnMealShared").off("click").on("click", function(e){

            MEAL_ID = '${logId}';
            $("#txtNotes").val("");

            if('${shareMealNotes}'){
                $("#txtNotes").val('${shareMealNotes}')
            }
            $("#shareMealNotesDialog").foundation("reveal", "open");
            return false;
        });

        populateQuestionSelector();
        bindHideShowMessagesView();

        $("#btnOpenQuestionsDialog").on("click", function(){
            $('#modalFavoriteQuestions').foundation('reveal', 'open');
        });

        $(".selected-question").on("click", function(e){
            e.preventDefault();
            $('#modalFavoriteQuestions').foundation('reveal', 'close');
            var favoriteQuestion = $(this).text();
            addQuestion(favoriteQuestion);
        });

        $('#select2').select2();
        $('#select2').on("change", function(e) {
            currentData = $(this).val();
            addQuestion(currentData);
        });

        if("${actionMessage}"){
            $("#actionError").show();
        }

        $("#logSuccess").hide();
        $("#infoSuccess").hide();
        $("#infoSuccess").hide();
        $("#infoError").hide();

        if("${statusMessage}".indexOf("LOG_SUCCESS") != -1){
            $("#logSuccess").text("Log Processed successfully");
            $("#logSuccess").slideDown();
        }

        if("${statusMessage}".indexOf("SUCCESS_INFO") != -1){
            $("#infoSuccess").text("Nutrition Info sent successfully");
            $("#infoSuccess").slideDown();
        }

        if("${statusMessage}".indexOf("DATA") == -1 && "${statusMessage}".indexOf("LOG_SUCCESS") == -1){
            $("#logError").text("Error in Log Processing");
            $("#infoSuccess").slideDown();
        }

        if("${statusMessage}".indexOf("DATA") == -1 && "${statusMessage}".indexOf("SUCCESS_INFO") == -1){
            $("#infoError").text("Error in sending Nutrition Info");
            $("#infoError").slideDown();
        }

        $("#loading").show();
        $('#unProcessedMeals').addClass('selected-tab');

        if ("${shouldRenderForTablet}") {
            $('#header').hide();
            $('.breadcrumb').hide();
            $('#addNotesDiv').hide();
            $('#foodButtons').hide();
            $('.submit-btns').hide();
            $('.foodAction').hide();
            $('.mainDiv').css('width', '100%');
        } else{
            $('.foodEmptyColumn').hide();
        }

        hideElementIfShown($("#divMoods"));
        bindSwitch($("#switch"), true);

        //set meal option shown in add food dialog
        var meals = "${mealOptions}";
        if(meals.length){
            mealOptions = meals.split(",");
        }

        //hide messages
        hideElement($("#actionMessage"));
        hideElement($("#logSuccess"));
        hideElement($("#logError"));
        hideElement($("#infoSuccess"));
        hideElement($("#infoError"));

        $(".new-msg-box").bind('keyup', function (e) {
            var value = this.value;
            this.value = value.substr(0, 1).toUpperCase() + value.substr(1);
        });

        var isRemoved = $("#isRemoved").val();
        if(isRemoved == "false" ){
            //$('.div-1-food-detail').animate({ scrollTop: $('.div-1-food-detail')[0].scrollHeight}, 1000);
            //$('.messages-panel').animate({ scrollTop: $('.messages-panel')[0].scrollHeight}, 1000);
        }
        var isRoleAdmin = "${isRoleAdmin}";

        //refresh messages threaded view
        if(isRoleAdmin.indexOf("false") != -1){

            nutritionFacts.init("${pageContext.request.contextPath}", '${mealJson}', '${foodJson}');

            //first call on page load, __oz
            refreshMessagesThreadedView();
        }

        if(getParameterByName('glucoselog')){
            $("#other-details").removeClass("secondary");
            $("#food-details").addClass("secondary");

            $("#foodDetails").hide();
            $("#otherDetails").show();
        }

        $("#btnMemberFoodRestrictions").on("click", function(e){
            e.preventDefault();
            $("#memberEatingPreferenceDialog").foundation("reveal", "open");
        });

        //load coaches area via ajax
        //Ajaxing.loadExternalPage($("#divCoachesAreaLeft"), "${pageContext.request.contextPath}/educator/coachesArea.jsp", loadExpCollapse);
        Ajaxing.loadExternalPage($("#divMemberNavArea"), "${pageContext.request.contextPath}/educator/memberNavArea.jsp", function(){
            $("#divMemberNavArea ul").css("top", "10px");
        });

        $("#loading").hide();
    });

    function getParameterByName( name ){
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)",
                regex = new RegExp( regexS ),
                results = regex.exec( window.location.href );
        if( results == null ){
            return "";
        } else{
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

    function getFormatedDateTime(date){
        var d = new Date(date);

        var month = d.getMonth()+1;
        var day = d.getDate();
        var hour = d.getHours();
        var minute = d.getMinutes();
        var ampm = 'AM';
        if(hour > 12){
            ampm = 'PM';
            hour = hour - 12;
        }

        if(hour != 12){
            hour = "0"+hour;
        }

        if(minute < 10){
            minute = "0"+minute;
        }

        var date = ((''+month).length<2 ? '0' : '') + month + '/' +
                ((''+day).length<2 ? '0' : '') + day + '/' +
                d.getFullYear();

        var time = ((''+hour).length<2 ? '0' :'') + hour + ':' +
                ((''+minute).length<2 ? '0' :'') + minute +" "+ ampm ;

        return date+" "+time;
    }

    function isNumber(evt) {

        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if ((charCode > 47 && charCode < 58) ||
                (charCode == 38) || (charCode == 40) ||
                (charCode == 9) || (charCode == 16) ||
                (charCode > 95 && charCode < 106) ||
                (charCode == 8) || charCode == 110 || charCode == 190) {

            return true;
        }
        return false;
    }

    function processLog(){
        $('#waiting-div').show();
        $("#logId").val('${logId}');
        $("#patientId").val('${patientId}');
        $("#logProcessStatus").val(true);
        $("#professionalNote").val($('.logNote').val());
        $("#updatelogForm").submit();
    }

    function addComments(){

        if($('.logNote').val() != ""){
            $("#logId").val('${logId}');
            $("#patientId").val('${patientId}');
            $("#note").val($('.logNote').val());

            var config = {
                "requestType":"GET",
                "successCallBack": onSuccessCommentAdd,
                "loading": $("#loading")
            };
            sendAjaxRequestWithData("addCommentsAjax", $("#commentForm").serialize(), config);
        }
    }

    function onSuccessCommentAdd(data){

        if($(".patContentDiv").length && $(".patContentDiv").text().indexOf("Not Available") != -1){

            var $newTable = '<table class="noteDivTableHeader"><tbody></tbody></table>';
            var $headerRow = '<tr style="background: #E6E6E6;"></tr>';
            var $headerCol1 = '<td style="width: 60%;"><label style="font-weight: bold; cursor: default;">Note</label></td>';
            var $headerCol2 = '<td style="width: 39%;"><label style="font-weight: bold; cursor: default;">Date Created</label></td>';

            $headerRow.append($headerCol1).append($headerCol2);
            $newTable.append($headerRow);

            var $dataDiv ='<div class="noteDiv"></div>';
            var $dataTable = '<table class="noteDivTable"><tbody></tbody></table>';
            var $dataTableRow = '<tr><td style="width: 60%;">'+$('.logNote').val()+'</td><td style="width: 39%;vertical-align: top;">'+data+'</td></tr>';
            $dataTable.append($dataTableRow);
        }
        else{
            var $dataTableRow = '<tr><td style="width: 60%;">'+$('.logNote').val()+'</td><td style="width: 39%;vertical-align: top;">'+data+'</td></tr>';
            $(".noteDivTable").append($dataTableRow);
        }

        $("#infoSuccess").text("Comment added successfully");
        $("#infoSuccess").slideDown();
        hideElement($("#infoSuccess"));

        $('.logNote').val("");
    }

    function hideElementIfShown($elem){
        if($elem.is(":visible")){
            $elem.hide();
        }
    }

    function saveData(){
        var note = $('.logNote').val();
        if(note){
            if(confirm("Your entered data will be lost.")){
                return true;
            } else{
                return false;
            };
        };
    }

    function hideElement(element){
        if(element.is(":visible")){
            setTimeout(function(){
                $(element).slideUp("slow");
            }, 3000);
        }
    }

    function sendMessage(){

        var $newMessage = $(".new-msg-box"),
                messageTxt = $.trim($newMessage.val()),
                $messageError = $("#messageError");

        if(messageTxt){
            if(messageTxt.indexOf(dash) > -1){
                showAlertMessage($messageError, "Please fill the empty value", "ERROR");
            }else{
                $("#message").val(messageTxt);
                var config = {
                    "requestType":"GET",
                    "successCallBack": onSuccessMessageSent,
                    "loading": $("#loading"),
                };
                sendAjaxRequestWithData("sendMessage", $("#msgForm").serialize(), config);
            }
        } else {
            $newMessage.val("");
            showAlertMessage($messageError, "Please send a valid message", "ERROR");
        }
    }

    function onSuccessMessageSent(data){
        if(data == null){
            showAlertMessage($("#actionError"), "Message sending failed", "ERROR");
        }else{
            addNewMessage(LABEL_COACH, data, $(".new-msg-box").val());
            $(".checkbox").show();
            $(".new-msg-box").val("");
        }
    };

    function addNewMessage(owner, time, message){
        if(owner == LABEL_COACH){
            $(".messages-panel").append('<div class="educator-msg-panel msg-panel"><label class="msg-head white-color">'+owner+'</label><label class="date white-color word-wrap-style">'+time+'</label><hr><label class="white-color">'+message+'</label></div>');
        }
        else{
            $(".messages-panel").append('<div class="patient-msg-panel msg-panel"><label class="msg-head">'+owner+'</label><label class="date">'+time+'</label class="word-wrap-style"><hr><label>'+message+'</label></div>');
        }
        $('.messages-panel').animate({ scrollTop: $('.messages-panel')[0].scrollHeight}, 1000);
    }

    function sendFoodNutritionInfo(){

        /*Add Logic Here*/
        var rowCount = $('#nutTable tr').length,
            $form = $("#nutritionInfoForm").attr("action", "sendFoodNutritionInfo.action");

        if(rowCount == 0)
        {
            $("#infoError").html("<div class='On-success-advice'>No Food Added</div>");
            $("#infoError").slideDown();
            setInterval(function () {$("#infoError").slideUp();}, 4000);

        }
        else
        {
            $("#newNote").val($('.logNote').val());
            $("#hasMissingFood").val($("#missingFood").is(":checked"));
            $("#foodLogName").val(replaceQuotes($("#logNameFromFood").val()));
            $("#txtHiddenMealName").val(replaceQuotes($("#logNameFromFood").val()));
            $("#nutritionInfoForm").submit();
        }

    };

    function askMemberPermission(){

        var actionUrl = "askMemberPermission.action"
                , dataToSend = {
                    "logId": "${logId}"
                }, postRequestActions = {
                    "requestType": "GET",
                    "loading": $("#loading"),
                    "successCallBack": onAskingMemberPermission
                };

        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
    }

    function onAskingMemberPermission(data){
        var alertMessage = AlertMessage.init(),
            $err = $("#errMessagePage");

        if(data && data.STATUS == "SUCCESS"){
            alertMessage.showAlertMessage($err, "Permission request sent successfully.", alertMessage.SUCCESS, true);
        }else if(data.REASON == "REQUEST_SENT_ALREADY"){
            alertMessage.showAlertMessage($err, "Permission request sent successfully.", alertMessage.SUCCESS, true);
        }

        location.reload(true);
    }

    function generateNewsFeedPost(mealId, notes){
        var actionUrl = "generateNewsFeedPost.action"
                , dataToSend = {
                    "logId": mealId,
                    "token": notes
                }
                , postRequestActions = {
                    "requestType": "GET",
                    "loading": $("#loading"),
                    "successCallBack": onGeneratingNewsFeed
                };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
    }

    function onGeneratingNewsFeed(data){
        var alertMessage = AlertMessage.init(),
            $err = $("#errMessagePage");

        $("#shareMealNotesDialog").foundation("reveal", "close");
        $("#txtNotes").val("");

        if(data && data.STATUS == "SUCCESS"){
            alertMessage.showAlertMessage($err, "Meal shared with Group successfully.", alertMessage.SUCCESS, true);
        }else {
            alertMessage.showAlertMessage($err, "Unable to share meal.", alertMessage.ERROR, true);
        }
    }

    function saveAsSuggestedLog(btn){
        var $form = $("#nutritionInfoForm").attr("action", "saveAsSuggestedMealLog.action");
        $("#newNote").val($('.logNote').val());
        $("#hasMissingFood").val($("#missingFood").is(":checked"));
        $("#foodLogName").val(replaceQuotes($("#logNameFromFood").val()));
        $("#txtHiddenMealName").val(replaceQuotes($("#logNameFromFood").val()));
        $(btn).attr("disabled", "true").off("click");
        $form.submit();
    }

    function onSuccessInfoSent(data){
        if(data == "SUCCESS"){
            showAlertMessage($("#nutritionMessage"), "Nutrition info send successfully", SUCCESS);
        }
        else{
            //showAlertMessage($("#nutritionError"), "Nutrition info sending failed", ERROR);
            redirectToLogin($("#nutritionError"), data, "Nutrition info sending failed");
        }
    }

    function addQuestion(currentData){

        //__oz updated following code
        var question = currentData,
                $newMessageBox = $(".new-msg-box").focus();

        if(currentData.indexOf(foodR) != -1 || currentData.indexOf(portionSizeR) != -1 || currentData.indexOf(theEachR) != -1 || currentData.indexOf(drinkR) != -1){
            question = currentData.replace(foodR, dash)
                    .replace(theEachR, dash)
                    .replace(portionSizeR, dash)
                    .replace(drinkR, dash);
        }

        $newMessageBox.val(question);
        $('.new-msg-panel').animate({ scrollTop: $('.new-msg-panel')[0].scrollHeight}, 1000);
    }

    //changed 'refresh' function signature to 'refreshMessagesThreadedView', __oz
    function refreshMessagesThreadedView(){

        coachMessages = Object.create(CoachMessages)
                .init("${pageContext.request.contextPath}", patId, "${logId}", "${onFoodImageName}", true);
    }

    function makeUnprocessed(){
        $("#logUnprocessedBtn").addClass("hide");
        $("#sendNutInfoBtn").removeClass("hide");
        $(".new-msg-panel").removeClass("hide");
        $("#foodActionInError").css("display", "block");
        $("#addFoodInError").css("display", "block");
    }

    function populateQuestionSelector(){
        var actionUrl = "populateQuestionSelector.action"
                , dataToSend = {


                }
                , postRequestActions = {
                    "requestType" : "GET",
                    "successCallBack" : questionSelector,
                    "loading": $("#loading")


                };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
    }

    function questionSelector(data){
        var mySelect = $('#select2');
        $.each(data, function (i, val) {
            var group = $('<optgroup>').prop('label', i);
            for (var i in val) {
                group.append($('<option>').html(val[i]));
            }

            mySelect.append(group);
        });
    }

    function archiveLog(){
        $("#loading").show();
        $("#Archivethislog").submit();
    }

    function closeLogDetailsPage(){
        $("#loading").show();

        var $mealName = $("#logNameFromFood");

        if($mealName && $mealName.val()){
            $("#foodLogName").val(replaceQuotes($mealName.val()));
            $("#txtHiddenMealName").val(replaceQuotes($mealName.val()));
        }

        var actionUrl = "updateMealName.action",
                dataToSend = {
                    "logId": $("#logIdval").val(),
                    "logName": $mealName.val()
                },
                postRequestActions = {
                    "requestType" : "GET",
                    "loading" : null
                };

        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        $("#closeLogDetailsForm").submit();
    }

    function saveToFavorite ()  {
        var actionUrl = "saveQuestionToFavorite.action"
            , dataToSend = {

                "favoriteQuestion": adviceId
            }
            , postRequestActions = {
                "requestType" : "GET",
                "successCallBack" : onSuccessSending,
                "loading": $("#loading")
            };

        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
    }

    function closeModal()  {
        $(".close-reveal-modal").click();
    }

    function bindHideShowMessagesView(){

        $("#btnHideShowMessagesView").on("click", function(e){
            e.preventDefault();

            var $this = $(this),
                $thisParent = $this.parent(),
                $img = $this.find("img"),
                $messagesView = $("#divMessagesView").hide("slow"),
                thisText = $this.text();

            if($img.attr("src") == "${pageContext.request.contextPath}/resources/css/images/arrow-up.png"){
                $img.attr("src", '${pageContext.request.contextPath}/resources/css/images/arrow-down.png');
                $messagesView.hide("slow");
            }else{
                $messagesView.show("slow");
                $img.attr("src", '${pageContext.request.contextPath}/resources/css/images/arrow-up.png');
            }
        });
    }

    function showFullSizedFoodImage() {
        $("#foodImageDialogImage").prop("src", $("#foodImage").prop("src"));
        $("#foodImageDialog").foundation("reveal", "open");
    }

</script>
</body>
</html>