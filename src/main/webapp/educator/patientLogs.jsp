<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>

<head>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/queue.style.css" />
<title>Patient Logs</title>
<style type="text/css">

body{
	font-size: 14px;	
 	background-color: #fff; 	
 	font-family:arial;
	height: 100%;
}

.input-field {
	width: 35% !important;
}

.ui-datepicker-trigger {
	margin: 0px;
	height: 37px;
	padding-top: 11px;
	width: 5px;
	padding-left: 1rem;
	background-color: #428bca;
}

.date-style {
	width: 82%;
	display: inline-block !important;
}

.header-style {
	width: 60%;
	padding: 0;
	top: 10px;
}

.ui-datepicker-trigger {
	margin: 0px;
	height: 37px;
	padding-top: 17px;
	width: 5px;
	padding-left: 1rem;
}

.dataTables_processing {
	position: absolute;
	top: 2% !important; 
	left: 50%;
	width: 250px;
	height: 30px;
	margin-left: -125px;
	margin-top: -15px;
	padding: 14px 0 2px 0;
	border: 0px !important; 
	text-align: center;
	color: #999;
	font-size: 14px;
	background: inherit !important;
	font-weight: bold;
	color: brown;
}
th {
	font-family: inherit !important;
}

.align-table{
	
	width:100% !important;
}

.table-div{
width: 96%;
}
</style>
</head>
<body>

<!-- BreadCrumb -->
<div class="breadcrumb-green">
    <div class="max-width-1129">
        <ul class="breadcrumbs">
            <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                <li>
                    <a href="unProcessedMeals.action">Meals</a>
                </li>
            </s:if>
            <li>
                <a href="members.action">My Members</a>
            </li>
            <li class="current">Member Logs</li>
            <li class="current"><b>Member Name:</b> <s:property value='%{patient.user.displayName}' /></li>
        </ul>
    </div>
</div>

	<br />
	<div class="main-page header-style" id="advancedControls">
		<s:form id="sForm" theme="simple" method="post" style="margin:0">

			<div class="searchHeader advSerDivLbl cursor-pointer"
				onclick="showHideAdvSearch()">
				<span class="cursor-pointer"><img class="toggle-style toggle"
					title="Show Advance Search" name="show" alt=""
					src="${pageContext.request.contextPath}/resources/images/show.png">
					Advance Search</span>
			</div>
			<div class="innerTableClass advSerDiv">
				<s:hidden id="patientId" name="patientId"></s:hidden>
				<s:hidden id="maxCount" name="maxCount"></s:hidden>
				<table class="table" style="margin-bottom: 0">
					<tr>
						<td align="left"><label class="inline">Date From</label></td>
						<td>
							<div class="collapse" style="display: block">
								<div class="small-10 columns padding-left-right-0">
									<s:textfield id="fromDate" data-calId="btnDateFrom"
										cssClass="patient-detail-text date-style" name="dateFrom"
										readonly="true"></s:textfield>
								</div>
								<div class="small-2 columns padding-left-right-0">
									<a id="btnDateFrom" href="#" class="button btn-teal postfix"
										style="background-color: #357ebd;"><img alt=""
										style="padding-top: 11px;"
										src="${pageContext.request.contextPath}/resources/images/calendar.gif"></a>
								</div>
							</div>
						</td>
						<td align="right"><label class="inline">Date To</label></td>
						<td>
							<div class="collapse" style="display: block">
								<div class="small-10 columns padding-left-right-0">
									<s:textfield id="toDate" data-calId="btnDateTo"
										cssClass="patient-detail-text date-style" name="dateTo"
										readonly="true"></s:textfield>
								</div>
								<div class="small-2 columns padding-left-right-0">
									<a id="btnDateTo" href="#" class="button btn-teal postfix"
										style="background-color: #357ebd;"><img alt=""
										style="padding-top: 11px;"
										src="${pageContext.request.contextPath}/resources/images/calendar.gif"></a>
								</div>
							</div>
						</td>
					</tr>
					<tr class="white-tr">
						<td style="text-align: left; padding-left: 18px;"><label class="inline">Status</label></td>
						<td><s:select id="advStatusMap" list="statusMap" name="advstatus"></s:select></td>
						<td align="right"><label class="inline">Log Type</label></td>
						<td>
							<s:select id="advLogMap" cssClass="display-inline-block" list="LogTypeMap" name="logstatus"></s:select>
						</td>
					</tr>
					<tr>
						<td colspan="4" align="center">
							<input type="button" value="Refresh & Search" class="button radius btn-teal" onclick="return redrawTable()">
						</td>
					</tr>
				</table>
			</div>

			<s:hidden id="isAdvSearch" name="isAdvSearch"></s:hidden>

		</s:form>
	</div>
	<br />
	<br />

	<div id="loading" class="loader" style="display: block;">
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>
			<div class="progress progress-striped active">
				<div class="progress-bar" role="progressbar" aria-valuenow="100"
					aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<!-- <p id="progressTxt" class="progress-text">We're uploading your content!!!</p> -->
		</div>
	</div>

	<div id="infoSuccess" class=" alert success alert-new-message"></div>

	<div class="max-width-1129 align-right">
		<label class="display-inline-block bold-style">Currently Displaying: </label>
		<s:select id="basicStatusMap" cssClass="display-inline-block width-17per" list="statusMap" name="basicstatus"></s:select>
	</div>

	<div class="max-width-1129 align-right">
		<label class="display-inline-block bold-style">Log Type: </label>
		<s:select id="basicLogMap" cssClass="display-inline-block width-17per" list="LogTypeMap" name="logstatus"></s:select>
	</div>

	<div id="tableDiv" class="main-page table-div padding-lr-0 green-bordered max-width-1129">

		<div class="filter-div-2 font-size15p align-center">
			<label class="display-inline-block medium-font">Logs from </label> 
			<span ><b><span id="dateFromProp" ></span></b> To <b><span id="dateToProp"></span></b> </span>
		</div>
		<div class="container align-table" id="click_me">

			<table id="table" class="table border-none">
				<thead>
					<tr>
						<th>Log Id</th>
						<th>Log Time</th>
						<th>Log Type</th>
						<th>Value</th>						
						<th>Carbs</th>
						<th>Meal Type</th>
						<th>Processed</th>
						<th>Processed Duration</th>
						<th>Processed By</th>
						<th onclick="return check();">Detail</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

	<script	src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>
	
	<script type="text/javascript">
	
		var CurrenDisplayValue ;
		var oTable;
		var showRowCount = 0;
		$(document).ready(function(){
			$('#dateFromProp').text($('#fromDate').val());
			$('#dateToProp').text($('#toDate').val());
			$('#unProcessedMeals').addClass('selected-tab');
			
			  $.fn.dataTableExt.sErrMode = 'throw'; 
			  oTable=  $('#table').dataTable(
			  {
				  "sDom" : "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'spanPagination'p>>",
				"sPaginationType" : "bootstrap",
				"oLanguage" : {
					"sLengthMenu" : "_MENU_ records per page"
				},
				"bServerSide": true ,
              "fnInitComplete": function (oSettings, json) {
                  //remove sorting class from td, __oz
                  $(".sorting_1").removeClass("sorting_1 sorting_1");
              },
                  "fnDrawCallback": function( oSettings ) {
                      $(".sorting_1").removeClass("sorting_1 sorting_1");
                  },
		 		"sAjaxSource": "fetchLogsfilter.action",
				"fnServerParams": function ( aoData ) {
				      aoData.push( {"name": "dateFrom", "value": $('#fromDate').val()},
				    		  	   {"name": "dateTo", "value": $('#toDate').val()},
				    		  	   {"name": "basicstatus", "value": $('#basicStatusMap').val() },
				    		  	   {"name": "advstatus", "value": $('#advStatusMap').val()},
				    		  	 	{"name": "logstatus", "value": $('#basicLogMap').val()},
				    		  	   {"name": "isAdvSearch", "value": $('#isAdvSearch').val()},
				    		  	 	
				    		  	 	{"name": "patientId", "value": $('#patientId').val()}
				    		  	 	
				    		  	 
				    		  	   );
				},  
				
				"bRetrieve" : true,
				"bPaginate" : true,
				"bStateSave" : false,
				"bSort" : true,
				"aaSorting": [[ 1, "desc" ]],
				"bProcessing": true,
	 			"aoColumns": [
			                   { "mDataProp": "logId"},
			                   { "mDataProp": function(data, type, full){

								   var signatureHtml = "<span class='signature'>Not Signed</span>";

								   if(data.isSigned){
									   signatureHtml = "";
								   }

								   return "<span class='no-line-break'>"+data.logTimeString+"</span>"+signatureHtml;
			                   }},


                               { "mDataProp": function ( data, type, full ){
                                   if(data.logType == "ActivityMinute"){
                                       return "<span class='no-line-break'>Activity ("+ data.alType +")</span>";
                                   }
                                   return "<span class='no-line-break'>"+data.logType+"</span>";
                               }},
			                   { "mDataProp": function ( data, type, full ){
			                	   if(data.logType == "Activity") {
			                		   if(data.stepsPerDay){
			                		   		return "<span class='no-line-break'>"+data.stepsPerDay+"<br/>steps </span>";   			                			   
			                		   }
			                		   return "<span class='no-line-break'>N/A</span>";			                		   
			                	   } else if(data.logType == "Medication") {
			                		   if(data.tookMeds) {
			                			   return "<span class='no-line-break'>"+data.tookMeds+"</span>";
			                		   }
			                		   return "<span class='no-line-break'>N/A</span>";
			                	   } else if(data.logType == "Glucose") {
			                		   if(data.glucoseLevel) {
			                		   return "<span class='no-line-break'>"+data.glucoseLevel+"<br/>mg/dl</span>";
			                		   }
			                		   return "<span class='no-line-break'>N/A</span>";
			                	   } else if(data.logType == "Meal") {
			                		   return "<span class='no-line-break'>N/A</span>";
			                	   } else if(data.logType == "Weight") {
			                		   if(data.weight) {
			                		   return "<span class='no-line-break'>"+data.weight+"<br/>lbs</span>";
			                		   }
			                		   return "<span class='no-line-break'>N/A</span>";
			                	   } else if(data.logType == "ActivityMinute") {
                                       if(data.alMinutesPerformed) {
                                           return "<span class='no-line-break'>"+data.alMinutesPerformed+"<br/>mins(s)</span>";
                                       }
                                       return "<span class='no-line-break'>N/A</span>";
                                   }
                               }},
			                   { "mDataProp": "carbs"},
			                   { "mDataProp": "MealType"},
			                   { "mDataProp": function ( data, type, full ){
			                	   if(data.isProccessedString == "No")
			                		   {
			                		   return '<p class="proflagNo">No</p>';
			                		   }
			                	   else if (data.isProccessedString == "Yes")
			                		   {
			                		   return '<p class="proflagYes">Yes</p>';
			                		   }
									
								}},
							   { "mDataProp": "minutesHours", 'bSortable': false},
							   { "mDataProp": "coachName", 'bSortable': false},
			                   { "mDataProp": function ( data, type, full ){
										return '<a href="${pageContext.request.contextPath}/app/educator/fetchLogDetail.action?logId='+data.logId+'&amp;patientId='+$('#patientId').val()+'&amp;isFromError=no"><button class="button radius btn-teal expand tiny">Details</button></a>';
								  },'bSortable': false},									
			                   ]
			  }); 
	 
			initDatePicker("fromDate");
			initDatePicker("toDate");
			
			//filterLogs();
			$('.row').css({'max-width':'100%', 'margin-top':'-9'});
			
			//basic status filter
			$("#basicStatusMap").on("change", function(){
				$("#loading").show();
				$("#advStatusMap").val($(this).val());
				CurrenDisplayValue =$("#basicStatusMap").val() ;
				redrawTable();
			});
			CurrenDisplayValue =$("#basicStatusMap").val() ;
			
			$("#basicLogMap").on("change", function(){
				$("#loading").show();
				var selected = ($(this).val());
				if(selected==1)
					{
					//$("#advStatusMap").prop("disabled", true);
					//$("#basicStatusMap").prop("disabled", true);
					CurrenDisplayValue =$("#basicStatusMap").val() ;
					$("#advStatusMap").val("0");
					$("#basicStatusMap").val("0");
					
					}
				else
					{
					//$("#advStatusMap").prop("disabled", false);
					//$("#basicStatusMap").prop("disabled", false);
					$("#advStatusMap").val(CurrenDisplayValue);
					$("#basicStatusMap").val(CurrenDisplayValue);
					
				}
				$("#advLogMap").val($(this).val());
				redrawTable();
			});
			
			$("#advStatusMap").on("change", function(){
				$("#basicStatusMap").val($(this).val());
			});
			
			$("#advLogMap").on("change", function(){
				$("#basicLogMap").val($(this).val());
				var selected = ($(this).val());
				if(selected==1)
				{
				//$("#advStatusMap").prop("disabled", true);
				//$("#basicStatusMap").prop("disabled", true);
				
				}
			else
				{
				//$("#advStatusMap").prop("disabled", false);
				//$("#basicStatusMap").prop("disabled", false);
				
				}
				
			});
			
			setInterval(function(){

				var actionUrl = "findNewLogs.action"
					, dataToSend = {
						"maxCount": $("#maxCount").val(),
						"patientId": $("#patientId").val(),
					}
				 	, postRequestActions = {
						"requestType" : "GET",
				 	  	"successCallBack" : showMessage
				 	};
			 	sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions); 
    		}, 20000);
			
			var $ul = $(".dataTables_paginate ul");
			
			$ul.each(function(){
				var $this = $(this);
				if($this && !$this.hasClass("pagination")){
					$this.addClass("pagination");
				}
			});
			
			$("#loading").hide();
		}); 
		function redrawTable(){
			$("#infoSuccess").slideUp("slow"); 
			oTable.fnDraw();
			$("#loading").hide();
			
			$('#dateFromProp').text($('#fromDate').val());
			$('#dateToProp').text($('#toDate').val());
		}
		
		function showMessage(data){
			
			var count = parseInt($("#maxCount").val());
			if(parseInt(data) > count){
				$("#infoSuccess").html("<div class='new-log-message'>New Log(s) Found</div> <div class='refresh-link'><a  class='cursor-underline color-white-link' href='javascript:void(0);' onclick='redrawTable()'>Refresh</a> &nbsp;<a  class='cursor-underline color-white-link' href='javascript:void(0);' onclick='hideMessage()'>Dismiss</a></div>");
				$("#infoSuccess").slideDown();
				$("#maxCount").val(parseInt(data));
				hideElement($("#infoSuccess"));				
			}
		}
		
		function hideElement(element){
			setTimeout(function(){
				$(element).slideUp("slow");
			}, 10000);
		}
		
		function hideMessage(){
			$("#infoSuccess").slideUp("slow");
		}
		
		function showHideAdvSearch(){
			
			if ($(".toggle").attr("name") == 'show') {
	        	$(".advSerDiv").slideDown("slow");
	        	$(".toggle").attr("name", "hide");
	        	$(".toggle").attr("src", "${pageContext.request.contextPath}/resources/images/hide.png");
	        	$(".toggle").attr("title", "Hide Advance Search");
	        	$("#isAdvSearch").val(true);
	        	
	        	
	        } else {
	        	$(".advSerDiv").slideUp("slow");
	        	$(".toggle").attr("name", "show");
	        	$(".toggle").attr("src", "${pageContext.request.contextPath}/resources/images/show.png");
	        	$(".toggle").attr("title", "Show Advance Search");
	        	$("#isAdvSearch").val(false);
	        }
		}
		
	</script>
</body>
</html>