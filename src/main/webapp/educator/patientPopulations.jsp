<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>
	<title>My Patients</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/logDetail.css" />
	
	<style type="text/css">
		.bs-example{
			margin: 20px;
		}
		.tab-content{
			display: block;
		}
		.heading-style{
			background-color: rgb(238, 238, 238);
		}
		body{
			background-color: #eee !important;
		}
		
		.main-page{
			box-shadow:none;
			border-radius:2px;
		}

		.footer{
			position: fixed;
			bottom: 0;
		}
	</style>
</head>
<body>
	
	<br/>
	<div id="loading" class="loader" style="display: block;">
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>
			<div class="progress progress-striped active">
				<div class="progress-bar" role="progressbar" aria-valuenow="100"
					aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<!-- <p id="progressTxt" class="progress-text">We're uploading your content!!!</p> -->
		</div>
	</div>
	<div class="bs-example">
		<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	</div>
	<s:hidden id="patientId" name="patientId"></s:hidden>
	
	<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
 	<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>
	
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
	
	<script type="text/javascript">
	
		$(function(){
			
			$('#patientPoplulationTab').addClass('selected-tab');
			populatePatientPopulationChart();
			$("#loading").hide();
		});
		
		function populatePatientPopulationChart(){
			$('#container').highcharts({
		        title: {
		            text: 'Patient Population Track',
		            x: -20 //center
		        },
		        subtitle: {
		            text: '',
		            x: -20
		        },
		        xAxis: {
		            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
		                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		        },
		        yAxis: {
		            title: {
		                text: 'Value'
		            },
		            plotLines: [{
		                value: 0,
		                width: 1,
		                color: '#808080'
		            }]
		        },
		        tooltip: {
		            valueSuffix: ''
		        },
		        legend: {
		            layout: 'vertical',
		            align: 'right',
		            verticalAlign: 'middle',
		            borderWidth: 0
		        },
		        series: [{
		            name: 'HDL test history',
		            data: [7.0+' mmol/1', 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
		        }, {
		            name: 'LDL test history',
		            unit : 'mmol/1',
		            data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
		        }, {
		            name: 'BMI history/trends',
		            unit : 'mmol/1',
		            data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
		        }, {
		            name: 'A1C test history',
		            unit : 'mmol/1',
		            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
		        }, {
		        	name: 'Avg BGL trend',
		        	unit : 'mmol/1',
		        	data: [1.9, 5.2, 4.7, 8.5, 10.9, 11.2, 13.0, 15.6, 12.2, 6.3, 5.6, 4.8] 
		        }]
		    });
		}
	</script>
</body>
</html>