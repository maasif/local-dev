<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>      
   <title>My Members</title>
   <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css?v=0.1" />
    <style type="text/css">
        .input-field {
            width: 35% !important;
        }

        .ui-datepicker-trigger {
            margin: 0px;
            height: 37px;
            padding-top: 11px;
            width: 5px;
            padding-left: 1rem;
            background-color: #428bca;
        }

        .date-style {
            width: 82%;
            display: inline-block !important;
        }

        .header-style {
            width: 60%;
            padding: 0;
            top: 10px;
        }

        .ui-datepicker-trigger {
            margin: 0px;
            height: 37px;
            padding-top: 17px;
            width: 5px;
            padding-left: 1rem;
        }

        .dataTables_processing {
            position: absolute;
            top: 2% !important;
            left: 50%;
            width: 250px;
            height: 30px;
            margin-left: -125px;
            margin-top: -15px;
            padding: 14px 0 2px 0;
            border: 0px !important;
            text-align: center;
            color: #999;
            font-size: 14px;
            background: inherit !important;
            font-weight: bold;
            color: brown;
        }

        .align-left-patient-id {
            width: 100%;
            display: block;
            text-align: left;
        }

        th {
            font-family: inherit !important;
        }

        .status{
            padding: 5px;
            font-size: 12px;
            display: inline-block;
            font-weight: bold;
            text-transform: uppercase;
        }

        .not-started-status {
            color: #f08a24;
        }

        .in-process-status {
            color: #008CBA;
        }

        .filter-div-2 {
            height: auto;
            background-color: #fff;
            width: 100%;
            padding-top: 5px;
            margin-bottom: 18px;
            padding-left: 20px;
            text-align: left;
        }

        .container {
            width: 100%;
        }

        .margin-top-50px{
            margin-top: 50px;;
        }

        .align-filter-dd{
            position: relative;
            top:-23px;
        }
		.main-page{
			box-shadow:none;
			border-radius:0px;
		}

        .width-10perc{
            width: 10%;
        }

        .width-20perc{
            width: 20%;
        }

        .align-meal-text{
            position: relative;
            top:5px;
            color: #53B6BD;
            font-size: 16px;
        }

        .patient-image {
            padding: 2px;
            border-radius: 50%;
            border: 4px solid #0F857F;
            display: inline-block;
            width: 44px;
            height: 44px;
            margin-right: 10px;
        }

        .margin-bottom-20{
            margin-bottom: 20px;
        }

        #row_wrapper .row:first-child{
            margin-left: auto !important;
            margin-right: auto !important;
        }

        .padding-table-10 td{
              padding: 12px !important;
        }

        tr:hover{
           cursor: default;
        }

        .dataTables_paginate{
            position: relative;
            right: 15px;
        }

        #errMessage{
            top:100px;
        }

        .status{
            padding: 5px;
            font-size: 12px;
            display: inline-block;
            font-weight: bold;
            text-transform: uppercase;
            cursor: default;
        }

        .next-session {
            color: #f08a24;
        }

        .next-session:hover{
            color: #f08a24;
        }

        tbody tr td:nth-child(8), tbody tr td:nth-child(9), tbody tr td:nth-child(10){
            background-color: #EDFAFA !important;
            border: 1px solid #fff !important;
            text-align: center;
        }

        .center-messages{
            display: block;
            text-align: center;
        }

        label.my-members-label{
            border: 2px solid #A4C749;
            padding-top: 9px;
            padding-left: 6px;
            padding-right: 6px;
            background-color: #fff;
            border-bottom: none;
        }

        span.my-members{
            position: relative;
            top: -10px;
            font-size: 18px;
            -webkit-transition: border 0.1s linear, box-shadow 0.1s linear;
            -moz-transition: border 0.1s linear, box-shadow 0.1s linear;
            transition: border 0.1s linear, box-shadow 0.1s linear;
        }

        tbody tr td:nth-child(7){
            text-align: left;
        }

        #txtCannedMessage{
            resize: vertical;
        }
	</style>
	
  </head>
  
<body>

    <div id="loading" class="loader">
        <div class="image-loader">
            <p class="progress-please-wait">Please wait...</p>
            <div class="progress progress-striped active">
                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
            </div>
        </div>
    </div>

	<br/><br/>

    <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>

        <div class="row max-width-1129 margin-bottom-20">
            <div class="columns medium-12">
                <div class="hs-page-heading">
                    <h2>My Members
                        <s:if test="%{#session.PROVIDER.type == 'Food Coach' || #session.PROVIDER.type == 'Coach'}">
                            <label class="right my-members-label"><input type="checkbox" id="chkMyMembers" class="large-radio margin-bottom-0" /> <span class="my-members"> Show Only My Members </span> </label>
                        </s:if>
                    </h2>
                </div>
                <div id="tableDiv" class="main-page padding-top-28 top-0 green-bordered">
                    <table id="table" class="table hs-table border-none">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th class="width-20perc">NAME</th>
                                <th>EMAIL</th>
                                <th>PHONE</th>
                                <%--<th>1:1 SESSION</th>--%>
                                <%--<th>Member Messages</th>--%>
                                <th>Last<br/>Message (sent)</th>
                                <th>Last<br/>Message (received)</th>
                                <th>Weeks<br/>Since<br/>Registration</th>
                                <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                                    <th>LAST POST</th>
                                    <th>Primary<br/>FOOD COACH</th>
                                    <th>Lead COACH</th>
                                </s:if>
                                <s:elseif test="%{#session.PROVIDER.type == 'Coach'}">
                                    <th>Lead COACH</th>
                                </s:elseif>
                                <s:else>
                                    <th>Action</th>
                                </s:else>
                            </tr>
                        </thead>
                    </table>
               </div>
            </div>
        </div>

    <!-- Member Details Dialog -->
    <div id="viewMemberDetailsDialog" class="reveal-modal small confirm-dialog">
        <div class="columns small-12 member-details-dialog">
            <h3 class="meal-summary-text remove-bold">Member Details</h3>
            <div class="grey-black-pair">
                <p class="margin-bottom-0 grey"> <img src="${pageContext.request.contextPath}/resources/css/images/email-black.png" width="20"> Email</p>
                <p id="pMemberEmail" class="black">__</p>
            </div>

            <div class="grey-black-pair">
                <p class="margin-bottom-0 grey"><img src="${pageContext.request.contextPath}/resources/css/images/phone-black.png" width="20"> Phone</p>
                <p id="pMemberPhone" class="black">__</p>
            </div>

            <div class="grey-black-pair">
                <p class="margin-bottom-0 grey"> <img src="${pageContext.request.contextPath}/resources/css/images/address-black.png" width="20"> Address</p>
                <p id="pMemberAddress" class="black">__</p>
            </div>

            <div class="grey-black-pair">
                <p class="margin-bottom-0 grey"> <img src="${pageContext.request.contextPath}/resources/css/images/home-black.png" width="20"> City</p>
                <p id="pMemberCity" class="black">__</p>
            </div>

            <div class="grey-black-pair">
                <p class="margin-bottom-0 grey"> <img class="last-image" src="${pageContext.request.contextPath}/resources/css/images/state-black.png" width="18"> State</p>
                <p id="pMemberState" class="black">__</p>
            </div>

            <div class="grey-black-pair">
                <p class="margin-bottom-0 grey"> <img class="exclude-image" src="${pageContext.request.contextPath}/resources/css/images/zip-black.png" width="20"> Zip</p>
                <p id="pMemberZip" class="black">__</p>
            </div>

        </div>
        <a class="close-reveal-modal">&#215;</a>
    </div>

    <!-- Member Details Dialog -->
    <div id="cannedMessageDialog" class="reveal-modal medium">
        <div id="errorCannedMessageDialog" class="alert-box alert alert-message-abs" style="display: none;">______________________</div>
        <div class="columns small-12 member-details-dialog">
            <h3 class="meal-summary-text remove-bold">Send Message</h3>
            <textarea id="txtCannedMessage" rows="5" maxlength="500"></textarea>
            <div class="align-right">
                <a id="btnSendCannedMessage" href="javascript:;" class="button small btn-teal radius">Send Message</a>
            </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
    </div>

		<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
	 	<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
		<script src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.mask.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fnReloadAjax.js"></script>

		<script type="text/javascript">

            var AJAXING = Object.create(Ajaxing),
                ALERT_MESSAGES = Object.create(AlertMessage).init(),
                oTable, coachType = '${session.PROVIDER.type}',
                LOCAL_STORAGE_MY_MEMBER_CHECKED_KEY = "myMemberCheckedStorage",
                SELECTED_ID = "", CANNED_MESSAGE_TEMPLATE = '${cannedMessageTemplate}';

            $.fn.dataTableExt.sErrMode = 'throw';

			$(function(){
                $('#patientsTab').addClass('selected-tab');
                loadTableData();
                $("#chkMyMembers").on("change", function(){
                    //Remember checked status in localStorage
                        if(localStorage){
                            localStorage.setItem(LOCAL_STORAGE_MY_MEMBER_CHECKED_KEY, $(this).prop('checked'));
                        }

                    oTable.fnReloadAjax("fetchMyMembersData.action?filterMyMembers="+getIsMyMembersChk());
                });

                $("#btnSendCannedMessage").off("click").on("click", function(e){
                    e.preventDefault();

                    var $error = $("#errorCannedMessageDialog").hide(),
                        $txtMessage = $("#txtCannedMessage");

                    if($txtMessage.val()){
                        var actionUrl = "sendMemberCannedMessage.action",
                                dataToSend = {
                                    "patientId": SELECTED_ID,
                                    "cannedMessageTemplate": $txtMessage.val()
                                },
                                postRequestActions = {
                                    "requestType": "POST",
                                    "successCallBack": onSentCannedMessage,
                                    "loading": $("#loading")
                                };

                        AJAXING.sendPostRequest(actionUrl, dataToSend, postRequestActions);
                    }else{
                        ALERT_MESSAGES.showAlertMessage($error, "Please add message.", ALERT_MESSAGES.ERROR);
                    }
                });
            });

            function onSentCannedMessage(data){
                var $error = $("#errMessage");

                $("#cannedMessageDialog").foundation("reveal", "close");

                if(data && data.STATUS == "SUCCESS"){
                    ALERT_MESSAGES.showAlertMessage($error, "Message sent successfully.", ALERT_MESSAGES.SUCCESS);
                    oTable.fnDraw();
                }else{
                    ALERT_MESSAGES.showAlertMessage($error, "Unable to send Message. Please try again later", ALERT_MESSAGES.ERROR);
                }
            }

            function loadTableData(){
                if(coachType == 'Food Coach') {
                    displayFoodCoachMembersData();
                } else if (coachType == 'Tech Support') {
                    displayTechSupportMembersData();
                } else{
                    displayCoachMembersData();
                }
            }

            function displayFoodCoachMembersData() {
                $("#chkMyMembers").prop("checked", true);
                //if checked Status is save in local storage
                if(localStorage && localStorage.getItem(LOCAL_STORAGE_MY_MEMBER_CHECKED_KEY)){
                    $("#chkMyMembers").prop("checked", localStorage.getItem(LOCAL_STORAGE_MY_MEMBER_CHECKED_KEY) === 'true' ? true : false);
                }

                oTable =  $('#table').dataTable({

                    "sDom" : "<'row row-auto'<'span6'l><'span6'f>r>t<'row'<'span6'i><'spanPagination'p>>",
                    "sPaginationType" : "bootstrap",
                    "oLanguage" : {
                        "sLengthMenu" : "_MENU_ records per page" // how many records you want to display per page
                    },
                    "bServerSide": true ,
                    "fnInitComplete": function (oSettings, json) {
                        //remove sorting class from td, __oz
                        $(".sorting_1").removeClass("sorting_1 sorting_1");
                    },
                    "sAjaxSource": "fetchMyMembersData.action?filterMyMembers="+getIsMyMembersChk(), // return jsonresponse with aaData json object
                    "fnServerParams": function ( aoData ) {
                    },
                    "fnDrawCallback": function( oSettings ) {
                        $(".sorting_1").removeClass("sorting_1 sorting_1");
                        removeCountryCode();
                    },
                    "bRetrieve" : true, // retrieve datatble object
                    "bPaginate" : true, // enable disable pagination
                    "bStateSave" : false, // saveState in cookie
                    "bSort" : true,
                    "bFilter":true,
                    stateSave: true,
                    "aaSorting": [[ 7, "desc" ]],
                    "bProcessing": true, // processing text in while processing
                    "aoColumns": [
                        { "mDataProp": "patientId"},
                        { "mDataProp": function(data, type, full){
                            return "<a href='dashboard.action?patientId="+data.patientId+"'>"+ data.patientName + "</a>";
                        }},
                        { "mDataProp": function(data, type, full){
                            return "<a href='dashboard.action?patientId="+data.patientId+"'>"+ data.email + "</a>";
                        }},
                        { "mDataProp": function(data, type, full){
                            return "<span class='phone-number'>"+ data.phone + "</span>";
                        }},
                        { "mDataProp": function(data, type, full){

                            var memberFirstName = (data.patientName) ? data.patientName.split(" ")[0]: undefined;

                            if(data.lastContactSentDate) {

                                var lastContactSentDate = parseInt( (data.lastContactSentDate)? data.lastContactSentDate : 0),
                                    contactSentDate = (lastContactSentDate != 0) ? HSDateUtils.formatDateFromTicks(lastContactSentDate) : "";

                                return "<div class='center-messages'><a href='coachPatientMessages.action?patientId="+data.patientId+"'> "+ contactSentDate +"</a><a href='javascript:;' data-id='"+data.patientId+"' data-name='"+memberFirstName+"' class='button btn-teal tiny radius canned-btn'>Send Message</a></div>";

                                //return "<a href='dashboard.action?patientId="+data.patientId+"&goto=coachNotes'><img src='${pageContext.request.contextPath}/resources/css/images/notes.png' width='28'/>"+ notesDate + " " + data.notesCoachName +"</a>";
                            }
                            return "<div class='center-messages'><a href='javascript:;' data-id='"+data.patientId+"' data-name='"+memberFirstName+"' class='button btn-teal tiny radius canned-btn'>Send Message</a></div>";
                        }},
                        { "mDataProp": function(data, type, full){
                            if(data.lastContactReceivedDate) {

                                var contactReceivedDate = parseInt( (data.lastContactReceivedDate)? data.lastContactReceivedDate : 0),
                                    receivedDate = (contactReceivedDate != 0) ? HSDateUtils.formatDateFromTicks(contactReceivedDate) : "";

                                return "<div class='center-messages'><a href='coachPatientMessages.action?patientId="+data.patientId+"'> "+ receivedDate +"</a></div>";

                                //return "<a href='dashboard.action?patientId="+data.patientId+"&goto=coachNotes'><img src='${pageContext.request.contextPath}/resources/css/images/notes.png' width='28'/>"+ notesDate + " " + data.notesCoachName +"</a>";
                            }
                            return "<span class='center-messages'>N/A</span>";
                        }},
                        { "mDataProp": function(data, type, full){
                            return "<span class='center-messages'>"+ data.registeredWeeks + "</span>";
                        }},
                        { "mDataProp": function(data, type, full){
                            var logTimeMillis = parseInt( (data.lastLogTimeMillis)? data.lastLogTimeMillis : 0),
                                logTime = (logTimeMillis != 0) ? HSDateUtils.formatDateFromTicks(logTimeMillis) : "N/A",
                                logType = (data.lastLogType)? data.lastLogType : "N/A";

                            if(logTime == "N/A" && logType == "N/A") {
                                return "<span class='center-messages'>N/A</span>";
                            }

                            return "<span class='center-messages'>"+ logTime + " / "+ logType + "</span>";
                        }},
                        { "mDataProp": function(data, type, full){
                            if(data.primaryCoachName) {
                                return data.primaryCoachName;
                            }
                            return "<a href='javascript:;' data-patid='"+ data.patientId +"' class='button radius tiny btn-teal margin-tb-0 assign-to-me'>Assign to me</a>";
                        },"bSortable": false},
                        { "mDataProp": function(data, type, full){
                            if(data.leadCoachName) {
                                return "<span class='center-messages'>"+data.leadCoachName+"</span>";
                            }
                            return "<span class='center-messages'>N/A</span>";
                        },"bSortable": false}
                    ]
                });

                fixedPaginationStyling();
                $(".sorting_1").removeClass("sorting_1 sorting_1");
                removeCountryCode();
            }

            function displayCoachMembersData(jObject) {
                $("#chkMyMembers").prop("checked", true);
                //if checked Status is save in local storage
                if(localStorage && localStorage.getItem(LOCAL_STORAGE_MY_MEMBER_CHECKED_KEY)){
                    $("#chkMyMembers").prop("checked", localStorage.getItem(LOCAL_STORAGE_MY_MEMBER_CHECKED_KEY) === 'true' ? true : false);
                }

                oTable =  $('#table').dataTable({

                    "sDom" : "<'row row-auto'<'span6'l><'span6'f>r>t<'row'<'span6'i><'spanPagination'p>>",
                    "sPaginationType" : "bootstrap",
                    "oLanguage" : {
                        "sLengthMenu" : "_MENU_ records per page" // how many records you want to display per page
                    },
                    "bServerSide": true ,
                    "fnInitComplete": function (oSettings, json) {
                        //remove sorting class from td, __oz
                        $(".sorting_1").removeClass("sorting_1 sorting_1");
                    },
                    "sAjaxSource": "fetchMyMembersData.action?filterMyMembers="+getIsMyMembersChk(), // return jsonresponse with aaData json object
                    "fnServerParams": function ( aoData ) {
                    },
                    "fnDrawCallback": function( oSettings ) {
                        $(".sorting_1").removeClass("sorting_1 sorting_1");
                        removeCountryCode();
                    },
                    "bRetrieve" : true, // retrieve datatble object
                    "bPaginate" : true, // enable disable pagination
                    "bStateSave" : false, // saveState in cookie
                    "bSort" : true,
                    "bFilter":true,
                    stateSave: true,
                    "aaSorting": [[ 4, "desc" ]],
                    "bProcessing": true, // processing text in while processing
                    "aoColumns": [
                        { "mDataProp": "patientId"},
                        { "mDataProp": function(data, type, full){
                            return "<a href='dashboard.action?patientId="+data.patientId+"'>"+ data.patientName + "</a>";
                        }},
                        { "mDataProp": function(data, type, full){
                            return "<a href='dashboard.action?patientId="+data.patientId+"'>"+ data.email + "</a>";
                        }},
                        { "mDataProp": function(data, type, full){
                            return "<span class='phone-number'>"+ data.phone + "</span>";
                        }},
                        { "mDataProp": function(data, type, full){

                            var memberFirstName = (data.patientName) ? data.patientName.split(" ")[0]: undefined;

                            if(data.lastContactSentDate) {

                                var lastContactSentDate = parseInt( (data.lastContactSentDate)? data.lastContactSentDate : 0),
                                        contactSentDate = (lastContactSentDate != 0) ? HSDateUtils.formatDateFromTicks(lastContactSentDate) : "";

                                return "<div class='center-messages'><a href='coachPatientMessages.action?patientId="+data.patientId+"'> "+ contactSentDate +"</a><a href='javascript:;' data-id='"+data.patientId+"' data-name='"+memberFirstName+"' class='button btn-teal tiny radius canned-btn'>Send Message</a></div>";

                                //return "<a href='dashboard.action?patientId="+data.patientId+"&goto=coachNotes'><img src='${pageContext.request.contextPath}/resources/css/images/notes.png' width='28'/>"+ notesDate + " " + data.notesCoachName +"</a>";
                            }
                            return "<div class='center-messages'><a href='javascript:;' data-id='"+data.patientId+"' data-name='"+memberFirstName+"' class='button btn-teal tiny radius canned-btn'>Send Message</a></div>";
                        }},
                        { "mDataProp": function(data, type, full){
                            if(data.lastContactReceivedDate) {

                                var contactReceivedDate = parseInt( (data.lastContactReceivedDate)? data.lastContactReceivedDate : 0),
                                        receivedDate = (contactReceivedDate != 0) ? HSDateUtils.formatDateFromTicks(contactReceivedDate) : "";

                                return "<div class='center-messages'><a href='coachPatientMessages.action?patientId="+data.patientId+"'> "+ receivedDate +"</a></div>";

                                //return "<a href='dashboard.action?patientId="+data.patientId+"&goto=coachNotes'><img src='${pageContext.request.contextPath}/resources/css/images/notes.png' width='28'/>"+ notesDate + " " + data.notesCoachName +"</a>";
                            }
                            return "<span class='center-messages'>N/A</span>";
                        }},
                        { "mDataProp": function(data, type, full){
                            return "<span class='center-messages'>"+ data.registeredWeeks + "</span>";
                        }},
                        { "mDataProp": function(data, type, full){
                            if(data.leadCoachName) {
                                return "<span class='center-messages'>"+data.leadCoachName+"</span>";
                            }
                            return "<span class='center-messages'>N/A</span>";
                        },"bSortable": false}
                    ]
                });

                fixedPaginationStyling();
                $(".sorting_1").removeClass("sorting_1 sorting_1");
                removeCountryCode();
            }

            function getIsMyMembersChk(){
                return ( ($("#chkMyMembers").is(":checked") && coachType == "Food Coach" ) ? "filterByPrimaryFoodCoach" : ($("#chkMyMembers").is(":checked") && coachType == "Coach" ) ? "filterByLeadCoach" : "");
            }

            function displayTechSupportMembersData(jObject) {
                oTable =  $('#table').dataTable({

                    "sDom" : "<'row row-auto'<'span6'l><'span6'f>r>t<'row'<'span6'i><'spanPagination'p>>",
                    "sPaginationType" : "bootstrap",
                    "oLanguage" : {
                        "sLengthMenu" : "_MENU_ records per page" // how many records you want to display per page
                    },
                    "bServerSide": true ,
                    "fnInitComplete": function (oSettings, json) {
                        //remove sorting class from td, __oz
                        $(".sorting_1").removeClass("sorting_1 sorting_1");
                    },
                    "sAjaxSource": "fetchMyMembersData.action?filterMyMembers="+getIsMyMembersChk(), // return jsonresponse with aaData json object
                    "fnServerParams": function ( aoData ) {
                    },
                    "fnDrawCallback": function( oSettings ) {
                        $(".sorting_1").removeClass("sorting_1 sorting_1");
                        removeCountryCode();
                    },
                    "bRetrieve" : true, // retrieve datatble object
                    "bPaginate" : true, // enable disable pagination
                    "bStateSave" : false, // saveState in cookie
                    "bSort" : true,
                    "bFilter":true,
                    stateSave: true,
                    "aaSorting": [[ 4, "desc" ]],
                    "bProcessing": true, // processing text in while processing
                    "aoColumns": [
                        { "mDataProp": "patientId"},
                        { "mDataProp": function(data, type, full){
                            return "<a href='dashboard.action?patientId="+data.patientId+"'>"+ data.patientName + "</a>";
                        }},
                        { "mDataProp": function(data, type, full){
                            return "<a href='dashboard.action?patientId="+data.patientId+"'>"+ data.email + "</a>";
                        }},
                        { "mDataProp": function(data, type, full){
                            return "<span class='phone-number'>"+ data.phone + "</span>";
                        }},
                        { "mDataProp": function(data, type, full){

                            var memberFirstName = (data.patientName) ? data.patientName.split(" ")[0]: undefined;

                            if(data.lastContactSentDate) {

                                var lastContactSentDate = parseInt( (data.lastContactSentDate)? data.lastContactSentDate : 0),
                                        contactSentDate = (lastContactSentDate != 0) ? HSDateUtils.formatDateFromTicks(lastContactSentDate) : "";

                                return "<div class='center-messages'><a href='coachPatientMessages.action?patientId="+data.patientId+"'> "+ contactSentDate +"</a><a href='javascript:;' data-id='"+data.patientId+"' data-name='"+memberFirstName+"' class='button btn-teal tiny radius canned-btn'>Send Message</a></div>";

                                //return "<a href='dashboard.action?patientId="+data.patientId+"&goto=coachNotes'><img src='${pageContext.request.contextPath}/resources/css/images/notes.png' width='28'/>"+ notesDate + " " + data.notesCoachName +"</a>";
                            }
                            return "<div class='center-messages'><a href='javascript:;' data-id='"+data.patientId+"' data-name='"+memberFirstName+"' class='button btn-teal tiny radius canned-btn'>Send Message</a></div>";
                        }},
                        { "mDataProp": function(data, type, full){
                            if(data.lastContactReceivedDate) {

                                var contactReceivedDate = parseInt( (data.lastContactReceivedDate)? data.lastContactReceivedDate : 0),
                                        receivedDate = (contactReceivedDate != 0) ? HSDateUtils.formatDateFromTicks(contactReceivedDate) : "";

                                return "<div class='center-messages'><a href='coachPatientMessages.action?patientId="+data.patientId+"'> "+ receivedDate +"</a></div>";

                                //return "<a href='dashboard.action?patientId="+data.patientId+"&goto=coachNotes'><img src='${pageContext.request.contextPath}/resources/css/images/notes.png' width='28'/>"+ notesDate + " " + data.notesCoachName +"</a>";
                            }
                            return "<span class='center-messages'>N/A</span>";
                        }},
                        { "mDataProp": function(data, type, full){
                            return "<span class='center-messages'>"+ data.registeredWeeks + "</span>";
                        }},
                        { "mDataProp": function(data, type, full){

                            var detailsObject = {
                                "email": nullSafeJS(data.email),
                                "phone": nullSafeJS(data.phone),
                                "address": nullSafeJS(data.address),
                                "city" : nullSafeJS(data.city),
                                "state": nullSafeJS(data.state),
                                "zip": nullSafeJS(data.zip)
                            };

                            return "<a href='javascript:;' class='view-details' data-details='"+JSON.stringify(detailsObject)+"'><img class='table-images-icon view' src='${pageContext.request.contextPath}/resources/css/images/view-new.png' title='View Details' /></a>" ;

                        },"bSortable": false}
                    ]
                });

                fixedPaginationStyling();
                $(".sorting_1").removeClass("sorting_1 sorting_1");
                removeCountryCode();
            }

            function fixedPaginationStyling(){
                var $ul = $(".dataTables_paginate ul");
                $ul.each(function(){
                    var $this = $(this);
                    if($this && !$this.hasClass("pagination")){
                        $this.addClass("pagination");
                    }
                });
            }
            function removeCountryCode(){
                $(".phone-number").each(function(index, phone){
                    var $phoneLabel = $(phone),
                        phoneText = $phoneLabel.text();

                    //written in common.js file
                    formatPhoneNumber($phoneLabel, phoneText);
                });

                $(".assign-to-me").off("click").on("click", function(e){
                    e.preventDefault();
                    var actionUrl = "assignMeAsPrimaryFoodCoach.action",
                        dataToSend = {
                            "patientId": $(this).data("patid")
                        },
                        postRequestActions = {
                            "requestType": "POST",
                            "successCallBack": onAssignPrimaryFoodCoach,
                            "loading": $("#loading")
                        };

                    AJAXING.sendPostRequest(actionUrl, dataToSend, postRequestActions);
                });

                $(".view-details").off("click").on("click", function(e){
                    e.preventDefault();
                        var $this = $(this),
                            detailsObject = $this.data("details");
                    if(detailsObject){
                        $("#pMemberEmail").text(detailsObject.email);
                        $("#pMemberAddress").text(detailsObject.address);
                        $("#pMemberCity").text(detailsObject.city);
                        $("#pMemberState").text(detailsObject.state);

                        formatZipCode($("#pMemberZip"), detailsObject.zip);
                        formatPhoneNumber($("#pMemberPhone"), detailsObject.phone);
                    }
                    $("#viewMemberDetailsDialog").foundation("reveal", "open");
                });

                $(".canned-btn").off("click").on("click", function(e){
                    e.preventDefault();
                    var $txtCannedMessage = $("#txtCannedMessage"),
                        replaceChunk = "$$fName$$";

                    SELECTED_ID = $(this).data("id");
                    if($(this).data("name")){
                        $txtCannedMessage.val(CANNED_MESSAGE_TEMPLATE.replace(replaceChunk, $(this).data("name")));
                    }else{
                        $txtCannedMessage.val(CANNED_MESSAGE_TEMPLATE.replace(replaceChunk, "____"));
                    }
                    $("#cannedMessageDialog").foundation("reveal", "open");
                });
            }

            function onAssignPrimaryFoodCoach(data){
                var $error = $("#errMessage");

                if(data && data.STATUS == "SUCCESS"){
                    ALERT_MESSAGES.showAlertMessage($error, "Assigned to me successfully.", ALERT_MESSAGES.SUCCESS);
                    oTable.fnDraw();
                }else{
                    ALERT_MESSAGES.showAlertMessage($error, "Unable to assign, please try again later.", ALERT_MESSAGES.ERROR);
                }
            }
		</script>
</body>
</html>