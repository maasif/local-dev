<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>
    <title>My Members</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/facility.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/hs-datatable.css" />

    <style type="text/css">

        .max-width-1129{
            max-width: 1260px !important;
        }

        .main-page{
            box-shadow:none;
            border-radius:0px;
        }

        tbody tr:hover{
            cursor: default;
        }
        .width-10perc{
            width: 10%;
        }

        .width-20perc{
            width: 20%;
        }

        .align-meal-text{
            position: relative;
            top:5px;
            color: #53B6BD;
            font-size: 16px;
        }

        .patient-image {
            padding: 2px;
            border-radius: 50%;
            border: 4px solid #0F857F;
            display: inline-block;
            width: 44px;
            height: 44px;
            margin-right: 10px;
        }

        .margin-bottom-20{
            margin-bottom: 20px;
        }

        #row_wrapper .row:first-child{
            margin-left: auto !important;
            margin-right: auto !important;
        }

        .padding-table-10 td{
            padding: 12px !important;
        }

        tr:hover{
            cursor: pointer;
        }

        .dataTables_paginate{
            position: relative;
            right: 15px;
        }

        .status{
            color: #fff;
            padding: 5px;
            min-width: 120px;
            font-size: 12px;
            border-radius: 2px;
            display: inline-block;
            font-weight: bold;
            text-transform: uppercase;
        }

        .denied-status{
            color: #f04124;
        }

        .requested-status{
            color: #f08a24;
        }

        .approved-status{
            color: #43AC6A;
        }

        .invited-status{
            color: #008CBA;
        }

        .meal-summary-text{
            color: #8AB329;
            font-weight: normal;
        }

        .margin-top-0{
            margin-top: 0 !important;
        }

        .remove-member{
            position: absolute;
            left: -20px;
            bottom: -3px;
        }

        .break-word{
            word-break: break-word;
        }

        tr td:first-child{
            position: relative;
        }
    </style>

    <script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>

</head>

<body ng-app="ProspectiveMembersModule">
<!-- BreadCrumb -->
<div class="breadcrumb-green">
    <div class="max-width-1129">
        <ul class="breadcrumbs">
            <li>
                <a href="memberRecruitment.action">Add Member</a>
            </li>
            <li class="current">Prospective Members</li>
        </ul>
    </div>
</div>
<br/>
<div id="loading" class="loader position-fixed">
    <div class="image-loader">
        <p class="progress-please-wait">Please wait...</p>
        <div class="progress progress-striped active">
            <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            </div>
        </div>
        <p id="progressTxt" class="progress-text">We're working</p>
    </div>
</div>

<div class="row max-width-1129 margin-bottom-20" ng-controller="ProspectiveMembersController">
    <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>

    <input type="hidden" id="txtHiddenPatientId" />
    <div class="row row-auto max-width-1129">
        <div class="medium-12 columns">
            <div class="border-bt-heading padding-left-0 hs-page-heading">
                <h2>Prospective Members</h2>
            </div>
        </div>
    </div>
    <div class="max-width-1129 bordered-white-bg green-bordered">

        <table id="table" class="table hs-table border-none">
            <thead>
            <tr>
                <th>Id</th>
                <th>MRN</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Requested/<br/>Invited</th>
                <th>Date Requested/<br/>Invited</th>
                <th>Consent Form Accepted?</th>
                <%--<th>CGM/<br/>Insulin</th>--%>
                <th>Lead Coach</th>
            </tr>
            </thead>
        </table>
    </div>

    <!-- Provider/Patient Mapping Dialog -->
    <div id="providerListDialog" class="reveal-modal large" data-reveal>
        <h3 class="meal-summary-text"> <img src="${pageContext.request.contextPath}/resources/css/images/provider.png" width="32" /> All Coaches </h3>
        <div id="dialogArea" class="dialog-area hs-table">
            <table id="tableProviders" class="table datatable border-none">
                <thead>
                <tr>
                    <th>Coach ID</th>
                    <th>Coach Name</th>
                    <th>Type</th>
                    <th>Assigned Facility</th>
                    <th>Action</th>
                   <%-- <th>Primary Food Coach</th>--%>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="pro in providerList">
                    <td>
                        {{pro.providerId}}
                    </td>
                    <td>
                        {{pro.lastName}}, {{pro.firstName}}
                    </td>
                    <td>
                        {{pro.type}}
                    </td>
                    <td>
                        {{pro.facilityName}}
                    </td>
                    <td>
                        <input type="checkbox" ng-model="pro.leadCoachId" ng-checked="{{ pro.providerId == pro.leadCoachId ? 'true': 'false' }}" ng-change="saveLeadCoach(pro.providerId);" class="checkbox-large margin-bottom-0" />
                    </td>
                    <%--<td>
                        <input type="checkbox" ng-model="pro.primaryFoodCoachId" ng-checked="{{ pro.providerId == pro.primaryFoodCoachId ? 'true': 'false' }}" ng-change="savePrimaryFoodCoach(pro.providerId);" class="checkbox-large margin-bottom-0" />
                    </td>--%>
                </tr>
                <tr ng-show="providerList == undefined || providerList.length == 0 || providerList == null">
                    <td colspan="5" class="align-center">No coaches found.</td>
                </tr>
                </tbody>
            </table>
        </div>
        <a id="closeProviderListDialog" class="close-reveal-modal">&#215;</a>

        <a href="javascript:;" class="button small radius hs-btns secondary right margin-bottom-0" onclick="$('#closeProviderListDialog').click();">Close</a>
    </div>

    <!-- MRN Dialog -->
    <div id="mrnDialog" class="reveal-modal small">
        <div class="columns small-12">
            <div id="mrnDialogError" class="alert-box alert alert-message-abs" style="display: none;">______________________</div>
            <h3 class="meal-summary-text">Member's MRN</h3>
            <label class="inline margin-bottom-0">
                Enter MRN:
                <input type="text" id="txtMrn" maxlength="16" />
            </label>
            <label class="inline margin-bottom-0">
                Confirm MRN:
                <input type="text" id="txtConfirmMrn" maxlength="16" />
            </label>
            <div class="align-right">
                <a href="javascript:;" id="btnAddMrnDialog" class="button radius btn-teal">Save</a>
            </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
    </div>

</div>

<div id="confirmationRemoveMemberDialog" class="reveal-modal small" data-reveal>
    <h3 class="meal-summary-text">Remove Member</h3>
    <p class="lead dialog-para">Are you sure you want to remove this Member?</p>
    <a id="closeConfirmDialog" class="close-reveal-modal">&#215;</a>
    <div class="align-right">
        <a href="javascript:;" id="btnConfirmDelete" class="button radius btn-teal margin-top-10 margin-bottom-0">Yes</a>
        <a href="javascript:;" class="button secondary radius margin-top-10 margin-bottom-0" onclick="$('#closeConfirmDialog').click(); return false;">No</a>
    </div>
</div>

<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.mask.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
<script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>

<script type="text/javascript">

    var AJAXING = Object.create(Ajaxing),
        ALERT_MESSAGING = Object.create(AlertMessage).init();
        AJAX_ALERT_MESSAGE = "Approval email has been sent to the member.",
        UUID = "",
        SHOULD_CALL_SAVE = false,
        PROVIDER_LIST = ("${providerListString}") ? $.parseJSON("${providerListString}") : undefined,
        PATIENT_LIST = undefined,
        SELECTED_ROW_INDEX = -1;

    $.fn.dataTableExt.sErrMode = 'throw';

    $(function(){
        $("#prospectiveMembersTab").addClass("selected-tab").parents("ul").prev("li").addClass("selected-tab");;
        $(document).foundation();
        initPageComponents();
    });

    function initPageComponents(){

        var oTable =  $('#table').dataTable({

            "sDom" : "<'row row-auto'<'span6'l><'span6'f>r>t<'row'<'span6'i><'spanPagination'p>>",
            "sPaginationType" : "bootstrap",
            "oLanguage" : {
                "sLengthMenu" : "_MENU_ records per page" // how many records you want to display per page
            },
            "bServerSide": true ,
            "fnInitComplete": function (oSettings, json) {
                //remove sorting class from td, __oz
                $(".sorting_1").removeClass("sorting_1 sorting_1");
            },
            "sAjaxSource": "fetchProspectiveMembersData.action", // return jsonresponse with aaData json object
            "fnServerParams": function ( aoData ) {
            },
            "fnDrawCallback": function( oSettings ) {
                $(".sorting_1").removeClass("sorting_1 sorting_1");
                bindOnChangeOfButtons();
            },
            "bRetrieve" : true, // retrieve datatable object
            "bPaginate" : true, // enable disable pagination
            "bStateSave" : false, // saveState in cookie
            "bSort" : true,
            "bFilter":true,
            stateSave: true,
            "aaSorting": [[ 0, "desc" ]],
            "bProcessing": true, // processing text in while processing
            "aoColumns": [
                { "mDataProp": function(data, type, full){
                    return data.patientId + "<a href='javascript:;' class='remove-member label error' data-id='"+data.uuid+"'/>Remove</a>";
                }},
                { "mDataProp": function(data, type, full){
                    if(data.mrn){
                        return "<a href='javascript:;' class='mrn-change' data-id='"+data.uuid+"' data-mrn='"+data.mrn+"'>"+data.mrn+"</a>";
                    }
                    return "<a href='javascript:;' class='mrn-change' data-id='"+data.uuid+"' data-mrn='"+data.mrn+"'>Add</a>";
                }},
                { "mDataProp": function(data, type, full){
                    if(data.firstName && data.lastName){
                        return data.firstName + " " + data.lastName;
                    }

                    return "";
                }},
                { "mDataProp": function(data, type, full){
                    return "<span class='word-break'>" + data.email + "</span>";
                }},
                { "mDataProp": function(data, type, full){
                    return "<span class='phone-number'>" + ((data.phone) ? data.phone : "") + "</span>";
                }},
                { "mDataProp": function(data, type, full){
                    if(data.invitedDate){
                        return "<span class='status invited-status'>Invited</span>";
                    }
                    return "<span class='status requested-status'>Requested</span>";
                },"bSortable": false},
                { "mDataProp": function(data, type, full){
                    if(data.invitedDate){
                        return HSDateUtils.formatDateFromTicks(parseFloat(data.invitedDate));
                    }
                    return (data.requestAnInvitedDate)? HSDateUtils.formatDateFromTicks(parseFloat(data.requestAnInvitedDate)) : "";
                }},
                { "mDataProp": function(data, type, full){
                    if(data.isConsentAccepted){
                        return "Yes";
                    }
                    return "No"
                },"bSortable": false},
                { "mDataProp": function(data, type, full){

                    if(data.isApproved == "0") {
                        return "<a href='memberRecruitment.action?uuid=" + data.uuid + "' class='button radius tiny hs-btns btn-teal margin-bottom-10 margin-top-0' data-leadcoach='"+data.facilityLeadCoach+"' data-facilityid='"+data.facilityId+"'>Add Member</a>" +
                                "<a href='javascript:;' data-accept='2' data-id='"+data.uuid+"' class='button radius tiny hs-btns btn-grey margin-tb-0 deny-btn'>Deny</a>";
                    }else if (data.isApproved == "2"){
                        return "<span class='status denied-status'>Denied</span>";
                    }

                    return "<span class='status approved-status'>Assigned</span>";
                },"bSortable": false}
            ]
        });

        //bind opened/closed events of dialog
        $("#mrnDialog").on({
            opened: function () {
                $("#txtMrn").focus();
            },
            closed: function () {

            }
        });

        bindOnChangeOfButtons();
        fixedPaginationStyling();

        $("#btnAddMrnDialog").on("click", function(e){
            e.preventDefault();

            var $txtMRN = $("#txtMrn"),
                    $txtMRNConfirm = $("#txtConfirmMrn"),
                    mrnText = $.trim($txtMRN.val()),
                    mrnConfirmText = $.trim($txtMRNConfirm.val()),
                    $error = $("#mrnDialogError");

            if(SHOULD_CALL_SAVE || mrnText.length > 0){
                if( (mrnText == mrnConfirmText) &&  mrnConfirmText.length > 0){
                    //send via ajax call
                    var dataToSend = {"uuid": UUID, "mrn": mrnText},
                            postRequestActions = {
                                "loading": $("#loading"),
                                "error": $error,
                                "successCallBack": onMRNSaved
                            };

                    AJAXING.sendPostRequest("saveMrn.action", dataToSend, postRequestActions);
                }else{
                    ALERT_MESSAGING.showAlertMessage($error, "Confirm MRN should match", ALERT_MESSAGING.ERROR);
                }
            }else{
                $txtMRN.add($txtMRNConfirm).val("");
                $("#mrnDialog").foundation("reveal", "close");
            }
        });

        $("#btnConfirmDelete").on("click", function(e){
            e.preventDefault();

            //send via ajax call
            var dataToSend = {"uuid": UUID},
                postRequestActions = {
                    "loading": $("#loading"),
                    "error": $("#errMessage"),
                    "successCallBack": onRemoveMember
                };

            AJAXING.sendPostRequest("removeMember.action", dataToSend, postRequestActions);
        });
    }

    function openProviderDialog(patientId){
        var $scope = angular.element("#table").scope();
        $scope.patientId = patientId;
        $scope.refreshProviderList(patientId);
        $("#providerListDialog").foundation("reveal", "open");
    }

    function fixedPaginationStyling(){
        var $ul = $(".dataTables_paginate ul");
        $ul.each(function(){
            var $this = $(this);
            if($this && !$this.hasClass("pagination")){
                $this.addClass("pagination");
            }
        });
    }

    function bindOnChangeOfButtons(){

        $(".phone-number").each(function(index, phone){

            var $phoneLabel = $(phone),
                phoneText = $phoneLabel.text();

            formatPhoneNumber($phoneLabel, phoneText);
        });

        $(".approve-btn,.deny-btn").off("click").on("click", function(e){

            e.preventDefault();

            var $this = $(this),
                uuid = $this.data("id"),
                approvedDenied = $this.data("accept"),
                $error = $("#errMessage");

            AJAX_ALERT_MESSAGE = "Member request is approved.";
            SELECTED_ROW_INDEX = $this.closest("tr").index();

            if(approvedDenied == "2" || approvedDenied == 2){
                AJAX_ALERT_MESSAGE = "Member request has been denied.";
            }

            //send via ajax call
            var dataToSend = {"uuid": uuid, "approvedDenied": approvedDenied},
                postRequestActions = {
                    "loading": $("#loading"),
                    "error": $error,
                    "successCallBack": onApproveInvite
                };

            AJAXING.sendPostRequest("approveInvite.action", dataToSend, postRequestActions);
        });

        $(".mrn-change").off("click").on("click", function(){
            var $this = $(this),
                mrn = $this.data("mrn"),
                $txt = $("#txtMrn");

            UUID = $this.data("id");
            SELECTED_ROW_INDEX = $this.closest("tr").index();

            $txt.val("");
            SHOULD_CALL_SAVE = false;

            if(mrn == 0 || mrn == "0"){
                mrn = mrn.toString()
            }

            if(mrn || mrn.length > 0){
                $txt.val(mrn);
                SHOULD_CALL_SAVE = true;
            }

            $("#txtConfirmMrn").val("");
            $("#mrnDialog").foundation("reveal", "open");
        });

        $(".remove-member").off("click").on("click", function(){
            var $this = $(this);
            UUID = $this.data("id");
            SELECTED_ROW_INDEX = $this.closest("tr").index();
            $("#confirmationRemoveMemberDialog").foundation("reveal", "open");
        });

        $(".add-member").off("click").on("click", function(e){
            e.preventDefault();

            var $this = $(this),
                href = $this.attr("href"),
                forAdmin = {
                    leadCoach : $this.data("leadcoach"),
                    facilityId : $this.data("facilityid"),
                    userType: "Coach"
                };

            //written in header.jsp
            checkFacilityLeadCoach(href, forAdmin);
        });
    }

    function onApproveInvite(data){
        var $error = $("#errMessage");

        if(data.STATUS == "SUCCESS"){
            if(data.DATA == 'true' || data.DATA == true){
                AJAX_ALERT_MESSAGE = "Mobile app download link has been sent to member email address.";
            }
            ALERT_MESSAGING.showAlertMessage($error, AJAX_ALERT_MESSAGE, ALERT_MESSAGING.SUCCESS);

            setTimeout(function(){
                location.reload(true);
            }, 500);

        }else{
            AJAXING.showErrorOrRedirect(data, $error, "Unable approve/denied request. Please try again later.");
        }
    }

    function onMRNSaved(data){
        var $error = $("#errMessage"),
        $dialogError = $("#mrnDialogError");
        if(data.STATUS == "SUCCESS"){
            ALERT_MESSAGING.showAlertMessage($error, "Member's MRN saved successfully.", ALERT_MESSAGING.SUCCESS);
            updateTableRowForMRN();
            $("#mrnDialog").foundation("reveal", "close");
        }else{
            if(data.REASON == "MRN already assigned"){
                ALERT_MESSAGING.showAlertMessage($dialogError, "MRN already assigned.", ALERT_MESSAGING.ERROR);
            } else {
                AJAXING.showErrorOrRedirect(data, $error, "Unable to save Member's MRN. Please try again later.");
                $("#mrnDialog").foundation("reveal", "close");
            }
        }
    }

    function onRemoveMember(data){
        var $error = $("#errMessage");
        $("#confirmationRemoveMemberDialog").foundation("reveal", "close");
        if(data.STATUS == "SUCCESS"){
            ALERT_MESSAGING.showAlertMessage($error, "Member removed successfully.", ALERT_MESSAGING.SUCCESS);
            $("tbody tr").eq(SELECTED_ROW_INDEX).remove().fadeOut(500);
        }else{
            AJAXING.showErrorOrRedirect(data, $error, "Unable to remove Member. Please try again later.");
        }
    }

    function updateTableRowForMRN(){
        var $tr = $("tbody tr").eq(SELECTED_ROW_INDEX),
                $a = $tr.find("a.mrn-change"),
                $txtMrn = $("#txtMrn");

        if($a){
            var mrnValue = $txtMrn.val();
            if(!mrnValue){
                mrnValue = "Add";
            }
            $a.data("mrn", $txtMrn.val()).text(mrnValue);
        }
    }

</script>

<script src="${pageContext.request.contextPath}/resources/js/controllers/prospective_members_controller.js?v=0.1"></script>

</body>
</html>