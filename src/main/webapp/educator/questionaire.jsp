<!DOCTYPE html>
<html>
<head>
    <!--<meta charset="utf-8" />-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />    
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/questionaire.style.css" />    
</head>
<body>
    
    <div id="profilePage" class="column medium-12">
            <a class="close-reveal-modal">&#215;</a>
            <h2>Patient Diet Profile</h2>
            <div class="content over-flow">
                <ul class="side-nav small-block-grid-1 medium-block-grid-1 large-block-grid-2">
                    <li>
                        <a href="#">Soda pop/Cola, Iced Tea, Energy Drinks; 'Which do you usually choose?'</a>
                        <div class="options">
                            <ul class="inline-list">
                                <li>
                                    <label class="inline"><input type="radio" checked name="soda" />
                                    <span class="text font-bold">Regular</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="soda" />  <span class="text">Diet</span></label>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#">Milk; 'Which do you usually choose?'</a>
                        <div class="options">
                            <ul class="inline-list">
                                <li>
                                    <label class="inline"><input type="radio" name="milk-choose-1" />
                                    <span class="text">Whole</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" checked name="milk-choose-1" />
                                    <span class="text font-bold">Low-fat</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="milk-choose-1" />
                                    <span class="text">Non-fat</span></label>
                                </li>

                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#">Coffee/Tea</a>
                        <div class="options">
                            <h6>Sugar (not SweetNLow, Splenda, etc.) teaspoons added:</h6>
                            <ul class="inline-list">
                                <li>
                                    <label class="inline"><input type="radio" checked name="sugar-1" />
                                    <span class="text font-bold">0</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="sugar-1" />
                                    <span class="text">1</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="sugar-1" />
                                    <span class="text">2</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="sugar-1" />
                                    <span class="text">3</span></label>
                                </li>
                            </ul>
                        </div>
                        <div class="options">
                            <h6>Creamer, unflavored, (half-and-half, whole milk, cream) teaspoons added:</h6>
                            <ul class="inline-list">
                                <li>
                                    <label class="inline"><input type="radio" name="creamer" />
                                    <span class="text">0</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" checked name="creamer" />
                                    <span class="text font-bold">1</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="creamer" />
                                    <span class="text">2</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="creamer" />
                                    <span class="text">3</span></label>
                                </li>
                            </ul>
                        </div>
                        <div class="options">
                            <h6>Creamer, flavored, i.e. hazelnut, vanilla, teaspoons added:</h6>
                            <ul class="inline-list">
                                <li>
                                    <label class="inline"><input type="radio" name="creamer-1" />
                                    <span class="text">0</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="creamer-1" />
                                    <span class="text">1</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" checked name="creamer-1" />
                                    <span class="text font-bold">2</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="creamer-1" />
                                    <span class="text">3</span></label>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#">Latte</a>
                        <div class="options">
                            <h6>Sugar (not SweetNLow, Splenda, etc.) teaspoons added:</h6>
                            <ul class="inline-list">
                                <li>
                                    <label class="inline"><input type="radio" name="sugar" />
                                    <span class="text">0</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="sugar" />
                                    <span class="text">1</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" checked name="sugar" />
                                    <span class="text font-bold">2</span></label>
                                </li>
                            </ul>
                        </div>
                        <div class="options">
                            <h6>Flavorings, e.g., vanilla or almond (not sugar-free), pumps added:</h6>
                            <ul class="inline-list">
                                <li>
                                    <label class="inline"><input type="radio" name="flavorings" />
                                    <span class="text">Whatever normally comes with the drink order</span></label>
                                </li>
                             </ul>
                             <ul class="inline-list">
                                <li>
                                    <label class="inline"><input type="radio" name="flavorings" />
                                    <span class="text">0</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" checked name="flavorings" />
                                    <span class="text font-bold">1</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="flavorings" />
                                    <span class="text">2</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="flavorings" />
                                    <span class="text">3</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="flavorings" />
                                    <span class="text">4</span></label>
                                </li>
                            </ul>
                        </div>
                        <div class="options">
                            <h6>Milk type:</h6>
                            <ul class="inline-list">
                                <li>
                                    <label class="inline"><input type="radio" name="milk-choose-2" />
                                    <span class="text">Whole</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" checked name="milk-choose-2" />
                                    <span class="text font-bold">Low-fat</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="milk-choose-2" />
                                    <span class="text">Non-fat</span></label>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#">Frappuccino/Mochaccino</a>
                        <div class="options">
                            <h6>Milk type:</h6>
                            <ul class="inline-list">
                                <li>
                                    <label class="inline"><input type="radio" checked name="milk-choose-3" />
                                    <span class="text font-bold">Whole</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="milk-choose-3" />
                                    <span class="text">Low-fat</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="milk-choose-3" />
                                    <span class="text">Non-fat</span></label>
                                </li>
                            </ul>
                        </div>
                        <div class="options">
                            <h6>Size:</h6>
                            <ul class="inline-list">
                                <li>
                                    <label class="inline"><input type="radio" checked name="cino-size" />
                                    <span class="text font-bold">Tall</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="cino-size" />
                                    <span class="text">12oz-grande</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="cino-size" />
                                    <span class="text">16oz-venti</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="cino-size" />
                                    <span class="text">24oz</span></label>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#">Fiber supplements, e.g., Benefiber, Metamucil; 'Tablespoons or tablets/day'</a>
                        <div class="options">
                            <ul class="inline-list">
                                <li>
                                    <label class="inline"><input type="radio" name="supplements" />
                                    <span class="text">1</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="supplements" />
                                    <span class="text">2</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="supplements" />
                                    <span class="text">3</span></label>
                                </li>
                                <li>
                                    <label class="inline"><input type="radio" name="supplements" />
                                    <span class="text font-bold">4 or more</span></label>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="align-right hide">
                <a href="javascript:void(0);" id="closeProfile" class="button margin-bottom-0 secondary">Close</a>
            </div>
        </div>        
    
    <script type="text/javascript">
        
    	$("#closeProfile").on("click", function(e){
    		e.preventDefault();
        	$(".close-reveal-modal").trigger("click");
        });
        
        $("input[type='radio']").on("change", function () {
            var $allLabels = $("input[name='" + $(this).attr("name") + "']").next("span").removeClass("font-bold"),
                $label = $(this).next("span");
            if (this.checked) {
                $label.addClass("font-bold");
            }
        });
    </script>
</body>
</html>

