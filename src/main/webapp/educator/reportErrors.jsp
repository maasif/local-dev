<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>
	<title>Errors</title>
</head>
<body>
	<br/><br/>

    <div class="max-width-1129">
        <div class="row">
            <div class="columns medium-12">
                <div class="hs-page-heading">
                    <h2>
                        Patient Reported Error
                    </h2>
                </div>
                <div id="tableDiv" class="main-page padding-top-28 top-0 green-bordered">
                    <div class="container" id="unprocessedTable">
                        <display:table name="errorObjs" requestURI="" class="table border-none" uid="row" style="width: 100%;">
                            <display:setProperty name="basic.empty.showtable" value="true" />

                            <display:column title="Patient ID" property="[1]" class="width-10-right-align"/>
                            <display:column title="Patient Name">
                                <s:property value="#attr.row[5]"/>
                            </display:column>
                            <display:column title="Log ID" property="[2]" class="width-10-right-align"/>
                            <display:column title="Last Reported Error" property="[3]"/>
                            <display:column title="Last Reported Date" style="width:15%">
                                <s:property value="@com.healthslate.patientapp.util.CommonUtils@gmtToest(#attr.row[4])" />
                            </display:column>
                            <display:column title="" style="width:10%; text-align:center;">
                                <s:url id="fetchLogDetail" action="fetchLogDetail">
                                    <s:param name="errorId" value="%{#attr.row[0]}"/>
                                    <s:param name="logId" value="%{#attr.row[2]}"/>
                                    <s:param name="patientId" value="%{#attr.row[1]}"/>
                                    <s:param name="isFromError" >
                                        yes
                                    </s:param>
                                </s:url>
                                <s:a href="%{fetchLogDetail}">
                                    <button class="button tiny margin-bottom-0 radius btn-teal">View Detail</button>
                                </s:a>
                            </display:column>
                        </display:table>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
 	<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#reportError').addClass('selected-tab');
			try {
				$.fn.dataTableExt.sErrMode = 'throw';
				initDataTable("row",{"sorting": [[4, "desc" ]]});
                //remove sorting class from td, __oz
                $(".sorting_1").removeClass("sorting_1 sorting_1");
			}
			catch(err) {console.log(err);}
			$('.row').css('max-width','100%');
		});
	</script>
</body>
</html>