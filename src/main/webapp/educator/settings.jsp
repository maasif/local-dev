<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Settings</title>    
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/chosen_v1.4.0/chosen.min.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/timepicker/jquery.timepicker.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/timepicker/bootstrap-datepicker.css" />
  
  	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/multiple-select/multiple-select.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css?v=0.2" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

    <style>

        .coach-notes {
            color: #8AB329 !important;
        }

        .coach-name {
            color: #444 !important;
        }

		#divMemberNavArea ul{
			top: -27px !important;
		}

		#divMemberNavArea ul li:last-child{
			display: none;
		}

		.settings-page-align{
			left: 117px !important;
		}

		body{
			overflow-x: hidden;
		}
    </style>
  </head>
  
  <body>

      <!-- BreadCrumb -->
      <div class="breadcrumb-green">
          <div class="max-width-1129">
              <ul class="breadcrumbs">
                  <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                      <li>
                          <a href="unProcessedMeals.action">Meals</a>
                      </li>
                  </s:if>
                  <li>
                      <a href="members.action">My Members</a>
                  </li>
                  <li>
                      <a href="dashboard.action?patientId=${patientId}">Member Dashboard</a>
                  </li>
                  <li class="current">Settings</li>
                  <li class="current"><b>Member Name:</b> <s:property value='%{patient.user.displayName}' /></li>
              </ul>
          </div>
      </div>

	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>			
			<p id="progressTxt" class="progress-text">We're saving data</p>
		</div>
	</div>

   <!-- Member's Food Restrictions Dialog -->
  <div id="memberEatingPreferenceDialog" class="reveal-modal medium">
      <div class="columns small-12">
          <h3 class="meal-summary-text remove-bold">Member's Food Restrictions</h3>
          <div class="dialog-area">

              <s:if test="%{patientEatingPreference == null || patientEatingPreference == ''}">
                  <p class="align-center">No food restriction defined</p>
              </s:if>
              <s:else>

                  <div id="divColdBeverage">
                      <p class="coach-notes margin-bottom-0">
                          Restrictions and Preferences
                      </p>
                      <p class="coach-name remove-uppercase">
                          <s:property value="%{patientEatingPreference.restriction}"></s:property>
                      </p>
                  </div>

                  <s:if test="%{patientEatingPreference.isColdBeverage == true}">
                      <div id="divColdBeverage">
                          <p class="coach-notes margin-bottom-0 margin-top-10">
                              Soda pop/Cola, Iced Tea, Energy Drinks
                          </p>
                          <p id="pColdBeverage" class="coach-name remove-uppercase">
                              <s:property value="%{patientEatingPreference.coldBeverageType}"></s:property>
                          </p>
                      </div>
                  </s:if>
                  <s:if test="%{patientEatingPreference.isHotBeverage == true}">
                      <div id="divHotBeverage">
                          <p class="coach-notes margin-bottom-0 margin-top-10">
                              Coffee or Tea with sugar (non-diet) sweetener(s) or flavoring(s):
                          </p>
                          <p class="coach-notes margin-bottom-0 remove-uppercase">
                              How many teaspoons of sugar sweetener or flavorings (not Splenda, Equal, diet flavorings, etc.) teaspoons added
                          </p>
                          <p id="pTeaspoon" class="coach-name remove-uppercase">
                              <s:property value="%{patientEatingPreference.hotBeverageTeaspoonAdded}"></s:property>
                          </p>

                          <p class="coach-notes margin-bottom-0 margin-top-10">
                              Size
                          </p>
                          <s:if test="%{patientEatingPreference.hotBeverageSize != ''}">
                              <p id="pSize" class="coach-name remove-uppercase">
                                  <s:property value="%{patientEatingPreference.hotBeverageSize}"></s:property>
                              </p>
                          </s:if>
                          <s:else>
                              <p id="pSize" class="coach-name remove-uppercase">
                                  N/A
                              </p>
                          </s:else>
                      </div>
                  </s:if>
                  <s:if test="%{patientEatingPreference.isAlcohol == true}">
                      <div id="divAlcohol">
                          <p class="coach-notes margin-bottom-0 margin-top-10">
                              Hard alcohol/Spirits
                          </p>
                          <p id="pAlcoholType" class="coach-name remove-uppercase">
                              <s:property value="%{patientEatingPreference.alcoholType}"></s:property>
                          </p>
                      </div>
                  </s:if>
              </s:else>
          </div>
      </div>
      <a class="close-reveal-modal">&#215;</a>
  </div>

	  <div id="divMemberNavArea"></div>
	<div class="page-container max-width-1129">
	
	  <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>
	
      <div class="settings-container">
      
      	<form id="formSettings" method="POST" class="margin-bottom-0">	  	           		     	
	  		
	  			<input type="hidden" id="txtMealTargets" name="mealTargets" />
	  			<input type="hidden" id="txtGlucoseTimes" name="glucoseTimes" />
	  			<input type="hidden" id="txtMedicationTimes" name="medTimes" />
	  			<input type="hidden" id="txtReminder" name="reminderJson" />
	  			<input type="hidden" id="txtHiddenTargetSource" name="target.source" />
				<input type="hidden" id="txtMealIsSigned" name="mealIsSigned" />
                <input type="hidden" id="txtPatientId" name="patientId" />

	  			<div class="row row-full">
   	  				<div class="large-6 columns">   	  					   	  					
   	  					<div class="border-bt-heading padding-left-0">
			         		<h4 class="meal-summary-text"><img src="${pageContext.request.contextPath}/resources/css/images/settings.png" />&nbsp;Settings</h4>
			         	</div>
   	  				</div>
   	  				<div class="large-6 align-right columns">
                        <a id="btnMemberFoodRestrictions" href="#" class="button radius left small btn-grey margin-bottom-0 margin-right-10"> Member's Food Restrictions</a>
	  					<label id="changedByLabel" class="inline changed-by margin-bottom-0" style="display:none;">Last updated by: <span id="txtTargetSource" class="target-source"></span></label>
   	  				</div> 	   	  				
    	  		</div>
	    	  			  		 		  	 
	    	  	<div class="page-sections row row-full main-page-settings">   
	    	  		
	    	  		
	    	  		<div class="columns medium-6">
	    	  			
	    	  			<!-- Meal Time -->
	    	  			<div class="colored-section border-orange">	       	  		  
			   	  			<div class="row row-full page-section">
			   	  				<div class="large-12 columns">   
			   	  					<h3>Food</h3>	  					   	  					
			   	  					<label class="inline page-section-header">Meal Times</label>
			   	  				</div>
			   	  				<div id="mealTimeContainer" class="large-12 columns page-section-right-content">
			   	  					  	  					
			   	  				</div>
			    	  		</div>
			    	  		<!-- Carb Ceilings per Meal --> 	  	
			    	  		<div class="row row-full page-section">
			   	  				<div class="large-12 columns">   	  					   	  					
			   	  					<label class="inline page-section-header">Carb Ceilings</label>
			   	  				</div>
			   	  				<div id="carbCeilingsContainer" class="large-12 columns page-section-right-content">
			   	  					  	  					
			   	  				</div>
			    	  		</div> 
			    	  	</div>
			    	  	<div class="colored-section border-darkgreen">
	   	  					<!-- Glucose Target range --> 	  		       	  		  
			   	  			<div class="row row-full page-section">
			   	  				<div class="large-12 columns">   	  	
			   	  					<h3>Glucose</h3>

                                    <div class="row row-auto">
                                        <div class="columns small-1">
                                            <input id="chkGlucoseTargetConsent" type="checkbox" class="large-chk margin-bottom-0" />
                                        </div>
                                        <div class="columns small-11 padding-left-0">
                                            <label class="inline margin-bottom-0 padding-top-0 font-16">
                                                Glucose targets were provided by the member's healthcare provider to the coach or member, or are based on standing orders from that provider.
                                            </label>
                                        </div>
                                    </div>
			   	  					<label class="inline page-section-header">Target Ranges</label>
			   	  				</div>
			   	  				<div id="glucoseTargetRangeContainer" class="large-12 columns page-section-right-content">
			  	  					 <div class="row page-section-row last-row">
				   	  					 <div class="columns large-3">
				   	  					  	<label class="inline label-bold margin-bottom-0">Two hours after meals</label>
				   	  					 </div>
				  	  					 <div class="columns large-9">
				  	  					 	<ul class="large-block-grid-4 margin-bottom-0">
				  	  					  		<li>
				  	  					  			<label class="inline margin-bottom-0">
				  	  					  				<input type="number" min="0" name="target.glucoseMin" class="margin-bottom-0 number-fields" />
				  	  					  			</label>
				  	  					  		</li>
				  	  					  		<li class="width-7perc">
				  	  					  			<label class="inline margin-bottom-0 align-label">
				  	  					  				-
				  	  					  			</label>	  	  					  			
				  	  					  		</li>
				  	  					  		<li>
				  	  					  			<label class="inline margin-bottom-0">
				  	  					  				<input type="number" min="0" name="target.glucoseMax" class="margin-bottom-0 number-fields" />
				  	  					  			</label>
				  	  					  		</li>
				  	  					  		<li>
				  	  					  			<label class="inline margin-bottom-0 align-label left-minus13">
				  	  					  			mg/dl
				  	  					  			</label>	  	  					  			
				  	  					  		</li>
				  	  					  	 </ul>
				  	  					  </div>
			  	  					  </div> 
			  	  					 <div class="row page-section-row last-row">
				   	  					 <div class="columns large-3">
				   	  					  	<label class="inline label-bold margin-bottom-0">Before Meals</label>
				   	  					 </div>
				  	  					 <div class="columns large-9">
				  	  					 	<ul class="large-block-grid-4 margin-bottom-0">
				  	  					  		<li>
				  	  					  			<label class="inline margin-bottom-0">
				  	  					  				<input type="number" min="0" name="target.glucoseMin2" class="margin-bottom-0 number-fields" />
				  	  					  			</label>
				  	  					  		</li>
				  	  					  		<li class="width-7perc">
				  	  					  			<label class="inline margin-bottom-0 align-label">
				  	  					  				-
				  	  					  			</label>	  	  					  			
				  	  					  		</li>
				  	  					  		<li>
				  	  					  			<label class="inline margin-bottom-0">
				  	  					  				<input type="number" min="0" name="target.glucoseMax2" class="margin-bottom-0 number-fields" />
				  	  					  			</label>
				  	  					  		</li>
				  	  					  		<li>
				  	  					  			<label class="inline margin-bottom-0 align-label left-minus13">
				  	  					  			mg/dl
				  	  					  			</label>	  	  					  			
				  	  					  		</li>
				  	  					  	 </ul>
				  	  					  </div>
			  	  					  </div>

                                    <div id="divOverlayGlucose" class="overlay-glucose"></div>
			   	  				</div>
			    	  		</div>
			    	  		<!-- Glucose Time --> 	  		       	  		  
			   	  			<div class="row row-full page-section">
			   	  				<div class="large-12 columns">   	  					   	  					
			   	  					<label class="inline page-section-header">Testing Frequency</label>
			   	  				</div>
			   	  				<div id="glucoseTimeContainer" class="large-12 columns page-section-right-content">
			   	  					  <div class="row page-section-row last-row">
				   	  					 <div class="columns large-3">
				   	  					  	<label class="inline label-bold glucose-time-label-align">Time</label>
				   	  					 </div>
				  	  					 <div class="columns large-9">
				  	  					 	<select id="ddGlucoseTimes" multiple="multiple">
										        <option class="all before" value="Before breakfast">Before breakfast</option>										        
										        <option class="all 2hour" value="2 hr after breakfast">2 hr after breakfast</option>
										        <option class="all before" value="Before lunch">Before lunch</option>
										        <option class="all 2hour" value="2 hr after lunch">2 hr after lunch</option>
										        <option class="all before" value="Before dinner">Before dinner</option>
										        <option class="all 2hour" value="2 hr after dinner">2 hr after dinner</option>
										        <option value="Before bed">Before bed</option>
										        <option class="all" value="Before all meals">Before all meals</option>
										        <option class="all" value="2 hr after all meals">2 hr after all meals</option>
										        <option value="Before and 2 hr after all meals">Before and 2 hr after all meals</option>
                                                <option class="three-times-weeks" value="Three times/Week">Three times/Week</option>
                                                <option class="once-week" value="Once/Week">Once/Week</option>
                                                <option value="Specific time of day">Specific time of day</option>
										    </select>
				  	  					 	<div id="specificTimeOfDay" style="display:none">
					  	  					 	<label class="inline margin-bottom-0">
				  					  				<input type="text" id="glucoseTime" placeholder="-- Select --" name="target.glucoseTime" class="margin-bottom-0 settings-picker" />
				  					  			</label>
				  					  			<ul id="glucoseTimesSelected" class="side-nav weight-frequency" style="display:none">	  					  				
				  					  			</ul>
			  					  			</div>
				  	  					  </div>
			  	  					  </div> 	 					
			   	  				</div>
			    	  		</div>
	   	  				</div>

						<div class="colored-section border-red">
							<div class="row row-full page-section">
								<div class="large-12 columns">
									<h3>ICF Signature</h3>
								</div>
								<div id="mealProcessedAssigned" class="large-12 columns page-section-right-content">
									<div class="row page-section-row last-row">
										<div class="columns large-3">
											<label class="inline label-bold">ICF Signed</label>
										</div>
										<div class="columns large-9">
											<div class='reminders'>
												<ul id="mealProcessedSwitch" class='button-group reminder-switch goals-reminder tiny round'>
													<li><a href='#' title='No' class='button small margin-bottom-0' data-text='No'>No</a></li>
													<li><a href='#' title='Yes' class='button small secondary margin-bottom-0 button-green' data-text='Yes'>Yes</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

	    	  		</div>
    	  			 
	   	  			<div class="columns medium-6">
		   	  			<div class="colored-section border-darkblue">
		   	  					<!-- Medication Reminders Time --> 	  		       	  		  
				   	  			<div class="row row-full page-section">
				   	  				<div class="large-12 columns">  
				   	  					<h3>Medication</h3> 	  					   	  					
				   	  					<label class="inline page-section-header">Times I take meds</label>
				   	  				</div>
				   	  				<div id="medicationsReminderContainer" class="large-12 columns page-section-right-content">
				   	  					  <div class="row page-section-row last-row">
					   	  					 <div class="columns large-3">
					   	  					  	<label class="inline label-bold">Time</label>
					   	  					 </div>
					  	  					 <div class="columns large-9">
					  	  					 	<label class="inline margin-bottom-0">
				  					  				<input type="text" id="medicationTime" placeholder="-- Click to Add Meds Time --" name="target.medicationTime" class="margin-bottom-0 settings-picker excluded" />
				  					  			</label>
				  					  			<ul id="medicationTimesSelected" class="side-nav" style="display:none">
				  					  			</ul>
					  	  					  </div>
				  	  					  </div> 	 					
				   	  				</div>
				    	  		</div>
		   	  			</div>
	   	  				
	   	  				<div class="colored-section border-red">
			    	  		<!-- Weight --> 	  		       	  		  
			   	  			<div class="row row-full page-section">
			   	  				<div class="large-12 columns">   	  					   	
			   	  					<h3>Other Diary Entries</h3>  					
			   	  					<label class="inline page-section-header">Weight</label>
			   	  				</div>
			   	  				<div id="weightContainer" class="large-12 columns page-section-right-content">
			   	  					  <div class="row page-section-row">
				   	  					 <div class="columns large-3">
				   	  					  	<label class="inline label-bold">Starting Weight</label>
				   	  					 </div>
				  	  					 <div class="columns large-9">
				  	  					 	<ul class="inline-list margin-bottom-0">
				  	  					 		<li>
				  	  					 			<label class="inline margin-bottom-0">
			  					  						<input type="number" min="0" id="startingWeight" name="target.startingWeight" class="margin-bottom-0 number-fields" />
			  					  					</label>
			  					  				</li>
			  					  				<li class="margin-left-5">
				  	  					 			<label class="inline margin-bottom-0 align-label">
			  					  						lbs
			  					  					</label>
			  					  				</li>
			  					  			</ul>  					  			
				  	  					  </div>
			  	  					  </div> 	
			  	  					  <div class="row page-section-row">
				   	  					 <div class="columns large-3">
				   	  					  	<label class="inline label-bold">Goal Weight</label>
				   	  					 </div>
				  	  					 <div class="columns large-9">
				  	  					 	<ul class="inline-list margin-bottom-0">
				  	  					 		<li>
				  	  					 			<label class="inline margin-bottom-0">
			  					  						<input type="number" min="0" id="targetWeight" name="target.targetWeight" class="margin-bottom-0 number-fields" />
			  					  					</label>
			  					  				</li>
			  					  				<li class="margin-left-5">
				  	  					 			<label class="inline margin-bottom-0 align-label">
			  					  						lbs
			  					  					</label>
			  					  				</li>
			  					  			</ul>
				  	  					  </div>
			  	  					  </div>
			  	  					  <div class="row page-section-row">
				   	  					 <div class="columns large-3">
				   	  					  	<label class="inline label-bold">Measurement Frequency</label>
				   	  					 </div>
				  	  					 <div class="columns large-9">
				  	  					 	<label class="inline margin-bottom-0">
						  						<select id="ddWeightFrequency" name="target.weightFrequency" class="weight-frequency large-dropdown margin-bottom-0">
						  							<option value="" disabled="disabled" selected="selected">-- Select Frequency</option>
                                                    <option value="Once/Day">Once/Day</option>
						  							<option value="Once/Week">Once/Week</option>  					  							
						  						</select>
						  					</label>
				  	  					  </div>
			  	  					  </div>     					
			   	  				</div>
			    	  		</div> 
			    	  		<!-- Activity --> 	  		       	  		  
			   	  			<div class="row row-full page-section">
			   	  				<div class="large-12 columns">   	  					   	  					
			   	  					<label class="inline page-section-header">Activity</label>
			   	  				</div>
			   	  				<div id="activityContainer" class="large-12 columns page-section-right-content">
			   	  					 <div class="row page-section-row">
				   	  					 <div class="columns large-3">
				   	  					  	<label class="inline label-bold">Steps</label>
				   	  					 </div>
				  	  					 <div class="columns large-9">
				  	  					 	<ul class="inline-list margin-bottom-0">
				  	  					 		<li>
				  	  					 			<label class="inline margin-bottom-0">
			  					  						<input type="number" min="0" id="activitySteps" name="target.activitySteps" placeholder="Enter steps..." class="margin-bottom-0 number-fields" />
			  					  					</label>
			  					  				</li>
			  					  				<li class="margin-left-5">
				  	  					 			<label class="inline margin-bottom-0 align-label">
			  					  						per day
			  					  					</label>
			  					  				</li>
			  					  			</ul>  					  			
				  	  					  </div>
			  	  					  </div> 				  	  					  			   	  					
			   	  				</div>
			    	  		</div>
			    	  		
			    	  	</div>
			    	  	
			    	  	<div class="colored-section border-darkblue">
	   	  					<!-- Reminders & Sharing  --> 	  		       	  		  
			   	  			<div class="row row-full page-section">
			   	  				<div class="large-12 columns">  
			   	  					<h3>Reminders & Sharing</h3> 	  					   	  					
			   	  					<label class="inline page-section-header">Send me reminders about</label>
			   	  				</div>
			   	  				<div id="reminderContainer" class="large-12 columns page-section-right-content">
			   	  					  <div class="row page-section-row">
				   	  					 <div class="columns large-3">
				   	  					  	<label class="inline label-bold">Log my Meals</label>
				   	  					 </div>
				  	  					 <div class="columns large-9">
							 				<div class='reminders'>
									 			<ul class='button-group reminder-switch goals-reminder tiny round' data-key="isMealTime"> 
													<li><a href='#' title='No Reminder' class='button tiny margin-bottom-0 button-green' data-text='REMINDER'><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/no-reminder.png" title='No Reminder' /></a></li> 
													<li><a href='#' title='Reminder' class='button tiny secondary margin-bottom-0' data-text='NO REMINDER'><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/reminder.png" title='Reminder'/></a></li> 
												</ul> 
												<span class='reminder-status'>NO REMINDER</span> 
											</div>   			
				  	  					  </div>
			  	  					  </div> 		 
			  	  					  <div class="row page-section-row">
				   	  					 <div class="columns large-3">
				   	  					  	<label class="inline label-bold">Glucose testing</label>
				   	  					 </div>
				  	  					 <div class="columns large-9">
							 				<div class='reminders'>
									 			<ul class='button-group reminder-switch goals-reminder tiny round' data-key="isGlucoseTesting"> 
													<li><a href='#' title='No Reminder' class='button tiny margin-bottom-0 button-green' data-text='REMINDER'><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/no-reminder.png" title='No Reminder' /></a></li> 
													<li><a href='#' title='Reminder' class='button tiny secondary margin-bottom-0' data-text='NO REMINDER'><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/reminder.png" title='Reminder'/></a></li> 
												</ul> 
												<span class='reminder-status'>NO REMINDER</span> 
											</div>   			
				  	  					  </div>
			  	  					  </div>
			  	  					  <div class="row page-section-row">
				   	  					 <div class="columns large-3">
				   	  					  	<label class="inline label-bold">Weighing myself</label>
				   	  					 </div>
				  	  					 <div class="columns large-9">
							 				<div class='reminders'>
									 			<ul class='button-group reminder-switch goals-reminder tiny round' data-key="isWeighingMyself"> 
													<li><a href='#' title='No Reminder' class='button tiny margin-bottom-0 button-green' data-text='REMINDER'><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/no-reminder.png" title='No Reminder' /></a></li> 
													<li><a href='#' title='Reminder' class='button tiny secondary margin-bottom-0' data-text='NO REMINDER'><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/reminder.png" title='Reminder'/></a></li> 
												</ul> 
												<span class='reminder-status'>NO REMINDER</span> 
											</div>   			
				  	  					  </div>
			  	  					  </div>
			  	  					  <div class="row page-section-row hide">
				   	  					 <div class="columns large-3">
				   	  					  	<label class="inline label-bold">Goal action plans</label>
				   	  					 </div>
				  	  					 <div class="columns large-9">
							 				<div class='reminders'>
									 			<ul class='button-group reminder-switch goals-reminder tiny round' data-key="isGoalActionPlan"> 
													<li><a href='#' title='No Reminder' class='button tiny margin-bottom-0 button-green' data-text='REMINDER'><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/no-reminder.png" title='No Reminder' /></a></li> 
													<li><a href='#' title='Reminder' class='button tiny secondary margin-bottom-0' data-text='NO REMINDER'><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/reminder.png" title='Reminder'/></a></li> 
												</ul> 
												<span class='reminder-status'>NO REMINDER</span> 
											</div>   			
				  	  					  </div>
			  	  					  </div>
			  	  					  <div class="row page-section-row">
				   	  					 <div class="columns large-3">
				   	  					  	<label class="inline label-bold">New topics available</label>
				   	  					 </div>
				  	  					 <div class="columns large-9">
							 				<div class='reminders'>
									 			<ul class='button-group reminder-switch goals-reminder tiny round' data-key="isNewTopicAvailable"> 
													<li><a href='#' title='No Reminder' class='button tiny margin-bottom-0 button-green' data-text='REMINDER'><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/no-reminder.png" title='No Reminder' /></a></li> 
													<li><a href='#' title='Reminder' class='button tiny secondary margin-bottom-0' data-text='NO REMINDER'><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/reminder.png" title='Reminder'/></a></li> 
												</ul> 
												<span class='reminder-status'>NO REMINDER</span> 
											</div>   			
				  	  					  </div>
			  	  					  </div>					
			   	  				</div>
			    	  		</div>
	   	  				</div>

	   	  				<div class="align-right padding-10 padding-right-0">
		    	  			<a href="javascript:;" id="btnSaveSettings" class="button radius btn-teal margin-bottom-0">Save</a>
		    	  		</div>
	   	  			</div>	    	  			    	  		 
	      	  	</div>        	  		   
      	  	</form>    	  
    	</div>    	
    </div>

      <!-- Three Times/week Dialog -->
      <div id="threeTimesWeekDialog" class="reveal-modal small">
          <div class="columns small-12">
              <h3 class="meal-summary-text">Select Days (Three times/Week) </h3>
              <div id="errMessageThreeTimesWeek" class="alert-box alert alert-message-abs">______________________</div>
              <label class="inline margin-bottom-0">
                  Select days
                  <select id="ddOnceWeek" class="margin-bottom-0" data-placeholder="(select)" multiple>
                      <option value="Monday">Monday</option>
                      <option value="Tuesday">Tuesday</option>
                      <option value="Wednesday">Wednesday</option>
                      <option value="Thursday">Thursday</option>
                      <option value="Friday">Friday</option>
                      <option value="Saturday">Saturday</option>
                      <option value="Sunday">Sunday</option>
                  </select>
              </label>
              <div class="align-right">
                  <a href="#" id="btnAddThreeTimesWeekDialog" class="button radius btn-teal">Save</a>
              </div>
          </div>
          <a class="close-reveal-modal">&#215;</a>
      </div>

      <!-- Once week Dialog -->
      <div id="onceWeekDialog" class="reveal-modal small">
          <div class="columns small-12">
              <h3 class="meal-summary-text">Select Day (Once/Week) </h3>
              <div id="errMessageOnceWeek" class="alert-box alert alert-message-abs">______________________</div>
              <div class="dialog-area">
                  <label class="inline margin-bottom-0">
                      Select day
                      <select id="ddOnceWeekDays" class="margin-bottom-0 font-18 large-dropdown">
                          <option disabled value="" selected>(select)</option>
                          <option value="Monday">Monday</option>
                          <option value="Tuesday">Tuesday</option>
                          <option value="Wednesday">Wednesday</option>
                          <option value="Thursday">Thursday</option>
                          <option value="Friday">Friday</option>
                          <option value="Saturday">Saturday</option>
                          <option value="Sunday">Sunday</option>
                      </select>
                  </label>
              </div>
              <div class="align-right">
                  <a href="#" id="btnAddOnceWeekDialog" class="button radius btn-teal">Save</a>
              </div>
          </div>
          <a class="close-reveal-modal">&#215;</a>
      </div>

      <!-- Once week Dialog -->
      <div id="medicationAddDialog" class="reveal-modal small">
          <div class="columns small-12">
              <h3 class="meal-summary-text">Add Medication Time</h3>
              <div id="errMessageMedication" class="alert-box alert alert-message-abs">______________________</div>
              <div class="dialog-area">
                  <label class="inline margin-bottom-0">
                      Select Time
                      <input type="text" id="medicationTimeDialog" placeholder="(select)" class="margin-bottom-0 settings-picker width-100" />
                  </label>
                  <label class="inline margin-bottom-0">
                      Select Repeating
                  </label>

                  <input type="radio" name="occurrence" class="meds-occurrence" data-aka="Daily" checked value="Once/Day"> Once/Day
                  <input type="radio" name="occurrence" class="meds-occurrence" data-aka="Weekly" value="Once/Week"> Once/Week

                  <label id="lblWeekly" class="inline margin-bottom-0 margin-left-0 width-100" style="display: none">
                      Select day of week
                      <select id="ddWeekly" class="margin-bottom-0 font-18 large-dropdown">
                          <option disabled value="" selected>(select)</option>
                          <option value="Monday">Monday</option>
                          <option value="Tuesday">Tuesday</option>
                          <option value="Wednesday">Wednesday</option>
                          <option value="Thursday">Thursday</option>
                          <option value="Friday">Friday</option>
                          <option value="Saturday">Saturday</option>
                          <option value="Sunday">Sunday</option>
                      </select>
                  </label>
              </div>
              <div class="align-right">
                  <a href="#" id="btnAddMedicationDialog" class="button radius btn-teal">Save</a>
              </div>
          </div>
          <a class="close-reveal-modal">&#215;</a>
      </div>
	  <!--[if lt IE 9]>
	    	<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
		<![endif]-->
		<!--[if (gte IE 9) | (!IE)]><!-->
		    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>				       
		    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
		<!--<![endif]-->       
	        
	    <!-- Foundation 3 for IE 8 and earlier -->
		<!--[if lt IE 9]>	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
		<![endif]-->
		
		<!-- Foundation 4 for IE 9 and later -->
		<!--[if gt IE 8]><!-->	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
		<!--<![endif]-->	
	   	   	  
       <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>

       <script src="${pageContext.request.contextPath}/resources/chosen_v1.4.0/chosen.jquery.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/multiple-select/jquery.multiple.select.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/timepicker/jquery.timepicker.min.js"></script>             
       <script src="${pageContext.request.contextPath}/resources/js/settings.js?v=0.3"></script>
       
      <script type="text/javascript">
		
        var settings = null,
        	TARGET_LOADED_OBJECT = ("${settingsLoadString}") ? $.parseJSON("${settingsLoadString}") : "",
			MEAL_IS_SIGNED = "${mealIsSigned}";
            patId = '${patientId}',
            loggedInUserID = '${session.PROVIDER.providerId}';
      
        $(function(){
		    $(document).foundation();
            $("#patientsTab").addClass("selected-tab");
            settings = Object.create(Settings); //Settings is a entity/class            
            settings.init();
            $("#btnMemberFoodRestrictions").on("click", function(e){
                e.preventDefault();
                $("#memberEatingPreferenceDialog").foundation("reveal", "open");
            });
			Ajaxing.loadExternalPage($("#divMemberNavArea"), "${pageContext.request.contextPath}/educator/memberNavArea.jsp", function(){
				$("#divMemberNavArea ul").addClass("settings-page-align").removeClass("left-align-52");
			});
        });
 
      </script>
  </body>          
</html>