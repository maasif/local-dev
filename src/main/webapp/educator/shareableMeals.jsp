<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>

   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->

	<link rel="stylesheet"	href="${pageContext.request.contextPath}/resources/css/queue.style.css" />
	<title>Shareable Meals</title>
	
	<style type="text/css">
		.input-field {
			width: 35% !important;
		}
		
		.ui-datepicker-trigger {
			margin: 0px;
			height: 37px;
			padding-top: 11px;
			width: 5px;
			padding-left: 1rem;
			background-color: #428bca;
		}
		
		.date-style {
			width: 82%;
			display: inline-block !important;
		}
		
		.header-style {
			width: 60%;
			padding: 0;
			top: 10px;
		}
		
		.ui-datepicker-trigger {
			margin: 0px;
			height: 37px;
			padding-top: 17px;
			width: 5px;
			padding-left: 1rem;
		}
		
		.dataTables_processing {
			position: absolute;
			top: 2% !important; 
			left: 50%;
			width: 250px;
			height: 30px;
			margin-left: -125px;
			margin-top: -15px;
			padding: 14px 0 2px 0;
			border: 0px !important; 
			text-align: center;
			color: #999;
			font-size: 14px;
			background: inherit !important;
			font-weight: bold;
			color: brown;
		}

		.share-statuses{
			font-size: 14px;
			font-weight: bold;
		}

		.dont-share-status{
			color: #f04124;
		}

		.share-anonymous-status{
			color: #008CBA;
		}

		.share-status{
			color: #43AC6A;
		}

        .footer{
            margin-top: 80px !important;
        }

		.text-red{
			color: #f04124;
			font-size: 14px;
		}

		.text-green{
			color: #008CBA;
			font-size: 14px;
		}

		.width-20perc{
			width: 20%;
		}
		
		table tr:hover{
			cursor: pointer;
		}

		.alert-message-abs{
			display: none;
			top:82px;
			position: absolute !important;
		}

		tbody tr td:last-child{
			text-align: center;
		}

		.stop-horizontal-scroll{
			resize: vertical;
			min-height: 100px;
		}
	</style>
</head>

<body>

	<br/>
	
	<div id="loading" class="loader" style="display: block;">
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>
			<div class="progress progress-striped active">
				<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
		</div>
	</div>

	<div id="errMessagePage" class="alert-box alert alert-message-abs" style="display: none">______________________</div>

    <div class="max-width-1129 margin-bottom-20">
        <div class="row">
            <div class="columns medium-12">
                <div class="hs-page-heading">
                    <h2>
                        Shareable Meals
                    </h2>
                </div>
                <div id="tableDiv" class="main-page padding-top-28 top-0 green-bordered">
                    <div class="container" id="unprocessedTable">
                        <table id="table" class="table border-none">
                            <thead>
                                <tr>
                                    <th class="width-20perc">Member Name</th>
                                    <th>Meal Name</th>
                                    <th class="width-20perc">Meal Type</th>
                                    <th class="width-20perc">Member Permission</th>
									<th>Request Permission Date</th>
									<th>Permission Granted Date</th>
									<th>Meal Shared Date</th>
                                    <th>Action/Response</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div id="shareMealNotesDialog" class="reveal-modal medium" data-reveal>
		<h3 class="meal-summary-text remove-bold">Add Notes</h3>
		<p class="lead dialog-para">
			<label>
				Notes
				<textarea id="txtNotes" rows="7" placeholder="Add notes..." class="stop-horizontal-scroll"></textarea>
				<small class="error hide">Required.</small>
			</label>
		</p>
		<a id="closeShareMealNotesDialog" class="close-reveal-modal">&#215;</a>
		<div class="align-right">
			<a href="javascript:;" id="btnPostShareMealDialog" class="button radius small btn-teal margin-top-10 margin-bottom-0">Post</a>
			<a href="javascript:;" class="button secondary radius margin-top-10 small margin-bottom-0" onclick="$('#closeShareMealNotesDialog').click(); return false;">Cancel</a>
		</div>
	</div>

	<script	src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>
		
	<script type="text/javascript">
	
		var CurrenDisplayValue,
			oTable,
			showRowCount = 0,
			MEAL_ID = "";
		
		$(document).ready(function(){

			$("#shareableMealsTab").addClass("selected-tab").parents("ul").prev("li").addClass("selected-tab");

			$.fn.dataTableExt.sErrMode = 'throw';
			
			  oTable=  $('#table').dataTable(
			  {
				"sDom" : "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'spanPagination'p>>",
				"sPaginationType" : "bootstrap",
				"oLanguage" : {
					"sLengthMenu" : "_MENU_ records per page" // how many records you want to display per page
				},
                 "fnInitComplete": function (oSettings, json) {
                    //remove sorting class from td, __oz
                    $(".sorting_1").removeClass("sorting_1 sorting_1");
                 },
                  "fnDrawCallback": function( oSettings ) {
                      $(".sorting_1").removeClass("sorting_1 sorting_1");
					  bindRowClickListener();
                  },
				"bServerSide": true ,
				
		 		"sAjaxSource": "fetchShareableMeals.action", // return jsonresponse with aaData json object
				"fnServerParams": function ( aoData ) {
				},  
				
				"bRetrieve" : true, // retrieve datatble object
				"bPaginate" : true, // enable disable pagination
				"bStateSave" : false, // saveState in cookie
				"bSort" : true, 
				"aaSorting": [[ 4, "desc" ]],
				"bProcessing": true, // processing text in while processing
	 			"aoColumns": [
							   { "mDataProp": "memberName"},
								{ "mDataProp": function (data, type, full){
									if(data.mealName){
										return data.mealName;
									}
									return "N/A";
								}},
			                   { "mDataProp": "logType"},
								{ "mDataProp": function (data, type, full){
									if(data.permission){

										switch (data.permission){
											case "SHARE":
												return "<span class='share-statuses share-status'>Share</span>";
											case "SHARE_ANONYMOUSLY":
												return "<span class='share-statuses share-anonymous-status'>Share but anonymously</span>";
											case "DONT_SHARE":
												return "<span class='share-statuses dont-share-status'>Don't share</span>";
										}
									}

									return "N/A";
								}},
			                   { "mDataProp": function (data, type, full){
									return "<span class='sp-log-id' data-id='"+data.logId+"' data-pid='"+data.patientId+"'>"+ HSDateUtils.formatDateFromTicks(data.requestSentDate)+"</span>";
								}},
								{ "mDataProp": function (data, type, full){
									if(data.permissionGrantedDate){
										return HSDateUtils.formatDateFromTicks(data.permissionGrantedDate);
									}
									return "N/A";
								}},
								{ "mDataProp": function (data, type, full){
									if(data.lastMealSharedDate){
										return HSDateUtils.formatDateFromTicks(data.lastMealSharedDate);
									}
									return "N/A";
								}},
			                   { "mDataProp": function ( data, type, full ){
								   if(data.permission){
									   if(data.permission == "SHARE" || data.permission == "SHARE_ANONYMOUSLY"){
										   var notes = (data.notes) ? data.notes : "";

										    if(data.mealSharedCount > 0){
												return "<button data-id='"+data.logId+"' data-notes='"+notes+"' class='button radius margin-bottom-0 btn-green display-inline-block small share-with-group'>Share Again</button>";
											}
									   		return "<button data-id='"+data.logId+"' data-notes='"+notes+"' class='button radius margin-bottom-0 btn-green display-inline-block small share-with-group'>Share this Meal with Group</button>";
									   }else{
										   return "<p class='text-red'>Member has denied permission of sharing this meal</p>";
									   }
								   } else {
									   return "<p class='text-green'>Already sent a request to member</p>";
								   }
							   }
							,'bSortable': false}
			             ]
			  }); 

			$('.row').css({'max-width':'100%', 'margin-top':'-9'});

			//fixed paging styling
			fixedPaginationStyling();
			bindRowClickListener();

            //remove sorting class from td, __oz
            $(".sorting_1").removeClass("sorting_1 sorting_1");

			$("#loading").hide();
		}); 

		//fixed paging styling
		function fixedPaginationStyling(){
			var $ul = $(".dataTables_paginate ul");			
			$ul.each(function(){
				var $this = $(this);
				if($this && !$this.hasClass("pagination")){
					$this.addClass("pagination");
				}
			});

			$("#btnPostShareMealDialog").off("click").on("click", function(e){
				e.preventDefault();

				var $txtNotes = $("#txtNotes");

				$txtNotes.next().addClass("hide");

				if(!$txtNotes.val()){
					$txtNotes.next().removeClass("hide");
					return;
				}

				if(MEAL_ID){
					generateNewsFeedPost(MEAL_ID, $txtNotes.val());
				}
			});
		}

		function bindRowClickListener(){
			$("table tbody tr").off("click").on("click", function(){
				var $tr = $(this),
					$sp = $tr.find("span.sp-log-id"),
					logId = $sp.data("id"),
					patientId = $sp.data("pid");

				if(logId && patientId){
					window.location = "fetchLogDetail.action?redirectToPage=shareableMeals&logId="+logId+"&patientId="+patientId+"&isFromError=no";
				}

				return false;
			});

			$(".share-with-group").off("click").on("click", function(){
				MEAL_ID = $(this).data("id");
				$("#txtNotes").val("");

				if($(this).data("notes") && $(this).data("notes") !== undefined){
					$("#txtNotes").val($(this).data("notes"))
				}
				$("#shareMealNotesDialog").foundation("reveal", "open");
				return false;
			});
		}

		function generateNewsFeedPost(reqLogId, notes){
			var actionUrl = "generateNewsFeedPost.action"
					, dataToSend = {
						"logId": reqLogId,
						"token": notes
					}
					, postRequestActions = {
						"requestType": "GET",
						"loading": $("#loading"),
						"successCallBack": onGeneratingNewsFeed
					};
			sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
		}

		function onGeneratingNewsFeed(data){
			var alertMessage = AlertMessage.init(),
				$err = $("#errMessagePage");

			//refresh table once you perform any action
			oTable.fnDraw();
			$("#shareMealNotesDialog").foundation("reveal", "close");
			$("#txtNotes").val("");

			if(data && data.STATUS == "SUCCESS"){
				alertMessage.showAlertMessage($err, "Meal shared with Group successfully.", alertMessage.SUCCESS, true);
			}else {
				alertMessage.showAlertMessage($err, "Unable to share meal.", alertMessage.ERROR, true);
			}
		}

	</script>
</body>
</html>