
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/logDetail.css?v=0.4" />

    <title>Suggested Meal Log Detail</title>

    <style>
        .tab-content, #nutTable{
            background-color: #F7FFDE !important;
        }
    </style>
</head>
<body>

<s:set var="logType" value="%{logType}" />
<%-- <s:property value="%{log.isProcessed"/> --%>

<!-- BreadCrumb -->
<div class="breadcrumb-green">
    <div class="max-width-1129">
        <ul class="breadcrumbs">
            <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">
                <li>
                    <a href="unProcessedMeals.action">Meals</a>
                </li>
            </s:if>
            <li>
                <a href="members.action">My Members</a>
            </li>

            <li>
                <a href="myMealsPage.action?patientId=<s:property value="%{patientId}"/>&mealType=<s:property value="%{log.foodLogSummary.type}"/>">Past/Suggested Meals</a>
            </li>

            <li class="current">SUGGESTED MEAL DETAILS</li>
            <li class="current"><b>Member Name:</b> <s:property value='%{patient.user.displayName}' /></li>
            <li class="current"><b>Last Modified Time:</b> <s:property value='@com.healthslate.patientapp.util.CommonUtils@timeInUserTimezone(suggestedLog.getLastModified(), log.getLogTimeOffset(), log.getOffsetKey())' /></li>
            <li class="current"><b>Log Type:</b> <s:property value='%{log.foodLogSummary.type}'/> </li>

        </ul>
    </div>
</div>

<div id="dialogError" class="reveal-modal small" data-reveal>
    <h3 class="meal-summary-text remove-bold">Suggested Meal Over Carb Ceiling</h3>
    <p class="lead dialog-para">Total carbs in this suggested meal exceed <span id="spExceeded" class="font-bold font-20 meal-summary-text"></span> from member carb ceiling. Do you still want to send this suggested meal to member?</p>
    <a id="closeConfirmErrorDialog" class="close-reveal-modal">&#215;</a>
    <div class="align-right">
        <button type="button" class="button secondary radius margin-top-10 margin-bottom-0 right" onclick="$('#closeConfirmErrorDialog').click(); return false;">No</button>
        <a id="btnDialogYesSuggestedMeal" href="javascript:;" class="button radius margin-top-10 margin-bottom-0 margin-right-10 btn-teal">Yes</a>
    </div>
</div>

<div id="confirmationAlert" class="reveal-modal small" data-reveal>
    <h3 class="remove-bold meal-summary-text">Delete food</h3>
    <p class="lead dialog-para">Are you sure you want to delete this food?</p>
    <a id="closeConfirmDialog" class="close-reveal-modal">&#215;</a>
    <div class="align-right">
        <button id="btnConfirmDelete" type="button" class="button radius btn-teal margin-top-10 margin-bottom-0">Yes</button>
        <button type="button" class="button secondary radius margin-top-10 margin-bottom-0" onclick="$('#closeConfirmDialog').click(); return false;">No</button>
    </div>
</div>

<div id="infoError" class=" alert error alert-new-message"></div>
<div id="loading" class="loader">
    <div class="image-loader">
        <p class="progress-please-wait">Please wait...</p>
        <div class="progress progress-striped active">
            <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            </div>
        </div>
        <!-- <p id="progressTxt" class="progress-text">We're uploading your content!!!</p> -->
    </div>
</div>

<div id="divMemberNavArea"></div>
<div class="dashboard-container margin-bottom-20 margin-top-10">

     <div class="row max-width-1129">

        <div class="coach-area-expand">
            <a href="#" id="btnExpColCoachArea"><img src="${pageContext.request.contextPath}/resources/css/images/arrow-right.png" title="Show" /></a>
        </div>

        <div class="columns medium-12">

            <!-- Charts, goals and Coach Notes area -->
            <div class="row bg-white row-auto" data-equalizer>

                <!-- Right white area -->
                <div class="columns medium-12 padding-lr-0 summary-area equal-areas" data-equalizer-watch>

                  <div id="main-panel">

    <div id="waiting-div" style="text-align: center; display: none;">
        <img alt="" src="${pageContext.request.contextPath}/resources/images/indicator.gif">
        <br/>
    </div>

    <s:form id="formUpload" method="POST" enctype="multipart/form-data" cssClass="margin-bottom-0 hide">
      <s:file type="file" id="uploadFile" accept="image/*" name="uploadFile" cssClass="hide"></s:file>
    </s:form>

    <s:actionmessage id="actionMessage" cssClass="alert success common-class"/>
    <div class="max-width-1129">

            <div>
                <div class="tab-content current custom-tc">
                    <div class="switch centered common-class">
                        <ul id="switch" data-content="switch-elements" class="button-group radius hide centered">
                            <li><a id="food-details" href="#" data-attached-elem="foodDetails" class="button">Food</a></li>
                            <li><a id="other-details" href="#" data-attached-elem="otherDetails" class="button secondary">Other</a></li>
                        </ul>
                    </div>
                    <div class="switch-container">

                        <label id="aMsg" class="alert success alert-new-message"></label>
                        <label id="logSuccess" class="alert success alert-new-message"></label>
                        <label id="logError" class="alert error alert-new-message"></label>
                        <label id="infoSuccess" class=" alert success alert-new-message"></label>
                        <label id="infoError" class="alert error alert-new-message"></label>

                        <div id="foodDetails" >
                            <s:if test="%{#logType=='Meal'}">
                                <div class="patHeadDiv common-class">
                                    <label class="heading-blue"> <span class="font-bold uppercase text-black"> Create Suggested Meal </span> <span class="right"><b>Created by:</b> <span class="text-black"> <s:property value='%{suggestedLog.createdBy.user.lastName}'/>, <s:property value='%{suggestedLog.createdBy.user.firstName}'/> </span> </span>  </label>
                                </div >
                                <br/>
                                <div class="row">
                                    <div class="columns large-6">
                                        <ul class="inline-list">
                                            <li>
                                                <span class="inline font-bold margin-bottom-0">Meal Name: </span>
                                            </li>
                                            <li>
                                                <s:textfield id="logNameFromFood" name="logNameFromFood" maxlength="100" required="required" size="40" cssClass="capital log-name-food margin-bottom-0"  placeholder="Enter Meal name"></s:textfield>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="columns large-6">
                                        <ul class="inline-list">
                                            <li>
                                                <span class="inline font-bold margin-bottom-0">Detailed Name: </span>
                                            </li>
                                            <li>
                                                <s:textfield id="minorFoodName" name="minorFoodName" maxlength="100" size="40" cssClass="capital log-name-food margin-bottom-0"  placeholder="Enter Detailed Name"></s:textfield>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="columns large-12">
                                        <span class="inline-block suggested-meal-notes font-bold vertical-top">Notes: </span>
                                        <div class="inline-block position-relative">
                                            <s:textarea id="userFoodNotes" name="userFoodNotes" rows="4" cssClass="width-828 margin-bottom-0 resize-vertical" ></s:textarea>
                                        </div>
                                    </div>
                                </div>

                                <br/>

                                <s:if test="%{mealCeilings.get(log.foodLogSummary.type) <= 0}">
                                    <div class="patHeadDiv common-class"><label class="heading-blue">Food Detail <span class="remove-bold label warning font-20"><span class="font-bold"> No Ceiling set </span> </span></label></div>
                                </s:if>
                                <s:else>
                                    <div class="patHeadDiv common-class"><label class="heading-blue">Food Detail <span class="remove-bold label warning font-20">Carb ceiling: <span class="font-bold"><s:property value='%{mealCeilings.get(log.foodLogSummary.type)}'/></span>g </span></label></div>
                                </s:else>

                                <s:actionerror id="actionError" name="actionMessage" cssClass="alert error alert-enhancment"/>
                                <form id="msgForm" action="sendMessage.action">
                                    <s:hidden id="message" name="message"></s:hidden>
                                    <s:hidden id="logId" name="logId"></s:hidden>
                                </form>
                                <div class="row row-auto">

                                    <div id="foodModal" class="hide-dialog">

                                        <s:if test="%{log.foodLogSummary == null ||
							      					  log.foodLogSummary.hasImage == null ||
							      					  log.foodLogSummary.hasImage == false ||
							      					  encodedFoodImage == null}"></s:if>
                                        <s:else>
                                            <div class="linker link-top modal-con"></div>
                                            <div class="linker link-bottom modal-con"></div>
                                        </s:else>

                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button id="btnCloseFoodDialogTop" type="button" class="close">&times;</button>
                                                    <h4 class="modal-title">Add Food</h4>
                                                </div>
                                                <div class="modal-body padding-bottom-0">
                                                    <div class="modal-con">
                                                        <ul class="button-group modal-con">
                                                            <li><label class="item-style inline modal-con">	Food:</label></li>
                                                            <li class="margin-left-5p"><s:textfield id="newFoodName" cssClass="modal-con food-name-style width-107p" maxlength="50" required="required"></s:textfield></li>
                                                        </ul>
                                                    </div>
                                                    <div class="modal-con">
                                                        <ul class="button-group modal-con">
                                                            <li><label class="item-style modal-con margin-top-9p">Serving: </label></li>
                                                            <li>
                                                                <select class="modal-con inline-block width-65p margin-left-5p margin-right-5p" id="servingSize">
                                                                    <option value="0.25">1/4</option>
                                                                    <option value="0.3">1/3</option>
                                                                    <option value="0.5">1/2</option>
                                                                    <option value="0.6">2/3</option>
                                                                    <option value="0.75">3/4</option>
                                                                    <option value="1">1</option>
                                                                    <option value="1.25">1.25</option>
                                                                    <option value="1.5">1.5</option>
                                                                    <option value="1.75">1.75</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                </select>
                                                            </li>
                                                            <li><s:select cssClass="modal-con width-145p" id="servingUnit" list="servingUnitMap" name="servingUnit"></s:select></li>
                                                        </ul>
                                                    </div>
                                                    <div class="modal-con">


                                                        <div class="modal-con inline-block margin-left-20">
                                                            <label class="item-style modal-con">Fats: </label>
                                                        </div>
                                                        <div class="modal-con inline-block">
                                                            <input type="number" id="fats" min="0" max="999" step="0.1" class="modal-con width-65p fat-style" onkeydown="return isNumber(event)">
                                                        </div>
                                                        <div class="modal-con inline-block margin-left-33">
                                                            <label class="item-style modal-con">Carbs: </label>
                                                        </div>
                                                        <div class="modal-con inline-block">
                                                            <input type="number" id="carbs" min="0" max="999" step="0.1" class="modal-con width-65p carbs-style" onkeydown="return isNumber(event)">
                                                        </div>

                                                        <div class="modal-con inline-block">
                                                            <label class="item-style modal-con">Protein: </label>
                                                        </div>
                                                        <div class="modal-con inline-block">
                                                            <input type="number" id="protein" min="0" max="999" step="0.1" class="modal-con width-65p protein-style" onkeydown="return isNumber(event)">
                                                        </div>
                                                    </div>
                                                    <div class="modal-con total_sum-style width-100perc">
                                                        <div class="inline-block modal-con">
                                                            <label class="item-style modal-con">Total: </label>
                                                        </div>


                                                        <div class="inline-block margin-10-width-135per modal-con">
                                                            <s:label id="totalFoodFats" cssClass="modal-con total-sum-style"></s:label>
                                                        </div>
                                                        <div class="inline-block width-18 margin-52p-left modal-con">
                                                            <s:label id="totalFoodCarbs" cssClass="modal-con total-sum-style"></s:label>
                                                        </div>
                                                        <div class="inline-block width-14per margin-left-75 modal-con">
                                                            <s:label id="totalFoodProtein" cssClass="modal-con total-sum-style"></s:label>
                                                        </div>
                                                    </div>
                                                    <div class="modal-con align-center new-food-success"></div>
                                                    <div class="modal-con align-center new-food-error"></div>
                                                </div>
                                                <div class="modal-footer footer-style align-right">
                                                    <button id="btnSaveAndAddMore" type="button" class="modal-butt button radius margin-bottom-0 saveFoodBtn btn-teal">Save and Add More Food</button>
                                                    <button id="btnSaveFood" type="button" class="modal-butt button radius margin-bottom-0 saveFoodBtn btn-teal">Save</button>
                                                    <button id="btnCloseFoodDialog" type="button" class="button radius secondary margin-bottom-0">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="EditNameDiv" class="modal fade bs-example-modal-sm"
                                         tabindex="-1" role="dialog"
                                         aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-sm">

                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                        <h4 class="modal-title">Edit Log Name</h4>
                                                    </div>
                                                    <div class="modal-body padding-bottom-0">
                                                        <div>
                                                            <ul class="button-group modal-con">
                                                                <li><label class="item-style inline modal-con">
                                                                    Log Name:</label></li>
                                                                <li class="margin-left-5p"><s:textfield
                                                                        id="newLogName"

                                                                        maxlength="50" required="required" ></s:textfield></li>
                                                            </ul>
                                                        </div>

                                                        <div class="modal-con align-center log-error"></div>

                                                    </div>
                                                    <div class="modal-footer footer-style">


                                                        <button type="button"
                                                                class="modal-butt button radius margin-bottom-0 editFoodBtn"
                                                                onclick="editName(document.getElementById('newLogName').value)">Save</button>
                                                        <button class="modal-butt button radius margin-bottom-0 editFoodBtn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="columns medium-6 padding-left-0 common-class">
                                        <label class="heading-style">Nutrition Facts</label>
                                        <div id="nutriDetailDiv" class="section-style div-1-main height-470">

                                            <form id="nutritionInfoForm" action="saveSuggestedLog.action" style="display: none;">
                                                <s:hidden id="logId" name="logId"></s:hidden>
                                                <s:hidden id="suggestedLogId" name="suggestedLogId"></s:hidden>
                                                <s:hidden id="patientId" name="patientId"></s:hidden>
                                                <s:hidden id="newNote" name="note"></s:hidden>
                                                <s:hidden id="maxCount" name="maxCount"></s:hidden>
                                                <s:hidden id="isAdvSearch" name="isAdvSearch"></s:hidden>
                                                <s:hidden id="isFromError" name="isFromError"></s:hidden>
                                                <s:hidden id="redirectToPage" name="redirectToPage"></s:hidden>
                                                <s:hidden id="hasMissingFood" name="hasMissingFood"></s:hidden>
                                                <s:hidden id="foodLogName" name="foodLogName"></s:hidden>
                                                <s:hidden id="isFromLogbook" name="isFromLogbook"></s:hidden>
                                                <s:hidden id="logMealName" name="logNameFromFood"></s:hidden>
                                                <s:hidden id="txtHiddenMealName" name="logName"></s:hidden>
                                                <s:hidden id="txtMinorFoodName" name="minorFoodName"></s:hidden>
                                                <s:hidden id="txtUserFoodNotes" name="userFoodNotes"></s:hidden>
                                                <s:hidden id="txtIsDraftedOrSendToMember" name="isDraftedOrSendToMember"></s:hidden>
                                            </form>

                                            <div id="nutritionMessage" class="success-msg-style"></div>
                                            <s:actionerror id="nutritionError" name="actionMessage" cssClass="alert error alert-enhancment"/>

                                            <s:form id="mealFoodForm" theme="simple" action="addMealFood.action" cssClass="hide">
                                                <s:hidden id="logId" name="logId"></s:hidden>
                                                <s:hidden id="suggestedLogId" name="suggestedLogId"></s:hidden>
                                                <s:hidden id="foodId" name="foodId"></s:hidden>
                                                <s:hidden id="foodName" name="foodName"></s:hidden>
                                                <s:hidden id="foodCarbs" name="foodCarbs"></s:hidden>
                                                <s:hidden id="foodFats" name="foodFats"></s:hidden>
                                                <s:hidden id="foodProtein" name="foodProtein"></s:hidden>
                                                <s:hidden id="foodServingSize" name="servingSize"></s:hidden>
                                                <s:hidden id="foodServingUnit" name="servingUnit"></s:hidden>
                                                <s:hidden id="logMealName" name="logNameFromFood"></s:hidden>
                                                <s:hidden id="txtMinorFoodName" name="minorFoodName"></s:hidden>
                                                <s:hidden id="txtIsDraftedOrSendToMember" name="isDraftedOrSendToMember"></s:hidden>
                                            </s:form>
                                            <s:form id="delFoodForm" theme="simple" action="deleteMealFood.action" cssClass="hide">
                                                <s:hidden id="logId" name="logId"></s:hidden>
                                                <s:hidden id="suggestedLogId" name="suggestedLogId"></s:hidden>
                                                <s:hidden id="delMealId" name="mealId"></s:hidden>
                                                <s:hidden id="delFoodId" name="foodId"></s:hidden>
                                                <s:hidden id="logMealNameDel" name="logNameFromFood"></s:hidden>
                                                <s:hidden id="txtMinorFoodName" name="minorFoodName"></s:hidden>
                                                <s:hidden id="txtIsDraftedOrSendToMember" name="isDraftedOrSendToMember"></s:hidden>
                                            </s:form>

                                            <div style="background: #E6E6E6;">
                                                <table style="width: 100%;margin-bottom: 0px;border: 0px !important;">
                                                    <tr style="background: #E6E6E6;">
                                                        <td class="first-col"><label style="font-weight: bold; cursor: default;">Food</label></td>
                                                        <td class="second-col"><label style="font-weight: bold; cursor: default;">Serving</label></td>
                                                        <td class="second-col"><label style="font-weight: bold; cursor: default;">Unit</label></td>
                                                        <td class="third-col"><label style="font-weight: bold; cursor: default;">Fat</label></td>
                                                        <td class="forth-col"><label style="font-weight: bold; cursor: default;">Carbs</label></td>
                                                        <td class="fifth-col"><label style="font-weight: bold; cursor: default;">Protein</label></td>
                                                        <td class="sixth-col foodAction">
                                                                <%-- <s:if test="%{log.isProcessed == null || log.isProcessed == 0}"> --%>
                                                            <label style="font-weight: bold; cursor: default;">Action</label>
                                                                <%-- </s:if> --%>
                                                        </td>
                                                    </tr>
                                                    <tr class="extra-tr">
                                                        <td class="first-col"></td>
                                                        <td class="second-col"></td>
                                                        <td colspan="4" class="third-col">
                                                            <label class="per-serving">(per serving)</label>
                                                        </td>
                                                        <td class="sixth-col foodAction">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <div class="div-suggested-food-detail">
                                                <s:if test="%{log.foodLogSummary == null ||
									      						log.foodLogSummary.foodLogDetails.size() == 0}">

                                                    <div class="nutriFoodSpan">No Food Added</div>
                                                    <div class="foodDiv"><table id="nutTable" style="width: 100%;margin-bottom: 0px;border: 0px !important;"></table></div>
                                                </s:if>
                                                <s:else>
                                                    <div class="nutriFoodSpan" style="display: none;">No Food Added</div>
                                                    <div class="foodDiv">
                                                        <table id="nutTable" style="width: 100%;margin-bottom: 0px;border: 0px !important;">

                                                            <s:iterator value="%{log.foodLogSummary.foodLogDetails}" var="foodLogDetail">
                                                                <!-- Store here -->
                                                                <tr class="nutTableRow<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>" data-trid="<s:property value='#foodLogDetail.foodMaster.foodMasterId'/>">
                                                                    <td class="first-col capital newFoodName<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>">
                                                                        <s:property value="#foodLogDetail.foodMaster.foodName"/>
                                                                    </td>
                                                                    <td class="second-col foodServing<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>">
                                                                        <s:property value="@com.healthslate.patientapp.util.CommonUtils@getCalculatedSize(#foodLogDetail.numberOfServings,'')" />
                                                                    </td>
                                                                    <td class="second-col foodServing<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>">
                                                                        <s:property value="#foodLogDetail.foodMaster.servingSizeUnit"/>
                                                                    </td>

                                                                    <td class="third-col foodFats<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>">
                                                                        <s:property value="@com.healthslate.patientapp.util.CommonUtils@getCalculatedSize(#foodLogDetail.foodMaster.fats,'')" />
                                                                    </td>
                                                                    <td class="forth-col foodCarbs<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>">
                                                                        <s:property value="@com.healthslate.patientapp.util.CommonUtils@getCalculatedSize(#foodLogDetail.foodMaster.carbs,'')" />
                                                                    </td>
                                                                    <td class="fifth-col foodProtein<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>">
                                                                        <s:property value="@com.healthslate.patientapp.util.CommonUtils@getCalculatedSize(#foodLogDetail.foodMaster.protein,'')" />
                                                                    </td>


                                                                    <td class="foodAction sixth-col">
                                                                        <s:if test="%{isFromError == 'yes'}">
                                                                            <div id="foodActionInError">
                                                                                <img
                                                                                        data-id="<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>"
                                                                                        alt="Edit Food" title="Edit Food" class="nav-icons edit-icon"
                                                                                        src="${pageContext.request.contextPath}/resources/images/Edit.png">
                                                                                <img
                                                                                        data-id="<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>"
                                                                                        alt="Delete Food" title="Delete Food" class="nav-icons del-icon"
                                                                                        src="${pageContext.request.contextPath}/resources/images/delete.png">
                                                                            </div>
                                                                        </s:if>
                                                                            <%-- <s:if test="%{log.isProcessed == null || log.isProcessed == 0}"> --%>
                                                                        <div>
                                                                            <img
                                                                                    data-id="<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>"
                                                                                    alt="Edit Food" title="Edit Food" class="nav-icons edit-icon"
                                                                                    src="${pageContext.request.contextPath}/resources/images/Edit.png">
                                                                            <img
                                                                                    data-id="<s:property value="#foodLogDetail.foodMaster.foodMasterId"/>"
                                                                                    alt="Delete Food" title="Delete Food" class="nav-icons del-icon"
                                                                                    src="${pageContext.request.contextPath}/resources/images/delete.png">
                                                                        </div>
                                                                            <%-- </s:if> --%>
                                                                    </td>
                                                                </tr>

                                                            </s:iterator>
                                                        </table>
                                                    </div>
                                                </s:else>
                                            </div>
                                            <div class="total-div-style">
                                                <table class="total-table-style">
                                                    <tr class="total-tr-style">
                                                        <td class="first-col"><span class="total-td-style" style="font-weight: bold">Total (grams)</span></td>
                                                        <td class="total-td-style second-col"></td>
                                                        <td class="total-td-style second-col"></td>

                                                        <td class="total-td-style third-col"><label class="total-td-style" id="totalFats" class="bold-style"></label></td>
                                                        <td class="total-td-style forth-col"><label class="total-td-style" id="totalCarbs" class="bold-style"></label></td>
                                                        <td class="total-td-style fifth-col"><label class="total-td-style" id="totalProtein" class="bold-style"></label></td>
                                                        <td class="total-td-style sixth-col"><button id="btnAddFood" class="modal-show button success radius btn-teal margin-bottom-0 small expand">Add Food</button></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="background: #E6E6E6;">
                                                <table style="width: 100%;margin-bottom: 0px;border: 0px !important;">
                                                    <tr style="background: #E6E6E6;">
                                                        <td class="first-col"><span style="font-weight: bold;">Calories (%)</span></td>
                                                        <td class="second-col"></td>
                                                        <td class="second-col"></td>
                                                        <td class="third-col"><label id="percentageOfCalsInFats" ></label></td>
                                                        <td class="forth-col"><label id="percentageOfCalsInCarbs" ></label></td>
                                                        <td class="fifth-col"><label id="percentageOfCalsInProtein" ></label></td>
                                                        <td class="sixth-col">
                                                            <label class="inline-block" id="totalSum"></label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="total-cal-style">
                                                <label>(Total Cal)</label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="columns medium-6 div-2 padding-right-0">
                                        <label class="heading-style modal-con">Food Image</label>
                                        <div class="responsive-container modal-con">
                                            <div class="dummy modal-con"></div>

                                            <div class="img-container modal-con">
                                                    <%-- <img id="foodImage" src="data:image/<s:property value='%{encodedFoodImage}'/>" title="<s:property value='%{log.foodLogSummary.foodImage.imageName}'/>" class="images width-100-height-100 modal-con"/> --%>
                                                <img style="cursor: pointer;" id="foodImage" onclick="showFullSizedFoodImage()" src="<s:property value='%{foodImageLocation}'/>" title="<s:property value='%{log.foodLogSummary.foodImage.imageName}'/>" class="images width-100-height-100 modal-con height-470"/>

                                                <label class="uploading-image" id="uploadingImage"><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Uploading...</label>

                                            </div>
                                            <a href="javascript:;" class="profile-upload" onclick="$(this).blur(); $('#uploadFile').val(''); $('#uploadFile').click();">Change Food Image</a>
                                        </div>
                                        <div id="foodImageDialog" class="reveal-modal large">
                                            <h3 class="dialog-header meal-summary-text"> Full size image </h3>
                                            <img id="foodImageDialogImage" src="">
                                            <a class="close-reveal-modal">&#215;</a>
                                        </div>
                                    </div>

                                </div>

                            </s:if>

                            <s:else>
                                <div class="patHeadDiv"><label class="heading-blue">Detail</label></div>
                                <table class="section-style">
                                    <tr>
                                        <td class="first-column">Log Time:</td>
                                        <td>
                                            <s:property value="@com.healthslate.patientapp.util.CommonUtils@timeInUserTimezone(log.getLogTime(), log.getLogTimeOffset(), log.getOffsetKey())" />
                                        </td>
                                    </tr>
                                </table>
                            </s:else>
                        </div>
                    </div>
                </div>
                <div class="tab-content current common-class">
                    <div style="text-align: right; display:block;max-width: 100%;" class="row collapse">

                        <s:form theme="simple" id="commentForm" action="addComments.action" cssClass="margin-bottom-0">
                            <s:hidden id="logId" name="suggestedLogId"></s:hidden>
                            <s:hidden id="patientId" name="patientId"></s:hidden>
                            <s:hidden id="suggestedLogId" name="suggestedLogId"></s:hidden>
                            <s:hidden id="providerId" name="providerId"></s:hidden>
                            <s:hidden id="note" name="note"></s:hidden>
                            <s:hidden id="txtMinorFoodName" name="minorFoodName"></s:hidden>
                        </s:form>

                        <s:form theme="simple" id="updatelogForm" action="processLog.action" cssClass="margin-bottom-0">
                            <s:hidden id="logId" name="suggestedLogId"></s:hidden>
                            <s:hidden id="suggestedLogId" name="suggestedLogId"></s:hidden>
                            <s:hidden id="logProcessStatus" name="logProcessStatus"></s:hidden>
                            <s:hidden id="professionalNote" name="note"></s:hidden>
                            <s:hidden id="txtMinorFoodName" name="minorFoodName"></s:hidden>
                        </s:form>
                        <div class="submit-btns">
                            <button onclick="saveSuggestedLog();" class="button success radius margin-bottom-0 btn-green margin-right-10">Send Suggested Meal to Member</button>
                            <s:if test="%{suggestedLog.isDrafted == null || suggestedLog.isDrafted == false}">
                                <button onclick="saveSuggestedMeal(false);" class="button secondary radius margin-bottom-0 right">Save as Draft</button>
                            </s:if>
                        </div>
                    </div>
                </div>

            </div>


    </div>
</div>

                 </div>

                <!-- Left grey area -->
                <div id="divCoachesAreaLeft" style="display: none;" data-equalizer-watch>
                    <script>
                        var patId = '${patientId}',
                                loggedInUserID = '${session.PROVIDER.providerId}';
                        $(document).ready(function(){
                            $("#divCoachesAreaLeft").load("${pageContext.request.contextPath}/educator/coachesArea.jsp");
                        });
                    </script>
                </div>
             </div>
         </div>
     </div>
 </div>

<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
<![endif]-->
<!--[if (gte IE 9) | (!IE)]><!-->
<script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
<!--<![endif]-->

<!-- Foundation 3 for IE 8 and earlier -->
<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
<![endif]-->

<!-- Foundation 4 for IE 9 and later -->
<!--[if gt IE 8]><!-->
<script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
<!--<![endif]-->


<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/select2-3.5.2/select2.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap-typeahead.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/date.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.autosize.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
<script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
<script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/hs.suggested_nutrition_facts.js?v=0.11"></script>

<script type="text/javascript">

    $(document).foundation();

    var foodName ;
    var mealOptions = [], srcMealId = "", foodSuggestions = [], foodId = "",mealJson = "", foodNameSuggest = "", foodJson = "", mealNamePart1 = "",mealNamePart2 = "";
    var deleteFoodId ;
    var foodSummaryAddedList = [] ;

    /*Repacement Constants */
    var portionSizeR = "(portion size)";
    var foodR = "(food)";
    var theEachR = "the / each";
    var drinkR = "(drink)";
    var dash = "______";
    var foodNamesList = new Array();

    //__oz
    var FIRST_TIME_REFRESH = false,
        msgsTotalCount = "${messagesTotalCount}",
        globalFoodId, LABEL_COACH = "Coach",
        coachMessages = null,
        patId = "${patientId}",
        TOTALS_CFPC = {
            "carbs": "${totalCarbs}",
            "fats": "${totalFats}",
            "proteins": "${totalProtein}",
            "calories": "${totalCalories}"
        },
        nutritionFacts = Object.create(NutritionFacts),
        patId = '${patientId}',
        loggedInUserID = '${session.PROVIDER.providerId}',
        validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"],
        CEILINGS = $.parseJSON('${targetString}'),
        CURRENT_MEAL_CEILING = 0;

    function replaceQuotes(val){
        return val.replace(/"/g, '\'');
    }

    function hideElement(element){
        if(element.is(":visible")){
            setTimeout(function(){
                $(element).slideUp("slow");
            }, 3000);
        }
    }

    function removeDuplicateRows(){
        var idArray = $('[data-trid]').map(function(index, r){
            var c = $(r).attr("class"),
                    start = "nutTableRow".length;

            return c.substring(start, c.length);
        });

        if(idArray && idArray.length > 0){
            $.each(idArray, function(index, id){

                var $tr = $("tr.nutTableRow"+id);
                if($tr.length > 1){
                    $tr.not(":last").remove();
                }
            });
        }
    }

    $(function() {
        $("#loading").show();
        removeDuplicateRows();
        if(CEILINGS['${log.foodLogSummary.type}']){
            CURRENT_MEAL_CEILING = CEILINGS['${log.foodLogSummary.type}'];
        }

        $('#unProcessedMeals').addClass('selected-tab');
        $("#userFoodNotes");//.prop("maxlength", 250);
        bindUploadImage();

        $("#btnDialogYesSuggestedMeal").on("click", function(e){
            e.preventDefault();
            $("#dialogError").foundation("reveal", "close");
            saveSuggestedMeal(true);
        });

        nutritionFacts.init("${pageContext.request.contextPath}", '${mealJson}', '${foodJson}');

        //load coaches area via ajax
        //Ajaxing.loadExternalPage($("#divCoachesAreaLeft"), "${pageContext.request.contextPath}/educator/coachesArea.jsp", loadExpCollapse);
        Ajaxing.loadExternalPage($("#divMemberNavArea"), "${pageContext.request.contextPath}/educator/memberNavArea.jsp", function(){
            $("#divMemberNavArea ul").css("top", "10px");
        });

        $("#loading").hide();
    });

    function getParameterByName( name ){
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)",
                regex = new RegExp( regexS ),
                results = regex.exec( window.location.href );
        if( results == null ){
            return "";
        } else{
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

    function getFormatedDateTime(date){
        var d = new Date(date);

        var month = d.getMonth()+1;
        var day = d.getDate();
        var hour = d.getHours();
        var minute = d.getMinutes();
        var ampm = 'AM';
        if(hour > 12){
            ampm = 'PM';
            hour = hour - 12;
        }

        if(hour != 12){
            hour = "0"+hour;
        }

        if(minute < 10){
            minute = "0"+minute;
        }

        var date = ((''+month).length<2 ? '0' : '') + month + '/' +
                ((''+day).length<2 ? '0' : '') + day + '/' +
                d.getFullYear();

        var time = ((''+hour).length<2 ? '0' :'') + hour + ':' +
                ((''+minute).length<2 ? '0' :'') + minute +" "+ ampm ;

        return date+" "+time;
    }

    function isNumber(evt) {

        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if ((charCode > 47 && charCode < 58) ||
                (charCode == 38) || (charCode == 40) ||
                (charCode == 9) || (charCode == 16) ||
                (charCode > 95 && charCode < 106) ||
                (charCode == 8) || charCode == 110 || charCode == 190) {

            return true;
        }
        return false;
    }

    function saveSuggestedLog(){

        try {

            if(CURRENT_MEAL_CEILING == 0 || CURRENT_MEAL_CEILING == "0"){
                saveSuggestedMeal(true);
            }else{
                var $totalCarbs = $("#totalCarbs"),
                    addedCarbs = parseFloat($totalCarbs.text());

                //if added carbs less than meal ceiling then let it save
                if(addedCarbs <= CURRENT_MEAL_CEILING){
                    saveSuggestedMeal(true);
                }else{
                    var minusCarbs = parseFloat(addedCarbs - CURRENT_MEAL_CEILING);
                    $("#spExceeded").text("("+ Math.round(minusCarbs)+"g)");
                    $("#dialogError").foundation("reveal", "open");
                }
            }
        } catch (e) {

        }
    }

    function saveSuggestedMeal(isDrafted){
        $("#loading").show()
        $("#newNote").val($('.logNote').val());
        $("#hasMissingFood").val($("#missingFood").is(":checked"));
        $("#foodLogName").val(replaceQuotes($("#logNameFromFood").val()));
        $("#txtHiddenMealName").val(replaceQuotes($("#logNameFromFood").val()));
        $("#txtMinorFoodName").val($("#minorFoodName").val());
        $("#txtUserFoodNotes").val($("#userFoodNotes").val());
        $("#txtIsDraftedOrSendToMember").val(isDrafted);
        $("#nutritionInfoForm").submit();
    }

    function bindUploadImage(){

        $("#uploadFile").on("change", function(){

            var file = $(this)[0].files[0],
                fileName = file.name,
                extractFileExtension = getFileExtension(fileName);
            if(validFileExtensions.indexOf(extractFileExtension) == -1){
                alert("Only image files uploading supported");
                return;
            }

            var $formUpload = $("#formUpload").off("submit"),
                formData = new FormData($formUpload[0]),
                $imgLoading = $("#uploadingImage").show();

            formData.append("logId", '${logId}');

            $formUpload.on("submit", function () {
                $.ajax({
                    url: "uploadSuggestedMealLogImage.action",
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        try {
                            data = $.parseJSON(data);
                            $imgLoading.hide();
                            if (data && data.STATUS == "SUCCESS") {
                                var fileUploaded = $("#uploadFile")[0].files[0],
                                    tempUrl = URL.createObjectURL(fileUploaded);

                                $("#foodImage").attr({"src": tempUrl});
                            }
                        } catch (err) {
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $imgLoading.hide();
                        console.log("ERROR", jqXHR, jqXHR.status);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            });

            $formUpload.submit();
        });
    }

    function getFileExtension(filename) {
        return typeof filename != "undefined" ? filename.substring(filename.lastIndexOf("."), filename.length).toLowerCase() : false;
    }

    function showFullSizedFoodImage() {
        $("#foodImageDialogImage").prop("src", $("#foodImage").prop("src"));
        $("#foodImageDialog").foundation("reveal", "open");
    }

</script>
</body>
</html>