<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>

   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->

	<link rel="stylesheet"	href="${pageContext.request.contextPath}/resources/css/queue.style.css" />
	<title>UnProcessed Meals</title>
	
	<style type="text/css">
		.input-field {
			width: 35% !important;
		}
		
		.ui-datepicker-trigger {
			margin: 0px;
			height: 37px;
			padding-top: 11px;
			width: 5px;
			padding-left: 1rem;
			background-color: #428bca;
		}
		
		.date-style {
			width: 82%;
			display: inline-block !important;
		}
		
		.header-style {
			width: 60%;
			padding: 0;
			top: 10px;
		}
		
		.ui-datepicker-trigger {
			margin: 0px;
			height: 37px;
			padding-top: 17px;
			width: 5px;
			padding-left: 1rem;
		}
		
		.dataTables_processing {
			position: absolute;
			top: 2% !important; 
			left: 50%;
			width: 250px;
			height: 30px;
			margin-left: -125px;
			margin-top: -15px;
			padding: 14px 0 2px 0;
			border: 0px !important; 
			text-align: center;
			color: #999;
			font-size: 14px;
			background: inherit !important;
			font-weight: bold;
			color: brown;
		}
		.not-started-status{
			color: red;
		}
		
		.in-process-status{
			color: orange;
		}
		
		.filter-div-2 {
			height:auto;
			background-color: #fff;
			width: 100%;
			padding-top: 5px;
			margin-bottom: 18px;
			padding-left: 20px;
			text-align: left;
		}

        .footer{
            margin-top: 80px !important;
        }
	</style>
</head>

<body>

	<br/>
	
	<div id="loading" class="loader" style="display: block;">
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>
			<div class="progress progress-striped active">
				<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
		</div>
	</div>

	<div id="infoSuccess" class=" alert success alert-new-message"></div>
	<input type="hidden" name="maxCount" id="maxCount" />

    <div class="max-width-1129 margin-bottom-20">
        <div class="row">
            <div class="columns medium-12">
                <div class="hs-page-heading">
                    <h2>
                        Unprocessed Meals
                    </h2>
                </div>
                <div id="tableDiv" class="main-page padding-top-28 top-0 green-bordered">
                    <div class="container" id="unprocessedTable">
                        <table id="table" class="table border-none">
                            <thead>
                                <tr>
                                    <th class="unprocessedTableColumn">Log Time</th>
                                    <th>Member Name</th>
                                    <th>Meal Type</th>
                                    <th>Status</th>
                                    <%--<th>Message</th>--%>
                                    <th>Time since Logged</th>
                                    <th onclick="return check();">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<script	src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	<script	src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>
		
	<script type="text/javascript">
	
		var CurrenDisplayValue,
			oTable,
			showRowCount = 0;
		
		$(document).ready(function(){

			$("#unProcessedMeals").addClass("selected-tab").parents("ul").prev("li").addClass("selected-tab");

			var d = new Date(); // for now
			
			d.getHours(); // => 9
			d.getMinutes(); // =>  30
			d.getSeconds(); // => 51
			
			$('#timeFromProp').text(d.getMonth()+1 + "/" + d.getDate() + "/" + d.getFullYear() );
			$('#timeToProp').text(d.getMonth()+1 + "/" + d.getDate() + "/" + d.getFullYear() );
            setFooterAlignment(120);
			$.fn.dataTableExt.sErrMode = 'throw';
			
			  oTable=  $('#table').dataTable(
			  {
				"sDom" : "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'spanPagination'p>>",
				"sPaginationType" : "bootstrap",
				"oLanguage" : {
					"sLengthMenu" : "_MENU_ records per page" // how many records you want to display per page
				},
                 "fnInitComplete": function (oSettings, json) {
                    //remove sorting class from td, __oz
                    $(".sorting_1").removeClass("sorting_1 sorting_1");
                 },
                  "fnDrawCallback": function( oSettings ) {
                      $(".sorting_1").removeClass("sorting_1 sorting_1");
                  },
				"bServerSide": true ,
				
		 		"sAjaxSource": "fetchUnProcessedMeals.action", // return jsonresponse with aaData json object
				"fnServerParams": function ( aoData ) {					
				      aoData.push( 
// 				    		  	   {"name": "dateFrom", "value": $('#fromDate').val()},
// 				    		  	   {"name": "dateTo", "value": $('#toDate').val()},
// 				    		  	   {"name": "basicstatus", "value": $('#basicStatusMap').val() },
// 				    		  	   {"name": "advstatus", "value": $('#advStatusMap').val()},
// 				    		  	   {"name": "logstatus", "value": $('#basicLogMap').val()},
// 				    		  	   {"name": "isAdvSearch", "value": $('#isAdvSearch').val()},				    		  	 	
// 				    		  	   {"name": "patientId", "value": $('#patientId').val()}				    		  	 				    		  	
				    		  	 );
				},  
				
				"bRetrieve" : true, // retrieve datatble object
				"bPaginate" : true, // enable disable pagination
				"bStateSave" : false, // saveState in cookie
				"bSort" : true, 
				"aaSorting": [[ 0, "desc" ]],
				"bProcessing": true, // processing text in while processing
	 			"aoColumns": [			                   
			                   { "mDataProp": function(data, type, full){

								   var signatureHtml = "<span class='signature'>Not Signed</span>";

								   if(data.isSigned){
									   signatureHtml = "";
								   }
			                	   	return "<span class='no-line-break'>"+data.logTimeString+"</span>"+signatureHtml;
			                   }},
			                   
			                   { "mDataProp": "patientName"},
			                   { "mDataProp": "MealType"},			                   
			                   { "mDataProp": function (data, type, full){
									
			                	   //set status coloring, __oz
			                	   var dtStatus = data.status,
			                	   	   statusClass = "";

			                	   if(dtStatus == "In Process"){
			                		   statusClass = "in-process-status label warning unprocess-status";    
			                	   }
			                	   return "<span class='"+statusClass+"'>"+dtStatus+"</span>";
								}
								,'bSortable': false},

			                   { "mDataProp": "minutesHours", "bSortable": false},
			                   { "mDataProp": function ( data, type, full ){
									return '<a href="${pageContext.request.contextPath}/app/educator/fetchLogDetail.action?redirectToPage=unprocessedmeals&logId='+data.logId+'&amp;patientId='+data.patientId+'&amp;isFromError=no"><button class="button radius tiny btn-teal margin-bottom-0 expand">Log Detail</button></a>';
									}
								,'bSortable': false},
			                 ]
			  }); 
	 
			initDatePicker("fromDate");
			initDatePicker("toDate");
			
			$('.row').css({'max-width':'100%', 'margin-top':'-9'});

			//init auto-refresh logs from server
			initRefreshLogsTimer();		
			
			//fixed paging styling
			fixedPaginationStyling();
			
			//first get newly processed meals
			getNewUnprocessedMeals();

            //remove sorting class from td, __oz
            $(".sorting_1").removeClass("sorting_1 sorting_1");

			$("#loading").hide();
		}); 
				
		//init auto-refresh logs from server
		function initRefreshLogsTimer(){			
			setInterval(getNewUnprocessedMeals, 20000);
		}
		
		//first get newly processed meals
		function getNewUnprocessedMeals(){
			var actionUrl = "findNewUnProcessedMeals1.action",
				dataToSend = {
					"maxCount": $("#maxCount").val(),
				},
				postRequestActions = {
					"requestType" : "GET",
			 	  	"successCallBack" : showAlertIfFound
			 	};
		
			if(isNaN($("#maxCount").val())){
				return;
			}
	 		sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
		}
		
		//fixed paging styling
		function fixedPaginationStyling(){
			var $ul = $(".dataTables_paginate ul");			
			$ul.each(function(){
				var $this = $(this);
				if($this && !$this.hasClass("pagination")){
					$this.addClass("pagination");
				}
			});
		}
		
		function showAlertIfFound(data){
			
			var $countCtrl = $("#maxCount"),
				oldCount = 0,
				newCount = 0,
				$infoMessage = $("#infoSuccess");
			
			//console.log(data);
			
			try{
				oldCount = parseInt($countCtrl.val());
				newCount = parseInt(data);				
			}catch(err){}
			
			$countCtrl.val(newCount);
			
			if(newCount > oldCount){
				$infoMessage.html("<div class='new-log-message'> New Log(s) Found </div>" + 
								  "<div class='refresh-link'>" +
								  "<a class='cursor-underline color-white-link' href='javascript:void(0);' onclick='refreshTable();'>Refresh</a> &nbsp;" +
								  "<a class='cursor-underline color-white-link' href='javascript:void(0);' onclick='dismissMessage();'>Dismiss</a>" +
								  "</div>").slideDown();
				
				$countCtrl.val(newCount);
				hideElement($infoMessage);				
			}
		}
		
		function hideElement(element){
			setTimeout(function(){
				$(element).slideUp("slow");
			}, 10000);
		}
		
		function refreshTable(){
			$("#infoSuccess").slideUp("slow"); 
			oTable.fnDraw();
			$("#loading").hide();
		}
		
		function dismissMessage(){
			$("#infoSuccess").slideUp("slow");
		}
		
	</script>
</body>
</html>