   <title>View | JSON Object</title>    
   
   <style type="text/css">
   
   body {   	
   	font-size: 14px;
	background: #f2f2f2;	
 	background-color: #f2f2f2;
 	overflow-x: hidden;
 	font-family:tahoma;
 	
   }
     label.inline{
         /*padding: 0px !important;*/
     }

     .border-style{
       top:5%;       
     }
     
     .txtStyle{
     	width: 100%;
		height: 40px;
		border: 1px solid #ccc;
		padding-left: 5px;		
		line-height: 40px;
		font-size: 22px;  
     	  
     }
     
     input[type=button]{
     	color: white !important;
     }
     
     #viewUl{
     	margin: 0 !important;     	
     }
     
     .font-larger{
     	font-size: 15px;
		text-transform: uppercase;
		font-family: tahoma;
		font-weight: bold;
     }
     
     .font-large{
     	font-size: 15px;  
     	font-family: tahoma;   	
     }
     
     .text-bold{
     	font-weight: bold;
     }
     
     .margin-top-20{
		margin-top: 20px;
	 }
	 
	 .border-style {		
		background-color: #fff !important;
		border-radius: 5px;
		position: relative;	
	 	border:1px solid #ccc;
	  	box-shadow: 0px 0px 5px #ccc;
	}
	
	.overflow-fix{
		position: relative;
	    overflow: hidden;
	}
	
	.row{
		max-width: 62.5rem;
		margin:0 auto
	}
	
	.small-6{
		width: 50%;
	}
	
	.float-left{
		float: left;
	}
	
	.float-right{
		float: right;
	}
	
	.view-registration ul{
		padding: 0;
		list-style-type: none;
		margin: 0;
	}
	
	.view-registration ul li{
		list-style-type: none;
		padding: 10px;		
		margin: 0;
		display: inline-block;
	}
	
   </style>

	 <div id="mainPage" class="row border-style margin-top-20 overflow-fix">
	  	<div align="center">              
	        <div class="view-registration">
	      		<ul id="viewUl">
	 				
				</ul>
	        </div>	      
	  	</div>  
	</div>
	   
   <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>                    
   <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>      
   
   <script>
        
   		var readableJson = {   				
                facilityName : "Facility Name",                
                contactPersonName : "Contact Person Name",                                
                address1 : "Address Line 1",
                address2 : "Address Line 2",
                contactInfo : "Contact Info",
                email: "Email",                
             	city : "City",
             	state : "State",
             	zip: "Zip",
             	country: "Country"           	             	
   		};
   		
   		function getReadableNameByKey(key){
   			return readableJson[key];   			
   		}

   	    function loadJsonObject(){
   	    	  
            var $ul = $("#viewUl");
           
            var jsonObject = $.parseJSON("${jsonString}");
            
            var jsonKeys = Object.keys(jsonObject);
            
            for(var i = 0; i < jsonKeys.length; i++){
            	
            	var key = jsonKeys[i];
            	var colName = getReadableNameByKey(key);
            	
            	if(colName !== undefined){            	
	            	var $li = $("<li/>"),
	            		$wrapper = $("<div/>",{"class":"row"}),
	            		$name = $("<div/>", {"class":"small-6 float-left font-large text-bold", "align":"left"}).text(colName),
	            		$value = $("<div/>", {"class":"small-6 float-right font-large", "align":"left"}).text(jsonObject[key]);
	            	
	            	$wrapper.append($name).append($value);
	            	
	            	$li.append($wrapper);
	            	
	            	$ul.append($li);            	
            	}
            }        
   	    }
   	    
		$(function() {					
			loadJsonObject();
		});
		
	</script>					
