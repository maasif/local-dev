<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
<head> 
	<meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="x-ua-compatible" content="IE=Edge"/>
	<meta http-equiv="EXPIRES" content="0" />
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-STORE">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="MUST-REVALIDATE">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="PRE-CHECK=0">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="POST-CHECK=0">
	<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
	
	<title><tiles:insertAttribute name="title" ignore="true" /></title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/semantic.min.css" />
    <script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>

    <style type="text/css">
        body{
            background: url('${pageContext.request.contextPath}/resources/css/images/body_pattern.png') repeat !important;
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <div id="header"><tiles:insertAttribute name="header" /></div>
	<div id="holder">	
	    <div id="body"><tiles:insertAttribute name="body" /></div>
	    <%-- <div id="footer"><tiles:insertAttribute name="footer" /></div> --%>
	</div>
</body>
</html>