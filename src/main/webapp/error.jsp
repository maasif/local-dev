<!doctype html>
<%-- <%@ taglib prefix="s" uri="/struts-tags"%> --%>
<html>
<head>
	<title>Reset Password</title>
	<meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0" />
	<meta http-equiv="x-ua-compatible" content="IE=Edge"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-3/css/bootstrap.min.css" />
	<!--[if (gte IE 9) | (!IE)]><!-->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation.min.css" />
	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->

	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/facility.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/login.style.css" />
	<script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>

	<style>
		.main-page{
			max-width: 680px;
		}
	</style>
</head>

<body>

<div id="mainPage" class="main-page overflow-fix">
	<div class="logo">
		<img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" />
	</div>

	<div class="padding-20">
		<h4 class="sign-in-text padding-20">Your link has been expired.</h4>
	</div>

</div>

<div id="footer" class="footer">

	<div class="row row-full">

		<div class="columns medium-12">
			<div class="copyright-healtshlate">
				<img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" />
			</div>
		</div>

		<%--<div class="columns medium-6">

			<span class="grey-separator"></span>

			<div class="about-healthslate">
				<h6 class="color-green">About HealthSlate</h6>
				<p class="our-mission">Our mission is to help with chronic diseases maintain their health through highly indisclosed programs of education, behavior medication and clinical support.
					<br/><br/>
					Combining expertise in consumer behavior, mobile technology and evidenced-based medication. HealthSlate improves patients' quality of life and health outcomes. Learn more at <a href="http://www.healthslate.com" target="_blank">www.healthslate.com</a>
				</p>
			</div>
		</div>--%>
	</div>

</div>

<!--[if (gte IE 9) | (!IE)]><!-->
<script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
<!--<![endif]-->

<!-- Foundation 4 for IE 9 and later -->
<!--[if gt IE 8]><!-->
<script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
<!--<![endif]-->

<script type="text/javascript">

	$(function(){
		var browserVersion = $.browser.version;
		if(!$.browser.msie || browserVersion >= 9){
			$(document).foundation();
		}
	});

</script>

</body>
</html>