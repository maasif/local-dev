<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/header.style.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/common.style.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css" />

	<style type="text/css">
		.footer{
			position: fixed;
			bottom: 0;
		}
	</style>
</head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DashBoard</title>

<body>
	<div class="row max-width-1129 top-30">

        <div class="columns medium-12">
            <h2 class="meal-summary-text">Welcome to Member application</h2>
            <h4>Some of the areas you will work with, includes:</h4>
            <ul class="teal-bullets">
                <li><label>Add/Edit Coaches</label></li>
                <li><label>Assign Lead Coaches to Members</label></li>
                <li><label>Invite Members to join HealthSlate Program</label></li>
            </ul>
        </div>
	</div>
</body>
</html>