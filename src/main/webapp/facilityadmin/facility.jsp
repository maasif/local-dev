<head>      
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Facility</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/facility.css" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

	<script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>

  </head>
  
  <body ng-app="FacilityModule">
	
	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>			
			<p id="progressTxt" class="progress-text">We're saving data</p>
		</div>
	</div>
	
	<div class="page-container max-width-1129">
	
	  <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>
	
      <div class="settings-container" ng-controller="FacilityController">

      	<form id="facilityForm" name="facilityForm" class="remove-margin" method="POST" class="margin-bottom-0" novalidate ng-submit="submitForm(facility)">

			<input type="hidden" name="facilityId" ng-model="facility.facilityId" />

			<div class="row row-full">
   	  				<div class="medium-12 columns">
   	  					<div class="border-bt-heading padding-left-0 hs-page-heading">
			         		<h2 class="align-page-heading">Facility</h2>
			         	</div>
   	  				</div>
				</div>
	    	  			  		 		  	 
	    	  	<div class="page-sections row row-full main-page-settings">   

	    	  		<div class="columns medium-6">

	    	  			<div class="colored-section border-orange">	       	  		  
			   	  			<div class="row row-full page-section">
			   	  				<div class="large-12 columns">
									<h4>DETAILS</h4>
			   	  					<label class="inline margin-bottom-0">Facility Name
										<input type="text" id="txtFacilityName" name="name" ng-model="facility.name" class="margin-bottom-0" required />
										<small class="error" ng-show="submitted && facilityForm.name.$error.required">Required.</small>
									</label>
									<label class="inline margin-bottom-0">State
                                        <select class="large-dropdown margin-bottom-0" name="state" ng-model="facility.state"
                                                ng-options="state as state for state in stateList track by state" required>

                                            <option value="" selected disabled>(select)</option>

                                        </select>
										<small class="error" ng-show="submitted && facilityForm.state.$error.required">Required.</small>
									</label>
									<label class="inline margin-bottom-0">City
										<input type="text" class="margin-bottom-0" name="city" ng-model="facility.city" required />
										<small class="error" ng-show="submitted && facilityForm.city.$error.required">Required.</small>
									</label>
									<label class="inline margin-bottom-0">Zip
										<input type="text" class="margin-bottom-0" name="zip" ng-model="facility.zip" required />
										<small class="error" ng-show="submitted && facilityForm.zip.$error.required">Required.</small>
									</label>
									<label class="inline margin-bottom-0">Address
										<textarea rows="2" class="margin-bottom-0" name="address" ng-model="facility.address" required></textarea>
										<small class="error" ng-show="submitted && facilityForm.address.$error.required">Required.</small>
									</label>
                                    <label class="inline margin-bottom-0">Timezone
                                        <select id="ddTimezone" class="large-dropdown margin-bottom-0" name="timezone" ng-model="facility.timezone"
                                                ng-options="timezone.name for timezone in timezoneList track by timezone.offset" required>
                                            <option value="" selected disabled>(select)</option>
                                        </select>
                                        <small class="error" ng-show="submitted && facilityForm.timezone.$error.required">Required.</small>
                                    </label>
			   	  				</div>
			    	  		</div>
			    	  	</div>

                        <div class="colored-section border-crimson" ng-show="leadCoachList.length > 0">
                            <div class="row row-full page-section">
                                <div class="large-12 columns">
                                    <h4>LEAD COACH</h4>
                                    <label class="inline margin-bottom-0">Select Lead Coach
                                        <select id="ddLeadCoach" class="large-dropdown margin-bottom-0" name="leadCoach" ng-model="facility.leadCoach"
                                                ng-options="l as (l.firstName + ' ' + l.lastName) for l in leadCoachList track by l.providerId" required>
                                            <option value="" selected disabled>(select)</option>
                                        </select>
                                        <small class="error" ng-show="submitted && facilityForm.leadCoach.$error.required">Required.</small>
                                    </label>
                                </div>
                            </div>
                        </div>

	   	  			</div>

	   	  			<div class="columns medium-6">
		   	  			<div class="colored-section border-darkgreen">
							<div class="row row-full page-section">
								<div class="large-12 columns">
									<h4>CONTACT</h4>
									<label class="inline margin-bottom-0">Contact Number
										<input type="text" class="margin-bottom-0" name="contactInfo" ng-model="facility.contactInfo" required />
										<small class="error" ng-show="submitted && facilityForm.contactInfo.$error.required">Required.</small>
									</label>
									<label class="inline margin-bottom-0">Contact Person Name
										<input type="text" class="margin-bottom-0" name="contactPersonName" ng-model="facility.contactPersonName" required />
										<small class="error" ng-show="submitted && facilityForm.contactPersonName.$error.required">Required.</small>
									</label>
                                    <label class="inline margin-bottom-0">
                                        <input type="checkbox" class="margin-bottom-0 large-chk" name="isNotificationEnabled" ng-model="facility.isNotificationEnabled" />
                                        <span class="align-sms-check">Notification Enabled</span>
                                    </label>
                                    <label class="inline margin-bottom-0">
                                        <input type="checkbox" class="margin-bottom-0 large-chk" name="isSkipConsent" ng-model="facility.isSkipConsent" />
                                        <span class="align-sms-check">Skip Consent Form</span>
                                    </label>
								</div>
							</div>
		   	  			</div>

                        <div class="colored-section border-darkblue hide">
                            <div class="row row-full page-section">
                                <div class="large-12 columns">
                                    <h4>FACILITY ADMIN</h4>
                                    <label class="inline margin-bottom-0">First Name
                                        <input id="txtFacilityAdminFirstName" type="text" name="facilityAdminFirstName" ng-model="facility.facilityAdminFirstName" class="validate-me login-input margin-bottom-0" required />
                                        <small class="error" ng-show="submitted && facilityForm.facilityAdminFirstName.$error.required" data-alert>Required.</small>
                                    </label>
                                    <label class="inline margin-bottom-0">Last Name
                                        <input id="txtFacilityAdminLastName" type="text" name="facilityAdminLastName" ng-model="facility.facilityAdminLastName" class="validate-me login-input margin-bottom-0" required />
                                        <small class="error" ng-show="submitted && facilityForm.facilityAdminLastName.$error.required" data-alert>Required.</small>
                                    </label>
                                    <label class="inline margin-bottom-0">Email address
                                        <input id="txtFacilityAdminEmail" type="email" name="facilityAdminEmail" ng-model="facility.facilityAdminEmail" ng-pattern="/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i" class="validate-me login-input margin-bottom-0" required />
                                        <small class="error" ng-show="submitted && facilityForm.facilityAdminEmail.$error.pattern" data-alert>Invalid email address.</small>
                                        <small class="error" ng-show="submitted && facilityForm.facilityAdminEmail.$error.required" data-alert>Required.</small>
                                    </label>
                                    <label class="inline margin-bottom-0">Phone
                                        <input id="txtFacilityAdminPhone" type="text" name="facilityAdminPhone" ng-model="facility.facilityAdminPhone" placeholder="(000) 000-0000" class="validate-me login-input margin-bottom-0" required />
                                        <%--<small class="error" ng-show="submitted && facilityForm.facilityAdminPhone.$error.pattern" data-alert>Please enter valid phone number.</small>--%>
                                        <small class="error" ng-show="submitted && facilityForm.facilityAdminPhone.$error.required" data-alert>Required.</small>
                                    </label>

                                    <div class="align-left">
                                        <a href="javascript:;" ng-click="resendEmailPinCode()" id="btnResendEmailAndCode" ng-show="isEdited == true && isFacilityAdminSet == true && facility.facilityAdminEmail && facility.facilityAdminPhone" class="button small radius hs-btns btn-teal margin-tb-0">Resend Email/Pin Code</a>
                                    </div>
                                </div>
                            </div>
                        </div>

	   	  				<div class="align-right padding-10 padding-right-0">
							<input type="submit" id="btnSaveFacility" ng-click="submitted=true" class="button small radius hs-btns btn-teal margin-tb-0" value="SAVE" />
						</div>
	   	  			</div>	    	  			    	  		 
	      	  	</div>        	  		   
      	  	</form>    	  
    	</div>    	
    </div>
    
	  <!--[if lt IE 9]>
	    	<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
		<![endif]-->
		<!--[if (gte IE 9) | (!IE)]><!-->
		    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>				       
		    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
		<!--<![endif]-->       
	        
	    <!-- Foundation 3 for IE 8 and earlier -->
		<!--[if lt IE 9]>	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
		<![endif]-->
		
		<!-- Foundation 4 for IE 9 and later -->
		<!--[if gt IE 8]><!-->	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
		<!--<![endif]-->

        <script src="${pageContext.request.contextPath}/resources/js/jquery.mask.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>

       <script type="text/javascript">
		   var FACILITY_LIST = ("${facilitiesListString}") ? $.parseJSON("${facilitiesListString}") : undefined,
               FACILITY_ADMINS_LIST = ("${facilityAdminListString}") ? $.parseJSON("${facilityAdminListString}") : undefined,
               LOADED_PAGE = "FACILITY_ADMIN",
               COACHES_LIST = ('${coachesListString}') ? $.parseJSON("${coachesListString}") : undefined;

           function removeCountryCode(phone){
               if(phone){
                   var countryCode = phone.indexOf("+1");

                   if(countryCode > -1){
                       phone = phone.substring(countryCode+3, phone.length);
                   }

                   return phone;
               }
           }

			$(function(){
				$(document).foundation();
				$("#facilityTab").addClass("selected-tab");

                //written in common.js
                formatPhoneNumber($("#txtFacilityAdminPhone"), $("#txtFacilityAdminPhone").val());
			});
       </script>

		<script src="${pageContext.request.contextPath}/resources/js/facility_controller.js?v=0.13"></script>
  </body>          
</html>