<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Join HealthSlate Program</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/>
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->

	<!--[if lt IE 9]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">
	<![endif]-->

	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css" />

   <!--[if lt IE 9]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

    <script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>

    <style type="text/css">
        .footer{
            position: absolute;
            bottom: 0;
        }

        .invite-members-container{
            margin: 50px auto;
            max-width: 700px;
        }

        .rounded{
            border-radius: 13px !important;
        }

        .padding-20{
            padding: 20px;
        }

        .bordered-grey{
            border:1px solid #ccc;
            border-bottom-width: 2px;
        }
    </style>
  </head>

  <body ng-app="InviteMembersModule">

      <div id="loading" class="loader position-fixed">
          <div class="image-loader">
              <p class="progress-please-wait">Please wait...</p>
              <div class="progress progress-striped active">
                  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                  </div>
              </div>
              <p id="progressTxt" class="progress-text">We're sending</p>
          </div>
      </div>

      <div class="invite-members-container">

          <div class="align-right">
              <a href="prospectiveMembers.action" class="button radius btn-teal min-width-250">Prospective Members</a>
          </div>

          <div class="bg-white padding-20 bordered-grey">
              <div class="row section-padding">
                  <div class="columns medium-12">
                      <div class="grey-area-section remove-border" ng-controller="InviteMembersController">
                          <h3 class="meal-summary-text">Enter prospective member's email address</h3>

                          <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>

                          <form id="formInviteMember" name="formInviteMember" novalidate method="POST" ng-submit="submitForm(patient)">

                                  <div class="row row-auto">

                                        <div class="columns medium-12 padding-left-0">
                                            <label class="inline meal-summary-text margin-bottom-0">
                                                <input type="email" name="email" ng-model="patient.email" class="rounded margin-bottom-0" ng-pattern="/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i" required />
                                                <small class="error" ng-show="submitted && formInviteMember.email.$error.pattern">Invalid email address.</small>
                                                <small class="error" ng-show="submitted && formInviteMember.email.$error.required">Required.</small>
                                            </label>
                                        </div>

                                  </div>

                                   <div class="row row-auto">

                                      <div class="columns medium-12 padding-left-0">
                                           <input type="submit" class="button radius btn-teal min-width-250" value="Send an Invite" />
                                      </div>

                                  </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>

    <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
    <![endif]-->
    <!--[if (gte IE 9) | (!IE)]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
    <!--<![endif]-->

    <!-- Foundation 3 for IE 8 and earlier -->
    <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
    <![endif]-->

    <!-- Foundation 4 for IE 9 and later -->
    <!--[if gt IE 8]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
    <!--<![endif]-->

      <script src="${pageContext.request.contextPath}/resources/js/jquery.mask.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/date.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
      <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
      <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>

      <script type="text/javascript">

        var BASE_URL = "${pageContext.request.contextPath}/",
            FACILITY_NAME = '${facilityName}';
        $(function(){
		    $(document).foundation();
            $("#inviteMembersTab").addClass("selected-tab").parents("ul").prev("li").addClass("selected-tab");;
        });

      </script>

      <script src="${pageContext.request.contextPath}/resources/js/controllers/invite_members_facility_controller.js"></script>
  </body>
</html>