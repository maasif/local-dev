<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Add/Edit Patient</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/chosen_v1.4.0/chosen.min.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/facility.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.css" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

	<script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>

  </head>
  
  <body ng-app="PatientModule">

  <!-- BreadCrumb -->
  <div class="breadcrumb-green">
      <div class="max-width-1129">
          <ul class="breadcrumbs">
              <li>
                  <a href="membersData.action">Members Data</a>
              </li>
              <li class="current">Member</li>
          </ul>
      </div>
  </div>

	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>			
			<p id="progressTxt" class="progress-text">We're saving data</p>
		</div>
	</div>
	
	<div class="page-container max-width-1129">
	
	  <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>

        <s:form id="formUpload" method="POST" enctype="multipart/form-data" cssClass="margin-bottom-0 hide">
            <s:file type="file" id="uploadFile" accept="image/*" name="uploadFile" cssClass="hide"></s:file>
            <s:hidden id="txtHiddenFileName" name="fileName"></s:hidden>
        </s:form>

      <div class="settings-container" ng-controller="PatientController">

      	<form id="patientForm" name="patientForm" method="POST" class="margin-bottom-0 remove-margin" novalidate ng-submit="submitForm(patient)">

			<input type="hidden" name="patientId" ng-model="patient.patientId" />
            <input type="hidden" name="facilityId" ng-model="patient.facilityId" />

	    	  	<div class="page-sections row row-full main-page-settings">

                    <!-- Image, Name, email etc. -->
                    <div class="row">
                        <div class="columns medium-2">
                            <a class="th">
                                <img id="imgProfile" src="${pageContext.request.contextPath}/resources/css/images/default_profile_image.gif" ng-src="{{patient.imagePath}}" class="profile-image" />
                                <a href="#" class="profile-upload" onclick="$(this).blur(); $('#uploadFile').val(''); $('#uploadFile').click();">Upload Profile Image</a>

                                <label class="uploading-image" id="uploadingImage"><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Uploading...</label>

                            </a>
                        </div>
                        <div class="columns medium-5">
                            <div class="grey-label-group">
                                <label class="inline uppercase margin-bottom-0">First Name</label>
                                <label class="inline margin-bottom-0">
                                    <input type="text" name="firstName" ng-model="patient.firstName" />
                                </label>
                            </div>
                            <div class="grey-label-group">
                                <label class="inline uppercase margin-bottom-0">Display Name</label>
                                <label class="inline margin-bottom-0">
                                    <input type="text" name="firstName" ng-model="patient.displayName" readonly />
                                </label>
                            </div>

                            <div class="grey-label-group">
                                <label class="inline uppercase margin-bottom-0">Date of Birth (MM/DD/YYYY)</label>
                                <label class="inline margin-bottom-0">
                                    <input id="txtDOB" type="text" name="dob" ng-model="patient.dob" />
                                    <small class="error" style="display: none">Required.</small>
                                </label>
                            </div>
                        </div>
                        <div class="columns medium-5">
                            <div class="grey-label-group">
                                <label class="inline uppercase margin-bottom-0">Last Name</label>
                                <label class="inline margin-bottom-0">
                                    <input type="text" name="lastName" ng-model="patient.lastName" />
                                </label>
                            </div>
                            <div class="grey-label-group">
                                <label class="inline uppercase margin-bottom-0">Email</label>
                                <label class="inline margin-bottom-0">
                                    <input type="text" name="email" ng-model="patient.email" readonly />
                                </label>
                            </div>
                            <div class="grey-label-group">
                                <label class="inline uppercase margin-bottom-0">Phone</label>
                                <label class="inline margin-bottom-0">
                                    <input id="txtPhone" type="tel" name="phone" placeholder="(000) 000-0000" ng-model="patient.phone" />
                                    <small class="error" style="display:none">Required.</small>
                                </label>
                            </div>
                        </div>
                     </div>

                    <div class="row">
                        <div class="medium-4 columns">
                            <div class="colored-section border-darkgreen">
                                <h4>DEMOGRAPHICS</h4>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">Gender</label>
                                    <label class="inline margin-bottom-0">
                                        <input type="radio" name="gender" class="margin-bottom-0" value="Male" ng-model="patient.gender" /> Male
                                        <input type="radio" name="gender" class="margin-bottom-0" value="Female" ng-model="patient.gender" /> Female
                                    </label>
                                </div>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">Nickname</label>
                                    <label class="inline margin-bottom-0">
                                        <input type="text" class="margin-bottom-0" name="nickname" ng-model="patient.nickname" />
                                    </label>
                                </div>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">Race</label>
                                    <label class="inline margin-bottom-0">
                                        <select class="large-dropdown margin-bottom-0" name="race" ng-model="patient.race"
                                                ng-options="item.name for item in racesList track by item.abbreviation">

                                            <option value="" selected disabled>(select)</option>

                                        </select>
                                    </label>
                                </div>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">State</label>
                                    <label class="inline margin-bottom-0">
                                        <select class="large-dropdown margin-bottom-0" name="state" ng-model="patient.state"
                                                ng-options="item.name for item in stateList track by item.abbreviation">

                                            <option value="" selected disabled>(select)</option>

                                        </select>
                                    </label>
                                </div>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">Education</label>
                                    <label class="inline margin-bottom-0">
                                        <select class="large-dropdown margin-bottom-0" name="education" ng-model="patient.education"
                                                ng-options="item.name for item in educationList track by item.abbreviation">

                                            <option value="" selected disabled>(select)</option>

                                        </select>
                                    </label>
                                </div>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">Language</label>
                                    <label class="inline margin-bottom-0">
                                        <select class="large-dropdown margin-bottom-0" name="language" ng-model="patient.language"
                                                ng-options="item.name for item in languageList track by item.abbreviation">

                                            <option value="" selected disabled>(select)</option>

                                        </select>
                                    </label>
                                </div>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">MRN</label>
                                    <label class="inline margin-bottom-0">
                                        <input id="txtMRN" type="text" class="margin-bottom-0" name="mrn" maxlength="16" ng-model="patient.mrn" />
                                    </label>
                                </div>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">Confirm MRN</label>
                                    <label class="inline margin-bottom-0">
                                        <input id="txtConfirmMRN" type="text" class="margin-bottom-0" maxlength="16" name="confirmMRN" ng-model="patient.confirmMRN" />
                                        <small class="error" style="display:none">Confirm MRN should match.</small>
                                    </label>
                                </div>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">Weight (lbs)</label>
                                    <label class="inline margin-bottom-0">
                                        <input type="text" name="weight" ng-model="patient.weight" />
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="medium-4 columns">
                            <div class="colored-section border-darkblue">
                                <h4>DIABETES INFORMATION</h4>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">Diagnosis</label>
                                    <label class="inline margin-bottom-0">
                                        <select class="large-dropdown margin-bottom-0" name="diagnosis" ng-model="patient.diagnosis"
                                                ng-options="item.name for item in diabetesList track by item.abbreviation">

                                            <option value="" selected disabled>(select)</option>

                                        </select>
                                    </label>
                                </div>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">Year of Diagnosis</label>
                                    <label class="inline margin-bottom-0">
                                        <select id="ddDiagnosisDate" class="large-dropdown margin-bottom-0" name="diagnosisDate" ng-model="patient.diagnosisDate"
                                                ng-options="year as year for year in yearsListDiag track by year">

                                            <option value="" selected disabled>(select)</option>

                                        </select>
                                    </label>
                                </div>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">Time since last Diabetes Education</label>
                                    <label class="inline margin-bottom-0">
                                        <select id="ddTimeSinceLastDiabetesEdu" class="large-dropdown margin-bottom-0" name="timeSinceLastDiabetesEdu" ng-model="patient.timeSinceLastDiabetesEdu"
                                                ng-options="year as year for year in yearsListDiab track by year">

                                            <option value="" selected disabled>(select)</option>

                                        </select>
                                    </label>
                                </div>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">Diabetes Support</label>
                                    <label class="inline margin-bottom-0">
                                        <select id="ddDiabetesSupport" class="margin-bottom-0" name="diabetesSupport" ng-model="patient.diabetesSupport"
                                                ng-options="item.name for item in diabetesSupportList track by item.abbreviation" multiple>

                                            <option value="" selected disabled>(select)</option>

                                        </select>
                                    </label>
                                </div>

                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">Being treated for depression currently?</label>
                                    <label class="inline margin-bottom-0">
                                        <input type="radio" name="depression" class="margin-bottom-0" value="Yes" ng-model="patient.treatedDepression" /> Yes
                                        <input type="radio" name="depression" class="margin-bottom-0" value="No" ng-model="patient.treatedDepression" /> No
                                        <input type="radio" name="depression" class="margin-bottom-0" value="Do not know" ng-model="patient.treatedDepression" /> Do not know
                                    </label>
                                </div>

                                <div class="grey-label-group hide">
                                    <label class="inline uppercase margin-bottom-0">Medications</label>
                                    <label class="inline margin-bottom-0">
                                        <select id="ddMedications" class="margin-bottom-0" name="medications" ng-model="patient.medications"
                                                ng-options="item.name for item in medicationList track by item.abbreviation" multiple>

                                            <option value="" selected disabled>(select)</option>

                                        </select>

                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="medium-4 columns">
                            <div class="colored-section border-orange">
                                <h4>DIETARY INFORMATION</h4>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">Dietary Restrictions</label>
                                    <label class="inline margin-bottom-0">
                                        <select id="ddDietaryRestrictions" class="margin-bottom-0" name="dietaryRestrictions" ng-model="patient.dietaryRestrictions"
                                                ng-options="item.name for item in dietaryRestrictionsList track by item.abbreviation" multiple>

                                            <option value="" selected disabled>(select)</option>

                                        </select>
                                    </label>
                                    <label class="inline margin-bottom-0">
                                        <input id="txtDietaryRestrictionsOther" type="text" placeholder="Enter Other text here..." name="dietaryRestrictionsOther" ng-model="patient.dietaryRestrictionsOther" maxlength="250" class="hide" />
                                        <small id="errorDietaryRestrictionsOther" class="error" style="display:none">Required.</small>
                                    </label>
                                </div>
                                <div class="grey-label-group">
                                    <label class="inline uppercase margin-bottom-0">Primary Meal Preparer</label>
                                    <label class="inline margin-bottom-0">
                                        <select id="ddMealPreparer" class="margin-bottom-0" name="mealPreparer" ng-model="patient.mealPreparer"
                                                ng-options="item.name for item in mealPreparerList track by item.abbreviation" multiple>

                                            <option value="" selected disabled>(select)</option>

                                        </select>
                                    </label>
                                </div>
                            </div>
                            <div class="align-right padding-10 padding-right-0">
                                <input type="submit" id="btnSavePatient" ng-click="submitted=true" class="button small radius hs-btns btn-teal margin-tb-0" value="SAVE" />
                            </div>
                        </div>
                    </div>
	      	  	</div>        	  		   
      	  	</form>    	  
    	</div>    	
    </div>
    
	  <!--[if lt IE 9]>
	    	<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
		<![endif]-->
		<!--[if (gte IE 9) | (!IE)]><!-->
		    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>				       
		    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
		<!--<![endif]-->       
	        
	    <!-- Foundation 3 for IE 8 and earlier -->
		<!--[if lt IE 9]>	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
		<![endif]-->
		
		<!-- Foundation 4 for IE 9 and later -->
		<!--[if gt IE 8]><!-->	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
		<!--<![endif]-->

       <script src="${pageContext.request.contextPath}/resources/chosen_v1.4.0/chosen.jquery.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/jquery.mask.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>

       <script type="text/javascript">

            var PATIENT_DETAILS = ('${patientDetailString}') ? $.parseJSON('${patientDetailString}'): undefined,
                APP_PATH = "${pageContext.request.contextPath}";
                IMAGES_PATH = APP_PATH + "/patientImages/",
                MEDICATIONS_LISTING = ('${medicationsListing}') ? $.parseJSON('${medicationsListing}'): undefined,
                REDIRECT_TO = "membersData.action";

			$(function(){
				$(document).foundation();
                $("#patientsTab").addClass("selected-tab");

                $("#txtDOB").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-100:+0",
                    onSelect: function(){
                        $(this).next("small").hide();
                    }
                });

                $("select[multiple]").chosen();

                //written in common.js
                formatPhoneNumber($("#txtPhone"), $("#txtPhone").val());

                /*$txtPhone.intlTelInput({
                    onlyCountries: ['us', 'gb', 'ca', 'pk'],
                    utilsScript: "${pageContext.request.contextPath}/resources/telephony/js/utils.js"
                });

                $txtPhone.on("keypress", function(){
                    if($(this).val().length > 0){
                        $(this).parent().next("small").hide();
                    }
                });*/

                bindUploadImage();
			});

           function bindUploadImage(){

               $("#txtDOB").on("keypress", function(){
                  if($(this).val()){
                      $(this).next("small").hide();
                  }
               });

               $("#uploadFile").on("change", function(){

                   var $formUpload = $("#formUpload").off("submit"),
                       formData = new FormData($formUpload[0]),
                       $imgLoading = $("#uploadingImage").show();

                   formData.append("patientId", PATIENT_DETAILS.patientId);

                   $formUpload.on("submit", function () {
                       $.ajax({
                           url: "uploadPatientImage.action",
                           type: 'POST',
                           data: formData,
                           success: function (data) {
                               try {
                                   data = $.parseJSON(data);
                                   $imgLoading.hide();
                                   if (data && data.STATUS == "SUCCESS") {
                                       var fileUploaded = $("#uploadFile")[0].files[0],
                                           tempUrl = URL.createObjectURL(fileUploaded);

                                       $("#imgProfile").attr({"src": tempUrl, "ng-src": tempUrl });
                                   }
                               } catch (err) {
                               }
                           },
                           error: function (jqXHR, textStatus, errorThrown) {
                               $imgLoading.hide();
                               console.log("ERROR", jqXHR, jqXHR.status);
                           },
                           cache: false,
                           contentType: false,
                           processData: false
                       });

                       return false;
                   });

                   $formUpload.submit();
               });
           }

       </script>

		<script src="${pageContext.request.contextPath}/resources/js/patient_controller.js?v=0.9"></script>
  </body>          
</html>