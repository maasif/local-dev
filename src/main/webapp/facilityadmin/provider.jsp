<head>      
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Provider</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/facility.css" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

	<script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>

  </head>
  
  <body ng-app="ProviderModule">

  <!-- BreadCrumb -->
  <div class="breadcrumb-green">
      <div class="max-width-1129">
          <ul class="breadcrumbs">
              <li>
                  <a href="coaches.action">Coaches</a>
              </li>
              <li class="current">Add/Edit Coach</li>
          </ul>
      </div>
  </div>

	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>			
			<p id="progressTxt" class="progress-text">We're processing</p>
		</div>
	</div>
	
	<div class="page-container max-width-1129">

        <div id="confirmationRemoveMemberDialog" class="reveal-modal small" data-reveal>
            <h3 class="meal-summary-text">Remove Coach</h3>
            <p class="lead dialog-para">Are you sure you want to remove this Coach?</p>
            <a id="closeConfirmDialog" class="close-reveal-modal">&#215;</a>
            <div class="align-right">
                <a href="javascript:;" id="btnConfirmDelete" class="button radius btn-teal margin-top-10 margin-bottom-0">Yes</a>
                <a href="javascript:;" class="button secondary radius margin-top-10 margin-bottom-0" onclick="$('#closeConfirmDialog').click(); return false;">No</a>
            </div>
        </div>

	  <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>
	
      <div class="settings-container" ng-controller="ProviderController">

      	<form id="providerForm" name="providerForm" method="POST" class="margin-bottom-0 remove-margin" novalidate ng-submit="submitForm(provider)">

			<input type="hidden" name="providerId" ng-model="provider.providerId" />
			<input type="hidden" name="userId" ng-model="provider.userId" />
			<input type="hidden" name="facilityId" ng-model="facilityId">
			<input type="hidden" name="designation" ng-model="designation">
            <input type="hidden" name="sortType" ng-model="sortType">
			
			<div class="row row-full">
   	  				<div class="medium-12 columns">
   	  					<div class="border-bt-heading padding-left-0 hs-page-heading">
			         		<h2 class="align-page-heading">Coach</h2>
			         	</div>
   	  				</div>
				</div>
	    	  			  		 		  	 
	    	  	<div class="page-sections row row-full main-page-settings">   

	    	  		<div class="columns medium-6">

	    	  			<div class="colored-section border-orange">	       	  		  
			   	  			<div class="row row-full page-section">
			   	  				<div class="large-12 columns">
									<h4>PERSONAL</h4>
			   	  					<label class="inline margin-bottom-0">First Name
										<input type="text" name="firstName" ng-model="provider.firstName" class="margin-bottom-0" required />
										<small class="error" ng-show="submitted && providerForm.firstName.$error.required">Required.</small>
									</label>
									<label class="inline margin-bottom-0">Last Name
										<input type="text" name="lastName" ng-model="provider.lastName" class="margin-bottom-0" required />
										<small class="error" ng-show="submitted && providerForm.lastName.$error.required">Required.</small>
									</label>
									<label class="inline margin-bottom-0">Type
										<select class="large-dropdown margin-bottom-0" name="userType" ng-model="provider.userType"
												ng-options="cType as cType for cType in typeList track by cType" required>
											<option value="" selected disabled>(select)</option>
										</select>
										<small class="error" ng-show="submitted && providerForm.userType.$error.required">Required.</small>
									</label>
			   	  				</div>
			    	  		</div>
			    	  	</div>
	   	  			</div>

	   	  			<div class="columns medium-6">

						<div class="colored-section border-crimson">
							<div class="row row-full page-section">
								<div class="large-12 columns">
									<h4>CONTACT</h4>
									<label class="inline margin-bottom-0">Email
										<input type="email" name="email" ng-model="provider.email" class="margin-bottom-0" required />
										<small class="error" ng-show="submitted && providerForm.email.$error.email">Invalid email address.</small>
										<small class="error" ng-show="submitted && providerForm.email.$error.required">Required.</small>
									</label>
									<label class="inline margin-bottom-0">Phone
										<input id="txtPhone" type="tel" name="phone" ng-model="provider.phone" class="margin-bottom-0" required />
										<small class="error" ng-show="submitted && providerForm.phone.$error.required">Required.</small>
									</label>
								</div>
							</div>
						</div>

		   	  			<div class="colored-section border-darkgreen">
							<div class="row row-full page-section">
								<div class="large-12 columns">
									<h4>Notifications</h4>
									 <label class="inline margin-bottom-0">
                                        <input type="checkbox" class="margin-bottom-0 large-chk" name="isEmailEnabled" ng-model="provider.isEmailEnabled" />
                                        <span class="align-sms-check">Email Enabled</span>
                                    </label>
                                     <label class="inline margin-bottom-0">
                                        <input type="checkbox" class="margin-bottom-0 large-chk" name="isSMSEnabled" ng-model="provider.isSMSEnabled" />
                                        <span class="align-sms-check">SMS Enabled</span>
                                    </label>
								</div>
							</div>
		   	  			</div>

	   	  				<div class="align-right padding-10 padding-right-0">
                            <a href="coaches.action"  class="button small radius hs-btns btn-black margin-tb-0 margin-left-2perc">CANCEL</a>
							<input type="submit" id="btnSaveProvider" ng-click="submitted=true" class="button small radius hs-btns btn-teal margin-tb-0" value="SAVE" />
						</div>
	   	  			</div>	    	  			    	  		 
	      	  	</div>        	  		   
      	  	</form>    	  
    	</div>
    </div>
    
	  <!--[if lt IE 9]>
	    	<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
		<![endif]-->
		<!--[if (gte IE 9) | (!IE)]><!-->
		    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>				       
		    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
		<!--<![endif]-->       
	        
	    <!-- Foundation 3 for IE 8 and earlier -->
		<!--[if lt IE 9]>	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
		<![endif]-->
		
		<!-- Foundation 4 for IE 9 and later -->
		<!--[if gt IE 8]><!-->	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
		<!--<![endif]-->

       <script src="${pageContext.request.contextPath}/resources/js/jquery.mask.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>

       <script type="text/javascript">

           var FACILITY_LIST = ("${facilitiesListString}") ? $.parseJSON("${facilitiesListString}") : undefined,
			   PROVIDER_LIST = ("${providerListString}") ? $.parseJSON("${providerListString}") : undefined,
               ID = "",
               AJAXING = Object.create(Ajaxing),
               ALERT_MESSAGING = Object.create(AlertMessage).init(),
               FACILITY_ID_PREF = '${facilityIdPref}';

			$(function(){
				$(document).foundation();
				$("#providerTab").addClass("selected-tab");

                //written in common.js
                formatPhoneNumber($("#txtPhone"), $("#txtPhone").val());
			});

           function removeCountryCode(phone){
               if(phone){
                   var countryCode = phone.indexOf("+1");

                   if(countryCode > -1){
                       phone = phone.substring(countryCode+3, phone.length);
                   }

                   return phone;
               }
           }

           function bindRemoveCoach(){

               $(".remove-member").off("click").on("click", function(e){
                   e.stopPropagation();
                   var $this = $(this);
                   ID = $this.data("id");
                   $("#confirmationRemoveMemberDialog").foundation("reveal", "open");
               });

               $("#btnConfirmDelete").off("click").on("click", function(e){
                   e.preventDefault();

                   //send via ajax call
                   var dataToSend = {"providerId": ID},
                           postRequestActions = {
                               "loading": $("#loading"),
                               "error": $("#errMessage"),
                               "successCallBack": onRemoveCoach
                           };

                   AJAXING.sendPostRequest("removeCoach.action", dataToSend, postRequestActions);
               });
           }

           function onRemoveCoach(data){
               var $error = $("#errMessage");
               $("#confirmationRemoveMemberDialog").foundation("reveal", "close");
               if(data.STATUS == "SUCCESS"){
                   ALERT_MESSAGING.showAlertMessage($error, "Coach removed successfully.", ALERT_MESSAGING.SUCCESS);
                   var $scope = angular.element("#providerListDialog").scope();
                   $scope.refreshProviderList();
               }else{
                   AJAXING.showErrorOrRedirect(data, $error, "Unable to remove Coach. Please try again later.");
               }
           }

       </script>

	   <script src="${pageContext.request.contextPath}/resources/js/provider_controller.js?v=0.9"></script>
  </body>          
</html>