<%@ taglib prefix="s" uri="/struts-tags"%>
<head><title>FAQ - Coach Portal</title>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <title>Patient Dashboard</title>
    <meta http-equiv="x-ua-compatible" content="IE=Edge"/>
    <!--[if (gte IE 9) | (!IE)]><!-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />
    <script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
    <!--<![endif]-->

    <!--[if lt IE 9]>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">
    <![endif]-->

    <!--[if gt IE 8]>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
    <![endif]-->

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css?v=0.4" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/faqs.css" />

</head>

<body class="bg-white">

<div class="updated">
    <p class="c2 c12"><span class="c14 c7">HealthSlate Coach Portal FAQ</span></p>
    <p class="c2 c12"><span class="c17">Updated 4/12/2015</span></p>
</div>
<div id="faqsBody" class="max-width-1129">

<p class="c1 c12"><span class="c17"></span></p>

<p class="c1"><span class="c7 c14"></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.o3rn5jj50fyt">Getting Started and Supported</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.r699h1qhm7fn">What is the phone number for technical support
    for coaches?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.uht3r3jnnj8e">How can my members/patients get technical/product
    support?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.1v994csr8yl1">What if I forget my password?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.y5a9ruopgox8">Can I choose my own user ID? Can I change my user
    ID?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.vd9fwr5rrk7">How the Program Works</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.3j7bpohn7rzs">What happens during the first 12
    weeks?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.ml930fvj6fax">What happens after the first 12 weeks?</a></span>
</p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.f7ob10q2mh8a">Can the member pause the program and come back to
    it. e,g, if they get sick or go on vacation?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.teg2o0hqq8bu">What is My Role?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.t3pgk9co89p3">I am a Lead Coach What is my role?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.euh666u4fpxj">How quickly should I respond to in-app messages
    from a member?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.nduz4s19w8xr">What should I NOT do?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.xs5kjxirgjdd">What about goals and targets other than glucose,
    BP and meds?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.a4in7muj9y6t">Do I need to train the member on how to use the
    app?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.145inkt0zrm9">How often should I review each member&rsquo;s
    status and progress?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.2d883obi8g8d">How do I know what I&rsquo;m supposed to do each
    week with/for a given member?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.uzd3vzaiwcx9">Are the &ldquo;Coach To Do&rsquo;s&rdquo;
    required or just suggestions? What if I don&rsquo;t get through them all in a given week?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.9ptg40if8w8q">What is my role after the initial 12-week
    curriculum?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.cac7uz65ry2o">Food Coaches</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.nnr685wgcjjp">What do the Food Coaches do?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.oak381bqpfhr">Whom do the Food Coaches work for?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.48oqoot1fk2u">If I am an RD can I be the member&rsquo;s Food
    Coach?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.qrxwkwt6za9r">Recruiting, Enrolling and On-Boarding
    Members</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.pked6wo3jxwh">Is this program available to people who have
    already received diabetes education?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.k0gz5y9jxy2w">Can I enroll someone that I am already working
    face-to-face if I believe the online program is a better fit for them.</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.rlv1t0v579op">How can I enroll a member? How do I authorize
    them to be a HealthSlate &ldquo;member?&rdquo;</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.pjg9shijwcm4">What happens after the prospective member
    receives the invitation email?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.xtky0skkcxpx">What is the Lead Coach&rsquo;s role in
    On-boarding?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.p25ut2hnnoo7">Tracking and Reporting Members&rsquo;
    Progress</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.k432k2q0enmq">What can I track?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.5fz9zqibfte">Can I have summary reports sent to me every
    week//month?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.yjw5uwt8b9k4">What kind of status reports will referring
    Providers receive about the member/member&rsquo;s progress? Must I send those reports or do they go out
    automatically?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.pw7vtpdzdprq">Messaging with Members</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.t6n1gahx69sz">How do I send a message to a member/patient?</a></span>
</p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.yvpir7g9orz">If I send a message to a member, where do they see
    it and how do they know that it has arrived?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.3ifxonmxn7lz">How do I know that I have received a message from
    a member?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.5hkjzxoyj8nz">How do I know which member sent me a message?</a></span>
</p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.oiyk5tnbxdnz">Can I use my cell phone or iPad to send messages
    to members? Can I use regular SMS?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.xcbnsjuvv0w">Can I somehow show members that I am unavailable
    for messaging</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.4sfcno7okpyp">What is the difference between the &ldquo;In-App
    Messaging&rdquo; and the &ldquo;SMS Messaging&rdquo; within a given member&rsquo;s chart?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.9saa9rdwkp51">1:1 Video Call Sessions with Members</a></span>
</p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.ps8vvl5yllrs">When am I supposed to do 1:1 sessions with each
    member?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.s386vvv2kztf">How do I know that I need to schedule a 1:1
    session with a member?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.gdm0mr7qrn0t">How do I see/know the time the video call is
    scheduled for? Is there a reminder? Does it integrate with my calendar?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.6byco079w8be">Are video calls recorded?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.yyrx2nmgl0wz">Can I just call a member&rsquo;s phone number
    instead of using the Video call?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.q1zjrqslr7dr">Do I do the video calls using my iPhone, Android
    phone iPad or other tablet?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.ac3pasydr91t">Can I use Facetime, Skype or any other video
    calling service/technology for the video calls?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.6gcn2dvhsw1x">Can the member use a 3G/4G (&ldquo;mobile data&rdquo;)
    connection or do they need to be on WiFi during a video call?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.q560ttsyo3wk">How do I start the video call?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.foc6rthwdpsz">How can I see if the member is available for the
    call?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.f0wwj3gf0scw">Do I need to hit the &ldquo;refresh&rdquo; button
    to see if the member is &ldquo;online,&rdquo; i.e., available &nbsp;to call?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.ywz41m57ko85">What should I do if the member is not &ldquo;Online&rdquo;
    at the scheduled time?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.tzpgkbh58hhz">What should I do if the video call gets
    dropped/disconnected during a 1:1 session?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.d8lfz7ik83pe">Can the members talk to me via the video call
    while also looking at logs, charts and other information in the app that I want to discuss with them?
    How?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.7rldx04ll3cz">If the member turns off their camera can they
    still see me?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.qb7o0g1gs3t9">If the member turns off their camera how can they
    turn it back on to re-start the 2-way video?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.tvbrgs3pm84">Can I turn off my camera so that the member can
    only hear me and not see me?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.lcjglj5ut32e">Working with Other Coaches</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.z1q3jtbg3fzn">How can I see who the other coaches are for one
    of my members?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.hsomugk3hujp">How do I send a message to another
    coach?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.q73z36y553ct">How do I know that I have received a message from
    another coach?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.my6229vp78vh">Goals</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.c2k8mcljafwk">Where are the members&rsquo; goals created and
    changed?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.7fzii8kz65re">If I perform a Coach Assessment for how a member
    is doing with a given Goal, does the member see my assessment?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.xi2w4oectf2t">The 12-week Curriculum</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.823nnpdebtae">What is the curriculum/online course based on?
    Who created the content? Who has reviewed it?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.x28jlc3zrai9">How can I see which week of the 12-week
    curriculum each member is in?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.sh6rrsdvt03i">How can I see what they are learning that
    week?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.v9m3bzse4j00">How can I watch the videos and/or read the
    transcripts of the curriculum?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.khwkpxcp44r2">How can I see whether a given member is actually
    watching the curriculum videos and how they are doing on the quizzes?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.ai3xybnt2mjr">What should I do if I find something in the
    curriculum that I think is inaccurate or unclear?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.2704m2z8uug9">Logbook</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.r29rf16jg6yy">Where can I see the member&rsquo;s
    logbook?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.44h7o95a8ar7">Glucose and Blood Pressure Logging</a></span>
</p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.xrgeujmr46wj">Do members need a glucometer as part of the
    program? If so, do they receive one? Where should they get one if they need it?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.n177f0lsosq9">What should we tell members whose insurance does
    not cover all the test strips they need in the program?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.ewjilrf0bph9">How often do the members test their glucose in
    the program?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.24cr0sd5pt1g">Can members download their glucose meter data
    directly to the HealthSlate app?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.yfgycugm4qr8">Are members supposed to track blood pressure? Do
    you provide a BP monitor for them to do so?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.iugibw7u55o8">Meal Logging and Carb Ceilings</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.whhq81kvfy3b">How do members log their meals?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.ceatjx3shi2v">Where can I see what the member has been
    eating?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.n2gt0ekvwxbs">Who sets the carb ceilings? What are they based
    on? Can the member change the targets?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.5srwxmjzxn0l">Weight Loss</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.cav5uupb7x4c">Are members supposed to have a weight loss
    goal?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.uugdukrrl01w">Who sets the weight loss goal?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.si8y5tkfmnrj">How do members log their weight? Does the
    HealthSlate App integrate with any wireless scales?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.9bszjgu2a2ph">Activity and Sleep Tracking</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.hqziiqdko1xc">When and how does the member receive their
    tracker?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.p4t02wbov10">Does the tracker come with instructions for how to
    use it? How can the member get help getting it working?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.iacovpe6xf8y">Does the member need to install a separate app
    for use with the tracker?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.ot026gy2c8f">What exactly does the Activity and Sleep Tracker
    track?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.o45gb96u5sh9">What should the member do if their tracker stops
    working?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.o05kyougln">Medications Tracking</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.o8fdqldayp8u">Why doesn&rsquo;t the app or Coach Portal keep
    track of the member&rsquo;s specific medications?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.rvydee85clas">Why are reminders to take medications sent via
    text message instead of via app notifications like all other reminders?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.jlrase370kr1">Does the the app include refill
    reminders?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.iejhochxi5ir">The Group Feed</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.wj6doqs6566w">Who is in which Group? Do members who are
    patients of my organization see members from other healthcare organizations in the Group Feed?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.a24epa3z3pgs">What can members &ldquo;share&rdquo; with the
    Group Feed?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.tig7zvwe8cgc">What can members NOT post to the Group Feed?</a></span>
</p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.4nr59y9yd9w">Is any type of sharing to the Group Feed defaulted
    to ON? Is it required?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.jmi1jsgxhxih">Can I post to the Group Feed?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.v6vr9z94xzew">Is the Group Feed monitored?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.lqj7nsp15gy5">What should I do if I see a member post something
    inaccurate or inappropriate on the Group Feed?</a></span></p>

<p class="c2 c13"><span class="c3"><a class="c6" href="#h.aahttk2ghyj">Technical and Security Questions</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.lzej2lhvt1qs">Does the HealthSlate Portal integrate with our
    EMR?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.om01l1pdlwjn">Can I use any computer to access the HealthSlate
    Coach Portal?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.jopawjg1a5u2">Can I use any browser?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.4gz7gtoejtkf">Can I access the HealthSlate Coach Portal outside
    of work? From home? Starbucks?</a></span></p>

<p class="c2 c8"><span class="c3"><a class="c6" href="#h.g6pbdroaxuqr">Can I access the Coach Portal via my phone or
    iPad or other tablet?</a></span></p>

<p class="c1"><span class="c14 c7"></span></p>

<h2 class="c2 c5"><a name="h.o3rn5jj50fyt"></a><span>Getting Started and Supported</span></h2>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.r699h1qhm7fn"></a><span>What is the phone number for technical support for coaches?</span>
</h3>

<p class="c2"><span>888-291-7245</span></p>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a name="h.uht3r3jnnj8e"></a><span>How can my members/patients get technical/product support?</span>
</h3>

<p class="c2"><span>They should send a message to the HealthSlate Tech Support person who appears on their My Team page. If that person is unavailable, the member&rsquo;s message will be forwarded to someone who can help them. We try to get back to everyone within 30 minutes.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.1v994csr8yl1"></a><span>What if I forget my password?</span></h3>

<p class="c2"><span>Use the Reset Password link on the HealthSlate Coach Portal login page.</span></p>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.y5a9ruopgox8"></a><span>Can I choose my own user ID? Can I change my user ID?</span></h3>

<p class="c2"><span>No. Your user ID is your email address. </span></p>

<p class="c1"><span class="c7"></span></p>

<p class="c1"><span class="c7"></span></p>

<h2 class="c2 c5"><a name="h.vd9fwr5rrk7"></a><span>How the Program Works</span></h2>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.3j7bpohn7rzs"></a><span>What happens during the first 12 weeks?</span></h3>

<p class="c2"><span>The first 12 weeks is an intensive program of education, coaching, and development of self-management skills and habits. During the first 12 weeks the member also takes an online course based on the AADE7 framework. The course requires about 15 minutes per week, and is delivered through short videos and quiz questions. The member can also participate in an online support group via the Group feed in the app.</span>
</p>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.ml930fvj6fax"></a><span>What happens after the first 12 weeks?</span></h3>

<p class="c2"><span>The member still uses the app, shares with the Group, and has access to on-demand support from their Food Coaches. Scheduled sessions with </span><span>Food Coaches are available once per quarter. Whether or not there are any scheduled sessions with Lead Coaches is the decision of each partner organization, and will likely depend on coverage policies by the insurers. Please consult with the head of your diabetes education program regarding your organization&rsquo;s plans.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.f7ob10q2mh8a"></a><span>Can the member pause the program and come back to it. e,g, if they get sick or go on vacation?</span>
</h3>

<p class="c2"><span>Yes. Although we encourage members to go through the course in 12 weeks, they can access the online course content at whatever pace they choose. You should schedule the &ldquo;week 4&rdquo;
    and &ldquo;week 12&rdquo; 1:1 sessions with them based on when they are actually engaged with those weeks of the course rather than where they are relative to </span><span>their</span><span>&nbsp;start date.</span>
</p>

<p class="c1"><span></span></p>

<p class="c1"><span class="c7"></span></p>

<h2 class="c2 c5"><a name="h.teg2o0hqq8bu"></a><span>What is My </span><span>Role</span><sup><a href="#cmnt1"
                                                                                                name="cmnt_ref1">[a]</a></sup><span>?</span>
</h2>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a name="h.t3pgk9co89p3"></a><span>I am a Lead Coach What is my role?</span></h3>

<p class="c2"><span>Your role is essentially the same as when you counsel diabetes patients in in-person 1:1 sessions: learning about their needs and goals, helping them develop and pursue goals and action plans, helping them develop problem solving and coping strategies, coordinating and communicating with the rest of their care team, and assessing their goal attainment. You do this through live, scheduled 1:1 sessions and through text message interactions that are not scheduled. Each member has three live 1:1 sessions during the 12-week program, one each in weeks 1, 4 and 12. The live sessions take place via video chat and are intended to take 30 minutes each. </span><span
        class="c7">Before conducting your first coaching session, you need to watch all of the online course videos for weeks 1 - 3. That will help you understand what the member is seeing and being asked to do. </span>
</p>

<p class="c1"><span></span></p>

<p class="c2"><span>The text messages are like the regular &ldquo;SMS&rdquo; text messages on your phone, but happen within the HealthSlate app in a HIPAA-compliant manner and are hence called &ldquo;in-app messages.&rdquo;
    Reasons for sending in-app messages include: scheduling 1:1 sessions, encouraging the member on goals, and checking in with a member based on what you see the member logging or not logging (you can see what they are logging via the Coach Portal) You will also use in-app messaging to reply to questions or comments sent to you by the member. </span>
</p>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a
        name="h.euh666u4fpxj"></a><span>How quickly should I respond to in-app messages from a member?</span></h3>

<p class="c2"><span>On weekdays If you receive a question via in-app messaging from a member before 3 PM you should reply to it before 8 PM. If you receive a message after 3 PM you should reply by noon the following day. If you sense that a member needs more immediate attention than you are able to provide, you should alert one off the member&#39;s other coaches who is able to reply promptly and ask them to do so.</span>
</p>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a name="h.nduz4s19w8xr"></a><span>What should I NOT do?</span></h3>

<p class="c2"><span>HealthSlate Coaches do NOT determine members&rsquo; glucose targets or blood pressure targets or anything related to members&rsquo;
    medications, including which medications to take at what dosages and times. The member&rsquo;s healthcare provider determines all of those things (blood pressure targets and logging are not yet supported.) Please contact the member&rsquo;s HCP to confirm this information if the HCP has not already provided it. In the case of glucose targets, you may establish &ldquo;standing orders&rdquo;
    with a given HCP such that if a member is referred with no glucose targets then we will apply that HCP&rsquo;s standing orders for those targets.</span>
</p>

<p class="c1"><span class="c7"></span></p>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a
        name="h.xs5kjxirgjdd"></a><span>What about goals and targets other than glucose, BP and meds?</span></h3>

<p class="c2"><span>Lead Coaches work with each member to make a collaborative decision around self-management goals that may include, but are not limited to, weight and physical activity. However, you or the member should consult with the member&rsquo;s HCP before the member starts a physical activity plan.</span>
</p>

<p class="c1"><span></span></p>

<p class="c2"><span>Food Coaches work with each member to make collaborative decisions around goals for carbohydrate consumption, i.e., &ldquo;carbohydrate ceilings.&rdquo;</span>
</p>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a
        name="h.a4in7muj9y6t"></a><span>Do I need to train the member on how to use the </span><span>app</span><span>?</span>
</h3>

<p class="c2"><span>Usually not. The &ldquo;guided tour&rdquo; they take during their initial use of the app and the videos they watch in the online course show them how to use the app. The online course videos are &nbsp;called Topics in their app. However, you should be generally familiar with the app and able to answer basic questions about how to use it.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a
        name="h.145inkt0zrm9"></a><span>How often should I review each member&rsquo;s status and progress?</span></h3>

<p class="c2"><span>For each member&rsquo;s first week in the program we ask you to review their status several times. Thereafter we ask you to review each member&#39;s status at least once weekly. However, your institution might require more frequent review. </span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.2d883obi8g8d"></a><span>How do I know what I&rsquo;m supposed to do each week with/for a given member? </span>
</h3>

<p class="c2">
    <span>See the Coach To Dos box in the top right corner of the member&rsquo;s dashboard, as shown below.</span></p>

<p class="c2"><span
        style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 550.00px; height: 323.00px;"><img
        alt="Coach To Dos.png" src="${pageContext.request.contextPath}/resources/css/images/faqs_images/image00.png"
        style="width: 550.00px; height: 323.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
        title=""></span></p>

<p class="c1"><span class="c11"></span></p>

<p class="c1"><span class="c11"></span></p>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a name="h.uzd3vzaiwcx9"></a><span>Are the &ldquo;Coach To Do&rsquo;s&rdquo; required or just suggestions? What if I don&rsquo;t get through them all in a given week?</span>
</h3>

<p class="c2"><span>During weeks in which you have a 1:1 session with a member the to-do lists are quite important, because they are connected with activities we are asking the member to do that week. During other weeks the To Do list items are usually suggestions. If you do not complete the To Do list items during a live 1:1 please use in- app messaging to finish the To Do&#39;s.</span>
</p>

<h3 class="c2 c5"><a name="h.9ptg40if8w8q"></a><span><br>What is my role after the initial 12-week curriculum?</span>
</h3>

<p class="c2">
    <span>The role of the lead coach after the initial 12 month period is decided by each institution. </span><span>We</span><span>&nbsp;hope that lead coaches continue to stay in touch with the members so that &nbsp;games </span><span>are</span><span>&nbsp;maintained long term.</span>
</p>

<p class="c1"><span></span></p>

<h2 class="c2 c5"><a name="h.cac7uz65ry2o"></a><span>Food Coaches</span></h2>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.nnr685wgcjjp"></a><span>What do the Food Coaches do?</span></h3>

<p class="c2"><span>The member&rsquo;s Food Coaches, who are RDs/CDEs, process meals that the member logs, provide individualized meal plans, and are available on-demand to answer questions about diabetes self-management and nutrition.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.oak381bqpfhr"></a><span>Whom do the Food Coaches work for?</span></h3>

<p class="c2"><span>Food Coaches work for HealthSlate.</span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.48oqoot1fk2u"></a><span>If I am an RD can I be the member&rsquo;s Food Coach?</span></h3>

<p class="c2"><span>No. The food coaches need to be available on demand to process meal logs and take phone calls and messages from members at all times of day, so it does not work well with the schedules of dietitians who work at a clinic or hospital.</span>
</p>

<p class="c1"><span class="c7"></span></p>

<h2 class="c2 c5"><a name="h.qrxwkwt6za9r"></a><span>Recruiting, Enrolling and On-Boarding Members</span></h2>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.pked6wo3jxwh"></a><span>Is this program available to people who have already received diabetes education? </span>
</h3>

<p class="c2"><span>The answer is up to your organization, as the reimbursement situation with insurers varies. For pilot deployments in most cases organizations are enrolling people who may have previously had diabetes education, though not a full course within the past two years. </span>
</p>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a name="h.k0gz5y9jxy2w"></a><span>Can I enroll someone that I am already working face-to-face if I believe the online program is a better fit for them.</span>
</h3>

<p class="c2"><span>The answer to this is up to your organization. If the answer is yes, then you will likely be able to make the initial 1;1 session shorter than 30 minutes, as you will likely have already covered many of the key topics for the week 1 session.</span>
</p>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a name="h.rlv1t0v579op"></a><span>How can I enroll a member? How do I authorize them to be a HealthSlate &ldquo;member?&rdquo;</span>
</h3>

<p class="c2"><span>Someone at your health system has been designated as the &ldquo;Facility Admin.&rdquo; That person has the authority to authorize new HealthSlate members. They do this by entering the member&rsquo;s email address into the HealthSlate server, which then sends an email to the prospective member introducing them to the program and letting them know that your healthcare organization is inviting them to participate.</span>
</p>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a name="h.pjg9shijwcm4"></a><span>What happens after the prospective member receives the invitation email?</span>
</h3>

<p class="c2"><span>The email takes the person to a page that explains more about the program and provides them the forms for enrolling. After they enroll the member receives another email with a link to the app store where they can download the HealthSlate app. They then open this link on their mobile phone and install the app. After they install the app, they create an account, enter their profile information, such as dietary restrictions, and then start a guided tour of the </span><span>app</span><span>.</span>
</p>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.xtky0skkcxpx"></a><span>What is the Lead Coach&rsquo;s role in On-boarding?</span></h3>

<p class="c2"><span>When a new member enrolls in the program the HealthSlate Admin for your organization will assign one of the CDEs to be that member&rsquo;s Lead Coach. If the admin assigns you as the Lead Coach, you will receive an email alerting you that you have a new Member. You should know that as soon as the member opens the HealthSlate app for the first time she or he will see an automated but personal message from you welcoming them to the program and asking them what times would be good for their first 1:1. </span><span
        class="c7">You need to have the first live 1:1 session with the member during their first week in the program if at all possible. </span><span>During the first 24 hours after the member enrolls try to schedule that 1:1. In addition to information they may provide in response to the welcome email, </span><span
        class="c19">you can also find information about their likely availability in the Coach Dashboard under the member&rsquo;s Settings, in the &ldquo;1:1 Availability Section.&rdquo;. (Coming in June, 2015.)</span>
</p>

<p class="c1"><span class="c7"></span></p>

<p class="c1"><span class="c7"></span></p>

<h2 class="c2 c5"><a name="h.p25ut2hnnoo7"></a><span>Tracking and Reporting Members&rsquo; Progress</span></h2>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.k432k2q0enmq"></a><span>What can I track?</span></h3>

<p class="c2"><span>The Dashboard for each member shows you the following member-reported information on a daily, weekly and monthly basis:</span>
</p>
<ul class="c4 lst-kix_g0f9f1fmtund-0 start">
    <li class="c2 c15 c8"><span>times that meds were taken</span></li>
    <li class="c2 c15 c8"><span>glucose test results</span></li>
    <li class="c2 c15 c8"><span>carbohydrate consumption, both absolute and relative to carbohydrate ceilings</span>
    </li>
    <li class="c2 c15 c8"><span>physical activity, in steps, minutes and type of activity</span></li>
    <li class="c2 c8 c15"><span>weight</span></li>
    <li class="c2 c15 c8"><span>for each of the above categories, the member is also able to log conditions like sick, hungry, stressed, </span><span>tired</span><span>.(These tags are changing in an upcomign release.)</span>
    </li>
    <li class="c2 c15 c8"><span>progress and quiz results for online course</span></li>
    <li class="c2 c15 c8"><span>participation in peer support via the Group Feed</span></li>
    <li class="c2 c15 c8"><span>goals and action plans created by the member, and the member&rsquo;s assessment of goal attainment</span>
    </li>
</ul>
<p class="c1"><span></span></p>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a name="h.5fz9zqibfte"></a><span>Can I have summary reports sent to me every week//month?</span></h3>

<p class="c1"><span></span></p>

<p class="c2"><span class="c19">Starting in May 2015 we intend to provide monthly summary reports of member progress that will include the following:</span>
</p>
<ul class="c4 lst-kix_hcngnq3qqhm6-0 start">
    <li class="c2 c15 c8"><span>Average BGL, total number of tests, % of test results above and below target, and highest and lowest test results</span>
    </li>
    <li class="c2 c15 c8"><span>% of days medications were reported as having been taken</span></li>
    <li class="c2 c15 c8"><span>Average physical activity</span></li>
    <li class="c2 c15 c8"><span>Average carbs/meal</span></li>
    <li class="c2 c15 c8"><span>Completion of online </span><span>course</span></li>
</ul>
<p class="c1"><span></span></p>

<p class="c2"><span>The report appears on the member&rsquo;s dashboard.</span></p>

<p class="c1"><span></span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.yjw5uwt8b9k4"></a><span>What kind of status reports will referring Providers receive about the member/member&rsquo;s progress? Must I send those reports or do they go out automatically? </span>
</h3>
<ul class="c4 lst-kix_87v99djqrr2u-0 start">
    <li class="c2 c15 c8"><span class="c19">Referring providers can elect to have the report referenced in the question above faxed to them each month. (Coming in May 2015) </span>
    </li>
</ul>
<p class="c1"><span></span></p>

<p class="c1"><span class="c7"></span></p>

<h2 class="c2 c5"><a name="h.pw7vtpdzdprq"></a><span>Messaging with Members</span></h2>

<h3 class="c2 c5"><a name="h.t6n1gahx69sz"></a><span>How do I send a message to a member/patient?</span></h3>

<p class="c2"><span>Use the Text Messages (sometimes called SMS Messages) button in the top right corner of the member&rsquo;s dashboard.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.yvpir7g9orz"></a><span>If I send a message to a member, where do they see it and how do they know that it has arrived?</span>
</h3>

<p class="c2"><span>The member receives a text message telling them that one of their HealthSlate coaches has sent them a message. The text includes a link to open their HealthSlate app.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.3ifxonmxn7lz"></a><span>How do I know that I have received a message from a member?</span>
</h3>

<p class="c2"><span>You can choose to be alerted by either text message or email. Please tell your admin your preference.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.5hkjzxoyj8nz"></a><span>How do I know which member sent me a message?</span></h3>

<p class="c2"><span>There is a Messages column on the right side of the My Member (formerly My Patients) page. If you click the top of that column it will re-sort that page to show which members have sent you a message. </span>
</p>

<p class="c1"><span></span></p>

<p class="c2"><span
        style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 518.00px; height: 251.00px;"><img
        alt="messages in My Members.png" src="${pageContext.request.contextPath}/resources/css/images/faqs_images/image03.png"
        style="width: 518.00px; height: 251.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
        title=""></span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.oiyk5tnbxdnz"></a><span>Can I use my cell phone or iPad to send messages to members? Can I use regular SMS?</span>
</h3>

<p class="c2"><span>No. You can only send message to the member from within the HealthSlate Coach Portal. </span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.xcbnsjuvv0w"></a><span>Can I somehow show members that I am unavailable for </span><span>messaging</span>
</h3>

<p class="c2"><span>Not at this time. We are working on that.</span></p>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.4sfcno7okpyp"></a><span>What is the difference between the &ldquo;In-App Messaging&rdquo;
    and the &ldquo;SMS Messaging&rdquo; within a given member&rsquo;s chart?</span></h3>

<p class="c2"><span>In-app messaging sends a message that the member reads within the app. It appears in their Me Feed and can also be accessed via the Messages button under your picture on the My Team pages in the member&rsquo;s app. The member receives a text message alerting them that they have received a message from a HealthSlate coach but that message does not include the contents of your message. It only alerts them to go their HealthSlate app to see the message fromt the coach. SMS messaging enables you to send messages in which the content appears as a text message on their phone; the member does not need to open the HealthSlate app. However, in order to ensure that Protected Health Information is not sent via text message, the SMS message feature does not allow you to &ldquo;free type;&rdquo;
    you can only send pre-written, &ldquo;canned&rdquo; messages.</span></p>

<p class="c1"><span class="c7"></span></p>

<p class="c1"><span class="c7"></span></p>

<p class="c1"><span class="c7"></span></p>

<h2 class="c2 c5"><a name="h.9saa9rdwkp51"></a><span>1:1 Video Call Sessions with Members</span></h2>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5 c22"><a name="h.wnnh34k25ibo"></a></h3>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a name="h.ps8vvl5yllrs"></a><span>When am I supposed to do 1:1 sessions with each member?</span></h3>

<p class="c2"><span>Each member has three live 1:1 sessions with their Lead Coach during the 12-week program, one each in weeks 1, 4 and 12. </span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a
        name="h.s386vvv2kztf"></a><span>How do I know that I need to schedule a 1:1 session with a member?</span></h3>

<p class="c2"><span>For each member for whom you are the Lead Coach you need to track where in the curriculum that member is. You can see that in the member&rsquo;s dashboard in the Coach Portal, under Topics.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.gdm0mr7qrn0t"></a><span>How do I see/know the time the video call is scheduled for? Is there a reminder? Does it integrate with my calendar?</span>
</h3>

<p class="c2"><span>Not at this time. You need to put the call time into your own calendar. </span></p>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a name="h.6byco079w8be"></a><span>Are video calls recorded?</span></h3>

<p class="c2"><span>No.</span></p>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.yyrx2nmgl0wz"></a><span>Can I just call a member&rsquo;s phone number instead of using the Video call?</span>
</h3>

<p class="c2"><span>Unless you have already conducted a face-to-face meeting with the member, the Week 1 session needs to take place via video call in order to establish a personal connection. After that, calls can be made using the phone instead of video if that is the member&rsquo;s preference. Note, however, that some insurers may require live video in order for a session to be reimbursed.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.q1zjrqslr7dr"></a><span>Do I do the video calls using my iPhone, Android phone iPad or other tablet?</span>
</h3>

<p class="c2"><span>No. &nbsp;As of now, you can only use your office computer or a HealthSlate-approved Chromebook that has been configured in accordance with our guidelines. This is the case both for call quality reasons and for data security and privacy reasons.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.ac3pasydr91t"></a><span>Can I use Facetime, Skype or any other video calling service/technology for the video calls?</span>
</h3>

<p class="c2"><span>No.</span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.6gcn2dvhsw1x"></a><span>Can the member use a 3G/4G (&ldquo;mobile data&rdquo;) connection or do they need to be on WiFi during a video call?</span>
</h3>

<p class="c2"><span>They can use either, though using their own mobile data connection will consume a lot of bandwidth, which could end up costing them money if they go above their monthly limit. WiFi also is usually a higher bandwidth connection, resulting in a higher quality audio and video signal and lower chance of the call being dropped.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.q560ttsyo3wk"></a><span>How do I start the video call?</span></h3>

<p class="c2"><span>Go to the member&rsquo;s dashboard on the Coach Portal, and touch the Video Call button in the top right to get to the Video Call page. From there you can touch the Click to Call button when the member is online.</span>
</p>

<p class="c1"><span></span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.foc6rthwdpsz"></a><span>How can I see if the member is available for the call?</span></h3>

<p class="c2">
    <span>If the member is logged into the HealthSlate app, on the video call page you will see a green &ldquo;Online&rdquo;
        message.</span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.f0wwj3gf0scw"></a><span>Do I need to hit the &ldquo;refresh&rdquo; button to see if the member is &ldquo;online,&rdquo;
    i.e., available &nbsp;to call?</span></h3>

<p class="c2"><span>No. The server checks every few seconds and updates the online/offline message. It will change from red to green automatically. The refresh button is just there in case you want to check more frequently.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.ywz41m57ko85"></a><span>What should I do if the member is not &ldquo;Online&rdquo; at the scheduled time?</span>
</h3>

<p class="c2"><span>You can text them using the SMS tool in the Messages page, which you access via the Messages link in the top right of the member&rsquo;s dashboard. One of the options in the drop down list sends a text message to the user telling them it&rsquo;s time to start the video c</span><span>all. </span><span
        class="c7">&nbsp;Do NOT ever text a member from your cell phone.</span></p>

<p class="c1"><span class="c7"></span></p>

<p class="c2"><span
        style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 414.00px; height: 269.00px;"><img
        alt="SMS button.png" src="${pageContext.request.contextPath}/resources/css/images/faqs_images/image04.png"
        style="width: 414.00px; height: 269.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
        title=""></span></p>

<p class="c2"><span
        style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 415.50px; height: 278.50px;"><img
        alt="Video call starting now.png" src="${pageContext.request.contextPath}/resources/css/images/faqs_images/image02.png"
        style="width: 415.50px; height: 278.50px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
        title=""></span></p>

<p class="c1"><span></span></p>

<p class="c2"><span>What do I do if the call is &ldquo;dropped&rdquo; accidentally? Will it re-start automatically or do I need to call them back?</span>
</p>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a name="h.tzpgkbh58hhz"></a><span>What should I do if the video call gets dropped/disconnected during a 1:1 session?</span>
</h3>

<p class="c2"><span>First, try to reconnect the call using the Click to Call button. If that does not work, you can call the member&rsquo;s cell phone from your phone.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.d8lfz7ik83pe"></a><span>Can the members talk to me via the video call while also looking at logs, charts and other information in the app that I want to discuss with them? How?</span>
</h3>

<p class="c2"><span>Yes. They need to touch the video camera icon that is on their screen during the video call. That will turn off their video camera so that they no longer see you and you no longer see them; then they can continue talking with you as they navigate other parts of the app. They re-start the 2-way video by touching the video camera icon again.</span>
</p>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a name="h.7rldx04ll3cz"></a><span>If the member turns off their camera can they still see me? </span>
</h3>

<p class="c2"><span>No</span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.qb7o0g1gs3t9"></a><span>If the member turns off their camera how can they turn it back on to re-start the 2-way video?</span>
</h3>

<p class="c2"><span>They touch the video camera icon again.</span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.tvbrgs3pm84"></a><span>Can I turn off my camera so that the member can only hear me and not see me?</span>
</h3>

<p class="c2"><span>Technically yes, but please do not do that unless you temporarily need to block something for privacy reason. The members should usually be able to see you when they are allowing themselves to be seen.</span>
</p>

<p class="c1"><span class="c7"></span></p>

<p class="c1"><span class="c7"></span></p>

<h2 class="c2 c5"><a name="h.lcjglj5ut32e"></a><span>Working with Other Coaches</span></h2>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a
        name="h.z1q3jtbg3fzn"></a><span>How can I see who the other coaches are for one of my members?</span></h3>

<p class="c2"><span>On that member&rsquo;s dashboard, look for Patient&rsquo;s Coaches on the right panel. </span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.hsomugk3hujp"></a><span>How do I send a message to another coach?</span></h3>

<p class="c2"><span>The only way to do that as of now is to create a new Coach Note and select that coach to be notified about the Coach Note.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a
        name="h.q73z36y553ct"></a><span>How do I know that I have received a message from another coach?</span></h3>

<p class="c2"><span>You will receive a text message or email, whichever you specified to your Admin.</span></p>

<p class="c1"><span class="c7"></span></p>

<p class="c1"><span class="c7"></span></p>

<h2 class="c2 c5"><a name="h.my6229vp78vh"></a><span>Goals </span></h2>

<p class="c1"><span class="c11 c7"></span></p>

<h3 class="c2 c5"><a name="h.c2k8mcljafwk"></a><span>Where are the members&rsquo; goals created and changed?</span></h3>

<p class="c2"><span>The member creates the goals via the HealthSlate App. The member can choose from pre-created Goals and Action plans or she can create her own goals and action plans. &nbsp;As of now, the only way for coaches to see the complete list of pre-created goals and action plans is via the app itself; we are working to make that information available from the Coach Portal.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.7fzii8kz65re"></a><span>If I perform a Coach Assessment for how a member is doing with a given Goal, does the member see my assessment?</span>
</h3>

<p class="c2"><span>No.</span></p>

<p class="c1"><span class="c7"></span></p>

<p class="c1"><span class="c7"></span></p>

<h2 class="c2 c5"><a name="h.xi2w4oectf2t"></a><span>The 12-week Curriculum</span></h2>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.823nnpdebtae"></a><span>What is the curriculum/online course based on? Who created the content? Who has reviewed it?</span>
</h3>

<p class="c2"><span>The curriculum is based on the AADE7 framework for diabetes self-management education. It was adapted from the Living with Diabetes series of educational videos developed by A to Z Health/HealthSlate and the AADE in 2012 and 2013 under the guidance of Ginger Kanzer-Lewis, RN, past president of AADE. The content was adapted for an online course by Meghann Moore, MPH, RD, CDE, and reviewed by Nicole Downey, RD, MBA, CDE, and Jacque Corey, RN, MPH, CCM, CNS, CDE.</span>
</p>

<p class="c1"><span></span></p>

<p class="c2"><span>Additional information is available in the document &ldquo;The Evidence Basis and Standards Compliance of the HealthSlate Program,&rdquo;
    which you can receive by sending email to Paul Coombs at </span><span class="c3"><a class="c6"
                                                                                        href="mailto:pcoombs@healthslate.com">pcoombs@healthslate.com</a></span><span>.</span>
</p>

<p class="c1"><span></span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.x28jlc3zrai9"></a><span>How can I see which week of the 12-week curriculum each member is in?</span>
</h3>

<p class="c2"><span>On the Coach Portal dashboard for that patient click the purple box labeled Topics, this shows the most recent week&rsquo;s content the member has watched.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.sh6rrsdvt03i"></a><span>How can I see what they are learning that week? </span></h3>

<p class="c2"><span>From the dashboard, touch the white arrow in the purple Topics box. That will take you to the Topics detail page, where you can see which week&rsquo;s content the member is currently learning, including a bullet point summary of the content as well as a list of the individual videos, as depicted in the screen shot below.</span>
</p>

<p class="c2"><span
        style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 300.00px; height: 299.00px;"><img
        alt="Topics status.png" src="${pageContext.request.contextPath}/resources/css/images/faqs_images/image01.png"
        style="width: 300.00px; height: 299.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
        title=""></span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.v9m3bzse4j00"></a><span>How can I watch the videos and/or read the transcripts of the curriculum?</span>
</h3>

<p class="c2"><span>The videos and transcripts are both here: </span><span class="c0"><a class="c6"
                                                                                         href="http://www.google.com/url?q=http%3A%2F%2Fcurriculum.healthslate.com%2F%23%2F%3Fuser%3Ddemo&amp;sa=D&amp;sntz=1&amp;usg=AFQjCNG46wq4z8z9Tciu71Cr0vz-RZ1MyQ" target="_blank">http://curriculum.healthslate.com/#/?user=demo</a></span>
</p>

<p class="c2"><span>The transcript for each video appears beneath the video.</span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.khwkpxcp44r2"></a><span>How can I see whether a given member is actually watching the curriculum videos and how they are doing on the quizzes?</span>
</h3>

<p class="c2"><span>On the Topics detail page. See the lower right red circled area in the image above.</span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.ai3xybnt2mjr"></a><span>What should I do if I find something in the curriculum that I think is inaccurate or unclear?</span>
</h3>

<p class="c2"><span>Please email Meghann Moore at Meghann@healthslate.com</span></p>

<p class="c1"><span></span></p>

<h2 class="c2 c5"><a name="h.2704m2z8uug9"></a><span>Logbook</span></h2>

<h3 class="c2 c5"><a name="h.r29rf16jg6yy"></a><span>Where can I see the member&rsquo;s logbook?</span></h3>

<p class="c2"><span>A button that takes you to the logbook is on the lower right side of the member&rsquo;s dashboard. See the image below.</span>
</p>

<p class="c2"><span
        style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 624.00px; height: 504.00px;"><img
        alt="accessing the logbook.png" src="${pageContext.request.contextPath}/resources/css/images/faqs_images/image05.png"
        style="width: 624.00px; height: 504.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
        title=""></span></p>

<h2 class="c2 c5"><a name="h.44h7o95a8ar7"></a><span>Glucose and Blood Pressure Logging</span></h2>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a name="h.xrgeujmr46wj"></a><span>Do members need a glucometer as part of the program? If so, do they receive one? Where should they get one if they need it?</span>
</h3>

<p class="c2"><span>Yes, they need a glucose meter. We do not supply one. They should obtain one that uses test strips covered by their insurance.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.n177f0lsosq9"></a><span>What should we tell members whose insurance does not cover all the test strips they need in the program?</span>
</h3>

<p class="c2"><span>You should decide that on a case by case basis, depending on the member&rsquo;s financial resources, the provider&rsquo;s instructions regarding glucose testing, and other factors. HealthSlate cannot tell you how to handle that situation.</span>
</p>

<p class="c1"><span></span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.ewjilrf0bph9"></a><span>How often do the members </span><span>test</span><sup><a
        href="#cmnt2" name="cmnt_ref2">[b]</a></sup><span>&nbsp;their glucose in the program?</span></h3>

<p class="c2"><span>At weeks 3 and 11 the members are asked to participate in an intensive 3-day testing exercise modeled after the Structured Testing in Pairs model, also sometimes known as ACCU-CHEK 360. This is not required. This exercise is intended for educational purposes, not as an ongoing means of glucose self-monitoring. The HealthSlate Program does not recommend any specific glucose testing schedule, but leaves the schedule up to the member&rsquo;s healthcare provider and diabetes educator, which may be their Lead Coach.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.24cr0sd5pt1g"></a><span>Can members download their glucose meter data directly to the HealthSlate app?</span>
</h3>

<p class="c2"><span>Not at this time.</span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.yfgycugm4qr8"></a><span>Are members supposed to track blood pressure? Do you provide a BP monitor for them to do so? </span>
</h3>

<p class="c2"><span>Blood pressure logging is not currently part of the app or program.</span></p>

<p class="c1"><span class="c7"></span></p>

<p class="c1"><span class="c7"></span></p>

<h2 class="c2 c5"><a name="h.iugibw7u55o8"></a><span>Meal Logging and Carb Ceilings</span></h2>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.whhq81kvfy3b"></a><span>How do members log their meals?</span></h3>

<p class="c2"><span>Members can log meals using the Add Log (circled cross) button in the top right corner of the app. However, note that we do not position this as traditional food logging, but rather as a way for the member to show Food Coaches what they are eating so that the Food Coaches can learn what the member likes to eat and help the member learn how much carbohydrate is in those foods.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.ceatjx3shi2v"></a><span>Where can I see what the member has been eating?</span></h3>

<p class="c2"><span>From the member&rsquo;s dashboard, go to Meal Summary (white arrow in the Green box) then touch Show Meals.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.n2gt0ekvwxbs"></a><span>Who sets the carb ceilings? What are they based on? Can the member change the </span><span>targets</span><sup><a
        href="#cmnt3" name="cmnt_ref3">[c]</a></sup><span>? </span></h3>

<p class="c2"><span>The Food Coaches and member agree on carb ceilings together.</span></p>

<p class="c1"><span></span></p>

<p class="c1"><span></span></p>

<h2 class="c2 c5"><a name="h.5srwxmjzxn0l"></a><span>Weight Loss</span></h2>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.cav5uupb7x4c"></a><span>Are members supposed to have a weight loss goal? </span></h3>

<p class="c2"><span>There is no requirement for a weight loss goal, and no recommendations are provided for any member&rsquo;s weight loss. Members may decide to set their own weight loss goals, which they can enter into the app. </span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.uugdukrrl01w"></a><span>Who sets the weight loss goal?</span></h3>

<p class="c2"><span>Each member sets his own goal, if any. &nbsp;Lead Coaches can discuss such goals with members, as they would do in traditional in-person diabetes education.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.si8y5tkfmnrj"></a><span>How do members log their weight? Does the HealthSlate App integrate with any wireless scales?</span>
</h3>

<p class="c2"><span>Members can log weight using the Add Log (circled cross) button in the top right corner of the app. The app is not integrated with any wireless scales.</span>
</p>

<p class="c1"><span class="c7"></span></p>

<p class="c1"><span class="c7"></span></p>

<h2 class="c2 c5"><a name="h.9bszjgu2a2ph"></a><span>Activity and Sleep Tracking</span></h2>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.hqziiqdko1xc"></a><span>When and how does the member receive their tracker? </span></h3>

<p class="c2"><span>In most cases the member will receive the tracker in week 3 or 4. However, this may vary depending on how your organization has chosen to incorporate the physical activity tracking device part of the program.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.p4t02wbov10"></a><span>Does the tracker come with instructions for how to use it? How can the member get help getting it working?</span>
</h3>

<p class="c2"><span>Yes, the tracker includes a User Manual, and the member can get help from Misfit or from HealthSlate Tech Support.</span>
</p>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.iacovpe6xf8y"></a><span>Does the member need to install a separate app for use with the tracker?</span>
</h3>

<p class="c2"><span>Currently, the member must install the Misfit app from the app store. We are working to integrate the device such that installing the Misfit app will no longer be required.</span>
</p>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.ot026gy2c8f"></a><span>What exactly does the Activity and Sleep Tracker track?</span></h3>

<p class="c2"><span>It tracks total steps taken per day, steps per hour, % of goal steps taken, total hours slept, and hours of deep sleep. </span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.o45gb96u5sh9"></a><span>What should the member do if their tracker stops working?</span>
</h3>

<p class="c2"><span>Contact HealthSlate Tech Support, which they can do via the My Team page in the app.</span></p>

<p class="c1"><span></span></p>

<p class="c1"><span></span></p>

<h2 class="c2 c5"><a name="h.o05kyougln"></a><span>Medications Tracking</span></h2>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.o8fdqldayp8u"></a><span>Why doesn&rsquo;t the app or Coach Portal keep track of the member&rsquo;s specific medications?</span>
</h3>

<p class="c2"><span>The HealthSlate server is not integrated with the EMR of the member&rsquo;s Provider. Therefore, we cannot be sure at any given time that the information provided by the member or their referral form is still current. &nbsp;</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.rvydee85clas"></a><span>Why are reminders to take medications sent via text message instead of via app notifications like all other reminders? </span>
</h3>

<p class="c2"><span>Text messages are more intrusive than app reminders so we need to greatly restrict what we send via text message so as not to annoy or overwhelm the member. We only want to sent the very most important or urgent information via text. Providers and educators have told us that remembering to take medications is the single most important thing for members to do.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.jlrase370kr1"></a><span>Does the the app include refill reminders?</span></h3>

<p class="c2"><span>Not at this time.</span></p>

<p class="c1"><span class="c7"></span></p>

<p class="c1"><span class="c7"></span></p>

<h2 class="c2 c5"><a name="h.iejhochxi5ir"></a><span>The Group Feed</span></h2>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a name="h.wj6doqs6566w"></a><span>Who is in which Group? Do members who are patients of my organization see members from other healthcare organizations in the Group Feed?</span>
</h3>

<p class="c2"><span>The Group Feed is not organized by partner healthcare organization. Currently, there is a single Group Feed for all members from all partner organizations. As more members join the program we intend to create smaller groups based on geographic and other criteria. </span>
</p>

<p class="c1"><span class="c7"></span></p>

<h3 class="c2 c5"><a name="h.a24epa3z3pgs"></a><span>What can members &ldquo;share&rdquo; with the Group Feed?</span>
</h3>

<p class="c2"><span>Members can share processed meal logs and physical activity logs. They can also post comments and pictures to the Group Feed. They can comment on logs shared and comments and pictures posted by other members to the Group Feed.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.tig7zvwe8cgc"></a><span>What can members NOT post to the Group Feed?</span></h3>

<p class="c2"><span>Glucose, medications, and weight logs cannot be shared to the Group Feed.</span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.4nr59y9yd9w"></a><span>Is any type of sharing to the Group Feed defaulted to ON? Is it required?</span>
</h3>

<p class="c2"><span>No. No sharing is required and nothing is shared by default.</span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.jmi1jsgxhxih"></a><span>Can I post to the Group Feed?</span></h3>

<p class="c2"><span>No, the Group Feed is intended as a peer support group. </span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.v6vr9z94xzew"></a><span>Is the Group Feed monitored?</span></h3>

<p class="c2"><span>Not actively. Members are informed in the Terms of Service that it is not monitored, but that they can report anything they consider inappropriate via the app, and that they can be terminated from the program for posting anything </span><span>inaccurate</span><sup><a
        href="#cmnt4" name="cmnt_ref4">[d]</a></sup><span>.</span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.lqj7nsp15gy5"></a><span>What should I do if I see a member post something inaccurate or inappropriate on the Group Feed?</span>
</h3>

<p class="c2"><span>You should inform Dan Sheeran at dsheeran@healthslate.com.</span></p>

<p class="c1"><span></span></p>

<p class="c1"><span></span></p>

<h2 class="c2 c5"><a name="h.aahttk2ghyj"></a><span>Technical and Security Questions</span></h2>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.lzej2lhvt1qs"></a><span>Does the HealthSlate Portal integrate with our EMR? </span></h3>

<p class="c2"><span>Not at this time.</span></p>

<h3 class="c2 c5"><a name="h.om01l1pdlwjn"></a><span><br>Can I use any computer to access the HealthSlate Coach Portal?</span>
</h3>

<p class="c2"><span>No. As of now, you can only use your office computer or a HealthSlate-approved Chromebook that has been configured in accordance with our guidelines. This is the case both for call quality reasons and for data security and privacy reasons.</span>
</p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.jopawjg1a5u2"></a><span>Can I use any browser?</span></h3>

<p class="c2"><span>No. The Coach Portal is designed to work with the Chrome browser.</span></p>

<p class="c1"><span></span></p>

<h3 class="c2 c5"><a name="h.4gz7gtoejtkf"></a><span>Can I access the HealthSlate Coach Portal outside of work? From home? Starbucks?</span>
</h3>

<p class="c2"><span>The answer to this depends on your organization&rsquo;s security policies. Your organization&rsquo;s policies regarding accessing Protected Health Information outside of the office still apply. That said, if you have a secure non-work environment that your organization allows you to use, you may access the Coach Portal as long as you are using a HealthSlate-approved Chromebook that has been configured in accordance with our guidelines.</span>
</p>

<p class="c1"><span class="c11"></span></p>

<h3 class="c2 c5"><a
        name="h.g6pbdroaxuqr"></a><span>Can I access the Coach Portal via my phone or iPad or other tablet?</span></h3>

<p class="c2"><span>Not at this time.</span></p>

<div class="c10"><p class="c18"><a href="#cmnt_ref1" name="cmnt1">[a]</a><span class="c9">I think there should be a q/a re: what to do after a 1:1 with a member? A: send impt info via message to lead coach</span>
</p></div>
<div class="c10"><p class="c18"><a href="#cmnt_ref2" name="cmnt2">[b]</a><span class="c9">In general I prefer we use the term &quot;check&quot; vs &quot;test&quot;</span>
</p></div>
<div class="c10"><p class="c18"><a href="#cmnt_ref3" name="cmnt3">[c]</a><span class="c9">this should be explained in the answer, that only the coach can set and adjust carb ceilings. Also there doesn&#39;t seem to be info on how to actually set carb ceilings and other things in the Settings field, I suggest including that as a separate question.</span>
</p></div>
<div class="c10"><p class="c18"><a href="#cmnt_ref4" name="cmnt4">[d]</a><span class="c9">how about also offensive and inappropriate?</span>
</p></div>
</div>
<script type="text/javascript">
    $("#faqsTab").addClass("selected-tab").parents("ul").prev("li").addClass("selected-tab");;
</script>

</body>
</html>