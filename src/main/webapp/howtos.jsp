<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
    <title>Member App How To Videos</title>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="x-ua-compatible" content="IE=Edge"/>
    <!--[if (gte IE 9) | (!IE)]><!-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />
    <script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
    <!--<![endif]-->

    <!--[if lt IE 9]>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">
    <![endif]-->

    <!--[if gt IE 8]>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
    <![endif]-->

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css?v=0.4" />

    <style type="text/css">

        .video-list li{
            border-bottom: 1px solid #ddd;
            margin-bottom: 0 !important;
            transition: all 0.2s linear;
        }

        .video-list li a{
            transition: all 0.2s linear;
        }

        .video-list li a:hover{
            border-left: 5px solid #8AB329;
        }

        .video-selected{
            border-left: 5px solid #8AB329;
        }

        .video-list li p{
            margin-bottom: 0 !important;
        }

        p.video-title{
            color: #444;
            font-size: 16px;
            letter-spacing: 1px;
        }

        p.video-time{
            color: #ADACAC;
            font-size: 14px;
        }

        .border-right-grey{
            border-right:1px solid #ddd;
        }

        iframe{
            border: 1px solid #ddd;
        }

        .help-text{
            color: #CACACA;
            font-size: 21px;
            height: 420px;
            line-height: 350px;
            text-align: center;
        }

        .list-loading{
            position: absolute;
            width: 100%;
            height: 100%;
            background: #fff;
            top: 0;
            left: 0;
            opacity: 0.6;
        }

        .list-loading label{
            text-align: center;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-left: -56px;
            color: #799c25;
            font-size: 18px;
            min-width: 100px;
        }
    </style>
</head>

<body>

<div class="max-width-1129 margin-top-10 margin-bottom-20">
    <div class="row row-full">
        <div class="columns medium-12">
            <div class="hs-page-heading">
                <h2>
                    Member App How To Videos
                </h2>
            </div>
        </div>
    </div>

    <div class="row row-full bg-white">

        <div class="medium-3 large-3 three columns border-right-grey padding-lr-0">
            <ul id="listHowtoVideos" class="side-nav video-list">

            </ul>

            <div id="listLoadingSpinner" class="list-loading">
                <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
            </div>
        </div>
        <div class="medium-9 large-9 nine columns">
            <h3 id="hVideoTitle" class="meal-summary-text margin-top-10"></h3>
            <div class="video-player">
                <iframe id="iframePlayer" src="about:blank" width="100%" height="520" class="hide"></iframe>
                <p class="help-text">Please choose video from list to play</p>
            </div>
        </div>
    </div>
</div>

<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
<![endif]-->

<script src="${pageContext.request.contextPath}/resources/js/hs.how-to-videos.js"></script>

<script type="text/javascript">

    $(function(){
        $("#howToTab").addClass("selected-tab").parents("ul").prev("li").addClass("selected-tab");
        HSHowToVideos.init({token:'${token}', mobileOrWeb: false});
    });

</script>
</body>
</html>