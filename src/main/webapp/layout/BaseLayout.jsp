<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
<head> 
	<%--<meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0" />--%>
    <meta http-equiv="x-ua-compatible" content="IE=Edge"/>
	<meta http-equiv="EXPIRES" content="0" />
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-STORE">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="MUST-REVALIDATE">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="PRE-CHECK=0">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="POST-CHECK=0">
	<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">

    <link rel="shortcut icon" href="${pageContext.request.contextPath}/favicon.ico" />

	<title><tiles:insertAttribute name="title" ignore="true" /></title>	
	
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-3/css/bootstrap.min.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/header.style.css?v=0.7" />
	
	
	<!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation.min.css" />	        
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ui-lightness/jquery-ui-1.10.4.min.css">   
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery.dataTables.css" />
	
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/common.style.css?v=0.2" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css?v=0.5" />
		
	<!--[if (gte IE 9) | (!IE)]><!-->
	    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
	<!--<![endif]-->       
	
	<!-- Foundation 4 for IE 9 and later -->
	<!--[if gt IE 8]><!-->	    
	    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
	<!--<![endif]-->	
	
	<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.migrate.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/idle-timer.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/common.js?v=0.3"></script>
    <script type="text/javascript">
		$(document).foundation();
	</script>
</head>
<body>
    <div id="header"><tiles:insertAttribute name="header" /></div>
	<div id="holder">	
	    <div id="body"><tiles:insertAttribute name="body" /></div>
	    <div id="footer"><tiles:insertAttribute name="footer" /></div>
	</div>
</body>
</html>