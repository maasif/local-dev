	<%@ taglib prefix="s" uri="/struts-tags"%>
	
	<nav class="navbar navbar-default" role="navigation">
	  <div class="container-fluid max-width-1129">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header margin-top-15">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>

          <s:if test="%{#session.PROVIDER.type != null}">
              <s:a class="navbar-brand" action="coachesDashboard.action" namespace="/app/educator"><img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" width="200" /></s:a>
          </s:if>
          <s:elseif test="%{#session.ADMIN != NULL}">
              <s:a class="navbar-brand" action="viewDashBoard.action" namespace="/app/admin"><img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" width="200" /></s:a>
          </s:elseif>
           <s:else>
               <s:a class="navbar-brand" action="viewDashBoard.action" namespace="/app/facilityadmin"><img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" width="200" /></s:a>
           </s:else>
	    </div>
	    <div class="collapse navbar-collapse margin-top-15 padding-right-0" id="bs-example-navbar-collapse-1">
		    <ul id="hsHeaderBar" class="nav navbar-nav hs-header">
		    	<s:if test="%{#session.ADMIN != NULL}"> 	       
		       		<%--<li><s:a id="patientsTab" class="navbar-brand" action="members.action" namespace="/app/admin">Patients</s:a></li>
		       		<li><s:a id="groupsTab" class="navbar-brand" action="viewSocialGroups.action" namespace="/app/admin">Groups</s:a></li>--%>
		       		<li><s:a id="facilityTab" class="navbar-brand" action="allFacilities.action" namespace="/app/admin">Facilities</s:a></li>
					<li><s:a id="providerTab" class="navbar-brand" action="coaches.action" namespace="/app/admin">Coaches</s:a></li>
					<li><s:a id="providerPatientMapTab" class="navbar-brand" action="membersData.action" namespace="/app/admin">Members Data</s:a></li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
                            Member Recruitment <span class="caret"></span>
                        </a>
                    </li>
                    <ul class="dropdown-menu" role="menu">
                        <li><s:a id="inviteMembersTab" class="navbar-brand" action="memberRecruitment.action" namespace="/app/admin">Add Member</s:a></li>
                        <li><s:a id="prospectiveMembersTab" class="navbar-brand" action="prospectiveMembers.action" namespace="/app/admin">Prospective Members</s:a></li>
                    </ul>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
                            More <span class="caret"></span>
                        </a>
                    </li>
                    <ul class="dropdown-menu" role="menu">
                        <li><s:a id="hippaLogsTab" class="navbar-brand" action="hippaLogs.action" namespace="/app/admin">HIPPA Logs</s:a></li>
                        <li><s:a id="feedbackTab" class="navbar-brand" action="Feedback.action" namespace="/app/admin">Feedback/Support</s:a></li>
                    </ul>
		       </s:if>
                <s:elseif test="%{#session.FACILITY_ADMIN != NULL}">
                    <li><s:a id="facilityTab" class="navbar-brand" action="facility.action" namespace="/app/facilityadmin">Facility</s:a></li>
                    <li><s:a id="providerTab" class="navbar-brand" action="coaches.action" namespace="/app/facilityadmin">Coaches</s:a></li>
                    <li><s:a id="providerPatientMapTab" class="navbar-brand" action="membersData.action" namespace="/app/facilityadmin">Members Data</s:a></li>
                    <li><s:a id="feedbackTab" class="navbar-brand" action="Feedback.action" namespace="/app/facilityadmin">Feedback</s:a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
                            Member Recruitment <span class="caret"></span>
                        </a>
                    </li>
                    <ul class="dropdown-menu" role="menu">
                        <li><s:a id="inviteMembersTab" class="navbar-brand" action="memberRecruitment.action" namespace="/app/facilityadmin">Add Member</s:a></li>
                        <li><s:a id="prospectiveMembersTab" class="navbar-brand" action="prospectiveMembers.action" namespace="/app/facilityadmin">Prospective Members</s:a></li>
                    </ul>
                </s:elseif>
		        <s:else>
                   <s:if test="%{#session.PROVIDER.type == 'Food Coach'}">

                       <li class="dropdown">
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
                               Meals <span class="caret"></span>
                           </a>
                       </li>
                       <ul class="dropdown-menu" role="menu">
                           <li><s:a id="unProcessedMeals" class="navbar-brand" action="unProcessedMeals.action" namespace="/app/educator">Unprocessed Meals</s:a></li>
                           <li><s:a id="shareableMealsTab" class="navbar-brand" action="shareableMeals.action" namespace="/app/educator">Shareable Meals</s:a></li>
                       </ul>
                   </s:if>
		       		<li><s:a id="patientsTab" class="navbar-brand" action="members.action" namespace="/app/educator">My Members</s:a></li>
		       		<%--<li><s:a id="reportError" class="navbar-brand" action="viewReportErrors.action" namespace="/app/educator">Errors</s:a></li>--%>
		       		<li><s:a id="DYKTab" class="navbar-brand" action="groupPosts.action" namespace="/app/educator">Group Posts</s:a></li>
                    <li><s:a id="feedbackTab" class="navbar-brand" action="Feedback.action" namespace="/app/educator">Feedback</s:a></li>
		       		<%--<li><s:a id="patientPoplulationTab" class="navbar-brand" action="fetchPatientPopulation.action" namespace="/app/educator">Patient Population</s:a></li>--%>
		       </s:else>
                <s:if test="%{#session.PROVIDER != NULL}">

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
                            Member Recruitment <span class="caret"></span>
                        </a>
                    </li>
                    <ul class="dropdown-menu" role="menu">
                        <li><s:a id="inviteMembersTab" class="navbar-brand" action="memberRecruitment.action" namespace="/app/educator">Add Member</s:a></li>
                        <li><s:a id="prospectiveMembersTab" class="navbar-brand" action="prospectiveMembers.action" namespace="/app/educator">Prospective Members</s:a></li>
                    </ul>

                </s:if>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
                        Help <span class="caret"></span>
                    </a>
                </li>
                <ul class="dropdown-menu" role="menu">
                    <li><s:a id="faqsTab" class="navbar-brand" action="faqs.action" namespace="/app">FAQs</s:a></li>
                    <li><s:a id="quickStartTab" class="navbar-brand" action="quickStart.action" namespace="/app">Quick Start Guide</s:a></li>
                    <li><s:a id="howToTab" class="navbar-brand" action="howtos.action" namespace="/app">Member App How To Videos</s:a></li>
                </ul>
		    </ul>
		    <ul class="nav navbar-nav navbar-right hs-header-welcome">
				<s:if test="%{#session.ADMIN != NULL}">
					<li>
						<a id="openProfileMenu" href="#"><s:property value="%{#session.USER.firstName}" /> <span class="caret-top">&#9660;</span></a>
					</li>
				</s:if>
				<s:else>
					<li>
                        <span class="label warning sp-facility-name"><s:property value="%{#session.USER.provider.facility.name}"/></span>
						<a href="#" id="openProfileMenu">My Account (<s:property value="%{#session.USER.lastName}" />, <s:property value="%{#session.USER.firstName}" />) <span class="caret-top">&#9660;</span></a>
					</li>
				</s:else>
                <li class="divider"><a href="#" class="padding-lr-0">|</a></li>
		        <li><s:a id="logout" action="logout" cssClass="logout" namespace="/app">Log Out</s:a></li>
		    </ul>
		  </div>
	  </div>
	</nav>

	<div class="login-menu" style="display: none;">
		<div class="triangle-border top">
			<p class="change-pass-text">Change my password</p>
			<div class="row">
				<div class="small-12 columns">
					<label class="inline margin-bottom-0">
						Old Password
						<input type="password" id="txtOldPassword" name="oldPassword" maxlength="20" class="margin-bottom-0" />
						<small class="error">Required.</small>
					</label>
					<label class="inline margin-bottom-0">
						New Password
						<input type="password" id="txtNewPassword" name="newPassword" maxlength="20" class="margin-bottom-0" />
						<small class="error">Required.</small>
					</label>
                    <label class="inline margin-bottom-0">
                        Confirm Password
                        <input type="password" id="txtHeaderConfirmPassword" name="confirmHeaderPassword" maxlength="20" class="margin-bottom-0" />
                        <small class="error">Required.</small>
                    </label>

					<label id="loadingPassword" class="inline" style="display: none">
						<img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Working...
					</label>
					<input id="btnChangePassword" type="button" class="small button expand radius btn-teal btn-sign-in margin-top-14 margin-bottom-0" value="CHANGE MY PASSWORD" />
				</div>
			</div>
		</div>
	</div>

    <div class="session-expired-overlay">
        <div id="divSessionExpired" class="session-expired alert-danger">
            <p class="margin-bottom-0">Your session will expire in <b><span id='sessionTimeoutCountdown'></span></b> seconds.</p>
            <p class="margin-bottom-0">Click <a href="" onclick="HSSessionExpire.refreshSession();">here</a> to cancel it.</p>
        </div>

        <div class="session-expired-dialog">
            <h3>Session Expired</h3>
            <p>Your session has expired click <a href="login.action"> here </a> to go to login page.</p>
        </div>
    </div>

    <div id="facilityLeadCoachDialog" class="reveal-modal small" data-reveal>
        <h3 class="meal-summary-text remove-bold">Facility Lead Coach</h3>
        <p id="pFacilityLeadCoachDialog" class="lead dialog-para">Facility default lead coach is not set. Do you want to set it now?</p>
        <a id="closeFacilityLeadCoachDialog" class="close-reveal-modal">&#215;</a>
        <div id="navFacilityLeadCoachDialog" class="align-right hide">
            <a href="facility.action" id="btnFacilityLeadCoachDialogYes" class="button radius btn-teal margin-top-10 margin-bottom-0">Yes</a>
            <a href="#" class="button secondary radius margin-top-10 margin-bottom-0" onclick="$('#closeFacilityLeadCoachDialog').click(); return false;">No</a>
        </div>
    </div>

    <script src="${pageContext.request.contextPath}/resources/bootstrap-3/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/hs.validator.js"></script>
	<script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js?v=0.1"></script>

    <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/hs.change.password.js?v=0.1"></script>
    <script src="${pageContext.request.contextPath}/resources/js/hs.session.expired.js?v=0.4"></script>
    <script src="${pageContext.request.contextPath}/resources/js/hs.force-refresh-resources.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/moment.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/hs.dateutils.js?v=0.3"></script>

	<script type="text/javascript">

		var ALERT_MESSAGES_HEADER = Object.create(AlertMessage).init(),
			AJAXING_HEADER = Object.create(Ajaxing),
			HS_VALIDATOR_HEADER = Object.create(HSValidator).init(),
			shouldSubmit = false;
            $spUnreadCountHeader = $("#spUnreadCountHeader"),
            APP_BASE_URL = '${pageContext.request.contextPath}/';

		$(function(){

            $(".sorting_1").removeClass("sorting_1 sorting_1");

            HSSessionExpire.init();
            HSForceRefreshResources.refreshResources();

            $('li.dropdown a').on('click', function(event) {

                var $a = $(this).addClass("selected-tab"),
                    pos = $a.parent().position(),
                    $menu = $a.parent().next("ul").css("display", "block");

                $menu.css({
                    left: pos.left
                });
            });

			hideOutsideClick(".login-menu");
            hideOutsideClick(".dropdown-menu");

            setFooterAlignment();
            initChangePassword();

            /*$("#inviteMembersTab").on("click", function(e){
                e.preventDefault();
                $(document).mouseup();
                checkFacilityLeadCoach("memberRecruitment.action");
            });*/
		});

        function checkFacilityLeadCoach(link, forAdmin){
            var coachFacilityLeadCoach = '${session.PROVIDER.facility.leadCoach}',
                facilityAdminLeadCoach = '${session.FACILITY.leadCoach}',
                isAdmin = ("${session.ADMIN}") ? true: false,
                isFacilityAdmin = ("${session.FACILITY_ADMIN}") ? true: false,
                isCoach = ("${session.PROVIDER}") ? true: false,
                $navFacilityLeadCoachDialog = $("#navFacilityLeadCoachDialog"),
                $btnFacilityLeadCoachDialogYes = $("#btnFacilityLeadCoachDialogYes"),
                $pFacilityLeadCoachDialog = $("#pFacilityLeadCoachDialog"),
                $facilityLeadCoachDialog = $("#facilityLeadCoachDialog");

            //means ADMIN logged in
            if(forAdmin){
                if(forAdmin.leadCoach){
                    window.location = link;
                }else{
                    if(forAdmin.userType == "Coach"){
                        $navFacilityLeadCoachDialog.addClass("hide");
                        $pFacilityLeadCoachDialog.text("Facility default lead coach is not set. Please check with your facility admin.");
                    }else{
                        $navFacilityLeadCoachDialog.removeClass("hide");
                        $btnFacilityLeadCoachDialogYes.attr("href", "facility.action?facilityId="+forAdmin.facilityId);
                        $facilityLeadCoachDialog.foundation("reveal", "open");
                    }
                }
            }else{
                if(isAdmin){
                    window.location = link;
                }else if(coachFacilityLeadCoach && isCoach){
                    window.location = link;
                }else if(facilityAdminLeadCoach && isFacilityAdmin){
                    window.location = link;
                }else{

                    if(isFacilityAdmin){
                        $navFacilityLeadCoachDialog.removeClass("hide");
                    }

                    if(isCoach){
                        $pFacilityLeadCoachDialog.text("Facility default lead coach is not set. Please check with your facility admin.");
                    }

                    //show dialog here if lead coach is not set
                    $facilityLeadCoachDialog.foundation("reveal", "open");
                }
            }
        }

        function loadExpCollapse(){
            setTimeout(function(){
                $(".coach-section-loading").hide();
                $(".coach-area-expand").show();
            }, 100);
        }

		function hideOutsideClick(className){
			$(document).mouseup(function (e){
				var container = $(className);
				if (!container.is(e.target) && container.has(e.target).length === 0) {
					container.hide();

                    if(container.prev("li.dropdown").length > 0){
                        $("li.dropdown").children("a").removeClass("selected-tab");
                    }
				}
			});
		}

        function setFooterAlignment(marginTop){
            var $footer = $('#footer'),
                docHeight = $(window).height(),
                footerHeight = $footer.height(),
                footerTop = $footer.position().top + footerHeight;

            if(!marginTop){
                marginTop = 0;
            }

            if (footerTop < docHeight) {
                $footer.css('margin-top', 10 + (footerHeight - marginTop) + 'px');
            }
        }

        function positionFooter(){
            var padding_top = $(".footer").css("padding-top").replace("px", ""),
                page_height = $(document.body).height() - padding_top,
                window_height = $(window).height(),
                difference = window_height - page_height;

            if (difference < 0) {
                difference = 0;
            }

            $(".footer").css({
                padding: difference + "px 0 0 0"
            })
        }

        function getUnreadCoachMessagesCount() {
            var that = this,
                postRequestActions = {
                    "successCallBack": onGettingCoachMessagesCount
                };

            AJAXING_HEADER.sendPostRequest("getUnreadCoachesMessageCount.action", null, postRequestActions);
        }

        function onGettingCoachMessagesCount(data){
            $spUnreadCountHeader.text(data.DATA);
            alignCoachMessageUnreadCount()
        }

        function alignCoachMessageUnreadCount(){
            var unreadCount = parseInt($spUnreadCountHeader.removeClass("left-5").text()),
                leftClass = "";

            if(unreadCount > 10){
                leftClass = "left-5";
            }

            $spUnreadCountHeader.addClass(leftClass).text(unreadCount);
        }
	</script>