<!doctype html>
<%-- <%@ taglib prefix="s" uri="/struts-tags"%> --%>
<html>
  <head>        
    <title>Sign in</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="x-ua-compatible" content="IE=Edge"/>
	  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-3/css/bootstrap.min.css" />
    <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation.min.css" />    
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->

      <!--[if lt IE 9]>
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
      <![endif]-->

	  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
	  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
	  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/facility.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/login.style.css?v=0.3" />
 	 </head>
  <body>

  <div id="loading" class="loader position-fixed">
	  <div class="image-loader">
		  <p class="progress-please-wait">Please wait...</p>
		  <div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
			  </div>
		  </div>
		  <p id="progressTxt" class="progress-text">We're sending email</p>
	  </div>
  </div>

	  <a href="#" data-reveal-id="emailDialog" style="display:none" id="emailBoxOpen" >Forgot password?</a>
	  <div id="emailDialog" class="reveal-modal medium six" data-reveal>
		  <div id="errorEmail" class="alert-upload error" style="display:none;">______________________</div>
		  <div class="small-12 twelve columns padding-lr-0">
			  <h3 id="uploadHeader" class="sign-in-text enter-email-dialog">Enter e-mail address</h3>
			  <div class="row collapse" style="display:block" id="content">
				  <div class="small-10 ten columns" id="txtFieldsDiv">
					  <input type="text" id="txtEmail" placeholder="Enter your email address..." />
				  </div>
				  <div class="small-2 two columns" id="browseDiv">
					  <a href="#" id="btnSubmitEmail" class="button postfix btn-teal">SUBMIT</a>
				  </div>
			  </div>
			  <div id="successMessage" style="display:none"></div>
		  </div>
		  <a class="close-reveal-modal" id="btnCloseUploadDialog">&#215;</a>
	  </div>

  	<div id="mainPage" class="main-page overflow-fix main-page-login-i8">
  			<div class="logo">
      			<img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" />
    		</div>
  	  	                 
             <div class="padding-20">
             	<h4 class="sign-in-text">Sign in</h4>
             </div>             
             <div class="content-login-form">                			  
	      		<div class="padding-20" id="page1">      	  		              		          
		          <div id="ajaxError" data-alert class="alert-box error radius margin-lr-10"></div> 
		         	<form id="f" name='f' action="/PatientApp/j_spring_security_check" method="POST" >
			            <div class="row">
			                <div class="small-12 twelve columns">
			                  <label class="align-left font-18 padding-20 ie-error-style position-rel">
			                  	Email address
			                    <input id="txtUserName" autofocus="autofocus" type="text" name='j_username' class="validate-me login-input login-input-i8" />
			                    <img alt="" class="img-credentials" src="${pageContext.request.contextPath}/resources/css/images/username.png">
			                  </label>
			                  <small class="error error-msg login-input-i8">Username is required</small>
			                </div>
			              </div>
			
			            <div class="row">
			              <div class="small-12 twelve columns">
			                <label class="align-left font-18 padding-20 ie-error-style position-rel">	
			                  Password	                 
			                  <input id="txtPassword" type="password" name='j_password' class="validate-me login-input login-input-i8" />
			                  <img alt="" class="img-credentials" src="${pageContext.request.contextPath}/resources/css/images/password.png">
			                </label>
			                <small class="error error-msg login-input-i8">Password is required</small>
			              </div>
			            </div>
			            <div class="row">
			                <div class="small-12 padding-lr-22 align-left twelve columns margin-bottom-0">		                	
			                	<input type="submit" id="btnSignIn" type="button" class="small button radius btn-teal btn-sign-in login-input-i8" value="SIGN IN" name="submit"/>
								<a href="#" id="forgotPassword" class="sign-in-text forgot-password">Forgot your password?</a>
			                 </div>	                  			                  		                        
			            </div>   
		          </form>  		          
	         </div>		         
    	</div>    	
    </div>

	<div id="footer" class="footer">

		<div class="row row-full">

			<div class="columns medium-12">
				<div class="copyright-healtshlate">
					<img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" />
				</div>
			</div>

			<%--<div class="columns medium-6">

				<span class="grey-separator"></span>

				<div class="about-healthslate">
					<h6 class="color-green">About HealthSlate</h6>
					<p class="our-mission">Our mission is to help with chronic diseases maintain their health through highly indisclosed programs of education, behavior medication and clinical support.
						<br/><br/>
						Combining expertise in consumer behavior, mobile technology and evidenced-based medication. HealthSlate improves patients' quality of life and health outcomes. Learn more at <a href="http://www.healthslate.com" target="_blank">www.healthslate.com</a>
					</p>
				</div>
			</div>--%>
		</div>

	</div>

  <!--[if lt IE 9]>
  <script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
  <![endif]-->

	<!--[if (gte IE 9) | (!IE)]><!-->
	    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
	<!--<![endif]-->

  <!-- Foundation 3 for IE 8 and earlier -->
  <!--[if lt IE 9]>
  <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
  <![endif]-->

	<!-- Foundation 4 for IE 9 and later -->
	<!--[if gt IE 8]><!-->	    
	    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
	<!--<![endif]-->

    <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
    <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
    <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>

    <script src="${pageContext.request.contextPath}/resources/js/jquery.migrate.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/hs.validator.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/common.js"></script>

  </body>
     
     <script type="text/javascript">

		 var ajaxing = Object.create(Ajaxing),
			 alertMessages = Object.create(AlertMessage).init(),
			 hsValidator = Object.create(HSValidator).init();

	     $(function(){		    	 
	    	var browserVersion = $.browser.version;

			 if(localStorage){
				 localStorage.removeItem('messageTxtStorage');
//				 localStorage.clear();
			 }

	 		if(!$.browser.msie || browserVersion >= 9){
	     		$(document).foundation();   	     		
	     	}

             $("#emailDialog").on({
                 opened: function () {
                     $("#txtEmail").focus();
                 },
                 closed: function () {

                 }
             });

	 		var username = getCookie('username');
	 		if (username.length) {
	 			$('#txtUserName').val(username.substring(1, username.length-1));
	 			$('#txtPassword').focus();
	 		} else {
	 			$('#txtUserName').focus();
	 		}
	 		
	 		var error = getQuerystringParameterByName('error'),
    		$error = $("#ajaxError");
    	
	    	if(error === 'true'){
				alertMessages.showPermanentAlertMessage($error, "Invalid Email address or Password", alertMessages.ERROR);
	    	} else if(error == 'pwchange') {
				alertMessages.showPermanentAlertMessage($error, "Your password has been expired. Please click <a id='btnResetPassword' href='#' style='color:yellow'>here</a> to reset.", alertMessages.ERROR);

                $("#btnResetPassword").on("click", function(e){
                    e.preventDefault();
                    openEmailDialog();
                });

			} else if(error == 'blocked') {
				alertMessages.showPermanentAlertMessage($error, "Your account has beed blocked. Please wait for a few minutes and try again.", alertMessages.ERROR);
			}

			 $("#forgotPassword").on("click", function(e){
				 e.preventDefault();
				 openEmailDialog();
			 });

			 $("#btnSubmitEmail").on("click", function(){
				 verifyEmail();
				 return false;
			 });

			 $("#txtEmail").on("keypress", function(e){
				 var code = e.keyCode || e.which;
				 if(code == 13) {
					 verifyEmail();
					 return false;
				 }
			 });

	    	Android.setTopHeading("Log In");
	    	Android.showBackNav(false);
	     });

		 function openEmailDialog(){
			 $("#ajaxError").hide();
			 $("#content").show();
			 $("#successMessage").hide();
             $("#txtEmail").val("");
             $("#emailBoxOpen").click();
			 $("#uploadHeader").text("Enter e-mail address");
		 }

		 function verifyEmail(){

			 var $txtEmail = $("#txtEmail"),
				 userName = $txtEmail.val(),
				 $error = $("#errorEmail").hide();

			 if(userName.trim().length == 0){
				 alertMessages.showAlertMessage($error, "Please enter email", alertMessages.ERROR);
			 }else{
				 if(hsValidator.validateFieldDataByType(hsValidator.VALIDATORS.EMAIL, userName)){

					 var dataToSend = {"userName": userName},
						 postRequestActions = {
							 "loading": $("#loading"),
							 "error": $error,
							 "successCallBack": onSuccessEmailSend
						 };

                     var resetAction = "app/sendResetEmail.action";
                     if(document.URL.indexOf("/app") > -1){
                         resetAction = "sendResetEmail.action";
                     }
					 ajaxing.sendPostRequest(resetAction, dataToSend, postRequestActions);
				 }else{
					 alertMessages.showAlertMessage($error, "Please enter valid email", alertMessages.ERROR);
				 }
			 }
		 }

		 function onSuccessEmailSend(result){
			 var $error = $("#errorEmail").hide();
			 if(result.STATUS === "SUCCESS"){
				 $("#content").hide();
				 $("#successMessage").show().text("An email has been sent successfully to your email address.");
			 }else{
				 alertMessages.showAlertMessage($error, result.REASON, alertMessages.ERROR);
			 }
		 }

	  </script>
    
</html>