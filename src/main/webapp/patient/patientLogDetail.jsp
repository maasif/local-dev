<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>

<title>Patient Log Detail</title>
  	   
	<style type="text/css">
		label{
			cursor: default;
		}
		.mainDiv {
			max-width: 90% !important;
			margin: 0 auto;
		}
		.back-btn-div{
			margin: 0 auto;
			width: 90%;
			margin-bottom: 13px;
			margin-top: 15px;
		}
		.back-btn-style{
			width: 135px;
			height: 44px;
			font-size: 22px;
			background-color: #2ba6cb;
			border-color: #2ba6cb;
		}
		#header{
			display: none;
		}
	</style>
</head>
<body>
	
	<div id="waiting-div" style="text-align: center; display: none;">
		<img alt="" src="${pageContext.request.contextPath}/resources/images/indicator.gif">		
	</div>
	
	<s:actionmessage id="actionMessage" cssClass="alert-success notification-style"/>
	
	<div class="row row-full mainDiv">			
		<div class="section-container">
			<div class="switch centered" style="margin-top: 10px !important;">
				<ul id="switch" data-content="switch-elements" class="button-group radius centered">
					<li><a href="#" data-attached-elem="foodDetails" class="button large margin-bottom-0">Food</a></li>
					<li><a href="#" data-attached-elem="otherDetails" class="button large secondary margin-bottom-0">Other</a></li>
				</ul>
			</div>			
			<div class="switch-container">
					
				<div id="foodDetails" class="display-none switch-elements">
					
					<h3 class="padding-10">Patient Food Notes</h3>
					
					<div class="notes">
			      		<span>
			      			<s:if test="%{log.foodLogSummary == null}"><span class="span-style"></span></s:if>
			      			<s:elseif test="%{log.foodLogSummary.notes == null}"><span class="span-style"></span></s:elseif>
			      			<s:else><span class="span-style"><s:property value="%{log.foodLogSummary.notes}"/></span></s:else>
			      		</span>
			      	</div>	
			      	
		      		<h3 class="padding-10">Nutrition Facts</h3>
		      		
					<div id="nutriDetailDiv" class="section-content">
			      		
			      		<s:if test="%{log.foodLogSummary == null}"><span class="span-style"></span></s:if>
			      		<s:elseif test="%{log.foodLogSummary.foodLogDetails.size() == 0}"><span class="span-style"></span></s:elseif>
			      		<s:else>
				      		<div>
					      		<table class="logs-table-hf">
					      			<tr>
					      				<td class="width-45-perc">Food</td>
					      				<td class="width-20-perc">Carbs</td>
					      				<td class="width-20-perc">Calories</td>
					      			</tr>
						      	</table>
					      	</div>
					      	
			      			<div style="max-height: 198px; overflow-y: auto;">			      									
					      		<table id="nutTable" class="logs-table margin-bottom-0">
					      			
					      			<s:iterator value="%{log.foodLogSummary.foodLogDetails}" var="foodLogDetail">
					      				<s:if test="#foodLogDetail.myMeal == null">
					      					<tr>
					      						<td class="width-45-perc">
					      							<s:property value="#foodLogDetail.foodMaster.foodName"/>
				      							</td>
				      							<td class="width-20-perc">
				      								<s:property value="@com.healthslate.patientapp.util.CommonUtils@getCalculatedCarbsOrCaloriesSize(#foodLogDetail.foodMaster.carbs,#foodLogDetail.numberOfServings)" />
				   								</td>
				   								<td class="width-20-perc">
				   									<s:property value="@com.healthslate.patientapp.util.CommonUtils@getCalculatedCarbsOrCaloriesSize(#foodLogDetail.foodMaster.calories,#foodLogDetail.numberOfServings)" />
				   								</td>
				 							</tr>
					      				</s:if>
									</s:iterator>
					      		</table>
			      			</div>
					      	<div>
						      	<table class="logs-table-hf section-content">
					      			<tr class="total-tr-style">
					      				<td class="width-45-perc total-td-style">Total</td>
					      				<td class="width-20-perc total-td-style" id="totalCarbs"></td>
					      				<td class="width-20-perc total-td-style" id="totalCalories"></td>
					      			</tr>
						      	</table>
					      	</div>
				      	</s:else>
			      	</div>
				</div>
				<div id="otherDetails" class="display-none switch-elements">
		      		<div class="section-content">		
						<table class="logs-table">
				      		<tr>
				      			<td class="first-column">Glucose:</td>
				      			<td style="width:35%">
				      				<s:if test="%{log.glucoseLog == null}"></s:if>
				      				<s:else>
				      					<s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.glucoseLog.glucoseLevel,'mg/dl')" />
			      					</s:else>
			      				</td>
				      			<td class="second-column white-space-nowrap">Glucose Type: </td>
				      			<td>
				      				<s:if test="%{log.glucoseLog == null}"></s:if>
				      				<s:elseif test="%{log.glucoseLog.glucoseLogType == null}"></s:elseif>
				      				<s:elseif test="%{log.glucoseLog.glucoseLogType.description == null}"></s:elseif>
				      				<s:else><s:property value="%{log.glucoseLog.glucoseLogType.description}"/></s:else>
				      			</td>
				      		</tr>
				      		<tr>
				      			<td class="first-column">Cardio:</td>
				      			<td>
				      				<s:if test="%{log.exerciseLogs.size() == 0}"></s:if>
				      				<s:else>
				      					<s:if test="%{log.exerciseLogs[0].type == 'cardio'}">
				      						<s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.exerciseLogs[0].minutesPerformed,'min')" />
				      					</s:if>
				      					<s:elseif test="%{log.exerciseLogs[1].type == 'cardio'}">
					      					<s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.exerciseLogs[1].minutesPerformed,'min')" />
				      					</s:elseif>
				      					<s:else></s:else>
				      				</s:else>
				      			</td>
				      			<td class="second-column">Strength:</td>
				      			<td>
				      				<s:if test="%{log.exerciseLogs.size() == 0}"></s:if>
				      				<s:else>
				      					<s:if test="%{log.exerciseLogs[0].type == 'strength'}">
				      						<s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.exerciseLogs[0].minutesPerformed,'min')" />
				      					</s:if>
				      					<s:elseif test="%{log.exerciseLogs[1].type == 'strength'}">
					      					<s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.exerciseLogs[1].minutesPerformed,'min')" />
				      					</s:elseif>
				      					<s:else></s:else>
				      				</s:else>
				      			</td>
				      		</tr>
				      		<tr>
				      			<td class="second-column">Sleep:</td>
				      			<td>
				      				<s:if test="%{log.miscLog == null}"></s:if>
				      				<s:else>
				      					<s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.miscLog.sleepHours,'hrs')" />
				      					<s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.miscLog.sleepMinuts,'min')" />
				      				</s:else>
				      			</td> 
				      			<td class="second-column">Weight:</td>
				      			<td>
				      				<s:if test="%{log.miscLog == null}"></s:if>
				      				<s:else>
					      					<s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.miscLog.weight,'lbs')" />
				      				</s:else>
				      			</td>
				      		</tr>
				      		<tr>
				      			<td class="first-column white-space-nowrap">Took Meds:</td>
				      			<td>
				      				<s:if test="%{log.medicationLog.tookMeds == true}">Yes</s:if>
				      				<s:else>No</s:else>
				      			</td>
				      			<td class="second-column white-space-nowrap">Blood Pressure:</td>
				      			<td>
				      				<label style="cursor: default;">
				      					<s:if test="%{log.miscLog == null}"></s:if>
				      					<s:elseif test="%{log.miscLog.bloodPressureSystolic == 0}"></s:elseif>
										<s:else>
											<s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.miscLog.bloodPressureSystolic,'/')" />
											<s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(log.miscLog.bloodPressureDiastolic,'mmHg')" />
										</s:else>
									</label>
			      				</td>
				      		</tr>
				      		<tr>
				      			<td class="first-column">Log Time:</td>
				      			<td>
				      				<s:property value="getText('{0,date,MM/dd/yy hh:mm a}',{log.logTime})"/>
			      				</td>
			      				<td class="second-column">Reviewed:</td>
								<td colspan="3"><s:if test="%{log.isProcessed == 1}">
										<span class="bold-green-font">Yes</span>
									</s:if> <s:else>
										<span class="bold-red-font">No</span>
									</s:else>
						    	</td>
				      		</tr>
				      	</table>
				     </div>
		      	</div>	
			</div>
		</div>
		
		<div class="section-container">
      		<div class="section-header"><h3 class="padding-10">Professional Notes</h3></div>	      		
	      	<div class="section-content">		      	
	      		<s:if test="%{log.notes.size() == 0}"><span class="span-style"></span></s:if>
	      		<s:else>
	      			<table class="logs-table-hf">
	      				<tr>
	      					<td class="width-60-perc">Note</td>
	      					<td class="width-39-perc">Date Created</td>
	      				</tr>
      				</table>
	      			<div style="max-height: 170px; overflow-y: auto;">
		      			<table class="logs-table">
				      		<s:iterator value="%{log.notes}" var="note">
								<tr>
									<td class="width-60-perc">
										<s:property value="#note.description"></s:property>
									</td>
									<td class="width-39-perc" style="vertical-align: top !important;">
										<s:property value="%{getText('{0,date,MM/dd/yy hh:mm a}',{#note.createdDate})}"/>
									</td>
								</tr>
							</s:iterator>
						</table>
					</div>
	      		</s:else>		      		
	      	</div>
		</div>					
	</div>
    		
	<script src="${pageContext.request.contextPath}/resources/js/bootstrap-typeahead.js"></script>
	
	<script type="text/javascript">
		
		var mealOptions = [], srcMealId = "", foodSuggestions = [], foodId = "", foodNameSuggest = "";
		
		$(function() {
			
			$('#patientsTab').addClass('selected-tab');
			$('.mainDiv').css('width', '100%');
			bindSwitch($("#switch"), true);
			
			//set carbs and calories
			$("#totalCarbs").text("${totalCarbs}");
			$("#totalCalories").text("${totalCalories}");
			Android.setTopHeading("Log Detail");
			Android.showBackNav(true);
		});
	</script>
</body>
</html>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       