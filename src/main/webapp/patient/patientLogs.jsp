<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<html>

<head>

	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bigdatepicker/default.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bigdatepicker/default.date.css" />       
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/queue.style.css" />
	
	<title>Patient Logs</title>
	<style type="text/css">
		.input-field{
			width: 35% !important;
			display: inline-block !important;
		}
		.ui-datepicker-trigger{
			margin: 0px;
			height: 37px;
			padding-top: 11px;
			width: 5px;
			padding-left: 1rem;
			background-color: #428bca;
		}
		.date-style{
			width: 76%;
			display: inline-block !important;
		}
		
		.header-style{
			width:60%;
			padding:0;
			top:10px;
		}
		
		#header{
			display: none;
		}
		
		#logDate li{
			margin: 10px !important;
		}
		
		.foodNotes{
		   width: 500px;
		   white-space: nowrap;
		   overflow: hidden;         
		   text-overflow: ellipsis;
		}
	</style>
</head>
<body>
	<s:form id="formLogs" theme="simple" action="viewPatientLogsByDateTablet" style="height:0;margin:0">
		<s:hidden id="selectedDate" name="selectedDate"></s:hidden>
	</s:form>			    				
    
    <div class="logbook-slider-container">
    	<div class="logbook-slider-navigation">
    		<div class="row">
    			<div class="medium-2 columns">&nbsp;</div>
    			<div class="medium-8 columns padding-10">    			    				    			
	    			<div class="medium-2 columns align-right"><a href="#" data-nav="prev" class="button margin-0 nav-buttons"><span class="glyphicon glyphicon-chevron-left"></span></a></div>
	    			<div class="medium-8 columns align-center">    		
		    			<h4 class="chooseDateText"></h4>
    					<input id="txtDate" class="fieldset__input js__datepicker hide" type="text" placeholder="Select date..." />
		    		</div>
		    		<div class="medium-2 columns align-left"><a href="#" id="btnNext" data-nav="next" class="button margin-0 nav-buttons"><span class="glyphicon glyphicon-chevron-right"></span></a></div>	    			
	    		</div>
	    		<div class="medium-2 columns">&nbsp;</div>
    		</div>
    	</div>
    	<div class="carousel-container">
    		<div id="tableDiv" class="logs-table">
				
				<display:table name="patientLogs" requestURI="" class="table" uid="row" style="width: 100%;border:1px solid #ccc;background:#fff">
					<display:setProperty name="basic.empty.showtable" value="true" />	
					<display:setProperty name="basic.msg.empty_list_row">
						<tr class="empty">
							<td colspan="6">No Logs Found.</td>
						</tr>
					</display:setProperty>
					<display:setProperty name="paging.banner.placement" value="bottom" />
					
					<display:column  title="Log Time" style="width: 20%;text-align:left;white-space: nowrap;"sortable="true">
					
						<s:property value="@com.healthslate.patientapp.util.CommonUtils@gmtToest(#attr.row.logTime)" />
					
					
										</display:column>
					
					<display:column title="Weight" style="text-align: left;width: 10%;">						
						<s:if test="%{#attr.row.miscLog == null}"></s:if>					
						<s:else>
							<s:property value="@com.healthslate.patientapp.util.CommonUtils@getIntegerFromFloatWIthUnit(#attr.row.miscLog.weight,'lbs')" />
						</s:else>						
					</display:column>
					<display:column title="Food" style="white-space: nowrap;">
						<s:if test="%{#attr.row.foodLogSummary == null}"></s:if>
						<s:elseif test="%{#attr.row.foodLogSummary.notes == null}"></s:elseif>
						<s:else>
							<div class="foodNotes">
								<s:property value="%{#attr.row.foodLogSummary.notes}"/>
							</div>
						</s:else>
					</display:column>
					
					<display:column title="Reviewed" style="width: 16%;white-space: nowrap;" sortable="true">
						<s:if test="%{#attr.row.isProcessed == 1}">
							<span class="bold-green-font">Yes</span>
						</s:if>
						<s:else>
							<span class="bold-red-font">No</span>
						</s:else>
					</display:column>
											
					<display:column title="" style="width:10%; text-align:center;">
						<s:if test="%{shouldRenderForTablet}">
							<s:url id="fetchLogDetail" action="viewLogDetailTablet">
									<s:param name="logId" value="%{#attr.row.logId}"/>
									<s:param name="patientId" value="patientId"/>
							</s:url>
						</s:if>
						<s:else>
							<s:url id="fetchLogDetail" action="fetchLogDetail">
									<s:param name="logId" value="%{#attr.row.logId}"/>
									<s:param name="patientId" value="patientId"/>
							</s:url>
						</s:else>
						<s:a href="%{fetchLogDetail}">
							<button class="btn btn-success btn-success-2">Log Details</button>
						</s:a>
					</display:column>
				</display:table>				
			</div>
    	</div>
    </div>
    
	<script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
 	<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/DT_bootstrap.js"></script>		
	<script src="${pageContext.request.contextPath}/resources/js/date.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/bigdatepicker/picker.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/bigdatepicker/picker.date.js"></script>	
	<script src="${pageContext.request.contextPath}/resources/js/jquery.logbook.js"></script>
	
	<script type="text/javascript">
		$(function(){
			
			var logbook = Object.create(LogBook);
			logbook.init();
			
			var dateSelected = "${selectedDate}";
			if(!dateSelected){
				dateSelected = new Date().toString();				
				$("#selectedDate").val(dateSelected);				
			}
			
			logbook.setDate(logbook.formatDate(dateSelected, logbook.dateFormatDetailed));
			logbook.disableEnableButtons(dateSelected);				
			logbook.picker.set('select', new Date(dateSelected));
			
			Android.setTopHeading("Logbook");
			Android.showBackNav(false);

		}); 
	</script>
</body>
</html>