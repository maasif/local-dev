<head>      
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>AADE Assessment Form</title>    
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jtf_game/css/foundation.min.css" />
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jtf_game/css/semantic.min.css" />      
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->
  
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/jquery.signaturepad.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/assessment.css" />
   
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->
	
  </head>
  
  <body>   

	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>			
			<p id="progressTxt" class="progress-text">We're saving data</p>
		</div>
	</div>
	
		<div class="page-container">		
		  <div class="breadcrumb-top">
	      	<ul class="breadcrumbs">
			  <li><a href="patientsListing.action">My Patients</a></li>	
			  <li><a href="goals.action">Goals</a></li>		  
			  <li class="current"><a href="#">AADE Assessment</a></li>
			</ul>
	      </div>
     
          <div class="bg-white shadow">	  	           	
   			<div id="errMessage" class="alert">______________________</div>      
   			
   			<div class="header-container">
   				<div class="row">
   					<div class="columns medium-4">   						
   						<img src="${pageContext.request.contextPath}/resources/css/images/DEAPLogo.jpg" class="aade-logo" />   						
   					</div>
   					<div class="columns medium-4 four align-left"> 
   						<div class="aade7-top-heading align-center">
   							<h2>AADE 7</h2>
   							<h4>Smart Goal Sheet</h4>
   							<p>Self-Care Behaivors</p>
   						</div>
   						<label class="inline label-black">Patient Name:
   							<input type="text" id="patientName" name="patientName" />
   						</label>  						
   					</div>
   					<div class="columns medium-4 four">	   					
	                    <p class="address">UTHS <br/>
	                       HELP PROGRAM <br/>
	                       7000 FANNIN, SUITE 1620 <br/>
	                       HOUSTON, TX 77030 <br/>
	                       713-500-3267
	                    </p>            
	                </div>                
   				</div>   				
   			</div>
   			
   			<hr class="orange-hr" />
                        
             <div class="row row-full">
               <div class="navigation-buttons">
                   <a href="#" id="btnSave" class="button large radius right navigation-buttons"> Save </a>                  
                   <a href="#" id="btnClear" class="button large secondary radius right navigation-buttons">Clear</a>
               </div>
             </div> 
      	  </div>
    	</div>
	  <!--[if lt IE 9]>
	    	<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
		<![endif]-->
		<!--[if (gte IE 9) | (!IE)]><!-->
		    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>		    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
		<!--<![endif]-->       
	        
	    <!-- Foundation 3 for IE 8 and earlier -->
		<!--[if lt IE 9]>	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
		<![endif]-->
		
		<!-- Foundation 4 for IE 9 and later -->
		<!--[if gt IE 8]><!-->	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
		<!--<![endif]-->	
	   
	   <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.signaturepad.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>             
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/forms.common.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/ada.assessment.js"></script> 
       
      <script type="text/javascript">

        var adaAssessment = null;

        $(function(){
            $(document).foundation();            
            $("#myPatientsNavLiId").addClass("active");
            adaAssessment = Object.create(AdaAssessment).init(); //AdaAssessment is a entity/class        	
        });

      </script>
  </body>          
</html>