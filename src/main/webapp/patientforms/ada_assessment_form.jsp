<head>      
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>ADA Assessment Form</title>    
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jtf_game/css/foundation.min.css" />
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jtf_game/css/semantic.min.css" />      
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->
  
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/jquery.signaturepad.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/assessment.css" />
   
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->
	
  </head>
  
  <body>   

	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>			
			<p id="progressTxt" class="progress-text">We're saving data</p>
		</div>
	</div>
	
		<div class="page-container">		
		  <div class="breadcrumb-top">
	      	<ul class="breadcrumbs">
			  <li><a href="patientsListing.action">My Patients</a></li>	
			  <li><a href="goals.action">Goals</a></li>		  
			  <li class="current"><a href="#">ADA Assessment</a></li>
			</ul>
	      </div>
     
          <div class="bg-white shadow">	  	           	
   			<div id="errMessage" class="alert">______________________</div>      
   			
   			
   			<div class="header-container">
   				<div class="row">
   					<div class="columns medium-6 align-left">
   						<h3>ADA</h3>
   						<p>Participant Follow-up Visit:</p>
   					</div>
   					<div class="columns medium-6 six top-buttons">	   						                                     
	                    <a href="#" id="btnTopClear" class="button large secondary radius right navigation-buttons">Clear</a>
	                    <a href="#" id="btnTopSave" class="button large radius right navigation-buttons"> Save </a>		                
	                </div>                
   				</div>   				
   			</div>
   					
   			<div class="row row-full">
   				<div class="medium-4 four columns">
   					<label class="inline label-black margin-bottom-0">Participant Name:
   						<input type="text" id="participantName" name="participantName" />
   					</label>
   				</div>
   				<div class="medium-4 four columns">
   					<label class="inline label-black margin-bottom-0">Referring Provider:
   						<input type="text" id="referringProvider" name="referringProvider" />
   					</label>
   				</div>
   				<div class="medium-4 four columns">
   					<label class="inline label-black margin-bottom-0">Date:
   						<input type="text" id="referringDate" name="referringDate" />
   					</label>
   				</div>
   			</div> 
   			
   			<div class="row row-full ratings-group">
   				<div class="columns medium-12 twelve heading">
   					<h5>Reassessment:</h5>
   				</div>
   				<div class="columns medium-1 one">
   					<label class="inline">Ratings:</label>
   				</div>
   				<div class="columns medium-11 eleven">
   					<ul class="button-group">
   						<li><label class="inline">1 = needs instruction <span class="font-bold">|</span></label></li>
   						<li><label class="inline">&nbsp;2 = needs review <span class="font-bold">|</span> </label></li>
   						<li><label class="inline">&nbsp;3 = comprehends key points <span class="font-bold">|</span> </label></li>
   						<li><label class="inline">&nbsp;4 = demonstrates competency <span class="font-bold">|</span> </label></li>
   						<li><label class="inline">&nbsp;N/A = not assessed</label></li>   						
   					</ul>
   				</div>
   			
   				<div class="reassessmentContainer">
   					<div class="columns medium-12 twevle">
	   					<ul class="item-grid" id="assessmentGrid">
							<li class="sort bg-lightgrey header">
								<div class="row">
									<div class="medium-4 four columns light-grey-text">Topic</div>
									<div class="medium-4 four columns light-grey-text">Reassessment Rating</div>
									<div class="medium-4 four columns light-grey-text">Comment/Re-education</div>																													
								</div>
							</li>									
						</ul>
					</div>
   				</div>
   			</div>
   			
   			<div class="row row-full ratings-group margin-top-15">
   				<div class="columns medium-12 twelve heading">
   					<h5>Goal Achievement:</h5>
   				</div>
   				<div class="columns medium-1 one">
   					<label class="inline">Ratings:</label>
   				</div>
   				<div class="columns medium-11 eleven">
   					<ul class="button-group">
   						<li><label class="inline">0% = never <span class="font-bold">|</span></label></li>
   						<li><label class="inline">&nbsp;25% = ocassionally <span class="font-bold">|</span> </label></li>
   						<li><label class="inline">&nbsp;50% = half the time <span class="font-bold">|</span> </label></li>
   						<li><label class="inline">&nbsp;75% = most of the time <span class="font-bold">|</span> </label></li>
   						<li><label class="inline">&nbsp;100% = always</label></li>   						
   					</ul>
   				</div>   			
   				<div class="goalAchievementContainer">
   					<div class="columns medium-12 twelve">
	   					<ul class="item-grid" id="goalAchievementGrid">
							<li class="sort bg-lightgrey header">
								<div class="row">
									<div class="medium-4 four columns light-grey-text">Goal</div>
									<div class="medium-4 four columns light-grey-text">Achievement Rating</div>
									<div class="medium-4 four columns light-grey-text">Comment</div>																													
								</div>
							</li>									
						</ul>
					</div>
   				</div>
   			</div>
   			  
   			 <div class="row row-full">
   			 	<div class="new-goals">
   			 		<div class="columns medium-12 twelve">
	   			 		<label class="inline label-black">
	   			 			New Goals:
	   			 			<textarea id="newGoals" name="newGoals" rows="2" cols=""></textarea>
	   			 		</label>
	   			 		<label class="inline label-black">
	   			 			Follow-up/DSMS Plan:
	   			 			<textarea id="dsmsPlan" name="dsmsPlan" rows="2" cols=""></textarea>
	   			 		</label>
	   			 	</div>
   			 	</div>
   			 	 
   			 </div>
   			 
   			 <div class="row row-full">   			 	
                <div class="medium-12 twelve twelve columns">    
                	       
                    <div id="divClinicianSignature" class="sigPad margin-left-0">
                      <ul class="sigNav">                                      
                      	<li class="left"><p class="inline label-black">Clinician Signature</p></li>      
                        <li class="clearButton"><a href="#clear">Clear</a></li>
                      </ul>
                      <div class="sig sigWrapper">                        
                        <canvas class="pad" width="409" height="73"></canvas>
                        <input type="hidden" name="clinicianSignature" id="clinicianSignature" class="output" />
                      </div>                      
                    </div>
                </div>                           
              </div> 
              <br/>
              
              <div class="row row-full">
                <div class="navigation-buttons">                                    
                    <a href="#" id="btnClear" class="button large secondary radius right navigation-buttons">Clear</a>
                    <a href="#" id="btnSave" class="button large radius right navigation-buttons"> Save </a> 
                </div>
              </div> 
      	  </div>
    	</div>
	  <!--[if lt IE 9]>
	    	<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
		<![endif]-->
		<!--[if (gte IE 9) | (!IE)]><!-->
		    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>		    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
		<!--<![endif]-->       
	        
	    <!-- Foundation 3 for IE 8 and earlier -->
		<!--[if lt IE 9]>	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
		<![endif]-->
		
		<!-- Foundation 4 for IE 9 and later -->
		<!--[if gt IE 8]><!-->	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
		<!--<![endif]-->	
	   
	   <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.signaturepad.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>             
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/forms.common.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/ada.assessment.js"></script> 
       
      <script type="text/javascript">

        var adaAssessment = null;

        $(function(){
            $(document).foundation();            
            $("#myPatientsNavLiId").addClass("active");
            adaAssessment = Object.create(AdaAssessment).init(); //AdaAssessment is a entity/class        	
        });

      </script>
  </body>          
</html>