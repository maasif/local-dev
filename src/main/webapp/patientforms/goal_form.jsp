<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Goal Form</title>    
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/semantic.min.css" />

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/timepicker/jquery.timepicker.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/timepicker/bootstrap-datepicker.css" />
 	 	
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/goal.css?v=0.1" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/goals_habits.css" />

   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->
	
  </head>
  
  <body>   
	
	<div id="confirmationImportantGoal" class="reveal-modal small confirm-dialog" data-reveal>
   		<h3>Important</h3>
   		<div class="dialog-area">
    		<p>Try to choose  goal that's important to you. Do you want to start again?</p>
   		</div>
		<a id="closeConfirmDialog" class="close-reveal-modal">&#215;</a>
		<div class="align-right">
			<a href="createGoal.action?formType=GOAL" class="button radius margin-top-10 margin-bottom-0 button-green">Yes</a>
            <button type="button" class="button secondary radius margin-top-10 margin-bottom-0" onclick="$('#closeConfirmDialog').click(); return false;">No</button>            
		</div>
	</div>
	
	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>
			<p id="progressTxt" class="progress-text">We're saving data</p>
		</div>
	</div>
	
	<div class="page-container goal-create-container overlow-horizontal-remove shadow">
		 
	  <div class="breadcrumb-top hide">
      	<ul class="breadcrumbs">
		  <li id="myPatientBreadCrumbLink"><a href="members.action">My Patients</a></li>
		  <li id="goalsBreadCrumbLink"><a href="goalsHabits.action">Goals and Habits</a></li>		  
		  <li class="current"><a href="#">Create a Goal</a></li>
		</ul>
      </div>
           
      <div class="bg-white shadow">	  	        
   	
   		<div id="errMessage" class="alert-box alert alert-message-abs full-width-message">______________________</div>
   		
   		<form id="goalForm" name="goalForm" method="POST">
   		
   		   <input type="hidden" id="txtImportant" name="important" />
           <input type="hidden" id="txtConfident" name="confident" />
           <input type="hidden" id="txtReminder" name="REMINDER" />
           <input type="hidden" id="txtPatientId" name="patientId" />
           <input type="hidden" id="txtAuthorization" name="authorizationCreds" />

           <!-- <input type="hidden" id="txtGoalCategory" name="goalCategory" /> -->
              
          <div class="row border-bt-heading">          	
          	  <div class="small-12 columns padding-left-0">
            	<h4 id="pageHeadingText" class="font-24 font-bold"></h4>
              </div>                            
          </div>
          
          <!-- ADD GOAL -->          
          <div id="page1" class="pages padding-top-10">
              <div class="row">
                <div class="small-12 columns">
                	<label class="inline margin-bottom-0 grey">
   						TYPE OF GOAL
   						<select id="ddGoalCategories" name="goalCategory" class="large-dropdown font-24 margin-bottom-0"></select>
   						<img id="catImage" class="cat-dd-img" style="display:none" src="${pageContext.request.contextPath}/resources/css/images/goal_icons/eating.png" />   						
   					</label>
   					<a href="#" class="button radius button-black small margin-bottom-0" style="display:none;">EXPLORE GOALS</a>
                	<div class="dotted-line"></div>    					
                </div>                                  
              </div>    

              <div class="row">
                <div class="small-12 columns">
                	<label class="inline margin-bottom-0 grey">
   						MY GOAL IS TO
   						<select id="ddMyGoals" name="goalOption" class="large-dropdown font-24 margin-bottom-0"></select>  						
   					</label>   	
   					<label id="sectionOtherGoals" class="inline margin-bottom-0 grey" style="display:none">
                        MY OWN GOAL
   						<input type="text" id="txtOtherMyGoals" name="goalOthers" class="font-18 margin-bottom-0" />  						
   					</label>
   					<div class="dotted-line"></div>				
                </div>                   
              </div>  

              <div class="row">
                <div class="small-12 columns">                
                	<div class="ratings-container rating-goal-progress white-bg border-bottom-none">
		   				How important is this to me?
			    		<ul id="importantGoal" class="large-block-grid-5 small-block-grid-5 scales create-scales goal-progress-rating">
			    			<li class="position-relative" data-scale="1"><label class="inline align-center label-align-left scale-text not-at-all">Not at all</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>1</span></label></li>
		    				<li class="position-relative" data-scale="2"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>2</span></label></li>
		    				<li class="position-relative" data-scale="3"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>3</span></label></li>
		    				<li class="position-relative" data-scale="4"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>4</span></label></li>
		    				<li class="position-relative" data-scale="5"><label class="inline align-center label-align-left scale-text reaching-all">Very important</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>5</span></label></li>
		   				</ul>  		
		    		</div>
    		    </div>                                          
              </div>  
                                    
            </div>
		  
		  <!-- Reaching my goal -->
		  <div id="page2" class="pages" style="display:none;">
				<div class="row">
					<div class="columns small-12">
		               	<label class="inline font-18">
		               		Now take a minute and think about how great it will feel when you reach that goal. 		               			
		               	</label>		               	
               			<ul class="bullet-list font-18">
               				<li>Where will you be?</li>
               				<li>Who will you be with?</li>
               				<li>What will you be doing? </li>
               			</ul>               			
	               </div>
	            </div>
		  </div>
		  
		  <!-- My Action Plan -->
          <div id="page3" class="pages" style="display:none;">  
          
          	<div class="row">
   				<div class="columns small-12">
   					<label class="inline margin-bottom-0">
	  					TO REACH MY GOAL I WILL
	  					<select id="ddActionPlans" name="actionPlan" class="large-dropdown margin-bottom-0 font-24"></select>	                    
  					</label> 
  					<label id="sectionOtherActionPlans" class="inline margin-bottom-0 grey" style="display:none">
                        MY OWN ACTION PLAN
   						<input type="text" id="txtOtherActionPlans" name="actionPlanOthers" class="font-18 margin-bottom-0" />  						
   					</label>
   				</div>
   			</div>
   			
          	<div class="row">
   				<div class="columns small-5">
   					<label class="inline margin-bottom-0">
   						HOW OFTEN
   						<select id="ddActionPlanFrequency" name="planFrequency" class="large-dropdown margin-bottom-0 font-24">	                    	
	                    	<option value="" selected="selected" disabled>(select)</option>
	                    	<option value="1 time">1 time</option>
	                    	<option value="2 times">2 times</option>
	                    	<option value="3 times">3 times</option>
	                    	<option value="4 times">4 times</option>
	                    	<option value="5 times">5 times</option>
	                    	<option value="6 times">6 times</option>
	                    	<option value="7 times">7 times</option>
	                    </select>	
   					</label>    					
   				</div>
   				<div class="columns small-2">
   					<label class="inline align-center margin-bottom-0">
   						<br/><br/>
   						per    						
   					</label>    					
   				</div>
   				<div class="columns small-5">
   					<label class="inline margin-bottom-0">
   						&nbsp;
   						<select id="ddCalendarFrequency" name="calendarFrequency" class="large-dropdown margin-bottom-0 font-24">
	                    	<option value="" selected="selected" disabled>(select)</option>
	                    	<option value="meal">meal</option>
	                    	<option value="hour">hour</option>
	                    	<option value="day">day</option>
	                    	<option value="week">week</option>
	                    	<option value="month">month</option>
	                    	<option value="year">year</option>
	                    	<option value="in the coming week">in the coming week</option>				                    	                  	
	                    </select>		
   					</label>    					
   				</div>
   			</div>
	    			
          	<div class="row">
                <div class="small-12 columns">                
                	<div class="ratings-container rating-goal-progress white-bg border-bottom-none">
		   				How often do I expect to stick to my Action Plan?
			    		<ul id="actionPlan" class="large-block-grid-5 small-block-grid-5 scales create-scales goal-progress-rating">
			    			<li class="position-relative" data-scale="1"><label class="inline align-center label-align-left scale-text not-at-all">Not often</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>1</span></label></li>
		    				<li class="position-relative" data-scale="2"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>2</span></label></li>
		    				<li class="position-relative" data-scale="3"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>3</span></label></li>
		    				<li class="position-relative" data-scale="4"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>4</span></label></li>
		    				<li class="position-relative" data-scale="5"><label class="inline align-center label-align-left scale-text reaching-all">All the time</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>5</span></label></li>
		   				</ul>  		
		    		</div>
    		    </div>                                          
              </div>             
         </div>
			
 		 <!-- My Challenges -->		  
		 <div id="page4" class="pages" style="display:none;">
			<div class="row">
                <div class="skip-btn-div">
                    <a id="btnSkip" href="#" class="button radius tiny margin-bottom-0 white-bordered-btn">Skip</a>
                </div>
				<div class="columns medium-12">
	               	<label class="inline font-18">
	               		Take a minute to picture a specific Challenge that could keep you from sticking with your Action Plan.<br/><br/>
	               		Describe that Challenge in 2 - 6 words	               		 
	               		<textarea id="txtStickFromActionPlan" rows="6" name="myChallenge" placeholder="Type here..."></textarea>
	               	</label>

               </div>
            </div>
		  </div>
		  
		  <!-- Overcoming My Challenge -->
		  <div id="page5" class="pages" style="display:none;">
			<div class="row">
				<div class="columns medium-12">
	               	<label class="inline font-18 margin-bottom-0">
	               		Finally, picture one thing you can do to overcome that challenge you just described.<br/><br/>
	               		Describe how you will overcome the Challenge in a few words
	               		<textarea id="txtOvercomeChallenge" rows="6" name="overcomeChallenge" placeholder="Type here..."></textarea>
	               	</label>	               	
               </div>
            </div>            
		 </div>
          
          <!-- Rewards & Reminders -->
          <div id="page6" class="pages" style="display:none;">
			<div class="row">
				<div class="columns small-12">	               	
	               	<label class="inline margin-bottom-0 grey">
	               		MY REWARD FOR REACHING MY GOAL
	               		<textarea id="txtMyReward" rows="4" name="myReward" class="margin-bottom-0" placeholder="Type here..."></textarea>
	               	</label>
	               	
	               	<div class="dotted-line"></div>
	               	
	               	<div class="ratings-container reminders white-bg border-bottom-none add-goal-form hide">
		   				DO YOU WANT TO BE REMINDED ABOUT YOUR GOAL?
			    		<ul class="button-group reminder-switch tiny round"> 
		   					<li>
		   						<a href="#" title="No Reminder" class="button tiny margin-bottom-0 button-green" data-text="REMINDER">
		   							<img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/no-reminder.png" width="20" title="No Reminder">
		   						</a>
		   					</li>
		   					<li>
		   						<a href="#" title="Reminder" class="button tiny secondary margin-bottom-0" data-text="NO REMINDER">
		   							<img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/reminder.png" width="19" title="Reminder">
		   						</a>
		   					</li>
		   				</ul> 
		   				<span class="reminder-status">NO REMINDER</span>   		
		    		</div>
	               	
               </div>
            </div>
            
		 </div>
		           
          </form>
          
          <div class="row row-full">
              <div>
              	 <ul class="button-group sticky-btns-two">
   					<li><a id="btnCancel" href="goalsHabitsListing.action?AUTHORIZATION=<s:property value='%{authorizationCreds}'/>" class="button expand secondary margin-bottom-0">CANCEL</a></li>
   					<li style="display:none"><a href="#" id="btnPrev" class="button expand secondary gradient-btns margin-bottom-0">BACK</a></li>
   					<li><a href="#" id="btnNext" class="button expand gradient-btns margin-bottom-0 button-teal"> NEXT </a></li>   					
   					<li style="display:none"><a href="#" id="btnSave" class="button expand gradient-btns button-teal margin-bottom-0"> SAVE </a></li>
   				</ul>
              </div>
          </div>        
      </div>
    </div>

    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/semantic.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
		
       <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/forms.common.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/goal.js?v=0.1"></script>
       
      <script type="text/javascript">

        var goal = null,
        	CATEGORY_COLOR = {
        		"Healthy Eating": {color: "healthy-eating", image : ("${pageContext.request.contextPath}/resources/css/images/goal_icons/eating.png")},
        		"Physical Activity": {color: "physical-activity", image : ("${pageContext.request.contextPath}/resources/css/images/goal_icons/physical-activity.png")},
        		"Medications & Health Mgmt": {color: "medication-monitoring", image : ("${pageContext.request.contextPath}/resources/css/images/goal_icons/medication-monitoring.png")},
        		"Money, Work and Travel": {color: "work-money", image : ("${pageContext.request.contextPath}/resources/css/images/goal_icons/work.png")},
        		"Stress and Sleep": {color: "stress-sleep", image : ("${pageContext.request.contextPath}/resources/css/images/goal_icons/stress.png")},
        		"Family & Relationships": {color: "family-relationship", image : ("${pageContext.request.contextPath}/resources/css/images/goal_icons/family.png")} 
            };
        
        $(function(){

            $(document).foundation();            
            $('#patientsTab').addClass('selected-tab');
            goal = Object.create(Goal); //Goal is a entity/class

            var fromServerData = "${jsonData}",  
            	goalDetails = {},
            	goalCategories = $.parseJSON("${jsonString}"),	        	
	        	webURL = "${pageContext.request.contextPath}/";
	        
	        if(fromServerData){
	        	goalDetails = $.parseJSON(fromServerData);
	        }

            $("#txtPatientId").val("${patientId}");
            $("#txtAuthorization").val("${authorizationCreds}");

            goal.init(goalCategories, goalDetails, webURL);
            
            try{
	        	Android.setTitle("ADD GOAL");        		
        	}catch(err){	        		
        	}
        });
		 
      </script>
  </body>          
</html>