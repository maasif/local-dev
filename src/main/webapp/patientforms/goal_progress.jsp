<%@ taglib prefix="s" uri="/struts-tags"%>
<head>      
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Goal Progress</title>    
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/goal.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/goals_habits.css" /> 	

	<script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>

    <style type="text/css">
        .dialog-area{
            max-height: 500px !important;
        }
    </style>
  </head>
  
  <div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>
            <p id="progressTxt" class="progress-text">We're saving data</p>
		</div>
	</div>
	
  <body class="white-bg" ng-app="GoalProgressModule">   
     
	<!-- Confirmation Delete Goal Dialog -->
	<div id="confirmationGoalDialog" class="reveal-modal small confirm-dialog" data-reveal>
   		<h3>Delete Goal</h3>
   		<div class="dialog-area">
    		<p>Are you sure you want to delete this Goal?</p>
   		</div>
		<a id="closeConfirmGoalDialog" class="close-reveal-modal">&#215;</a>
		<div class="align-right">
			<button type="button" id="btnDeleteYesGoal" class="button radius margin-top-10 margin-bottom-0 button-green" onclick="onConfirmDeleteGoal()">Yes</button>
            <button type="button" class="button secondary radius margin-top-10 margin-bottom-0" onclick="$('#closeConfirmGoalDialog').click();">No</button>            
		</div>
	</div>
	
   	<div id="errMessage" class="alert-box alert alert-message-abs full-width-message">______________________</div>   	
   	
   	<div class="page-container padding-bottom-60 overlow-horizontal-remove">
   		
   		<div class="goals-container goal-progress-container" ng-controller="GoalProgressController">
   			
   			<div class="ratings-container rating-goal-progress">
   				I'M REACHING MY GOAL
                <a href="javascript:;" id="btnViewPreviousRatings" class="button small tiny btn-teal right radius hide">Goal Ratings</a>
	    		<ul id="goalRatingList" class="large-block-grid-5 small-block-grid-5 scales create-scales goal-progress-rating">
	    			<li class="position-relative" data-scale="1"><label class="inline align-center label-align-left scale-text not-at-all">Not at all</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>1</span></label></li>
    				<li class="position-relative" data-scale="2"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>2</span></label></li>
    				<li class="position-relative" data-scale="3"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>3</span></label></li>
    				<li class="position-relative" data-scale="4"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>4</span></label></li>
    				<li class="position-relative" data-scale="5"><label class="inline align-center label-align-left scale-text reaching-all">All the time</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>5</span></label></li>
   				</ul>  		
    		</div>
    		
    		<form id="goalForm" name="goalForm" novalidate method="POST" ng-submit="submitForm(goal)">

		   	<input type="hidden" name="patientId" id="txtPatientId" />
   			<input type="hidden" name="filledFormId" ng-model="goal.filledFormId" />
   			<input type="hidden" name="formType" ng-model="goal.formType" value="GOAL" />
   	
   			<input type="hidden" name="goalCategory" ng-model="goal.goalCategory" />
   			<input type="hidden" name="meals" ng-model="goal.meals" />
   			<input type="hidden" name="goalOption" ng-model="goal.goalOption" />
   			<input type="hidden" name="confident" ng-model="goal.confident" />
   			<input type="hidden" name="important" ng-model="goal.important" />
            <input type="hidden" id="txtAuthorization" name="authorizationCreds" />

   			<input id="txtReminder" type="hidden" name="REMINDER" ng-model="goal.reminder" />
   			<input id="txtGoalRating" type="hidden" name="goalRating" ng-model="goal.goalRating" />
   			
	    		<div class="goals-item white-bg goal-progress"> 
	    			<div id="divCategoryOuter" class="category-outer"> 
	    				<div id="divCategoryName" class="category-name"></div>
	    			</div>
	    			
	    			<div class="goal-details">    					
	   					<div class="row">
	   						<div class="columns small-2 align-left">
	   							<label class="inline margin-bottom-0">
	   								<img id="imgCategory" class="category-image" src="" />
	   							</label>
	   						</div>
	   						
	   						<div class="columns small-10">    							
	   							<div class="grey-black-pair">
		   							<label id="lblGoalName" class="inline margin-bottom-0 goal-name"></label>
	   								<label id="lblCreatedDate" class="inline margin-bottom-0 created-date grey grey-forced"></label>
	   							</div>
	   						</div>
	   					</div>
	    			</div>
	    			
	    		</div>
	   			<div class="goal-progress-controls">
	   				<div class="row">
	    				<div class="columns small-12">
	    					<label class="inline margin-bottom-0 grey">
	    						ACTION PLAN
	    						<select id="ddActionPlans" name="actionPlan" ng-model="goal.actionPlan" required class="margin-bottom-0 font-18 large-dropdown" ng-change="actionPlanOnChange();">
	    							
	    						</select>
	    						<small class="error" ng-show="submitted && goalForm.actionPlan.$error.required">Required.</small>
	    					</label> 
	    					
	    					<label id="lblOther" class="inline margin-bottom-0 grey" ng-show="goal.actionPlanOthers">
	    						MY OWN ACTION PLAN
	    						<input type="text" id="txtOther" name="actionPlanOthers" ng-model="goal.actionPlanOthers" class="margin-bottom-0" />
	    						<small class="error" ng-show="submitted && goalForm.actionPlanOthers.$error.required">Required.</small>
	    					</label>
	    					   					
	    				</div>
	    			</div>
	    			
	    			<div class="row">
	    				<div class="columns small-5">
	    					<label class="inline margin-bottom-0">
	    						HOW OFTEN
	    						<select id="ddHowOften" name="planFrequency" ng-model="goal.planFrequency" required class="large-dropdown margin-bottom-0 font-18">
	    							<option value="" selected="selected" disabled>(select)</option>			                    	
			                    	<option value="1 time">1 time</option>
			                    	<option value="2 times">2 times</option>
			                    	<option value="3 times">3 times</option>
			                    	<option value="4 times">4 times</option>
			                    	<option value="5 times">5 times</option>
			                    	<option value="6 times">6 times</option>
			                    	<option value="7 times">7 times</option>			                    	
			                    </select>	
			                    <small class="error" ng-show="submitted && goalForm.planFrequency.$error.required">Required.</small>	                    
	    					</label>    					
	    				</div>
	    				<div class="columns small-2">
	    					<label class="inline align-center margin-bottom-0">
	    						<br/><br/>
	    						per    						
	    					</label>    					
	    				</div>
	    				<div class="columns small-5">
	    					<label class="inline margin-bottom-0">
	    						&nbsp;
	    						<select id="ddDaysWeeks" name="calendarFrequency" ng-model="goal.calendarFrequency" required class="large-dropdown margin-bottom-0 font-18">
	    							<option value="" selected="selected" disabled>(select)</option>
			                    	<option value="meal">meal</option>
			                    	<option value="hour">hour</option>
			                    	<option value="day">day</option>
			                    	<option value="week">week</option>
			                    	<option value="month">month</option>
			                    	<option value="year">year</option>
			                    	<option value="in the coming week">in the coming week</option>			                    	                  	
			                    </select>		
			                    <small class="error" ng-show="submitted && goalForm.calendarFrequency.$error.required">Required.</small>                    
	    					</label>    					
	    				</div>
	    			</div>
	    			
	    			<div class="row">
	    				<div class="columns small-12">
	    					<label class="inline margin-bottom-0 padding-bottom-0">
	    						MY CHALLENGES
	    						<textarea id="txtMyChallenges" name="myChallenge" ng-model="goal.myChallenge" rows="1" cols="" class="margin-bottom-0"></textarea>
	    					</label>    					
	    				</div>
	    			</div>
	    			
	    			<div class="row">
	    				<div class="columns small-12">
	    					<label class="inline margin-bottom-0 padding-bottom-0">
	    						OVERCOMING YOUR CHALLENGES
	    						<textarea id="txtOvercomingChallenges" name="overcomeChallenge" ng-model="goal.overcomeChallenge" rows="1" cols="" class="margin-bottom-0"></textarea>
	    					</label>    					
	    				</div>
	    			</div>
	    		</div>
	    		
	    		<div class="dotted-line"></div>
	    		
	    		<div class="goal-progress-controls">
		   			<div class="row">
		   				<div class="columns small-12">
		   					<label class="inline margin-bottom-0 padding-bottom-0">
		   						MY REWARD
		   						<textarea id="txtMyReward" name="myReward" ng-model="goal.myReward" rows="1" cols="" class="margin-bottom-0"></textarea>
		   					</label>    					
		   				</div>
		   			</div> 
		   			
		   			<div class="row hide">
		   				<div class="columns small-12">
		   					<div class="reminders reminder-progress"> 
				   				<ul class="button-group reminder-switch tiny round"> 
				   					<li>
				   						<a href="#" title="No Reminder" class="button tiny margin-bottom-0 button-green" data-text="REMINDER">
				   							<img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/no-reminder.png" title="No Reminder">
				   						</a>
				   					</li>
				   					<li>
				   						<a href="#" title="Reminder" class="button tiny secondary margin-bottom-0" data-text="NO REMINDER">
				   							<img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/reminder.png" title="Reminder">
				   						</a>
				   					</li>
				   				</ul> 
				   				<span class="reminder-status">NO REMINDER</span> 
				   			</div>
				   		</div>
				   	</div>
				   	   			   			
		   		</div>
	   			
	   			<div class="sticky-nav-buttons">
		   			<div class="row">
		   				<div class="columns small-12 padding-lr-0">
			   				<ul class="button-group sticky-btns-three radius">
			   					<li><a href="goalsHabitsListing.action?AUTHORIZATION=<s:property value='%{authorizationCreds}'/>" class="button expand secondary margin-bottom-0">CANCEL</a></li>
			   					<li><a href="#" id="btnDeleteGoal" class="button expand secondary margin-bottom-0">DELETE</a></li>
			   					<li><input type="submit" id="btnSubmit" ng-click="submitted=true" class="button expand button-teal" value="SAVE" /></li>
			   				</ul>
			   			</div>
		   			</div>
	   			</div>
   			</form>
   			
   		</div>
   		
   	</div>

    <!-- Goals Rating Dialog -->
    <div id="goalRatingDialog" class="reveal-modal medium" data-reveal>
        <div class="columns small-12">
            <h3 id="h3GoalName" class="meal-summary-text">Goal Ratings</h3>
            <div class="dialog-area">
                <ul id="dialogGoalRatingList" class="side-nav">

                </ul>
            </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
    </div>

    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/semantic.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
    <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
    <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/goal.progress.js?v=0.2"></script>
    
    	<script type="text/javascript">
				
	      	var ACTION_PLANS = ('${actionPlanString}') ? $.parseJSON('${actionPlanString}') : undefined,
	      	 	GOAL_PROGRESS = ('${jsonString}') ? $.parseJSON('${jsonString}') : undefined,
                GOAL_RATINGS =  ('${goalRatings}') ? $.parseJSON('${goalRatings}') : undefined;
	      			
	        $(function(){

	        	$('#patientsTab').addClass('selected-tab');	        	
	        	$(document).foundation();

                $("#txtPatientId").val("${patientId}");
                $("#txtAuthorization").val("${authorizationCreds}");

	        	GoalProgress.init(GOAL_PROGRESS, ACTION_PLANS , "${pageContext.request.contextPath}/");
	        	
	        	try{
		        	Android.setTitle("GOAL PROGRESS");  		        	
	        	}catch(err){	        		
	        	}
	        });
			
    	</script>	                   
  </body>          