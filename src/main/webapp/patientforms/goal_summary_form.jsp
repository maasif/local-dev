<head>      
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Goal Summary Form</title>    
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->
  
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/goal.css" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->
	
  </head>
  
  <body>   

	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>			
			<p id="progressTxt" class="progress-text">We're saving data</p>
		</div>
	</div>
	
	<input type="hidden" name="filledFormId" />
	<input type="hidden" id="txtGoalStatus" name="goalStatus" />
	
	<div class="page-container">
		 
	  <div class="breadcrumb-top">
      	<ul class="breadcrumbs">
		  <li id="myPatientBreadCrumbLink"><a href="members.action">My Patients</a></li>
		  <li id="goalsBreadCrumbLink"><a href="goalsHabits.action">Goals and Habits</a></li>		  
		  <li class="current"><a href="#">Goal Summary</a></li>
		</ul>
      </div>
      
      <div class="bg-white shadow">	  	        
   		     	
   		  <div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>
   		  	  	  
    	  <div class="page-sections goal-summary-container">
    	  		
    	  		<div class="page-header hide-on-touch row row-full">
	               <div class="columns medium-9 padding-left-5">
	                   <h3>Goal Summary</h3>
	               </div>
	               <div class="columns medium-3 align-right">
	                   <a href="#" class="button right btnEditGoalSummary margin-bottom-0 small button-black radius">EDIT</a>
	               </div>
	           </div>
	           		
	           	<!-- Goal Status  -->
    	  		<div class="row row-full page-section page-section-first hide-on-touch">
   	  				<div class="large-3 columns">   	  					
   	  					<div class="show-on-touch-block align-right">
   	  						<a href="#" class="button btnEditGoalSummary margin-bottom-0 top-15 small button-black radius">EDIT</a>	
   	  					</div>
   	  					<label class="inline page-section-header margin-bottom-0">Review Status</label>
   	  				</div>
   	  				<div class="large-9 columns page-section-right-content">
   	  					<div class="row page-section-row last-row">
   	  						<div class="columns large-6">
   	  							<label class="inline label-bold margin-bottom-0">Coach Goal Review</label>
   	  						</div>
   	  						<div class="columns large-6">
   	  							<select id="ddAchieved" name="goalStatus" class="large-dropdown margin-bottom-0 font-18">
   	  								<option value="" disabled="disabled">-- Select goal status --</option>
   	  								<option value="Achieved">Achieved</option>
   	  								<option value="Not Achieved" selected="selected">Not Achieved</option>
   	  							</select>
   	  						</div>
   	  					</div>   	  					
   	  				</div>
    	  		</div>
    	  		
    	  		<!-- Goal Summary  -->
    	  		<div class="row row-full page-section page-section-first">
   	  				<div class="large-3 columns">   
   	  					<div class="show-on-touch-block align-right">
   	  						<a href="#" class="button btnEditGoalSummary margin-bottom-0 top-15 small button-black radius">EDIT</a>	
   	  					</div>   	  						  				
   	  					<label class="inline page-section-header">Goal Summary</label>
   	  				</div>
   	  				<div class="large-9 columns page-section-right-content">
   	  					<div class="row page-section-row">
   	  						<div class="columns large-6">
   	  							<label class="inline label-bold">Goal Category</label>
   	  						</div>
   	  						<div class="columns large-6">
   	  							<label class="inline" id="goalCategory">--</label>
   	  						</div>
   	  					</div>
   	  					<div class="row page-section-row">
   	  						<div class="columns large-6">
   	  							<label class="inline label-bold">My Goal is</label>
   	  						</div>
   	  						<div class="columns large-6">
   	  							<label class="inline" id="goalOption">--</label>
   	  						</div>
   	  					</div>
   	  					<div class="row page-section-row last-row">
   	  						<div class="columns large-6">
   	  							<label class="inline label-bold">How important is this goal to me?</label>
   	  						</div>
   	  						<div class="columns large-6">
   	  							<ul id="important" class="button-group scales scale-readonly"></ul>   	  							
   	  						</div>
   	  					</div>
   	  				</div>
    	  		</div>
    	  		
    	  		<!-- Action Plans  -->
    	  		<div class="row row-full page-section">
   	  				<div class="large-3 columns">
   	  					<label class="inline page-section-header">Action Plans</label>
   	  				</div>
   	  				<div class="large-9 columns page-section-right-content">
   	  					<div class="row page-section-row">
   	  						<div class="columns large-6">
   	  							<label class="inline label-bold">To achieve this goal am going to</label>
   	  						</div>
   	  						<div class="columns large-6">
   	  							<label class="inline" id="actionPlan">--</label>
   	  						</div>
   	  					</div>
   	  					<div class="row page-section-row">
   	  						<div class="columns large-6">
   	  							<label id="planFrequency" class="inline">How often?</label>
   	  						</div>
   	  						<div class="columns large-6">
   	  							<label class="inline"></label>
   	  						</div>
   	  					</div>   	  					   	  					
   	  					<div class="row page-section-row last-row">
   	  						<div class="columns large-6">
   	  							<label class="inline label-bold">How confident am I that I can keep to my action plan?</label>
   	  						</div>
   	  						<div class="columns large-6">
   	  							<ul id="confident" class="button-group scales scale-readonly"></ul>
   	  						</div>
   	  					</div>  					
   	  				</div>
    	  		</div>
    	  		
    	  		<!-- Your Challenges  -->
    	  		<div class="row row-full page-section">
   	  				<div class="large-3 columns page-section-right-content">
   	  					<label class="inline page-section-header">Your Challenges</label>
   	  				</div>
   	  				<div class="large-9 columns page-section-right-content">
   	  					<div class="row page-section-row last-row">
   	  						<div class="columns large-12">
   	  							<label class="inline" id="stickFromActionPlan">--</label>   	  							
   	  						</div>
   	  					</div>   	  									
   	  				</div>
    	  		</div>
    	  		
    	  		<!-- Overcoming Your Challenges  -->
    	  		<div class="row row-full page-section">
   	  				<div class="large-3 columns">
   	  					<label class="inline page-section-header margin-bottom-0">Overcoming Your Challenges</label>
   	  				</div>
   	  				<div class="large-9 columns page-section-right-content">
   	  					<div class="row page-section-row last-row">
   	  						<div class="columns large-12">
   	  							<label class="inline" id="overcomeChallenge">--</label>
   	  						</div>
   	  					</div>   	  									
   	  				</div>
    	  		</div>
    	  		
    	  </div>
    	        
      </div>
    </div>
	  <!--[if lt IE 9]>
	    	<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
		<![endif]-->
		<!--[if (gte IE 9) | (!IE)]><!-->
		    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>				       
		    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
		<!--<![endif]-->       
	        
	    <!-- Foundation 3 for IE 8 and earlier -->
		<!--[if lt IE 9]>	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
		    <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
		<![endif]-->
		
		<!-- Foundation 4 for IE 9 and later -->
		<!--[if gt IE 8]><!-->	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
		<!--<![endif]-->	
	   	   	  
       <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>             
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
       
      <script type="text/javascript">
		
      	var howOftenKeys = {
      		"planFrequency": " How often ",
      		"calendarFrequency": " time(s) per ",
      		"meals": " when ",
      	},
      	formId = "${filledFormId}";
      	
        $(function(){
            
        	$(document).foundation();   
           
            $('#patientsTab').addClass('selected-tab');
            
            //edit button onclick
            $(".btnEditGoalSummary").off("click").on("click", function(e){
            	e.preventDefault();
            	window.location = "showGoalForm.action?formType=GOAL&filledFormId="+formId;
            });
            
            //loads data into page
            loadSummary("${jsonData}");
            
            //on change of dropdown save status
            $("#ddAchieved").on("change", function(){
            	var status = $(this).val();
            	if(status){
            		
            		var ajaxing = Object.create(Ajaxing),	            		
	            		$error = $("#errMessage"),
                	    dataToSend = { "filledFormId": formId, "goalStatus": status },
                		postRequestActions = {    			
                			"loading": $("#loading"),
                			"error": $error,
                			"successCallBack": onSaveStatus
                		};    	
                	        	
                	ajaxing.sendPostRequest("saveGoalStatus.action", dataToSend, postRequestActions);
            	}
            }).val("${goalStatus}");
        });

        //response callback from server after ajax call
        function onSaveStatus(data){
        	
        	var alertMessage = Object.create(AlertMessage).init(),
        		$message = $("#errMessage");
        	
        	if(data.STATUS == "SUCCESS"){
        		alertMessage.showAlertMessage($message, "Status saved successfully.", alertMessage.SUCCESS); 		
        	}else{
        		var ajaxing = Object.create(Ajaxing);
        		ajaxing.showErrorOrRedirect(data, $message, "Unable to save status. Please try again later.");
        	}
        }
        
        //load in controls by iterating over jsonObject
        function loadSummary(jsonString){
			
        	try{        		
        		var jsonList = $.parseJSON(jsonString),
        			howOften = [],
        			frequencyString = "";   
        		
        		//iterates over jsonObject
        		$.each(jsonList, function(index, obj){
        			var keyName = obj.name;
        			//filter these two and render checkboxes and select upto the given value
        			if(keyName == "important" || keyName == "confident"){
        				renderScales(keyName, obj.value);
        			//for following, store in an array and display in one string	
        			}else if(keyName == "calendarFrequency" || keyName == "planFrequency" || keyName == "meals"){
        				howOften.push({ "key" : howOftenKeys[keyName], "value": obj.value});
        			}else{
        				$("#"+keyName).text(obj.value);
        			}        			
        		});  
        		     		
        		//displays frequency in one string
        		$.each(howOften.sort(compareStrings), function(index, o){
        			frequencyString += "<b>"+ o.key + " </b>" + o.value;
        		});        		
        		$("#planFrequency").html(frequencyString);
        	}catch(err){}            
        }
        
        //render checkboxes and select upto the given value
        function renderScales(className, valUpto){
           var $ul = $("#"+className).empty(),
           	   html = "";

           for (var i = 1; i <= 5; i++) {        	  
         	  var unchecked = "unchecked.png";        	  
         	  if(i <= valUpto){
         		  unchecked = "checked.png";
         	  }
               html += "<li><label class='inline'> <img src='${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/"+unchecked+"' class='"+className+"' data-ischecked='false' /><span>"+i+"</span></label></li>";
           }

           return $ul.append(html);
       	}
		
        //compares two strings
        function compareStrings(a, b) {
       	  a = a.key.toLowerCase();
       	  b = b.key.toLowerCase();
		
       	  return (a < b) ? -1 : (a > b) ? 1 : 0;
       	}
        
      </script>
  </body>          
</html>