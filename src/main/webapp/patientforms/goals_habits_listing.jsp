<%@ taglib prefix="s" uri="/struts-tags"%>
<head>      
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Goals and Habits</title>    
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" /> 
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/semantic.min.css" />   
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="resources/css/ie-style.css" />	
	<![endif]-->
  
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/jquery.signaturepad.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/goal.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/goals_habits.css?v=0.1" />
   
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="resources/css/ie-style.css" />
	<![endif]-->
	
  </head>
  
  <div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>						
		</div>
	</div>
	
  <body>

     <input type="hidden" id="txtAuthorization" name="authorizationCreds" />

     <!-- Confirmation Delete Habit Dialog -->
     <div id="confirmationHabitDialog" class="reveal-modal small confirm-dialog" data-reveal>
   		<h3>Delete Habit</h3>
   		<div class="dialog-area">
    		<p>Are you sure you want to delete this Habit?</p>
   		</div>
		<a id="closeConfirmDialog" class="close-reveal-modal">&#215;</a>
		<div class="align-right">
			<button type="button" id="btnDeleteYesHabit" class="button radius margin-top-10 margin-bottom-0 button-green">Yes</button>
            <button type="button" class="button secondary radius margin-top-10 margin-bottom-0" onclick="$('#closeConfirmDialog').click(); return false;">No</button>            
		</div>
	</div>
	
	<!-- Confirmation Delete Goal Dialog -->
	<div id="confirmationGoalDialog" class="reveal-modal small confirm-dialog" data-reveal>
   		<h3>Delete Goal</h3>
   		<div class="dialog-area">
    		<p>Are you sure you want to delete this Goal?</p>
   		</div>
		<a id="closeConfirmGoalDialog" class="close-reveal-modal">&#215;</a>
		<div class="align-right">
			<button type="button" id="btnDeleteYesGoal" class="button radius margin-top-10 margin-bottom-0 button-green" onclick="onConfirmDeleteGoal()">Yes</button>
            <button type="button" class="button secondary radius margin-top-10 margin-bottom-0" onclick="$('#closeConfirmGoalDialog').click(); return false;">No</button>            
		</div>
	</div>
	
	<!-- Goal Status Dialog -->
	<div id="goalRatingDialog" class="reveal-modal small confirm-dialog" data-reveal>   
		<div id="errMessageDialog" class="alert-box alert alert-message-abs alert-message-abs-dialog full-width-message">______________________</div>   			
   		<div class="dialog-area columns small-12">
   			<h3>GOAL STATUS</h3>
    		<p class="margin-bottom-5"><b>Goal: </b> <span id="spGoalName"></span></p>
    		<p class="margin-bottom-5"><b>Action Plan: </b> <span id="spActionPlan"></span></p>
    		<br/>
    		<p>How have you been reaching this goal?</p>
    		
    		<div class="ratings-container">
	    		<ul id="goalRatingList" class="large-block-grid-5 small-block-grid-5 scales create-scales goal-status-dialog-rating">
	    			<li class="position-relative" data-scale="1"><label class="inline align-center label-align-left scale-text not-at-all">Not at all</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>1</span></label></li>
    				<li class="position-relative" data-scale="2"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>2</span></label></li>
    				<li class="position-relative" data-scale="3"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>3</span></label></li>
    				<li class="position-relative" data-scale="4"><label class="inline"><img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>4</span></label></li>
    				<li class="position-relative" data-scale="5"><label class="inline align-center label-align-left scale-text reaching-all">All the time</label><label class="inline"> <img src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/unchecked.png" class="important" data-ischecked="false"><span>5</span></label></li>
   				</ul>  		
    		</div>
    		<button id="btnEditGoalDialog" type="button" class="button radius margin-top-10 expand margin-bottom-0 button-green">EDIT GOAL</button>    		
   		</div>   		
		<a id="closeGoalRatingDialog" class="close-reveal-modal">&#215;</a>
		<button type="button" class="button dialog-back-btn radius button-black margin-top-10 margin-bottom-0" onclick="$('#closeGoalRatingDialog').click();">Back</button>	
	</div>
	
   	<div id="errMessage" class="alert-box alert alert-message-abs full-width-message">______________________</div>   	
   	<input type="hidden" name="patientId" id="txtPatientId" />
   	
   	<div class="page-container">
   		
   		<div class="goals-container">
   			
   			<!-- Heading -->
   			<div class="row row-full">
				<div class="columns large-10 small-8">
					<h3 class="heading-align"> My Goals </h3>
				</div>
				
				<div class="columns large-2 small-4 padding-right-9">
					<a href="createGoal.action?formType=GOAL&patientId=<s:property value='%{patientId}'/>&authorizationCreds=<s:property value='%{authorizationCreds}'/>" class="button radius small expand add-goal-btn margin-bottom-0"> <img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/add.png" /> Add Goal</a>
				</div>
			</div>
			<div class="listing-container">
				<ul id="goalList" class="listing large-block-grid-1">
					
				</ul>				
   			</div>
   		</div>
   	
   		<div class="habits-container">
   			<!-- Heading -->
   			<div class="row row-full">
				<div class="columns large-10 small-8">
					<h3 class="heading-align"> My Habits </h3>
				</div>
				
				<div class="columns large-2 small-4 padding-right-9">
					<a id="dummyAhref" href="createHabit.action?formType=HABIT&patientId=<s:property value='%{patientId}'/>&authorizationCreds=<s:property value='%{authorizationCreds}'/>" class="button radius small expand add-goal-btn margin-bottom-0"><img src="${pageContext.request.contextPath}/resources/css/images/goal_icons/add.png" /> Add Habit</a>
				</div>
			</div>
			<div class="listing-container">
				<ul id="habitList" class="listing large-block-grid-1">
					
				</ul>				
   			</div>
   		</div>
   		
   	</div>
   	
   	
    <!--[if lt IE 9]>
    	<script src="resources/js/foundation4/jquery.js"></script>
	<![endif]-->
	<!--[if (gte IE 9) | (!IE)]><!-->
	    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/js/semantic.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
	<!--<![endif]-->       
        
    <!-- Foundation 3 for IE 8 and earlier -->
	<!--[if lt IE 9]>	    
	    <script src="resources/js/foundation3/foundation.min.js"></script>
	    <script src="resources/js/foundation3/app.js"></script>
	<![endif]-->
	
	<!-- Foundation 4 for IE 9 and later -->
	<!--[if gt IE 8]><!-->	    
	    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
	<!--<![endif]-->	

    <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
    <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/rating/jquery.barrating.min.js"></script>	   	   
    <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>      
    
    <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/goals.habits.js?v-0.1"></script>
      
    	<script type="text/javascript">

	      	var GOALS_HABITS = ("${jsonString}") ? $.parseJSON("${jsonString}") : undefined,
	      		goalsHabitsJS = Object.create(GoalsHabits);
				      
	        $(function(){

	        	$('#patientsTab').addClass('selected-tab');	        	
	        	$(document).foundation();

                $("#txtPatientId").val("${patientId}");
                $("#txtAuthorization").val("${authorizationCreds}");

	        	goalsHabitsJS.init(GOALS_HABITS, "${pageContext.request.contextPath}/");	

	        	try{		        	
		        	Android.setTitle("Goals and Habits");
	        	}catch(err){	        		
	        	}
	        });
			
    	</script>	                   
  </body>          