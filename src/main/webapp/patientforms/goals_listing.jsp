<%@ taglib prefix="s" uri="/struts-tags"%>
<head>      
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Goals and Habits</title>    
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" /> 
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/semantic.min.css" />   
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="resources/css/ie-style.css" />	
	<![endif]-->
  
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/jquery.signaturepad.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/goal.css" /> 	
   
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="resources/css/ie-style.css" />
	<![endif]-->
	
	<script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>
	
  </head>
  
  <div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">			    
			  </div>
			</div>			
			<p id="progressTxt" class="progress-text">We're loading data</p>
		</div>
	</div>
	
  <body ng-app="Habits_Goals_Listing">   
      	  
   	<div id="errMessage" class="alert-box alert">______________________</div>   	
   	
   	<label id="txtModule" class="hide"><s:property value="%{#session.MODULE_TYPE}" /></label>
   	<input type="hidden" id="txtFormType" name="formType" />
   	<div class="inner-content">
   		<div class="row row-full">      
   			<div class="large-3 medium-4 three columns align-right padding-0">
   				<!-- <div id="goalListingBreadcrumb" class="breadcrumb-top breadcrumb-top-align">
	      		<ul class="breadcrumbs">
				  <li><a href="members.action">My Patients</a></li>
				  <li class="current"><a href="#">Goals/Habits</a></li>
				</ul>
    		  </div> -->
    		  
    		  <ul id="goalsHabitsSwitch" class="small-block-grid-2 goal-habits-switch radius">
   					<li class="padding-left-10">
   						<a href="#" data-content="goal" class="button expand button-green margin-bottom-0">Goals</a>
   					</li>
   					<li class="padding-right-10">
   						<a href="#" data-content="habit" class="button expand margin-bottom-0 secondary">Habits</a>
   					</li>
   				</ul>
   			</div>
   			<div class="large-9 medium-8 nine columns align-right padding-0">   				
   				<a href="createGoal.action?formType=GOAL" class="button margin-top-18 button-black radius goal-content">CREATE GOAL</a>
   				<a href="#" id="btnShowAchievedGoals" class="button margin-top-18 button-black success radius goal-content">ACHIEVED GOALS</a>      					
   				<a href="createHabit.action?formType=HABIT" class="button margin-top-18 button-black top-17 radius habit-content">CREATE HABIT</a>
   			</div>      			
		</div>
	
	  <div class="habit-content margin-top-15" ng-controller="HabitsController as habitCtrl">
	  	<div class="row row-full main-page-goal ">		  								
			<div class="large-12 columns border-left-grey padding-lr-0 bg-white">												
				<div id="habitDetails" class="content-manager goal-listview">				    
					<ul class="item-grid" id="habitsGrid">
						<li class="sort bg-lightgrey header hide-on-touch">
							<div class="row">
								<div class="large-3 three columns light-grey-text">Triggering Activity</div>
								<div class="large-3 three columns light-grey-text">Healthier Habit</div>
								<div class="large-3 three columns light-grey-text">Celebrate</div>																													
								<div class="large-3 three columns light-grey-text">Created On</div>
							</div>
						</li>	
						<li class="empty-item" ng-show="(habitCtrl.habitDetailsList === undefined || habitCtrl.habitDetailsList == null || habitCtrl.habitDetailsList == 'null')">
							<div class="row empty-item" align="center">No data found to display</div>
						</li>
						<li class="goal-item" ng-click="habitCtrl.openHabitForm(habit)" data-formid="{{habit.formId}}" ng-repeat="habit in habitCtrl.habitDetailsList | orderBy : '-createdOn'">
							<div class="row">
								<div class="large-3 three columns light-grey-text"><label class="inline margin-bottom-0"><span class="show-on-touch">Triggering Activity: </span>{{habit.triggeringActivity}}</label></div>
								<div class="large-3 three columns light-grey-text"><label class="inline margin-bottom-0"><span class="show-on-touch">Healthier Habit: </span>{{habit.healthierHabit}}</label></div>
								<div class="large-3 three columns light-grey-text"><label class="inline margin-bottom-0"><span class="show-on-touch">Celebrate: </span>{{habit.celebrate}}</label></div>
								<div class="large-3 three columns light-grey-text"><label class="inline margin-bottom-0"><span class="show-on-touch">Created On: </span>{{habit.createdOn | date: 'MM/dd/yyyy'}}</label></div>
							</div>
							<div class="show-on-touch-block align-right margin-top-10">
								<a href="#" class="button radius button-black margin-top-10 margin-bottom-0">Show Details</a>
							</div>
						</li>			
					</ul>
				</div>
			</div>				
		  </div>
	  </div>
	  
      <div class="goal-content" ng-controller="GoalCategoriesController as catCtrl">      	      		
		  <div class="main-page-goal row row-full">						
			<div class="large-3 medium-4 three columns padding-0">
				<div class="show-on-touch">
					<label class="inline font-24 margin-bottom-0">
						Choose Goal Category:
						<select id="ddGoalCategories" ng-model="ddCategories" ng-change="catCtrl.onDropDownChange(this)" class="large-dropdown font-24 margin-bottom-0">
							<option value="{{ct.goalCategoryId}}" ng-repeat="ct in catCtrl.categories">{{ct.name}}</option>
						</select>						
					</label>
				</div>
				<ul class="side-nav hide-on-touch" id="goalCategories">				
					<li class="categories" ng-class="{activeSelected: $index == catCtrl.selected}" ng-click="catCtrl.setSelected($index, cat.goalCategoryId)" data-catid="{{cat.goalCategoryId}}" ng-repeat="cat in catCtrl.categories">	
						<span class="list-image">
							<img ng-src="${pageContext.request.contextPath}/resources/patient-forms-resources/css/images/{{cat.name + '.png'}}" />
						</span>					
						<span class="span-text">{{cat.name}}</span>
					</li>				
				</ul>
			</div>
			<div class="large-9 medium-8 nine columns border-left-grey padding-lr-0 bg-white" id="contentsGrid">												
				<div id="goalDetails" class="content-manager goal-listview">				    
					<ul class="item-grid" id="itemGrid">
						<li class="sort bg-lightgrey header hide-on-touch">
							<div class="row">
								<div class="large-3 three columns light-grey-text">Goal Name</div>
								<div class="large-3 three columns light-grey-text">Created On</div>
								<div class="large-3 three columns light-grey-text">My Goal Review On</div>																													
								<div class="large-3 three columns light-grey-text">My Goal Review</div>
							</div>
						</li>	
						<li class="empty-item" ng-show="(catCtrl.goalDetailsList === undefined || catCtrl.goalDetailsList == null || catCtrl.goalDetailsList == 'null')">
							<div class="row empty-item" align="center">No data found to display</div>
						</li>
						<li class="goal-item" ng-click="catCtrl.openGoalForm(detail)" data-formid="{{detail.formId}}" ng-repeat="detail in catCtrl.goalDetailsList">
							<div class="row">
								<div class="large-3 three columns light-grey-text"><label class="inline margin-bottom-0"><span class="show-on-touch">Goal Name: </span>{{detail.goalName}}</label>
									<span ng-show="detail.goalStatus == 'Achieved'" class="label success achieved-label">Achieved</span>
								</div>
								<div class="large-3 three columns light-grey-text"><label class="inline margin-bottom-0"><span class="show-on-touch">Created On: </span>{{detail.createdOn | date: 'MM/dd/yyyy'}}</label></div>
								<div class="large-3 three columns light-grey-text">
									<label ng-if="detail.ratedOn != '-1'" class="inline margin-bottom-0"><span class="show-on-touch">My Goal Review On: </span>{{detail.ratedOn | date: 'MM/dd/yyyy'}}</label>
								</div>
								<div class="large-3 three columns light-grey-text">									
									<div class="input select rating-c">	
										<span class="show-on-touch">My Goal Review: </span>						            
							            <select name="rating" class="rating">
							            	<option value="">0</option>
							                <option value="1">1</option>
							                <option value="2">2</option>
							                <option value="3">3</option>
							                <option value="4">4</option>
							                <option value="5">5</option>
							            </select>
							        </div>
								</div>
							</div>
							<div class="show-on-touch-block align-right margin-top-10">
								<a href="#" class="button radius button-black margin-top-10 margin-bottom-0">Show Details</a>
							</div>
						</li>			
					</ul>
				</div>
			</div>				
		  </div>
		 </div>		 		                 
	 </div>
		
	  <!--[if lt IE 9]>
	    	<script src="resources/js/foundation4/jquery.js"></script>
		<![endif]-->
		<!--[if (gte IE 9) | (!IE)]><!-->
		    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
		    <script src="${pageContext.request.contextPath}/resources/js/semantic.min.js"></script>
		    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
		<!--<![endif]-->       
	        
	    <!-- Foundation 3 for IE 8 and earlier -->
		<!--[if lt IE 9]>	    
		    <script src="resources/js/foundation3/foundation.min.js"></script>
		    <script src="resources/js/foundation3/app.js"></script>
		<![endif]-->
		
		<!-- Foundation 4 for IE 9 and later -->
		<!--[if gt IE 8]><!-->	    
		    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
		<!--<![endif]-->	

	   <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
	   <script src="${pageContext.request.contextPath}/resources/js/rating/jquery.barrating.min.js"></script>	   	   
       <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>      
       
       <script type="text/javascript">
      	
	      var GOAL_CATEGORIES = $.parseJSON("${jsonString}"),
	  		  GOAL_DETAILS = ("${jsonData}") ? $.parseJSON("${jsonData}") : null, 
	  		  HABIT_DETAILS = ("${habitsJsonData}") ? $.parseJSON("${habitsJsonData}") : null,
	  		  ajaxing = Object.create(Ajaxing);
				      
	        $(function(){ 	        	
	        	$('#patientsTab').addClass('selected-tab');
	        	bindHabitGoalSwitch();
	        	$(document).foundation();	        		        	
	        	loadRating(GOAL_DETAILS);	  
	        	
	        	//onclick of Achieved Goals button
	        	$("#btnShowAchievedGoals").on("click", function(e){
	        		
	        		//gets the scope of a element under GoalCategoriesController and call refresh and load in table via controller
	        		var thisText = $(this).text(),
	        			$scope = angular.element("#ddGoalCategories").scope(),	        		
	        			dataToSend = {"goalCategory": -1},
		        		postRequestActions = {    			
		        			"loading": $("#loading"),
		        			"error": $("#errMessage"),
		        			"successCallBack": $scope.catCtrl.onLoadDetails
		        		};
	    	        	
	        		if(thisText == "ACHIEVED GOALS"){
		            	ajaxing.sendPostRequest("getAchievedGoals.action", dataToSend, postRequestActions);
		            	$(this).text("ALL GOALS");
	        		}else{
	        			var catId = $("#goalCategories li.activeSelected").data("catid");
	        			$scope.catCtrl.getGoalDetailsByCategoryId(catId);
	        			$(this).text("ACHIEVED GOALS");
	        		}
	        		
	        		//for removing hover effect from button
	        		$(this).blur();
	        	});
	        });
				
	        //binding habit/goal switch
	        function bindHabitGoalSwitch(){
	        	var $switch = $("#goalsHabitsSwitch a"),
	        		index = ($("#txtModule").text() == "goal") ? 0 : 1;
	        		        	
	        	$switch.on("click", function(e){
	        		e.preventDefault();
	        		
	        		$switch.removeClass("button-green").addClass("secondary");
	        		
	        		var classContent = $(this).data("content");	        		
	        		$(".goal-content,.habit-content").hide();	        		
	        		$("."+classContent+"-content").show();
	        		
	        		$("#txtFormType").val(classContent);
	        		
	        		$(this).removeClass("secondary").addClass("button-green");
	        		
	        		saveSwitchStateToServer(classContent);
	        	});
	        	
	        	if(index){
	        		$switch.eq(index).click();
	        	}
	        }
	        
	        //send ajax call to server for saving rating
	        function sendRatingToServer(formId, rating){	        
	        	var dataToSend = {"formId": formId, "rating": rating },
        		postRequestActions = {    			
        			"loading": $("#loading"),
        			"error": $("#errMessage"),
        			"successCallBack": onSaveRating
        		};
        	        	
        		ajaxing.sendPostRequest("saveRating.action", dataToSend, postRequestActions);
	        }
	        
	        //once the rating save, trigger already selected link from the blue menu
	        function onSaveRating(result){	        		        	
	        	
	        	var alert = Object.create(AlertMessage).init();
	        	if(result.STATUS == "SUCCESS"){	        		
		        	var $select = $("#ddGoalCategories"),
		        		$blueCats = $("#goalCategories"),
		        		$liItem = $blueCats.find("li.activeSelected");
		        	
		        	//for desktop version
		        	if($blueCats.is(":visible") && $liItem){		        		
		        		$liItem.click();
		        	}		        	
		        	
		        	//for mobile version
		        	if($select.is(":visible")){		        		
		        		var newVal = $select.val(),
		        			scope = angular.element($select).scope();	        		
		        		
		        		scope.$apply();		        		
		        		scope.catCtrl.getGoalDetailsByCategoryId(newVal);		        		
		        	}			        	
	        	}else{
	        		alert.showAlertMessage($("#errMessage"), "Rating already saved.", alert.INFO);
	        	}	        	
	        }
	        
	        //send ajax call to save switch state
	        function saveSwitchStateToServer(habitGoal){	        	        	
        		ajaxing.sendPostRequest("saveSwitchState.action", {"formType": habitGoal});
	        }
	        
	        //set heighlight blue to rating control
	        function setStarHighligting($a, rating){
	        	$a.each(function(index, elem){
					
					var ratingMinus = (rating -1); 
					
					if(index < ratingMinus){
						$(this).addClass("br-selected");
					}
					
					if(index == ratingMinus){
						$(this).addClass("br-selected br-current");
					}
				});
	        }
	        
	        //load rating in rating control
	        function loadRating(goalDetails){
	        	
	        	var $ratingControl = $('.rating').barrating({ 
	    			showSelectedRating: false,	
	    			showValues:true,
	    			onSelect: function(value, text){		    				
	    				sendRatingToServer($(this).closest("li").data("formid"), value);
    	        		return false;
	    			}
	    		});

				$(".br-widget a").attr("href", "javascript:;");

	        	//set rating highlight traversing details list
	        	$.each(goalDetails, function(index, obj){
		    		var $li = $("#itemGrid li[data-formid='"+obj.formId+"']");	
		    		
		    		if($li && obj.rating != -1){		    	
		    			
	    				//now gets the rating control from the list and make it readonly
		    			var $readOnlyCtrl = $ratingControl.eq(index);
		    			
	    				//destroy first and re-bind and set readonly state to it
		    			$readOnlyCtrl.barrating("destroy").barrating({ 
		    				showSelectedRating: false,	   
		    				showValues:true,
		    				readonly:true
		    			});
	    				
	    				//after destroy rating plugin gets the new readonly control a links for highlighting
		    			var $stars = $readOnlyCtrl.next(".br-widget").find("a");		    			
	    				setStarHighligting($stars, obj.rating);	    					    			
		    		}			    	
		    	});
	        }
	    </script>
	       
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/goal.listing.js"></script>             
             
  </body>          