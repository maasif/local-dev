<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Habit Form</title>    
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/goal.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/patient-forms-resources/css/goals_habits.css" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->
	
	<script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>
	
  </head>
  
  <body ng-app="HabitsCreateModule">   

	<div id="loading" class="loader">		
		<div class="image-loader">
			<p class="progress-please-wait">Please wait...</p>
			<p id="progressTxt" class="progress-text">We're saving data</p>
		</div>
	</div>
	
	<div class="page-container habit-create-container">
		 
	  <div class="breadcrumb-top hide">
      	<ul class="breadcrumbs">
		  <li id="myPatientBreadCrumbLink"><a href="members.action">My Patients</a></li>
		  <li id="goalsBreadCrumbLink"><a href="goalsHabits.action">Goals and Habits</a></li>		  
		  <li class="current"><a href="#">Create a Habit</a></li>
		</ul>
      </div>
           
      <div class="bg-white shadow" ng-controller="HabitSaveController">	  	        
   	
   		<div id="errMessage" class="alert-box alert">______________________</div>
   		
   		<form id="habitForm" name="habitForm" novalidate method="POST" ng-submit="submitForm(habit)">

           <input type="hidden" id="txtPatientId" name="patientId" />
           <input type="hidden" id="txtAuthorization" name="authorizationCreds" />

          <div id="page1">
              <div class="row row-full">
                <div class="medium-4 columns">
                    <label class="inline margin-bottom-0 grey habit-create-label">
                    	After I
                    </label>
                </div>
                <input type="hidden" name="filledFormId" ng-model="habit.filledFormId" />  
                <div class="medium-8 columns">
                    <label class="inline margin-bottom-0 padding-bottom-0 grey">
	                    <textarea id="txtTrigger" name="triggerActivity" ng-model="habit.triggerActivity" required rows="4" cols="" class="margin-bottom-0"></textarea>
	                    <small class="error" ng-show="submitted && habitForm.triggerActivity.$error.required">Required.</small>
	                </label>
                </div>                 
              </div>  
              <div class="row row-full">
                <div class="medium-4 columns">
                    <label class="inline margin-bottom-0 grey habit-create-label">
                    	I will then 
                    </label>
                </div>  
                <div class="medium-8 columns">
                	<label class="inline margin-bottom-0 padding-bottom-0 font-24">
	                    <textarea id="txtHealthierHabit" name="healthierHabit" ng-model="habit.healthierHabit" required rows="4" cols="" class="margin-bottom-0"></textarea>
	                    <small class="error" ng-show="submitted && habitForm.healthierHabit.$error.required">Required.</small>
	                 </label>
                </div>                 
              </div> 
              <div class="row row-full">
                <div class="medium-4 columns">
                    <label class="inline margin-bottom-0 grey habit-create-label">
                    	and to Celebrate I will
                    </label>
                </div>  
                <div class="medium-8 columns">
                	<label class="inline margin-bottom-0 padding-bottom-0 font-24">
	                    <textarea id="txtCelebrate" name="celebrate" ng-model="habit.celebrate" required rows="4" cols="" class="margin-bottom-0"></textarea>
	                    <small class="error" ng-show="submitted && habitForm.celebrate.$error.required">Required.</small>
	                </label>
                </div>                
              </div>                                            
           </div>
			   <div class="row row-full">
	              <div class="medium-12 columns align-right margin-top-10">
	              		<ul class="button-group sticky-btns-two radius">
	              		  <li><a href="goalsHabitsListing.action?AUTHORIZATION=<s:property value='%{authorizationCreds}'/>" class="button secondary small expand">CANCEL</a></li>
		                  <li><input type="submit" id="btnSubmit" ng-click="submitted=true" class="button small expand button-teal" value="SAVE" /></li>	                                   
	              		</ul>
	              </div>
	          </div>   
          </form>          
                
      </div>
    </div>

       <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
       <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
               
       <script type="text/javascript">

        var HABIT_FORM_DATA = "${jsonData}";
        	
        $(function(){

            $(document).foundation();            
            $('#patientsTab').addClass('selected-tab');

            $("#txtPatientId").val("${patientId}");
            $("#txtAuthorization").val("${authorizationCreds}");

            $("#txtRemindedTime").timepicker({ 'timeFormat': 'h:i A' }).on("keypress keydown", function(evt){
    			return false;
            });
            
            try{
	        	Android.setTitle("ADD HABIT");        		
        	}catch(err){	        		
        	}
        });
		
       </script>
      
       <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/habit.js"></script>
  </body>          
</html>