<head>    
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Short Assessment Form</title>    
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="resources/css/foundation.min.css" />    
    	<script src="resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="resources/css/ie-style.css" />	
	<![endif]-->
  
 	<link rel="stylesheet" href="resources/patient-forms-resources/css/form.builder.css" />
 	<link rel="stylesheet" href="resources/patient-forms-resources/css/datepicker-theme.css" />
   
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="resources/css/ie-style.css" />
	<![endif]-->
	
  </head>
  
  <body>   

      <div class="main-page">	  	        
          
          <div id="formBuilderContainer" class="form-builder-container">

          </div>

          <div class="row row-full">
            <div class="navigation-buttons" style="display:none">
              <a href="#" id="btnNext" class="button large radius right margin-top-20"> NEXT </a>            
              <a href="#" id="btnPrev" class="button large secondary radius right navigation-buttons margin-top-20" style="display:none">PREVIOUS</a>
            </div>
          </div>                
      </div>
	  
   	    <!--[if lt IE 9]>
	    	<script src="resources/js/foundation4/jquery.js"></script>
		<![endif]-->
		<!--[if (gte IE 9) | (!IE)]><!-->
		    <script src="resources/js/jquery.js"></script>
		    <script src="resources/js/foundation.min.js"></script>
		<!--<![endif]-->       
	        
	    <!-- Foundation 3 for IE 8 and earlier -->
		<!--[if lt IE 9]>	    
		    <script src="resources/js/foundation3/foundation.min.js"></script>
		    <script src="resources/js/foundation3/app.js"></script>
		<![endif]-->
		
		<!-- Foundation 4 for IE 9 and later -->
		<!--[if gt IE 8]><!-->	    
		    <script src="resources/js/foundation4/foundation.min.js"></script>	    
		<!--<![endif]-->	
	   
	   <script src="resources/js/jquery-ui-1.10.4.min.js"></script>
       <script src="resources/js/json2.js"></script>      
       <script src="resources/patient-forms-resources/js/jquery.form.builder.js"></script>      
	   	   
       <script type="text/javascript">

        var frmBuilder = null;

        $(function(){
            $(document).foundation();             
            var json = $.parseJSON("${jsonString}");            
            frmBuilder = $("#formBuilderContainer").formBuilder({"json":json});
        });

      </script>
  </body>          