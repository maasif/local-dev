<%@ taglib prefix="s" uri="/struts-tags" %>

<head>
    <title>Quick Start Guide</title>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="x-ua-compatible" content="IE=Edge" />
    <!--[if (gte IE 9) | (!IE)]><!-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />
    <script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
    <!--<![endif]-->

    <!--[if lt IE 9]>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">
    <![endif]-->

    <!--[if gt IE 8]>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
    <![endif]-->

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css?v=0.4" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/quick-start.css" />

</head>

<body>

<div class="updated">
    <h4 class="margin-top-0 padding-10 align-center">HealthSlate<sup>&#174;</sup> Coach Portal QUICK START GUIDE V 1.1  5/17/2015</h4>
</div>

<div class="max-width-1129 margin-top-10 margin-bottom-20">

    <div class="row row-full bg-white left-margin-20">
        <div class="padding-left-10">
            <div>
                <h4 class="text-underline">Coach portal Logging in</h4>
                <p>Pre-launch: <a href="https://hstage.healthslate.com" target="_blank">https://hstage.healthslate.com </a>; Post-launch: <a href="https://diabetes.healthslate.com" target="_blank">https://diabetes.healthslate.com</a> user ID is your email address; resetting password requires PIN texted to your phone.
                </p>
            </div>

            <div>
                <h4 class="text-underline">Tech Support</h4>
                <p>
                    Phone 888-291-7245
                    <br/> Email <a href="#"> info@healthslate.com </a> or
                    <a href="#">lsanchez@healthslate.com</a>
                </p>
            </div>

            <div>
                <h4 class="text-underline">Key to Remember</h4>
                <p>
                    The HealthSlate Program enables and encourages an active, ongoing relationship with each member; this is different from the episodic interactions common with in-person diabetes education.
                </p>
            </div>
            <div>
                <h4 class="text-underline">Which members need my attention today/this week?</h4>
                <p>
                    On My Members page, sort columns by using arrows at the top of these three rows:
                </p>
                <ol>
                    <li type="a">&#34;1:1 Sessions&#34; shows which members have an upcoming 1:1 coaching session scheduled with you or any other coach</li>
                    <li type="a">&#34;New messages&#34; shows which members have sent you an &#34;in-app message&#34;</li>
                    <li type="a">&#34;New messages by Coach&#34; shows instances where another coach has notified you about a new Coach Note they have created about a given member.</li>
                    <img src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/myMembers.png" />
                </ol>
            </div>

            <div>
                <h4 class="text-underline">Key Areas of Focus When Interacting with Members</h4>
                <p>
                    &#91;Access the member&#39;s dashboard by clicking member&#39;s name from My Members page&#93;
                </p>
                <ol>
                    <li>What is the member&#39;s motivation? Encourage them to upload My Motivation photo and notes via Menu/Settings/Profile/My Motivation</li>
                    <li>How does Motivation tie to their Goals & Action Plans? Explain importance of visualizing and recording
                        <ul class="square">
                            <li>Challenges to sticking with Action Plans</li>
                            <li>Specific, detailed plan for overcoming the Challenges
                                <br/> &#40;The Goal creation tool in the member app facilitates all of this&#41;
                            </li>
                            <li>Remind member to self-assess goal progress &#40;see images at bottom of page.&#41;</li>
                        </ul>
                    </li>
                    <li>Look at coach&#39;s To Do list for the week.
                        <ul class="disc">
                            <li>This is particularly important in weeks with a 1:1.</li>
                            <img class="padding-top-13 " src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/memberInfo.png" />
                        </ul>
                        <div>
                            <p class="text-underline top-margin-25">How Members can self assess their goal progress:&#40;Menu/Goals & Habits&#41;</p>
                            <img class ="margin-top-10" src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/goalsAndHabits.png" />
                        </div>
                    </li>
                </ol>
            </div>
            <div>
                <h4 class="text-underline">Coach Notes</h4>
                <p>Check to see if anything is new about member that another coach has noted. &#40; 4 &#41;</p>
                <img src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/coachNotes.png" />
                <p class="top-margin-25" >Use Coach Notes to communicate anything with other coaches.</p>
                <ul class="disc">
                    <li>Use the Notify Coaches box to choose which coaches to notify.</li>
                    <li>Coaches receive an email and/or text alerting them to the Coach Note.</li>
                    <img class="margin-top-10" src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/addNote.png" />
                </ul>
            </div>
            <div>
                <h4 class="text-underline">Review member&#39;s self-management metrics</h4>
                <p>5. are BGs at goal?</p>
                <p>6. are majority of meals under carb ceiling?</p>
                <p>7. how physically active is the member? They can manually enter this information or it can be pulled from Misfit</p>
                <img src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/summary.png" />
            </div>
            <div>
                <h4 class="text-underline top-margin-25" >See member&#39;s full log book</h4>
                <p > via button at top right of Member&#39;s dashboard</p>
                <div class="margin-top-10">
                    <img src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/logBookNav.png">
                </div>
                <div class="top-margin-25">
                        <p>
                            Member accesses her own log book via Menu/Progress and Logs.
                            <br/>Log book button is at the <span class="text-underline">top of the Progress and Logs page</span>.
                        </p>
                    <div class="margin-top-10">
                        <img src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/leftSlider.png">
                    </div>

                </div>
            </div>
            <div>
                <h4 class="text-underline top-margin-25">Topics/Curriculum</h4>
                <ul class="disc">
                    <li>Is member getting through the weekly Topics? <span class="italic">&#40;Touch arrow in purple Topics module in dashboard to access Topics details page below&#41;</span></li>
                    <img class="margin-top-10" src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/topics.png" />
                </ul>
            </div>

            <div>
                <h4 class="text-underline top-margin-25">Messaging with members</h4>
                <ul class="disc">
                    <li>
                            When you send a Member an in-app message she receives a text saying she has received a message from her HealthSlate Coach with a link to open the app. The message appears in her Feed.
                    </li>

                    <img class="margin-top-10" src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/messagesNav.png" />
                    <p class="top-margin-25">8. You can include pictures by touching the icon to the left of the input box.</p>
                    <p>9. &#34;In-app messages&#34; are HIPAA compliant; the &#34;Text Messages&#34; tool only allows canned messages that do not include Protected Health Information.</p>
                </ul>
                <div >
                    <img src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/inAppMessage.png" />
                </div>
            </div>

            <div class="margin-top-10 ">
                <h4 class="text-underline top-margin-25">Video Calls with members</h4>
                <img  class="margin-top-10" src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/videoNav.png" />
                <ul class="disc">
                    <li class="top-margin-25" >Coaches can call members. Members CANNOT call Coaches.</li>
                    <li>The member&#39;s app must be open in order for her to receive your call. If the member&#39;s app is open you will see &#34;Member status: ONLINE&#34; at the top of the video chat page</li>
                    <li>
                        If the member is OFFLINE, you can send them a text message to open their app; from the Dashboard go to Messages then Text Messages and select the appropriate message from the drop down menu. &#40;See image at right.&#41; You can also call the member; their phone number is in &#34;Show member information&#34; at the top of their dashboard.
                    </li>
                    <img class="top-margin-25 width-750" src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/vCall.png" />
                    <li class="top-margin-25">If Member appears OFFLINE when their app is actually open, try refreshing your browser and/or ask him to close the app and then re-launch it.</li>
                    <li>What the member can do during a video call:
                        <ul class="circle">
                            <li>View you in the main window and themselves in a small window &#40;default&#41;</li>
                            <li>Stop viewing themselves and instead use the rear camera of their device to show you something, e.g., a glucometer, their feet, their food pantry. They do this by toggling the front vs. rear camera button.</li>
                            <li>Stop the video so they can navigate elsewhere in the app while still talking to you, e.g., to review logbook or charts. They re-start the video by touching a button on screen.</li>
                        </ul>
                    </li>
                </ul>
                <img class="width-1080 height-500" src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/sightCallInterface.png" />
            </div>
            <div>
                <h4 class="text-underline top-margin-25">Settings</h4>
                <div>
                    <p>Setting button is at top right of Member&#39;s Dashboard.</p>
                </div>
                <img  src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/settingNav.png" />
                <p class="margin-top-10">Use Settings to set and view</p>
                <ul class="disc">
                    <li>Medications reminders via text message &#40;this is often very effective so please try to get member to use it&#41;; member can also set this up via the app.</li>
                    <li>Carb ceilings &#40;member CANNOT change this via ap&#41;</li>
                    <li>Glucose targets &#40;get glucose targets from member&#39;s HCP; member CANNOT change this via app&#41;</li>
                    <li>Glucose testing schedule.</li>
                </ul>
                <div >
                    <img class="width-1080" src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/settingUI.png" />
                </div>
                <div>
                    <p class="top-margin-25 margin-bottom-0">Settings page in Member App &#40;Found in the Menu&#41;</p>
                    <img src="${pageContext.request.contextPath}/resources/css/images/quick_start_images/settingMobileUI.png" />
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $("#quickStartTab").addClass("selected-tab").parents("ul").prev("li").addClass("selected-tab");
    });
</script>
</body>

</html>