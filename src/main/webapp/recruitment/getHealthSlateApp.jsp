<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <title>Get HealthSlate App</title>
    <meta http-equiv="x-ua-compatible" content="IE=Edge"/>
    <!--[if (gte IE 9) | (!IE)]><!-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />
    <script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
    <!--<![endif]-->

    <!--[if lt IE 9]>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">
    <![endif]-->

    <!--[if gt IE 8]>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
    <![endif]-->

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/recruitment.css?v=0.2" />

    <!--[if lt IE 9]>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie8-style.css" />
    <![endif]-->

    <style type="text/css">
        .footer{
            position: absolute;
            bottom: 0;
        }

        .phone{
            position: static;
        }

        .spec{
            text-align: left;
        }

        @media (max-width: 600px) {

            .phone{
                display: none;
            }

            .footer{
                position: static;
            }

            .spec{
                text-align: center;
            }
        }

        @media \0screen\,screen\9 {
            .footer{
                position: absolute;
                bottom: -350px;
            }
        }

    </style>

</head>

<body>

<div class="recruitment-green remove-border">

    <div id="errMessage" class="alert-box alert alert-message-abs" style="display: none">______________________</div>

    <div class="row">
        <div class="columns medium-3">
            &nbsp;
        </div>

        <div class="columns medium-9">
            <p class="white-logo-align">
                <img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" />
            </p>
        </div>
    </div>

</div>

<div class="bg-white">

    <div class="row section-padding remove-border">
        <div class="columns medium-3 large-3 three">
            <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/android%20phone.png" class="phone" />
        </div>
        <div class="columns medium-6 large-6 six">
            <div class="grey-area-section remove-border">
                <h2 class="meal-summary-text">Get the HealthSlate App</h2>
                <div class="row">
                    <div class="columns medium-6 large-6 six">
                        <a href="${appLinkIOS}"><img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/apple-button.png"></a>
                    </div>
                    <div class="columns medium-6 large-6 six">
                        <label class="inline spec font-bold"> Requires iOS 7 or Higher </label>
                    </div>
                </div>
                <div class="row">
                    <div class="columns medium-6 large-6 six">
                        <a href="${appLinkAndroid}"><img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/android-play-store.png"></a>
                    </div>
                    <div class="columns medium-6 large-6 six">
                        <label class="inline font-bold spec"> Requires Android 4.4 or Higher </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="columns medium-3 large-3 three">
            <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/iphone.png" class="phone" />
        </div>
    </div>
</div>

<!--[if (gte IE 9) | (!IE)]><!-->
<script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
<!--<![endif]-->

<!-- Foundation 3 for IE 8 and earlier -->
<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
<![endif]-->

<!-- Foundation 4 for IE 9 and later -->
<!--[if gt IE 8]><!-->
<script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
<!--<![endif]-->

<script src="${pageContext.request.contextPath}/resources/js/jquery.migrate.min.js"></script>

<script type="text/javascript">

    $(function(){
        var browserVersion = $.browser.version;

        if(!$.browser.msie || browserVersion >= 9) {
            $(document).foundation();
        }
    });

</script>

</body>
</html>