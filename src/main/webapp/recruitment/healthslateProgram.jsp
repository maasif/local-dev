<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Introducing HealthSlate Program</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/>
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css?v=0.1" />
	<![endif]-->

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/recruitment.css?v=0.1" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/recruitment_responsive.css" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie8-style.css?v=0.1" />
	<![endif]-->
    <style>
        .eligible-text{
            display: inline-block;
            width: 100%;
        }

        .eligible-criteria-list li{
            font-size: 16px;
            text-align: left;
        }
    </style>
  </head>
  
  <body>

      <div class="recruitment-green">

          <div class="row">
              <div class="columns medium-6 large-6 six">
                    <div class="cascade-description">
                        <h2>Live the life you want</h2>
                        <h5>while managing your diabetes</h5>
                        <p class="statement">You don't have to give up your favorite foods, or become an exercise fanatic, just because you have diabetes.</p>

                        <p class="healthslate-logo">
                            <img src="${pageContext.request.contextPath}/resources/css/images/healthslate_logo_green.png" />
                        </p>
                    </div>
              </div>

              <div class="columns medium-6 large-6 six align-right">
                  <div class="recruitment-video">
                      <a href="#" data-reveal-id="videoModal">
                        <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/recruitment_video.png" />
                      </a>
                  </div>
              </div>
          </div>

      </div>

      <div class="recruitment-brown">
          <div class="row">
              <div class="columns medium-2 large-2 two">
                  <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/recruitment_smiley.png" class="smiley" />
              </div>
              <div class="columns medium-7 large-7 seven padding-left-0">
                    <div class="introducing-description">
                        <h4>Introducing the HealthSlate Program</h4>
                        <p>with this free pilot program, you learn how to <span class="meal-summary-text"> do what you WANT </span> in a way that works <span class="meal-summary-text"> with your diabetes. </span></p>
                    </div>
              </div>
              <div class="columns medium-3 large-3 three">
                <s:if test="%{#session.ENROLL_PROGRAM == true}">
                    <a href="TermsOfService.action?token=${uuid}&assignedFacility=${assignedFacility}" class="button radius expand btn-teal learn-more-align">Enroll</a>
                </s:if>
                <s:else>
                    <a href="javascript:;" class="button radius expand btn-teal learn-more-align consent-form-btn">Apply to Join</a>
                </s:else>
              </div>
          </div>
      </div>

      <div class="bg-white">

          <div class="row section-padding">
              <div class="columns medium-6 large-6 six">
                  <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/recruitment_misfit.png" />
              </div>
              <div class="columns medium-6 large-6 six">
                  <div class="grey-area-section remove-border">
                      <h4 class="meal-summary-text">Designed around your needs</h4>
                      <ul class="side-nav teal-bullets">
                          <li><label><span class="meal-summary-text">Make the food you love work</span> with your diabetes</label></li>
                          <li><label><span class="meal-summary-text">Meal plans</span> built around how you eat and what you like</label></li>
                          <li><label><span class="meal-summary-text">Free fitness and sleep tracker</span> for you to keep</label></li>
                          <li><label><span class="meal-summary-text">12 month program</span> that works with your schedule - no classes to show up to</label></li>
                          <li><label><span class="meal-summary-text">Weekly sessions</span> take as little as 5 minutes a day</label></li>
                          <li><label><span class="meal-summary-text">Right on your phone</span> - works with Android and iPhone</label></li>
                      </ul>
                  </div>
              </div>
          </div>

          <div class="row section-padding">
              <div class="columns medium-6 large-6 six">
                  <div class="grey-area-section remove-border">
                      <h4 class="activity-summary-text">One-on-one coaching</h4>
                      <ul class="side-nav teal-bullets">
                          <li><label><span class="activity-summary-text">Your own team of coaches</span> specializing in diabetes</label></li>
                          <li><label><span class="activity-summary-text">Find food you enjoy,</span> with your meal coaches</label></li>
                          <li><label><span class="activity-summary-text">Personalized support</span> and encouragement to help you manage your diabetes</label></li>
                          <li><label><span class="activity-summary-text">Immediate help</span> by phone, text, and video messaging</label></li>
                      </ul>
                  </div>
              </div>
              <div class="columns medium-6 large-6 six">
                  <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/recruitment_my_coaches.png" />
              </div>
          </div>

          <div class="row section-padding remove-border">
              <div class="columns medium-6 large-6 six">
                  <div class="grey-area-section remove-border">
                      <h4 class="teal-text">Knowing what works</h4>
                      <p class="logbook-summary-text font-size-12">The folks who designed the HealthSlate Program have helped thousands of people with diabetes live the lives they want. They know what works and this program can work for you too.</p>
                      <h6 class="margin-bottom-0">Join the FREE HealthSlate Program</h6>

                      <s:if test="%{#session.ENROLL_PROGRAM == true}">
                          <a href="TermsOfService.action?token=${uuid}&assignedFacility=${assignedFacility}" class="button radius btn-teal min-width-250">Enroll</a>
                      </s:if>
                      <s:else>
                          <a href="javascript:;" class="button radius btn-teal min-width-250 consent-form-btn">Apply to Join</a>
                      </s:else>

                      <p class="logbook-summary-text font-size-12">Available for Android and iPhone.</p>
                  </div>
              </div>
              <div class="columns medium-6 large-6 six">
                  <h4 class="meal-summary-text">Real people, real results</h4>
                  <ul class="side-nav real-people-list">
                      <li>
                          <div class="row row-auto">
                              <div class="medium-3 large-3 three columns">
                                  <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/recruitment_real_people_2.png" />
                              </div>
                              <div class="medium-9 large-9 nine columns">
                                   <p class="logbook-summary-text font-size-14 italic">"In the 2.5 years of practicing what I learned, I can tell you I have not given up any of the foods I love"</p>
                              </div>
                          </div>
                      </li>
                      <li>
                          <div class="row row-auto">
                              <div class="medium-3 large-3 three columns">
                                  <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/recruitment_real_people_1.png" />
                              </div>
                              <div class="medium-9 large-9 nine columns">
                                  <p class="logbook-summary-text font-size-14 italic">"You can enjoy this. You can make your life better, live healthier, and get through this disease"</p>
                              </div>
                          </div>
                      </li>
                  </ul>
              </div>
          </div>

      </div>

      <div id="videoModal" class="reveal-modal medium large" data-reveal aria-labelledby="videoModalTitle" aria-hidden="true" role="dialog">
          <h2 id="videoModalTitle">Live the life you want</h2>
          <iframe id="divIframe" src="about:blank" frameborder="0" width="100%" height="500"></iframe>
          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>

      <div id="eligibleDialog" class="reveal-modal medium" data-reveal>

          <h1 class="eligible-text">
              <div class="row row-auto">
                  <div class="columns medium-6 large-6 padding-left-0">
                      <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/providence_solutions_logo.png" class="cascade-logo left" />
                  </div>
                  <div class="columns medium-6 large-6 align-right">
                      <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/logowhite.png" class="cascade-logo right" />
                  </div>
              </div>
          </h1>

          <h3 class="meal-summary-text margin-bottom-0 align-left">You are eligible to participate if you have:</h3>
          <ul class="side-nav eligible-criteria-list">
              <li>
                  &#8212; Type 2 diabetes
              </li>
              <li>
                  &#8212; A Providence Medical Group doctor
              </li>
              <li>
                  &#8212; Insurance through Providence Health Plan
              </li>
          </ul>

          <p class="margin-bottom-0 align-left">If you meet all three criteria, click <b>Apply to Join</b></p>
          <p class="align-left">If you do not meet all three criteria, you are not eligible for this pilot program. Click <b>Exit</b> </p>

          <a id="closeEligibleDialog" class="close-reveal-modal">&#215;</a>
          <div class="align-right">
              <a href="JoinHealthSlateProgram.action?assignedFacility=${assignedFacility}" id="btnApplyToJoinDialog" class="button small radius btn-teal margin-top-10 margin-bottom-0">Apply to Join</a>
              <a href="javascript:;" class="button radius margin-top-10 btn-grey small margin-bottom-0" onclick="$('#closeEligibleDialog').click(); return false;">Exit</a>
          </div>
      </div>

    <!--[if (gte IE 9) | (!IE)]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
    <!--<![endif]-->

    <!-- Foundation 3 for IE 8 and earlier -->
    <!--[if lt IE 9]>

      <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>

      <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
      <script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
      <script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>

    <![endif]-->

    <!-- Foundation 4 for IE 9 and later -->
    <!--[if gt IE 8]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
    <!--<![endif]-->

      <script src="${pageContext.request.contextPath}/resources/js/jquery.migrate.min.js"></script>

      <script type="text/javascript">

        $(function(){
            var browserVersion = $.browser.version,
                $player = $("#divIframe"),
                oldVideoLink = "https://embed-ssl.wistia.com/deliveries/026fcc7683d63d13c2151326d4b37d77a63d7624/file.mp4",
                videoLink = "//fast.wistia.net/embed/iframe/ybwv19sb1r";

            if(!$.browser.msie || browserVersion >= 9) {
                $(document).foundation();
            }

            //for < IE8
            if($.browser.msie && browserVersion < 9){

                $("#videoModal").bind('reveal:opened', function() {
                    videoLink = oldVideoLink;//"https://embed-ssl.wistia.com/deliveries/026fcc7683d63d13c2151326d4b37d77a63d7624/file.mp4";
                    $player.css("position", "relative").attr("src", videoLink).prop("src", videoLink);
                }).bind("reveal:closed", function(){
                    $player.attr("src", "about:blank").prop("src", "about:blank");
                });

                $(".consent-form-btn").attr({
                    "data-reveal-id": "eligibleDialog",
                    "href": "#"
                }).data("reveal-id", "eligibleDialog");

            }else{
                $("#videoModal").on({
                    opened: function(){
                        $player.attr("src", videoLink);
                    },
                    closed: function(){
                        $player.attr("src", "about:blank");
                    }
                });
            }

            $(".consent-form-btn").on("click", function(e){
                e.preventDefault();
                var $headerLogo = $("#hLogo"),
                    src = $headerLogo.attr("src"),
                    href = "JoinHealthSlateProgram.action?assignedFacility=${assignedFacility}";

                //if its providence then show dialog
                if(src.indexOf("providence_solutions_logo.png") > -1){
                    $("#btnApplyToJoinDialog").attr("href", href);
                    if($.browser.msie && browserVersion < 9){
                        $(this).click();
                    }else{
                        $("#eligibleDialog").foundation("reveal", "open");
                    }
                }else{
                    window.location = href;
                }
            });
        });

      </script>
  </body>          
</html>