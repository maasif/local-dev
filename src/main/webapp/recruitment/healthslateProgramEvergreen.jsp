<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Introducing HealthSlate Program</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/recruitment.css?v=0.2" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie8-style.css" />
	<![endif]-->
	
  </head>
  
  <body>

      <div class="recruitment-green">

          <div class="row">
              <div class="columns medium-6 large-6 six">
                    <div class="cascade-description bg-dark-green">
                        <h3 class="evergreen-header text-white">Helping you live your <br/> healthiest best with diabetes. </h3>
                        <p class="statement font-size-14 margin-tb-0">We're proud to partner with you and HealthSlate to offer diabetes education and tools that fit in your schedule helping make diabetes management easier than ever.</p>
                        <h4 class="text-white">Welcome to the HealthSlate Program</h4>
                        <p class="statement font-size-14 margin-tb-0">EvergreenHealth is partnering with HealthSlate to offer this free diabetes education tool designed to help you develop lifestyle skills to manage your diabetes.</p>
                    </div>
              </div>

              <div class="columns medium-6 large-6 six align-right">
                  <div class="recruitment-video">
                      <a href="#" data-reveal-id="videoModal">
                        <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/recruitment_video.png" />
                      </a>
                  </div>
              </div>
          </div>

      </div>

      <div class="recruitment-brown">
          <div class="row">
              <div class="columns medium-2 large-2 two">
                  <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/recruitment_smiley.png" class="smiley" />
              </div>
              <div class="columns medium-7 large-7 seven padding-left-0">
                    <div class="introducing-description">
                        <h4>Introducing the HealthSlate Program</h4>
                        <p>with this free pilot program, you learn how to <span class="meal-summary-text"> do what you WANT </span> in a way that works <span class="meal-summary-text"> with your diabetes. </span></p>
                    </div>
              </div>
              <div class="columns medium-3 large-3 three">
                <s:if test="%{#session.ENROLL_PROGRAM == true}">
                    <a href="TermsOfService.action?token=${uuid}&assignedFacility=${assignedFacility}" class="button radius expand btn-teal learn-more-align">Enroll</a>
                </s:if>
                <s:else>
                    <a href="JoinHealthSlateProgram.action" class="button radius expand btn-teal learn-more-align">Apply to Join</a>
                </s:else>
              </div>
          </div>
      </div>

      <div class="bg-white">

          <div class="row section-padding">
              <div class="columns medium-6 large-6 six">
                  <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/recruitment_misfit.png" />
              </div>
              <div class="columns medium-6 large-6 six">
                  <div class="grey-area-section remove-border">
                      <h3 class="meal-summary-text">Helping you live your healthiest best with diabetes</h3>
                      <ul class="side-nav teal-bullets">
                          <li><label>12 month program works with your schedule - no classes to attend</label></li>
                          <li><label>Weekly sessions take as little as 5 minutes a day</label></li>
                          <li><label>Learn how the food you love can work with your diabetes</label></li>
                          <li><label>Customized meal plans built around how you eat and what you like</label></li>
                          <li><label>Free fitness and sleep tracker is yours to keep</label></li>
                          <li><label>Works on-the-go with your phone - Android and iPhone compatible</label></li>
                      </ul>
                  </div>
              </div>
          </div>

          <div class="row section-padding">
              <div class="columns medium-6 large-6 six">
                  <div class="grey-area-section remove-border">
                      <h3 class="activity-summary-text">Coaching is one-on-one</h3>
                      <div class="row row-auto">
                          <div class="columns medium-6 large-6 six">
                              <ul class="side-nav teal-bullets">
                                  <li><label>Meet your team of coaches who specialize in diabetes</label></li>
                                  <li><label>Meal coaches work with you to find food you enjoy and work with your diabetes</label></li>
                                  <li><label>Personalized report and daily encouragement help you manage your diabetes</label></li>
                                  <li><label>Assistance by phone and email</label></li>
                              </ul>
                          </div>
                          <div class="columns medium-6 large-6 six">
                              <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/recruitment_my_coaches_evergreen.png" />
                          </div>
                      </div>
                  </div>
              </div>
              <div class="columns medium-6 large-6 six">
                  <div class="grey-area-section remove-border">
                      <h3 class="activity-summary-text">Alexis Mettler, MS, RD</h3>
                      <div class="row row-auto">
                          <div class="columns medium-6 large-6 six">
                              <p class="general-text font-size-14">
                                 Meet Alexis Mettler, the lead Coach for HealthSlate at EvergreenHealth.<br/>
                                 Alexis is an accomplished chef, and a Registered Dietitian who graduated from Bastyr.<br/>
                                 As part of the diabetes education staff, Alexis will be reviewing your diabetes self-management skills. Count on her to be an active part of your diabetes life transition.
                              </p>
                          </div>
                          <div class="columns medium-6 large-6 six">
                            <img src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/alexis-mettler.jpg" />
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <div class="row section-padding remove-border">
              <div class="columns medium-6 large-6 six">
                  <div class="grey-area-section remove-border">
                      <h6 class="margin-bottom-0">Join the FREE HealthSlate Program</h6>
                      <s:if test="%{#session.ENROLL_PROGRAM == true}">
                          <a href="TermsOfService.action?token=${uuid}&assignedFacility=${assignedFacility}" class="button radius expand btn-teal learn-more-align">Enroll</a>
                      </s:if>
                      <s:else>
                          <a href="JoinHealthSlateProgram.action?assignedFacility=${assignedFacility}" class="button radius btn-teal min-width-250">Apply to Join</a>
                      </s:else>
                  </div>
              </div>
              <div class="columns medium-6 large-6 six">
                  <h3 class="meal-summary-text">HealthSlate works!</h3>
                  <p class="logbook-summary-text font-size-14">The folks who designed the HealthSlate Program have helped thousands of people with diabetes live the lives they want. They know what works and this program can work for you too.</p>
              </div>
          </div>

      </div>

      <div id="videoModal" class="reveal-modal large" data-reveal aria-labelledby="videoModalTitle" aria-hidden="true" role="dialog">
          <h2 id="videoModalTitle">Live the life you want</h2>
          <div class="flex-video widescreen vimeo">
              <iframe id="divIframe" width="1280" height="720" src="about:blank" frameborder="0" allowfullscreen></iframe>
          </div>

          <a class="close-reveal-modal" aria-label="Close">&#215;</a>
      </div>

    <!--[if (gte IE 9) | (!IE)]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
    <!--<![endif]-->

    <!-- Foundation 3 for IE 8 and earlier -->
    <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>

        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
        <script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
        <script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>

    <![endif]-->

    <!-- Foundation 4 for IE 9 and later -->
    <!--[if gt IE 8]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
    <!--<![endif]-->
   <script src="${pageContext.request.contextPath}/resources/js/date.js"></script>
   <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/jquery.migrate.min.js"></script>

      <script type="text/javascript">

        $(function(){
            var browserVersion = $.browser.version,
                $player = $("#divIframe"),
                oldVideoLink = "https://embed-ssl.wistia.com/deliveries/026fcc7683d63d13c2151326d4b37d77a63d7624/file.mp4",
                videoLink = "//fast.wistia.net/embed/iframe/ybwv19sb1r";

            if(!$.browser.msie || browserVersion >= 9) {
                $(document).foundation();
            }

            //for < IE8
            if($.browser.msie && browserVersion < 9){
                $("#videoModal").bind('reveal:opened', function() {
                    videoLink = oldVideoLink;//"https://embed-ssl.wistia.com/deliveries/026fcc7683d63d13c2151326d4b37d77a63d7624/file.mp4";
                    $player.css("position", "relative").attr("src", videoLink).prop("src", videoLink);
                }).bind("reveal:closed", function(){
                    $player.attr("src", "about:blank").prop("src", "about:blank");
                });
            }else{
                $("#videoModal").on({
                    opened: function(){
                        $player.attr("src", videoLink);
                    },
                    closed: function(){
                        $player.attr("src", "about:blank");
                    }
                });
            }
        });

      </script>
  </body>          
</html>