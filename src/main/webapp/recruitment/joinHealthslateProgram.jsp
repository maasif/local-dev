<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Join HealthSlate Program</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->

    <!--[if lt IE 9]>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">
    <![endif]-->

	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
	<![endif]-->

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/recruitment.css?v=0.1" />

   <!--[if lt IE 9]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie8-style.css" />
	<![endif]-->

    <script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>

    <style type="text/css">
        .footer{
            position: absolute;
            bottom: 0px;
        }

        @media \0screen\,screen\9 {
            .footer{
                position: inherit;
                bottom: 0px;
            }
        }

        small.error{
            margin-bottom: 0 !important;
        }

        .err-transparent{
            background-color: transparent !important;
            background: transparent !important;
            color: #fff !important;
        }
    </style>
  </head>

  <body data-ng-app="JoinProgramModule">

      <div id="loading" class="loader position-fixed">
          <div class="image-loader">
              <p class="progress-please-wait">Please wait...</p>
              <div class="progress progress-striped active">
                  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                  </div>
              </div>
              <p id="progressTxt" class="progress-text">We're saving data</p>
          </div>
      </div>

      <div class="recruitment-green remove-border">

          <div id="errMessage" class="alert-box alert alert-message-abs" style="display: none">______________________</div>

          <div class="row">
              <div class="columns medium-12">
                  <p class="white-logo-align">
                      <img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" />
                  </p>
              </div>
          </div>

      </div>

      <div class="bg-white">

          <div class="row section-padding remove-border">
              <div class="columns medium-12 large-12 twelve">
                  <div class="grey-area-section remove-border" data-ng-controller="JoinProgramController">
                      <h2 class="meal-summary-text">Request an Invite to join the HealthSlate Program</h2>

                      <form id="formJoinProgram" name="formJoinProgram" novalidate method="POST" ng-submit="submitForm(patient)">

                              <div class="row row-auto">

                                  <div class="columns medium-6 large-6 six padding-left-0">
                                      <label class="inline margin-bottom-0 meal-summary-text">
                                          <span class="meal-summary-text font-bold">First Name</span>
                                          <input type="text" name="firstName" tabindex="1" data-ng-model="patient.firstName" class="margin-bottom-0 rounded" placeholder="first name" required />
                                          <small class="error err-transparent" ng-show="submitted && formJoinProgram.firstName.$error.required">Required.</small>
                                      </label>
                                  </div>

                                  <div class="columns medium-6 large-6 six padding-left-0">
                                      <label class="inline margin-bottom-0 meal-summary-text">
                                          <span class="meal-summary-text font-bold">Last Name</span>
                                          <input type="text" name="lastName" tabindex="2" data-ng-model="patient.lastName" class="margin-bottom-0 rounded" placeholder="last name" required />
                                          <small class="error err-transparent" ng-show="submitted && formJoinProgram.lastName.$error.required">Required.</small>
                                      </label>
                                  </div>

                              </div>
                              <div class="row row-auto">

                                    <div class="columns medium-6 large-6 six padding-left-0">
                                        <label class="inline meal-summary-text margin-bottom-0">
                                            <span class="meal-summary-text font-bold">Email address (you can access on your smart phone)</span>
                                            <input type="email" name="email" tabindex="3" data-ng-model="patient.email" placeholder="email address" class="rounded margin-bottom-0" ng-pattern="/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i" required />
                                            <small id="errEmailRequired" class="error err-transparent" ng-show="submitted && formJoinProgram.email.$error.required">Required.</small>
                                            <small id="errEmailIncorrect" class="error err-transparent" ng-show="submitted && formJoinProgram.email.$error.pattern">Invalid email address.</small>
                                        </label>
                                    </div>

                                  <div class="columns medium-6 large-6 six padding-left-0">
                                      <label class="inline margin-bottom-0">
                                          <span class="meal-summary-text font-bold">Phone number</span>
                                          <input id="txtPhone" name="phoneNumber" tabindex="4" data-ng-model="patient.phoneNumber" type="tel" placeholder="(000) 000-0000" ng-pattern="/^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/" class="rounded margin-bottom-0" required />
                                          <small class="error err-transparent" ng-show="submitted && formJoinProgram.phoneNumber.$error.required">Required.</small>
                                          <small class="error err-transparent" ng-show="submitted && formJoinProgram.phoneNumber.$error.pattern">
                                              Please enter valid phone number
                                          </small>
                                      </label>
                                  </div>
                              </div>

                              <div class="row row-auto">
                                  <div class="columns medium-6 large-6 six padding-left-0">
                                      <label class="inline margin-bottom-0">
                                          <span class="meal-summary-text font-bold">Date of Birth</span>
                                          <input id="txtDOB" name="dobString" tabindex="5" datepicker data-ng-model="patient.dobString" type="tel" placeholder="date of birth" class="rounded margin-bottom-0" required />
                                          <small class="error err-transparent" ng-show="submitted && formJoinProgram.dobString.$error.required">Required.</small>
                                      </label>
                                  </div>

                              </div>

                              <hr/>

                               <div class="row row-auto">

                                  <div class="columns medium-12 large-12 twelve padding-left-0">
                                      <input id="btnSendRequest" type="submit" tabindex="6" class="button radius btn-teal min-width-250" value="Send Request" />
                                  </div>

                              </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>

      <!--[if (gte IE 9) | (!IE)]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
      <!--<![endif]-->

      <!-- Foundation 3 for IE 8 and earlier -->
      <!--[if lt IE 9]>
      <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
      <![endif]-->

      <!-- Foundation 4 for IE 9 and later -->
      <!--[if gt IE 8]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
      <!--<![endif]-->

      <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
      <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
      <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.4.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/jquery.migrate.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/jquery.mask.min.js"></script>

      <script type="text/javascript">

        var BASE_URL = "${pageContext.request.contextPath}/",
            ENROLL_EMAIL = '${email}',
            TOKEN = '${token}',
            FACILITY_NAME = '${facilityName}',
            SHOULD_SKIP_CONSENT = '${shouldSkipConsent}',
            browserVersion = $.browser.version,
            assignedFacility = '${assignedFacility}';

        $(function(){

            if(!$.browser.msie || browserVersion >= 9) {
                $(document).foundation();
            }

            $("#txtPhone").mask('(000) 000-0000');
            $("small.error").removeClass("err-transparent");
        });

      </script>

      <script src="${pageContext.request.contextPath}/resources/js/controllers/join_program_controller.js?v=0.12"></script>
  </body>
</html>