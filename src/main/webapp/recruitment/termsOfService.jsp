<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Terms Of Service</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css?v=0.1" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/recruitment.css?v=0.2" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie8-style.css" />
	<![endif]-->

    <script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>

    <style type="text/css">
        @media \0screen\,screen\9 {
            .footer{
                position: inherit;
                bottom: 0px;
            }
        }
    </style>
  </head>
  
  <body ng-app="TOSModule">

      <div id="loading" class="loader position-fixed">
          <div class="image-loader">
              <p class="progress-please-wait">Please wait...</p>
              <div class="progress progress-striped active">
                  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                  </div>
              </div>
              <p id="progressTxt" class="progress-text">We're saving data</p>
          </div>
      </div>

      <div class="recruitment-green remove-border">

          <div id="errMessage" class="alert-box alert alert-message-abs" style="display: none">______________________</div>

          <div class="row">
              <div class="columns medium-12">
                  <p class="white-logo-align">
                      <img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" />
                  </p>
              </div>
          </div>

      </div>

      <div class="bg-white">

          <div class="row section-padding remove-border">
              <div class="columns large-12 medium-12 twelve">
                  <div class="grey-area-section remove-border" ng-controller="TOSController">

                      <div id="divConsentFormContainer" class="consent-form-container">

                          <div class="printer-email-buttons">
                              <ul class="inline-list right">
                                  <li>
                                      <a id="btnPrintConsent" href="#" class="printer-img">
                                          <img title="Print Consent" src="${pageContext.request.contextPath}/resources/css/images/printer-black.png" width="20" />
                                      </a>
                                  </li>
                                  <li>
                                      <a id="btnEmailConsent" href="#">
                                          <img title="Email Consent" src="${pageContext.request.contextPath}/resources/css/images/email-black.png" width="20" />
                                      </a>
                                  </li>
                              </ul>
                          </div>

                          <div id="divConsentForm">

                          </div>

                          <p class="logbook-summary-text font-size-14 margin-top-10 font-bold">I have read all of the above, and received satisfactory answers about what I did not understand.</p>

                          <div id="loadingConsentForm" class="consent-form-loading">
                              <label><img src="${pageContext.request.contextPath}/resources/css/images/refresh_gif.gif" /> Loading...</label>
                          </div>
                      </div>

                      <form id="tosForm" name="tosForm" novalidate method="POST" ng-submit="submitForm(patient)">

                              <div class="row row-auto">

                                    <div class="columns large-6 medium-6 six padding-left-0">
                                        <label class="inline margin-bottom-0 meal-summary-text">
                                            <span class="meal-summary-text font-bold">First Name</span>
                                            <input id="txtFirstName" type="text" name="firstName" tabindex="1" ng-model="patient.firstName" class="margin-bottom-0 rounded" placeholder="first name" required />
                                            <small class="error" ng-show="submitted && tosForm.firstName.$error.required">Required.</small>
                                        </label>
                                        <label class="inline meal-summary-text margin-bottom-0">
                                            <span class="meal-summary-text font-bold">Email address</span>
                                            <input id="txtEmail" type="email" name="email" tabindex="3" ng-model="patient.email" placeholder="email address" class="rounded margin-bottom-0" ng-pattern="/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i" required />
                                            <small class="error" ng-show="submitted && tosForm.email.$error.pattern">Invalid email address.</small>
                                            <small class="error" ng-show="submitted && tosForm.email.$error.required">Required.</small>
                                        </label>
                                    </div>

                                    <div class="columns medium-6 large-6 six padding-left-0">
                                        <label class="inline margin-bottom-0 meal-summary-text">
                                            <span class="meal-summary-text font-bold">Last Name</span>
                                            <input id="txtLastName" type="text" name="lastName" tabindex="2" ng-model="patient.lastName" class="margin-bottom-0 rounded" placeholder="last name" required />
                                            <small class="error" ng-show="submitted && tosForm.lastName.$error.required">Required.</small>
                                        </label>
                                        <%--<label class="inline margin-bottom-0">
                                            <span class="meal-summary-text font-bold">Phone number</span>
                                            <input id="txtPhone" name="phone" tabindex="4" ng-model="patient.phone" type="tel" placeholder="(000) 000-0000" ng-pattern="/^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/" class="rounded margin-bottom-0" required />
                                            <small class="error" ng-show="submitted && tosForm.phone.$error.required">Required.</small>
                                            <small class="error" ng-show="submitted && tosForm.phone.$error.pattern">
                                                Please enter valid phone number
                                            </small>
                                        </label>--%>
                                    </div>

                              </div>

                              <hr/>

                               <div class="row row-auto">

                                  <div class="columns medium-12 large-12 padding-left-0">
                                      <input id="btnSubmit" type="submit" ng-click="submitted=true" class="button radius btn-teal min-width-250" value="I Accept" />
                                      <a href="HealthSlateProgram.action?token${token}&uuid=${token}&assignedFacility=${assignedFacility}" class="button radius btn-grey min-width-250">I Do Not Accept</a>
                                  </div>
                              </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>

    <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
    <![endif]-->
    <!--[if (gte IE 9) | (!IE)]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
    <!--<![endif]-->

    <!-- Foundation 3 for IE 8 and earlier -->
    <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
    <![endif]-->

    <!-- Foundation 4 for IE 9 and later -->
    <!--[if gt IE 8]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
    <!--<![endif]-->

      <script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/jquery.migrate.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
      <script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js?v=0.1"></script>

      <script type="text/javascript">

        var BASE_URL = "${pageContext.request.contextPath}/",
            PATIENT_ID = '${patientId}',
            PAT_OBJECT = {
                'patientId': '${patientId}',
                'firstName': "${firstName}",
                'lastName': "${lastName}",
                'email': '${email}',
                'phone': '${phone}'
            },
            TOKEN = '${token}',
            consentFormFile  = '${pageContext.request.contextPath}/consentForms/${consentFormFile}',
            assignedFacility = '${assignedFacility}';

        function removeCountryCode(){
            var countryCode = PAT_OBJECT.phone.indexOf("+1");

            if(countryCode > -1){
                PAT_OBJECT.phone = PAT_OBJECT.phone.substring(countryCode+3, PAT_OBJECT.phone.length);
            }

            $("#txtPhone").val(PAT_OBJECT.phone);
        }

        $(function(){
            var browserVersion = $.browser.version;

            if(!$.browser.msie || browserVersion >= 9) {
                $(document).foundation();
            }

            //load consent form via ajax
            Ajaxing.loadExternalPage($("#divConsentForm"), consentFormFile);

            $("#btnPrintConsent").on("click", function(e){
                e.preventDefault();
                printConsentWindow($("#divConsentForm").html());
            });

            if(PAT_OBJECT){
                $("#pFirstName").text(PAT_OBJECT.firstName);
                $("#pLastName").text(PAT_OBJECT.lastName);
                $("#pEmail").text(PAT_OBJECT.email);
            }

            $("#btnEmailConsent").on("click", function(e){
                e.preventDefault();

                var postRequestActions = {
                        "successCallBack": onSuccessEmailTOS,
                        "loading": $("#loading")
                    },
                    dataToSend = {
                        "otherData": document.getElementById("txtEmail").value,
                        "firstName": document.getElementById("txtFirstName").value,
                        "lastName": document.getElementById("txtLastName").value,
                        "assignedFacility": '${assignedFacility}'
                    };

                Ajaxing.sendPostRequest("emailTOS.action", dataToSend, postRequestActions);
            });
        });

        function printConsentWindow(data) {
            var printWindow = window.open('', 'divConsentForm', 'scrollbars=1,height=800,width=1000');
            printWindow.document.write('<html><head><title></title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(data);
            printWindow.document.write('</body></html>');

            printWindow.document.close(); // necessary for IE >= 10
            printWindow.focus(); // necessary for IE >= 10

            printWindow.print();
            printWindow.close();

            return true;
        }

          function onSuccessEmailTOS(data){
              var alertMessages = Object.create(AlertMessage).init(),
                  $error = $("#errMessage");

              if(data.STATUS == "SUCCESS"){
                  alertMessages.showAlertMessage($error, "Email sent successfully.", alertMessages.SUCCESS);
              }else{
                  alertMessages.showAlertMessage($error, "Unable to save, please try again later.", alertMessages.ERROR);
              }
          }
      </script>

      <script src="${pageContext.request.contextPath}/resources/js/controllers/tos_controller.js?v=0.5"></script>
  </body>          
</html>