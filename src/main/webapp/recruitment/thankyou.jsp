<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <title>Join HealthSlate Program</title>
   <meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
   <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation-latest-5.4.7/foundation.min.css" />	     
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->    
	
	<!--[if lt IE 9]>		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">			
	<![endif]-->
	
	<!--[if gt IE 8]>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/normalize.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation4/foundation.min.css">	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />	
	<![endif]-->

 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/patient.dashboard.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/recruitment.css?v=0.1" />
      
   <!--[if lt IE 9]>			
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie8-style.css" />
	<![endif]-->


    <style type="text/css">
        .footer{
            position: absolute;
            bottom: 0;
        }

        @media \0screen\,screen\9 {
            .footer{
                position: absolute;
                bottom: -350px;
            }
        }
    </style>
  </head>
  
  <body>

      <div id="loading" class="loader position-fixed">
          <div class="image-loader">
              <p class="progress-please-wait">Please wait...</p>
              <div class="progress progress-striped active">
                  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                  </div>
              </div>
              <p id="progressTxt" class="progress-text">We're saving data</p>
          </div>
      </div>

      <div class="recruitment-green remove-border">

          <div class="row">
              <div class="columns medium-12">
                  <p class="white-logo-align">
                      <img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" />
                  </p>
              </div>
          </div>

      </div>

      <div class="bg-white">

          <div class="row section-padding remove-border top-30">
              <div class="columns medium-12">
                  <div class="grey-area-section remove-border top-30">
                      <h2 class="meal-summary-text">Thank you for your request</h2>
                      <p class="logbook-summary-text font-size-14">We are reviewing your information and will get back to you within the next 5 days to let you know If you qualify for our program.</p>
                  </div>
              </div>
          </div>
      </div>

    <!--[if (gte IE 9) | (!IE)]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
    <!--<![endif]-->

    <!-- Foundation 3 for IE 8 and earlier -->
    <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
    <![endif]-->

    <!-- Foundation 4 for IE 9 and later -->
    <!--[if gt IE 8]><!-->
        <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
    <!--<![endif]-->

      <script src="${pageContext.request.contextPath}/resources/js/jquery.migrate.min.js"></script>

      <script type="text/javascript">

        $(function(){
            var browserVersion = $.browser.version;

            if(!$.browser.msie || browserVersion >= 9) {
                $(document).foundation();
            }
        });

      </script>
  </body>          
</html>