  <div id="footer" class="footer">

    <div class="row row-auto">

        <div class="columns medium-6 large-6 six">
          <div class="copyright-healtshlate top-30">
            <img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" />
            <p>HealthSlate legal text and copyright statement. Also any partner legal text and copyrights.</p>
          </div>
        </div>

        <div class="columns medium-6 large-6 six">

          <span class="grey-separator"></span>

          <div class="about-healthslate">
            <h6 class="color-green">About HealthSlate</h6>
            <p class="our-mission">Our mission is to help with chronic diseases maintain their health through highly indisclosed programs of education, behavior medication and clinical support.
            <br/><br/>
            Combining expertise in consumer behavior, mobile technology and evidenced-based medication. HealthSlate improves patients' quality of life and health outcomes. Learn more at <a href="http://www.healthslate.com" target="_blank">www.healthslate.com</a>
            </p>
          </div>
       </div>
    </div>

</div>
  
