<%@ taglib prefix="s" uri="/struts-tags"%>
<nav class="navbar bg-white recruitment-nav" role="navigation">
  <div class="container-fluid row">
      <div class="navbar-header margin-top-15">
          <s:if test="%{facility.facilityLogo == 'cascade_solutions_logo.png'}">
              <img id="hLogo" src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/<s:property value='%{facility.facilityLogo}'/>" class="cascade-logo remove-width" />
          </s:if>
          <s:else>
              <img id="hLogo" src="${pageContext.request.contextPath}/resources/css/images/recruitment_icons/<s:property value='%{facility.facilityLogo}'/>" class="cascade-logo" />
          </s:else>
          <p id="pSlogan" class="logbook-summary-text cascade-slogan"><s:property value='%{facility.slogan}'/></p>
      </div>
  </div>
</nav>
<script type="text/javascript">
    $(function(){
       $("#pSlogan").html('${facility.slogan}');
    });
</script>
