<!doctype html>
<%-- <%@ taglib prefix="s" uri="/struts-tags"%> --%>
<html>
<head>
	<title>Reset Password</title>
	<meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0" />
	<meta http-equiv="x-ua-compatible" content="IE=Edge"/>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-3/css/bootstrap.min.css" />
	<!--[if (gte IE 9) | (!IE)]><!-->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation.min.css" />
	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->

    <!--[if lt IE 9]>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/foundation.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation3/app.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ie-style.css" />
    <![endif]-->


    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/facility.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/login.style.css?v=0.3" />
	<script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>
</head>

<body ng-app="ResetPasswordModule">

<div id="loading" class="loader position-fixed">
	<div class="image-loader">
		<p class="progress-please-wait">Please wait...</p>
		<div class="progress progress-striped active">
			<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
			</div>
		</div>
		<p id="progressTxt" class="progress-text">We're saving data</p>
	</div>
</div>

<div id="mainPage" class="main-page overflow-fix main-page-login-i8">
	<div class="logo">
		<img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" />
	</div>

	<div class="padding-20">
		<h4 class="sign-in-text">Reset Password</h4>
	</div>

	<div id="errMessage" class="alert-box alert alert-message-abs" style="display: none">______________________</div>

	<div class="content-login-form" ng-controller="ResetPasswordController">
		<div class="padding-20" id="page1">
			<form id="resetPasswordForm" name="resetPasswordForm" method="POST" novalidate ng-submit="submitForm(resetPassword)">

				<input type="hidden" name="resetPassword.userName" id="txtHiddenUser" />
				<input type="hidden" name="resetPassword.token" id="txtHiddenToken" />

				<div class="row">
					<div class="small-12 twelve columns">
						<label class="align-left font-18 padding-20 ie-error-style position-rel">
							Password
							<input id="txtPassword" type="password" autofocus="autofocus" name="password" ng-model="resetPassword.password" ng-pattern="/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[':;,_.#?!@$%^&*-]).{8,}$/" class="validate-me login-input margin-bottom-0 padding-left-5 login-input-i8" required />
							<small class="error login-input-i8" ng-show="submitted && resetPasswordForm.password.$error.required">Required.</small>
							<small class="error login-input-i8" ng-show="submitted && resetPasswordForm.password.$error.pattern">
								Password must contain one upper case letter, one lower case letter, one number, and one special character such as #.
							</small>
						</label>
					</div>
				</div>

				<div class="row">
					<div class="small-12 twelve columns">
						<label class="align-left font-18 padding-20 ie-error-style position-rel">
							Repeat Password
							<input id="txtRepeatPassword" type="password" name="repeatPassword" ng-model="resetPassword.repeatPassword" required class="validate-me login-input margin-bottom-0 padding-left-5 login-input-i8" />
							<small class="error login-input-i8" ng-show="submitted && resetPasswordForm.repeatPassword.$error.required">Required.</small>
							<small class="error login-input-i8" ng-show="submitted && resetPassword.password!=resetPassword.repeatPassword">Passwords do not match.</small>
						</label>
					</div>
				</div>

				<div class="row">
					<div class="small-12 twelve columns">
						<label class="align-left font-18 padding-20 ie-error-style position-rel">
							Pin Code
							<input id="txtPinCode" type="text" name="pinCode" ng-model="resetPassword.pinCode" required class="validate-me login-input margin-bottom-0 padding-left-5 login-input-i8" />
							<small class="error login-input-i8" ng-show="submitted && resetPasswordForm.pinCode.$error.required">Required.</small>
						</label>
					</div>
				</div>

				<div class="row">
					<div class="small-12 padding-lr-22 align-left twelve columns margin-bottom-0">
						<input type="submit" ng-click="submitted=true" class="small button radius btn-teal btn-sign-in margin-top-14 margin-bottom-0 login-input-i8" value="RESET MY PASSWORD" />
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="footer" class="footer">

	<div class="row row-full">

		<div class="columns medium-12">
			<div class="copyright-healtshlate">
				<img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" />
			</div>
		</div>

		<%--<div class="columns medium-6">

			<span class="grey-separator"></span>

			<div class="about-healthslate">
				<h6 class="color-green">About HealthSlate</h6>
				<p class="our-mission">Our mission is to help with chronic diseases maintain their health through highly indisclosed programs of education, behavior medication and clinical support.
					<br/><br/>
					Combining expertise in consumer behavior, mobile technology and evidenced-based medication. HealthSlate improves patients' quality of life and health outcomes. Learn more at <a href="http://www.healthslate.com" target="_blank">www.healthslate.com</a>
				</p>
			</div>
		</div>--%>
	</div>

</div>

<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/resources/js/foundation4/jquery.js"></script>
<![endif]-->

<!--[if (gte IE 9) | (!IE)]><!-->
<script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
<!--<![endif]-->

<!-- Foundation 3 for IE 8 and earlier -->
<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/resources/js/foundation3/foundation.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/foundation3/app.js"></script>
<![endif]-->

<!-- Foundation 4 for IE 9 and later -->
<!--[if gt IE 8]><!-->
<script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>
<!--<![endif]-->

<script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
<script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
<script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/jquery.migrate.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/common.js"></script>

<script type="text/javascript">

	$(function(){
		var browserVersion = $.browser.version;
		if(!$.browser.msie || browserVersion >= 9){
			$(document).foundation();
		}

		$("#txtHiddenUser").val("${userName}");
		$("#txtHiddenToken").val("${token}");
	});

    function clearPasswordsOnFocus(){
        var $allPasswords = $("#txtRepeatPassword,#txtPassword");
        $allPasswords.on("focus", function(){
            $allPasswords.off("focus").val("");
        });
    }
</script>

<script src="${pageContext.request.contextPath}/resources/js/controllers/reset_password_controller_forced.js?v=0.3"></script>

</body>
</html>