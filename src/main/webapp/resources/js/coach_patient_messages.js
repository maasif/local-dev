if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

var SHOULD_SHOW_LOADING = false;
//==========================================
/* CoachMessages JS Model*/
var CoachMessages = {
	
	init: function(appPath, patId, logId, imagePath, fromLogDetail){
		var that = this;
		
		that.patientId = patId;
		that.logId = "0";
		that.imagePath = "";
		that.appPath = appPath;
		that.imagesPath = that.appPath;
        that.SMS_LIST = [];
		that.shouldShowLoading = true;
        that.fromLogDetail = true;
        that.listCount = that.SMS_LIST.length;

		window.coachMessagesObject = that;

		if(logId){
			that.logId = logId;
		}

        if(fromLogDetail){
            that.fromLogDetail = false;
        }

		if(imagePath){
			that.imagePath = "/logImages/" + imagePath;
		}

		//alert message instance
		that.ALERT_MESSAGES = Object.create(AlertMessage).init();

		// init twilio msgs dialog
		that.initTwilioMessages();

		//ajaxing instance
		that.AJAXING = Object.create(Ajaxing);
		
		//cache page controls
		that.cachePageConstants();

		//bind auto grow plugin to textarea
        if(that.logId == "0") {
            that.bindTextareaAutoGrow(that.$txtMessage);
        }

		//bind event listener
		that.bindEventListener();
		that.getSmsListFromServer();

		//then sets refresh interval after each 50 seconds
		setInterval(function(){
			SHOULD_SHOW_LOADING = false;
			that.getSmsListFromServer();
		}, 50000);

        that.$txtMessage = $("#txtCoachMessage").val("").change().on("keyup", function(){
            var charLength = $(this).val().length,
                maxLength = parseInt($(this).prop("maxlength")) - charLength;

            that.$maxCharacters.text(maxLength + " characters left").removeClass("right-padding");

            if(($(this).height() == "100")){
                that.$maxCharacters.addClass("right-padding");
            }
        });

		that.getProvidersListFromServer();

		//if data is save in local storage then retrieve it and load in field
		if(localStorage && localStorage.getItem(LOCAL_STORAGE_MESSAGE_KEY)){
			that.$txtMessage.val(localStorage.getItem(LOCAL_STORAGE_MESSAGE_KEY)).change().trigger("keyup");
		}

        return that;
	},
	
	//cache page controls
	cachePageConstants: function(){
		
		var that = this;
		
		that.$btnSendMessage = $("#btnSendCoachMessage");
		that.$txtMessage = $("#txtCoachMessage").val("").change();
		that.$threadedViewEmpty = $(".coach-thread-empty").show();
		that.$dialogArea = $("#coachThreadView");
		that.$threadViewList = $("#coachThreadViewList").empty().hide();
		that.$errMessage = $("#errMessageCoach");
		that.$loading = $("#loading");
		that.$shadow = $("#coachThreadViewShadow").hide();
		that.$btnInAppRefreshMessages = $("#btnInAppRefreshMessages").off("click");
        that.$maxCharacters = $(".max-characters");
		that.$chatProviderList = $("#chatProviderList").val(loggedIn_UserId).on("change", function(){

            var $msgSendContainer = $("#msg-send-txt-drpdwn");

            that.userId = $(this).val();
            SHOULD_SHOW_LOADING = true;

			if ($(this).val() == loggedIn_UserId ) {
                $msgSendContainer.show();
			}else{
                $msgSendContainer.hide();
			}

			that.getSmsListFromServer()
		});

		//when scroll dialog area div show shadow on the above, just to let user know there is data upside
		that.$dialogArea.on("scroll", function(){
		  var y = $(this).scrollTop();
		  if( y > 0 ){
			  that.$shadow.show();
		  } else {
			  that.$shadow.hide();
		  }
		});

		//on text blue save changes in localStorage
		that.$txtMessage.on("blur", function(){
			if(localStorage){
				localStorage.setItem(LOCAL_STORAGE_MESSAGE_KEY, $(this).val());
			}
		});
	},
		
	//bind event listener
	bindEventListener: function(){
		var that = window.coachMessagesObject;
		
		that.$btnSendMessage.on("click", function(e){
			e.preventDefault();
			
			that.shouldShowLoading = true;
			window.coachMessagesObject = that;
						
			var messageText = that.$txtMessage.val(),
                $formUpload = $("#formUpload");

			if($.trim(messageText).length > 0){

                $(this).prop("disabled", "disabled").addClass("disabled");

                if($formUpload.length > 0){

                    if($formUpload && $("#uploadFile")[0].files.length > 0 && isAllowUploading) {
                        that.$loading.show();
                        var formData = new FormData($formUpload[0]);

                        $formUpload.off("submit").on("submit", function () {
                            $.ajax({
                                url: "uploadImageMessage.action",
                                type: 'POST',
                                data: formData,
                                success: function (data) {
                                    try {
                                        data = $.parseJSON(data);
                                        that.onSuccessUploadImage(data);
                                    } catch (err) {
                                    }
                                },
                                error: function (data, status) {
                                    console.log("ERROR", data);
                                },
                                cache: false,
                                contentType: false,
                                processData: false
                            });

                            return false;
                        });

                        $formUpload.submit();
                    }else{

                        if(isAllowUploading){
                            that.sendMessageToDevice(messageText);
                        }else{
                            alert("Only image files uploading supported");
                        }
                    }
                }else{
                    that.sendMessageToDevice(messageText);
                }

			}else{
				that.ALERT_MESSAGES.showAlertMessage(that.$errMessage, "Please write message to send.", that.ALERT_MESSAGES.ERROR);
			}						    		
		});

		that.$btnInAppRefreshMessages.on("click", function(e){
			e.preventDefault();
			SHOULD_SHOW_LOADING = true;
			that.getSmsListFromServer();
		});
	},

	initTwilioMessages: function(){
		var that = this;

		$("#twilioDialogOpen").on("click", function(e){
			e.preventDefault();

			var twilioMessages = Object.create(TwilioMessages),
				patId = that.patientId,
				patPhoneNumber = $(this).data("phonenumber");

			//init twilio functionality
			twilioMessages.init(patId, patPhoneNumber);

			//sets Patient-Name to Span/Label
			$("#phoneNumberSpan").text($(this).data("patientname"));

			//open dialog
			$("#twilioMessageDialog").foundation("reveal", "open");
		});
	},

    //send message to device via server action
    sendMessageToDevice: function(messageText){
         var that = this,
             postRequestActions = {
                "loading": that.$loading,
                "error": that.$errMessage,
                "successCallBack": that.onSentMessage
            },
            latestMessage = that.SMS_LIST[that.SMS_LIST.length-1];

        var messageObject = {"messageText": messageText, "logId": that.logId};

        if(that.imagePath){
            messageObject.imagePath = that.imagePath;
        }

        var dataToSend = { "messageString" : JSON.stringify(messageObject), "patientId" : that.patientId, "broadcastMsg" : $("#chkSendBroadMsg").is(":checked")};

        if(latestMessage){
            messageObject = {"messageText": messageText, "logId": that.logId};
            if(that.imagePath){
                messageObject.imagePath = that.imagePath;
            }

            dataToSend = { "messageString" : JSON.stringify(messageObject), "patientId" : that.patientId, "latestMessageTime": latestMessage.timestamp, "broadcastMsg" : $("#chkSendBroadMsg").is(":checked")};
        }

        if(that.logId == "0"){
            that.imagePath = "";
        }

        that.AJAXING.sendPostRequest("sendMessageToDevice.action", dataToSend, postRequestActions);
    },

    //on success image uploaded then send message request to server and attached image path with it.
    onSuccessUploadImage: function(data){
        var that = window.coachMessagesObject;

        if(data && data.STATUS == "SUCCESS"){
            that.imagePath = data.DATA;
            $("#pFileAttached").hide();
            $("#uploadFile").val("");
            that.$btnSendMessage.removeProp("disabled").removeAttr("disabled").removeClass("disabled");
            that.sendMessageToDevice(that.$txtMessage.val());
        }
    },

	//on success message sent, do following
	onSentMessage: function(data){
		var that = window.coachMessagesObject;

		//clear local storage data saved in
		if(localStorage){
			localStorage.removeItem(LOCAL_STORAGE_MESSAGE_KEY);
		}

    	if(data.STATUS == "SUCCESS"){    		
    		that.$txtMessage.val("").change().trigger("keyup");
			$("#chkSendBroadMsg").prop("checked", false);
            $("#pFileAttached").hide();
            $("#uploadFile").val("");
            that.$btnSendMessage.removeProp("disabled").removeAttr("disabled").removeClass("disabled");
			SHOULD_SHOW_LOADING = false;
			that.getSmsListFromServer();
    	}else{
            that.$btnSendMessage.removeProp("disabled").removeAttr("disabled").removeClass("disabled");
            if(data.REASON){
                that.AJAXING.showErrorOrRedirect(data, that.$errMessage, data.REASON);
            }else{
              console.log("MESSAGE_SENDING_ERROR", data);
            }
    	}
    	
    	return that;
	},
	
	//bind auto grow plugin to textarea
	bindTextareaAutoGrow: function ($txtArea) {
        $txtArea.on("change keyup", function () {
            var value = $(this).val();
            if ($.trim(value).length > 0) {
                $(this).autosize().trigger("autosize.resize");
            } else {
                $(this).trigger("autosize.destroy");
            }
        }).trigger("change");
    },
    
    //gets sms list from server
    getSmsListFromServer: function(){
    	var that = window.coachMessagesObject,
			$loadingInner = "";

		//if shouldShowLoading flag true then assign
		if(SHOULD_SHOW_LOADING){
			$loadingInner = that.$loading;
		}
    	
    	var dataToSend = { "patientId" : that.patientId, "userId": that.userId, "shouldRead": that.fromLogDetail },
			postRequestActions = {    			
				"loading": $loadingInner,
				"error": that.$errMessage,
				"successCallBack": that.onGetMessagesList
			};    	
        	
    	that.AJAXING.sendPostRequest("getAllCoachPatientMessages.action", dataToSend, postRequestActions);
    },

	//gets providers list from server
	getProvidersListFromServer: function(){
		var that = window.coachMessagesObject;;

		var dataToSend = { "patientId" : that.patientId },
			postRequestActions = {
				"successCallBack": that.onGetProviderList
			};

		that.AJAXING.sendPostRequest("getAllProvidersByPatientId.action", dataToSend, postRequestActions);
	},

	onGetProviderList: function(data){
		var that = window.coachMessagesObject;;

		if(data && data.STATUS == "SUCCESS"){
			PROVIDERS_LISTING = $.parseJSON(data.DATA);
            if(PROVIDERS_LISTING && PROVIDERS_LISTING.length > 0){
                $("#spChatList").removeClass("visibility-hidden");
                that.$chatProviderList.fillChatProvidersList(PROVIDERS_LISTING, "userId", ["firstName", "lastName"], true);
            }
		}
	},

    //on success get messages list, do following
    onGetMessagesList: function(data){
		var that = window.coachMessagesObject;
    	
    	if(data){    		    			
    		that.buildThreadedView(data);
    	}else{    		    		    		    
    		if(data){    			
    			that.AJAXING.showErrorOrRedirect(data, that.$errMessage, "Unable to get messages. Please try again later.");
    		}    		
    	}
    	
    	return that;
	},
	
	//build threaded view of messages
	buildThreadedView: function(data){
		var that = window.coachMessagesObject,
			html = "";

        //if messages after refresh count is same as previous then don't refresh threaded view
        if(data && data.length == that.listCount){
            //return;
        }

		that.$threadedViewEmpty.show();

		if(data && data.length > 0){

            that.SMS_LIST = data;
            that.listCount = that.SMS_LIST.length;

			that.$threadedViewEmpty.hide();



			$.each(data, function(index, coachMessage){

				var foodLogSummary = coachMessage.foodLogSummary,
					foodImage = "",
					imagePath = "",
					messageBubble = (coachMessage.owner == "PATIENT") ? {bubble: "bubble bubble-teal", direction: "left"} : {bubble: "bubble bubble-orange", direction: "right"},
					excludeImage = "<div class='"+ messageBubble.bubble + "' >" + coachMessage.message + "</div>";

				//if food log summary found, then get foodImage from it.
				if(foodLogSummary){
					foodImage = foodLogSummary.foodImage;
				}

				if(coachMessage.imagePath){
					foodImage = {"imageName": coachMessage.imagePath};
				}

				if(foodImage){

					var fullImagePath = that.imagesPath + foodImage.imageName;

					excludeImage = "<div class='"+ messageBubble.bubble + "'>" +
									   "<div class='small-1 columns padding-lr-0'><img title='Click to enlarge' class='in-app-image' width='60' src='"+fullImagePath+"' /></div>" +
									   "<div class='small-11 columns'><div>" + coachMessage.message + "</div></div>" +
								   "</div>";
				}
				var notifiedUserName = "";
				//(coachMessage.userFirstName) ? coachMessage.userFirstName : " "
				if(coachMessage.repliedFirstUserName){
					notifiedUserName = ", " + coachMessage.repliedFirstUserName + " " + coachMessage.repliedLastUserName;
				}

				html += "<li class='"+ messageBubble.direction +"'><div class='bubble-wrapper'>" + excludeImage + "<span class='time'>"+ HSDateUtils.prettyDate(coachMessage.timestamp, "MM/DD/YYYY hh:mm A") + notifiedUserName + "</span></div></li>";
			});
			
			that.$threadViewList.empty().append(html).show();

            setTimeout(function(){
                that.scrollToBottom(that.$dialogArea);
            }, 100);

            //bind image opening in dialog
            $(".in-app-image").off("click").on("click", function(e){
                e.preventDefault();
                $("#inAppMessageDialogImage").prop("src", $(this).prop("src"));
                $("#inAppMessageDialog").foundation("reveal", "open");
            });
		}
	},
	
	//scroll to bottom of passing div
	scrollToBottom: function ($div) {
        if($div && $div.length > 0){
            $div.animate({ scrollTop: $div[0].scrollHeight }, 70);
        }
    }
};

/* A plugin to fill dropdown with passing array */
$.fn.fillChatProvidersList = function(values, valKey, textKey, addSelectText){
	var $dropdown = $(this).empty(),
		idList = [];

	$.each(values, function(val, obj) {

		idList.push(obj["userId"].toString());

		var fullName = obj[textKey];
		if($.isArray(textKey)){
			fullName = obj[textKey[1]] + ", " + obj[textKey[0]];
		}
		var $option = $("<option/>", {"value": obj[valKey], "text": fullName});
		$dropdown.append($option);
	});
	if(addSelectText){
		$dropdown.children(":first").before("<option value='' selected='selected' disabled>(select)</option>");
	}

	if(idList.indexOf(loggedIn_UserId) > -1){
		$dropdown.val(loggedIn_UserId);
	}else{
		$dropdown.val("");
	}

	return $dropdown;
};