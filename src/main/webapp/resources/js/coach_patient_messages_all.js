if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

var SHOULD_SHOW_LOADING = false;
//==========================================
/* CoachMessagesAll JS Model*/
var CoachMessagesAll = {

    init: function(appPath, patId, logId, imagePath){
        var that = this;

        that.patientId = patId;
        that.logId = "0";
        that.imagePath = "";
        that.appPath = appPath;
        that.imagesPath = that.appPath;
        that.SMS_LIST = [];
        that.shouldShowLoading = true;
        that.listCount = that.SMS_LIST.length;
        that.SORT_TYPE = "desc";
        window.coachMessagesObject = that;

        if(logId){
            that.logId = logId;
        }

        if(imagePath){
            that.imagePath = "/logImages/" + imagePath;
        }

        //alert message instance
        that.ALERT_MESSAGES = Object.create(AlertMessage).init();

        //ajaxing instance
        that.AJAXING = Object.create(Ajaxing);

        //cache page controls
        that.cachePageConstants();

        //bind event listener
        that.bindEventListener();

        that.getSmsListFromServer();

        //then sets refresh interval after each 50 seconds
        setInterval(function(){
            SHOULD_SHOW_LOADING = false;
            that.getSmsListFromServer();
        }, 50000);

        return that;
    },

    //cache page controls
    cachePageConstants: function(){

        var that = this;

        that.startDt = new Date();
        that.startDt.setHours(0, 0, 0, 0);

        that.endDt = new Date();
        that.endDt.setHours(23, 59, 59, 999);

        that.$threadedViewEmpty = $(".coach-thread-empty").show();
        that.$dialogArea = $("#coachThreadView");
        that.$threadViewList = $("#coachThreadViewList tbody").empty().hide();
        that.$errMessage = $("#errMessageCoach");
        that.$loading = $("#loading");
        that.$shadow = $("#coachThreadViewShadow").hide();
        that.$btnInAppRefreshMessages = $("#btnInAppRefreshMessages").off("click");
        that.messagesSearch = $("#messagesSearch").off("click").on('click', function(){ $(this).blur(); SHOULD_SHOW_LOADING = true; that.getSmsListFromServer();});
        that.messagesStartDate = $("#messagesStartDate").datepicker({maxDate: new Date(), changeYear: true}).val(HSDateUtils.formatDateFromTicks(that.startDt.getTime()));
        that.messagesEndDate = $("#messagesEndDate").datepicker({maxDate: new Date(), changeYear: true}).val(HSDateUtils.formatDateFromTicks(that.endDt.getTime()));
        that.$dateReceivedSort = $('.date-received-sort');
    },

    //gets sms list from server
    getSmsListFromServer: function(){
        var that = window.coachMessagesObject,
            $loadingInner = "";

        //if shouldShowLoading flag true then assign
        if(SHOULD_SHOW_LOADING){
            $loadingInner = that.$loading;
        }

        var startDate = new Date(that.messagesStartDate.val());
        startDate.setHours(0, 0, 0, 0);

        var endDate = new Date(that.messagesEndDate.val());
        endDate.setHours(23, 59, 59, 999);

        that.startDt = startDate.getTime();
        that.endDt = endDate.getTime();

        if ( that.startDt >= that.endDt ) {
            that.ALERT_MESSAGES.showAlertMessage(that.$errMessage, "Start date must be less than end date.", that.ALERT_MESSAGES.ERROR);
        } else {
            var dataToSend = {
                "patientId": that.patientId,
                "userId": that.userId,
                "messagesStartDate": that.startDt,
                "messagesEndDate": that.endDt
            },
            postRequestActions = {
                "loading": $loadingInner,
                "error": that.$errMessage,
                "successCallBack": that.onGetMessagesList
            };

            that.AJAXING.sendPostRequest("getAllCoachesPatientMessages.action", dataToSend, postRequestActions);
        }
    },

    //on success get messages list, do following
    onGetMessagesList: function(data){
        var that = window.coachMessagesObject;

        if(data){
            that.buildThreadedView(data);
        }else{
            if(data){
                that.AJAXING.showErrorOrRedirect(data, that.$errMessage, "Unable to get messages. Please try again later.");
            }
        }

        return that;
    },

    //bind event listener
    bindEventListener: function(){
        var that = window.coachMessagesObject;
        that.$btnInAppRefreshMessages.on("click", function(e){
            e.preventDefault();
            SHOULD_SHOW_LOADING = true;
            that.getSmsListFromServer();
        });

        that.$dateReceivedSort.off("click").on("click", function(){
           var $this = $(this),
               $img = $this.find("img"),
               sortAscDesc = $this.attr("data-sort");

            if(sortAscDesc == "asc"){
                $this.attr("data-sort", "desc");
                $img.attr("src", that.appPath + "/resources/css/images/sort_desc.png");
            }else{
                $this.attr("data-sort", "asc");
                $img.attr("src", that.appPath + "/resources/css/images/sort_asc.png");
            }

            that.SORT_TYPE = sortAscDesc;
            that.SMS_LIST.sort(that.sortByDate);
            that.buildThreadedView(that.SMS_LIST);
        });
    },

    //build threaded view of messages
    buildThreadedView: function(data){
        var that = window.coachMessagesObject,
            html = "";

        //if messages after refresh count is same as previous then don't refresh threaded view
        if(data && data.length == that.listCount){
            //return;
        }

        that.$threadedViewEmpty.show();
        $("#coachThreadViewList").addClass("hide");

        if(data && data.length > 0){

            that.SMS_LIST = data;

            that.$threadedViewEmpty.hide();
            html = "";

            $.each(data, function(index, coachMessage){

                var foodLogSummary = coachMessage.foodLogSummary,
                    foodImage = "",
                    fullImagePath = "";

                //if food log summary found, then get foodImage from it.
                if(foodLogSummary){
                    foodImage = foodLogSummary.foodImage;
                }

                if(coachMessage.imagePath){
                    foodImage = {"imageName": coachMessage.imagePath};
                }

                if(foodImage){
                    fullImagePath = that.imagesPath + foodImage.imageName;
                }

                var coachName = (coachMessage.owner == 'PROVIDER') ? coachMessage.providerName : "N/A",
                    mealMessageYN = (coachMessage.isMealMessage) ? "Yes" : "No",
                    fromOrTo = "<td><span class='status member'>Member</span></td><td>"+coachMessage.providerName+"</td>",
                    mealMessageHTML = "<td>" + coachMessage.message + "</td>";

                if(coachMessage.owner == 'PROVIDER'){
                    fromOrTo = "<td>"+coachName+"</td><td><span class='status member'>Member</span></td>";
                }

                if(mealMessageYN == "Yes" && foodImage){
                    mealMessageHTML = "<td><img title='Click to enlarge' class='in-app-image' width='60' src='"+fullImagePath+"' />  "+coachMessage.message + "</td>";
                }

                html += "<tr>" + mealMessageHTML+
                        "<td>"+HSDateUtils.prettyDate(coachMessage.timestamp, "MM/DD/YYYY hh:mm A")+"</td>" +
                        fromOrTo + "<td>"+mealMessageYN+"</td></tr>";
            });

            $("#coachThreadViewList").removeClass("hide");
            that.$threadViewList.empty().append(html).show();

            //bind image opening in dialog
            $(".in-app-image").off("click").on("click", function(e){
                e.preventDefault();
                $("#inAppMessageDialogImage").prop("src", $(this).prop("src"));
                $("#inAppMessageDialog").foundation("reveal", "open");
            });
        }
    },

    sortByDate: function (a, b){
        var that = window.coachMessagesObject;

        if(that.SORT_TYPE == "desc"){
            return new Date(a.timestamp) - new Date(b.timestamp);
        }

        return new Date(b.timestamp) - new Date(a.timestamp);
    }
};
