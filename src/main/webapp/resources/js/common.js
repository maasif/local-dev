var SUCCESS = "success",
	ERROR = "error",
	AJAX_ERROR_MESSAGE = "Unable to connect to server, please check your internet settings and try again. If the problem persists please contact Customer Service at <a href='info@healthslate.com' style='color:yellow'>info@healthslate.com</a> or (888) 291-7245 extension 1.";

function nullSafeJS(v){
    return (v) ? v : "N/A";
}

function formatPhoneNumber($phoneLabel, phoneText){
    if(phoneText || $phoneLabel){
        var countryCode = phoneText.indexOf("+1");

        if(countryCode > -1){
            phoneText = phoneText.substring(countryCode+3, phoneText.length);
        }

        var areaCode = phoneText.substring(0, 2),
            maskedFormat = "(000) 000-0000";

        if(areaCode == "+9"){
            //maskedFormat = "+00 (000) 000-0000";
        }

		if($.fn.mask){
			$phoneLabel.text($.trim(phoneText)).val($.trim(phoneText)).mask(maskedFormat);
		}
    }
}

function formatZipCode($zipLabel, zipText){
    if(zipText || $zipLabel){
        var maskedFormat = "00000-0000";
        if(zipText == "N/A"){
            $zipLabel.text($.trim(zipText));
        }else{
            $zipLabel.text($.trim(zipText)).val($.trim(zipText)).mask(maskedFormat);
        }
    }
}

function redirectToLogin($error, data, errorText){	
	data = $.trim(data);
	if(data.toLowerCase().indexOf("<!doctype" > -1)){
		window.location = "login.jsp";
	}else{
		showPermanentAlertMessage($error, errorText, ERROR);
	}
}

function getAlertMessage($message, alertType){
	if(alertType == SUCCESS){
		$message.removeClass(ERROR).addClass(SUCCESS).show();
	}else{
		$message.removeClass(SUCCESS).addClass(ERROR).show();
	}

	return $message;
}

function showPermanentAlertMessage($message, errorMessage, alertType){		
	var $m = getAlertMessage($message, alertType);
	
	$m.html(errorMessage).append("<a href='#' class='close-alert'>&times;</a>");
	
	$("a.close-alert").off("click").on("click", function(e){
		e.preventDefault();
		$m.fadeOut(500);
	});
}

function showAlertMessage($message, errorMessage, alertType){
	
	var timer = null;
	
	clearTimeout(timer);
	
	var $m = getAlertMessage($message, alertType);
	$m.html(errorMessage);
	
	timer = setTimeout(function(){
		$m.fadeOut(500);
	}, 3000);	
}

function sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions){		
	
  var $loadingSpinner = null,
  	  $error = null;
	
  if(postRequestActions.error){
	  $error = postRequestActions.error;
	  $error.hide();
  }
  
  if(postRequestActions.loading){	  
	  $loadingSpinner = postRequestActions.loading;
	  $loadingSpinner.show();
  }
  
   $.ajax({
		type : postRequestActions.requestType,
		url : actionUrl,
		data : dataToSend,				
		contentType : "application/javascript",				
		success : function(data) {											
			
			if($loadingSpinner){
				$loadingSpinner.hide();
			}
			
			if($error){
			    $error.hide();
			}
			
			try{
				if(data != "")
					data = JSON.parse(data);						
			}catch(err){
				console.log(err.message);
			}
			
			var callBack = postRequestActions.successCallBack;
			if(callBack){				
				callBack(data);
			}					
		},				
		error : function(data, status) {			
						
			if($loadingSpinner){
				$loadingSpinner.hide();
			}

            console.log("ERROR", status);

			if($error){
                showErrorOrRedirect(data, $error);
            }
		}
	});	    	  
}

function showErrorOrRedirect(data, $error){
    data = $.trim(data);
    if(data.toLowerCase().indexOf("<!doctype") > -1){
        window.location = "login.jsp";
    }else{
        showPermanentAlertMessage($error, AJAX_ERROR_MESSAGE, ERROR);
    }
}

function getQuerystringParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);

    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function bindSwitch($switch, manualSelect){
	
	var switchContent = $switch.data("content"),
		$switchBtns = $switch.find("a");			
	
	
	
	$switchBtns.on("click", function (e){		
		e.preventDefault();					
		
		if(switchContent){
			$("."+switchContent).hide();
		}
		
		$switchBtns.addClass("secondary").removeClass("category-selected");
		
		var $this = $(this).removeClass("secondary").addClass("category-selected"),		
			elem = $this.data("attached-elem");
		
		if(elem){
			$("#"+elem).show();
		}		
	});
	
	if(manualSelect){
		$switchBtns.eq(0).click();
	}
}

function isAnyOrAllChecked($chkBoxes, isAny){
	var result = $chkBoxes.filter(function(){
		if(isAny){ // true means any checked, else means all checked
			return this.checked;
		}else{
			return !this.checked;
		}		
	});
						
	return (result.length === 0);
}

function getCommaSeparatedString($source, byFilter){
	var commaSeparated = $source.map(function(){
		if(byFilter){
			if(this.checked){
				return this.value;
			}
		}else{
			return this.value;
		}
		
	}).get().join(",");
	
	return commaSeparated;
}

function initDatePicker(id, path, maxToday){
	if($.fn.datepicker){ // if plugin available then call, in case when .js file is not included then prevent console error
		
		var calPath = "",
			$ctrl = $("#"+id),
			calId = $ctrl.data("calid");
		
		if(path){
			calPath  = path+"/resources/images/calendar.gif";
			$ctrl.datepicker({
				 showOn: 'both',
				 buttonImage: calPath,
				 buttonImageOnly: false,
				 changeMonth: true,
				 changeYear: true,
				 showAnim: 'slideDown',
				 duration: 'fast'
			});
		}
		else{
			$ctrl.datepicker({
				 showOn: 'both',
				 buttonImageOnly: true,
				 changeMonth: true,
				 changeYear: true,
				 yearRange: "-100:+0",
				 showAnim: 'slideDown',
				 duration: 'fast',
				 maxDate: ((maxToday) ? new Date() : null)
			});
			
			$(".ui-datepicker-trigger").hide();
		}
		
		
		if(calId){
			$("#"+calId).off("click").on("click", function(e){
				e.preventDefault();
				var $div = $("#ui-datepicker-div");
				$(".ui-datepicker-year").val();
				
				if($div.is(":visible")){
					$ctrl.datepicker("hide");
				}else{
					$ctrl.datepicker("show");
				}
				
			});
		}		
	}
}

function centerAlignPagination(){
	
	var $pagination = $('.spanPagination'),	
	    width = $pagination.width() / 2;
	
	$pagination.css('margin-left',-1*width+"px");
}

function initDataTable(id, options){

	var $table = $('#'+id); 
	$table.dataTable({
		"sDom" : "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'spanPagination'p>>",
		"sPaginationType" : "bootstrap",
		"aaSorting": options.sorting ,
		"oLanguage" : {
			"sLengthMenu" : "_MENU_ records per page"
		},
        "fnDrawCallback": function( oSettings ) {
            $(".sorting_1").removeClass("sorting_1 sorting_1");

            if(options.callback){
                options.callback();
            }
        }
	});
	
	var $ul = $(".dataTables_paginate ul");
	
	$ul.each(function(){
		var $this = $(this);
		if($this && !$this.hasClass("pagination")){
			$this.addClass("pagination");
		}
	});
	
	centerAlignPagination();
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}

function goBack() {
    window.history.back()
}

function getThreeDaysDatesArray(date){
	var rangeDatesArray = [];
	var firstDate = date, secondDate = date, thirdDate = date;
	firstDate.setDate(firstDate.getDate());
	firstDate = $.datepicker.formatDate('mm/d/yy', firstDate);
	secondDate.setDate(secondDate.getDate() + 1);
	secondDate = $.datepicker.formatDate('mm/d/yy', secondDate);
	thirdDate.setDate(thirdDate.getDate() + 1);
	thirdDate = $.datepicker.formatDate('mm/d/yy', thirdDate);

	rangeDatesArray[0] = firstDate;
	rangeDatesArray[1] = secondDate;
	rangeDatesArray[2] = thirdDate;

	return rangeDatesArray;
}
