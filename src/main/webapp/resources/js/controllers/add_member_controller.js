(function(){

    var app = angular.module("AddMemberModule", []);

    app.directive('datepicker', function() {
        return {
            restrict: 'A',
            require : 'ngModel',
            link : function (scope, element, attrs, ngModelCtrl) {
                $(function(){
                    element.datepicker({
                        dateFormat:'mm/dd/yy',
                        changeMonth: true,
                        changeYear: true,
                        yearRange: "-100:+0",
                        onSelect:function (date) {
                            scope.$apply(function () {
                                ngModelCtrl.$setViewValue(date);
                            });
                        }
                    });
                });
            }
        }
    });

    app.controller("AddMemberController", ["$scope", "$http", function($scope, $http){

        //storing in page level object
        $scope.patient = {};
        $scope.facilityList = FACILITY_LIST;
        $scope.coachesListAll = [];
        $scope.patientFacilityId = PATIENT_FACILITY_ID;
        $scope.patientFacilityName = PATIENT_FACILITY_NAME;

        //build coaches list facility wise
        $.each(COACHES_LIST, function(index, coach){
            if(!$scope.coachesListAll[coach.facilityId]){
                $scope.coachesListAll[coach.facilityId] = [];
                $scope.coachesListAll[coach.facilityId].push(coach);
            }else{
                $scope.coachesListAll[coach.facilityId].push(coach);
            }
        });

        var $ddFacility = $("#ddFacility");
        $scope.coachesList = [];
        $scope.primaryCoachesList = [];
        if($ddFacility && $ddFacility.length == 1){
            $scope.aToZFacilityId = ATOZ_FACILITY_ID;
        } else {
            $scope.coachesList = COACHES_LIST;
            $scope.primaryCoachesList = PRIMARY_COACHES_LIST;
        }

        $scope.aToZFacilityId = ATOZ_FACILITY_ID;

        $scope.getFacilityCoaches = function(fac){
            $scope.coachesList = $scope.coachesListAll[fac.facilityId];
            $scope.foodCoachesList =  $scope.getFoodCoaches($scope.coachesList);
            $scope.superFacilityCoachesList = $scope.coachesListAll[$scope.aToZFacilityId];
            $scope.primaryCoachesList = $.merge( $.merge( [], $scope.superFacilityCoachesList ), $scope.foodCoachesList );
            $scope.patient.facility = fac;
            FACILITY_NAME = fac.name;
        };

        $scope.getFoodCoaches = function(coachesList){
            var foodCoaches = [];
            if(coachesList){
                foodCoaches = $.map(coachesList, function(coach, index){
                    if(coach && coach.type == "Food Coach"){
                        return coach;
                    }
                });
            }
            return foodCoaches;
        };

        $scope.getCoaches = function(coachesList){
            var coaches = [];
            if(coachesList){
                coaches = $.map(coachesList, function(coach, index){
                    if(coach && coach.type == "Coach"){
                        return coach;
                    }
                });
            }
            return coaches;
        };

        //storing for showing required fields label when user submits
        $scope.submitted = false;
        $scope.stateList = ["AL", "AK", "AS", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FM", "FL", "GA", "GU", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MH", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "MP", "OH", "OK", "OR", "PW", "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VI", "VA", "WA", "WV", "WI", "WY"];
        $scope.uuid = UUID;

        if($scope.uuid){
            $scope.patient.email = EMAIL;
            $scope.patient.firstName = FIRST_NAME;
            $scope.patient.lastName = LAST_NAME;
            $scope.patient.dobString = DOB;
            $scope.patient.mrn = MRN;

            if($scope.patientFacilityId && $scope.patientFacilityName){
                var facility = {facilityId:$scope.patientFacilityId, name:$scope.patientFacilityName};
                $scope.patient.facility = facility;
                $scope.getFacilityCoaches(facility);
            }
        }

        //submit form function
        $scope.submitForm = function(patient) {
            $scope.submitted = true;

            var $txtMRN = $("#txtMRN"),
                $txtConfirmMRN = $("#txtConfirmMRN"),
                $errorTeam = $("#errorMemberTeam"),
                $errorTeamCoach = $("#errorMemberTeamCoach"),
                $errorMemberTeam = (!$errorTeam.is(":visible"))? $errorTeam.addClass("hide"): $errorTeam.removeClass("hide"),
                selectedProviders = $(".selected-providers:checked").map(function(){
                    return $(this).val();
                }).get().join(", "),
                commaSeparatedSuperFacilityCoaches = $.map($scope.primaryCoachesList, function(obj, index){
                    if(obj.facilityId === parseInt($scope.aToZFacilityId)){
                        return obj.providerId;
                    }
                }).join(", ");

            $txtConfirmMRN.next("small").hide();

            if($.trim($txtMRN.val()).length > 0 && $txtMRN.val() != $txtConfirmMRN.val()){
                $txtConfirmMRN.next("small").show();
                return;
            }

            if(!selectedProviders){
                $errorMemberTeam.removeClass("hide");
                return;
            }

            if(selectedProviders.indexOf(patient.leadCoach.providerId) < 0 ){
                $errorTeamCoach.removeClass("hide");
                return;
            }

            if(patient.primaryCoach
                && patient.primaryCoach.providerId
                && selectedProviders.indexOf(patient.primaryCoach.providerId) < 0
                && commaSeparatedSuperFacilityCoaches.indexOf(patient.primaryCoach.providerId) < 0 ){

                $errorTeamCoach.removeClass("hide");

                return;
            }

            $errorTeamCoach.addClass("hide");
            $errorMemberTeam.addClass("hide");

            //if form is valid then process
            if($scope.formAddMember.$valid){

                var ajaxing = Object.create(Ajaxing),
                    $loading = $("#loading").show(),
                    $error = $("#errMessage"),
                    alertMessages = Object.create(AlertMessage).init(),
                    selectedFacId = (!$scope.uuid && patient.facility) ? patient.facility.facilityId : undefined,
                    dateString = patient.dobString,
                    selectedCoachId = (patient.leadCoach) ? patient.leadCoach.providerId : undefined,
                    primaryCoachId = (patient.primaryCoach) ? patient.primaryCoach.providerId : undefined;

                patient.dobString = dateString;
                $errorMemberTeam.addClass("hide");

                if(patient.facility && patient.facility.facilityId){
                    patient.facility = patient.facility.name;
                }else{
                    patient.facility = FACILITY_NAME;
                }

                if($scope.uuid){
                    patient.callFrom = "ADD_REQUESTED_MEMBER";
                    patient.phoneNumber = PHONE;
                    patient.leadCoach = selectedCoachId;
                    patient.selectedProviders = selectedProviders;
                    patient.primaryCoach = primaryCoachId;

                }else{
                    patient.isInvitation = true;
                    patient.leadCoach = selectedCoachId;
                    patient.primaryCoach = primaryCoachId;
                    patient.selectedProviders = selectedProviders;
                }

                var patObjectJson = angular.toJson(patient);

                if(!$scope.uuid){
                   $scope.patient.facility = {"facilityId": selectedFacId, "name": FACILITY_NAME};
                }

                $scope.patient.leadCoach = {"providerId": selectedCoachId };
                $scope.patient.primaryCoach = {"providerId": primaryCoachId };
                $scope.patient.selectedProviders = selectedProviders;

                $http({
                    url: BASE_URL + "services/signUp/patientSignUp",
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    data: patObjectJson
                }).success(function(data){

                    data = angular.fromJson(data);
                    $loading.hide();

                    if(data.STATUS == "SUCCESS"){

                        $scope.submitted = false;
                        $loading.hide();
                        alertMessages.showAlertMessage($error, "Member is added successfully.", alertMessages.SUCCESS);
                        window.location = "prospectiveMembers.action";
                        $scope.patient = {};

                    } else {

                        if(!$scope.uuid){
                            $scope.patient.facility = patient.facility;
                        }

                        $scope.patient.leadCoach = {"providerId": selectedCoachId};
                        $scope.patient.primaryCoach = {"providerId": primaryCoachId };
                        $scope.patient.dobString = dateString;

                        if ($scope.patient.gender) {
                            if ($scope.patient.gender == "Male") {
                                $scope.patient.gender = "Male";
                            } else {
                                $scope.patient.gender = "Female";
                            }
                        }

                        if(data.REASON == "MRN already assigned"){
                        alertMessages.showAlertMessage($error, "This MRN is already assigned.", alertMessages.ERROR);
                    } else {
                        alertMessages.showAlertMessage($error, "This email address is already registered.", alertMessages.ERROR);
                    }
                }
                }).error(function(err){
                    $loading.hide();

                    if(!$scope.uuid){
                        $scope.patient.facility = patient.facility;
                    }

                    $scope.patient.leadCoach = {"providerId": selectedCoachId };
                    $scope.patient.primaryCoach = {"providerId": primaryCoachId };
                    $scope.patient.dobString = dateString;

                    if($scope.patient.gender && $scope.patient.gender == "Male"){
                        $scope.patient.gender = "Male";
                    }else {
                        $scope.patient.gender = "Female";
                    }

                    if(err){
                        alertMessages.showAlertMessage($error, AJAX_ERROR_MESSAGE, alertMessages.ERROR);
                    }else{
                        //to be on save side handling gateway error, because member is already created
                        $scope.submitted = false;
                        $loading.hide();
                        alertMessages.showAlertMessage($error, "Member is added successfully.", alertMessages.SUCCESS);
                        window.location = "prospectiveMembers.action";
                        $scope.patient = {};
                    }
                });
            }
        };
    }]);
})();