(function(){

    var app = angular.module("InviteMembersModule", []);

    app.controller("InviteMembersController", ["$scope", "$http", function($scope, $http){

        //storing in page level object
        $scope.patient = {};

        //storing for showing required fields label when user submits
        $scope.submitted = false;

        //submit form function
        $scope.submitForm = function(patient) {

            $scope.submitted = true;

            //if form is valid then process
            if($scope.formInviteMember.$valid){

                var ajaxing = Object.create(Ajaxing),
                    $loading = $("#loading").show(),
                    $error = $("#errMessage"),
                    alertMessages = Object.create(AlertMessage).init();

                patient.facility = "Providence";
                if(FACILITY_NAME){
                    patient.facility = FACILITY_NAME;
                }

                patient.isInvitation = true;

                $http({
                    url: BASE_URL + "services/signUp/patientSignUp",
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    data: angular.toJson(patient)
                }).success(function(data){
                    data = angular.fromJson(data);
                    if(data.STATUS == "SUCCESS"){
                        $loading.hide();
                        $scope.patient = {};
                        $scope.submitted = false;
                        alertMessages.showAlertMessage($error, "Invitation sent successfully.", alertMessages.SUCCESS);

                        setTimeout(function(){
                            window.location = "prospectiveMembers.action";
                        }, 500);

                    }else{
                        $loading.hide();
                        alertMessages.showAlertMessage($error, "This email address is already registered.", alertMessages.ERROR);
                    }
                }).error(function(err){
                    $loading.hide();
                    alertMessages.showAlertMessage($error, AJAX_ERROR_MESSAGE, alertMessages.ERROR);
                });
            }
        };
    }]);
})();