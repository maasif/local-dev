(function(){

    var app = angular.module("JoinProgramModule", []);

    //workaround to fix controller binding in IE8/IE9
    if($.browser.msie && browserVersion <= 9) {
        angular.element(document).ready(function () {
            angular.bootstrap(document, ['JoinProgramModule']);
        });
    }

    app.directive('datepicker', function() {
        return {
            restrict: 'A',
            require : 'ngModel',
            link : function (scope, element, attrs, ngModelCtrl) {
                $(function(){
                    element.datepicker({
                        dateFormat:'mm/dd/yy',
                        changeMonth: true,
                        changeYear: true,
                        yearRange: "-100:+0",
                        maxDate: new Date(),
                        onSelect:function (date) {
                            scope.$apply(function () {
                                ngModelCtrl.$setViewValue(date);
                            });
                        }
                    });
                });
            }
        }
    });

    //for fixing placeholder in IE8/IE9
    app.directive('placeholder', function($timeout){
        if (!$.browser.msie || $.browser.version >= 10) {
            return {};
        }
        return {
            link: function(scope, elm, attrs){
                if (attrs.type === 'password') {
                    return;
                }
                $timeout(function(){
                    elm.val(attrs.placeholder).focus(function(){
                        if ($(this).val() == $(this).attr('placeholder')) {
                            $(this).val('');
                        }
                    }).blur(function(){
                        if ($(this).val() == '') {
                            $(this).val($(this).attr('placeholder'));
                        }
                    });
                });
            }
        }
    });

    app.controller("JoinProgramController", ["$scope", "$http", function($scope, $http){

        //storing in page level object
        $scope.patient = {};

        //storing for showing required fields label when user submits
        $scope.submitted = false;

        //submit form function
        $scope.submitForm = function(patient) {

            $scope.submitted = true;

            //if form is valid then process
            if($scope.formJoinProgram.$valid){

                var ajaxing = Object.create(Ajaxing),
                    $loading = $("#loading").show(),
                    $error = $("#errMessage"),
                    alertMessages = Object.create(AlertMessage).init();

                patient.facility = "Providence";
                if(FACILITY_NAME){
                    patient.facility = FACILITY_NAME;
                }

                patient.callFrom = "ENROLL_FORM";
                $http({
                    url: BASE_URL + "services/signUp/patientSignUp",
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    data: angular.toJson(patient)
                }).success(function(data){
                    data = angular.fromJson(data);
                    if(data.STATUS == "SUCCESS"){
                        $scope.submitted = false;
                        window.location = "ThankyouForRequest.action?assignedFacility="+assignedFacility;
                        $scope.patient = {};
                    }else{
                        $loading.hide();
                        alertMessages.showAlertMessage($error, "This email address is already registered.", alertMessages.ERROR);
                    }
                }).error(function(err){
                    $loading.hide();

                    if(err){
                        alertMessages.showAlertMessage($error, AJAX_ERROR_MESSAGE, alertMessages.ERROR);
                    }else{
                        //to be on save side handling gateway error, because request is submitted
                        $scope.submitted = false;
                        window.location = "ThankyouForRequest.action?assignedFacility="+assignedFacility;
                        $scope.patient = {};
                    }
                });
            }
        };
    }]);
})();