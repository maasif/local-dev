(function(){

    var app = angular.module("ProviderPatientModule", []);

    app.controller("ProviderPatientController", ["$scope", "$http", function($scope, $http){

        $scope.typeList = [{ value: "PROVIDER", text:"Provider"}, { value: "NURSE", text:"Nurse"}, { value: "COACH", text:"Coach"}];

        $scope.patientList = [];
        $scope.providerList = PROVIDER_LIST;

        $scope.providerId = -1;
        $scope.patientId = -1;
        $scope.isLeadCoach = false;

        //storing for showing required fields label when user submits
        $scope.submitted = false;

        $scope.openProviderDialog = function(pat){
            $scope.patientId = pat.patientId;
            $scope.refreshProviderList(pat.patientId);
            $("#providerListDialog").foundation("reveal", "open");
        };

        $scope.refreshProviderList = function(patientId){
            $("#txtHiddenPatientId").val(patientId);

            $http({
                url: "getProviderListByPatientId.action",
                data: {"patientId": patientId},
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                }
            }).success(function(data){
                //TODO: I don't know why its happening so i am double parsing it to convert to jsonObject array, __oz, change it later
                $scope.providerList = $.parseJSON($.parseJSON(data));
            }).error(function(err){	});
        };
        // String to date
        $scope.convertToDate = function (stringDate){
            return new Date(stringDate).getTime();
        };

        //submit form function
        $scope.saveLeadCoach = function(providerId) {

            var ajaxing = Object.create(Ajaxing),
                $loading = $("#loading").show(),
                $error = $("#errMessage"),
                alertMessages = Object.create(AlertMessage).init();

            $http({
                url: "saveLeadCoach.action",
                method: 'POST',
                data: { "patientId": $("#txtHiddenPatientId").val(), "providerId" : providerId },
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                }
            }).success(function(data){
                data = angular.fromJson(data);
                if(data.STATUS == "SUCCESS" || data.indexOf("SUCCESS") > -1){
                    $loading.hide();
                    alertMessages.showAlertMessage($error, "Data saved successfully.", alertMessages.SUCCESS);
                    $("#providerListDialog").foundation("reveal", "close");
                }else{
                    $loading.hide();
                    ajaxing.showErrorOrRedirect(data, $error, AJAX_ERROR_MESSAGE);
                }
            }).error(function(err){
                $loading.hide();
                ajaxing.showErrorOrRedirect(err, $error, AJAX_ERROR_MESSAGE);
            });
        };

        //submit form function
        $scope.savePrimaryFoodCoach = function(providerId) {

            var ajaxing = Object.create(Ajaxing),
                $loading = $("#loading").show(),
                $error = $("#errMessage"),
                alertMessages = Object.create(AlertMessage).init();

            $http({
                url: "savePrimaryFoodCoach.action",
                method: 'POST',
                data: { "patientId": $("#txtHiddenPatientId").val(), "providerId" : providerId },
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                }
            }).success(function(data){
                data = angular.fromJson(data);
                if(data.STATUS == "SUCCESS" || data.indexOf("SUCCESS") > -1){
                    $loading.hide();
                    alertMessages.showAlertMessage($error, "Data saved successfully.", alertMessages.SUCCESS);
                    $("#providerListDialog").foundation("reveal", "close");
                }else{
                    $loading.hide();
                    ajaxing.showErrorOrRedirect(data, $error, AJAX_ERROR_MESSAGE);
                }
            }).error(function(err){
                $loading.hide();
                ajaxing.showErrorOrRedirect(err, $error, AJAX_ERROR_MESSAGE);
            });
        };

    }]);

})();