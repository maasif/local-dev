(function(){

    var app = angular.module("ProspectiveMembersModule", []);

    app.controller("ProspectiveMembersController", ["$scope", "$http", function($scope, $http){

        $scope.typeList = [{ value: "PROVIDER", text:"Provider"}, { value: "NURSE", text:"Nurse"}, { value: "COACH", text:"Coach"}];

        $scope.patientList = [];
        $scope.providerList = [];

        $scope.providerId = -1;
        $scope.patientId = -1;
        $scope.isLeadCoach = false;

        //storing for showing required fields label when user submits
        $scope.submitted = false;

        $scope.openProviderDialog = function(patientId){
            $scope.refreshProviderList(patientId);
            $("#providerListDialog").foundation("reveal", "open");
        };

        $scope.refreshProviderList = function(patientId){
            $("#txtHiddenPatientId").val(patientId);
            $http({
                url: "getProviderListByPatientId.action",
                data: {"patientId": patientId},
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                }
            }).success(function(data){
                //TODO: I don't know why its happening so i am double parsing it to convert to jsonObject array, __oz, change it later
                $scope.providerList = $.parseJSON($.parseJSON(data));
            }).error(function(err){	});
        };
        // String to date
        $scope.convertToDate = function (stringDate){
            return new Date(stringDate).getTime();
        };

        //submit form function
        $scope.saveLeadCoach = function(providerId) {

            var ajaxing = Object.create(Ajaxing),
                $loading = $("#loading").show(),
                $error = $("#errMessage"),
                alertMessages = Object.create(AlertMessage).init();

            $http({
                url: "saveLeadCoachRecruitment.action",
                method: 'POST',
                data: { "patientId": $("#txtHiddenPatientId").val(), "providerId" : providerId },
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                }
            }).success(function(data){
                data = angular.fromJson(data);
                $("#providerListDialog").foundation("reveal", "close");
                if(data.STATUS == "SUCCESS" || data.indexOf("SUCCESS") > -1){
                    $loading.hide();
                    alertMessages.showAlertMessage($error, "Data saved successfully.", alertMessages.SUCCESS);
                    setTimeout(function(){
                        location.reload(true);
                    }, 500);
                }else{
                    $loading.hide();
                    ajaxing.showErrorOrRedirect(data, $error, AJAX_ERROR_MESSAGE);
                }
            }).error(function(err){
                $loading.hide();
                $("#providerListDialog").foundation("reveal", "close");
                ajaxing.showErrorOrRedirect(err, $error, AJAX_ERROR_MESSAGE);
            });
        };

        //submit form function
        $scope.savePrimaryFoodCoach = function(providerId) {

            var ajaxing = Object.create(Ajaxing),
                $loading = $("#loading").show(),
                $error = $("#errMessage"),
                alertMessages = Object.create(AlertMessage).init();

            $http({
                url: "savePrimaryFoodCoachRecruitment.action",
                method: 'POST',
                data: { "patientId": $("#txtHiddenPatientId").val(), "providerId" : providerId },
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                }
            }).success(function(data){
                data = angular.fromJson(data);
                $("#providerListDialog").foundation("reveal", "close");
                if(data.STATUS == "SUCCESS" || data.indexOf("SUCCESS") > -1){
                    $loading.hide();
                    alertMessages.showAlertMessage($error, "Data saved successfully.", alertMessages.SUCCESS);
                    setTimeout(function(){
                        location.reload(true);
                    }, 500);
                }else{
                    $loading.hide();
                    ajaxing.showErrorOrRedirect(data, $error, AJAX_ERROR_MESSAGE);
                }
            }).error(function(err){
                $("#providerListDialog").foundation("reveal", "close");
                $loading.hide();
                ajaxing.showErrorOrRedirect(err, $error, AJAX_ERROR_MESSAGE);
            });
        };

    }]);

})();