(function(){
        	
	var app = angular.module("ResetPasswordModule", []);
	
	app.controller("ResetPasswordController", ["$scope", "$http", function($scope, $http){

		//storing in page level object
		$scope.resetPassword = {};

		//storing for showing required fields label when user submits
		$scope.submitted = false;

		$scope.clearForm = function(){
			$scope.resetPassword = {};
			$scope.submitted = false;
		};

		//submit form function
		$scope.submitForm = function(resetPassword) {
			
			$scope.submitted = true;
						
			//validate both fields
			var $password = $("#txtPassword"),
				$repeatPassword = $("#txtRepeatPassword");
						
			if($password.val() != $repeatPassword.val()){
				return;
			}
						
			//if form is valid then process
			if($scope.resetPasswordForm.$valid){

				var ajaxing = Object.create(Ajaxing),
        			$loading = $("#loading").show(),
					$error = $("#errMessage"),
					alertMessages = Object.create(AlertMessage).init();

        		$http({
        	        url: "setPasswordForced.action",
        	        method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					data: {"password": resetPassword.password,
						   "userName": $("#txtHiddenUser").val(),
						   "token": $("#txtHiddenToken").val(),
						   "pinCode": resetPassword.pinCode
					},
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					transformRequest: function(obj) {
						var str = [];
						for(var p in obj)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
						return str.join("&");
					}
        	    }).success(function(data){		        	    	
        	    	data = angular.fromJson(angular.fromJson(data));
					$loading.hide();
        	    	if(data.STATUS == "SUCCESS"){
						alertMessages.showAlertMessage($error, "Password reset successfully. You will be redirected to login screen in 5 seconds.", alertMessages.SUCCESS);
						$scope.clearForm();

						setTimeout(function(){
							window.location = "login";
						}, 4000);
        	    	}else{
                        $scope.submitted = false;
						ajaxing.showErrorOrRedirect(data, $error, data.REASON);
                        clearPasswordsOnFocus();
        	    	}
        	    }).error(function(err){
                    $scope.submitted = false;
        	    	$loading.hide();
        	    	ajaxing.showErrorOrRedirect(err, $error, AJAX_ERROR_MESSAGE);
                    clearPasswordsOnFocus();
        	    });	   
			}        			        
		};
	}])
})();