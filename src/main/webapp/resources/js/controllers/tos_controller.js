(function(){

    var app = angular.module("TOSModule", []);

    //workaround to fix controller binding in IE8/IE9
    angular.element(document).ready(function () {
        angular.bootstrap(document, ['TOSModule']);
    });

    app.controller("TOSController", ["$scope", "$http", function($scope, $http){

        //storing in page level object
        $scope.patient = PAT_OBJECT;

        //storing for showing required fields label when user submits
        $scope.submitted = false;

        //submit form function
        $scope.submitForm = function(patient) {

            $scope.submitted = true;

            //if form is valid then process
            if($scope.tosForm.$valid){

                $("#btnSubmit").addClass("disabled").off("click");

                var ajaxing = Object.create(Ajaxing),
                    $loading = $("#loading").show(),
                    $error = $("#errMessage"),
                    alertMessages = Object.create(AlertMessage).init();

                if(window.console){
                    console.log(patient);
                }

                $http({
                    url: "saveParticipant.action" ,
                    method: 'POST',
                    data: { "userData" : angular.toJson(patient)},
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    transformRequest: function(obj) {
                        var str = [];
                        for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    }
                }).success(function(data){
                    data = angular.fromJson(angular.fromJson(data));
                    if(data.STATUS == "SUCCESS"){
                        window.location = "GetHealthSlateApp.action?token="+TOKEN+"&assignedFacility="+assignedFacility;
                    }else{
                        $loading.hide();
                        alertMessages.showAlertMessage($error, "Unable to save, please try again later.", alertMessages.ERROR);
                    }
                }).error(function(err){
                    $loading.hide();
                    alertMessages.showAlertMessage($error, AJAX_ERROR_MESSAGE, alertMessages.ERROR);
                });
            }
        };
    }]);
})();