(function(){

	var app = angular.module("FacilityModule", []);

	app.controller("FacilityController", ["$scope", "$http", function($scope, $http){

		$scope.facilityList = [];
        $scope.facilityListNew = {};
		$scope.facilityAdminsFromServer = [];
		$scope.facilityAdmins = {};

        $scope.refreshFacilityList = function(){
            $http({
                url: "getFacilityList.action",
                method: 'GET'
            }).success(function(data){
                //TODO: I don't know why its happening so i am double parsing it to convert to jsonObject array, __oz, change it later

                var biggerData = $.parseJSON($.parseJSON(data)),
                    facList = $.parseJSON(biggerData.FACILITIES),
                    facAdminsList = $.parseJSON(biggerData.FACILITY_ADMINS);

                $scope.facilityList = facList;

                $scope.facilityAdminsFromServer = facAdminsList;
                $scope.facilityAdmins = {};

                angular.forEach($scope.facilityAdminsFromServer, function(facilityAdmin, index){
                    $scope.facilityAdmins[facilityAdmin.facilityId] = facilityAdmin;
                });

                angular.forEach($scope.facilityList, function(facility, index){
                    facility.adminDetails = $scope.facilityAdmins[facility.facilityId];
                });

            }).error(function(err){	});
        };

        //storing in page level object
        $scope.facility = {};
        $scope.isEdited = false;
        $scope.isFacilityAdminSet = false;
        $scope.leadCoachList = COACHES_LIST;

        //storing for showing required fields label when user submits
        $scope.submitted = false;
        $scope.stateList = ["AL", "AK", "AS", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FM", "FL", "GA", "GU", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MH", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "MP", "OH", "OK", "OR", "PW", "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VI", "VA", "WA", "WV", "WI", "WY"];
        $scope.timezoneList = [
            { name: '(GMT -12:00) Eniwetok, Kwajalein', offset: '-12'},
            { name: '(GMT -11:00) Midway Island, Samoa', offset: '-11' },
            { name: '(GMT -10:00) Hawaii', offset: '-10' },
            { name: '(GMT -9:00) Alaska', offset: '-9' },
            { name: '(GMT -8:00) Pacific Time (US & Canada)', offset: '-8' },
            { name: '(GMT -7:00) Mountain Time (US & Canada)', offset: '-7' },
            { name: '(GMT -6:00) Central Time (US & Canada), Mexico City', offset: '-6' },
            { name: '(GMT -5:00) Eastern Time (US & Canada), Bogota, Lima', offset: '-5' },
            { name: '(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz', offset: '-4' },
            { name: '(GMT -3:30) Newfoundland', offset: '-3.5' },
            { name: '(GMT -3:00) Brazil, Buenos Aires, Georgetown', offset: '-3' },
            { name: '(GMT -2:00) Mid-Atlantic', offset: '-2' },
            { name: '(GMT -1:00 hour) Azores, Cape Verde Islands', offset: '-1' },
            { name: '(GMT) Western Europe Time, London, Lisbon, Casablanca', offset: '0' },
            { name: '(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris', offset: '+1' },
            { name: '(GMT +2:00) Kaliningrad, South Africa', offset: '+2' },
            { name: '(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg', offset: '+3' },
            { name: '(GMT +3:30) Tehran', offset: '3.5' },
            { name: '(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi', offset: '+4' },
            { name: '(GMT +4:30) Kabul', offset: '+4.5' },
            { name: '(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent', offset: '+5' },
            { name: '(GMT +5:30) Bombay, Calcutta, Madras, New Delhi', offset: '+5.5' },
            { name: '(GMT +5:45) Kathmandu', offset: '+5.75' },
            { name: '(GMT +6:00) Almaty, Dhaka, Colombo', offset: '+6' },
            { name: '(GMT +7:00) Bangkok, Hanoi, Jakarta', offset: '+7' },
            { name: '(GMT +8:00) Beijing, Perth, Singapore, Hong Kong', offset: '+8' },
            { name: '(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk', offset: '+9' },
            { name: '(GMT +9:30) Adelaide, Darwin', offset: '+9.5' },
            { name: '(GMT +10:00) Eastern Australia, Guam, Vladivostok', offset: '+10' },
            { name: '(GMT +11:00) Magadan, Solomon Islands, New Caledonia', offset: '+11' },
            { name: '(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka', offset: '+12' },
        ];

        $scope.editFacility = function(facility){
            $scope.facility = facility;
            $scope.facility.name = facility.facilityName;
            $scope.submitted = false;
            $scope.isEdited = true;
            $scope.isFacilityAdminSet = true;
            $scope.facility.timezone = {"offset": $scope.facility.timezoneOffset};
            $scope.facility.leadCoach = {"providerId": $scope.facility.leadCoach};
            if(LOADED_PAGE != "ADMIN"){
                var facilityAdminDetails = $scope.facilityAdmins[facility.facilityId];
                $scope.isFacilityAdminSet = false;
                if(facilityAdminDetails){
                    $scope.isFacilityAdminSet = true;
                    $scope.facility.facilityAdminFirstName = facilityAdminDetails.firstName;
                    $scope.facility.facilityAdminLastName = facilityAdminDetails.lastName;
                    $scope.facility.facilityAdminEmail = facilityAdminDetails.email;
                    $scope.facility.facilityAdminPhone = facilityAdminDetails.phone;
                    $("#txtFacilityAdminPhone").val(removeCountryCode(facilityAdminDetails.phone));

                    //written in common.js
                    setTimeout(function(){
                        formatPhoneNumber($("#txtFacilityAdminPhone"), $("#txtFacilityAdminPhone").val());
                    }, 20);
                }

                $("#facilityListDialog").foundation("reveal", "close");
            }
        };

        //when load from facility admin, pre-populate all data
        if(LOADED_PAGE == "ADMIN"){
            $scope.facility = FACILITY_LIST;
            if($scope.facility){
                $scope.editFacility($scope.facility);
            }
        }

        $scope.loadFacilityAdminPageDetails = function(){

            $scope.facilityList = FACILITY_LIST;
            $scope.facilityListNew = {};
            $scope.facilityAdminsFromServer = FACILITY_ADMINS_LIST;
            $scope.facilityAdmins = {};

            $scope.isEdited = true;
            $scope.facility = $scope.facilityList;

            $scope.facility.timezone = {"offset": $scope.facility.timezoneOffset};
            $scope.facility.leadCoach = {"providerId": $scope.facility.leadCoach};

            angular.forEach($scope.facilityAdminsFromServer, function(facilityAdmin, index){
                $scope.facilityAdmins[facilityAdmin.facilityId] = facilityAdmin;
            });

            angular.forEach($scope.facilityList, function(facility, index){
                facility.adminDetails = $scope.facilityAdmins[facility.facilityId];
            });

            var facilityAdminDetails = $scope.facilityAdmins[$scope.facility.facilityId];
            if(facilityAdminDetails) {
                $scope.isFacilityAdminSet = true;
                $scope.isFacilityAdminSet = true;
                $scope.facility.facilityAdminFirstName = facilityAdminDetails.firstName;
                $scope.facility.facilityAdminLastName = facilityAdminDetails.lastName;
                $scope.facility.facilityAdminEmail = facilityAdminDetails.email;
                $scope.facility.facilityAdminPhone = facilityAdminDetails.phone;
                $("#txtFacilityAdminPhone").val(removeCountryCode(facilityAdminDetails.phone));

                //written in common.js
                setTimeout(function () {
                    formatPhoneNumber($("#txtFacilityAdminPhone"), $("#txtFacilityAdminPhone").val());
                }, 20);
            }
        };

        //when load from facility admin, pre-populate all data
        if(LOADED_PAGE == "FACILITY_ADMIN"){
            $scope.loadFacilityAdminPageDetails();
        }

        $scope.openFacilityDialog = function(){
			$("#facilityListDialog").foundation("reveal", "open");
		};

		$scope.clearForm = function(){
			$scope.submitted = false;
			$scope.facility = {};
			$scope.isEdited = false;
            $scope.isFacilityAdminSet = false;
		};

        $scope.getFacilityAdmin = function(facId){
            var facAdminDetails = $scope.facilityAdmins[facId];
            return (facAdminDetails) ? facAdminDetails : undefined;
        };

        $scope.resendEmailPinCode = function(){

            $("#progressTxt").text("We're sending");

            var ajaxing = Object.create(Ajaxing),
                $loading = $("#loading").show(),
                $error = $("#errMessage"),
                alertMessages = Object.create(AlertMessage).init();

            $http({
                url: "resendEmailPinCode.action",
                method: 'POST',
                data: {
                    "facilityAdminEmail": $scope.facility.facilityAdminEmail
                },
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                }
            }).success(function(data){
                data = angular.fromJson(angular.fromJson(data));
                $loading.hide();

                if(data.STATUS == "SUCCESS"){
                    alertMessages.showAlertMessage($error, "Email/PinCode resend successfully.", alertMessages.SUCCESS);
                    $scope.refreshFacilityList();
                    $scope.clearForm();
                }else{
                    if(data.REASON){
                        alertMessages.showAlertMessage($error, data.REASON, alertMessages.ERROR);
                    }else{
                        //TODO: when we are testing for local numbers then it happens
                        console.log("ERROR");
                        alertMessages.showAlertMessage($error, "Email/Pin Code resent successfully.", alertMessages.SUCCESS);
                    }
                }
            }).error(function(err){	});
        };

		//submit form function
		$scope.submitForm = function(facilityToSave) {

			$scope.submitted = true;

            $("#progressTxt").text("We're saving data");

            var $ddTimezone = $("#ddTimezone"),
                $ddLeadCoach = $("#ddLeadCoach"),
                isAllow = true;

            if($ddTimezone && !$ddTimezone.val()){
                $ddTimezone.next().removeClass("ng-hide");
                isAllow = false;
            }

            if($ddLeadCoach && !$ddLeadCoach.val() && $ddLeadCoach.is(":visible")){
                $ddLeadCoach.next().removeClass("ng-hide");
                isAllow = false;
            }

            if(!isAllow){
                return;
            }

            //if form is valid then process
			if($scope.facilityForm.$valid){

				var facilityId = facilityToSave.facilityId,
        			actionName = "saveFacility.action",
        			ajaxing = Object.create(Ajaxing),
        			$loading = $("#loading").show(),
					$error = $("#errMessage"),
					alertMessages = Object.create(AlertMessage).init();

                facilityToSave.timezoneName = facilityToSave.timezone.name;
                facilityToSave.timezoneOffset = facilityToSave.timezone.offset;
                facilityToSave.leadCoach = facilityToSave.leadCoach.providerId;

                if(!facilityToSave.timezoneName){
                    facilityToSave.timezoneName = $ddTimezone.children("option:selected").text();
                }

                if(!facilityToSave.timezoneOffset){
                    facilityToSave.timezoneOffset = $ddTimezone.val();
                }

                if(!facilityToSave.leadCoach){
                    facilityToSave.leadCoach = $ddLeadCoach.val();
                }

        		//if greater than '0' it means to update the form
        		if(facilityId > 0){
        			actionName = "saveFacility.action?facilityId="+facilityId;
        		}

        		$http({
        	        url: actionName,
        	        method: 'POST',
        	        data: {
						"facilityData" : angular.toJson(facilityToSave),
						"facilityAdminEmail": $("#txtFacilityAdminEmail").val(),
						"facilityAdminPhone": $("#txtFacilityAdminPhone").val(),
						"facilityAdminFirstName": $("#txtFacilityAdminFirstName").val(),
						"facilityAdminLastName": $("#txtFacilityAdminLastName").val()
					},
        	        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        	        transformRequest: function(obj) {
        	            var str = [];
        	            for(var p in obj)
        	            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        	            return str.join("&");
        	        }
        	    }).success(function(data){
        	    	data = angular.fromJson(angular.fromJson(data));
					$loading.hide();

                    facilityToSave.timezone.offset = {offset: facilityToSave.timezoneOffset};
                    facilityToSave.leadCoach = {providerId: facilityToSave.leadCoach};
                    $ddTimezone.val(facilityToSave.timezoneOffset);
                    $ddLeadCoach.val(facilityToSave.leadCoach);

        	    	if(data.STATUS == "SUCCESS"){
						alertMessages.showAlertMessage($error, "Data saved successfully.", alertMessages.SUCCESS);
                        if(LOADED_PAGE == "ADMIN"){
                            window.location = "allFacilities.action";
                        }else{
                            window.location.reload(true);
                        }
        	    	}else{
                        $scope.submitted = false;
                        alertMessages.showAlertMessage($error, data.REASON, alertMessages.ERROR);
        	    	}
        	    }).error(function(err){
        	    	$loading.hide();
                    $scope.submitted = false;
        	    	ajaxing.showErrorOrRedirect(err, $error, AJAX_ERROR_MESSAGE);
        	    });
			}
		};
	}]);
})();