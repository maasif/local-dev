if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

var FormsUtil = {
	populateFormData: function(jsonArray) {		
		$.each(jsonArray, function(name, val) {
			var $el = $('[name="' + this.name + '"]');
			if ($el.is('select')) {						
				if ($el.attr('multiple') == 'multiple') {
					var values = this.value; // it has the multiple values to set, separated by comma
					var valuesArray = values.split(',');
					$el.val(valuesArray);
				} else {					
					$el.val(this.value);
				}
			} else {
				var type = $el.attr('type');				
				switch (type) {
				case 'checkbox':
					break;
				case 'radio':					
					break;
				default:
					$el.val(this.value);					
				}
			}
		});
	}
};
