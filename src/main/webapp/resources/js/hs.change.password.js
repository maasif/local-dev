/**
 * Created by omursaleen on 2/16/2015.
 */

function initChangePassword(){
    var $loginMenu = $("#openProfileMenu");
    $loginMenu.on("click", function(e){
        e.preventDefault();

        var pos = $(this).offset(),
            $menu = $(".login-menu");

        $menu.show().css({
            top: pos.top+30,
            left: pos.left-167
        });

        clearChangePasswordForm();
    });

    $("#txtOldPassword").on("change", function(){
        var $this = $(this),
            $error = $this.next("small.error").removeClass("success").text("Required.").hide(),
            oldPassword = $this.val(),
            $loading = $("#loadingPassword");

        if($.trim(oldPassword).length == 0){
            $error.text("Required.").css("display", "block");
        }else{
            //ajax parameters
            var dataToSend = { "oldPassword" : oldPassword },
                postRequestActions = {
                    "loading": $loading,
                    "error": $error,
                    "successCallBack": onSuccessVerifyPassword
                };

            AJAXING_HEADER.sendPostRequest("verifyPassword1.action", dataToSend, postRequestActions);
        }
    });

    $("#btnChangePassword").on("click", function(){
        changePassword();
    });

    $("#txtNewPassword").on("keypress", function(e){
        var code = e.keyCode || e.which;

        if(code == 13) {
            changePassword();
            return false;
        }
    });
}

function clearChangePasswordForm(){
    $("#loadingPassword").hide();

    var $oldPassword = $("#txtOldPassword").val(""),
        $confirmPassword = $("#txtHeaderConfirmPassword").val(""),
        $newPassword = $("#txtNewPassword").val("");

    $oldPassword.next("small.error").text("Required.").removeClass("success").text("Required.").hide();
    $newPassword.next("small.error").text("Required.").removeClass("success").text("Required.").hide();
    $confirmPassword.next("small.error").text("Required.").removeClass("success").text("Required.").hide();
}

function changePassword(){
    if($("#txtOldPassword").next("small").is(":visible")
        && !$("#txtOldPassword").next("small").hasClass("success")
        && !shouldSubmit){
        return;
    }

    var $loading = $("#loadingPassword"),
        $oldPassword = $("#txtOldPassword"),
        $errorOld = $oldPassword.next("small.error").text("Required.").removeClass("success").text("Required.").hide(),
        oldPassword = $.trim($oldPassword.val()),
        $newPassword = $("#txtNewPassword"),
        $errorNew = $newPassword.next("small.error").text("Required.").removeClass("success").text("Required.").hide(),
        newPassword = $.trim($newPassword.val()),
        $confirmPassword = $("#txtHeaderConfirmPassword"),
        confirmPassword = $.trim($confirmPassword.val()),
        $errorConform = $confirmPassword.next("small.error").text("Required.").removeClass("success").text("Required.").hide();

    if(oldPassword.length == 0 && newPassword.length == 0 && confirmPassword.length == 0){
        $errorOld.add($errorNew).add($errorConform).css("display", "block");
        return;
    }

    if(oldPassword.length == 0){
        $errorOld.css("display", "block");
        return;
    }

    if(newPassword.length == 0){
        $errorNew.css("display", "block");
        return;
    }

    if(confirmPassword.length == 0){
        $errorConform.css("display", "block");
        return;
    }

    if(!HS_VALIDATOR_HEADER.validateFieldDataByType(HS_VALIDATOR_HEADER.VALIDATORS.PASSWORD, newPassword)){
        $errorNew.text("Password must contain one upper case letter, one lower case letter, one number, and one special character such as #.").css("display", "block");
        return;
    }

    if(newPassword != confirmPassword){
        $errorConform.text("New/Confirm password do not matches.").css("display", "block");
        return;
    }

    if(oldPassword == newPassword){
        $errorConform.text("New password and old password cannot be same.").css("display", "block");
        return;
    }

    if(shouldSubmit){
        //ajax parameters
        var dataToSend = { "newPassword" : newPassword },
            postRequestActions = {
                "loading": $loading,
                "error": $errorNew,
                "successCallBack": onSuccessChangePassword
            };

        AJAXING_HEADER.sendPostRequest("changePassword1.action", dataToSend, postRequestActions);
    }
}

function onSuccessVerifyPassword(data){

    var $error = $("#txtOldPassword").next("small.error").removeClass("success").text("Required.").hide();
    shouldSubmit = false;
    if(data.STATUS == "SUCCESS"){
        //$error.addClass("success").text("Password matched.").css("display", "block");
        shouldSubmit = true;
    }else{
        $error.removeClass("success").text(data.REASON).css("display", "block");
    }
}

function onSuccessChangePassword(data){
    var $error = $("#txtHeaderConfirmPassword").next().removeClass("success").text("Required.").hide();
    if(data.STATUS == "SUCCESS"){
        $error.addClass("success").text("Password changed successfully.").css("display", "block");
        setTimeout(function(){
            clearChangePasswordForm();
            $(".login-menu").hide();
        }, 1500);
    }else{
        $error.removeClass("success").text(data.REASON).css("display", "block");
    }
}
