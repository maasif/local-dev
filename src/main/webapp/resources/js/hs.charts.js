/**
 * Created by omursaleen on 3/10/2015.
 */

// ------------------------------------------- START INIT FUNCTION --------------------------//
function initCharts() {
    $('#patientsTab').addClass('selected-tab');
    initDatePicker("selDate", null, true);
    initDatePicker("selWeeklyLogsDate",null, true);
    initDatePicker("selWeekTrendDate",null, true);
    initDatePicker("selThreeDaysDate",null, true);
    initDatePicker("selWeekDate",null, true);
    initDatePicker("selFourteenDaysDate",null, true);
    initDatePicker("selMonthDate",null, true);

    var hash = window.location.hash;
    if(!hash){
        getTrendsData("fourteenDays");
    }

    $("#selDate").on("change", function () {
        var date = new Date($(this).val());
        if (validateDate(date)) {
            oldDailyDate = $('#selDate').val();
            getTrendsData("daily");
        } else {
            $(this).val(oldDailyDate);
            alert("Please select a valid date");
            showAlertMessage($("#logbookerror"), "Please select a valid date", "ERROR");
            return false;
        }
        disableEnableButtons($.datepicker.formatDate('m/d/yy', date), "daily");
    });

    $("#selThreeDaysDate").on("change", function () {
        var currDate = new Date($(this).val());
        if (validateDate(currDate)) {
            var startDate = $('#selThreeDaysDate').datepicker('getDate'),
                endDate = $('#selThreeDaysDate').datepicker('getDate');
            endDate.setDate(startDate.getDate() + 2);

            if(validateDate(endDate)){
                $(this).val($.datepicker.formatDate('m/d/yy', startDate) + " - " + $.datepicker.formatDate('m/d/yy', endDate));
            } else{
                endDate = new Date();
                $(this).val($.datepicker.formatDate('m/d/yy', startDate) + " - " + $.datepicker.formatDate('m/d/yy', endDate));
            }

            var oldLogsDate = $('#selThreeDaysDate').val();

            disableEnableButtons(oldLogsDate);
            getTrendsData("threeDays");
            $(this).datepicker("hide");
        } else {
            alert("Please select a valid date");
            showAlertMessage($("#logbookerror"), "Please select a valid date", "ERROR");
            $(this).datepicker("hide");
            return false;
        }
        disableEnableButtons($.datepicker.formatDate('m/d/yy', currDate), "threeDays");
    });

    $("#selWeekTrendDate").on("change", function () {
        var date = new Date($(this).val());
        if (validateDate(date)) {
            var endDate = $('#selWeekTrendDate').datepicker('getDate');
            endDate.setDate(endDate.getDate() + 6);
            if (validateDate(endDate)) {
                //$(this).val($(this).val() + " - " + $.datepicker.formatDate('m/d/yy', endDate));
            } else {
                endDate = new Date();
                //$(this).val($(this).val() + " - " + $.datepicker.formatDate('m/d/yy', endDate));
            }
            oldWeekTrendDate = $('#selWeekTrendDate').val();
            getTrendsData("weekly");
        } else {
            $(this).val(oldWeelSummDate);
            alert("Please select a valid date");
            showAlertMessage($("#logbookerror"), "Please select a valid date", "ERROR");
            return false;
        }
        disableEnableButtons($.datepicker.formatDate('m/d/yy', date), "weekly-a");
    });

    $("#selWeekDate").on("change", function () {
        var date = new Date($(this).val());

        if (validateDate(date)) {
            var startDate = $('#selWeekDate').datepicker('getDate'),
                endDate = $('#selWeekDate').datepicker('getDate');
            endDate.setDate(endDate.getDate() + 6);
            if (validateDate(endDate)) {
                $(this).val($.datepicker.formatDate('m/d/yy', startDate) + " - " + $.datepicker.formatDate('m/d/yy', endDate));
            }
            else {
                endDate = new Date();
                $(this).val($.datepicker.formatDate('m/d/yy', startDate) + " - " + $.datepicker.formatDate('m/d/yy', endDate));
            }

            oldWeelSummDate = $('#selWeekDate').val();
            getTrendsData("weekly");
        }
        else {
            $(this).val(oldWeelSummDate);
            alert("Please select a valid date");
            showAlertMessage($("#logbookerror"), "Please select a valid date", "ERROR");
            return false;
        }
        disableEnableButtons($.datepicker.formatDate('m/d/yy', date), "weekly");
    });

    $("#selFourteenDaysDate").on("change", function () {
        var currDate = new Date($(this).val());

        if (validateDate(currDate)) {
            var startDate = $('#selFourteenDaysDate').datepicker('getDate'),
                endDate = $('#selFourteenDaysDate').datepicker('getDate');
            endDate.setDate(endDate.getDate() + 13);

            if(validateDate(endDate)){
                $(this).val($.datepicker.formatDate('m/d/yy', startDate) + " - " + $.datepicker.formatDate('m/d/yy', endDate));
            } else{
                endDate = new Date();
                $(this).val($.datepicker.formatDate('m/d/yy', startDate) + " - " + $.datepicker.formatDate('m/d/yy', endDate));
            }

            var oldLogsDate = $('#selFourteenDaysDate').val();

            disableEnableButtons(oldLogsDate);
            getTrendsData("fourteenDays");
            $(this).datepicker("hide");
        } else {
            alert("Please select a valid date");
            showAlertMessage($("#logbookerror"), "Please select a valid date", "ERROR");
            $(this).datepicker("hide");
            return false;
        }
        disableEnableButtons($.datepicker.formatDate('m/d/yy', currDate), "fourteenDays");
    });

    $("#selMonthDate").on("change", function () {
        var currDate = new Date($(this).val());
        if (validateDate(currDate)) {
            var startDate = $('#selMonthDate').datepicker('getDate'),
                endDate = $('#selMonthDate').datepicker('getDate');
            endDate.setDate(endDate.add({months: + 1}).getDate());

            if(validateDate(endDate)){
                $(this).val($.datepicker.formatDate('m/d/yy', startDate) + " - " + $.datepicker.formatDate('m/d/yy', endDate));
            } else {
                endDate = new Date();
                $(this).val($.datepicker.formatDate('m/d/yy', startDate) + " - " + $.datepicker.formatDate('m/d/yy', endDate));
            }

            var oldLogsDate = $('#selMonthDate').val();


            disableEnableButtons(oldLogsDate);
            getTrendsData("monthly");
            $(this).datepicker("hide");
        } else {
            alert("Please select a valid date");
            showAlertMessage($("#logbookerror"), "Please select a valid date", "ERROR");
            $(this).datepicker("hide");
            return false;
        }
        disableEnableButtons($.datepicker.formatDate('m/d/yy', currDate), "monthly");
    });

    oldMonthlyDate = $('#selMonthDate').val();
    oldWeelSummDate = $('#selWeekDate').val();
    oldDailyDate = $('#selDate').val();
    oldWeekTrendDate = $('#selWeekTrendDate').val();
    disableEnableButtons($.datepicker.formatDate('m/d/yy', new Date()), "weekly");
    disableEnableButtons($.datepicker.formatDate('m/d/yy', new Date()), "weekly-a");
    disableEnableButtons($.datepicker.formatDate('m/d/yy', new Date()), "daily");
    disableEnableButtons($.datepicker.formatDate('m/d/yy', new Date()), "monthly");
    disableEnableButtons($.datepicker.formatDate('m/d/yy', new Date()), "threeDays");
    disableEnableButtons($.datepicker.formatDate('m/d/yy', new Date()), "fourteenDays");
}

function loadDailyTabData() {
    getTodaysLogs();
}

function loadMonthlyTabData() {
    getTrendsData("monthly");
}

function loadWeeklyTabData() {
    getTrendsData("weekly");
}

function loadThreeDaysTabData() {
    getTrendsData("threeDays");
}

function loadFourteenDaysTabData() {
    getTrendsData("fourteenDays");
}

function showHideTrends(source) {

    if (source == "daily") {

        $("#weeklyTrends").hide();
        $("#dailyTrends").fadeIn("slow");

        $("#dailyTrendsBtn").removeClass("inactive-trend");
        $("#weeklyTrendsBtn").removeClass("active-trend");

        $("#dailyTrendsBtn").addClass("active-trend");
        $("#weeklyTrendsBtn").addClass("inactive-trend");

        $(".highcharts-axis-labels").show();
    } else {

        $("#dailyTrends").hide();
        $("#weeklyTrends").fadeIn("slow");

        $("#weeklyTrendsBtn").removeClass("inactive-trend");
        $("#dailyTrendsBtn").removeClass("active-trend");

        $("#weeklyTrendsBtn").addClass("active-trend");
        $("#dailyTrendsBtn").addClass("inactive-trend");

        $(".highcharts-axis-labels").hide();
    }

    $(".trend-btns").blur();
}

//------------------------------------------- END INIT FUNCTION --------------------------//

//------------------------------------------- START AJAX CALLS  ---------------------------//
function showMonthlyScatterGlucoseSummary(data) {
    //var glucoseSummaryData = data.Glucose;
    drawMonthlyScatterGlucoseChart(data);
}

function showFourteenDaysGlucoseSummary(data) {
    $("#container-FourteenDaysGlucose").removeClass("week-no-record");
    $("#spFourteenDaysGlucoseAvg").html(" N/A");
    $("#spFourteenDaysGlucoseTest").html(" 0");
    $("#spFourteenDaysGlucoseTarget").html("");

    $("#container-FourteenDaysGlucoseAfterMeal").removeClass("week-no-record");
    $("#spFourteenDaysGlucoseAvg-g").html(" N/A");
    $("#spFourteenDaysGlucoseTest-g").html(" 0");
    $("#spFourteenDaysGlucoseTarget-g").html("");

    var glucoseSummaryData = data.Glucose;
    var glucoseSummaryDataAfterMeal = data.After_Meal_Glucose;
    var glucoseSummaryDataNotAfterMeal = data.Not_After_Meal_Glucose;

    if (glucoseSummaryData) {
        drawFourteenDaysGlucoseChart(data, glucoseSummaryDataAfterMeal, glucoseSummaryDataNotAfterMeal);
    } else {
        $("#container-FourteenDaysGlucose").text("No Log Found");
        $("#container-FourteenDaysGlucose").addClass("week-no-record");
        $("#container-FourteenDaysGlucoseAfterMeal").text("No Log Found");
        $("#container-FourteenDaysGlucoseAfterMeal").addClass("week-no-record");
    }
}

function showFourteenDaysScatterGlucoseSummary(data) {
    //var glucoseSummaryData = data.Glucose;
    drawFourteenDaysScatterGlucoseChart(data);
}

function showThreeDaysScatterGlucoseSummary(data) {
    //var glucoseSummaryData = data.Glucose;
    drawThreeDaysScatterGlucoseChart(data);
}

function showThreeDaysGlucoseSummary(data) {
    $("#container-ThreeDaysGlucose").removeClass("week-no-record");
    $("#container-ThreeDaysGlucose-g").removeClass("week-no-record");

    var glucoseSummaryData = data.Glucose;
    var glucoseSummaryDataAfterMeal = data.After_Meal_Glucose;
    var glucoseSummaryDataNotAfterMeal = data.Not_After_Meal_Glucose;

    $("#spThreeDaysGlucoseAvg").html(" N/A");
    $("#spThreeDaysGlucoseTest").html(" 0");
    $("#spThreeDaysGlucoseTarget").html("");

    $("#spThreeDaysGlucoseAvg-g").html(" N/A");
    $("#spThreeDaysGlucoseTest-g").html(" 0");
    $("#spThreeDaysGlucoseTarget-g").html("");

    if (glucoseSummaryData) {
        drawThreeDaysGlucoseChart(data, glucoseSummaryDataAfterMeal, glucoseSummaryDataNotAfterMeal);
    } else {
        $("#container-ThreeDaysGlucose").text("No Log Found");
        $("#container-ThreeDaysGlucose").addClass("week-no-record");
        $("#container-ThreeDaysGlucose-g").text("No Log Found");
        $("#container-ThreeDaysGlucose-g").addClass("week-no-record");
    }
}

function showWeeklyGlucoseSummary(data) {

    $("#container-weeklyGlucoseAfterMeal").removeClass("week-no-record");
    $("#container-weeklyGlucoseNotAfterMeal").removeClass("week-no-record");
    $("#container-weeklyMeal").removeClass("week-no-record");

    var glucoseSummaryData = data.Glucose;
    var glucoseSummaryDataAfterMeal = data.After_Meal_Glucose;
    var glucoseSummaryDataNotAfterMeal = data.Not_After_Meal_Glucose;

    $("#avgGlucose-b").html(" N/A");
    $("#avgGlucoseTest-b").html(" 0");
    $("#avgGlucose-g").html(" N/A");
    $("#avgGlucoseTest-g").html(" 0");

    if (glucoseSummaryData) {
        drawGlucoseChart(glucoseSummaryData, glucoseSummaryDataAfterMeal, glucoseSummaryDataNotAfterMeal);
    } else {
        $("#container-weeklyGlucoseAfterMeal").text("No Log Found");
        $("#container-weeklyGlucoseAfterMeal").addClass("week-no-record");
        $("#container-weeklyGlucoseNotAfterMeal").text("No Log Found");
        $("#container-weeklyGlucoseNotAfterMeal").addClass("week-no-record");
    }

    getTrendsData("weekly");
}

function getTrendsData(source) {
    if (source == "" || source == "weekly") {
        var $date = $("#selWeekTrendDate");
        if (getSelectedTab() == "weekly") {
            $date = $("#selWeekDate");
        }
        var millisDateRange = HSDateUtils.dateRangeToMillisRange($date.val(), true);

        var actionUrl = "fetchWeeklyGlucoseScatterGlucoseData.action"
            , dataToSend = {
                "patientId": $("#patientId").val(),
                "weeklyReportDateString": millisDateRange
            }
            , postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawWeeklyDaysScatterGlucoseChart,
                "loading": $("#clWeeklyGlucoseChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //Weekly meal charts
        var actionUrl = "fetchWeeklyMealsData.action"
            , dataToSend = {
                "patientId": $("#patientId").val(),
                "weeklyTrendDateString": $date.val()
            }
            , postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawWeeklyMeals,
                "loading": $("#clWeeklyMealChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //Weekly Med charts
        var actionUrl = "fetchWeeklyMedicationData.action"
            , dataToSend = {
                "patientId": $("#patientId").val(),
                "weeklyTrendDateString": $date.val()
            }
            , postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawWeeklyMedicationTrendChart,
                "loading": $("#clWeeklyMedLogChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //weekly Activity chart
        var actionUrl = "fetchWeeklyActivityChart.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "weeklyReportDateString": millisDateRange

            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawWeeklyActivityChart,
                "loading": $("#clWeeklyActivityChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        var actionUrl = "fetchWeeklyCombineActivityMinutesData.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "weeklyReportDateString": millisDateRange

            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawWeeklyActivityMinutesChart,
                "loading": $("#clWeeklyActivityChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

    } else if (source == "" || source == "monthly") {
        var $date = $("#selMonthDate"),
            millisDateRange = HSDateUtils.dateRangeToMillisRange($date.val(), true);
        //Weekly meal charts
        var actionUrl = "fetchMonthlyMealsData.action"
            , dataToSend = {
                "patientId": $("#patientId").val(),
                "monthlyTrendDateString": $date.val()
            }
            , postRequestActions = {
                "requestType": "GET",
                "successCallBack": showMonthlyMeals,
                "loading": $("#clMonthlyMealChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //Monthly Activity chart
        var actionUrl = "fetchCombineActivityData.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "customDateString": millisDateRange,
                "noOfDays": 30

            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawMonthlyActivityChart,
                "loading": $("#clMonthlyActivityChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        var actionUrl = "fetchMonthlyCombineActivityMinutesData.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "monthlyTrendDateString": millisDateRange

            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawMonhtlyActivityMinutesChart,
                "loading": $("#clMonthlyActivityChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //Monthly Weight chart
        var actionUrl = "fetchMonthlyWeightChart.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "monthlyTrendDateString": $date.val()

            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawMonthlyWeightChart,
                "loading": $("#clMonthlyWeightLossChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //Monthly Glucose chart
        var actionUrl = "fetchMonthlyScatterGlucoseData.action"
            , dataToSend = {
                "patientId": $("#patientId").val(),
                "monthlyReportDateString": millisDateRange
            }
            , postRequestActions = {
                "requestType": "GET",
                "successCallBack": showMonthlyScatterGlucoseSummary,
                "loading": $("#clMonthlyDaysGlucoseChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //monthly Med charts
        var actionUrl = "fetchMonthlyMedicationData.action"
            , dataToSend = {
                "patientId": $("#patientId").val(),
                "monthlyTrendDateString": $date.val()
            }
            , postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawMonthlyMedicationTrendChart,
                "loading": $("#clMonthlyMedLogChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
    } else if (source == "" || source == "threeDays") {
        var $date = $("#selThreeDaysDate"),
            millisDateRange = HSDateUtils.dateRangeToMillisRange($date.val(), true);

        //three days Glucose chart
        var actionUrl = "fetchThreeDaysScatterGlucoseData.action"
            , dataToSend = {
                "patientId": $("#patientId").val(),
                "threeDaysDateString": millisDateRange
            }
            , postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawThreeDaysScatterGlucoseChart,
                "loading": $("#clThreeDaysGlucoseChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //three days meal charts
        var actionUrl = "fetchThreeDaysMealsData.action"
            , dataToSend = {
                "patientId": $("#patientId").val(),
                "threeDaysDateString": $date.val()
            }
            , postRequestActions = {
                "requestType": "GET",
                "successCallBack": showThreeDaysMeal,
                "loading": $("#clThreeDaysMealChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //Three Med charts
        var actionUrl = "fetchThreeDaysMedicationData.action"
            , dataToSend = {
                "patientId": $("#patientId").val(),
                "threeDaysDateString": $date.val()
            }
            , postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawThreeDaysMedicationTrendChart,
                "loading": $("#clThreeDaysMedicationChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //three days Activity chart, OZ
        var actionUrl = "fetchThreeDaysCombineActivityData.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "threeDaysDateString": millisDateRange,
                "noOfDays": 3

            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawThreeDaysActivityChart,
                "loading": $("#clThreeDaysActivityCombineChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        var actionUrl = "fetchThreeDaysCombineActivityMinutesData.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "threeDaysDateString": millisDateRange

            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawThreeDaysActivityMinutesChart,
                "loading": $("#clThreeDaysActivityCombineChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //three days Weight chart
        var actionUrl = "fetchThreeDaysWeightChart.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "threeDaysDateString": $date.val()

            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawThreeDaysWeightChart,
                "loading": $("#clThreeDaysWeightLossChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

    } else if (source == "" || source == "fourteenDays") {
        var $date = $("#selFourteenDaysDate"),
            millisDateRange = HSDateUtils.dateRangeToMillisRange($date.val(), true);

        //three days Glucose chart
        var actionUrl = "fetchFourteenDaysScatterGlucoseData.action"
            , dataToSend = {
                "patientId": $("#patientId").val(),
                "fourteenDaysReportDateString": millisDateRange
            }
            , postRequestActions = {
                "requestType": "GET",
                "successCallBack": showFourteenDaysScatterGlucoseSummary,
                "loading": $("#clFourteenDaysGlucoseChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //three days meal charts
        var actionUrl = "fetchFourteenDaysMealsData.action"
            , dataToSend = {
                "patientId": $("#patientId").val(),
                "fourteenDaysReportDateString": $date.val()
            }
            , postRequestActions = {
                "requestType": "GET",
                "successCallBack": showFourteenDaysMeal,
                "loading": $("#clFourteenDaysMealChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //monthly Med charts
        var actionUrl = "fetchFourteenDaysMedicationData.action"
            , dataToSend = {
                "patientId": $("#patientId").val(),
                "fourteenDaysReportDateString": $date.val()
            }
            , postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawFourteenDaysMedicationTrendChart,
                "loading": $("#clFourteenDaysMedicationChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //three days Activity chart
        var actionUrl = "fetchCombineActivityData.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "customDateString": millisDateRange,
                "noOfDays": 14

            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawFourteenDaysActivityChart,
                "loading": $("#clFourteenDaysActivityChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        var actionUrl = "fetchFouteenDaysCombineActivityMinutesData.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "fourteenDaysReportDateString": millisDateRange

            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawFourteenDaysActivityMinutesChart,
                "loading": $("#clFourteenDaysActivityChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //three days Weight chart
        var actionUrl = "fetchFourteenDaysWeightChart.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "fourteenDaysReportDateString": $date.val()

            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawFourteenDaysWeightChart,
                "loading": $("#clFourteenDaysWeightLossChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

    } else if (source == "" || source == "daily") {
        var $date = $("#selDate"),
            millisDateRange = HSDateUtils.dateRangeToMillisRange($date.val()+"-"+$date.val(), true, true);

        //Daily glucose chart
        var actionUrl = "populateGlucoseChart.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "dailyReportDateString": millisDateRange
            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawDailyGlucoseChart,
                "loading": $("#clDailyGlucoseChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //Daily carbs chart
        var actionUrl = "populateCarbsChart.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "dailyReportDateString": $("#selDate").val()

            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawDailyCarbsChart,
                "loading": $("#clDailyCarbIntakeChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        //Daily Activity chart
        var actionUrl = "fetchDailyCombineActivityData.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "dailyReportDateString": millisDateRange

            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawDailyActivityChart,
                "loading": $("#clDailyActivityCombineChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);

        var actionUrl = "fetchDailyCombineActivityMinutesData.action",
            dataToSend = {
                "patientId": $("#patientId").val(),
                "dailyReportDateString": millisDateRange

            }, postRequestActions = {
                "requestType": "GET",
                "successCallBack": drawDailyActivityMinutesChart,
                "loading": $("#clDailyActivityCombineChart")
            };
        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
    }
}

function showFourteenDaysMeal(data) {
    var fourteenDaysMealsData = data.fourteen_days_Meal;
    if (fourteenDaysMealsData[0] == 0) {
        fourteenDaysMealsData[1] = 0;
        fourteenDaysMealsData[2] = 0;
        fourteenDaysMealsData[3] = 0;
        fourteenDaysMealsData[4] = 0;
        fourteenDaysMealsData[5] = 0;
        fourteenDaysMealsData[6] = 0;
        fourteenDaysMealsData[7] = 0;
        fourteenDaysMealsData[8] = 0;
        fourteenDaysMealsData[9] = 0;
        fourteenDaysMealsData[10] = 0;
        fourteenDaysMealsData[11] = 0;
        fourteenDaysMealsData[12] = 0;
    }
    drawFourteenDaysMealChart(data);
}

function showThreeDaysMeal(data) {
    var threeDaysMealsData = data.three_days_Meal;
    if (threeDaysMealsData[0] == 0) {
        threeDaysMealsData[1] = 0;
        threeDaysMealsData[2] = 0;
        threeDaysMealsData[3] = 0;
        threeDaysMealsData[4] = 0;
        threeDaysMealsData[5] = 0;
        threeDaysMealsData[6] = 0;
        threeDaysMealsData[7] = 0;
        threeDaysMealsData[8] = 0;
        threeDaysMealsData[9] = 0;
        threeDaysMealsData[10] = 0;
        threeDaysMealsData[11] = 0;
        threeDaysMealsData[12] = 0;
    }
    drawThreeDaysMealChart(data);
}

function showMonthlyMeals(data) {
    var monthlyMealsData = data.Monthly_Meal;
    if (monthlyMealsData[0] == 0) {
        monthlyMealsData[1] = 0;
        monthlyMealsData[2] = 0;
        monthlyMealsData[3] = 0;
        monthlyMealsData[4] = 0;
        monthlyMealsData[5] = 0;
        monthlyMealsData[6] = 0;
        monthlyMealsData[7] = 0;
        monthlyMealsData[8] = 0;
        monthlyMealsData[9] = 0;
        monthlyMealsData[10] = 0;
        monthlyMealsData[11] = 0;
        monthlyMealsData[12] = 0;
    }
    drawMonthlyMealChart(data);
}

function drawWeeklyMeals(data) {
    var weeklyTrendData = data.Weekly_Meal;
    if (weeklyTrendData[0] == 0) {
        weeklyTrendData[1] = 0;
        weeklyTrendData[2] = 0;
        weeklyTrendData[3] = 0;
        weeklyTrendData[4] = 0;
        weeklyTrendData[5] = 0;
        weeklyTrendData[6] = 0;
        weeklyTrendData[7] = 0;
        weeklyTrendData[8] = 0;
        weeklyTrendData[9] = 0;
        weeklyTrendData[10] = 0;
        weeklyTrendData[11] = 0;
        weeklyTrendData[12] = 0;
    }
    drawWeeklyMealChart(data);
}

function getSelectedTab() {
    return $("#tabWeeklyDaily li.active").data("tab");
}

//------------------------------------------- END AJAX CALLS ---------------------------//

// ------------------------------- START NAVIGATION FUNCTION ----------------------------------//

/*--------- WEEK ON PRE-NEXT BUTTONS -------- */
function getNextWeek(callFrom) {
    if (callFrom == "weekly") {
        var dateString = $('#selWeekDate').val();
        var datesMap = getNextStartEndDates(dateString, callFrom);
        $('#selWeekDate').val(
            datesMap["startDate"] + " - " + datesMap["endDate"]);
        oldWeelSummDate = $('#selWeekDate').val();
    } else if (callFrom == "weekly-a") {
        var dateString = $('#selWeekTrendDate').val();
        var datesMap = getNextStartEndDates(dateString, callFrom);
        $('#selWeekTrendDate').val(
            datesMap["startDate"] + " - " + datesMap["endDate"]);
        oldWeekTrendDate = $('#selWeekTrendDate').val();
    }
    getTrendsData("weekly");
}

function getPreWeek(callFrom) {
    if (callFrom == "weekly") {
        var dateString = $('#selWeekDate').val();
        var datesMap = getPreStartEndDates(dateString, callFrom);
        $('#selWeekDate').val(
            datesMap["startDate"] + " - " + datesMap["endDate"]);
        oldWeelSummDate = $('#selWeekDate').val();
    } else if (callFrom == "weekly-a") {
        var dateString = $('#selWeekTrendDate').val();

        var datesMap = getPreStartEndDates(dateString, callFrom);
        $('#selWeekTrendDate').val(
            datesMap["startDate"] + " - " + datesMap["endDate"]);
        oldWeekTrendDate = $('#selWeekTrendDate').val();
    }
    getTrendsData("weekly");
}

function getPreStartEndDates(dateString, callFrom) {
    var dateStringParts = dateString.split("-");
    var sourceDate = $.trim(dateStringParts[0]);

    var date = new Date(sourceDate);
    date.setDate(date.getDate() - 1);
    var endDate = $.datepicker.formatDate('m/d/yy', date);

    date.setDate(date.getDate() - 6);
    var startDate = $.datepicker.formatDate('m/d/yy', date);

    var datesMap = {};
    datesMap["startDate"] = startDate;
    datesMap["endDate"] = endDate;
    disableEnableButtons(endDate, callFrom);
    return datesMap;
}

function getNextStartEndDates(dateString, callFrom) {

    var startDate, endDate;
    var dateStringParts = dateString.split("-");
    var sourceDate = $.trim(dateStringParts[1]);

    var date = new Date(sourceDate);
    date.setDate(date.getDate() + 1);
    if (validateDate(date)) {
        startDate = $.datepicker.formatDate('m/d/yy', date);
    } else {
        date = new Date();
        startDate = $.datepicker.formatDate('m/d/yy', date);
    }

    date.setDate(date.getDate() + 6);
    if (validateDate(date)) {
        endDate = $.datepicker.formatDate('m/d/yy', date);
    } else {
        date = new Date();
        endDate = $.datepicker.formatDate('m/d/yy', date);
    }

    var datesMap = {};
    datesMap["startDate"] = startDate;
    datesMap["endDate"] = endDate;

    disableEnableButtons(endDate, callFrom);
    return datesMap;
}
/*------ THREE DAYS ON PRE-NEXT BUTTONS ------ */
function getThreeDaysTime(callFrom) {
    if (callFrom == "threePrev") {
        var dateString = $('#selThreeDaysDate').val();
        var datesMap = getThreePreStartEndDates(dateString, "threeDays");
        $('#selThreeDaysDate').val(datesMap["startDate"] + " - " + datesMap["endDate"]);
    } else if (callFrom == "threeNext") {
        var dateString = $('#selThreeDaysDate').val();
        var datesMap = getThreeNextStartEndDates(dateString, "threeDays");
        $('#selThreeDaysDate').val(datesMap["startDate"] + " - " + datesMap["endDate"]);
    }
    getTrendsData("threeDays");
}

function getThreePreStartEndDates(dateString, callFrom) {
    var dateStringParts = dateString.split("-");
    var sourceDate = $.trim(dateStringParts[0]);

    var date = new Date(sourceDate);
    date.setDate(date.getDate() - 1);
    var endDate = $.datepicker.formatDate('m/d/yy', date);

    date.setDate(date.getDate() - 2);
    var startDate = $.datepicker.formatDate('m/d/yy', date);

    var datesMap = {};
    datesMap["startDate"] = startDate;
    datesMap["endDate"] = endDate;
    disableEnableButtons(endDate, callFrom);
    return datesMap;
}

function getThreeNextStartEndDates(dateString, callFrom) {

    var startDate, endDate;
    var dateStringParts = dateString.split("-");
    var sourceDate = $.trim(dateStringParts[1]);

    var date = new Date(sourceDate);
    date.setDate(date.getDate() + 1);
    if (validateDate(date)) {
        startDate = $.datepicker.formatDate('m/d/yy', date);
    } else {
        date = new Date();
        startDate = $.datepicker.formatDate('m/d/yy', date);
    }

    date.setDate(date.getDate() + 2);
    if (validateDate(date)) {
        endDate = $.datepicker.formatDate('m/d/yy', date);
    } else {
        date = new Date();
        endDate = $.datepicker.formatDate('m/d/yy', date);
    }

    var datesMap = {};
    datesMap["startDate"] = startDate;
    datesMap["endDate"] = endDate;

    disableEnableButtons(endDate, callFrom);
    return datesMap;
}

/*------ FOURTEEN DAYS ON PRE-NEXT BUTTONS ------ */
function getFourteenDaysTime(callFrom) {
    if (callFrom == 'prev') {
        var dateString = $('#selFourteenDaysDate').val();
        var datesMap = getFourteenPreStartEndDates(dateString, "fourteenDays");
        $('#selFourteenDaysDate').val(datesMap["startDate"] + " - " + datesMap["endDate"]);
    } else if (callFrom == 'next') {
        var dateString = $('#selFourteenDaysDate').val();
        var datesMap = getFourteenNextStartEndDates(dateString, "fourteenDays");
        $('#selFourteenDaysDate').val(datesMap["startDate"] + " - " + datesMap["endDate"]);
    }
    getTrendsData("fourteenDays");
}

function getFourteenPreStartEndDates(dateString, callFrom) {
    var dateStringParts = dateString.split("-");
    var sourceDate = $.trim(dateStringParts[0]);

    var date = new Date(sourceDate);
    date.setDate(date.getDate() - 1);
    var endDate = $.datepicker.formatDate('m/d/yy', date);

    date.setDate(date.getDate() - 13);
    var startDate = $.datepicker.formatDate('m/d/yy', date);

    var datesMap = {};
    datesMap["startDate"] = startDate;
    datesMap["endDate"] = endDate;
    disableEnableButtons(endDate, callFrom);
    return datesMap;
}

function getFourteenNextStartEndDates(dateString, callFrom) {

    var startDate, endDate;
    var dateStringParts = dateString.split("-");
    var sourceDate = $.trim(dateStringParts[1]);

    var date = new Date(sourceDate);
    date.setDate(date.getDate() + 1);
    if (validateDate(date)) {
        startDate = $.datepicker.formatDate('m/d/yy', date);
    } else {
        date = new Date();
        startDate = $.datepicker.formatDate('m/d/yy', date);
    }

    date.setDate(date.getDate() + 13);
    if (validateDate(date)) {
        endDate = $.datepicker.formatDate('m/d/yy', date);
    } else {
        date = new Date();
        endDate = $.datepicker.formatDate('m/d/yy', date);
    }

    var datesMap = {};
    datesMap["startDate"] = startDate;
    datesMap["endDate"] = endDate;

    disableEnableButtons(endDate, callFrom);
    return datesMap;
}


/*------ MONTH ON PRE-NEXT BUTTONS ------ */
function getMonthTime(callParam) {
    if (callParam == "monthPrev") {
        var dateString = $('#selMonthDate').val();
        var datesMap = getPreMonthStartEndDates(dateString, "monthly");
        $('#selMonthDate').val(datesMap["startDate"] + " - " + datesMap["endDate"]);
    } else if (callParam == "monthNext") {
        var dateString = $('#selMonthDate').val();
        var datesMap = getNextMonthStartEndDates(dateString, "monthly");
        $('#selMonthDate').val(datesMap["startDate"] + " - " + datesMap["endDate"]);
    }
    getTrendsData("monthly");
}

function getPreMonthStartEndDates(dateString, callFrom) {
    var dateStringParts = dateString.split("-");
    var sourceDate = $.trim(dateStringParts[0]);

    var date = new Date(sourceDate);
    date.setDate(date.getDate() - 1);
    var endDate = $.datepicker.formatDate('m/d/yy', date);

    date = Date.parse(endDate).add({months: -1});
    var startDate = $.datepicker.formatDate('m/d/yy', date);

    var datesMap = {};
    datesMap["startDate"] = startDate;
    datesMap["endDate"] = endDate;
    disableEnableButtons(endDate, callFrom);
    return datesMap;
}

function getNextMonthStartEndDates(dateString, callFrom) {
    var dateStringParts = dateString.split("-");
    var sourceDate = $.trim(dateStringParts[1]);

    var date = new Date(sourceDate);
    date.setDate(date.getDate() + 1);
    var startDate = $.datepicker.formatDate('m/d/yy', date);

    date = Date.parse(startDate).add({months: +1});
    var endDate = $.datepicker.formatDate('m/d/yy', date);

    var datesMap = {};
    datesMap["startDate"] = startDate;
    datesMap["endDate"] = endDate;
    disableEnableButtons(endDate, callFrom);
    return datesMap;
}


function validateDate(date) {

    var today = new Date().getTime();

    if (today < date.getTime()) {
        return false;
    }

    return true;
}

function disableEnableButtons(dateString, callFrom) {
    var sourceDate = $.datepicker.formatDate('m/d/yy', new Date(dateString));
    var todays = $.datepicker.formatDate('m/d/yy', new Date());

    if (sourceDate == todays) {
        if (callFrom == "weekly") {
            $("#weekSummDateNext").attr("disabled", true);
        } else if (callFrom == "weekly-a") {
            $("#weekTrendDateNext").attr("disabled", true);
        } else if (callFrom == "daily") {
            $("#dailyDateNext").attr("disabled", true);
        } else if (callFrom == "monthly") {
            $("#monthlyDateNext").attr("disabled", true);
        } else if (callFrom == "threeDays") {
            $("#threeDaysDateNext").attr("disabled", true);
        } else if (callFrom == "fourteenDays") {
            $("#fourteenDaysDateNext").attr("disabled", true);
        }
    } else {
        if (callFrom == "weekly") {
            $("#weekSummDateNext").removeAttr("disabled");
        } else if (callFrom == "weekly-a") {
            $("#weekTrendDateNext").removeAttr("disabled");
        } else if (callFrom == "daily") {
            $("#dailyDateNext").removeAttr("disabled");
        } else if (callFrom == "monthly") {
            $("#monthlyDateNext").removeAttr("disabled");
        } else if (callFrom == "threeDays") {
            $("#threeDaysDateNext").removeAttr("disabled");
        } else if (callFrom == "fourteenDays") {
            $("#fourteenDaysDateNext").removeAttr("disabled");
        }
    }
}

function getNextDay() {

    var dateString = $('#selDate').val();
    var date = new Date(dateString);
    date.setDate(date.getDate() + 1);

    if (validateDate(date)) {
        $('#selDate').val($.datepicker.formatDate('m/d/yy', date));
    } else {
        date = new Date();
        $('#selDate').val($.datepicker.formatDate('m/d/yy', date));
    }
    getTrendsData("daily");
    disableEnableButtons($.datepicker.formatDate('m/d/yy', date), "daily");
}

function getPreDay() {

    var dateString = $('#selDate').val();
    var date = new Date(dateString);
    date.setDate(date.getDate() - 1);
    $('#selDate').val($.datepicker.formatDate('m/d/yy', date));
    getTrendsData("daily");
    disableEnableButtons($.datepicker.formatDate('m/d/yy', date),
        "daily");
}

function getTodaysLogs() {
    var date = new Date();
    $('#selDate').val($.datepicker.formatDate('m/d/yy', date));
    getTrendsData("daily");
    disableEnableButtons($.datepicker.formatDate('m/d/yy', date), "daily");
}

//------------------------------ END NAVIGATION FUNCTION --------------------------------//

// ------------------------------ START DRAW CHARTS --------------------------------//

function drawDailyCarbsChart(listdata) {
    var date = new Date($("#selDate").val());
    var month = date.getMonth();
    var year = date.getYear();
    var day = date.getDay();

    var options = {
        scrollbar: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },

        global: {
            timezoneOffset: 5 * 60,
            useUTC: false
        },

        chart: {
            renderTo: 'containerCarbs',
            ignoreHiddenSeries: false,
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {},
        xAxis: {

            title: {
                enabled: true,
                text: 'Hours '
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true,
            type: 'datetime',
            tickInterval: 3600 * 1000,
            min: listdata.MinMax[0],
            max: listdata.MinMax[1],
            dateTimeLabelFormats: {
                hour: '%I %p',
                minute: '%I:%M %p'
            }
        },
        yAxis: {
            /*plotLines: plotLinesArray,*/
            min: 0,
            max: 120,
            title: {
                text: 'Carbs (gram) '
            }
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%I:%M %p} , {point.y} grams',
            shared: true,
            useHTML: true,
            positioner: function (boxWidth, boxHeight, point) {
                return {
                    x: point.plotX - 100,
                    y: point.plotY - 25
                };
            }
        },
        plotOptions: {
            column: {
                groupPadding: 0.1
            }
        },

        series: [{
            name: 'Over Target',
            color: '#786565',
            tickInterval: 3600 * 1000,
            pointRange: 3600 * 1000,
            dataLabels: {
                enabled: true,

                style: {

                    color: '#786565'
                }
            }
        }, {
            name: 'In Target',
            color: '#5EA647',
            tickInterval: 3600 * 1000,
            pointRange: 3600 * 1000,
            dataLabels: {
                enabled: true,
                style: {
                    format: '{point.y:.0f}',
                    color: '#5EA647'
                }
            }
        }, {
            name: 'Missing Food',
            color: '#000',
            tickInterval: 3600 * 1000,
            pointRange: 3600 * 1000,
            dataLabels: {
                enabled: true,
                style: {
                    color: '#000'
                }
            }
        }]
    };

    options.series[0].data = listdata.High;
    options.series[1].data = listdata.Normal;
    options.series[2].data = listdata.HasMissingFood;

    CarbsChart = new Highcharts.Chart(options);
}

function drawDailyGlucoseChart(listdata) {
    var $date = $("#selDate"),
        minMax = HSDateUtils.dateRangeFromMilli($date.val(), 2),
        maxYAxis = getMaxValueInArray(listdata.maxYAxis, 200);

    loadGlucoseChartLegend(listdata);

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    var options = {
        dataLabels: {
            enabled: true,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor)
            || 'black',
            style: {
                textShadow: '0 0 3px black, 0 0 3px black'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        chart: {
            renderTo: 'containerGlucose',
            type: 'scatter',
            zoomType: 'xy',
            ignoreHiddenSeries: false
        },
        title: {
            text: ''
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Time'
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true,
            type: 'datetime',
            tickInterval: 3600 * 1000,
            min: minMax[0],
            max: minMax[1],
            labels: {
                enabled: true,
                formatter: function (){
                    return Highcharts.dateFormat('%I %p',this.value);
                },
                style:{
                    width:'1px'
                },
                step: 1
            },
            dateTimeLabelFormats: {
                hour: '%I %p'
            }
        },
        yAxis: {
            min: 0,
            max: maxYAxis,
            title: {
                text: 'Glucose (mg/dL)'
            },
            floor: 0,
            plotBands: [{
                // After Meal PLOT
                color: 'rgba(0,255,0,.50)', // Color value
                from: listdata.beforeMealGlucoseTargets[0][0], // Start of the plot band
                to: listdata.beforeMealGlucoseTargets[0][1]
            }, {
                // Before Meal PLOT
                color: 'rgba(0,255,0,.20)', // Color value
                from: listdata.afterMealGlucoseTargets[0][0], // Start of the plot band
                to: listdata.afterMealGlucoseTargets[0][1]
            }]
        },
        legend: {
            align: 'center',
            x: 10,
            verticalAlign: 'bottom',
            y: 23,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            shadow: false
        },
        plotOptions: {
            dataLabels: {
                enabled: true,
                x: 40,
                formatter: function () {
                    return this.point.name;
                },
                style: {
                    color: "black"
                }
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (event) {
                            window.location.href = "fetchLogDetail.action?patientId="+patId+"&logId="+this.l;
                        }
                    }
                }
            },
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                }
            }
        },
        tooltip: {
            snap: 0,
            enabled: true,
            shared: false,
            formatter: function(){
                return "<b>" + this.series.name + "</b> <br/>" + HSDateUtils.formatDateFromTicks(this.point.t, 'MM/DD/YYYY hh:mm A') + " , " + this.point.y + " mg/dl <br/>" + this.point.z;
            }
        },
        series: [{
            stickyTracking: false,
            name: 'Under Target',
            color: '#ECB648',
            marker: {
                symbol: 'circle',
                fillColor: "#ECB648",
                lineColor: "#ECB648",
                radius: 5
            },
            pointInterval: 36e5,
            dataLabels: {
                enabled: true,
                style: {
                    color: '#ECB648'
                }
            }

        }, {
                name: 'In Target',
                color: '#5EA647',
                marker: {
                    symbol: 'circle',
                    fillColor: "#5EA647",
                    lineColor: "#5EA647",
                    radius: 5
                },
                pointInterval: 36e5,
                dataLabels: {
                    enabled: true,
                    style: {
                        color: '#5EA647'
                    }
                }

            },{

            name: 'Over Target',
            color: '#DA4453',
            marker: {
                symbol: 'circle',
                fillColor: "#DA4453",
                lineColor: "#DA4453",
                radius: 5
            },

            pointInterval: 36e5,
            dataLabels: {
                enabled: true,
                style: {
                    color: '#DA4453'
                }
            }
        }]
    };

    var normalNewList = glucoseSeriesList(listdata.Normal, true);
    var lowNewList = glucoseSeriesList(listdata.Low, true);
    var highNewList = glucoseSeriesList(listdata.High, true);

    options.series[1].data = normalNewList;
    options.series[0].data = lowNewList;
    options.series[2].data = highNewList;
    GlucoseChart = new Highcharts.Chart(options);

    var container = $('#glucoseTargets-c');
    var containerAfterMeal = $('#glucoseTargets-g');
    if (getSelectedTab() == "monthly") {
        container = $('#glucoseTargets-c');
    } else if (getSelectedTab() == "weekly") {
        container = $('#glucoseTargets-b');
        containerAfterMeal = $('#glucoseTargets-g');
    }
    container.text("Before Meal Targets: " + listdata.beforeMealGlucoseTargets[0][0] + "-" + listdata.beforeMealGlucoseTargets[0][1]);
    containerAfterMeal.text("After Meal Targets: " + listdata.afterMealGlucoseTargets[0][0] + "-" + listdata.afterMealGlucoseTargets[0][1]);
}


function drawDailyActivityChart(data) {
    var activityArray = [], steps = 0,
        $dailyStepsLbl = $("#dailyStepsLbl").hide(),
        $containerDailyActivity = $('#container-DailyActivityCombine'),
        minMax = [],
        $date = $("#selDate"),
        minMax = HSDateUtils.dateRangeFromMilli($date.val(), 2);

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    $.each(data.DATA, function (index, activity) {
        var valArray = [];
        valArray.push(activity.timestamp);
        valArray.push(activity.steps);
        activityArray.push(valArray);
        steps += activity.steps;

    });

    if (steps > 0) {
        $dailyStepsLbl.text(steps + " steps").show();
    }

    if($containerDailyActivity.highcharts()){
        $containerDailyActivity.highcharts().destroy();
    }

    $containerDailyActivity.highcharts({
        plotOptions: {
            series: {
                pointWidth: 10
            }
        },
        xAxis: {
            tickInterval: 3600 * 1000,
            min: minMax[0],
            max: minMax[1],
            type: 'datetime',
            minorTickLength: 0,
            tickLength: 0,
            labels: {
                enabled: false,
                formatter: function (){
                    return Highcharts.dateFormat('%I %p',this.value);
                },
                style:{
                    width:'1px'
                },
                step: 1
            },
            dateTimeLabelFormats: {
                hour: '%I %p'
            },
            title: {
                text: ''
            }
        },
        yAxis: {
            min: 0,
            max: data.MAXYAXIS,
            title: {
                text: 'Steps/Hour',
                style: {
                    color: '#f5c430',
                    fontSize: '1.2em',
                    fontWeight: 'bold'
                }
            },
            labels: {
                overflow: 'justify'
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%I:%M %p} , {point.y} steps'
        },
        series: [{
            pointRange: 1,
            data: activityArray,
            name: 'Activity Steps',
            color: '#f5c430',
            dataLabels: {
                enabled: false
            }
        }]
    });
}


function drawDailyActivityMinutesChart(data) {
    var activityArray = [],
        labelsArray = {},
        $container = $('#container-DailyActivityMinutes'),
        minMax = [],
        $date = $("#selDate"),
        minMax = HSDateUtils.dateRangeFromMilli($date.val(), 2);

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    if($container.highcharts()){
        $container.highcharts().destroy();
    }

    $.each(data.DATA, function (index, activity) {
        var valArray = [];
        valArray.push(activity.timestamp);
        valArray.push(activity.minutesPerformed);
        labelsArray[activity.timestamp] = activity.type;
        activityArray.push(valArray);
    });

    $container.highcharts({
        chart: {
            type: 'column'
        },
        plotOptions: {
            series: {
                pointWidth: 10
            },
            column: {
                stacking: 'normal'
            }
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Hours'
            },
            type: 'datetime',
            tickInterval: 3600 * 1000,
            min: minMax[0],
            max: minMax[1],
            labels: {
                enabled: true,
                formatter: function (){
                    return Highcharts.dateFormat('%I %p',this.value);
                },
                style:{
                    width:'1px'
                },
                step: 1
            },
            dateTimeLabelFormats: {
                hour: '%I %p'
            },
            plotBands: {
                color: 'rgba(101, 109, 120, 0.2)', // Color value
                from: 0.5, // Start of the plot band
                to: 2.7
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Activity (minutes)',
                style: {
                    color: '#ff6666',
                    fontSize: '1.2em',
                    fontWeight: 'bold'
                }
            },
            stackLabels: {
                qTotals: labelsArray,
                enabled: true,
                style: {
                    fontWeight: 'bold'
                },
                formatter: function () {
                    return this.options.qTotals[this.x];
                }
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%I:%M %p} , {point.y} mins'
        },
        series: [{
            pointRange: 1,
            data: activityArray,
            name: 'Activity Minutes',
            color: '#999999'
        }]
    });
}

function drawGlucoseChart(glucoseSummaryData, glucoseSummaryDataAfterMeal, glucoseSummaryDataNotAfterMeal) {
    var avgGlucoseContainer = $("#avgGlucose-b").html("N/A"),
        avgGlucoseTest = $("#avgGlucoseTest-b").html(" 0"),
        avgGlucoseContainerAfterMeal = $("#avgGlucose-g").html("N/A"),
        avgGlucoseTestAfterMeal = $("#avgGlucoseTest-g").html(" 0");

    Highcharts.setOptions({
        colors: ['#ECB648', '#DA4453', '#5EA647']
    });

    var containerAfterMeal = $('#container-weeklyGlucoseAfterMeal');
    var containerNotAfterMeal = $('#container-weeklyGlucoseNotAfterMeal');
    if(containerAfterMeal.highcharts()){
        containerAfterMeal.highcharts().destroy();
        $("#container-weeklyGlucoseAfterMeal").text("No Log Found");
        $("#container-weeklyGlucoseAfterMeal").addClass("week-no-record");
    }
    if(containerNotAfterMeal.highcharts()){
        containerNotAfterMeal.highcharts().destroy();
        $("#container-weeklyGlucoseNotAfterMeal").text("No Log Found");
        $("#container-weeklyGlucoseNotAfterMeal").addClass("week-no-record");
    }

    if (glucoseSummaryDataNotAfterMeal && glucoseSummaryDataNotAfterMeal[0]) {
        avgGlucoseContainer.html(" <b>" + glucoseSummaryDataNotAfterMeal[0].toFixed(0) + " mg/dl</b>");
        avgGlucoseTest.html(" " + glucoseSummaryDataNotAfterMeal[4]);

        containerNotAfterMeal.highcharts({
            exporting: {enabled: false},
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '<b>{point.percentage:.0f}%</b> of Total'
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    size: '60%',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.0f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: '',
                data: [
                    ['Under Target', glucoseSummaryDataNotAfterMeal[1]],
                    ['Over Target', glucoseSummaryDataNotAfterMeal[3]],
                    ['In Target', glucoseSummaryDataNotAfterMeal[2]]
                ]
            }]
        });
    }
    if (glucoseSummaryDataAfterMeal && glucoseSummaryDataAfterMeal[0]) {
        avgGlucoseContainerAfterMeal.html(" <b>" + glucoseSummaryDataAfterMeal[0].toFixed(0) + " mg/dl</b>");
        avgGlucoseTestAfterMeal.html(" " + glucoseSummaryDataAfterMeal[4]);

        containerAfterMeal.highcharts({
            exporting: {enabled: false},
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '<b>{point.percentage:.0f}%</b> of Total'
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    size: '60%',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.0f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: '',
                data: [
                    ['Under Target', glucoseSummaryDataAfterMeal[1]],
                    ['Over Target', glucoseSummaryDataAfterMeal[3]],
                    ['In Target', glucoseSummaryDataAfterMeal[2]]
                ]
            }]
        });
    }

}
function getGuageOptions() {

    var gaugeOptions = {

        exporting: {enabled: false},

        chart: {
            type: 'solidgauge'
        },

        title: null,

        pane: {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            stops: [
                [0.1, '#5EA647'], // green
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
            labels: {
                y: 16
            }
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };

    return gaugeOptions;
}

/*----------------- DRAWING WEEKLY DAYS CAHRTS -----------------*/
function drawWeeklyMealChart(weeklyTrendData) {
    /* Creating categories DATA  */
    var categories = ['Breakfast', 'Lunch', 'Dinner', 'Snack'];
    if (weeklyTrendData.Weekly_Meal) {
        var meals = weeklyTrendData.Weekly_Meal;
        categories = populateCategoriesValues(meals);
    }

    /* Creating Avg Meal DATA  */
    var averageMeals = [];
    if (weeklyTrendData.Meal_AVG) {
        var mealsAverage = weeklyTrendData.Meal_AVG;
        averageMeals = populateMealAverageValues(mealsAverage);
    }
    var mealTargets = [];
    mealTargets = $.grep(weeklyTrendData.Target, function (v, index) {
        if (v == 0) {
            v = null;
        } else {
            Math.round(v)
        }
        return true;
    });

    // Create the chart
    $('#container-weeklyMeal').highcharts({
        chart: {
            type: 'column'
        },
        xAxis: {
            categories: categories,
            type: 'category'
        },
        yAxis: [{
            title: {
                text: 'Average Meal Carbs'
            }
        }, {
            title: {
                text: ''
            },
            opposite: true
        }],
        plotOptions: {
            column: {
                showInLegend: true,
                grouping: false,
                shadow: false,
                borderWidth: 0,
                dataLabels: {
                    color: 'rgb(0,0,0)',
                    enabled: true,
                    format: '{point.y:0f}g'
                }
            }, series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            var category = this.category.split('<br/>')[0];
                            window.location.href = "myMealsPage.action?patientId=" + patId + "&mealType=" + category + "&date=" + $("#selMonthDate").val() + "&processed=true";
                        }
                    }
                }
            }
        },
        tooltip: {
            shared: true
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        title: {
            text: ''
        },
        series: [{
            dataLabels: {
                enabled: false
            },
            name: 'Carb Ceiling',
            data: mealTargets,
            color: 'rgba(192,192,192,0.7)',
            tooltip: {
                valueSuffix: 'g'
            }
        }, {
            name: 'Average',
            color: 'rgba(245,196,48,1)',
            pointPadding: 0.3,
            data: averageMeals,
            tooltip: {
                valueSuffix: 'g'
            }

        }]
    });
}
function drawWeeklyMedicationTrendChart(data) {
    var $weeklyMedicationChart = $('#container-weeklyMedLog');
    var medications = data.Medication;
    if (medications.length == 0) {
        $weeklyMedicationChart.html("<span class='centered-msg'>No Log Found</span>");
    } else {
        var medsArray = [];
        $.each(medications, function (index, med) {
            var valArray = [];
            valArray.push(med.timestamp);
            valArray.push(med.medsTaken);
            valArray.push(med.total);
            medsArray.push(valArray);
        });

        $weeklyMedicationChart.highcharts({
            xAxis: {
                tickInterval: 24 * 3600 * 1000,
                min: data.MINMAX[0],
                max: data.MINMAX[1],
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e %b',
                    year: '%b'
                },
                title: {
                    text: '',
                    align: 'high'
                }
            },
            yAxis: {
                min: 0,
                tickInterval: 1,
                title: {
                    text: ' '
                }
            },
            exporting: {
                enabled: false
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                formatter: function () {
                    var fromMeds = medsArray[this.point.index];
                    return '<b>' + this.series.name + '</b><br/>' + fromMeds[1] + " med(s) taken";
                }
            },
            series: [{
                data: medsArray,
                name: 'Took Meds',
                color: '#5EA647',
                dataLabels: {
                    enabled: true,
                    style: {
                        fontSize: '8px',
                        format: '{point.y:0f}',
                        color: '#5EA647'
                    }
                }
            }]
        });
    }
}


function drawWeeklyActivityChart(data) {
    var activityArray = [],
        steps = 0,
        $containerWeeklyActivity = $('#container-weeklyActivity'),
        $date = $("#selWeekDate"),
        minMax = HSDateUtils.dateRangeFromRange($date.val(), 7, true);

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    if($containerWeeklyActivity.highcharts()) {
        $containerWeeklyActivity.highcharts().destroy();
    }

    $.each(data.STEPS, function (index, actSteps) {
        var valArray = [];
        valArray.push(HSDateUtils.getStartOfDayTime(actSteps[1]));
        valArray.push(actSteps[0]);
        activityArray.push(valArray);
    });

    $containerWeeklyActivity.highcharts({
        chart: {
            type: 'column'
        },
        plotOptions: {
            series: {
                pointWidth: 10
            }
        },
        xAxis: {
            labels: {
                enabled: false
            },
            tickInterval: 3600 * 1000 * 24,
            min: minMax[0],
            max: minMax[6],
            type: 'datetime',
            startOnTick: true,
            endOnTick: true,
            showLastLabel: false,
            tickLength: 0,
            dateTimeLabelFormats: {
                day: '%a'
            },
            title: {
                text: ''
            }
        },
        yAxis: {
            min: 0,
            max: data.MAXYAXIS,
            title: {
                text: 'Total steps per day',
                style: {
                    color: '#f5c430',
                    fontSize: '1.2em',
                    fontWeight: 'bold'
                }
            },
            labels: {
                overflow: 'justify'
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.y} steps'
        },
        series: [{
            pointRange: 1,
            data: activityArray,
            name: 'Activity Steps',
            color: '#f5c430',
            dataLabels: {
                enabled: true,
                formatter:function(){
                    if(this.y > 0)
                        return this.y + ' Steps';
                }
            }
        }]
    });
}

function drawWeeklyActivityMinutesChart(data) {
    var activityArray = [],
        labelsArray = [],
        $containerWeeklyActivityMinutes = $('#container-WeeklyActivityMinutes'),
        minMax = [],
        $date = $("#selWeekDate");
        minMax = HSDateUtils.dateRangeFromRange($date.val(), 7, true);

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    if($containerWeeklyActivityMinutes.highcharts()) {
        $containerWeeklyActivityMinutes.highcharts().destroy();
    }

    $.each(data.DATA, function (index, activity) {
        var valArray = [];
        valArray.push(HSDateUtils.getStartOfDayTime(activity.timestamp));
        //valArray.push(activity.timestamp);
        valArray.push(activity.activityMinutes);
        valArray.push(activity.activityMinutesTypes);
        valArray.push(activity.date);

        if(!labelsArray[HSDateUtils.getStartOfDayTime(activity.timestamp)]){
            labelsArray[HSDateUtils.getStartOfDayTime(activity.timestamp)] = activity.activityMinutesTypes;
        }

        if(activity.activityMinutesTypes){
            activityArray.push(valArray);
        }
    });

    $containerWeeklyActivityMinutes.highcharts({
        chart: {
            type: 'column'
        },
        plotOptions: {
            series: {
                pointWidth: 10
            },
            column: {
                stacking: 'normal'
            }
        },

        xAxis: {
            title: {
                enabled: true,
                text: 'Days'
            },
            labels: {
                useHTML:true,
                style: {
                    textTransform: "uppercase"
                }
            },
            tickInterval: 24 * 3600 * 1000,
            min: minMax[0],
            max: minMax[6],
            type: 'datetime',
            startOnTick: true,
            endOnTick: true,
            showLastLabel: false,
            dateTimeLabelFormats: { // don't display the dummy year
                day: '%a'
            },

            plotBands: {
                color: 'rgba(101, 109, 120, 0.2)', // Color value
                from: 0.5, // Start of the plot band
                to: 2.7
            }
        },

        yAxis: {
            min: 0,
            //max: data.MAXYAXIS[0],
            title: {
                text: 'Activity (minutes)',
                style: {
                    color: '#ff6666',
                    fontSize: '1.2em',
                    fontWeight: 'bold'
                }
            },
            stackLabels: {
                qTotals: labelsArray,
                enabled: true,
                style: {
                    fontWeight: 'bold'
                },
                useHTML: true,
                formatter: function () {
                    var topLabels = this.options.qTotals[this.x];
                    return "<span title='"+topLabels+"'>" + topLabels + "</span>";
                }
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.y} mins'
        },
        series: [{
            pointRange: 1,
            data: activityArray,
            name: 'Activity Minutes',
            color: '#999999'
        }]
    });
}


function drawWeeklyDaysScatterGlucoseChart(listdata) {
    var $date = $("#selWeekDate"),
        minMax = HSDateUtils.dateRangeFromRange($date.val(), 7),
        maxYAxis = getMaxValueInArray(listdata.maxYAxis, 200);

    loadGlucoseChartLegend(listdata);

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    var options = {
        dataLabels: {
            enabled: true,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor)
            || 'black',
            style: {
                textShadow: '0 0 3px black, 0 0 3px black'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        chart: {
            renderTo: 'container-weeklyGlucose',
            type: 'scatter',
            zoomType: 'xy',
            ignoreHiddenSeries: false
        },
        title: {
            text: ''
        },
        /*  subtitle: {
         text: 'Source: Heinz  2003'
         },  */
        xAxis: {
            title: {
                enabled: true,
                text: 'Time'
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true,
            type: 'datetime',
            tickInterval: 3600 * 2000,
            min: minMax[0],
            max: minMax[1],
            dateTimeLabelFormats: {
                hour: '%I %p',
                minute: '%I:%M %p'
            },
            labels: {
                formatter: function (){
                    return Highcharts.dateFormat('%I %p',this.value);
                }
            }
        },
        yAxis: {
            min: 0,
            max: maxYAxis,
            title: {
                text: 'Glucose (mg/dL)'
            },
            floor: 0,
            plotBands: [{
                // After Meal PLOT
                color: 'rgba(0,255,0,.50)', // Color value
                from: listdata.beforeMealGlucoseTargets[0][0], // Start of the plot band
                to: listdata.beforeMealGlucoseTargets[0][1]

                // End of the plot band
            }, {
                // Before Meal PLOT
                color: 'rgba(0,255,0,.25)', // Color value
                from: listdata.afterMealGlucoseTargets[0][0], // Start of the plot band
                to: listdata.afterMealGlucoseTargets[0][1]
            }]
        },
        legend: {
            align: 'center',
            x: 10,
            verticalAlign: 'bottom',
            y: 23,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2)
            || 'white',
            /*   borderColor: '#CCC',
             borderWidth: 1, */
            shadow: false
        },
        plotOptions: {
            dataLabels: {
                enabled: true,
                x: 40,
                formatter: function () {
                    return this.point.name;
                },
                style: {
                    color: "black"
                }
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (event) {
                            window.location.href = "fetchLogDetail.action?patientId="+patId+"&logId="+this.l;
                        }
                    }
                }
            },
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: true
                        }
                    }
                }
            }
        },
        tooltip: {
            snap: 0,
            enabled: true,
            shared: false,
            formatter: function(){
                return "<b>" + this.series.name + "</b> <br/>" + HSDateUtils.formatDateFromTicks(this.point.t, 'MM/DD/YYYY hh:mm A') + " , " + this.point.y + " mg/dl <br/>" + this.point.z;
            }
        },
        series: [{
            stickyTracking: false,
            name: 'Under Target',
            color: '#ECB648',
            marker: {
                symbol: 'circle',
                fillColor: "#ECB648",
                lineColor: "#ECB648",
                radius: 5
            },

            pointInterval: 36e5,
            dataLabels: {
                enabled: true,

                style: {

                    color: '#ECB648'
                }
            }

        }, {
                name: 'In Target',
                color: '#5EA647',
                marker: {
                    symbol: 'circle',
                    fillColor: "#5EA647",
                    lineColor: "#5EA647",
                    radius: 5
                },

                pointInterval: 36e5,
                dataLabels: {
                    enabled: true,

                    style: {

                        color: '#5EA647'
                    }
                }
            }, {
            name: 'Over Target',
            color: '#DA4453',
            marker: {
                symbol: 'circle',
                fillColor: "#DA4453",
                lineColor: "#DA4453",
                radius: 5
            },

            pointInterval: 36e5,
            dataLabels: {
                enabled: true,

                style: {

                    color: '#DA4453'
                }
            }

        }

        ]
    };

    var normalNewList = glucoseSeriesList(listdata.Normal, true);
    var lowNewList = glucoseSeriesList(listdata.Low, true);
    var highNewList = glucoseSeriesList(listdata.High, true);

    options.series[1].data = normalNewList;
    options.series[0].data = lowNewList;
    options.series[2].data = highNewList;
    GlucoseChart = new Highcharts.Chart(options);

}

/*----------------- DRAWING THREE DAYS CAHRTS -----------------*/
function drawThreeDaysMealChart(threeDaysTrendData) {
    /* Creating categories DATA  */
    var categories = ['Breakfast', 'Lunch', 'Dinner', 'Snack'];
    if (threeDaysTrendData.three_days_Meal) {
        var meals = threeDaysTrendData.three_days_Meal;
        categories = populateCategoriesValues(meals);
    }

    /* Creating Avg Meal DATA  */
    var averageMeals = [];
    if (threeDaysTrendData.Meal_AVG) {
        var mealsAverage = threeDaysTrendData.Meal_AVG;
        averageMeals = populateMealAverageValues(mealsAverage);
    }

    var mealTargets = [];
    mealTargets = $.grep(threeDaysTrendData.Target, function (v, index) {
        if (v == 0) {
            v = null;
        } else {
            Math.round(v)
        }
        return true;
    });

    // Create the chart
    $('#container-ThreeDaysMeal').highcharts({
        chart: {
            type: 'column'
        },
        xAxis: {
            categories: categories,
            type: 'category'
        },
        yAxis: [{
            title: {
                text: 'Average Meal Carbs'
            }
        }, {
            title: {
                text: ''
            },
            opposite: true
        }],
        plotOptions: {
            column: {
                showInLegend: true,
                grouping: false,
                shadow: false,
                borderWidth: 0,
                dataLabels: {
                    color: 'rgb(0,0,0)',
                    enabled: true,
                    format: '{point.y:0f}g'
                }
            }, series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            var category = this.category.split('<br/>')[0];
                            window.location.href = "myMealsPage.action?patientId=" + patId + "&mealType=" + category + "&date=" + $("#selMonthDate").val() + "&processed=true";
                        }
                    }
                }
            }
        },
        tooltip: {
            shared: true
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        title: {
            text: ''
        },
        series: [{
            dataLabels: {
                enabled: false
            },
            name: 'Carb Ceiling',
            data: mealTargets,
            color: 'rgba(192,192,192,0.7)',
            tooltip: {
                valueSuffix: 'g'
            }
        }, {
            name: 'Average',
            color: 'rgba(245,196,48,1)',
            pointPadding: 0.3,
            data: averageMeals,
            tooltip: {
                valueSuffix: 'g'
            }

        }]
    });
}
function drawThreeDaysGlucoseChart(glucoseSummaryData, glucoseSummaryDataAfterMeal, glucoseSummaryDataNotAfterMeal) {

    var glucoseTargetsBeforeMeal = $("#spThreeDaysGlucoseTarget");
    var glucoseTargetsAfterMeal = $("#spThreeDaysGlucoseTarget-g");

    glucoseTargetsBeforeMeal.text("Before Meal Targets: " + glucoseSummaryData.beforeMinTarget + "-" + glucoseSummaryData.beforeMaxTarget);
    glucoseTargetsAfterMeal.text("After Meal Targets: " + glucoseSummaryData.afterMinTarget + "-" + glucoseSummaryData.afterMaxTarget);

    //glucoseTargets.text("Before Meal: " + data.beforeMinTarget + "-" + data.beforeMaxTarget + " | After Meal: " + data.afterMinTarget + "-" + data.afterMaxTarget);

    var avgGlucoseContainer = $("#spThreeDaysGlucoseAvg").html("N/A"),
        testGlucose = $("#spThreeDaysGlucoseTest").html(" 0"),
        avgGlucoseContainerAfterMeal = $("#spThreeDaysGlucoseAvg-g").html("N/A"),
        testGlucoseAfterMeal = $("#spThreeDaysGlucoseTest-g").html(" 0");
    var container = $('#container-ThreeDaysGlucose');
    var containerAfterMeal = $('#container-ThreeDaysGlucose-g');

    if(containerAfterMeal.highcharts()){
        containerAfterMeal.highcharts().destroy();
        $("#container-ThreeDaysGlucose-g").text("No Log Found");
        $("#container-ThreeDaysGlucose-g").addClass("week-no-record");
    }
    if(container.highcharts()){
        container.highcharts().destroy();
        $("#container-ThreeDaysGlucose").text("No Log Found");
        $("#container-ThreeDaysGlucose").addClass("week-no-record");
    }

    if (glucoseSummaryDataNotAfterMeal && glucoseSummaryDataNotAfterMeal[0]) {
        avgGlucoseContainer.html(" <b>" + glucoseSummaryDataNotAfterMeal[0].toFixed(0) + " mg/dl</b>");
        testGlucose.html(" " + glucoseSummaryDataNotAfterMeal[4]);

        container.highcharts({
            exporting: {enabled: false},
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.percentage:.0f}%</b> of Total'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    size: '60%',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.0f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: '',
                data: [
                    ['Under Target', glucoseSummaryDataNotAfterMeal[1]],
                    ['Over Target', glucoseSummaryDataNotAfterMeal[3]],
                    ['In Target', glucoseSummaryDataNotAfterMeal[2]]
                ]
            }]
        });
    }

    if (glucoseSummaryDataAfterMeal && glucoseSummaryDataAfterMeal[0]) {
        avgGlucoseContainerAfterMeal.html(" <b>" + glucoseSummaryDataAfterMeal[0].toFixed(0) + " mg/dl</b>");
        testGlucoseAfterMeal.html(" " + glucoseSummaryDataAfterMeal[4]);

        containerAfterMeal.highcharts({
            exporting: {enabled: false},
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.percentage:.0f}%</b> of Total'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    size: '60%',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.0f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: '',
                data: [
                    ['Under Target', glucoseSummaryDataAfterMeal[1]],
                    ['Over Target', glucoseSummaryDataAfterMeal[3]],
                    ['In Target', glucoseSummaryDataAfterMeal[2]]
                ]
            }]
        });
    }

    Highcharts.setOptions({
        colors: ['#ECB648', '#DA4453', '#5EA647']
    });
}

function drawThreeDaysScatterGlucoseChart(listdata) {
   var $date = $("#selThreeDaysDate"),
       minMax = HSDateUtils.dateRangeFromRange($date.val(), 4),
       maxYAxis = getMaxValueInArray(listdata.maxYAxis, 200);

    loadGlucoseChartLegend(listdata);

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    var options = {
        dataLabels: {
            enabled: true,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor)
            || 'black',
            style: {
                textShadow: '0 0 3px black, 0 0 3px black'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        chart: {
            renderTo: 'container-ThreeDaysGlucose',
            type: 'scatter',
            zoomType: 'xy',
            ignoreHiddenSeries: false
        },
        title: {
            text: ''
        },
        /*  subtitle: {
         text: 'Source: Heinz  2003'
         },  */
        xAxis: {
            title: {
                enabled: true,
                text: 'Time'
            },
            labels: {
                formatter: function (){
                    return Highcharts.dateFormat('%I %p',this.value);
                },
                style:{
                    width:'1px'
                },
                step: 1
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true,
            type: 'datetime',
            tickInterval: 3600 * 2000,
            min: minMax[0],
            max: minMax[1],
            dateTimeLabelFormats: {
                hour: '%I %p',
                minute: '%I:%M %p'
            }

        },
        yAxis: {
            min: 0,
            max: maxYAxis,
            title: {
                text: 'Glucose (mg/dL)'
            },
            floor: 0,
            plotBands: [{
                // After Meal PLOT
                color: 'rgba(0,255,0,.50)', // Color value // Color value
                from: listdata.beforeMealGlucoseTargets[0][0], // Start of the plot band
                to: listdata.beforeMealGlucoseTargets[0][1]

                // End of the plot band
            }, {
                // Before Meal PLOT
                color: 'rgba(0,255,0,.25)', // Color value // Color value
                from: listdata.afterMealGlucoseTargets[0][0], // Start of the plot band
                to: listdata.afterMealGlucoseTargets[0][1]
            }]
        },
        legend: {
            align: 'center',
            x: 10,
            verticalAlign: 'bottom',
            y: 23,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2)
            || 'white',
            /*   borderColor: '#CCC',
             borderWidth: 1, */
            shadow: false
        },
        plotOptions: {
            dataLabels: {
                enabled: true,
                x: 40,
                formatter: function () {
                    return this.point.name;
                },
                style: {
                    color: "black"
                }
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (event) {
                            window.location.href = "fetchLogDetail.action?patientId="+patId+"&logId="+this.l;
                        }
                    }
                }
            },
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: true
                        }
                    }
                }
            }
        },
        tooltip: {
            snap: 0,
            enabled: true,
            shared: false,
            formatter: function(){
                return "<b>" + this.series.name + "</b> <br/>" + HSDateUtils.formatDateFromTicks(this.point.t, 'MM/DD/YYYY hh:mm A') + " , " + this.point.y + " mg/dl <br/>" + this.point.z;
            }
        },
        series: [{
            stickyTracking: false,
            name: 'Under Target',
            color: '#ECB648',
            marker: {
                symbol: 'circle',
                fillColor: "#ECB648",
                lineColor: "#ECB648",
                radius: 5
            },

            pointInterval: 36e5,
            dataLabels: {
                enabled: true,

                style: {

                    color: '#ECB648'
                }
            }

        }, {
                name: 'In Target',
                color: '#5EA647',
                marker: {
                    symbol: 'circle',
                    fillColor: "#5EA647",
                    lineColor: "#5EA647",
                    radius: 5
                },

                pointInterval: 36e5,
                dataLabels: {
                    enabled: true,

                    style: {

                        color: '#5EA647'
                    }
                }

            }, {

            name: 'Over Target',
            color: '#DA4453',
            marker: {
                symbol: 'circle',
                fillColor: "#DA4453",
                lineColor: "#DA4453",
                radius: 5
            },

            pointInterval: 36e5,
            dataLabels: {
                enabled: true,

                style: {

                    color: '#DA4453'
                }
            }

        }

        ]
    };

    var normalNewList = glucoseSeriesList(listdata.Normal, true);
    var lowNewList = glucoseSeriesList(listdata.Low, true);
    var highNewList = glucoseSeriesList(listdata.High, true);

    options.series[1].data = normalNewList;
    options.series[0].data = lowNewList;
    options.series[2].data = highNewList;
    GlucoseChart = new Highcharts.Chart(options);

}

function drawThreeDaysMedicationTrendChart(data) {
    var $monthlyMedicationChart = $('#container-ThreeDaysMedication');
    var medications = data.Medication;
    if (medications.length == 0) {
        $monthlyMedicationChart.html("<span class='centered-msg'>No Log Found</span>");
    } else {
        var medsArray = [];
        $.each(medications, function (index, med) {
            var valArray = [];
            valArray.push(med.timestamp);
            valArray.push(med.medsTaken);
            valArray.push(med.total);
            medsArray.push(valArray);
        });

        $monthlyMedicationChart.highcharts({
            xAxis: {
                tickInterval: 24 * 3600 * 1000,
                min: data.MINMAX[0],
                max: data.MINMAX[1],
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e %b',
                    year: '%b'
                },
                title: {
                    text: '',
                    align: 'high'
                }
            },
            yAxis: {
                min: 0,
                tickInterval: 1,
                title: {
                    text: ' '
                }
            },
            exporting: {
                enabled: false
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                formatter: function () {
                    var fromMeds = medsArray[this.point.index];
                    return '<b>' + this.series.name + '</b><br/>' + fromMeds[1] + " med(s) taken";
                }
            },
            series: [{
                data: medsArray,
                name: 'Took Meds',
                color: '#5EA647',
                dataLabels: {
                    enabled: true,
                    style: {
                        fontSize: '8px',
                        format: '{point.y:0f}',
                        color: '#5EA647'
                    }
                }
            }]
        });
    }
}
function drawThreeDaysWeightChart(data) {
    var weightArray = [];
    var weightString = "Starting Weight: <b> " + data.STARTING_WEIGHT + "(lbs)</b><br/>Target Weight: <b>" + data.TARGET_WEIGHT + "(lbs)</b>";
    $('#threeDaysTargetWeights').html(weightString);
    var emptyCount = 0;
    $.each(data.DATA, function (index, weight) {
        var valArray = [];
        valArray.push(weight.timestamp);
        valArray.push(weight.weightLost);
        weightArray.push(valArray);
        emptyCount++;
    });

    if (emptyCount == 0) {
        $('#container-ThreeDaysWeightLoss').html("<span class='centered-msg no-log-error-center'>No Log Found</span>");
    } else {
        $('#container-ThreeDaysWeightLoss').highcharts({
            chart: {
                type: 'line'
            },
            xAxis: {
                tickInterval: 24 * 3600 * 1000,
                min: data.MINMAX[0],
                max: data.MINMAX[1],
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    day: '%e. %b',
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Days',
                    align: 'high'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Pounds',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            exporting: {
                enabled: false
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.0f} lbs'
            },

            series: [{
                data: weightArray,
                /* pointStart: Date.UTC(2015, 0, 0), */
                name: 'Weight',
                color: '#5EA647',
                //pointInterval : 24 * 3600 * 1000, // one day
                dataLabels: {
                    enabled: true,

                    style: {
                        format: '{point.y:.1f}',
                        color: '#5EA647'
                    }
                }
            }]
        });
    }
}
function drawThreeDaysActivityChart(data) {
    var activityArray = [],
        $firstStepsTitle = $("#firstStepsTitle").hide(),
        $secondStepsTitle = $("#secondStepsTitle").hide(),
        $thirdStepsTitle = $("#thirdStepsTitle").hide(),
        $containerThreeDaysActivity = $('#container-ThreeDaysActivityCombine'),
        $date = $("#selThreeDaysDate"),
        minMax = HSDateUtils.dateRangeFromRange($date.val(), 4),
        dateMap = [];

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    if($containerThreeDaysActivity.highcharts()) {
        $containerThreeDaysActivity.highcharts().destroy();
    }

    $.each(minMax, function (index, date) {
        var formatedDate = HSDateUtils.formatDateFromTicks(date, "MM/DD/YYYY");
        dateMap[formatedDate] = 0;
    });

    $.each(data.DATA, function (index, activity) {
        var valArray = [],
            activityFormatedDate = HSDateUtils.formatDateFromTicks(activity.timestamp, "MM/DD/YYYY");
        valArray.push(activity.timestamp);
        valArray.push(activity.steps);
        activityArray.push(valArray);
        
        if(dateMap[activityFormatedDate] != null && dateMap[activityFormatedDate] != undefined){
            dateMap[activityFormatedDate] += activity.steps;
        }
    });

    if (dateMap) {

        var d1 = HSDateUtils.formatDateFromTicks(minMax[0], "MM/DD/YYYY"),
            d2 = HSDateUtils.formatDateFromTicks(minMax[1], "MM/DD/YYYY"),
            d3 = HSDateUtils.formatDateFromTicks(minMax[2], "MM/DD/YYYY");

        if(dateMap[d1]){
            $firstStepsTitle.text(dateMap[d1] + " steps").show();
        }

        if(dateMap[d2]){
            $secondStepsTitle.text(dateMap[d2] + " steps").show();
        }

        if(dateMap[d3]){
            $thirdStepsTitle.text(dateMap[d3] + " steps").show();
        }
    }

    $containerThreeDaysActivity.highcharts({
        plotOptions: {
            series: {
                pointWidth: 10
            }
        },
        global: {
            useUTC: false
        },
        xAxis: {
            tickInterval: 3600 * 1000 * 4,
            min: minMax[0],
            max: minMax[3],
            type: 'datetime',
            startOnTick: true,
            endOnTick: true,
            minorTickLength: 0,
            tickLength: 0,
            labels: {
                enabled: false,
                overflow: 'justify',
                formatter: function (){
                    return Highcharts.dateFormat('%I %p',this.value);
                },
                style:{
                    width:'1px'
                },
                step: 1
            },
            dateTimeLabelFormats: {
                hour: '%I %p',
                minute: '%I:%M %p'
            },
            title: {
                text: ''
            }
        },
        yAxis: {
            min: 0,
            max: data.MAXYAXIS[0],
            title: {
                text: 'Steps/Hour',
                style: {
                    color: '#f5c430',
                    fontSize: '1.2em',
                    fontWeight: 'bold'
                }
            },
            labels: {
                enabled: true
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%I:%M %p} , {point.y} steps'
        },
        series: [{
            pointRange: 1,
            data: activityArray,
            name: 'Activity Steps',
            color: '#f5c430',
            dataLabels: {
                enabled: false
            }
        }]
    });
}

function drawThreeDaysActivityMinutesChart(data) {
    var activityArray = [],
        labelsArray = {},
        $containerThreeDaysActivityMinutes = $('#container-ThreeDaysActivityMinutes'),
        $date = $("#selThreeDaysDate"),
        minMax = HSDateUtils.dateRangeFromRange($date.val(), 4);

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    if($containerThreeDaysActivityMinutes.highcharts()) {
        $containerThreeDaysActivityMinutes.highcharts().destroy();
    }

    $.each(data.DATA, function (index, activity) {
        var valArray = [];
        valArray.push(activity.timestamp);
        valArray.push(activity.minutesPerformed);
        labelsArray[activity.timestamp] = activity.type;
        activityArray.push(valArray);
    });

    $containerThreeDaysActivityMinutes.highcharts({
        chart: {
            type: 'column'
        },
        plotOptions: {
            series: {
                pointWidth: 10
            },
            column: {
                stacking: 'normal'
            }
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Hours'
            },
            type: 'datetime',
            startOnTick:true,
            endOnTick: true,
            tickInterval: 3600 * 1000 * 4,
            min: minMax[0],
            max: minMax[3],
            labels: {
                enabled: true,
                overflow: 'justify',
                formatter: function (){
                    return Highcharts.dateFormat('%I %p',this.value);
                },
                style:{
                    width:'1px'
                },
                step: 1
            },
            dateTimeLabelFormats: {
                hour: '%I %p',
                minute: '%I:%M %p'
            },
            plotBands: {
                color: 'rgba(101, 109, 120, 0.2)', // Color value
                from: 0.5, // Start of the plot band
                to: 2.7
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Activity (minutes)',
                style: {
                    color: '#ff6666',
                    fontSize: '1.2em',
                    fontWeight: 'bold'
                }
            },
            stackLabels: {
                qTotals: labelsArray,
                enabled: true,
                style: {
                    fontWeight: 'bold'
                },
                formatter: function () {
                    return this.options.qTotals[this.x];
                }
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%I:%M %p} , {point.y} mins'
        },
        series: [{
            pointRange: 1,
            data: activityArray,
            name: 'Activity Minutes',
            color: '#999999'
        }]
    });
}

function drawThreeDaysComboGlcusoeChart(data) {
    if (data.Normal == 0 && data.Low == 0 && data.High == 0) {
        $('#glucoseLineFirst').addClass('div-first-left');
        $('#glucoseLineSecond').addClass('div-second-left');
        $('#glucoseLineFirst').addClass('div-empty-first-left');
        $('#glucoseLineSecond').addClass('div-empty-second-left');
    }
    var options = {
        dataLabels: {
            enabled: true,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
            style: {
                textShadow: '0 0 3px black, 0 0 3px black'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },

        global: {
            useUTC: false
        },
        chart: {
            renderTo: 'container-ThreeDaysCombineGlucoseChart',
            type: 'scatter',
            zoomType: 'xy',
            ignoreHiddenSeries: false
        },
        title: {
            text: ''
        },
        xAxis: {
            title: {
                enabled: true,
                text: ' '
            },
            type: 'datetime',
            tickInterval: 3600 * 1000 * 4,
            min: data.MinMax[0],
            max: data.MinMax[1],
            minorTickLength: 0,
            lineWidth: 0,
            tickLength: 0,
            labels: {
                enabled: false
            },
            dateTimeLabelFormats: {
                hour: '%I %p'
            }
        },
        yAxis: {
            min: 0,
            max: 300,
            title: {
                text: 'Glucose (mg/dL)'
            },
            floor: 0,
            plotBands: [{
                // After Meal PLOT
                color: 'rgba(0,255,0,.50)', // Color value
                from: data.beforeMealGlucoseTargets[0][0], // Start of the plot band
                to: data.beforeMealGlucoseTargets[0][1]

                // End of the plot band
            }, {
                // Before Meal PLOT
                color: 'rgba(0,255,0,.25)', // Color value
                from: data.afterMealGlucoseTargets[0][0], // Start of the plot band
                to: data.afterMealGlucoseTargets[0][1]
            }]
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            dataLabels: {
                enabled: true,
                x: 40,
                formatter: function () {
                    return this.point.name;
                },
                style: {
                    color: "black"
                }
            },

            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.x:%I:%M %p} , {point.y} mg/dL'

                }
            }
        },
        series: [{
            name: 'In Target',
            color: '#5EA647',
            marker: {
                symbol: 'circle',
                fillColor: "#5EA647",
                lineColor: "#5EA647",
                radius: 5
            },

            pointInterval: 36e5,
            dataLabels: {
                enabled: true,
                style: {
                    fontSize: '10px',
                    color: '#5EA647'
                }
            }

        }, {
            name: 'Under Target',
            color: '#ECB648',
            marker: {
                symbol: 'circle',
                fillColor: "#ECB648",
                lineColor: "#ECB648",
                radius: 5
            },

            pointInterval: 36e5,
            dataLabels: {
                enabled: true,

                style: {
                    fontSize: '10px',
                    color: '#ECB648'
                }
            }

        }, {
            name: 'Over Target',
            color: '#DA4453',
            marker: {
                symbol: 'circle',
                fillColor: "#DA4453",
                lineColor: "#DA4453",
                radius: 5
            },

            pointInterval: 36e5,
            dataLabels: {
                enabled: true,
                style: {
                    fontSize: '10px',
                    color: '#DA4453'
                }
            }

        }]
    };
    options.series[0].data = data.Normal;
    options.series[1].data = data.Low;
    options.series[2].data = data.High;
    GlucoseChart = new Highcharts.Chart(options);
}
function drawThreeDaysComboActivityChart(data) {
    var activityArray = [];
    if (data.DATA == 0) {
        $('#activityLineFirst').removeClass('div-first-left');
        $('#activityLineSecond').removeClass('div-second-left');
        $('#activityLineFirst').addClass('div-empty-first-left');
        $('#activityLineSecond').addClass('div-empty-second-left');
    }
    $.each(data.DATA, function (index, activity) {
        var valArray = [];
        valArray.push(activity.timestamp);
        valArray.push(activity.steps);
        activityArray.push(valArray);
    });
    $('#container-ThreeDaysCombineActivityChart').highcharts({
        plotOptions: {
            series: {
                pointWidth: 10
            }
        },
        xAxis: {
            tickInterval: 3600 * 1000 * 4,
            min: data.MINMAX[0],
            max: data.MINMAX[1],
            type: 'datetime',
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true,
            minorTickLength: 0,
            tickLength: 0,
            labels: {
                enabled: false
            },
            dateTimeLabelFormats: {
                hour: '%I %p'
            },
            title: {
                text: ''
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Steps/Hour'
            },
            labels: {
                overflow: 'justify'
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%I:%M %p} , {point.y} steps'
        },
        series: [{
            data: activityArray,
            name: 'Activity Steps',
            color: '#f5c430',
            dataLabels: {
                enabled: false
            }
        }]
    });
}
function drawThreeDaysComboCarbsChart(data) {
    var carbsDataArray = [];
    var carbsLabelDataArray = [];
    if (data.DATA.length == 0) {
        $('#medsLineFirst').removeClass('div-first-left');
        $('#medsLineFirst').removeClass('div-second-left');
        $('#medsLineFirst').addClass('div-empty-first-left');
        $('#medsLineSecond').addClass('div-empty-second-left');
        var valArray = [];
        var dataArray = [];
        valArray.push(0);
        valArray.push(100);
        dataArray.push(valArray);
        carbsDataArray.push(dataArray);
        carbsLabelDataArray.push(50);
    }
    $.each(data.DATA, function (index, carbs) {
        var valArray = [];
        var dataArray = [];
        valArray.push(carbs.timestamp);
        valArray.push(0);
        dataArray.push(valArray);
        carbsDataArray.push(dataArray);
        carbsLabelDataArray.push(carbs.carbs + 'g');
    });

    var optionsSeries = [];

    for (index = 0; index < carbsDataArray.length; index++) {
        var newOption = {
            "name": carbsLabelDataArray[index],
            "data": carbsDataArray[index]
        };
        optionsSeries.push(newOption);
    }

    var options = {
        dataLabels: {
            enabled: true,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
            style: {
                textShadow: '0 0 3px black, 0 0 3px black'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        chart: {
            renderTo: 'container-ThreeDaysCombineCarbsChart',
            type: 'scatter'
        },
        title: {
            text: ''
        },
        xAxis: {
            title: {
                enabled: true,
                text: ''
            },
            type: 'datetime',
            tickInterval: 3600 * 1000 * 4,
            min: data.MINMAX[0],
            max: data.MINMAX[1],
            minorTickLength: 0,
            tickLength: 0,
            labels: {
                enabled: false
            },
            dateTimeLabelFormats: {
                hour: '%I %p'
            }
        },
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Carbs'
            },
            labels: {
                enabled: true,
                style: {
                    color: 'transparent'
                }
            },
            plotBands: {
                color: 'rgba(101, 109, 120, 0.2)', // Color value
                from: 0, // Start of the plot band
                to: 100
            }
        },
        plotOptions: {
            series: {
                marker: {
                    enabled: false
                },
                dataLabels: {
                    enabled: true,
                    format: '{series.name}'
                }
            }
        },
        tooltip: {
            headerFormat: '<b>Carbs<br>',
            pointFormat: '{series.name}, {point.x:%I:%M %p}'
        },
        legend: {
            enabled: false
        },
        series: optionsSeries
    };
    GlucoseChart = new Highcharts.Chart(options);
}
function drawThreeDaysComboMedicationChart(data) {
    var medsDataArray = [];
    var valArray = [];
    var dataArray = [];
    if (data.DATA.length == 0) {
        $('#medsLineFirst').removeClass('div-first-left');
        $('#medsLineFirst').removeClass('div-second-left');
        $('#medsLineFirst').addClass('div-empty-first-left');
        $('#medsLineSecond').addClass('div-empty-second-left');
        valArray.push(0);
        valArray.push(0);
        dataArray.push(valArray);
        medsDataArray.push(dataArray);
    }
    var imagePath = data.applicationPath + '/resources/css/images/ic_meds_log_overlay.png';
    $.each(data.DATA, function (index, meds) {
        valArray.push(meds.timestamp);
        valArray.push(100);
        dataArray.push(valArray);
        medsDataArray.push(dataArray);
    });
    var optionsSeries = [];
    for (index = 0; index < medsDataArray.length; index++) {
        var newOption = {
            "name": '',
            "data": medsDataArray[index]
        };
        optionsSeries.push(newOption);
    }

    var options = {
        dataLabels: {
            enabled: true,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
            style: {
                textShadow: '0 0 3px black, 0 0 3px black'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        chart: {
            renderTo: 'container-ThreeDaysCombineMedicationChart',
            type: 'scatter'
        },
        title: {
            text: ''
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Hours'
            },
            type: 'datetime',
            tickInterval: 3600 * 1000 * 4,
            min: data.MINMAX[0],
            max: data.MINMAX[1],
            labels: {
                enabled: true
            },
            dateTimeLabelFormats: {
                hour: '%I %p'
            }, plotBands: {
                color: 'rgba(101, 109, 120, 0.2)', // Color value
                from: 0.5, // Start of the plot band
                to: 2.7
            }
        },
        yAxis: {
            min: 0,
            gridLineWidth: 0,
            minorGridLineWidth: 0,
            title: {
                text: 'Meds'
            },
            labels: {
                style: {
                    color: 'transparent'
                }
            }
        },
        plotOptions: {
            series: {
                marker: {
                    symbol: 'url(' + imagePath + ')',
                    width: 25,
                    height: 25
                },
                dataLabels: {
                    enabled: false
                }
            }
        },
        tooltip: {
            headerFormat: '<b>Medication<br>',
            pointFormat: '{point.x:%I:%M %p}'
        },
        legend: {
            enabled: false
        },
        series: optionsSeries
    };
    GlucoseChart = new Highcharts.Chart(options);
}
/*----------------- DRAWING FROURTEEN DAYS CAHRTS -----------------*/
function drawFourteenDaysMealChart(data) {
    /* Creating categories DATA  */
    var categories = ['Breakfast', 'Lunch', 'Dinner', 'Snack'];
    if (data.fourteen_days_Meal) {
        var meals = data.fourteen_days_Meal;
        categories = populateCategoriesValues(meals);
    }

    /* Creating Avg Meal DATA  */
    var averageMeals = [];
    if (data.Meal_AVG) {
        var mealsAverage = data.Meal_AVG;
        averageMeals = populateMealAverageValues(mealsAverage);
    }

    var mealTargets = [];
    mealTargets = $.grep(data.Target, function (v, index) {
        if (v == 0) {
            v = null;
        } else {
            Math.round(v)
        }
        return true;
    });

    // Create the chart
    $('#container-FourteenDaysMeal').highcharts({
        chart: {
            type: 'column'
        },
        xAxis: {
            categories: categories,
            type: 'category'
        },
        yAxis: [{
            title: {
                text: 'Average Meal Carbs'
            }
        }, {
            title: {
                text: ''
            },
            opposite: true
        }],
        plotOptions: {
            column: {
                showInLegend: true,
                grouping: false,
                shadow: false,
                borderWidth: 0,
                dataLabels: {
                    color: 'rgb(0,0,0)',
                    enabled: true,
                    format: '{point.y:0f}g'
                }
            }, series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            var category = this.category.split('<br/>')[0];
                            window.location.href = "myMealsPage.action?patientId=" + patId + "&mealType=" + category + "&date=" + $("#selMonthDate").val() + "&processed=true";
                        }
                    }
                }
            }
        },
        tooltip: {
            shared: true
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        title: {
            text: ''
        },
        series: [{
            dataLabels: {
                enabled: false
            },
            name: 'Carb Ceiling',
            data: mealTargets,
            color: 'rgba(192,192,192,0.7)',
            tooltip: {
                valueSuffix: 'g'
            }
        }, {
            name: 'Average',
            color: 'rgba(245,196,48,1)',
            pointPadding: 0.3,
            data: averageMeals,
            tooltip: {
                valueSuffix: 'g'
            }

        }]
    });
}
function drawFourteenDaysGlucoseChart(glucoseSummaryData, glucoseSummaryDataAfterMeal, glucoseSummaryDataNotAfterMeal) {
    var glucoseTargetsBeforeMeal = $("#spFourteenDaysGlucoseTarget");
    var glucoseTargetsAfterMeal = $("#spFourteenDaysGlucoseTarget-g");

    glucoseTargetsBeforeMeal.text("Before Meal Targets: " + glucoseSummaryData.beforeMinTarget + "-" + glucoseSummaryData.beforeMaxTarget);
    glucoseTargetsAfterMeal.text("After Meal Targets: " + glucoseSummaryData.afterMinTarget + "-" + glucoseSummaryData.afterMaxTarget);

    //glucoseTargets.text("Before Meal: " + data.beforeMinTarget + "-" + data.beforeMaxTarget + " | After Meal: " + data.afterMinTarget + "-" + data.afterMaxTarget);

    var avgGlucoseContainer = $("#spFourteenDaysGlucoseAvg").html("N/A"),
        testGlucose = $("#spFourteenDaysGlucoseTest").html(" 0"),
        avgGlucoseContainerAfterMeal = $("#spFourteenDaysGlucoseAvg-g").html("N/A"),
        testGlucoseAfterMeal = $("#spFourteenDaysGlucoseTest-g").html(" 0");

    var container = $('#container-FourteenDaysGlucose');
    var containerAfterMeal = $('#container-FourteenDaysGlucoseAfterMeal');

    if(containerAfterMeal.highcharts()){
        containerAfterMeal.highcharts().destroy();
        $("#container-FourteenDaysGlucoseAfterMeal").text("No Log Found");
        $("#container-FourteenDaysGlucoseAfterMeal").addClass("week-no-record");
    }
    if(container.highcharts()){
        container.highcharts().destroy();
        $("#container-FourteenDaysGlucose").text("No Log Found");
        $("#container-FourteenDaysGlucose").addClass("week-no-record");
    }

    if (glucoseSummaryDataNotAfterMeal[0]) {
        avgGlucoseContainer.html(" <b>" + glucoseSummaryDataNotAfterMeal[0].toFixed(0) + " mg/dl</b>");
        testGlucose.html(" " + glucoseSummaryDataNotAfterMeal[4]);

        container.highcharts({
            exporting: {enabled: false},
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.percentage:.0f}%</b> of Total'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    size:'60%',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.0f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: '',
                data: [
                    ['Under Target', glucoseSummaryDataNotAfterMeal[1]],
                    ['Over Target', glucoseSummaryDataNotAfterMeal[3]],
                    ['In Target', glucoseSummaryDataNotAfterMeal[2]]
                ]
            }]
        });
    }

    if (glucoseSummaryDataAfterMeal && glucoseSummaryDataAfterMeal[0]) {
        avgGlucoseContainerAfterMeal.html(" <b>" + glucoseSummaryDataAfterMeal[0].toFixed(0) + " mg/dl</b>");
        testGlucoseAfterMeal.html(" " + glucoseSummaryDataAfterMeal[4]);

        containerAfterMeal.highcharts({
            exporting: {enabled: false},
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.percentage:.0f}%</b> of Total'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    size:'60%',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.0f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: '',
                data: [
                    ['Under Target', glucoseSummaryDataAfterMeal[1]],
                    ['Over Target', glucoseSummaryDataAfterMeal[3]],
                    ['In Target', glucoseSummaryDataAfterMeal[2]]
                ]
            }]
        });
    }

    Highcharts.setOptions({
        colors: ['#ECB648', '#DA4453', '#5EA647']
    });
}

function drawFourteenDaysScatterGlucoseChart(listdata) {
    var $date = $("#selFourteenDaysDate"),
        minMax = HSDateUtils.dateRangeFromRange($date.val(), 14),
        maxYAxis = getMaxValueInArray(listdata.maxYAxis, 200);

    loadGlucoseChartLegend(listdata);

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    var options = {
        dataLabels: {
            enabled: true,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
            style: {
                textShadow: '0 0 3px black, 0 0 3px black'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        chart: {
            renderTo: 'container-fourteenDaysGlucose',
            type: 'scatter',
            zoomType: 'xy',
            ignoreHiddenSeries: false
        },
        title: {
            text: ''
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Time'
            },
            labels: {
                formatter: function (){
                    return Highcharts.dateFormat('%I %p',this.value);
                }
            },
            showLastLabel: true,
            type: 'datetime',
            tickInterval: 3600 * 2000,
            startOnTick: true,
            endOnTick: true,
            min: minMax[0],
            max: minMax[1],
            dateTimeLabelFormats: {
                hour: '%I %p',
                minute: '%I:%M %p'
            }
        },
        yAxis: {
            min: 0,
            max: maxYAxis,
            title: {
                text: 'Glucose (mg/dL)'
            },
            floor: 0,
            plotBands: [{
                // After Meal PLOT
                color: 'rgba(0,255,0,.50)', // Color value
                from: listdata.beforeMealGlucoseTargets[0][0], // Start of the plot band
                to: listdata.beforeMealGlucoseTargets[0][1]
            }, {
                // Before Meal PLOT
                color: 'rgba(0,255,0,.25)', // Color value
                from: listdata.afterMealGlucoseTargets[0][0], // Start of the plot band
                to: listdata.afterMealGlucoseTargets[0][1]
            }]
        },
        legend: {
            align: 'center',
            x: 10,
            verticalAlign: 'bottom',
            y: 23,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            shadow: false
        },
        plotOptions: {
            dataLabels: {
                enabled: true,
                x: 40,
                formatter: function () {
                    return this.point.name;
                },
                style: {
                    color: "black"
                }
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (event) {
                            window.location.href = "fetchLogDetail.action?patientId="+patId+"&logId="+this.l;
                        }
                    }
                }
            },
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: true
                        }
                    }
                }
            }
        },
        tooltip: {
            snap: 0,
            enabled: true,
            shared: false,
            formatter: function(){
                return "<b>" + this.series.name + "</b> <br/>" + HSDateUtils.formatDateFromTicks(this.point.t, 'MM/DD/YYYY hh:mm A') + " , " + this.point.y + " mg/dl <br/>" + this.point.z;
            }
        },
        series: [{
            stickyTracking: false,
            name: 'Under Target',
            color: '#ECB648',
            marker: {
                symbol: 'circle',
                fillColor: "#ECB648",
                lineColor: "#ECB648",
                radius: 5
            },

            pointInterval: 3600 * 2000,
            dataLabels: {
                enabled: true,
                style: {
                    color: '#ECB648'
                }
            }

        }, {
                name: 'In Target',
                color: '#5EA647',
                marker: {
                    symbol: 'circle',
                    fillColor: "#5EA647",
                    lineColor: "#5EA647",
                    radius: 5
                },

                pointInterval: 3600 * 2000,
                dataLabels: {
                    enabled: true,
                    style: {
                        color: '#5EA647'
                    }
                }

            }, {
            name: 'Over Target',
            color: '#DA4453',
            marker: {
                symbol: 'circle',
                fillColor: "#DA4453",
                lineColor: "#DA4453",
                radius: 5
            },

            pointInterval: 3600 * 2000,
            dataLabels: {
                enabled: true,
                style: {
                    color: '#DA4453'
                }
            }
        }]
    };

    var normalNewList = glucoseSeriesList(listdata.Normal, true);
    var lowNewList = glucoseSeriesList(listdata.Low, true);
    var highNewList = glucoseSeriesList(listdata.High, true);

    options.series[1].data = normalNewList;
    options.series[0].data = lowNewList;
    options.series[2].data = highNewList;
    GlucoseChart = new Highcharts.Chart(options);
}

function drawFourteenDaysMedicationTrendChart(data) {
    var $fourteenDaysMedicationChart = $('#container-FourteenDaysMedication');
    var medications = data.Medication;
    if (medications.length == 0) {
        $fourteenDaysMedicationChart.html("<span class='centered-msg'>No Log Found</span>");
    } else {
        var medsArray = [];
        $.each(medications, function (index, med) {
            var valArray = [];
            valArray.push(med.timestamp);
            valArray.push(med.medsTaken);
            medsArray.push(valArray);
        });
        $fourteenDaysMedicationChart.highcharts({
            xAxis: {
                tickInterval: 24 * 3600 * 1000,
                min: data.MINMAX[0],
                max: data.MINMAX[1],
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e %b',
                    year: '%b'
                },
                title: {
                    text: '',
                    align: 'high'
                }
            },
            yAxis: {
                min: 0,
                tickInterval: 1,
                title: {
                    text: ' '
                }
            },
            exporting: {
                enabled: false
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                formatter: function () {
                    var fromMeds = medsArray[this.point.index];
                    return '<b>' + this.series.name + '</b><br/>' + fromMeds[1] + " med(s) taken";
                }
            },
            series: [{
                data: medsArray,
                name: 'Took Meds',
                color: '#5EA647',
                dataLabels: {
                    enabled: true,
                    style: {
                        format: '{point.y:0f}',
                        color: '#5EA647'
                    }
                }
            }]
        });
    }
}
function drawFourteenDaysWeightChart(data) {
    var weightArray = [];
    var weightString = "Starting Weight: <b> " + data.STARTING_WEIGHT + "(lbs)</b><br/>Target Weight: <b>" + data.TARGET_WEIGHT + "(lbs)</b>";
    $('#fourteenDaysTargetWeights').html(weightString);
    var emptyCount = 0;
    $.each(data.DATA, function (index, weight) {
        var valArray = [];
        valArray.push(weight.timestamp);
        valArray.push(weight.weightLost);
        weightArray.push(valArray);
        emptyCount++;
    });

    if (emptyCount == 0) {
        $('#container-FourteenDaysWeightLoss').html("<span class='centered-msg no-log-error-center'>No Log Found</span>");
    } else {
        $('#container-FourteenDaysWeightLoss').highcharts({
            chart: {
                type: 'line'
            },
            xAxis: {
                tickInterval: 24 * 3600 * 1000,
                min: data.MINMAX[0],
                max: data.MINMAX[1],
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    day: '%e. %b',
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Days',
                    align: 'high'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Pounds',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            exporting: {
                enabled: false
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.0f} lbs'
            },

            series: [{
                data: weightArray,
                /* pointStart: Date.UTC(2015, 0, 0), */
                name: 'Weight',
                color: '#5EA647',
                //pointInterval : 24 * 3600 * 1000, // one day
                dataLabels: {
                    enabled: true,

                    style: {
                        format: '{point.y:.1f}',
                        color: '#5EA647'
                    }
                }
            }]
        });
    }
}

function drawFourteenDaysActivityChart(data) {
    var activityArray = [],
        $containerFourteenDaysActivity = $('#container-FourteenDaysActivity'),
        $date = $("#selFourteenDaysDate"),
        minMax = HSDateUtils.dateRangeFromRange($date.val(), 14);

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    if($containerFourteenDaysActivity.highcharts()) {
        $containerFourteenDaysActivity.highcharts().destroy();
    }

    $.each(data.DATA, function (index, actSteps) {
        var valArray = [];
        valArray.push(actSteps[0]);
        valArray.push(actSteps[1]);
        activityArray.push(valArray);
    });

    $containerFourteenDaysActivity.highcharts({
        plotOptions: {
            series: {
                pointWidth: 10
            }
        },
        xAxis: {
            tickInterval: 3600 * 2000,
            min: minMax[0],
            max: minMax[1],
            type: 'datetime',
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true,
            minorTickLength: 0,
            tickLength: 0,
            labels: {
                enabled: false
            },
            dateTimeLabelFormats: {
                hour: '%I %p',
                minute: '%I:%M %p'
            },
            title: {
                text: ''
            }
        },
        yAxis: {
            min: 0,
            max: data.MAXYAXIS,
            title: {
                text: 'Steps/Hour',
                style: {
                    color: '#f5c430',
                    fontSize: '1.2em',
                    fontWeight: 'bold'
                }
            },
            labels: {
                overflow: 'justify'
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%I:%M %p} {point.y} steps'
        },
        series: [{
            pointRange: 1,
            data: activityArray,
            name: 'Activity Steps',
            color: '#f5c430',
            dataLabels: {
                enabled: true,
                formatter:function(){
                    if(this.y > 0)
                        return this.y + ' Steps';
                }
            }
        }]
    });
}

function drawFourteenDaysActivityMinutesChart(data) {
    var activityArray = [],
        labelsArray = {},
        $containerFourteenDaysActivityMinutes = $('#container-FourteenDaysActivityMinutes'),
        minMax = [],
        $date = $("#selFourteenDaysDate"),
        minMax = HSDateUtils.dateRangeFromRange($date.val(), 14);

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    if($containerFourteenDaysActivityMinutes.highcharts()) {
        $containerFourteenDaysActivityMinutes.highcharts().destroy();
    }

    $.each(data.DATA, function (index, activity) {
        var valArray = [];
        valArray.push(activity[0]);
        valArray.push(activity[1]);
        labelsArray[activity[0]] = { label: activity[2], mins: activity[1] };
        activityArray.push(valArray);
    });

    $containerFourteenDaysActivityMinutes.highcharts({
        chart: {
            type: 'column'
        },
        plotOptions: {
            series: {
                pointWidth: 10
            },
            column: {
                stacking: 'normal'
            }
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Hours'
            },
            type: 'datetime',
            tickInterval: 3600 * 2000,
            min: minMax[0],
            max: minMax[1],
            labels: {
                enabled: true,
                formatter: function (){
                    return Highcharts.dateFormat('%I %p',this.value);
                }
            },
            dateTimeLabelFormats: {
                hour: '%I %p',
                minute: '%I:%M %p'
            },
            plotBands: {
                color: 'rgba(101, 109, 120, 0.2)', // Color value
                from: 0.5, // Start of the plot band
                to: 2.7
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Activity (minutes)',
                style: {
                    color: '#ff6666',
                    fontSize: '1.2em',
                    fontWeight: 'bold'
                }
            },
            stackLabels: {
                qTotals: labelsArray,
                enabled: true,
                style: {
                    fontWeight: 'bold'
                },
                useHTML: true,
                formatter: function () {
                    var topLabels = this.options.qTotals[this.x].label,
                        mins = this.options.qTotals[this.x].mins;

                    return "<span title='"+topLabels + "&nbsp;(" + mins +" mins)'>" + topLabels + "</span>";
                }
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.y} mins'
        },
        series: [{
            pointRange: 1,
            data: activityArray,
            name: 'Activity Minutes',
            color: '#999999'
        }]
    });
}

/*----------------- DRAWING MONTHLY CAHRTS -----------------*/
function drawMonthlyMealChart(monthlyTrendData) {
    /* Creating categories DATA  */
    var categories = ['Breakfast', 'Lunch', 'Dinner', 'Snack'];
    if (monthlyTrendData.Monthly_Meal) {
        var meals = monthlyTrendData.Monthly_Meal;
        categories = populateCategoriesValues(meals);
    }

    /* Creating Avg Meal DATA  */
    var averageMeals = [];
    if (monthlyTrendData.Meal_AVG) {
        var mealsAverage = monthlyTrendData.Meal_AVG;
        averageMeals = populateMealAverageValues(mealsAverage);
    }

    var mealTargets = [];
    mealTargets = $.grep(monthlyTrendData.Target, function (v, index) {
        if (v == 0) {
            v = null;
        } else {
            Math.round(v)
        }
        return true;
    });

    // Create the chart
    $('#container-monthlyMeal').highcharts({
        chart: {
            type: 'column'
        },
        xAxis: {
            categories: categories,
            type: 'category'
        },
        yAxis: [{
            title: {
                text: 'Average Meal Carbs'
            }
        }, {
            title: {
                text: ''
            },
            opposite: true
        }],
        plotOptions: {
            column: {
                showInLegend: true,
                grouping: false,
                shadow: false,
                borderWidth: 0,
                dataLabels: {
                    color: 'rgb(0,0,0)',
                    enabled: true,
                    format: '{point.y:0f}g'
                }
            }, series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            var category = this.category.split('<br/>')[0];
                            window.location.href = "myMealsPage.action?patientId=" + patId + "&mealType=" + category + "&date=" + $("#selMonthDate").val() + "&processed=true";
                        }
                    }
                }
            }
        },
        tooltip: {
            shared: true
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        title: {
            text: ''
        },
        series: [{
            dataLabels: {
                enabled: false
            },
            name: 'Carb Ceiling',
            data: mealTargets,
            color: 'rgba(192,192,192,0.7)',
            tooltip: {
                valueSuffix: 'g'
            }
        }, {
            name: 'Average',
            color: 'rgba(245,196,48,1)',
            pointPadding: 0.3,
            data: averageMeals,
            tooltip: {
                valueSuffix: 'g'
            }

        }]
    });
}

function drawMonthlyScatterGlucoseChart(listdata) {
    var $date = $("#selMonthDate"),
        minMax = HSDateUtils.dateRangeFromRange($date.val(), 30),
        maxYAxis = getMaxValueInArray(listdata.maxYAxis, 200);

    loadGlucoseChartLegend(listdata);

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    var options = {
        dataLabels: {
            enabled: true,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor)
            || 'black',
            style: {
                textShadow: '0 0 3px black, 0 0 3px black'
            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        chart: {
            renderTo: 'container-monthlyDaysGlucose',
            type: 'scatter',
            zoomType: 'xy',
            ignoreHiddenSeries: false
        },
        title: {
            text: ''
        },
        /*  subtitle: {
         text: 'Source: Heinz  2003'
         },  */
        xAxis: {
            title: {
                enabled: true,
                text: 'Time'
            },
            labels: {
                formatter: function (){
                    return Highcharts.dateFormat('%I %p',this.value);
                }
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true,
            type: 'datetime',
            tickInterval: 3600 * 2000,
            min: minMax[0],
            max: minMax[1],
            dateTimeLabelFormats: {
                hour: '%I %p',
                minute: '%I:%M %p'
            }

        },
        yAxis: {
            min: 0,
            max: maxYAxis,
            title: {
                text: 'Glucose (mg/dL)'
            },
            floor: 0,
            plotBands: [{
                // After Meal PLOT
                color: 'rgba(0,255,0,.50)', // Color value
                from: listdata.beforeMealGlucoseTargets[0][0], // Start of the plot band
                to: listdata.beforeMealGlucoseTargets[0][1]

                // End of the plot band
            }, {
                // Before Meal PLOT
                color: 'rgba(0,255,0,.25)', // Color value
                from: listdata.afterMealGlucoseTargets[0][0], // Start of the plot band
                to: listdata.afterMealGlucoseTargets[0][1]
            }]
        },
        legend: {
            align: 'center',
            x: 10,
            verticalAlign: 'bottom',
            y: 23,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2)
            || 'white',
            /*   borderColor: '#CCC',
             borderWidth: 1, */
            shadow: false
        },
        plotOptions: {
            dataLabels: {
                enabled: true,
                x: 40,
                formatter: function () {
                    return this.point.name;
                },
                style: {
                    color: "black"
                }
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (event) {
                            window.location.href = "fetchLogDetail.action?patientId="+patId+"&logId="+this.l;
                        }
                    }
                }
            },
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: true
                        }
                    }
                }
            }
        },
        tooltip: {
            snap: 0,
            enabled: true,
            shared: false,
            formatter: function(){
                return "<b>" + this.series.name + "</b> <br/>" + HSDateUtils.formatDateFromTicks(this.point.t, 'MM/DD/YYYY hh:mm A') + " , " + this.point.y + " mg/dl <br/>" + this.point.z;
            }
        },
        series: [{
            stickyTracking: false,
            name: 'Under Target',
            color: '#ECB648',
            marker: {
                symbol: 'circle',
                fillColor: "#ECB648",
                lineColor: "#ECB648",
                radius: 5
            },

            pointInterval: 36e5,
            dataLabels: {
                enabled: true,

                style: {

                    color: '#ECB648'
                }
            }

        }, {
                name: 'In Target',
                color: '#5EA647',
                marker: {
                    symbol: 'circle',
                    fillColor: "#5EA647",
                    lineColor: "#5EA647",
                    radius: 5
                },

                pointInterval: 36e5,
                dataLabels: {
                    enabled: true,

                    style: {

                        color: '#5EA647'
                    }
                }

            }, {
            name: 'Over Target',
            color: '#DA4453',
            marker: {
                symbol: 'circle',
                fillColor: "#DA4453",
                lineColor: "#DA4453",
                radius: 5
            },

            pointInterval: 36e5,
            dataLabels: {
                enabled: true,

                style: {

                    color: '#DA4453'
                }
            }

        }

        ]
    };

    var normalNewList = glucoseSeriesList(listdata.Normal, true);
    var lowNewList = glucoseSeriesList(listdata.Low, true);
    var highNewList = glucoseSeriesList(listdata.High, true);

    options.series[1].data = normalNewList;
    options.series[0].data = lowNewList;
    options.series[2].data = highNewList;
    GlucoseChart = new Highcharts.Chart(options);

}

function drawMonthlyMedicationTrendChart(data) {
    var $monthlyMedicationChart = $('#container-monthlyMedLog');
    var medications = data.Medication;
    if (medications.length == 0) {
        $monthlyMedicationChart.html("<span class='centered-msg'>No Log Found</span>");
    } else {
        var medsArray = [];
        $.each(medications, function (index, med) {
            var valArray = [];
            valArray.push(med.timestamp);
            valArray.push(med.medsTaken);
            medsArray.push(valArray);
        });

        $monthlyMedicationChart.highcharts({
            xAxis: {
                tickInterval: 24 * 3600 * 1000,
                min: data.MINMAX[0],
                max: data.MINMAX[1],
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e %b',
                    year: '%b'
                },
                title: {
                    text: '',
                    align: 'high'
                }
            },
            yAxis: {
                min: 0,
                tickInterval: 1,
                title: {
                    text: ' '
                }
            },
            exporting: {
                enabled: false
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                formatter: function () {
                    var fromMeds = medsArray[this.point.index];
                    return '<b>' + this.series.name + '</b><br/>' + fromMeds[1] + " med(s) taken";
                }
            },
            series: [{
                data: medsArray,
                name: 'Took Meds',
                color: '#5EA647',
                dataLabels: {
                    enabled: true,
                    style: {
                        fontSize: '8px',
                        format: '{point.y:0f}',
                        color: '#5EA647'
                    }
                }
            }]
        });
    }
}
function drawMonthlyWeightChart(data) {
    var weightArray = [];
    var weightString = "Starting Weight: <b> " + data.STARTING_WEIGHT + "(lbs)</b><br/>Target Weight: <b>" + data.TARGET_WEIGHT + "(lbs)</b>";
    $('#targetWeights').html(weightString);
    var emptyCount = 0;
    $.each(data.DATA, function (index, weight) {
        var valArray = [];
        valArray.push(weight.timestamp);
        valArray.push(weight.weightLost);
        weightArray.push(valArray);
        emptyCount++;
    });

    if (emptyCount == 0) {
        $('#containerMonthlyWeightLoss').html("<span class='centered-msg no-log-error-center'>No Log Found</span>");
    } else {
        $('#containerMonthlyWeightLoss').highcharts({
            chart: {
                type: 'line'
            },
            xAxis: {
                tickInterval: 24 * 3600 * 1000,
                min: data.MINMAX[0],
                max: data.MINMAX[1],
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    day: '%e. %b',
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Days',
                    align: 'high'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Pounds',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            exporting: {
                enabled: false
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.0f} lbs'
            },

            series: [{
                data: weightArray,
                /* pointStart: Date.UTC(2015, 0, 0), */
                name: 'Weight',
                color: '#5EA647',
                //pointInterval : 24 * 3600 * 1000, // one day
                dataLabels: {
                    enabled: true,

                    style: {
                        format: '{point.y:.1f}',
                        color: '#5EA647'
                    }
                }
            }]
        });
    }
}
function drawMonthlyActivityChart(data) {
    var activityArray = [],
        $containerMonthlyActivity = $('#containerMonthlyActivity'),
        $date = $("#selMonthDate"),
        minMax = HSDateUtils.dateRangeFromRange($date.val(), 30);

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    if($containerMonthlyActivity.highcharts()) {
        $containerMonthlyActivity.highcharts().destroy();
    }

    $.each(data.DATA, function (index, actSteps) {
        var valArray = [];
        valArray.push(actSteps[0]);
        valArray.push(actSteps[1]);
        activityArray.push(valArray);
    });

    $containerMonthlyActivity.highcharts({
        plotOptions: {
            series: {
                pointWidth: 10
            }
        },
        xAxis: {
            tickInterval: 3600 * 1000,
            min: minMax[0],
            max: minMax[1],
            type: 'datetime',
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true,
            minorTickLength: 0,
            tickLength: 0,
            labels: {
                enabled: false
            },
            dateTimeLabelFormats: {
                hour: '%I %p',
                minute: '%I:%M %p'
            },
            title: {
                text: ''
            }
        },
        yAxis: {
            min: 0,
            max: data.MAXYAXIS,
            title: {
                text: 'Steps/Hour',
                style: {
                    color: '#f5c430',
                    fontSize: '1.2em',
                    fontWeight: 'bold'
                }
            },
            labels: {
                overflow: 'justify'
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%I:%M %p} , {point.y} steps'
        },
        series: [{
            pointRange: 1,
            data: activityArray,
            name: 'Activity Steps',
            color: '#f5c430',
            dataLabels: {
                enabled: true,
                formatter:function(){
                    if(this.y > 0)
                        return this.y + ' Steps';
                }
            }
        }]
    });
}


function drawMonhtlyActivityMinutesChart(data) {
    var activityArray = [],
        labelsArray = {},
        $containerMonthlyActivityMinutes = $('#container-MonhtlyActivityMinutes'),
        minMax = [],
        $date = $("#selMonthDate"),
        minMax = HSDateUtils.dateRangeFromRange($date.val(), 30);

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    if($containerMonthlyActivityMinutes.highcharts()) {
        $containerMonthlyActivityMinutes.highcharts().destroy();
    }

    $.each(data.DATA, function (index, activity) {
        var valArray = [];
        valArray.push(activity[0]);
        valArray.push(activity[1]);
        labelsArray[activity[0]] = { label: activity[2], mins: activity[1] };
        activityArray.push(valArray);
    });

    $containerMonthlyActivityMinutes.highcharts({
        chart: {
            type: 'column'
        },
        plotOptions: {
            series: {
                pointWidth: 10
            },
            column: {
                stacking: 'normal'
            }
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Hours'
            },
            type: 'datetime',
            tickInterval: 3600 * 2000,
            min: minMax[0],
            max: minMax[1],
            labels: {
                enabled: true,
                formatter: function (){
                    return Highcharts.dateFormat('%I %p',this.value);
                }
            },
            dateTimeLabelFormats: {
                hour: '%I %p',
                minute: '%I:%M %p'
            },
            plotBands: {
                color: 'rgba(101, 109, 120, 0.2)', // Color value
                from: 0.5, // Start of the plot band
                to: 2.7
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Activity (minutes)',
                style: {
                    color: '#ff6666',
                    fontSize: '1.2em',
                    fontWeight: 'bold'
                }
            },
            stackLabels: {
                qTotals: labelsArray,
                enabled: true,
                style: {
                    fontWeight: 'bold'
                },
                useHTML: true,
                formatter: function () {
                    var topLabels = this.options.qTotals[this.x].label,
                        mins = this.options.qTotals[this.x].mins;

                    return "<span title='"+topLabels + "&nbsp;(" + mins +" mins)'>" + topLabels + "</span>";
                }
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.y} mins'
        },
        series: [{
            pointRange: 1,
            data: activityArray,
            name: 'Activity Minutes',
            color: '#999999'
        }]
    });
}

/*----------------- HELPING FUNCTIONS -----------------*/
function populateMealAverageValues(data) {
    var averageMeals = [];
    averageMeals[0] = (data[2]) ? Math.round(parseFloat(data[2].toFixed(1))) : 0;
    averageMeals[1] = (data[3]) ? Math.round(parseFloat(data[3].toFixed(1))) : 0;
    averageMeals[2] = (data[1]) ? Math.round(parseFloat(data[1].toFixed(1))) : 0;
    averageMeals[3] = (data[4]) ? Math.round(parseFloat(data[4].toFixed(1))) : 0;
    return averageMeals;
}

function populateCategoriesValues(meals) {
    var categories = [];
    categories[0] = 'Breakfast<br/>' + ((meals[9]) ? Math.round(meals[9]) : 0) + '% (' + meals[5] + ' of ' + meals[1] + ')';
    categories[1] = 'Lunch<br/>' + ((meals[10]) ? Math.round(meals[10]) : 0) + '% (' + meals[6] + ' of ' + meals[2] + ')';
    categories[2] = 'Snack<br/>' + ((meals[11]) ? Math.round(meals[11]) : 0) + '% (' + meals[7] + ' of ' + meals[3] + ')';
    categories[3] = 'Dinner<br/>' + ((meals[12]) ? Math.round(meals[12]) : 0) + '% (' + meals[8] + ' of ' + meals[4] + ')';
    return categories;
}

function loadGlucoseChartLegend(data){

    $(".before-meal-tests").text(data.TotalTests[0]);
    $(".after-meal-tests").text(data.TotalTests[1]);
    $(".before-meal-avg").text(data.AvgBGL[0] === 0 ?  " mg/dl" : data.AvgBGL[0] + " mg/dl");
    $(".after-meal-avg").text(data.AvgBGL[1] === 0 ?  " mg/dl" : data.AvgBGL[1] + " mg/dl");
    $(".before-meal-std").text(data.stdBeforMeal[0] === null ? " N/A" : data.stdBeforMeal[0] + " mg/dl");
    $(".after-meal-std").text(data.stdAfterMeal[0] === null ? " N/A" : data.stdAfterMeal[0] + " mg/dl");
    $(".before-meal-targets").text(data.beforeMealGlucoseTargets[0][0] + "-" + data.beforeMealGlucoseTargets[0][1] + " mg/dl");
    $(".after-meal-targets").text(data.afterMealGlucoseTargets[0][0] + "-" + data.afterMealGlucoseTargets[0][1] + " mg/dl");
}

function glucoseSeriesList(data, isDifferentBeforeAfter){
    return $.map(data, function(n, index){
        if(isDifferentBeforeAfter){
            var beforeAfterSymbol = (n[2] && n[2].toLowerCase() == 'before meal') ? 'triangle' : 'square';
            return {x: n[0], y: n[1], z: n[2], l: n[3], t: n[4], marker: {radius: 5, symbol: beforeAfterSymbol} };
        }
        return {x: n[0], y: n[1], z: n[2], l: n[3], t: n[4]};
    });
}

function getMaxValueInArray(arr, defaultMax){

    if(arr && arr.length > 0){
        return Math.max.apply(null, arr);
    }

    return defaultMax;
}