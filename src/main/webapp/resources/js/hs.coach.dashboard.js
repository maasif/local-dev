/**
 * Created by omursaleen on 9/04/2015.
 */

if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

$.expr[":"].contains = $.expr.createPseudo(function(arg) {
    return function( elem ) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

//==========================================
/* HSCoachDashboard JS Model*/
var HSCoachDashboard = {

    init: function(providerId){
        var that = this;
        that.PROVIDER_ID = providerId;

        //alert message instance
        that.ALERT_MESSAGES = Object.create(AlertMessage).init();

        //ajaxing instance
        that.AJAXING = Object.create(Ajaxing);

        window.coachDashboardObject = that;

        that.cachePageConstants();
        that.getUpcomingSessionsFromServer();
        that.getMemberMessagesFromServer();
        that.getCoachNotesFromServer();
        that.getMyMembersFromServer();
        that.getInactiveMembersFromServer();
        that.getMembersLastContactOn();
        that.getCoachPreferenceDataFromServer();
        that.bindEventListeners();

        return that;
    },

    cachePageConstants: function(){
       var that = this;

       that.$listMyNewMembers = $("#listMyNewMembers").empty();
       that.$listInactivityWatch = $("#listInactivityWatch").empty();
       that.$listUpcomingSessions = $("#listUpcomingSessions").empty();
       that.$listCoachMessages = $("#listCoachMessages").empty();
       that.$listMemberMessages = $("#listMemberMessages").empty();
       that.$listMemberMessagesLastContactOn = $("#listMemberMessagesLastContactOn").empty();
       that.$btnEditCoachPreferences = $("#btnEditCoachPreferences");
       that.$btnSaveCoachSchedulingPreferences = $("#btnSaveCoachSchedulingPreferences");
    },

    bindEventListeners: function(){
        var that = this;

        that.$btnEditCoachPreferences.on("click", function(e){
            e.preventDefault();
            $(".session-days,.session-times").each(function(index, obj){
                this.checked = false;
            })
            $("#coachEatingPreferencesDialog").foundation("reveal", "open");
        });

        that.$btnSaveCoachSchedulingPreferences.on("click", function(e){
            e.preventDefault();

            var $sessionDays = $(".session-days:checked"),
                $sessionTimes = $(".session-times:checked"),
                $err = $("#coachErrorSchedulingPreference");

            if($sessionDays.length == 0){
                that.ALERT_MESSAGES.showAlertMessage($err, "Please select days", that.ALERT_MESSAGES.ERROR);
                return;
            }

            if($sessionTimes.length == 0){
                that.ALERT_MESSAGES.showAlertMessage($err, "Please select times", that.ALERT_MESSAGES.ERROR);
                return;
            }

            window.coachDashboardObject = that;

            var commaSeparatedDays = $sessionDays.map(function(i, day){ return $(day).val() }).get().join(", "),
                commaSeparatedTimes = $sessionTimes.map(function(i, time){ return $(time).val() }).get().join(", ");

            var dataToSend = { "sessionDays": commaSeparatedDays, "providerId": that.PROVIDER_ID, "sessionTimes": commaSeparatedTimes },
                postRequestActions = {
                    "loading": $("#loading"),
                    "successCallBack": that.onAddSchedulePreferences
                };

            that.AJAXING.sendPostRequest("saveCoachSchedulingPreferences.action", dataToSend, postRequestActions);
        });
    },

    onAddSchedulePreferences: function(data){
        var that = window.coachDashboardObject;
        $("#coachEatingPreferencesDialog").foundation("reveal", "close");
        if(data && data.STATUS == "SUCCESS"){
            //get member scheduling preferences via ajax
            that.getCoachSchedulingPreferenceFromServer();
        }
    },

    getCoachPreferenceDataFromServer: function(){
        var that = this;

        window.coachDashboardObject = that;

        var dataToSend = {"providerId": that.PROVIDER_ID },
            postRequestActions = {
                "successCallBack": that.onGettingCoachPreferenceData
            };

        that.AJAXING.sendPostRequest("getCoachPreferenceDataSet.action", dataToSend, postRequestActions);
    },

    onGettingCoachPreferenceData: function(data){
        var that = window.coachDashboardObject;
        if(data && data.STATUS == "SUCCESS"){
            COACH_SESSION_PREFERENCE = (data.COACH_SESSION_PREFERENCE) ? $.parseJSON(data.COACH_SESSION_PREFERENCE) : undefined;
            that.loadCoachSessionPreference();
        }
    },

    getCoachSchedulingPreferenceFromServer: function(){
        var that = window.coachDashboardObject,
            dataToSend = {"providerId": that.PROVIDER_ID },
            postRequestActions = {
                "successCallBack": that.onGettingCoachSchedulingPreferences
            };

        that.AJAXING.sendPostRequest("getCoachSchedulingPreferences.action", dataToSend, postRequestActions);
    },

    onGettingCoachSchedulingPreferences: function(data){
        var that = window.coachDashboardObject;
        if(data && data.STATUS == "SUCCESS"){
            COACH_SESSION_PREFERENCE = (data.DATA) ? $.parseJSON(data.DATA) : undefined;
            that.loadCoachSessionPreference();
        }
    },

    loadCoachSessionPreference: function(){
        var that = this;

        if(COACH_SESSION_PREFERENCE){

            $("#coachEatingPreferencesDialog").off("opened closed").on({
                opened: function () {

                    //loading in modal for edit
                    var splitDays = COACH_SESSION_PREFERENCE.days.split(", "),
                        splitTimes = COACH_SESSION_PREFERENCE.times.split(", ");

                    $.each(splitDays, function(index, day){
                        $(".session-days[value='"+day+"']").prop("checked", "true");
                    });

                    $.each(splitTimes, function(index, time){
                        $(".session-times[value='"+time+"']").prop("checked", "true");
                    });
                },
                closed: function () {

                }
            });
        }
    },

    getMyMembersFromServer: function(){
        var that = this,
            postRequestActions = {
                "successCallBack": that.onGettingMyMembers,
                "loading": $("#loadingMyMembers")
            };

        that.AJAXING.sendPostRequest("getCoachDashboardMyMembers.action", null, postRequestActions);
    },

    onGettingMyMembers: function(data){
        var that = window.coachDashboardObject;
        if(data.STATUS == "SUCCESS"){
            if(data.DATA){
                that.renderMyMembers(data.DATA);
            }else{
                that.$listMyNewMembers.append("<li class='align-center'>No data</li>");
            }
        }else{
            that.$listMyNewMembers.append("<li class='align-center'>No data</li>");
            that.findParentsAndHideShow(that.$listMyNewMembers, ".c-dashboard-item", ".c-dashboard-list-header");
        }
    },

    renderMyMembers: function(members){
        var that = window.coachDashboardObject,
            html = "";

        if(members && members.length > 0) {
            that.findParentsAndHideShow(that.$listMyNewMembers, ".c-dashboard-item", ".c-dashboard-list-header", true);

            $.each(members, function (index, member) {
                html += "<li><a href='dashboard.action?patientId=" + member.patientId + "&uuid=" + member.uuid + "'><div class='row'><div class='columns medium-6 large-6'>" + that.formatMemberName(member.displayName, member.firstName, member.lastName) + "</div><div class='columns medium-6 large-6>'> " + HSDateUtils.formatDateFromTicks(member.regDate) + "</div></div></a></li>";
            });

            var settings = {
                total: members.length,
                shown: 3,
                jump: 6
            };

            that.$listMyNewMembers.append(html).loadMoreLess(settings);
        }else{
            that.$listMyNewMembers.append("<li class='align-center'>No data</li>");
            that.findParentsAndHideShow(that.$listMyNewMembers, ".c-dashboard-item", ".c-dashboard-list-header");
        }
    },

    getInactiveMembersFromServer: function(){
        var that = this,
            postRequestActions = {
                "successCallBack": that.onGettingInactiveMembers,
                "loading": $("#loadingInactiveMembers")
            };

        that.AJAXING.sendPostRequest("getCoachDashboardInactiveMembers.action", null, postRequestActions);
    },

    onGettingInactiveMembers: function(data){
        var that = window.coachDashboardObject;
        if(data.STATUS == "SUCCESS"){
            if(data.DATA){
                that.renderInactiveMembers(data.DATA);
            }else{
                that.$listInactivityWatch.append("<li class='align-center'>No data</li>");
            }
        }else{
            that.$listInactivityWatch.append("<li class='align-center'>No data</li>");
            that.findParentsAndHideShow(that.$listInactivityWatch, ".c-dashboard-item", ".c-dashboard-list-header");
        }
    },

    renderInactiveMembers: function(members){
        var that = window.coachDashboardObject,
            html = "";

        if(members && members.length > 0) {
            that.findParentsAndHideShow(that.$listInactivityWatch, ".c-dashboard-item", ".c-dashboard-list-header", true);

            $.each(members, function (index, member) {
                var inactiveDate = (member.invitedDate) ? HSDateUtils.formatDateFromTicks(parseFloat(member.invitedDate), "MMM DD") : "N/A";
                html += "<li><a href='dashboard.action?patientId=" + member.patientId + "&uuid=" + member.uuid + "'><div class='row'><div class='columns medium-6 large-6'>" + that.formatMemberName(member.displayName, member.firstName, member.lastName) + "</div><div class='columns medium-6 large-6>'> " + inactiveDate + "</div></div></a></li>";
            });

            var settings = {
                total: members.length,
                shown: 3,
                jump: 6
            };

            that.$listInactivityWatch.append(html).loadMoreLess(settings);
        }else{
            that.$listInactivityWatch.append("<li class='align-center'>No data</li>");
            that.findParentsAndHideShow(that.$listInactivityWatch, ".c-dashboard-item", ".c-dashboard-list-header");
        }
    },

    getUpcomingSessionsFromServer: function(){
        var that = this,
            postRequestActions = {
                "successCallBack": that.onGettingUpcomingSessions,
                "loading": $("#loadingUpcomingSessions")
            };

        that.AJAXING.sendPostRequest("getCoachDashboardUpcomingSessions.action", null, postRequestActions);
    },

    onGettingUpcomingSessions: function(data){
        var that = window.coachDashboardObject;
        if(data.STATUS == "SUCCESS"){
            if(data.DATA){
                that.renderUpcomingSessions(data.DATA);
            }else{
                that.$listUpcomingSessions.append("<li class='align-center'>No data</li>");
            }
        }else{
            that.$listUpcomingSessions.append("<li class='align-center'>No data</li>");
            that.findParentsAndHideShow(that.$listUpcomingSessions, ".c-dashboard-item", ".c-dashboard-list-header");
        }
    },

    renderUpcomingSessions: function(members){
        var that = window.coachDashboardObject,
            html = "";

        if(members && members.length > 0) {
            that.findParentsAndHideShow(that.$listUpcomingSessions, ".c-dashboard-item", ".c-dashboard-list-header", true);

            $.each(members, function (index, member) {
                html += "<li><a href='dashboard.action?patientId=" + member.patientId + "&uuid=" + member.uuid + "'><div class='row'><div class='columns medium-6 large-6'>" + that.formatMemberName(member.displayName, member.firstName, member.lastName) + "</div><div class='columns medium-6 large-6>'> " + HSDateUtils.formatDateFromTicks(parseFloat(member.invitedDate), "MM/DD/YYYY hh:mm A") + "</div></div></a></li>";
            });

            var settings = {
                total: members.length,
                shown: 3,
                jump: 6
            };

            that.$listUpcomingSessions.append(html).loadMoreLess(settings);
        }else{
            that.$listUpcomingSessions.append("<li class='align-center'>No data</li>");
            that.findParentsAndHideShow(that.$listUpcomingSessions, ".c-dashboard-item", ".c-dashboard-list-header");
        }
    },

    getCoachNotesFromServer: function(){
        var that = this,
            postRequestActions = {
                "successCallBack": that.onGettingCoachNotes,
                "loading": $("#loadingCoachMessages")
            };

        that.AJAXING.sendPostRequest("getCoachDashboardCoachMessages.action", null, postRequestActions);
    },

    onGettingCoachNotes: function(data){
        var that = window.coachDashboardObject;
        if(data.STATUS == "SUCCESS"){
            if(data.DATA){
                that.renderCoachNotes(data.DATA);
            }else{
                that.$listCoachMessages.append("<li class='align-center'>No data</li>");
            }
        }else{
            that.$listCoachMessages.append("<li class='align-center'>No data</li>");
            that.findParentsAndHideShow(that.$listCoachMessages, ".c-dashboard-item", ".c-dashboard-list-header");
        }
    },

    renderCoachNotes: function(members){
        var that = window.coachDashboardObject,
            html = "";

        if(members && members.length > 0) {
            that.findParentsAndHideShow(that.$listCoachMessages, ".c-dashboard-item", ".c-dashboard-list-header", true);

            $.each(members, function (index, member) {
                html += "<li><a href='dashboard.action?patientId=" + member.patientId + "&uuid=" + member.uuid + "&goto=coachNotes'><div class='row'><div class='columns medium-6 large-6'>" + that.formatMemberName(member.displayName, member.firstName, member.lastName) + "</div><div class='columns medium-6 large-6>'> " + HSDateUtils.formatDateFromTicks(parseFloat(member.invitedDate)) + "</div></div></a></li>";
            });

            var settings = {
                total: members.length,
                shown: 3,
                jump: 6
            };

            that.$listCoachMessages.append(html).loadMoreLess(settings);
        }else{
            that.$listCoachMessages.append("<li class='align-center'>No data</li>");
            that.findParentsAndHideShow(that.$listCoachMessages, ".c-dashboard-item", ".c-dashboard-list-header");
        }
    },

    getMemberMessagesFromServer: function(){
        var that = this,
            postRequestActions = {
                "successCallBack": that.onGettingMemberMessages,
                "loading": $("#loadingMemberMessages")
            };

        that.AJAXING.sendPostRequest("getCoachDashboardMemberMessages.action", null, postRequestActions);
    },

    onGettingMemberMessages: function(data){
        var that = window.coachDashboardObject;
        if(data.STATUS == "SUCCESS"){
            if(data.DATA){
                that.renderMemberMessages(data.DATA);
            }else{
                that.$listMemberMessages.append("<li class='align-center'>No data</li>");
            }
        }else{
            that.$listMemberMessages.append("<li class='align-center'>No data</li>");
            that.findParentsAndHideShow(that.$listMemberMessages, ".c-dashboard-item", ".c-dashboard-list-header");
        }
    },

    renderMemberMessages: function(members){
        var that = window.coachDashboardObject,
            html = "";

        if(members && members.length > 0) {
            that.findParentsAndHideShow(that.$listMemberMessages, ".c-dashboard-item", ".c-dashboard-list-header", true);

            $.each(members, function (index, member) {
                html += "<li><a href='coachPatientMessages.action?patientId=" + member.patientId + "&uuid=" + member.uuid + "'><div class='row'><div class='columns medium-1 large-1'><span class='message-bubble'>" + member.messageCount + "</span></div><div class='columns medium-5 large-5'>" + that.formatMemberName(member.displayName, member.firstName, member.lastName) + "</div><div class='columns medium-6 large-6>'>" + HSDateUtils.formatDateFromTicks(parseFloat(member.invitedDate), "MM/DD/YYYY hh:mm A") + "</div></div></a></li>";
            });

            var settings = {
                total: members.length,
                shown: 3,
                jump: 6
            };

            that.$listMemberMessages.append(html).loadMoreLess(settings);
        }else{
            that.$listMemberMessages.append("<li class='align-center'>No data</li>");
            that.findParentsAndHideShow(that.$listMemberMessages, ".c-dashboard-item", ".c-dashboard-list-header");
        }
    },

    findParentsAndHideShow: function($el, parentClass, elemToHide, isShow){

        var $elem = $el.parents(parentClass).children(elemToHide).hide();

        if(isShow){
            $elem.show();
        }
    },

    formatMemberName: function(displayName, firstName, lastName){

        var finalName = displayName;

        if(!displayName){
            if(firstName){
                finalName = firstName;
            }
            if(lastName){
                finalName += " " + lastName.charAt(0).toUpperCase();
            }
        }

        return finalName;
    },

    getMembersLastContactOn: function(){
        var that = this,
            postRequestActions = {
                "successCallBack": that.onGettingMembersLastContactOn,
                "loading": $("#loadingMemberMessagesLastContactOn")
            };

        that.AJAXING.sendPostRequest("getCoachDashboardMemberLastContactOn.action", null, postRequestActions);
    },

    onGettingMembersLastContactOn: function(data){
        var that = window.coachDashboardObject;
        if(data.STATUS == "SUCCESS"){
            if(data.DATA){
                that.renderMembersLastContactOn(data.DATA);
            }else{
                that.$listMemberMessagesLastContactOn.append("<li class='align-center'>No data</li>");
            }
        }else{
            that.$listMemberMessagesLastContactOn.append("<li class='align-center'>No data</li>");
            that.findParentsAndHideShow(that.$listMemberMessagesLastContactOn, ".c-dashboard-item", ".c-dashboard-list-header");
        }
    },

    renderMembersLastContactOn: function(members){
        var that = window.coachDashboardObject,
            html = "";

        if(members && members.length > 0) {
            that.findParentsAndHideShow(that.$listMemberMessagesLastContactOn, ".c-dashboard-item", ".c-dashboard-list-header", true);

            $.each(members, function (index, member) {
                html += "<li><a href='coachPatientMessages.action?patientId=" + member.patientId + "&uuid=" + member.uuid + "'><div class='row'><div class='columns medium-6 large-6'>" + that.formatMemberName(member.displayName, member.firstName, member.lastName) + "</div><div class='columns medium-6 large-6>'>" + HSDateUtils.formatDateFromTicks(parseFloat(member.invitedDate), "MM/DD/YYYY hh:mm A") + "</div></div></a></li>";
            });

            var settings = {
                total: members.length,
                shown: 3,
                jump: 6
            };

            that.$listMemberMessagesLastContactOn.append(html).loadMoreLess(settings);
        }else{
            that.$listMemberMessagesLastContactOn.append("<li class='align-center'>No data</li>");
            that.findParentsAndHideShow(that.$listMemberMessagesLastContactOn, ".c-dashboard-item", ".c-dashboard-list-header");
        }
    }
};

//plugin to bind load more functionality to list, __oz
$(function($) {
    $.fn.loadMoreLess = function(settings){
        return this.each(function(){
            var $this = $(this),
                total = settings.total,
                shown = settings.shown,
                jump = settings.jump,
                $loadMore = $this.next(".more").off("click"),
                $scrollable = $this.parents(".c-dashboard-item-scrollable");

            $this.find("li:gt("+shown+")").hide();

            if( (shown < total) && ((shown+1) < total)){
                $loadMore.show();
            }

            $loadMore.on("click", function(e){
                e.preventDefault();

                shown = $this.find('li:visible').size() + jump;

                if($scrollable && $scrollable.length > 0){
                    $scrollable.animate({ scrollTop: $scrollable[0].scrollHeight+15 }, 320);
                }

                if(shown < total) {
                    $this.find('li:lt('+shown+')').show();
                } else {
                    $this.find('li:lt('+total+')').show();
                    $(this).hide();
                }
            });
        });
    }
});