/**
 * Created by omursaleen on 3/9/2015.
 */

if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//==========================================
/* HSCoachesArea JS Model*/
var HSCoachesArea = {

    init: function(patientId, loggedInUserID, imagesPath){
        var that = this;

        that.PATIENT_ID = patientId;
        that.MEDICATION_ID = "";
        that.CUSTOM_ID = "";
        that.CUSTOM_OBJECT = "";
        that.UUID = "";
        that.CUSTOM_DATA = "";
        that.LOGGED_IN_PROVIDER_ID = loggedInUserID;

        //alert message instance
        that.ALERT_MESSAGES = Object.create(AlertMessage).init();

        //ajaxing instance
        that.AJAXING = Object.create(Ajaxing);

        that.IMAGES_PATH = imagesPath;
        window.coachesDashboardObject = that;

        that.cachePageConstants();
        that.getCoachesAreaDataFromServer();
        that.bindEventListeners();

        //$("div.equal-areas").equalRowHeights();

        return that;
    },

    cachePageConstants: function(){
       var that = this;

       that.$btnAddNote = $("#btnAddNote");
       that.$btnAddNotesDialog = $("#btnAddNotesDialog");
       that.$btnViewAllCoachNotes = $("#btnViewAllCoachNotes");
       that.$btnViewAllCoachLogs = $("#btnViewAllCoachLogs");
       that.$btnAddCoachLogs = $("#btnAddCoachLogs");
       that.$txtCoachLog = $("#txtCoachLog");
       that.$txtCoachLogDate = $("#txtCoachLogDate");
       that.$btnViewAllDietaryNotes = $("#btnViewAllDietaryNotes");
       that.$btnAddDietaryNotes = $("#btnAddDietaryNotes");
       that.$txtDietaryNotes = $("#txtDietaryNotes");

       that.$btnAddMemberKeyFacts = $("#btnAddMemberKeyFacts");
       that.$txtMemberKeyFacts = $("#txtMemberKeyFacts");

       that.$btnExpColCoachArea = $("#btnExpColCoachArea").parent().show().end();
       that.$divCoachesAreaLeft = $("#divCoachesAreaLeft");

       that.$btnAddOneToOneSession = $("#btnAddOneToOneSession");
       that.$btnAddOneToOneSessionDialog = $("#btnAddOneToOneSessionDialog");
       that.$btnCancelOneToOneSessionDialog = $("#btnCancelOneToOneSessionDialog");

       that.$btnConfirmMedSessionDelete = $("#btnConfirmMedSessionDelete");
       that.$ddCoachesList = $("#ddCoachesList");
       that.$txtAddSessionDate = $("#txtAddSessionDate").datepicker({minDate: new Date()});
       that.$btnViewAllOneToOneSession = $("#btnViewAllOneToOneSession");
       that.$btnViewAllTodoList = $("#btnViewAllTodoList");

       if(document.URL.indexOf("topicsOverview.action") == -1){
           that.$coachTodoNav = $(".coach-todo-nav a").parent().removeClass("hide").end();
       }

       //Diabetes
       that.$caDiagnosis = $("#caDiagnosis");
       that.$caDiagnosisDate = $("#caDiagnosisDate");
       that.$caDiabetesSupport = $("#caDiabetesSupport");
       that.$caMedications = $("#caMedications");
       that.$caTimeSinceLastEdu = $("#caTimeSinceLastEdu");

       //Dietary
       that.$caDietaryRestrictions = $("#caDietaryRestrictions");
       that.$caMealPreparer = $("#caMealPreparer");
       that.$ddMedications = $("#ddMedications");
       that.$btnMedicationDelete = $("#btnMedicationDelete");
       that.$btnConfirmMedDelete = $("#btnConfirmMedDelete");
       that.$btnViewAllMedications = $("#btnViewAllMedications");

       //Demographics
       that.$caGender = $("#caGender");
       that.$caDob = $("#caDob");
       that.$caRace = $("#caRace");
       that.$caEducation = $("#caEducation");

       that.$btnPatientCoachesDialog = $("#btnPatientCoachesDialog");
       that.$ddNotifyCoaches = $("#ddNotifyCoaches").empty();

       that.$divPrimaryFoodCoach = $("#divPrimaryFoodCoach");
       that.$btnAssignToMe = $("#btnAssignToMe").data("patid", that.PATIENT_ID);
       that.$chkMemberConsent = $("#chkMemberConsent");
       that.$divOverlayMedication = $("#divOverlayMedication");
       that.$btnAddMedication = $("#btnAddMedication");

       that.$btnViewAllMemberKeyFacts = $("#btnViewAllMemberKeyFacts");
       that.$btnEditSchedulingPreferences = $("#btnEditSchedulingPreferences");
       that.$btnSaveSchedulingPreferences = $("#btnSaveSchedulingPreferences");

       that.txtCoachLogDate = $("#txtCoachLogDate").datepicker();

       that.$txtAddSessionTime = $("#txtAddSessionTime").timepicker({ 'timeFormat': 'h:i A', 'step': 15 }).on("keypress keydown", function(evt){
            return false;
        }).on("changeTime", function(e){
        });

       $("a.edit-info").prop("href", "addEditPatient.action?patientId="+that.PATIENT_ID);
    },

    bindEventListeners: function(){
        var that = this;

        $("#addNotesDialog").on({
            opened: function () {
                $("#txtAddNotes").focus();
            },
            closed: function () {

            }
        });

        that.$btnAddNote.on("click", function(e){
            e.preventDefault();
            $("#txtAddNotes").val("");
            $("#ddNotifyCoaches").val("").trigger("chosen:updated");
            $("#addNotesDialog").foundation("reveal", "open");
        });

        that.$btnViewAllTodoList.on("click", function(e){
            e.preventDefault();
            $("#coachTodoListDialog").foundation("reveal", "open");
        });

        that.$btnAddOneToOneSession.on("click", function(e){
            e.preventDefault();
            that.$ddCoachesList.val("");
            that.$txtAddSessionDate.val("");
            that.$txtAddSessionTime.val("");
            that.$btnAddOneToOneSessionDialog.text("Save");
            that.$btnCancelOneToOneSessionDialog.addClass("hide");
            that.CUSTOM_ID = "";
            that.CUSTOM_DATA = "";
            that.CUSTOM_OBJECT = "";
            $("#addOneToOneSessionDialog").foundation("reveal", "open");
        });

        that.$btnCancelOneToOneSessionDialog.on("click", function(e){
            e.preventDefault();
            that.CUSTOM_ID = "";
            that.CUSTOM_OBJECT = "";
            $("#allOneToOneSessionsDialog").foundation("reveal", "open");
        });

        that.$btnPatientCoachesDialog.on("click", function(e){
            e.preventDefault();
            $("#patientCoachesDialog").foundation("reveal", "open");
        });

        that.$btnViewAllCoachNotes.on("click", function(e){
            e.preventDefault();
            $("#allCoachNotesDialog").foundation("reveal", "open");
        });

        that.$btnViewAllCoachLogs.on("click", function(e){
            e.preventDefault();
            $("#allCoachLogsDialog").foundation("reveal", "open");
        });

        that.$btnViewAllDietaryNotes.on("click", function(e){
            e.preventDefault();
            $("#allDietaryNotesDialog").foundation("reveal", "open");
        });

        that.$btnViewAllOneToOneSession.on("click", function(e){
            e.preventDefault();
            that.CUSTOM_ID = "";
            that.CUSTOM_DATA = "";
            that.CUSTOM_OBJECT = "";
            $("#allOneToOneSessionsDialog").foundation("reveal", "open");
        });

        that.$btnMedicationDelete.on("click", function(e){
            e.preventDefault();
            that.MEDICATION_ID = $(this).data("id");
            $("#medicationDeleteDialog").foundation("reveal", "open");
        });

        that.$btnViewAllMedications.on("click", function(e){
            e.preventDefault();
            $("#patientMedicationsDialog").foundation("reveal", "open");
        });

        that.$btnViewAllMemberKeyFacts.on("click", function(e){
            e.preventDefault();
            $("#memberKeyFactsDialog").foundation("reveal", "open");
        });

        that.$btnEditSchedulingPreferences.on("click", function(e){
            e.preventDefault();
            $(".session-days,.session-times").each(function(index, obj){
                this.checked = false;
            })
            $("#memberEatingPreferencesDialog").foundation("reveal", "open");
        });

        that.$btnSaveSchedulingPreferences.on("click", function(e){
            e.preventDefault();

            var $sessionDays = $(".session-days:checked"),
                $sessionTimes = $(".session-times:checked"),
                $err = $("#errorSchedulingPreference");

            if($sessionDays.length == 0){
                that.ALERT_MESSAGES.showAlertMessage($err, "Please select days", that.ALERT_MESSAGES.ERROR);
                return;
            }

            if($sessionTimes.length == 0){
                that.ALERT_MESSAGES.showAlertMessage($err, "Please select times", that.ALERT_MESSAGES.ERROR);
                return;
            }

            window.coachesDashboardObject = that;

            var commaSeparatedDays = $sessionDays.map(function(i, day){ return $(day).val() }).get().join(", "),
                commaSeparatedTimes = $sessionTimes.map(function(i, time){ return $(time).val() }).get().join(", ");

            var dataToSend = { "sessionDays": commaSeparatedDays, "patientId": that.PATIENT_ID, "sessionTimes": commaSeparatedTimes },
                postRequestActions = {
                    "loading": $("#loading"),
                    "successCallBack": that.onAddSchedulePreferences
                };

            that.AJAXING.sendPostRequest("saveMemberSchedulingPreferences.action", dataToSend, postRequestActions);
        });

        that.$btnAddNotesDialog.on("click", function(e){
            e.preventDefault();

            var $txtNote = $("#txtAddNotes"),
                notes = $.trim($txtNote.val()),
                notifyCoachesList = that.$ddNotifyCoaches.val();

            window.coachesDashboardObject = that;

            if(notes.length > 0){
                var dataToSend = { "coachNotesAJAX": notes, "patientId": that.PATIENT_ID, "customDataNumber": new Date().getTime() },
                    postRequestActions = {
                        "loading": $("#loading"),
                        "successCallBack": that.onAddCoachNotes
                    };

                if(notifyCoachesList && notifyCoachesList.length > 0){
                    dataToSend.notifyCoachesListAJAX = notifyCoachesList.join(",");
                }

                that.AJAXING.sendPostRequest("addCoachNotes.action", dataToSend, postRequestActions);
            }else{
                that.ALERT_MESSAGES.showAlertMessage($("#addNotesDialogError"), "Please add notes", that.ALERT_MESSAGES.ERROR);
            }
        });

        that.$btnAddOneToOneSessionDialog.on("click", function(e){
            e.preventDefault();

            var coachId = that.$ddCoachesList.val(),
                date = $.trim(that.$txtAddSessionDate.val()),
                sessionTime = $.trim(that.$txtAddSessionTime.val());

            window.coachesDashboardObject = that;

            if(coachId && date && sessionTime){
                var milisSelected = new Date(date+ " "+ sessionTime).getTime(),
                    actionName = "addOneToOneSessionWithPatient.action",
                    dataToSend = {"customDataNumber": milisSelected, "chosenCoachIdAJAX": coachId, "patientId": that.PATIENT_ID, "sessionTimeAJAX": sessionTime },
                    postRequestActions = {
                        "loading": $("#loading"),
                        "successCallBack": that.onAddOneToOneSession
                    };

                if(that.CUSTOM_OBJECT){
                    dataToSend.customIdAJAX = that.CUSTOM_OBJECT.sessionId;
                    actionName = "rescheduleOneToOneSessionWithPatient.action";
                }

                that.AJAXING.sendPostRequest(actionName, dataToSend, postRequestActions);
            }else{
                that.ALERT_MESSAGES.showAlertMessage($("#addOneToOneSessionDialogError"), "All fields are required.", that.ALERT_MESSAGES.ERROR);
            }
        });

        that.$btnExpColCoachArea.on("click", function(e){
            e.preventDefault();

            var $img = $(this).children("img"),
                imagePath = $img.prop("src"),
                $logDetailsButtons = $("#logDetailButtons");

            if(imagePath.indexOf("-left") > -1){
                that.$divCoachesAreaLeft.prev("div").removeClass("large-9 medium-9").addClass("large-12 medium-12");
                that.$divCoachesAreaLeft.removeClass("columns large-3 medium-3 bg-grey equal-areas").hide();
                $img.prop({
                    "src": that.IMAGES_PATH + "/resources/css/images/arrow-right.png",
                    "title": "Show"
                });


                if($logDetailsButtons && $logDetailsButtons.length > 0){
                    //$logDetailsButtons.removeClass("log-details-btn-align");
                }

            }else{
                $img.prop({
                    "src": that.IMAGES_PATH + "/resources/css/images/arrow-left.png",
                    "title": "Hide"
                });
                that.$divCoachesAreaLeft.prev("div").removeClass("large-12 medium-12").addClass("large-9 medium-9");
                that.$divCoachesAreaLeft.addClass("columns large-3 medium-3 bg-grey equal-areas").show();

                if($logDetailsButtons && $logDetailsButtons.length > 0){
                    //$logDetailsButtons.addClass("log-details-btn-align");
                }

            }
        });

        that.$btnAddDietaryNotes.on("click", function(e){
            e.preventDefault();

            var $txtNote = that.$txtDietaryNotes,
                $err = $("#errAddDietaryNotes").hide(),
                notes = $.trim($txtNote.val());

            window.coachesDashboardObject = that;

            if(notes.length > 0){
                var dataToSend = { "dietaryNotesAJAX": notes, "patientId": that.PATIENT_ID,  "customDataNumber": new Date().getTime() },
                    postRequestActions = {
                        "loading": $("#loading"),
                        "successCallBack": that.onAddDietaryNotes
                    };

                that.AJAXING.sendPostRequest("addDietaryNotes.action", dataToSend, postRequestActions);
            }else{
                $err.show();
                setTimeout(function(){
                    $err.hide();
                }, 3000);
            }
        });

        that.$btnAddMemberKeyFacts.on("click", function(e){
            e.preventDefault();

            var $txtKeyFacts = that.$txtMemberKeyFacts,
                $err = $("#errMemberKeyFacts").hide(),
                keyFacts = $.trim($txtKeyFacts.val());

            window.coachesDashboardObject = that;

            if(keyFacts.length > 0){
                var dataToSend = { "dietaryNotesAJAX": keyFacts, "patientId": that.PATIENT_ID, "customDataNumber": new Date().getTime() },
                    postRequestActions = {
                        "loading": $("#loading"),
                        "successCallBack": that.onAddMemberKeyFacts
                    };

                that.AJAXING.sendPostRequest("addMemberKeyFacts.action", dataToSend, postRequestActions);
            }else{
                $err.show();
                setTimeout(function(){
                    $err.hide();
                }, 3000);
            }
        });

        that.$btnAssignToMe.on("click", function(e){
            e.preventDefault();

            window.coachesDashboardObject = that;

            var dataToSend = {"patientId": that.PATIENT_ID },
                postRequestActions = {
                    "loading": $("#loading"),
                    "successCallBack": that.onAssignMemberToMe
                };

            that.AJAXING.sendPostRequest("assignMemberToMe.action", dataToSend, postRequestActions);
        });

        /*that.$chkMemberConsent.on("change", function(){
           if(this.checked){
               that.$divOverlayMedication.hide();
           }else{
               that.$divOverlayMedication.show();
           }
        }).trigger("change");*/

        that.$btnAddMedication.on("click", function(e){
           e.preventDefault();

           var $error = $("#errMedications").hide(),
               $txtDosage = $("#txtDosage");

            if($txtDosage.val() && that.$ddMedications.val()){
                window.coachesDashboardObject = that;


                var medSelected = $("#ddMedications").trigger("change").children("option:selected").text();

                var dataToSend = {
                        "patientId": that.PATIENT_ID,
                        "medicationMasterIdAJAX": that.MEDICATION_ID,
                        "medicationNameAJAX": medSelected,
                        "medicationConfirmedAJAX": that.$chkMemberConsent.is(":checked"),
                        "dosageAJAX": $txtDosage.val()
                    },
                    postRequestActions = {
                        "loading": $("#loading"),
                        "successCallBack": that.onSaveMedication
                    };

                that.AJAXING.sendPostRequest("saveMedication.action", dataToSend, postRequestActions);
            }else{
                $error.show();
                that.ALERT_MESSAGES.fadeOutAlertMessage($error);
            }
        });

        that.$btnAddCoachLogs.on("click", function(e){
            e.preventDefault();

            var $txtCoachLogs = that.$txtCoachLog,
                $txtCoachLogDate = that.$txtCoachLogDate,
                $err = $("#errCoachLog").hide(),
                $errCoachLogDate = $("#errCoachLogDate").hide(),
                coachLog = $.trim($txtCoachLogs.val()),
                coachLogDate = $.trim($txtCoachLogDate.val());

            window.coachesDashboardObject = that;

            if(coachLog.length > 0 && coachLogDate.length > 0){
                var dataToSend = { "customDataAJAX": coachLog, "patientId": that.PATIENT_ID, "customDataNumber": new Date().getTime(), "HBloggingDate": new Date(coachLogDate).getTime()  },
                    postRequestActions = {
                        "loading": $("#loading"),
                        "successCallBack": that.onAddCoachLogs
                    };

                that.AJAXING.sendPostRequest("addCoachLogs.action", dataToSend, postRequestActions);
            }else{
                if (coachLog.length <= 0 ) {
                    $err.show();
                    setTimeout(function () {
                        $err.hide();
                    }, 3000);
                }
                if (coachLogDate.length <= 0 ) {
                    $errCoachLogDate.show();
                    setTimeout(function () {
                        $errCoachLogDate.hide();
                    }, 3000);
                }
            }
        });

        that.$btnConfirmMedDelete.on("click", function(e){
            var dataToSend = {
                    "patientId": that.PATIENT_ID,
                    "id": that.MEDICATION_ID
                },
                postRequestActions = {
                    "loading": $("#loading"),
                    "successCallBack": that.onDeleteMedication
                };

            that.AJAXING.sendPostRequest("deleteMedication.action", dataToSend, postRequestActions);
        });

        that.$btnConfirmMedSessionDelete.on("click", function(e){
            var dataToSend = {
                    "patientId": that.PATIENT_ID,
                    "customIdAJAX": that.CUSTOM_ID,
                    "customSMSAJAX": that.CUSTOM_DATA
                },
                postRequestActions = {
                    "loading": $("#loading"),
                    "successCallBack": that.onDeleteOneToOneSession
                };

            that.AJAXING.sendPostRequest("deleteOneToOneSession.action", dataToSend, postRequestActions);
        });

        //next/prev coach todo list load
        if(that.$coachTodoNav){
            that.$coachTodoNav.off("click").on("click", function(e){
                e.preventDefault();
                var nextOrPrev = $(this).data("nav");
                that.loadAndRenderCoachTodoList(nextOrPrev);
            });
        }
    },

    onSaveMedication: function(data){
        var that = window.coachesDashboardObject;
        if(data && data.STATUS == "SUCCESS"){
            $("#txtDosage").val("");
            that.MEDICATION_ID = -1;
            that.$btnAddMedication.find("span.teal-text").text("ADD MEDICATION");
            that.$chkMemberConsent.prop("checked", false);
            PATIENT_MEDICATIONS = (data.PATIENT_MEDICATIONS) ? $.parseJSON(data.PATIENT_MEDICATIONS) : undefined;
            that.$ddMedications.chosen('destroy').prop("selectedIndex", 0);
            that.renderMedicationList();
        }
    },

    onDeleteMedication: function(data){
        var that = window.coachesDashboardObject,
            $error = $("#errMessage");

        $("#medicationDeleteDialog").foundation("reveal", "close");

        if(data && data.STATUS == "SUCCESS"){
            that.ALERT_MESSAGES.showAlertMessage($error, "Medication removed successfully.", that.ALERT_MESSAGES.SUCCESS);
            PATIENT_MEDICATIONS = (data.PATIENT_MEDICATIONS) ? $.parseJSON(data.PATIENT_MEDICATIONS) : undefined;
            that.renderMedicationList();
        }else{
            that.ALERT_MESSAGES.showAlertMessage($error, data.REASON, that.ALERT_MESSAGES.ERROR);
        }
    },

    onDeleteOneToOneSession: function(data){
        var that = window.coachesDashboardObject,
            $error = $("#errMessage");

        $("#oneToOneSessionDeleteDialog").foundation("reveal", "close");

        if(data && data.STATUS == "SUCCESS"){
            that.ALERT_MESSAGES.showAlertMessage($error, "Changes saved successfully.", that.ALERT_MESSAGES.SUCCESS);
            that.getOneToOneSessionsFromServer();
        }else{
            that.ALERT_MESSAGES.showAlertMessage($error, data.REASON, that.ALERT_MESSAGES.ERROR);
        }
    },

    onAssignMemberToMe: function(data){
        var that = window.coachesDashboardObject;
        if(data && data.STATUS == "SUCCESS"){

            PRIMARY_FOOD_COACH_ID = (data.PRIMARY_FOOD_COACH_ID) ? $.parseJSON(data.PRIMARY_FOOD_COACH_ID) : undefined;

            if(!PRIMARY_FOOD_COACH_ID){
                that.$divPrimaryFoodCoach.removeClass("hide");
            }else{
                that.$divPrimaryFoodCoach.addClass("hide");
            }

            MY_COACHES_LISTING = $.parseJSON(data.PATIENT_COACHES);
            LEAD_COACH_ID = (data.LEAD_COACH_ID) ? $.parseJSON(data.LEAD_COACH_ID) : undefined;
            that.renderMyCoaches();
        }
    },

    getCoachesAreaDataFromServer: function(){
        var that = this;

        window.coachesDashboardObject = that;

        var dataToSend = {"patientId": that.PATIENT_ID },
            postRequestActions = {
                "successCallBack": that.onGettingCoachesAreaData
            };

        that.AJAXING.sendPostRequest("getCoachesAreaDataSet.action", dataToSend, postRequestActions);
    },

    onGettingCoachesAreaData: function(data){
        var that = window.coachesDashboardObject;
        if(data && data.STATUS == "SUCCESS"){

            PRIMARY_FOOD_COACH_ID = (data.PRIMARY_FOOD_COACH_ID) ? $.parseJSON(data.PRIMARY_FOOD_COACH_ID) : undefined;
            LEAD_COACH_ID = (data.LEAD_COACH_ID) ? $.parseJSON(data.LEAD_COACH_ID) : undefined;
            COACH_TYPE = (data.COACH_TYPE) ? data.COACH_TYPE : undefined;

            if(!PRIMARY_FOOD_COACH_ID && COACH_TYPE == "Food Coach"){
                that.$divPrimaryFoodCoach.removeClass("hide");
                window.setInterval(function() {
                    that.$divPrimaryFoodCoach.toggleClass('active');
                }, 700);
            }

            COACH_NOTES_LISTING = $.parseJSON(data.COACH_NOTES);
            COACH_LOGS_LISTING = $.parseJSON(data.COACH_LOGS);
            DIETARY_NOTES_LISTING = $.parseJSON(data.DIETARY_NOTES);
            ONE_TO_ONE_SESSIONS_LISTING = $.parseJSON(data.ONE_TO_ONE_SESSIONS);
            LATEST_ONE_TO_ONE_SESSION = (data.LATEST_ONE_TO_ONE_SESSION) ? $.parseJSON(data.LATEST_ONE_TO_ONE_SESSION) : undefined;
            MY_COACHES_LISTING = $.parseJSON(data.PATIENT_COACHES);
            COACHES_LISTING = $.parseJSON(data.COACHES_LISTING);
            PATIENT_DETAILS = $.parseJSON(data.PATIENT_DETAILS);
            EXCLUDED_ME_COACH = $.parseJSON(data.EXCLUDED_ME_COACH);
            MEDICATIONS_LISTING = (data.MEDICATIONS_LISTING) ? $.parseJSON(data.MEDICATIONS_LISTING) : undefined;
            PATIENT_MEDICATIONS = (data.PATIENT_MEDICATIONS) ? $.parseJSON(data.PATIENT_MEDICATIONS) : undefined;
            PATIENT_SESSION_PREFERENCE = (data.PATIENT_SESSION_PREFERENCE) ? $.parseJSON(data.PATIENT_SESSION_PREFERENCE) : undefined;

            MEMBER_KEY_FACTS = (data.MEMBER_KEY_FACTS) ? $.parseJSON(data.MEMBER_KEY_FACTS) : undefined;

            that.UUID = data.UUID;
            that.CURRICULUM_WEEK = data.CURRICULUM_WEEK;
            CURRICULUM_API_ADDRESS = data.CURRICULUM_API_ADDRESS;

            that.renderMemberKeyFacts();
            that.loadMemberSessionPreference();
            that.loadPatientDetails();
            that.renderCoachNotes();
            that.renderCoachLogs();
            that.renderOneToOneSessions();
            that.renderMyCoaches();
            that.renderNotifyCoaches();
            that.renderDietaryNotes();
            that.renderMedicationList();
            that.renderCoachToDoList();

            var gotoPage = getQuerystringParameterByName("goto");
            if(gotoPage && gotoPage == "coachNotes"){
                that.$btnViewAllCoachNotes.click();
            }
        }
    },

    onAddOneToOneSession: function(data){
        var that = window.coachesDashboardObject;
        $("#addOneToOneSessionDialog").foundation("reveal", "close");
        if(data && data.STATUS == "SUCCESS"){

            //means session is rescheduled
            if(data.REASON == "Changed"){
                that.ALERT_MESSAGES.showAlertMessage($("#errMessage"), "Changes saved successfully.", that.ALERT_MESSAGES.SUCCESS);
            }

            //get all 1:1 session list via ajax
            that.getOneToOneSessionsFromServer();
        }
    },

    getOneToOneSessionsFromServer: function(){
        var that = window.coachesDashboardObject,
            dataToSend = {"patientId": that.PATIENT_ID },
            postRequestActions = {
                "successCallBack": that.onGettingOneToOneSessions
            };

        that.AJAXING.sendPostRequest("getAllOneToOneSessionsByPatient.action", dataToSend, postRequestActions);
    },

    onGettingOneToOneSessions: function(data){
        var that = window.coachesDashboardObject;
        if(data && data.STATUS == "SUCCESS"){
            ONE_TO_ONE_SESSIONS_LISTING = $.parseJSON(data.DATA);
            LATEST_ONE_TO_ONE_SESSION = (data.LATEST_ONE_TO_ONE_SESSION) ? $.parseJSON(data.LATEST_ONE_TO_ONE_SESSION) : undefined;

            that.renderOneToOneSessions();
        }
    },

    onAddSchedulePreferences: function(data){
        var that = window.coachesDashboardObject;
        $("#memberEatingPreferencesDialog").foundation("reveal", "close");
        if(data && data.STATUS == "SUCCESS"){
            //get member scheduling preferences via ajax
            that.getMemberSchedulingPreferenceFromServer();
        }
    },

    onAddCoachNotes: function(data){
        var that = window.coachesDashboardObject;
        $("#addNotesDialog").foundation("reveal", "close");
        if(data && data.STATUS == "SUCCESS"){
            //get all coaches notes list via ajax
            that.getCoachNotesFromServer();
        }
    },

    onAddDietaryNotes: function(data){
        var that = window.coachesDashboardObject;
        if(data && data.STATUS == "SUCCESS"){
            //get all coaches notes list via ajax
            that.$txtDietaryNotes.val("");
            that.getDietaryNotesFromServer();
        }
    },

    getDietaryNotesFromServer: function(){
        var that = window.coachesDashboardObject,
            dataToSend = {"patientId": that.PATIENT_ID },
            postRequestActions = {
                "successCallBack": that.onGettingDietaryNotes
            };

        that.AJAXING.sendPostRequest("getAllDietaryNotes.action", dataToSend, postRequestActions);
    },

    onAddMemberKeyFacts: function(data){
        var that = window.coachesDashboardObject;
        if(data && data.STATUS == "SUCCESS"){
            //get all member key facts list via ajax
            that.$txtMemberKeyFacts.val("");
            that.getMemberKeyFactsFromServer();
        }
    },

    getMemberKeyFactsFromServer: function(){
        var that = window.coachesDashboardObject,
            dataToSend = {"patientId": that.PATIENT_ID },
            postRequestActions = {
                "successCallBack": that.onGettingMemberKeyFacts
            };

        that.AJAXING.sendPostRequest("getAllMemberKeyFacts.action", dataToSend, postRequestActions);
    },

    getCoachLogsFromServer: function(){
        var that = window.coachesDashboardObject,
            dataToSend = {"patientId": that.PATIENT_ID },
            postRequestActions = {
                "successCallBack": that.onGettingLogs
            };

        that.AJAXING.sendPostRequest("getAllCoachLogs.action", dataToSend, postRequestActions);
    },

    getMemberSchedulingPreferenceFromServer: function(){
        var that = window.coachesDashboardObject,
            dataToSend = {"patientId": that.PATIENT_ID },
            postRequestActions = {
                "successCallBack": that.onGettingMemberSchedulingPreferences
            };

        that.AJAXING.sendPostRequest("getMemberSchedulingPreferences.action", dataToSend, postRequestActions);
    },

    onGettingMemberSchedulingPreferences: function(data){
        var that = window.coachesDashboardObject;
        if(data && data.STATUS == "SUCCESS"){
            PATIENT_SESSION_PREFERENCE = (data.DATA) ? $.parseJSON(data.DATA) : undefined;
            that.loadMemberSessionPreference();
        }
    },

    getCoachNotesFromServer: function(){
        var that = window.coachesDashboardObject,
            dataToSend = {"patientId": that.PATIENT_ID },
            postRequestActions = {
                "successCallBack": that.onGettingNotes
            };

        that.AJAXING.sendPostRequest("getAllCoachNotes.action", dataToSend, postRequestActions);
    },

    onGettingNotes: function(data){
        var that = window.coachesDashboardObject;
        if(data && data.STATUS == "SUCCESS"){
            COACH_NOTES_LISTING = $.parseJSON(data.DATA);
            that.renderCoachNotes();
        }
    },

    onGettingLogs: function(data){
        var that = window.coachesDashboardObject;
        if(data && data.STATUS == "SUCCESS"){
            COACH_LOGS_LISTING = $.parseJSON(data.DATA);
            that.renderCoachLogs();
        }
    },

    onGettingDietaryNotes: function(data){
        var that = window.coachesDashboardObject;
        if(data && data.STATUS == "SUCCESS"){
            DIETARY_NOTES_LISTING = $.parseJSON(data.DATA);
            that.renderDietaryNotes();
        }
    },

    onGettingMemberKeyFacts: function(data){
        var that = window.coachesDashboardObject;
        if(data && data.STATUS == "SUCCESS"){
            MEMBER_KEY_FACTS = $.parseJSON(data.DATA);
            that.renderMemberKeyFacts();
        }
    },

    onAddCoachLogs: function(data){
        var that = window.coachesDashboardObject;
        $("#addCoachLogsDialog").foundation("reveal", "close");
        if(data && data.STATUS == "SUCCESS"){
            //get all coaches notes list via ajax
            that.$txtCoachLog.val("");
            that.$txtCoachLogDate.val("");
            that.getCoachLogsFromServer();
        }
    },

    renderCoachLogs: function(){

        var that = this,
            latestLog = COACH_LOGS_LISTING[0],
            $empty = $("#emptyCoachLog").addClass("hide");
        if(latestLog){

            var $coachLogs = $("#coachLog"),
                $coachLogsDetails = $("#coachLogDate");

            $coachLogs.add($coachLogsDetails).removeClass("hide");
            var notesDateLong = parseInt(latestLog.timeToDateString);
            var HBLoggingDate = parseInt(latestLog.HBLoggingDateToString);

            $coachLogsDetails.text(latestLog.coachName + " - " + HSDateUtils.prettyDate(notesDateLong));

            $coachLogs.html('<b>'+latestLog.log + '</b><br>' + 'Date taken: ' + ((HBLoggingDate)? HSDateUtils.formatDateFromTicks(HBLoggingDate) : "N/A") );
        }else{
            $empty.removeClass("hide");
        }

        //render in dialog list all coach logs
        var $ul = $("#dialogCoachLogList").empty(),
            $emptyList = $("<li/>", {"class": "align-center", "html": "No logs"}),
            html = "";

        if(COACH_LOGS_LISTING && COACH_LOGS_LISTING.length > 0){
            $.each(COACH_LOGS_LISTING, function(index, log){

                var notesDateLong = parseInt(log.timeToDateString);
                var hbLoggingLong = parseInt(log.HBLoggingDateToString.trim());

                html += "<li><div class='grey-section-data'>"
                + "<p class='coach-name font-size-14'>"
                +    log.coachName + " - " + HSDateUtils.prettyDate(notesDateLong)
                + "</p>"
                + "<p class='coach-notes font-size-14'>"
                +    '<b>'+log.log + "</b><br/>" + "Date taken: " + ((hbLoggingLong) ? HSDateUtils.formatDateFromTicks(hbLoggingLong) : "N/A")
                + "</p>"
                + "</div></li>";
            });

            $ul.append(html);
        }else{
            $ul.append($emptyList);
        }
    },

    renderCoachNotes: function(){
        var that = this,
            latestNote = COACH_NOTES_LISTING[0],
            $empty = $("#emptyCoachNotes").addClass("hide");

        if(latestNote){

            var $coachNotes = $("#coachNotes"),
                $coachNotesDetails = $("#coachNotesNameDate");

            var notesDateLong = parseInt(latestNote.timeToDateString);

            $coachNotes.add($coachNotesDetails).removeClass("hide");

            $coachNotesDetails.text(latestNote.coachName + " - " + HSDateUtils.prettyDate(notesDateLong));
            $coachNotes.html(latestNote.notes);
        }else{
            $empty.removeClass("hide");
        }

        //render in dialog list all coach notes
        var $ul = $("#dialogCoachList").empty(),
            $emptyList = $("<li/>", {"class": "align-center", "html": "No notes"}),
            html = "";

        if(COACH_NOTES_LISTING && COACH_NOTES_LISTING.length > 0){
            $.each(COACH_NOTES_LISTING, function(index, note){

                var notesDateLong = parseInt(note.timeToDateString);

                html += "<li><div class='grey-section-data'>"
                         + "<p class='coach-name font-size-14'>"
                         +    note.coachName + " - " + HSDateUtils.prettyDate(notesDateLong)
                         + "</p>"
                         + "<p class='coach-notes font-size-14'>"
                         +    note.notes
                         + "</p>"
                     + "</div></li>";
            });

            $ul.append(html);
        }else{
            $ul.append($emptyList);
        }
    },

    renderDietaryNotes: function(){
        var that = this,
            latestNote = DIETARY_NOTES_LISTING[0],
            $empty = $("#emptyDietaryNotes").addClass("hide");

        if(latestNote){

            var $coachNotes = $("#pDietaryCoachNotes"),
                $coachNotesDetails = $("#pDietaryCoach");

            $coachNotes.add($coachNotesDetails).removeClass("hide");
            var notesDateLong = parseInt(latestNote.timeToDateString);

            $coachNotesDetails.text(latestNote.coachName + " - " + HSDateUtils.prettyDate(notesDateLong));
            $coachNotes.text(latestNote.notes);
        }else{
            $empty.removeClass("hide");
        }

        //render in dialog list all coach notes
        var $ul = $("#dietaryNotesList").empty(),
            $emptyList = $("<li/>", {"class": "align-center", "html": "No notes"}),
            html = "";

        if(DIETARY_NOTES_LISTING && DIETARY_NOTES_LISTING.length > 0){
            $.each(DIETARY_NOTES_LISTING, function(index, note){

                var notesDateLong = parseInt(note.timeToDateString);

                html += "<li><div class='grey-section-data'>"
                + "<p class='coach-name font-size-14'>"
                +    note.coachName + " - " + HSDateUtils.prettyDate(notesDateLong)
                + "</p>"
                + "<p class='coach-notes font-size-14'>"
                +    note.notes
                + "</p>"
                + "</div></li>";
            });

            $ul.append(html);
        }else{
            $ul.append($emptyList);
        }
    },

    renderMemberKeyFacts: function(){
        var that = this,
            latestKeyFact = MEMBER_KEY_FACTS[0],
            $empty = $("#emptyMemberKeyFacts").addClass("hide");

        if(latestKeyFact){

            var $pMemberKeyFacts = $("#pMemberKeyFacts"),
                $pMemberKeyFactCoachDate = $("#pMemberKeyFactCoachDate");

            $pMemberKeyFacts.add($pMemberKeyFactCoachDate).removeClass("hide");
            var keyFactsDateLong = parseInt(latestKeyFact.timeToDateString);

            $pMemberKeyFactCoachDate.text(latestKeyFact.coachName + " - " + HSDateUtils.prettyDate(keyFactsDateLong));
            $pMemberKeyFacts.text(latestKeyFact.keyFact);
        }else{
            $empty.removeClass("hide");
        }

        //render in dialog list all coach notes
        var $ul = $("#memberKeyFactsList").empty(),
            $emptyList = $("<li/>", {"class": "align-center", "html": "No member key facts added"}),
            html = "";

        if(MEMBER_KEY_FACTS && MEMBER_KEY_FACTS.length > 0){
            $.each(MEMBER_KEY_FACTS, function(index, keyFct){

                var keyFactsDateLong = parseInt(keyFct.timeToDateString);

                html += "<li><div class='grey-section-data'>"
                + "<p class='coach-name font-size-14'>"
                +    keyFct.coachName + " - " + HSDateUtils.prettyDate(keyFactsDateLong)
                + "</p>"
                + "<p class='coach-notes font-size-14'>"
                +    keyFct.keyFact
                + "</p>"
                + "</div></li>";
            });

            $ul.append(html);
        }else{
            $ul.append($emptyList);
        }
    },

    renderOneToOneSessions: function(){
        var that = this,
            latestSession = LATEST_ONE_TO_ONE_SESSION,
            $empty = $("#emptyOneToOneSession").addClass("hide"),
            $nextSessionDetails = $("#nextSessionDetails").addClass("hide");

        if(latestSession && latestSession.sessionId){

            $("#nextSession").add($nextSessionDetails).removeClass("hide");

            var sessionLongTime = parseInt(latestSession.timeToDateString);
            $("#sessionDate").text(HSDateUtils.formatDateTimeFromTicks(sessionLongTime));

            //$("#sessionDate").text(latestSession.timeToDateString + " " + latestSession.onlyTimeString);
            $("#sessionCoachName").text(latestSession.coachName);
            $("#sessionCoachType").text(latestSession.coachType);
        }else{
            $empty.removeClass("hide");
        }

        //fill dropdown coaches list
        that.$ddCoachesList.fillDD(COACHES_LISTING, "providerId", ["firstName", "lastName"], true);

        //render in dialog list all coach notes
        var $ul = $("#dialogOneToOneSessionList tbody").empty(),
            $emptyList = $("<tr/>", {"class": "align-center", "html": "<td colspan='2' class='align-center'>No sessions</td>"}),
            html = "";

        if(ONE_TO_ONE_SESSIONS_LISTING && ONE_TO_ONE_SESSIONS_LISTING.length > 0){

            $.each(ONE_TO_ONE_SESSIONS_LISTING, function(index, session){

                var navButtonsHTML = "<img src='"+that.IMAGES_PATH+"/resources/css/images/edit-new.png' title='Re-schedule' class='icon-images edit edit-session' data-id="+session.sessionId+" data-obj='"+JSON.stringify(session)+"' />"
                    + "<img src='"+that.IMAGES_PATH+"/resources/css/images/cross.svg' title='Cancel' class='icon-images cross cross-session' data-id="+session.sessionId+" />";

                if(session.disableEditing){
                    navButtonsHTML = "<img src='"+that.IMAGES_PATH+"/resources/css/images/cross.svg' title='Remove' class='icon-images cross cross-session' data-id="+session.sessionId+" data-chk='NOSMS' />";
                }

                var sessionLongTime = parseInt(session.timeToDateString);

                html += "<tr><td><div class='grey-section-data'><label class='black'>"
                + "<span class='sp-black font-size-14'> "+ HSDateUtils.formatDateTimeFromTicks(sessionLongTime) + "</span>"
                + "<span class='uppercase coach-name font-size-14'> With </span>"
                + "<span class='sp-black font-size-14'> "+ session.coachName +" </span>"
                + "<span class='sp-black italic font-size-14'> "+ session.coachType +" </span>"
                + "</label></div></td><td>"
                + navButtonsHTML + "</td></tr>";
            });

            $ul.append(html);

            $(".icon-images.cross-session").off("click").on("click", function(e){
                e.preventDefault();

                var $o2oDialogHeader = $("#o2oDialogHeader").text("Cancel Session"),
                    $o2oDialogDescription = $("#o2oDialogDescription").text("Are you sure you want to cancel this Session?");

                that.CUSTOM_ID = $(this).data("id");
                that.CUSTOM_DATA = $(this).data("chk");

                if(that.CUSTOM_DATA){
                    $o2oDialogHeader.text("Remove Session");
                    $o2oDialogDescription.text("Are you sure you want to remove this Session?");
                }

                $("#oneToOneSessionDeleteDialog").foundation("reveal", "open");
            });

            $(".icon-images.edit-session").off("click").on("click", function(e){
                e.preventDefault();
                that.CUSTOM_ID = $(this).data("id");
                that.CUSTOM_OBJECT = $(this).data("obj");
                that.$btnAddOneToOneSessionDialog.text("Update");
                that.$btnCancelOneToOneSessionDialog.removeClass("hide");
                that.editOneToOneSession(that.CUSTOM_OBJECT);
                $("#addOneToOneSessionDialog").foundation("reveal", "open");
            });

        }else{
            $ul.append($emptyList);
        }
    },

    getMyCoachesFromServer: function(){
        var that = window.coachesDashboardObject,
            dataToSend = {"patientId": that.PATIENT_ID },
            postRequestActions = {
                "successCallBack": that.onGettingMyCoaches
            };

        that.AJAXING.sendPostRequest("getMyCoaches.action", dataToSend, postRequestActions);
    },

    onGettingMyCoaches: function(data){
        var that = window.coachesDashboardObject;
        if(data && data.STATUS == "SUCCESS"){
            MY_COACHES_LISTING = $.parseJSON(data.DATA);
            that.renderMyCoaches();
        }
    },

    renderMyCoaches: function(){
        var that = this,
            $table = $("#tableMyCoaches tbody").empty(),
            html = "",
            coachIds = {};

        if(MY_COACHES_LISTING && MY_COACHES_LISTING.length > 0){
            $.each(MY_COACHES_LISTING, function(index, myCoach){

               var leadCoachHtml = "",
                   primaryCoachHtml = "";

               coachIds[myCoach.providerId] = myCoach.lastName + ", " +myCoach.firstName;

               if(myCoach.providerId == LEAD_COACH_ID) {
                   leadCoachHtml = "<span class='label warning coach-designation-label primary-coach'>Lead Coach</span>";
               }

               if(myCoach.providerId == PRIMARY_FOOD_COACH_ID){
                   primaryCoachHtml = "<span class='label warning coach-designation-label lead-coach'>Primary Food Coach</span>";
               }

                var $phone = $("<span/>", {"class": "my-coach-phone", "text": myCoach.phone}),
                    $email = $("<a/>", {"href": "mailto:"+myCoach.email, "html": myCoach.email});

                html += "<tr>" +
                            "<td>"+myCoach.lastName + ", " +myCoach.firstName + primaryCoachHtml + leadCoachHtml + "</td>" +
                            "<td>"+$email.wrap("<p/>").parent().html()+"</td>" +
                            "<td>"+$phone.wrap("<p/>").parent().html()+"</td>" +
                            "<td>"+myCoach.type+"</td>" +
                        "</tr>";
            });

            $table.append(html);

            $(".my-coach-phone").text(function(){
                var $this = $(this);
                formatPhoneNumber($this, $this.text());
            });

            that.setLeadOrPrimaryFoodCoaches(coachIds);
        }
    },

    //3d: Diabetes, Dietary, Demographics :P
    loadPatientDetails: function(){
        var that = window.coachesDashboardObject,
            patientDetails = PATIENT_DETAILS;

        if(patientDetails){

            var patDietRestrictions = patientDetails.dietaryRestrictions;

            //Dietary
            if(patDietRestrictions){
                if(patDietRestrictions.indexOf("Other") > -1){
                    var otherIndex = patDietRestrictions.indexOf("Other"),
                        removeOther = patDietRestrictions.substring(0, otherIndex);

                    if(patientDetails.dietaryRestrictionsOther){
                        patDietRestrictions = removeOther + patientDetails.dietaryRestrictionsOther;
                    }
                }
                that.$caDietaryRestrictions.text(patDietRestrictions);
            }

            if(patientDetails.mealPreparer){
                that.$caMealPreparer.text(patientDetails.mealPreparer);
            }

            //Diabetes
            if(patientDetails.diagnosis){
                that.$caDiagnosis.text(patientDetails.diagnosis);
            }

            if(patientDetails.diagnosisDate){
                that.$caDiagnosisDate.text(patientDetails.diagnosisDate);
            }

            if(patientDetails.diabetesSupport){
                that.$caDiabetesSupport.text(patientDetails.diabetesSupport);
            }

            if(patientDetails.timeSinceLastDiabetesEdu){
                that.$caTimeSinceLastEdu.text(patientDetails.timeSinceLastDiabetesEdu);
            }

            if(patientDetails.medications){
                that.$caMedications.text(patientDetails.medications);
            }

            //Demographics
            if(patientDetails.gender){
                that.$caGender.text(patientDetails.gender);
            }

            if(patientDetails.dob){
                that.$caDob.text(patientDetails.dob);
            }

            if(patientDetails.race){
                that.$caRace.text(patientDetails.race);
            }

            if(patientDetails.education){
                that.$caEducation.text(patientDetails.education);
            }
        }
    },

    renderNotifyCoaches: function(){
        var that = this;

        if(EXCLUDED_ME_COACH && EXCLUDED_ME_COACH.length > 0){
            that.$ddNotifyCoaches.fillDD(EXCLUDED_ME_COACH, "providerId", ["firstName", "lastName"], false)
                                 .chosen();
        }
    },

    setLeadOrPrimaryFoodCoaches: function(coachIds){
        var that = this,
            $spLeadPrimaryDefaultText = $("#spLeadPrimaryDefaulText").html('<span class="red-text font-bold">Note:</span> Lead Coach <span id="spLeadCoachName" class="label warning coach-designation-label primary-coach coach-name-notes"></span> & Primary Food Coach <span id="spPrimaryFoodCoachName" class="label warning coach-designation-label lead-coach coach-name-notes"></span> will be <span class="italic">automatically</span> notified.'),
            $spLeadCoachText = $("#spLeadCoachName").text("Not Set"),
            $spPrimaryCoachText = $("#spPrimaryFoodCoachName").text("Not Set");

        if(that.LOGGED_IN_PROVIDER_ID == LEAD_COACH_ID){
            $spLeadPrimaryDefaultText.html('<span class="red-text font-bold">Note:</span> Primary Food Coach <span id="spPrimaryFoodCoachName" class="label warning coach-designation-label lead-coach coach-name-notes"></span> will be <span class="italic">automatically</span> notified.');
            $spPrimaryCoachText = $("#spPrimaryFoodCoachName");
        } else if(that.LOGGED_IN_PROVIDER_ID == PRIMARY_FOOD_COACH_ID){
            $spLeadPrimaryDefaultText.html('<span class="red-text font-bold">Note:</span> Lead Coach <span id="spLeadCoachName" class="label warning coach-designation-label primary-coach coach-name-notes"></span> will be <span class="italic">automatically</span> notified.');
            $spLeadCoachText = $("#spLeadCoachName");
        }

        var myLeadCoach = coachIds[LEAD_COACH_ID],
            myPrimaryFoodCoach = coachIds[PRIMARY_FOOD_COACH_ID];

        if(myLeadCoach){
            $spLeadCoachText.text(myLeadCoach);
        }else{
            $spLeadCoachText.text("Not Set");
        }

        if(myPrimaryFoodCoach){
            $spPrimaryCoachText.text(myPrimaryFoodCoach);
        }else{
            $spPrimaryCoachText.text("Not Set");
        }
    },

    renderCoachToDoList: function(){

        var that = this,
            week = parseInt(that.CURRICULUM_WEEK);

        $("#loadingCoachTodoList").show();

        window.coachesDashboardObject = that;

        if(week == 0){
            week = 1;
        }

        that.CURRICULUM_WEEK = week;

        if(that.CURRICULUM_WEEK <= 12){
            var actionUrl = CURRICULUM_API_ADDRESS + "users/" + that.UUID + "/summary/" + week,
                postRequestActions = {
                    "requestType" : "GET",
                    "successCallBack" : that.onGettingCoachToDoList
                };

            that.AJAXING.sendRequest(actionUrl, null, postRequestActions);
        }else{
            $(".sp-coach-todo-week,.coach-todo-nav,#loadingCoachTodoList").hide();
            $("#coachTodoList").empty().append("<li class='align-center'>No coach to-do list, 12-weeks completed</li>");
        }
    },

    loadAndRenderCoachTodoList: function(nextOrPrevious){
        var that = this,
            weekNumber = parseInt(that.CURRICULUM_WEEK);

        if(nextOrPrevious == "next"){
            weekNumber += 1;
        }else{
            weekNumber -= 1;
        }

        if(weekNumber > 11){
            weekNumber = 12;
        }

        if(weekNumber <= 1){
            weekNumber = 1;
        }

        that.CURRICULUM_WEEK = weekNumber;
        that.renderCoachToDoList();
    },

    onGettingCoachToDoList: function(data){
        var that = window.coachesDashboardObject;

        if(data){
            that.buildCoachTodoList(data.coaching, data.week, that.IMAGES_PATH);
        }
    },

    buildCoachTodoList: function(todoListString, week, imagesPath){

        var that = this,
            $latestTodoList = $("#coachTodoList").empty(),
            $coachSessionWeek = $("#coachSessionWeek span"),
            $dialogCoachTodoList = $("#dialogCoachTodoList").empty(),
            html = "",
            coachToDoList = [],
            $btnPrevTodo = $("#btnPrevTodo"),
            $btnNextTodo = $("#btnNextTodo");

        if(todoListString){
            var coaching = todoListString.replace(/\*/g, '').replace(/\-/g, '').trim().split("\n");
            coachToDoList = coaching;
        }

        if(!that.IMAGES_PATH){
            that.IMAGES_PATH = imagesPath;
        }

        if(week){
            var totalLength = parseInt(coachToDoList.length);

            if(week == 1 || week == '1'){

                totalLength = totalLength - 1;
                $btnPrevTodo.addClass("coach-todo-disable").off("click").attr("disabled", "true");

            }else{

                $btnPrevTodo.removeClass("coach-todo-disable").removeAttr("disabled").off("click").on("click", function(e){

                    e.preventDefault();

                    that.loadAndRenderCoachTodoList($(this).data("nav"));
                });

                if( week > 11 || week == '12'){
                    $btnNextTodo.addClass("coach-todo-disable").off("click").attr("disabled", "true");
                }else{
                    $btnNextTodo.removeClass("coach-todo-disable")
                        .removeAttr("disabled").off("click").on("click", function(e){

                            e.preventDefault();

                            that.loadAndRenderCoachTodoList($(this).data("nav"));
                        });
                }
            }
            $coachSessionWeek.text("Week " + week + " - Item 1 of " + totalLength);
        }

        //dialog coach to do list rendering
        $.each(coachToDoList, function(index, todo){
            if(todo) {

                html += "<li>"
                + "<div class='row'>"
                + "<div class='columns small-2'>"
                + "<img src='" + that.IMAGES_PATH + "/resources/css/images/goal_icons/tick-selected.png'/>"
                + "</div>"
                + "<div class='columns small-10 padding-left-0'>"
                + todo
                + "</div>"
                + "</div>"
                + "</li>";
            }
        });
        $latestTodoList.append(html);

        $coachSessionWeek.text("Week " + week + " - Item 1 of " + $latestTodoList.children("li").length);

        $("#loadingCoachTodoList").hide();
    },

    renderMedicationList: function(){
        var that = this,
            $medicationsList = $("#medicationsList").empty(),
            $pMedication = $("#pMedicationName").addClass("hide"),
            $pMedicationDosage = $("#pMedicationDosage").addClass("hide"),
            $tableMedications = $("#tableMedications tbody").empty();

        that.$btnMedicationDelete.addClass("hide");
        if(MEDICATIONS_LISTING){
            that.$ddMedications.fillDD(MEDICATIONS_LISTING, "medicationMasterId", "name", true).chosen().prop("selectedIndex", 0).trigger("chosen:updated").on("change", function(){
                var valSelected = $(this).val();
                that.$ddMedications.children("option").prop("selected", false);
                that.$ddMedications.children("option[value='"+valSelected+"']").prop("selected", true);
            });
        }

        if(PATIENT_MEDICATIONS && PATIENT_MEDICATIONS.length > 0){

            var html = '', htmlMedList = '';

            $.each(PATIENT_MEDICATIONS, function(index, med){
                var dateString = new Date(med.timestamp).toString("MM/dd/yyyy");
                html += "<tr>" +
                            "<td>"+med.medicationsName+"</td>" +
                            "<td>"+med.dosage+"</td>" +
                            "<td>"+dateString+"</td>" +
                            "<td><img src='"+that.IMAGES_PATH+"/resources/css/images/cross.svg' title='Remove' class='cross dialog-del-med' data-id="+med.patientMedicationId+" /></td>" +
                        "</tr>";

                var confirmedHtml = "<img src='"+that.IMAGES_PATH+"/resources/css/images/not-confirmed.png' title='Not Confirmed' class='med-taken-status' />"
                if(med.isConfirmed){
                    confirmedHtml = "<img src='"+that.IMAGES_PATH+"/resources/css/images/confirmed.png' title='Member Confirmed' class='med-taken-status' />"
                }
                htmlMedList += "<li>" +
                                   "<div class='row position-relative'>" +
                                        "<div class='small-4 columns'>" +
                                            "<p class='coach-notes margin-bottom-0'>"+med.medicationsName + " </p>" +
                                        "</div>" +
                                        "<div class='small-4 columns'>" +
                                            "<p class='coach-notes margin-bottom-0'>" + med.dosage + " </p>" +
                                        "</div>" +
                                        "<div class='small-4 columns'>" +
                                            "<img src='"+that.IMAGES_PATH+"/resources/css/images/edit-new.png' title='Edit' class='icon-images edit edit-med' data-obj='"+JSON.stringify(med)+"' data-id="+med.patientMedicationId+" />" +
                                            "<img src='"+that.IMAGES_PATH+"/resources/css/images/cross.svg' title='Remove' class='icon-images cross cross-med' data-id="+med.patientMedicationId+" />" +
                                        "</div>" +
                                        confirmedHtml +
                                   "</div>" +
                                "</li>";

            });
            //$tableMedications.append(html);

            var firstRow = '<li class="first">'
                                +'<div class="row">'
                                    +'<div class="columns small-4"><p class="coach-name">Medication</p></div>'
                                    +'<div class="columns small-4"><p class="coach-name">Dosage</p></div>'
                                    +'<div class="columns small-4">&nbsp;</div>'
                                +'</div>'
                            +'</li>';

            $medicationsList.append(firstRow+htmlMedList);
            $(".icon-images.cross-med").off("click").on("click", function(e){
                e.preventDefault();
                that.MEDICATION_ID = $(this).data("id");
                $("#medicationDeleteDialog").foundation("reveal", "open");
            });

            $(".icon-images.edit-med").off("click").on("click", function(e){
                e.preventDefault();
                that.MEDICATION_ID = $(this).data("id");
                that.editMedication($(this).data("obj"));
            });
        }else{
            $tableMedications.append("<tr> <td class='align-center' colspan='4'> No medication added </td> </tr>");
            $medicationsList.append("<li> <p class='align-center'> No medication added </p> </li>");
        }
    },

    loadMemberSessionPreference: function(){
        var that = this,
            $emptyMemberSessionPreference = $("#emptyMemberSessionPreference").removeClass("hide"),
            $divMemberSessionPreference = $("#divMemberSessionPreference").addClass("hide"),
            $pMemberSessionPreferenceDays = $("#pMemberSessionPreferenceDays").addClass("hide"),
            $pMemberSessionPreferenceTimes = $("#pMemberSessionPreferenceTimes").addClass("hide");

        if(PATIENT_SESSION_PREFERENCE){

            $emptyMemberSessionPreference.addClass("hide");
            $divMemberSessionPreference.removeClass("hide");
            $pMemberSessionPreferenceDays.removeClass("hide").text(PATIENT_SESSION_PREFERENCE.days);
            $pMemberSessionPreferenceTimes.removeClass("hide").text(PATIENT_SESSION_PREFERENCE.times);

            $("#memberEatingPreferencesDialog").off("opened closed").on({
                opened: function () {

                    //loading in modal for edit
                    var splitDays = PATIENT_SESSION_PREFERENCE.days.split(", "),
                        splitTimes = PATIENT_SESSION_PREFERENCE.times.split(", ");

                    $.each(splitDays, function(index, day){
                        $(".session-days[value='"+day+"']").prop("checked", "true");
                    });

                    $.each(splitTimes, function(index, time){
                        $(".session-times[value='"+time+"']").prop("checked", "true");
                    });
                },
                closed: function () {

                }
            });
        }
    },

    editMedication: function(med){
        var that = this,
            $option = that.$ddMedications.find("option:contains(" + med.medicationsName + ")");

        $("#txtDosage").val(med.dosage);
        that.$chkMemberConsent.prop("checked", med.isConfirmed);
        that.$ddMedications.val($option.val()).chosen().trigger("chosen:updated");
        that.$btnAddMedication.find("span.teal-text").text("UPDATE MEDICATION");
    },

    editOneToOneSession: function(session){
        var that = this;
        that.$ddCoachesList.val(session.coachId);

        var sessionLongTime = parseInt(session.timeToDateString);

        that.$txtAddSessionDate.val(HSDateUtils.formatDateFromTicks(sessionLongTime));
        that.$txtAddSessionTime.val(HSDateUtils.formatTimeFromTicks(sessionLongTime));
    }
};

//================================================
/* A plugin to fill dropdown with passing array */
$.fn.fillDD = function(values, valKey, textKey, addSelectText){
    var $dropdown = $(this).empty();
    $.each(values, function(val, obj) {
        var fullName = obj[textKey];
        if($.isArray(textKey)){
            fullName = obj[textKey[1]] + ", " + obj[textKey[0]];
        }
        var $option = $("<option/>", {"value": obj[valKey], "text": fullName});
        $dropdown.append($option);
    });
    if(addSelectText){
        $dropdown.children(":first").before("<option value='' selected='selected' disabled>(select)</option>");
    }
    return $dropdown;
};

//====================================================
/* A plugin to set equal heights for the containers */
(function($) {
    $.fn.equalRowHeights = function() {

        var $this = this;

        $this.height('auto');
        var count = this.length;

        for (var i = 0; i < count; ) {

            var x = this.eq(i).offset().top;

            var $rowEls = this.filter(function() {
                return $(this).offset().top === x;
            });

            $rowEls.height( Math.max.apply(null, $rowEls.map(function() { return $(this).height(); })) );

            i = $rowEls.filter(':last').index() + 1;
        }

        var timer = 0;
        if (!$this.data('equalRowHeight')) {
            $(window).on('resize', function(e) {
                if (timer) {
                    clearTimeout(timer);
                }
                timer = setTimeout(function() {
                    $this.equalRowHeights();
                }, 50);
            });
            $this.data('equalRowHeight', true);
        }
    };
})(jQuery);