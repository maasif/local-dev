/**
 * Created by omursaleen on 6/30/2015.
 */

if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() {
        };
        F.prototype = obj;
        return new F();
    };
}

//var timezone =  d.getTimezoneOffset() // difference in minutes from GMT
//==========================================
/* HSDateUtils JS Model*/
var HSDateUtils = {

    getCurrentTimeUTC: function () {
        //RETURN:
        //      = number of milliseconds between current UTC time and midnight of January 1, 1970
        var tmLoc = new Date();
        //The offset is in minutes -- convert it to ms
        return tmLoc.getTime() + tmLoc.getTimezoneOffset() * 60000;
    },

    getCurrentTimeUTC: function (dateString) {
        //RETURN:
        //      = number of milliseconds between current UTC time and midnight of January 1, 1970
        var tmLoc = new Date(dateString);
        //The offset is in minutes -- convert it to ms
        return tmLoc.getTime() + tmLoc.getTimezoneOffset() * 60000;
    },

    formatDateTimeFromTicks: function (nTicks) {
        var d = new Date(nTicks);
        return moment(d).format("MM/DD/YYYY hh:mm A");
    },

    formatDateFromTicks: function (nTicks, format) {
        var d = new Date(nTicks);

        if(!format){
            format = "MM/DD/YYYY";
        }

        return moment(d).format(format);
    },

    formatTimeFromTicks: function (nTicks) {
        var d = new Date(nTicks);
        return moment(d).format("hh:mm A");
    },

    dateObjectFromTicks: function (nTicks) {
        var d = new Date(nTicks);
        return Date.UTC(d.getFullYear(), d.getMonth(), d.getDate());
    },

    getHoursFromDate: function (date) {
        //var d = new Date(date);
        return moment(date, "MM/DD/YYYY hh:mm A Z").hours();
    },

    dateRangeToMillisRange: function(dateRangeString, startAndEndChunks, showInCurrentTimezone){

        var that = this,
            dateParts = dateRangeString.split("-");

        var startDate = new Date(dateParts[0]),
            endDate = new Date(dateParts[1]);

        //means start time of startDate and end time of endDate
        if(startAndEndChunks){
            startDate.setHours(0, 0, 0, 0);
            endDate.setHours(23, 59, 59, 999);
        }

        //if we want time millis in current timezone offset
        if(showInCurrentTimezone){
            return startDate.getTime() + "-" + endDate.getTime();
        }

        return Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()) + "-" + endDate.getTime();
    },

    getStartOfDayTime: function(dateMili){
        var date = new Date(dateMili);
        var startOfDayTime = date.setHours(0,0,0,0);

        return startOfDayTime;
    },

    dateRangeFromMilli: function(startDate, noOfDays, isUTC){
        var minMax = [],
            oneDayMilli = 24 * 60 * 60 * 1000,
            firstDate = new Date(startDate);
        minMax.push(firstDate.getTime());

        for(var i = 1; i < noOfDays ; i++) {
            var utcTime = firstDate.getTime();
            if(isUTC){
                utcTime = Date.UTC(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate());
            }
            minMax.push(utcTime + oneDayMilli);
        }

        return minMax;
    },

    dateRangeFromRange: function(startDate, noOfDays, isUTC){
        var minMax = [],
            dateParts = startDate.split("-"),
            oneDayMilli = 24 * 60 * 60 * 1000,
            firstDate = new Date(dateParts[0]);

        if(isUTC){
            minMax.push(Date.UTC(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate()));
        } else {
            minMax.push(firstDate.getTime());
        }

        for(var i = 1; i < noOfDays ; i++) {

            var utcTime = firstDate.getTime();
            if(isUTC){
                utcTime = Date.UTC(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate());
            }
            minMax.push(utcTime + oneDayMilli);
            firstDate = new Date(firstDate.getTime() + oneDayMilli);
        }

        return minMax;
    },

    getWeekDaysDatesArray: function (date, format){
        var that = this,
            rangeDatesArray = [],
            firstDate = date, secondDate = date, thirdDate = date, fourthDate = date, fifthDate = date, sixthDate = date, seventhDate = date;

        firstDate.setDate(firstDate.getDate());
        firstDate = that.formatDateFromTicks(firstDate.getTime(), format);
        secondDate.setDate(secondDate.getDate() + 1);
        secondDate = that.formatDateFromTicks(secondDate.getTime(), format);
        thirdDate.setDate(thirdDate.getDate() + 1);
        thirdDate = that.formatDateFromTicks(thirdDate.getTime(), format);
        fourthDate.setDate(fourthDate.getDate() + 1);
        fourthDate = that.formatDateFromTicks(fourthDate.getTime(), format);
        fifthDate.setDate(fifthDate.getDate() + 1);
        fifthDate = that.formatDateFromTicks(fifthDate.getTime(), format);
        sixthDate.setDate(sixthDate.getDate() + 1);
        sixthDate = that.formatDateFromTicks(sixthDate.getTime(), format);
        seventhDate.setDate(seventhDate.getDate() + 1);
        seventhDate = that.formatDateFromTicks(seventhDate.getTime(), format);

        rangeDatesArray[0] = firstDate;
        rangeDatesArray[1] = secondDate;
        rangeDatesArray[2] = thirdDate;
        rangeDatesArray[3] = fourthDate;
        rangeDatesArray[4] = fifthDate;
        rangeDatesArray[5] = sixthDate;
        rangeDatesArray[6] = seventhDate;

        return rangeDatesArray;
    },

    prettyDate: function(time, format){

        var d = new Date(time),
            rightNow = moment(time).fromNow();

        if(rightNow == "a day ago"){
            return "Yesterday" + (format ? " at " + moment(d).format(format) : "");
        }else if (rightNow.indexOf("days") > -1 || rightNow.indexOf("month") > -1 ){
            if ( format ) {
                return moment(d).format(format);
            }
            else {
                return moment(d).format("MM/DD/YYYY");
            }
        }

        return rightNow;
    },

    addDays: function(theDate, format, days) {
        return moment(theDate, format).add(days, 'days');
    }
};
