/**
 * Created by Irfan Nasim on 10/9/2015.
 */

if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//==========================================
/* EditMembersTeam JS Model*/
var EditMembersTeam = {

    init: function(patientId){
        var that = this;

        //ajaxing instance
        that.AJAXING = Object.create(Ajaxing);
        that.PATIENT_ID = patientId;

        window.memberTeamObject = that;

        that.cachePageConstants();
        that.bindEventListeners();

        return that;
    },

    cachePageConstants: function(){
       var that = this;

       that.$btnEditMemberTeam = $("#btnEditMemberTeam");
       that.$btnSaveMemberTeam = $("#btnSaveMemberTeam");
       that.$editMemberTeamDialog = $("#editMemberTeamDialog");
       that.errMessageMemberTeamDialog =  $("#errMessageMemberTeamDialog");
       that.$dialogLoading = $("#dialogLoadingSpinner");
       that.$btnEditMembersTeamNav = $("#btnEditMembersTeamNav");
    },

    bindEventListeners: function(){
        var that = this;

        that.$btnEditMembersTeamNav.on("click", function (e) {
            e.preventDefault();
            that.$editMemberTeamDialog.foundation("reveal", "open");
        });

        that.$btnEditMemberTeam.on("click", function (e) {
            e.preventDefault();
            that.$editMemberTeamDialog.foundation("reveal", "open");
        });

        that.$btnSaveMemberTeam.on("click", function(e){
            e.preventDefault();

            window.memberTeamObject = that;

            var selectedProviders = $(".selected-providers:checked").map(function(){
                return $(this).val();
            }).get().join(", ");

            if(selectedProviders){
                var actionUrl = "updateMemberTeam.action",
                    dataToSend = {
                        "patientId": patId,
                        "providerListString": selectedProviders
                    }, postRequestActions = {
                        "successCallBack": that.teamMemberUpdated,
                        "loading": that.$dialogLoading
                    };
                sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
            }else{
                var alertMessage = AlertMessage.init();
                alertMessage.showAlertMessage($("#errMessageMemberTeamDialog"), "Please select at least one coach." , alertMessage.ERROR);
            }
        });

        that.$editMemberTeamDialog.on({
            opened: function(){
                var actionUrl = "getMemberTeamList.action",
                    dataToSend = {
                        "patientId":  that.PATIENT_ID
                    }, postRequestActions = {
                        "requestType": "GET",
                        "successCallBack": that.populateCoachesInDialog,
                        "loading": that.$dialogLoading
                    };
                sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
            }
        });
    },

    populateCoachesInDialog: function populateCoachesInDialog(data) {
        var that = window.memberTeamObject,
            $listFoodCoaches = $("#lstFoodCoaches").empty(),
            $listCoaches = $("#lstCoaches").empty();

        var htmlStringFoodCoaches = "", htmlStringCoaches = "";

        $.each(data.COACHES_LISTING, function (index, p) {
            if(p.type == "Food Coach"){
                htmlStringFoodCoaches += '<li title="'+ (p.email) +'">'
                + '<div>'
                + '<input type="checkbox" name="provider" class="selected-providers" value="' + p.providerId + '">'
                + '<span class="sel-pro-lbl">' + p.firstName + ' ' + p.lastName + ' </span>'
                + '</div>'
                + '</li>';
            }else{
                htmlStringCoaches += '<li title="'+ (p.email) +'">'
                + '<div>'
                + '<input type="checkbox" name="provider" class="selected-providers" value="' + p.providerId + '">'
                + '<span class="sel-pro-lbl">' + p.firstName + ' ' + p.lastName + ' </span>'
                + '</div>'
                + '</li>';
            }
        });

        $listFoodCoaches.append(htmlStringFoodCoaches);
        $listCoaches.append(htmlStringCoaches);

        //set checked values
        $.each(data.PATIENT_COACHES, function (index, p) {
            $('.selected-providers[value="'+p.providerId+'"]').prop("checked", true);
        });

        var $lblLeadCoach = $("#lblLeadCoach"),
            $lblPrimaryCoach = $("#lblPrimaryCoach");

        if(data.LEAD_COACH_ID){
            $lblLeadCoach.removeClass("hide").children("#spLeadCoach").text(data.LEAD_COACH_ID.firstName + " " + data.LEAD_COACH_ID.lastName).prop("title", data.LEAD_COACH_ID.email);
            $('.selected-providers[value="'+data.LEAD_COACH_ID.provider.providerId+'"]').prop("disabled", "disabled").parent().addClass("disabled");
        }else{
            $lblLeadCoach.parents("li").hide();
        }

        if(data.PRIMARY_FOOD_COACH_ID){
            $lblPrimaryCoach.removeClass("hide").children("#spPrimaryFoodCoach").text(data.PRIMARY_FOOD_COACH_ID.firstName + " " + data.PRIMARY_FOOD_COACH_ID.lastName).prop("title", data.PRIMARY_FOOD_COACH_ID.email);
            $('.selected-providers[value="'+data.PRIMARY_FOOD_COACH_ID.provider.providerId+'"]').prop("disabled", "disabled").parent().addClass("disabled");
        }else{
            $lblPrimaryCoach.parents("li").hide();
        }
    },

    teamMemberUpdated: function teamMemberUpdated(data){
        var that = window.memberTeamObject,
            $error = that.errMessageMemberTeamDialog,
            alertMessage = AlertMessage.init();

        if(data && data.STATUS === 'SUCCESS'){
            alertMessage.showAlertMessage($error, "Member's team updated successfully" , alertMessage.SUCCESS);
            that.$editMemberTeamDialog.foundation("reveal", "close");
        } else {
            alertMessage.showAlertMessage($error, "Unable to save member's team" , alertMessage.ERROR);
        }
    }
};
