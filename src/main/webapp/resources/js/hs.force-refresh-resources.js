/**
 * Created by omursaleen on 3/13/2015.
 */

if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//==========================================
/* HSForceRefreshResources JS Model*/
var HSForceRefreshResources = {

    refreshResources: function(){
        var that = this;

        //that.refreshCSSFiles();
        //that.refreshJSFiles();

        return that;
    },

    refreshJSFiles: function(){
        $("script").each(function(index, lnk){
            var href = $(this).prop("src");
            href += "?v="+new Date().getTime();
            $(this).prop("src", href);
        });
    },

    refreshCSSFiles: function(){
        $("link").each(function(index, lnk){
            var href = $(this).prop("href");
            href += "?v="+new Date().getTime();
            $(this).prop("href", href);
        });
    }
};
