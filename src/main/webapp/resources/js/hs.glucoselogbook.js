/**
 * Created by omursaleen on 6/24/2015.
 */

function initLogBook(){
    initDatePicker("selWeeklyGlucoseLogsDate");
    getWeeklyGlucoseLogsData();

    $("#selWeeklyGlucoseLogsDate").on("change", function(){

        var date = new Date($(this).val());

        if(validateDate(date)){

            var endDate = $('#selWeeklyGlucoseLogsDate').datepicker('getDate');

            endDate.setDate(endDate.getDate() + 6);

            if(validateDate(endDate)){

                $(this).val($(this).val() + " - " + $.datepicker.formatDate('m/d/yy', endDate));

            } else{

                endDate = new Date();

                $(this).val($(this).val() + " - " + $.datepicker.formatDate('m/d/yy', endDate));
            }

            oldLogsDate = $('#selWeeklyGlucoseLogsDate').val();

            disableEnableButtons(oldLogsDate);

            getWeeklyGlucoseLogsData();

        } else {

            $(this).val(oldLogsDate);

            alert("Please select a valid date");

            showAlertMessage($("#logbookerror"), "Please select a valid date", "ERROR");

            return false;
        }
    });

    oldLogsDate = $('#selWeeklyGlucoseLogsDate').val();

    disableEnableButtons(new Date());
}

function getWeeklyGlucoseLogsData(){

    var actionUrl = "fetchWeeklyGlucoseLogsData.action",
        dataToSend = {
            "patientId": $("#patientId").val(),
            "weeklyLogsDateString": $("#selWeeklyGlucoseLogsDate").val()
        },
        postRequestActions = {
            "requestType" : "GET",
            "successCallBack" : showWeeklyGlucoseLogs,
            "loading": $("#loading")
        };

    sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
}

function getGroupedArray(list, timeKey) {
    var arrNames = [];

    $.each(list, function (index, item) {

        var key = HSDateUtils.formatDateFromTicks(item[timeKey], "dddd");

        if (!arrNames[key]) {
            arrNames[key] = [];
        }
        arrNames[key].push(item);
    });

    return arrNames;
}

function showWeeklyGlucoseLogs(data){
    var glucoseData = data.Glucose_Logs;
    var glucoseDatewiseData = [];
    var endDate = $('#selWeeklyGlucoseLogsDate').datepicker('getDate');
    var rangeDatesArray = HSDateUtils.getWeekDaysDatesArray(endDate, "dddd");

    glucoseDatewiseData = getGroupedArray(glucoseData, "logTime");

    //console.log(glucoseDatewiseData, rangeDatesArray);
    populateGlucoseDateTable(rangeDatesArray, glucoseDatewiseData);
}

function validateDate(date){

    var today = new Date().getTime();

    if (today  < date.getTime())   {
        return false;
    }

    return true;
}

function disableEnableButtons(dateString){

    var sourceDate = $.datepicker.formatDate('m/d/yy', new Date(dateString)),
        todayDate = $.datepicker.formatDate('m/d/yy', new Date()),
        $logDateNext = $("#logDateNext").removeAttr("disabled");

    if(sourceDate == todayDate){
        $logDateNext.attr("disabled", true);
    }
}

function getNextWeek(){

    var $selWeeklyGlucoseLogsDate = $('#selWeeklyGlucoseLogsDate'),
        dateString = $selWeeklyGlucoseLogsDate.val(),
        datesMap = getNextStartEndDates(dateString);

    $selWeeklyGlucoseLogsDate.val(datesMap["startDate"] + " - " + datesMap["endDate"]);

    oldLogsDate = $selWeeklyGlucoseLogsDate.val();

    getWeeklyGlucoseLogsData();
}

function getPreWeek(){

    var $selWeeklyGlucoseLogsDate = $('#selWeeklyGlucoseLogsDate'),
        dateString = $selWeeklyGlucoseLogsDate.val(),
        datesMap = getPreStartEndDates(dateString);

    $selWeeklyGlucoseLogsDate.val(datesMap["startDate"] + " - " + datesMap["endDate"]);

    oldLogsDate = $selWeeklyGlucoseLogsDate.val();

    getWeeklyGlucoseLogsData();
}

function getPreStartEndDates(dateString){

    var dateStringParts = dateString.split("-");
    var sourceDate = $.trim(dateStringParts[0]);

    var date = new Date(sourceDate);
    date.setDate(date.getDate() - 1);
    var endDate = $.datepicker.formatDate('m/d/yy', date);

    date.setDate(date.getDate() - 6);
    var startDate = $.datepicker.formatDate('m/d/yy', date);

    var datesMap = {};
    datesMap["startDate"] = startDate;
    datesMap["endDate"] = endDate;
    disableEnableButtons(endDate);
    return datesMap;
}

function getNextStartEndDates(dateString){

    var startDate,
        endDate,
        dateStringParts = dateString.split("-"),
        sourceDate = $.trim(dateStringParts[1]);

    var date = new Date(sourceDate);
    date.setDate(date.getDate() + 1);
    if(validateDate(date)){
        startDate = $.datepicker.formatDate('m/d/yy', date);
    } else{
        date = new Date();
        startDate = $.datepicker.formatDate('m/d/yy', date);
    }

    date.setDate(date.getDate() + 6);
    if(validateDate(date)){
        endDate = $.datepicker.formatDate('m/d/yy', date);
    }else{
        date = new Date();
        endDate = $.datepicker.formatDate('m/d/yy', date);
    }

    var datesMap = {};
    datesMap["startDate"] = startDate;
    datesMap["endDate"] = endDate;
    disableEnableButtons(endDate);
    return datesMap;
}

function populateGlucoseDateTable(dateRanges, data) {
    var $tbody = $("#tblGlucoseLogs tbody"),
        html = '',
        indexArrays = [];

    //creating table cells
    $.each(dateRanges, function (index, dayName) {
        html += "<tr><td>" + dayName + "</td>" + getTableScriptOfData() + "</tr>";
        if(data[dayName]){
            var indexObjectAndData = {
                "index": index,
                "data": data[dayName]
            }
            indexArrays.push(indexObjectAndData);
        }
    });

    $tbody.html(html);

    if(indexArrays){
        $.each(indexArrays, function(index, trIndex){
            var cellWiseValues = trIndex.data;
            $.each(cellWiseValues, function(i, dt){
                var cellObject = getTableCell(dt);
                $tbody.children("tr").eq(trIndex.index)
                      .children("td")
                      .eq(cellObject.tdIndex)
                      .addClass(cellObject.tdBG)
                      .text(dt.glucoseLevel);
            });
        });
    }
}

function getTableScriptOfData(trIndex, data) {
    var html = '';

    for(var i = 0; i < 12 ; i++){
        html += "<td></td>";
    }

    return html;
}

function getTableCell(data){
    var cellObject = {
            tdIndex: 0
        },
        hours = HSDateUtils.getHoursFromDate(data.logTimeString),
        currentVal = data.glucoseLevel,
        beforeAfterMeal = data.gType,
        targetMin = (TARGETS) ? TARGETS.glucoseMin2 : 0,
        targetMax = (TARGETS) ? TARGETS.glucoseMax2 : 0;

    if(beforeAfterMeal && beforeAfterMeal.toLowerCase() == "after meal"){
        targetMin = (TARGETS) ? TARGETS.glucoseMin : 0;
        targetMax = (TARGETS) ? TARGETS.glucoseMax : 0;
    }

    if (hours >= 0 && hours < 2) {
        cellObject.tdIndex = 1;
    } else if (hours >= 2 && hours < 4) {
        cellObject.tdIndex = 2;
    } else if (hours >= 4 && hours < 6) {
        cellObject.tdIndex = 3;
    } else if (hours >= 6 && hours < 8) {
        cellObject.tdIndex = 4;
    } else if (hours >= 8 && hours < 10) {
        cellObject.tdIndex = 5;
    } else if (hours >= 10 && hours < 12) {
        cellObject.tdIndex = 6;
    } else if (hours >= 12 && hours < 14) {
        cellObject.tdIndex = 7;
    } else if (hours >= 14 && hours < 16) {
        cellObject.tdIndex = 8;
    } else if (hours >= 16 && hours < 18) {
        cellObject.tdIndex = 9;
    } else if (hours >= 18 && hours < 20) {
        cellObject.tdIndex = 10;
    }else if (hours >= 20 && hours < 22) {
        cellObject.tdIndex = 11;
    }else if (hours >= 22 && hours < 0) {
        cellObject.tdIndex = 12;
    }

    cellObject.tdBG = getTableCellBg(currentVal, targetMin, targetMax);
    return cellObject;
}

function getTableCellBg(currentVal, targetMin, targetMax){
    var tdBG = "align-center ";

    if(currentVal > targetMin && currentVal < targetMax){
        tdBG = "align-center glu-in";
    }else if(currentVal < targetMin){
        tdBG = "align-center";
    }else if(currentVal > targetMax){
        tdBG = "align-center glu-over";
    }

    return tdBG;
}
