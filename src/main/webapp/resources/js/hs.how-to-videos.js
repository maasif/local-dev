/**
 * Created by omursaleen on 9/17/2015.
 */

if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//==========================================
/* HSHowToVideos JS Model*/
var HSHowToVideos = {

    init: function(config){
        var that = this;

        that.CONFIG = config;
        that.WISTIA_PROJECT_HASH_ID = "v9pgsd9mn0";
        that.TOKEN = that.CONFIG.token;
        window.howToVideosObject = that;

        that.cachePageConstants();
        that.getHowToVideos();

        return that;
    },

    cachePageConstants: function(){
       var that = this;

       that.$listHowtoVideos = $("#listHowtoVideos").empty();
       that.$iframePlayer = $("#iframePlayer");
       that.$hVideoTitle = $("#hVideoTitle");
       that.$helpText = $(".help-text");
    },

    getHowToVideos: function(){
        var that = this,
            projectUrl = "https://api.wistia.com/v1/projects/",
            $loading = $("#listLoadingSpinner").show();

        $.getJSON(projectUrl + that.WISTIA_PROJECT_HASH_ID + ".json?api_password="+that.TOKEN)
            .done(function(wistiaProject) {

                var html = "";
                $loading.hide();
                if(wistiaProject){

                  var medias = wistiaProject.medias;

                  if(medias && medias.length > 0){
                      $.each(medias, function(index, media){
                          var date = new Date(null);
                          date.setSeconds(media.duration);
                          if(media.type == "Video"){
                              html += that.getVideoListItem(media, date, that.CONFIG.mobileOrWeb);
                          }
                      });
                      that.$listHowtoVideos.append(html);
                      that.bindEventListener();
                  }else{
                      alert("No medias found.");
                  }
                }else{
                    alert("Unable to get wistia project details.");
                }
            }).fail(function( jqxhr, textStatus, error ) {
                $loading.hide();
                alert("Error getting videos from Wistia API.");
                console.log(jqxhr, textStatus, error);
            });
    },

    getVideoListItem: function(media, date, isForMobile){

        var html = "";

        //for mobile member app
        if(isForMobile){
            html += "<li>";
            html += "<a href='#' data-videoid='"+media.hashed_id+"'>";
            html += "<img src='"+media.thumbnail.url+"' width='100' />";
            html += "<p class='video-title'>"+media.name.replace("HT_", "")+"</p>";
            html += "<p class='video-time'>"+date.toISOString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1").replace(/^0+/, '').replace(/^:+/, '')+"</p>";
            html += "</a>";
            html += "</li>";
        }else{
            html += "<li>";
            html += "<a href='#' data-videoid='"+media.hashed_id+"'>";
            html += "<div class='row'>";
            html += "<div class='columns medium-3 large-3 three'>";
            html += "<img src='"+media.thumbnail.url+"' width='100' />";
            html += "</div>";
            html += "<div class='columns medium-9 large-9 nine padding-left-0'>";
            html += "<p class='video-title'>"+media.name.replace("HT_", "")+"</p>";
            html += "<p class='video-time'>"+date.toISOString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1").replace(/^0+/, '').replace(/^:+/, '')+"</p>";
            html += "</div>";
            html += "</div>";
            html += "</a>";
            html += "</li>";
        }

        return html;
    },

    bindEventListener: function(){

        var that = window.howToVideosObject,
            $videoItems = that.$listHowtoVideos.find("a");

        //for mobile member app
        if(that.CONFIG.mobileOrWeb){

           var $divVideoItems = $("#divVideoItems"),
               $divIframe = $("#divIframe");

            $videoItems.on("click", function(e){
                e.preventDefault();
                $divVideoItems.hide();
                $divIframe.removeClass("hide");
                $videoItems.removeClass("video-selected");
                $(this).addClass("video-selected");
                that.$hVideoTitle.text($(this).find("p.video-title").text());
                that.$helpText.hide();
                that.$iframePlayer.attr("src", "//fast.wistia.net/embed/iframe/"+$(this).data("videoid"));
            });

            $("#btnBack").on("click", function(e){
                e.preventDefault();
                $divVideoItems.show();
                $divIframe.addClass("hide");
                $videoItems.removeClass("video-selected").blur();
                that.$iframePlayer.attr("src", "about:blank");
            });

        }else{
            $videoItems.on("click", function(e){
                e.preventDefault();
                $videoItems.removeClass("video-selected");
                $(this).addClass("video-selected");
                that.$hVideoTitle.text($(this).find("p.video-title").text());
                that.$helpText.hide();
                that.$iframePlayer.removeClass("hide").attr("src", "//fast.wistia.net/embed/iframe/"+$(this).data("videoid"));
            });
        }
    }
};
