/**
 * Created by omursaleen on 6/24/2015.
 */

function initLogBook(){
    initDatePicker("selWeeklyLogsDate");
    getWeeklyLogsData("");

    $("#ddFilterBy").on("change", function(){
        onChangeEvent(false);
    });

    $("#selWeeklyLogsDate").on("change", function(){
        onChangeEvent(true);
    });

    disableEnableButtons(new Date());
}

function onChangeEvent(shouldOverride){
    var $this = $("#selWeeklyLogsDate"),
        date = new Date($this.val());

    if(validateDate(date)){

        var endDate = $this.datepicker('getDate');
        endDate.setDate(endDate.getDate() + 6);

        if(validateDate(endDate)){
            if(shouldOverride){
                $this.val($this.val() + " - " + $.datepicker.formatDate('m/d/yy', endDate));
            }
        } else{
            endDate = new Date();
            if(shouldOverride) {
                $this.val($this.val() + " - " + $.datepicker.formatDate('m/d/yy', endDate));
            }
        }

        oldLogsDate = $this.val();
        disableEnableButtons(oldLogsDate);
        getWeeklyLogsData($("#ddFilterBy").val());

    } else {
        $this.val(oldLogsDate);
        alert("Please select a valid date");
        showAlertMessage($("#logbookerror"), "Please select a valid date", "ERROR");
        return false;
    }

    oldLogsDate = $this.val();
}

function getWeeklyLogsData(logType){

    var actionUrl = "fetchWeeklyLogsData.action",
        dataToSend = {
            "patientId": $("#patientId").val(),
            "weeklyLogsDateString": $("#selWeeklyLogsDate").val(),
            "logName": logType
        },
        postRequestActions = {
            "requestType" : "GET",
            "successCallBack" : showWeeklyLogs,
            "loading": $("#loading")
        };

    sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
}

//[2/10/2015]: refactored by, __oz
function getGroupedArray(list) {
    var arrNames = [];

    $.each(list, function (index, item) {

        var datewise = item.onlyDate,
            date = new Date(datewise),
            key = $.datepicker.formatDate(FORMAT_DATE, date);

        if (!arrNames[key]) {
            arrNames[key] = [];
        }
        arrNames[key].push(item);
    });

    return arrNames;
}

function sortByDate(a, b){

    /*if(SORT_TYPE == "desc"){
        return new Date(a.timestamp) - new Date(b.timestamp);
    }*/

    return new Date(a.createdOn) - new Date(b.createdOn);
}

//[2/10/2015]: refactored by, __oz
function showWeeklyLogs(data){

    var datewiseGroupedMeals = getGroupedArray(data.Meal_Logs),
        datesMap = setLogsDataLbls(),
        mealLength = data.Meal_Logs.length,
        MEAL_TARGETS = buildCarbTargets((TARGETS)?TARGETS.mealTargets:null);

    for(var index = 0; index < mealLength; index++){
        var result = data.Meal_Logs[index],
            date = new Date(result.onlyDate),
            key = $.datepicker.formatDate(FORMAT_DATE, date),
            $dataDiv = $(""+datesMap[key]).empty();
        if(key in datesMap){
            $dataDiv.removeClass("align-center");
        }else{
            $dataDiv.append("<li class='centered-li'>No log found</li>");
        }
    }

    //start -- refactored code:

    $.each(datesMap, function(key, $div){
        if(key in datesMap){
            var mealsList = datewiseGroupedMeals[key];
            if(mealsList){

                //asc order each row
                mealsList.sort(sortByDate);

                $.each(mealsList, function(jIndex, result){

                    var $a,
                        $valuelbl,
                        $infolbl,
                        $img,
                        $logtype,
                        $logtime,
                        $li,
                        $wrapper,
                        $div = "",
                        timeString = result.onlyTime;

                    switch (result.logType){
                        case "Meal":
                            $a = $("<a/>", {"href":BASE_URL+'app/educator/fetchLogDetail.action?logId='+result.logId+'&patientId='+$('#patientId').val()+'&redirectToPage=fetchlogbook'});
                            $logtype = '<label class="meal-log-info">'+result.MealType.toUpperCase()+'</label>';
                            $logtime = '<label class="meal-log-time">'+timeString+'</label>';
                            $img = "<img class='meal-img' src='"+BASE_URL+"resources/images/meal_log_img.png'/>";

                            if(result.carbs == "N/A"){
                                $div = $("<div/>", {"class":"log-data-div glu-under"});
                                $valuelbl = '<label class="data-div-value-prg">In <br/> Process</label>';
                                $infolbl = '<label class="data-div-lbl-prg"></label>';
                            } else{
                                var carbs = (result.carbs == "N/A") ? "0g" : Math.round(result.carbs)+"g",
                                    isMealOver = false;

                                if(carbs != "N/A"){
                                    var mTarget = MEAL_TARGETS[result.MealType];
                                    var fCarbs = result.carbs;

                                    try{
                                        fCarbs = parseFloat(result.carbs);
                                    }catch(err){
                                        fCarbs = result.carbs;
                                    }

                                    if(fCarbs > mTarget){
                                        isMealOver = true;
                                    }

                                    if(mTarget == 0){
                                        isMealOver = false;
                                    }
                                }

                                if(isMealOver){
                                    $div = $("<div/>", {"class":"log-data-div meal-over"});
                                    $valuelbl = '<label class="data-div-value-white">'+carbs+'</label>';
                                    $infolbl = '<label class="data-div-lbl-white">Carbs</label>';
                                } else{
                                    $div = $("<div/>", {"class":"log-data-div meal-under"});
                                    $valuelbl = '<label class="data-div-value">'+carbs+'</label>';
                                    $infolbl = '<label class="data-div-lbl">Carbs</label>';
                                }
                            }

                            $li = $("<li/>");
                            $wrapper = $("<div/>", {"class": "wrapper-logbook-item"});
                            throwInDOM($(""+datesMap[key]), $li, $wrapper, $div, $a, $valuelbl, $infolbl, $img, $logtype, $logtime);

                            break;
                        case "Glucose":

                            var beforeAfterMeal = result.gType,
                                gTargetMin = (TARGETS)?TARGETS.glucoseMin2:0,
                                gTargetMax = (TARGETS)?TARGETS.glucoseMax2:0;

                            if(beforeAfterMeal){
                                if(beforeAfterMeal.toLowerCase() == "after meal"){
                                    gTargetMin = (TARGETS)?TARGETS.glucoseMin:0;
                                    gTargetMax = (TARGETS)?TARGETS.glucoseMax:0;
                                }

                                var glucoseData = getImageByLogType(parseInt(result.glucoseLevel), gTargetMin, gTargetMax, result.logType);

                                $a = $("<a/>", {"href":BASE_URL+'app/educator/fetchLogDetail.action?logId='+result.logId+'&patientId='+$('#patientId').val()+'&redirectToPage=fetchlogbook'});
                                $logtype = '<label class="meal-log-info">'+result.logType.toUpperCase()+'</label>';
                                $logtime = '<label class="meal-log-time">'+timeString+'</label>';
                                $img = $("<img/>", {"class": glucoseData.resources.class, "src": glucoseData.resources.image}).wrap("<p/>").parent().html();

                                if(glucoseData.flags.isBelow){
                                    $div = $("<div/>", {"class":"log-data-div glu-under"});
                                    $valuelbl = '<label class="data-div-value">'+result.glucoseLevel+'</label>';
                                    $infolbl = '<label class="data-div-lbl">mg/dl</label>';
                                } else if(glucoseData.flags.isIn){
                                    $div = $("<div/>", {"class":"log-data-div glu-in"});
                                    $valuelbl = '<label class="data-div-value">'+result.glucoseLevel+'</label>';
                                    $infolbl = '<label class="data-div-lbl">mg/dl</label>';
                                } else if(glucoseData.flags.isOver){
                                    $div = $("<div/>", {"class":"log-data-div glu-over"});
                                    $valuelbl = '<label class="data-div-value-white">'+result.glucoseLevel+'</label>';
                                    $infolbl = '<label class="data-div-lbl-white">mg/dl</label>';
                                } else {
                                    $div = $("<div/>", {"class":"log-data-div glu-in"});
                                    $valuelbl = '<label class="data-div-value">'+result.glucoseLevel+'</label>';
                                    $infolbl = '<label class="data-div-lbl">mg/dl</label>';
                                }

                                $li = $("<li/>");
                                $wrapper = $("<div/>", {"class": "wrapper-logbook-item"});
                                throwInDOM($(""+datesMap[key]), $li, $wrapper, $div, $a, $valuelbl, $infolbl, $img, $logtype, $logtime);
                            }

                            break;
                        case "Weight":
                            var weightData = getImageByLogType(result.glucoseLevel, TARGETS.glucoseMin2, TARGETS.glucoseMax2, result.logType);

                            $a = $("<a/>", {"href":BASE_URL+'app/educator/fetchLogDetail.action?logId='+result.logId+'&patientId='+$('#patientId').val()+'&redirectToPage=fetchlogbook'});
                            $logtype = '<label class="meal-log-info">'+result.logType.toUpperCase()+'</label>';
                            $logtime = '<label class="meal-log-time">'+timeString+'</label>';
                            $img = $("<img/>", {"class": weightData.resources.class, "src": weightData.resources.image}).wrap("<p/>").parent().html();

                            $div = $("<div/>", {"class":"log-data-div glu-under"});
                            $valuelbl = '<label class="data-div-value">'+result.weight+'</label>';
                            $infolbl = '<label class="data-div-lbl">lbs</label>';

                            $li = $("<li/>");
                            $wrapper = $("<div/>", {"class": "wrapper-logbook-item"});
                            throwInDOM($(""+datesMap[key]), $li, $wrapper, $div, $a, $valuelbl, $infolbl, $img, $logtype, $logtime);

                            break;
                        case "Activity":
                            var activityData = getImageByLogType(result.stepsPerDay, TARGETS.activitySteps, TARGETS.activitySteps, result.logType);

                            $a = $("<a/>", {"href":BASE_URL+'app/educator/fetchLogDetail.action?logId='+result.logId+'&patientId='+$('#patientId').val()+'&redirectToPage=fetchlogbook'});
                            $logtype = '<label class="meal-log-info">'+result.logType.toUpperCase()+'</label>';
                            $logtime = '<label class="meal-log-time">'+timeString+'</label>';
                            $img = $("<img/>", {"class": activityData.resources.class, "src": activityData.resources.image}).wrap("<p/>").parent().html();

                            if(activityData.flags.isBelow){
                                $div = $("<div/>", {"class":"log-data-div glu-in"});
                                $valuelbl = '<label class="data-div-value">'+result.stepsPerDay+'</label>';
                                $infolbl = '<label class="data-div-lbl">steps</label>';
                            } else if(activityData.flags.isOver){
                                $div = $("<div/>", {"class":"log-data-div glu-over"});
                                $valuelbl = '<label class="data-div-value-white">'+result.stepsPerDay+'</label>';
                                $infolbl = '<label class="data-div-lbl-white">steps</label>';
                            }

                            $li = $("<li/>");
                            $wrapper = $("<div/>", {"class": "wrapper-logbook-item"});
                            throwInDOM($(""+datesMap[key]), $li, $wrapper, $div, $a, $valuelbl, $infolbl, $img, $logtype, $logtime);

                            break;
                        case "Medication":
                            var activityData = getImageByLogType(result.stepsPerDay, TARGETS.activitySteps, TARGETS.activitySteps, result.logType);

                            $a = $("<a/>", {"href":BASE_URL+'app/educator/fetchLogDetail.action?logId='+result.logId+'&patientId='+$('#patientId').val()+'&redirectToPage=fetchlogbook'});
                            $logtype = '<label class="meal-log-info">'+result.logType.toUpperCase()+'</label>';
                            $logtime = '<label class="meal-log-time">'+timeString+'</label>';
                            $div = $("<div/>", {"class":"log-data-div meal-under"});
                            $valuelbl = '<label class="data-div-value font-size-20">Took <br/> Med</label>';
                            $infolbl = '<label class="data-div-lbl">&nbsp;</label>';

                            $li = $("<li/>");
                            $wrapper = $("<div/>", {"class": "wrapper-logbook-item"});
                            throwInDOM($(""+datesMap[key]), $li, $wrapper, $div, $a, $valuelbl, $infolbl, null, $logtype, $logtime);

                            break;

                        case "ActivityMinute":
                            var activityData = getImageByLogType(result.stepsPerDay, 0, 0, result.logType);

                            $a = $("<a/>", {"href":BASE_URL+'app/educator/fetchLogDetail.action?logId='+result.logId+'&patientId='+$('#patientId').val()+'&redirectToPage=fetchlogbook'});
                            $logtype = '<label class="meal-log-info">'+"Activity".toUpperCase()+'</label>';
                            $logtime = '<label class="meal-log-time">'+timeString+'</label>';
                            $img = $("<img/>", {"class": activityData.resources.class, "src": activityData.resources.image}).wrap("<p/>").parent().html();

                            $div = $("<div/>", {"class":"log-data-div glu-in"});
                            $valuelbl = '<label class="data-div-value">'+result.alMinutesPerformed+'</label>';
                            $infolbl = '<label class="data-div-lbl">mins</label>';

                            $li = $("<li/>");
                            $wrapper = $("<div/>", {"class": "wrapper-logbook-item"});
                            throwInDOM($(""+datesMap[key]), $li, $wrapper, $div, $a, $valuelbl, $infolbl, $img, $logtype, $logtime);
                            break;
                        default:
                            break;
                    }
                });
            }
        }
    });

    //end -- refactored code
}

//[2/10/2015]: added by, __oz
function throwInDOM($container, $li, $wrapper, $div, $a, $valuelbl, $infolbl, $img, $logtype, $logtime){
    if($div){
        $a.append($valuelbl).append($infolbl).append($img);
        $div.append($a);
        $wrapper.append($div).append($logtype).append($logtime);
        $li.append($wrapper);
        $container.find("li.centered-li").remove().end().append($li);
    }else{
        //$container.append("<li class='centered-li'>No log found</li>");
    }
}

//[2/10/2015]: added by, __oz
function getImageByLogType(currentVal, targetMin, targetMax, logType){

    var LOG_TYPE_IMAGES = {
            "Glucose": {"class": "glucose-img", "image": BASE_URL+"resources/images/glucose_log_img.png"},
            "Weight": {"class": "glucose-img", "image": BASE_URL+"resources/images/weight_img.png"},
            "Activity": {"class": "glucose-img", "image": BASE_URL+"resources/images/activity_img.png"},
            "ActivityMinute": {"class": "glucose-img", "image": BASE_URL+"resources/images/activity_img.png"},
            "Meal": {"class": "meal-img", "image": BASE_URL+"resources/images/meal_log_img.png"}
        },
        flags = {
            "isOver": false,
            "isBelow": false,
            "isIn": false
        };

    if(logType == "Glucose"){

        if(currentVal > targetMin && currentVal < targetMax){
            //in
            flags.isIn = true;
        }else if(currentVal < targetMin){
            //below
            flags.isBelow = true;
        }else if(currentVal > targetMax){
            //over
            flags.isOver = true;
        }

    }else{
        if(currentVal > targetMin){
            //over
            flags.isOver = true;
        }else if(currentVal < targetMin){
            //below
            flags.isBelow = true;
        }else{
            //equals
            flags.isIn = true;
        }
    }

    return {"resources": LOG_TYPE_IMAGES[logType] , "flags": flags };
}

//[2/10/2015]: added by, __oz
function buildCarbTargets(mealTargets){

    var targetsArray = {};

    $.each(mealTargets, function(index, mealTarget){
        targetsArray[mealTarget.mealType] = mealTarget.carbTarget;
    });

    return targetsArray;
}

function validateDate(date){

    var today = new Date().getTime();

    if (today  < date.getTime())   {
        return false;
    }

    return true;
}

function setLogsDataLbls(){

    var datesMap = {},
        dateParts = $("#selWeeklyLogsDate").val().split("-"),
        sourceDate = $.trim(dateParts[1]),
        date = new Date(sourceDate);

    for(var index = 1; index <= 7; index++){

        if(index == 1){
            date.setDate(date.getDate());
        }else{
            date.setDate(date.getDate() - 1);
        }
        datesMap[$.datepicker.formatDate(FORMAT_DATE, date)] = "#last-"+index+"-day";
        $("#last-"+index+"-day-lbl").text($.datepicker.formatDate('DD mm/dd', date));
        $("#last-"+index+"-day").empty().addClass("align-center").append("<li>No log found</li>");
    }

    return datesMap;
}

function disableEnableButtons(dateString){

    var sourceDate = $.datepicker.formatDate('m/d/yy', new Date(dateString)),
        todayDate = $.datepicker.formatDate('m/d/yy', new Date()),
        $logDateNext = $("#logDateNext").removeAttr("disabled");

    if(sourceDate == todayDate){
        $logDateNext.attr("disabled", true);
    }
}

function getNextWeek(){

    var $selWeeklyLogsDate = $('#selWeeklyLogsDate'),
        dateString = $selWeeklyLogsDate.val(),
        datesMap = getNextStartEndDates(dateString);

    $selWeeklyLogsDate.val(datesMap["startDate"] + " - " + datesMap["endDate"]);

    oldLogsDate = $selWeeklyLogsDate.val();

    getWeeklyLogsData($("#ddFilterBy").val());
}

function getPreWeek(){

    var $selWeeklyLogsDate = $('#selWeeklyLogsDate'),
        dateString = $selWeeklyLogsDate.val(),
        datesMap = getPreStartEndDates(dateString);

    $selWeeklyLogsDate.val(datesMap["startDate"] + " - " + datesMap["endDate"]);

    oldLogsDate = $selWeeklyLogsDate.val();

    getWeeklyLogsData($("#ddFilterBy").val());
}

function getPreStartEndDates(dateString){

    var dateStringParts = dateString.split("-");
    var sourceDate = $.trim(dateStringParts[0]);

    var date = new Date(sourceDate);
    date.setDate(date.getDate() - 1);
    var endDate = $.datepicker.formatDate('m/d/yy', date);

    date.setDate(date.getDate() - 6);
    var startDate = $.datepicker.formatDate('m/d/yy', date);

    var datesMap = {};
    datesMap["startDate"] = startDate;
    datesMap["endDate"] = endDate;
    disableEnableButtons(endDate);
    return datesMap;
}

function getNextStartEndDates(dateString){

    var startDate,
        endDate,
        dateStringParts = dateString.split("-"),
        sourceDate = $.trim(dateStringParts[1]);

    var date = new Date(sourceDate);
    date.setDate(date.getDate() + 1);
    if(validateDate(date)){
        startDate = $.datepicker.formatDate('m/d/yy', date);
    } else{
        date = new Date();
        startDate = $.datepicker.formatDate('m/d/yy', date);
    }

    date.setDate(date.getDate() + 6);
    if(validateDate(date)){
        endDate = $.datepicker.formatDate('m/d/yy', date);
    }else{
        date = new Date();
        endDate = $.datepicker.formatDate('m/d/yy', date);
    }

    var datesMap = {};
    datesMap["startDate"] = startDate;
    datesMap["endDate"] = endDate;
    disableEnableButtons(endDate);
    return datesMap;
}
