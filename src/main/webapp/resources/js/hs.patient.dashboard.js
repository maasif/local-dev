/**
 * Created by omursaleen on 2/25/2015.
 */

if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

$.expr[":"].contains = $.expr.createPseudo(function(arg) {
    return function( elem ) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

//==========================================
/* HSPatientDashboard JS Model*/
var HSPatientDashboard = {

    init: function(params){
        var that = this;

        that.PATIENT_ID = params.patientId;
        that.UUID = params.uuid;
        that.IS_DASHBOARD = params.isFromDashboard;
        that.GOALS_RATING_BUNDLE = [];

        if(params.week){
            that.WEEK = params.week;
        }

        //that.hidePatientId();

        //alert message instance
        that.ALERT_MESSAGES = Object.create(AlertMessage).init();

        //ajaxing instance
        that.AJAXING = Object.create(Ajaxing);

        that.GOALS_JSON_KEYS = {
            "goalOption" : "<b>My Goal</b>",
            "actionPlan" : "<b>My Action Plan</b>",
            "myChallenge" : "<b>My Challenge</b>",
            "overcomeChallenge" : "To <b>Overcome</b> this	Challenge <b>I will</b>",
            "myReward" : "<b>My Reward</b>",
            "remindMe": "<b>Remind Me</b>"
        };

        that.CATEGORY_COLOR = {
            "Healthy Eating": {"class": "healthy-eating", "small": "Eating"},
            "Physical Activity": {"class": "physical-activity", "small": "Activity"},
            "Medications & Health Mgmt": {"class": "medication-monitoring", "small": "Meds"},
            "Money, Work and Travel": {"class": "work-money", "small": "Work"},
            "Stress and Sleep": {"class": "stress-sleep", "small": "Sleep"},
            "Family & Relationships": {"class": "family-relationship", "small": "Family"}
        };

        that.IMAGES_PATH = params.imagesPath;
        window.patientDashboardObject = that;

        that.cachePageConstants();
        that.alignUnreadCount();

        if(that.IS_DASHBOARD){

            that.getSummaryData();
            that.getAllGoalsAndHabits();
            that.getCurriculumProgress(that.UUID);
            that.getMisfitAverageStepsAndBatteryStatus();
            that.getLogEntriesCountAndCurrentWeight();
            that.getUserActivityCount();
        }

        that.bindEventListeners();
        that.getUnreadInappMessagesCount();

        //then sets refresh interval after each 50 seconds
        setInterval(function(){
            that.getUnreadInappMessagesCount();
        }, 50000);

        return that;
    },

    cachePageConstants: function(){
       var that = this;

       that.$spUnreadCount = $("#spUnreadCount");
       that.$divPatientInfo = $("#divPatientInfo");
       that.$divPatName = $("#divPatName");
       that.$divPatID = $("#divPatID");
       that.$btnExpColPatInfo = $("#btnExpColPatInfo");
       that.$btnSeeAllGoalsBtn = $("#btnSeeAllGoalsBtn");
       that.$btnMotivationImage = $("#btnMotivationImage");
       that.$btnSecureLink = $("#btnSecureLink");
       /*that.$btnViewGroupFeed = $("#btnViewGroupFeed");*/

        var $phoneLabel = $("#dashboardPhone"),
            phoneText = $phoneLabel.text();

        //written in common.js file
        formatPhoneNumber($phoneLabel, phoneText);
    },

    bindEventListeners: function(){
        var that = this;

        that.$btnExpColPatInfo.on("click", function(e){
            e.preventDefault();
            var $img = $(this).children("img"),
                title = $img.prop("title"),
                $btnExpColCoachArea = $("#btnExpColCoachArea");

            if(title == "Show member information"){
                $img.prop({
                    "src": that.IMAGES_PATH + "/resources/css/images/arrow-up.png",
                    "title": "Hide member information"
                });

                var id = that.$divPatID.data("id").toString();
                that.$divPatientInfo.show();
                that.$divPatName.show();
                that.$divPatID.text(id).show();
                $(this).children("span").text("Hide member information");

                $("#divEmpty").addClass("large-1");
                $("#divPatNameId").removeClass("large-6").addClass("large-5");
                $btnExpColCoachArea.parent().addClass("top-132").removeClass("top-77");
            }else{
                $img.prop({
                    "src": that.IMAGES_PATH + "/resources/css/images/arrow-down.png",
                    "title": "Show member information"
                });

                that.$divPatientInfo.hide();
                that.$divPatName.hide();

                that.hidePatientId();
                $("#divEmpty").removeClass("large-1");
                $("#divPatNameId").removeClass("large-5").addClass("large-6");

                $(this).children("span").text("Show member information");
                $btnExpColCoachArea.parent().removeClass("top-132").addClass("top-77");
            }
        });

        that.$btnSeeAllGoalsBtn.on("click", function(e){
            e.preventDefault();
            $("#allGoalsAndHabitsDialog").foundation("reveal", "open");
        });

        that.$btnMotivationImage.on("click", function(e){
            e.preventDefault();
            $("#motivationImageDialog").foundation("reveal", "open");
        });

        /*that.$btnViewGroupFeed.on("click", function(e){
            e.preventDefault();
            $("#groupFeedDialog").foundation("reveal", "open");
        });*/

        $("#secureLinkDialog").on({
            opened: function () {
                $("#btnCopySecureLink").off("click").on("click", function(){
                    var copyTextarea = document.querySelector('#txtSecureLink');
                    copyTextarea.select();
                    try {
                        var successful = document.execCommand('copy');
                        var msg = successful ? 'successful' : 'unsuccessful';
                        if(msg == 'successful'){
                            that.ALERT_MESSAGES.showAlertMessage($("#secureLinkDialogError"), "Copied to your clipboard.", that.ALERT_MESSAGES.SUCCESS);
                        }else{
                            console.log('Copying text command was ' + msg);
                        }
                    } catch (err) {
                        console.log('Oops, unable to copy');
                    }
                });
            },
            closed: function () {
                $("#btnCopySecureLink").off("click");
            }
        });

        that.$btnSecureLink.on("click", function(e){
            e.preventDefault();
            $("#secureLinkDialog").foundation("reveal", "open");
        });
    },

    getUnreadInappMessagesCount: function(){
        var that = this,
            dataToSend = { "patientId" : that.PATIENT_ID },
            postRequestActions = {
                "successCallBack": that.onGettingMessagesCount
            };

        that.AJAXING.sendPostRequest("getUnreadMessagesCount.action", dataToSend, postRequestActions);
    },

    onGettingMessagesCount: function(data){
        var that = window.patientDashboardObject;
        that.$spUnreadCount.text(data.DATA);
        that.alignUnreadCount();
    },

    alignUnreadCount: function(){
        var that = this,
            unreadCount = parseInt(that.$spUnreadCount.removeClass("left-5").text()),
            leftClass = "";

        if(unreadCount > 10){
            leftClass = "left-5";
        }

        that.$spUnreadCount.addClass(leftClass).text(unreadCount);
    },

    getUserActivityCount: function(){
        var that = this,
            dataToSend = { "email": EMAIL },
            postRequestActions = {
                "successCallBack": that.onGettingUserActivityCount,
                "loading": $("#loadingActivityUser")
            };

        that.AJAXING.sendPostRequest("getUserActivityCount.action", dataToSend, postRequestActions);
    },

    onGettingUserActivityCount: function(data){
        var activity = data.DATA;
        if(activity){
            $("#spSharedPosts").text(activity.sharedPost);
            $("#spLikes").text(activity.likes);
            $("#spComments").text(activity.comments);
        }
    },

    getAllGoalsAndHabits: function(){
        var that = this,
            dataToSend = { "patientId" : that.PATIENT_ID },
            postRequestActions = {
                "successCallBack": that.onGettingGoalsAndHabits,
                "loading": $("#loadingGoalsHabitsTop")
            };

        that.AJAXING.sendPostRequest("getAllGoalsAndHabits.action", dataToSend, postRequestActions);
    },

    onGettingGoalsAndHabits: function(data){
        var that = window.patientDashboardObject,
            $ulGoals = $("#goalListTop,#goalListDialog").empty(),
            $ulHabits = $("#habitListDialog").empty();

        if(data.STATUS == "SUCCESS"){
            GOALS_LISTING = (data.GOAL) ? $.parseJSON(data.GOAL) : undefined;
            GOAL_RATINGS = (data.GOAL_RATINGS) ? $.parseJSON(data.GOAL_RATINGS) : undefined;

            //buckle app goal ratings group by formIds
            if(GOAL_RATINGS && GOAL_RATINGS.length > 0){
                $.each(GOAL_RATINGS, function(index, goalRating){
                    var formId = goalRating.formId;
                    if(!that.GOALS_RATING_BUNDLE[formId]){
                        that.GOALS_RATING_BUNDLE[formId] = [];
                        that.GOALS_RATING_BUNDLE[formId].push(goalRating);
                    }else{
                        that.GOALS_RATING_BUNDLE[formId].push(goalRating);
                    }
                });
            }

            that.renderGoals($ulGoals);

            HABITS_LISTING = (data.HABIT) ? $.parseJSON(data.HABIT) : undefined;
            that.renderHabits($ulHabits);

            var $goalRating = $("[data-goalrating]");
            that.fillRating($goalRating, "goalrating");

            that.bindGoalDetailsDialog();

        }else{
            $ulGoals.append("<li><div class='align-center padding-10'>No goals found</div></li>");
            $ulHabits.append("<li><div class='align-center padding-10'>No habits found</div></li>");
        }
    },

    buildActionPlanString: function(goalDetails, actionPlan, planFrequency, calendarFrequency, meals, other){

        var mealsString = "";

        if(meals){
            mealsString =  " when " + meals;
        }

        if(!calendarFrequency){
            calendarFrequency = "";
        }

        var actionPlanString = actionPlan;

        if(actionPlan.toLowerCase() == 'create my own'){
            actionPlanString = goalDetails.actionPlanOthers;
        }
        return (actionPlanString + " " + planFrequency + " per " + calendarFrequency + mealsString);
    },

    buildGoalDetailsArray: function(dataList){
        var goalDetails = {};

        //converting fieldname and fieldValue to single goalDetailsObject
        $.each(dataList, function(index, fd){
            var fieldName = fd.fieldName;
            goalDetails[fieldName] = fd.fieldValue;
        });

        return goalDetails;
    },

    renderGoals: function($container){

        var that = this,
            html = "",
            dataArray = [];

        if(!$.isEmptyObject(GOALS_LISTING)){
            dataArray = GOALS_LISTING;
        }

        that.goalsDataArray = dataArray;

        $("#goalListTop,#goalListDialog").each(function(index, ul){
            $ul = $(ul).empty();
            html = "";
            if(dataArray && dataArray.length > 0){

                var newDataArray = dataArray;

                //in case of goal list at top, show only latest one goal
                if($ul.attr("id") == "goalListTop"){
                    newDataArray = [];
                    newDataArray.push(dataArray[0]);
                }

                $.each(newDataArray, function(index, goal){

                    html += "<li>";

                    var listHtml = "",
                        dataList = goal.formDataList,
                        goalDetails = {};

                    if(dataList && dataList.length > 0){
                        goalDetails = that.buildGoalDetailsArray(dataList);
                    }

                    if(goalDetails){
                        html += that.buildGoalItemTop(goal, goalDetails);
                    }

                    html += "</li>";
                });

                $ul.append(html);

                $(".view-goal-rating").off("click").on("click", function(e){
                    e.preventDefault();
                    var formId = $(this).data("id");
                    that.loadGoalRatingHistory(formId);
                });

            }else{
                $ul.append("<li><div class='align-center padding-10'>No goals found</div></li>");
            }
        });
    },

    buildGoalItem: function(goal, goalDetails){
        var that = this,
            actionPlanString = that.buildActionPlanString(goalDetails, goal.actionPlan, goalDetails.planFrequency, goalDetails.calendarFrequency, goalDetails.meals, goalDetails.other),
            classText = that.CATEGORY_COLOR[$.trim(goal.categoryName)],
            bgColor = classText.class,
            foreColor = bgColor+"-text",
            goalName = goal.goalName;

        if(goalName.toLowerCase() == "create my own"){
            goalName = goalDetails.goalOthers;
        }

        var rating = "";

        if(goalDetails.goalRating > -1){
            rating = "data-goalrating="+goalDetails.goalRating;
        }

        return  "<div class='row row-auto'>" +
                "<div class='columns medium-5 padding-left-0'>" +
                    "<div class='goals-item'>"
                        + "<div class='category-outer "+ bgColor +"'>"
                        +	"<div class='category-name'>"+classText.small+"</div>"
                        + "</div>"
                        + "<div class='goal-details'>"
                        + "<div class='goal-name "+ foreColor +"'>"+goalName+"</div>"
                        + "<div class='grey-black-pair'>"
                        + "	<label class='inline margin-bottom-5 grey "+ foreColor +"'>Action Plan</label>"
                        + "	<label class='inline margin-bottom-0 black'>"+ actionPlanString +"</label>"
                        + "</div>"
                    + "</div>"+
                "</div></div>"+
                "<div class='columns medium-3'>" +
                    "<ul "+rating+" class='large-block-grid-5 small-block-grid-5 scales create-scales goal-progress-rating'>"
                    + "<li class='position-relative' data-scale='1'><label class='inline'><img src='"+that.IMAGES_PATH + "/resources/patient-forms-resources/css/images/unchecked.png' class='important' data-ischecked='false'></label></li>"
                    + "<li class='position-relative' data-scale='2'><label class='inline'><img src='"+that.IMAGES_PATH + "/resources/patient-forms-resources/css/images/unchecked.png' class='important' data-ischecked='false'></label></li>"
                    + "<li class='position-relative' data-scale='3'><label class='inline'><img src='"+that.IMAGES_PATH + "/resources/patient-forms-resources/css/images/unchecked.png' class='important' data-ischecked='false'></label></li>"
                    + "<li class='position-relative' data-scale='4'><label class='inline'><img src='"+that.IMAGES_PATH + "/resources/patient-forms-resources/css/images/unchecked.png' class='important' data-ischecked='false'></label></li>"
                    + "<li class='position-relative' data-scale='5'><label class='inline'><img src='"+that.IMAGES_PATH + "/resources/patient-forms-resources/css/images/unchecked.png' class='important' data-ischecked='false'></label></li>"
                    + "</ul>"+
                "</div>" +
                "<div class='columns medium-3'>" +
                    " <a href='#' data-formdata='"+JSON.stringify(goal.formDataList)+"' class='button radius tiny margin-bottom-0 white-bordered-btn'>Goal Details</a>" +
                    " <a href='goalsHabitsDashboard.action?patientId="+ that.PATIENT_ID +"' class='button radius tiny margin-bottom-0 white-bordered-btn'>Coach Assessment</a>" +
                "</div>" +
            "</div>";
    },

    buildGoalItemTop: function(goal, goalDetails){
        var that = this,
            actionPlanString = that.buildActionPlanString(goalDetails, goal.actionPlan, goalDetails.planFrequency, goalDetails.calendarFrequency, goalDetails.meals, goalDetails.other),
            classText = that.CATEGORY_COLOR[$.trim(goal.categoryName)],
            bgColor = classText.class,
            foreColor = bgColor+"-text",
            goalName = goal.goalName,
            goalRatingsFromList = that.GOALS_RATING_BUNDLE[goal.filledFormId];

        if(goalName.toLowerCase() == "create my own"){
            goalName = goalDetails.goalOthers;
        }

        var rating = "";

        if(goalDetails.goalRating){
            rating = "data-goalrating="+goalDetails.goalRating;
        }

        if(goalDetails.goalRating == 0){
            rating = "data-goalrating="+goal.rating;
        }

        var goalRatingsBtnHTML = "";
        if(goalRatingsFromList){
            goalRatingsBtnHTML = "<a href='javascript:;' data-id='"+goal.filledFormId+"' class='button btn-teal tiny radius margin-bottom-0 view-goal-rating'>View All Ratings</a>";
        }

        return  "<div class='row row-auto'>" +
            "<div class='columns medium-6 padding-left-0'>" +
            "<div class='goals-item'>"
            + "<div class='category-outer "+ bgColor +"'>"
            +	"<div class='category-name'>"+classText.small+"</div>"
            + "</div>"
            + "<div class='goal-details'>"
            + "<div class='goal-name "+ foreColor +"'>"+goalName+"</div>"
            + "<div class='grey-black-pair'>"
            + "	<label class='inline margin-bottom-5 grey "+ foreColor +"'>Action Plan</label>"
            + "	<label class='inline margin-bottom-0 black'>"+ actionPlanString +"</label>"
            + "</div>"
            + "</div>"+
            "</div></div>"+
            "<div class='columns medium-6'>"
            +"<ul "+rating+" class='large-block-grid-5 small-block-grid-5 scales create-scales goal-progress-rating top-goal-image'>"
            + "<li class='position-relative' data-scale='1'><label class='inline'><img src='"+that.IMAGES_PATH + "/resources/patient-forms-resources/css/images/unchecked.png' class='important' data-ischecked='false'></label></li>"
            + "<li class='position-relative' data-scale='2'><label class='inline'><img src='"+that.IMAGES_PATH + "/resources/patient-forms-resources/css/images/unchecked.png' class='important' data-ischecked='false'></label></li>"
            + "<li class='position-relative' data-scale='3'><label class='inline'><img src='"+that.IMAGES_PATH + "/resources/patient-forms-resources/css/images/unchecked.png' class='important' data-ischecked='false'></label></li>"
            + "<li class='position-relative' data-scale='4'><label class='inline'><img src='"+that.IMAGES_PATH + "/resources/patient-forms-resources/css/images/unchecked.png' class='important' data-ischecked='false'></label></li>"
            + "<li class='position-relative' data-scale='5'><label class='inline'><img src='"+that.IMAGES_PATH + "/resources/patient-forms-resources/css/images/unchecked.png' class='important' data-ischecked='false'></label></li>"
            + "</ul>"+
                "<ul class='button-group teal-bordered'>" +
                "<li><a href='#' data-formdata='"+JSON.stringify(goal.formDataList)+"' class='button tiny margin-bottom-0 white-bordered-btn remove-width'>Goal Details</a></li>" +
                "<li><a href='goalsHabitsDashboard.action?patientId="+ that.PATIENT_ID +"' class='button tiny margin-bottom-0 white-bordered-btn remove-width'>Coach Assessment</a></li></ul>" +
                goalRatingsBtnHTML +
            "</div>" +
            "</div>";
    },

    buildHabitItem: function(habit){
        var that = this;

        return "<div class='goals-item'> "
            + "<div class='category-outer habit-color'>"
            +	"<div class='category-name'>Habit</div>"
            + "</div>"
            + "<div class='goal-details'> "
            + "<div class='row'> "
            + "<div class='grey-black-pair'> "
            + "<div class='columns small-5'><label class='inline grey align-right grey-forced'>After I</label></div>"
            + "<div class='columns small-7 padding-left-0'><label class='inline margin-bottom-0 margin-top-5 black'>"+(($.trim(habit.triggeringActivity)) ? habit.triggeringActivity : "N/A") +"</label></div>"
            + "</div> "
            + "</div> "
            + "<div class='row'> "
            + "<div class='grey-black-pair'> "
            + "<div class='columns small-5'><label class='inline grey align-right grey-forced'>I Will</label></div> "
            + "<div class='columns small-7 padding-left-0'><label class='inline margin-bottom-0 margin-top-5 black'>"+(($.trim(habit.healthierHabit)) ? habit.healthierHabit : "N/A") +"</label></div> "
            + "</div> "
            + "</div> "
            + "<div class='row'> "
            + "<div class='grey-black-pair'> "
            + "<div class='columns small-5'><label class='inline grey align-right grey-forced celebrate-label'>And Celebrate</label></div> "
            + "<div class='columns small-7 padding-left-0'><label class='inline margin-bottom-0 margin-top-5 black'>"+(($.trim(habit.celebrate)) ? habit.celebrate : "N/A") +"</label></div> "
            + "</div> "
            + "</div> "
            + "</div> ";
    },

    loadGoalRatingHistory: function(filledFormId){
        var that = this,
            $ul = $("#dialogGoalRatingList").empty(),
            html = "",
            goalRatingsFromList = that.GOALS_RATING_BUNDLE[filledFormId];

        if(goalRatingsFromList){

            $.each(goalRatingsFromList, function(index, goalRating){

                html += "<li><label class='inline margin-bottom-0'><span class='font-bold'>Rated On: </span>"+ goalRating.ratedOn + "</label><div class='grey-section-data'> <ul class='large-block-grid-5 small-block-grid-5 scales goal-progress-rating' data-goalrating='"+goalRating.rating+"'>"
                + "<li class='position-relative'><label class='inline'><img src='"+that.IMAGES_PATH+"/resources/patient-forms-resources/css/images/unchecked.png' /><span>1</span></label></li>"
                + "<li class='position-relative'><label class='inline'><img src='"+that.IMAGES_PATH+"/resources/patient-forms-resources/css/images/unchecked.png' /><span>2</span></label></li>"
                + "<li class='position-relative'><label class='inline'><img src='"+that.IMAGES_PATH+"/resources/patient-forms-resources/css/images/unchecked.png' /><span>3</span></label></li>"
                + "<li class='position-relative'><label class='inline'><img src='"+that.IMAGES_PATH+"/resources/patient-forms-resources/css/images/unchecked.png' /><span>4</span></label></li>"
                + "<li class='position-relative'><label class='inline'><img src='"+that.IMAGES_PATH+"/resources/patient-forms-resources/css/images/unchecked.png' /><span>5</span></label></li>"
                + "</ul></div></li>";
            });

            $ul.append(html);

            var $scales = $("[data-goalrating]");
            that.fillRating($scales, "goalrating");

        }else{
            $ul.append("<li class='remove-border'><p class='align-center'>No goal ratings</p></li>");
        }

        $("#goalRatingDialog").foundation("reveal", "open");
    },

    fillRating: function ($scales, key){
        var that = this;

        $scales.each(function(index, scale){
            var rating = $(scale).data(key),
                $children = $(scale).children("li");

            $children.each(function(index, child){

                if(index < rating){
                    $(child).find("img").attr("src", that.IMAGES_PATH + "/resources/patient-forms-resources/css/images/checked.png");
                }
            });
        });
    },

    renderHabits: function($container){
        var that = this,
            $ul = $container.empty(),
            html = "",
            dataArray = [];

        if(!$.isEmptyObject(HABITS_LISTING)){
            dataArray = HABITS_LISTING;
        }

        that.habitsDataArray = dataArray;

        if(dataArray && dataArray.length > 0){

            $.each(dataArray, function(index, habit){

                html += "<li>" + that.buildHabitItem(habit) + "</li>";
            });

            $ul.append(html);

        }else{
            $ul.append("<li><div class='align-center padding-10'>No habits found</div></li>");
        }
    },

    fillRating: function($scales, key){

        var that = this;

        $scales.each(function(index, scale){
            var rating = $(scale).data(key),
                $children = $(scale).children("li");

            $children.each(function(index, child){

                if(index < rating){
                    $(child).find("img").attr("src", that.IMAGES_PATH + "/resources/patient-forms-resources/css/images/checked.png");
                }
            });
        });
    },

    bindGoalDetailsDialog: function(){
        var that = this,
            $a = $("a[data-formdata]");

        $a.off("click").on("click", function(e){

            e.preventDefault();

            var formDataString = $(this).data("formdata");

            $.each(formDataString, function(index, formData){
                var $control = $("#"+ formData.fieldName);
                if($control){
                    $control.text(formData.fieldValue);
                }

                if(formData.fieldName == "important"){
                    that.checkUncheckScale($("#importantScale img"), formData.fieldValue);
                }

                if(formData.fieldName == "confident"){
                    that.checkUncheckScale($("#actionPlanScale img"), formData.fieldValue);
                }
            });

            $("#goalDetailsDialog").foundation("reveal", "open");
        });
    },

    checkUncheckScale: function($items, scale){

        var that = this;

        $items.attr({"src": that.IMAGES_PATH + "/resources/patient-forms-resources/css/images/unchecked.png", "data-ischecked": false});

        $.each($items, function(index, img){
            if(index < scale){
                $(img).attr({"src": that.IMAGES_PATH + "/resources/patient-forms-resources/css/images/checked.png", "data-ischecked": true});
            }
        });
    },

    getCurriculumProgress: function(uuid){
        var that = this;

        window.patientDashboardObject = that;

        var actionUrl = CURRICULUM_API_ADDRESS + "users/" + uuid + "/summary/"
            , postRequestActions = {
                "requestType" : "GET",
                "successCallBack" : that.onGettingCurriculumProgress,
                "loading": $("#loadingCurriculumTopics")
            };

        that.AJAXING.sendRequest(actionUrl, null, postRequestActions);
    },

    onGettingCurriculumProgress: function(data){
        var that = window.patientDashboardObject,
            $progressSummaryWeek = $("#progressSummaryWeek"),
            $progressSummaryText = $("#progressSummaryText");

        if(data){

            var startDate = new Date(data.start),
                lastActivity = new Date(data.lastActivity),
                weekNumber = data.week;

            if(lastActivity && weekNumber > 0){
                $("#lblWeeksAgo").text(that.calculateDifferenceInWeeks(new Date(), lastActivity));
            }

            if(startDate && weekNumber > 0){
                $("#lblWeekStartedOn").text(startDate.toString("MMMM d"));
            }

            $("#curriculumLink").prop("href", "topicsOverview.action?patientId="+that.PATIENT_ID+"&uuid="+that.UUID+"&week="+weekNumber);
            $progressSummaryWeek.text(weekNumber);
            $progressSummaryText.text(data.progress);
            that.saveCurriculum(data);
        }
    },

    calculateDifferenceInWeeks: function(d1, d2){

        var dif = Math.round(d1 - d2);
        return Math.round(dif/1000/60/60/24/7);
    },

    saveCurriculum: function (data){

        var that = this;

        window.patientDashboardObject = that;

        var actionUrl = "saveCurriculum.action"
            , dataToSend = {
                "patientId": that.PATIENT_ID,
                "curriculumStringAJAX": JSON.stringify(data)
            };

        that.AJAXING.sendPostRequest(actionUrl, dataToSend, null);
    },

    getSummaryData: function (){

        var that = this;

        window.patientDashboardObject = that;

        var actionUrl = "fetchWeeklyReportData.action"
            , dataToSend = {
                "patientId": that.PATIENT_ID,
                "weeklyReportDateString": $("#txtDateString").val()
            }
            , postRequestActions = {
                "requestType" : "GET",
                "successCallBack" : that.drawCharts,
                "loading": $(".only-chart-loading")
            };

        that.AJAXING.sendPostRequest(actionUrl, dataToSend, postRequestActions);
    },

    drawCharts: function (data){
        var that = window.patientDashboardObject,
            /*mealSummaryData = data.Meal,*/
            glucoseSummaryData = data.Glucose,
            medicationSummaryData = data.Medication,
            $mealsChartContainer = $("#mealChartContainer"),
            $glucoseChartContainer = $("#glucoseChartContainer"),
            $medsChartContainer = $("#medsChartContainer"),
            $spMealsAverage = $("#spMealsAverage"),
            $spMealsPercentage = $("#spMealsPercentage"),
            $lblWeeklyPercentage = $('#lblWeeklyPercentage'),
            mealAverage = (data.Meal_AVG[1])? Math.round(data.Meal_AVG[1].toFixed(1)): 0;

        if(data.Target) {
            var total = data.Target[0] + data.Target[1] + data.Target[2] +data.Target[3];
            if(total > 0) {
                var mealPercentage = (data.Meal_Percentage[2])? Math.ceil(data.Meal_Percentage[2]): 0;
                $spMealsPercentage.text(mealPercentage+'%');
                $lblWeeklyPercentage.text('meals below carb ceiling');
            } else {
                $spMealsPercentage.text('');
                $lblWeeklyPercentage.removeClass('logbook-summary-text').addClass('meal-summary-text font-24').text('No ceilings set');
            }
        }

        $spMealsAverage.text(mealAverage);

        //Glucose summary chart
        if(glucoseSummaryData){

            $("#avgGlucose").html(glucoseSummaryData[0].toFixed(0) + " mg/dl");
            $("#stdGlucose").html(glucoseSummaryData[0].toFixed(0) + " mg/dl");
            $("#glucoseChartNoData").remove();

            $(".total-glucose-tests").show();
            $("#lblGlucoseTotal").html(glucoseSummaryData[4]);

            Highcharts.setOptions({
                colors: ['#ECB648', '#ff6666', '#5EA647']
            });

            $glucoseChartContainer.highcharts({
                exporting: { enabled: false },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: true
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.0f}%</b> of Total'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        /*size:'70%',*/
                        dataLabels: {
                            enabled: true,
                            format: '<span class="glucose-label">{point.name}</span>:{point.percentage:.0f}%',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: '',
                    data: [
                        ['Under <br/> Target', glucoseSummaryData[1]],
                        ['Over <br/> Target', glucoseSummaryData[3]],
                        ['In <br/> Target', glucoseSummaryData[2]]
                    ]
                }]
            });

            $("text:contains('Highcharts.com')").hide();
        }

        if(medicationSummaryData && medicationSummaryData.length > 0){
            $("#medsChartNoData").remove();
            var medications = data.Medication;
            if(medications.length == 0){
                $medsChartContainer.html("<span class='centered-msg'>No Log Found</span>");
            } else {
                var medsArray = [];
                $.each(medications, function(index, med){
                    var valArray = [];
                    valArray.push(med.timestamp);
                    valArray.push(med.medsTaken);
                    medsArray.push(valArray);
                });

                $medsChartContainer.highcharts({
                    xAxis : {
                        tickInterval: 24 * 3600 * 1000,
                        min : data.MINMAX[0],
                        max : data.MINMAX[1],
                        type : 'datetime',
                        dateTimeLabelFormats : { // don't display the dummy year
                            month : '%e %b',
                            year : '%b'
                        },
                        title : {
                            text : '',
                            align : 'high'
                        }
                    },
                    yAxis: {
                        min:0,
                        tickInterval: 1,
                        title: {
                            text: ' '
                        }
                    },
                    exporting : {
                        enabled : false
                    },
                    chart : {
                        plotBackgroundColor : null,
                        plotBorderWidth : null,
                        plotShadow : true
                    },
                    title : {
                        text : ''
                    },
                    credits : {
                        enabled : false
                    },
                    tooltip: {
                        headerFormat: '<b>{series.name}</b><br>',
                        formatter: function () {
                            var fromMeds = medsArray[this.point.index];
                            return '<b>' + this.series.name + '</b><br/>' + fromMeds[1] + " med(s) taken";
                        }
                    },
                    series : [ {
                        data : medsArray,
                        name : 'Took Meds',
                        color : '#5EA647',
                        dataLabels : {
                            enabled : true,
                            style : {
                                fontSize: '8px',
                                format : '{point.y:0f}',
                                color : '#5EA647'
                            }
                        }
                    } ]
                });
            }
        }
    },

    getGuageOptions: function (greyOrGreen){

        var gaugeOptions = {

            exporting: { enabled: false }  ,

            chart: {
                type: 'solidgauge'
            },

            title: null,

            pane: {
                center: ['50%', '85%'],
                size: '160%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || greyOrGreen.first,
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },

            tooltip: {
                enabled: false
            },

            // the value axis
            yAxis: {
                stops: [
                    [0.1, greyOrGreen.second], // green
                ],
                lineWidth: 0,
                minorTickInterval: null,
                tickPixelInterval: 400,
                tickWidth: 0,
                title: {
                    y: -70
                },
                labels: {
                    y: 16
                }
            },

            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        borderWidth: 0,
                        useHTML: true
                    }
                }
            }
        };

        return gaugeOptions;
    },

    hidePatientId: function(){
        var that = this,
            $patId = $("#divPatID"),
            id = $patId.data("id").toString(),
            hideID;

        hideID = "&#149;&#149;&#149;"+ id.slice(-1);
        $patId.html("Member: "+ $("#spDisplayName").text()).show();
        return hideID;
    },

    getMisfitAverageStepsAndBatteryStatus: function(){
        var that = this,
            dataToSend = { "patientId": that.PATIENT_ID },
            postRequestActions = {
                "successCallBack": that.onGettingMisfitAverageStepsAndBatteryStatus,
                "loading": $("#loadingMisfitActivity")
            };

        that.AJAXING.sendPostRequest("getMisfitAverageStepsAndBatteryStatus.action", dataToSend, postRequestActions);
    },

    onGettingMisfitAverageStepsAndBatteryStatus: function(data){
        var misfit = $.parseJSON(data.DATA);
        if(misfit){
            $("#divAvgSteps").text(misfit.AVG_STEPS);
            if(misfit.MISFIT_BATTERY_STATUS && misfit.MISFIT_BATTERY_STATUS != -1){
                $("#divMisfitBatteryStatus").show().find("span.warning").text(misfit.MISFIT_BATTERY_STATUS+"%");
            }
        }
    },

    getLogEntriesCountAndCurrentWeight: function(){
        var that = this,
            dataToSend = { "patientId": that.PATIENT_ID },
            postRequestActions = {
                "successCallBack": that.onGettingLogEntriesCountAndCurrentWeight,
                "loading": $(".log-weight-loading")
            };

        that.AJAXING.sendPostRequest("getLogEntriesCountAndCurrentWeight.action", dataToSend, postRequestActions);
    },

    onGettingLogEntriesCountAndCurrentWeight: function(data){
        var logWeight = $.parseJSON(data.DATA);
        if(logWeight){
            $("#spLogEntriesDate").text(logWeight.DATE_SPAN);
            $("#spLogEntriesCount").text(logWeight.LOG_ENTRIES_COUNT);
            $("#spCurrentWeight").text(logWeight.CURRENT_WEIGHT);
        }
    }
};
