/**
 * Created by omursaleen on 2/19/2015.
 */

if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//==========================================
/* HSSessionExpire JS Model*/
var HSSessionExpire = {

    init: function(){
        var that = this,
            currentPageURL = window.location.pathname,
            actionName = currentPageURL.substr(currentPageURL.lastIndexOf("/")+1, currentPageURL.length),
            SKIP_PAGES = ["goalsHabitsListing.action", "createGoal.action", "createHabit.action", "goalProgress.action"];

        that.TICK_DURATION = 1000; //1 second
        that.MESSAGE_SHOWN_TIME = 60000; //when 60 seconds left

        that.$sessionExpiredOverlay = $(".session-expired-overlay").hide();
        that.$divSessionExpired = $("#divSessionExpired").show();
        that.$expiredCounter = $("#sessionTimeoutCountdown");
        that.$expiredDialog = $(".session-expired-dialog").hide();

        that.SESSION_TIMER = null;

        that.SESSION_PREFS = {
            //Logout Settings
            inactiveTimeout: 2400000, //40 minutes,     //(ms) The time until we display a warning message
            warningTimeout: 60000,      //(ms) The time until we log them out
            minWarning: 5000,           //(ms) If they come back to page (on mobile), The minumum amount, before we just log them out
            warningStart: null,         //Date time the warning was started
            warningTimer: null,         //Timer running every second to countdown to logout
            logout: function () {
                window.location = "logout"; //Logout function once warningTimeout has expired
            },

            //Keepalive Settings
            keepaliveTimer: null,
            keepaliveUrl: "",
            keepaliveInterval: 5000,     //(ms) the interval to call said url
            keepAlive: function () {
            }
        };

        that.SESSION_TIMEOUT_INTERVAL = that.SESSION_PREFS.inactiveTimeout;
        that.MESSAGE_SHOWN_TIME = that.SESSION_PREFS.warningTimeout;

        //skips session expiry on following pages
        if($.inArray(actionName, SKIP_PAGES) == -1){
            that.startTicking();
        }

        return that;
    },

    startTicking: function(){

        var that = this,
            $document = $(document),
            consoleLoggingTimer;

        that.$sessionExpiredOverlay = $(".session-expired-overlay").hide();
        that.$divSessionExpired = $("#divSessionExpired").show();
        that.$expiredCounter = $("#sessionTimeoutCountdown");
        that.$expiredDialog = $(".session-expired-dialog").hide();

        if(window.console){
            console.log("SESSION-TIMEOUT_STARTED");
        }

        clearInterval(consoleLoggingTimer);

        //console logging timer
        window.windowHSSessionObject = that;

        $('*').off('mousemove keydown scroll').on('mousemove keydown scroll', function () {
            clearTimeout(consoleLoggingTimer);
            that.SESSION_TIMEOUT_INTERVAL = that.SESSION_PREFS.inactiveTimeout;
            consoleLoggingTimer = setInterval(that.sessionLogging, that.MESSAGE_SHOWN_TIME);
        });

        $("body").trigger("mousemove");

        $document.on("idle.idleTimer", function (event, elem, obj) {

            console.log("Idle @ " + new Date().toLocaleString());

            //Get time when user was last active
            var diff = (+new Date()) - obj.lastActive - obj.timeout,
                warning = (+new Date()) - diff;

            //On mobile js is paused, so see if this was triggered while we were sleeping
            if (diff >= that.SESSION_PREFS.warningTimeout || warning <= that.SESSION_PREFS.minWarning) {
                that.SESSION_PREFS.logout();
            }else{
                //Show dialog, and note the time
                that.$sessionExpiredOverlay.slideDown();
                that.$expiredCounter.html(Math.round((that.SESSION_PREFS.warningTimeout - diff) / 1000));
                that.SESSION_PREFS.warningStart = (+new Date()) - diff;

                //Update counter downer every second
                that.SESSION_PREFS.warningTimer = setInterval(function () {
                    var remaining = Math.round((that.SESSION_PREFS.warningTimeout / 1000) - (((+new Date()) - that.SESSION_PREFS.warningStart) / 1000));
                    if (remaining >= 0) {
                        that.$expiredCounter.html(remaining);
                    } else {
                        that.SESSION_PREFS.logout();
                    }
                }, 1000);
            }
        }).on("active.idleTimer", function () {
            console.log("Active @ " + new Date().toLocaleString());
        });

        $document.idleTimer(that.SESSION_PREFS.inactiveTimeout);
    },

    restart: function(){
        return this.init();
    },

    refreshSession: function(){
        Ajaxing.sendPostRequest("refreshSession", null, null);
        window.location.reload(true);
    },

    millisToMinutesAndSeconds: function (millis) {
        var minutes = Math.floor(millis / 60000);
        var seconds = ((millis % 60000) / 1000).toFixed(0);
        return minutes + " minutes";
    },

    sessionLogging: function(){ // calculate time difference every 1 second

        var that = window.windowHSSessionObject,
            timeLeft = that.SESSION_TIMEOUT_INTERVAL - that.MESSAGE_SHOWN_TIME;

        that.SESSION_TIMEOUT_INTERVAL = timeLeft;

        console.log("SESSION TIMEOUT LEFT", that.millisToMinutesAndSeconds(timeLeft));
    }
};