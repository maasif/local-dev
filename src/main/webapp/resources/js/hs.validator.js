/**
 * Created by omursaleen on 2/3/2015.
 */

if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//==========================================
/* HSValidator JS Model*/
var HSValidator  = {

    init: function(){
        var that = this;

        that.VALIDATORS = {
            EMAIL: "email",
            ZIP: "zip",
            NUMBER: "number",
            PASSWORD: "password",
            PHONE: "phone",
            URL: "url"
        };

        return that;
    },

    validateFieldDataByType: function(fieldType, txtToValidate){
        var that = this,
            pattern = that.getRegexPattern(fieldType),
            pTest = pattern.test(txtToValidate);

        // in case of password check is there a single number in it or not.
        //if(pTest && fieldType === "password"){
        //    pTest = that.hasNumbers(txtToValidate);
        //}

        return pTest;
    },

    getRegexPattern: function(regexType){

        var that = this;

        switch (regexType) {
            case that.VALIDATORS.ZIP:
                return new RegExp(/^\d{5}(-\d{4})?$/);
            case that.VALIDATORS.EMAIL:
                return new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
            case that.VALIDATORS.NUMBER:
                return new RegExp(/^\d+$/);
            case that.VALIDATORS.PASSWORD:
                //(input must contain at least one digit/lowercase/uppercase/special letter and be at least eight characters long)
                return new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[':;,_.#?!@$%^&*-]).{8,}$/);
                //8 minimum letters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character
                ///^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8}$/

            case that.VALIDATORS.PHONE:
                return new RegExp(/^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/);
            case that.VALIDATORS.URL:
                return new RegExp("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?");
            default:
                break;
        }
    },

    hasNumbers: function(t){
        return /\d/.test(t);
    }
};