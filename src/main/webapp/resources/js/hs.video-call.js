/**
 * Created by omursaleen on 10/02/2015.
 */

if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//==========================================
/* HSVideoCall JS Model*/
var HSVideoCall = {

    init: function(config){
        var that = this;

        that.CONFIG = config;

        //alert message instance
        that.ALERT_MESSAGES = Object.create(AlertMessage).init();

        //ajaxing instance
        that.AJAXING = Object.create(Ajaxing);

        window.videoCallObject = that;
        that.cachePageConstants();
        that.getVideoCallTokenFromRestAPI();

        return that;
    },

    cachePageConstants: function(){
       var that = this;

       $("#buttonCall").on("click", function(e){
           e.preventDefault();
          that.createCall();
       });

        $("#buttonRefresh").on("click", function(e){
            e.preventDefault();
            that.refreshPatientStatus(true);
        });
    },

    getVideoCallTokenFromRestAPI: function(){
        var that = this,
            postRequestActions = {
                "successCallBack": that.onGettingToken,
                "loading": $("#connecting"),
                "ajaxType": "GET"
            };

        that.AJAXING.sendRequest(that.CONFIG.baseUrl+ "/services/sightcall/getToken?uid="+that.CONFIG.callerId, null, postRequestActions);
    },

    onGettingToken: function(data){
        var that = window.videoCallObject;
        if(data.token){
            that.CONFIG.TOKEN = data.token;
            $("#connecting").show();
            that.initSightCall(that.CONFIG.TOKEN);
        }else{
            that.ALERT_MESSAGES.showAlertMessage($("#terminatedMessage"), "Unable to get token from rest API", that.ALERT_MESSAGES.ERROR);
        }
    },

    initSightCall: function (token) {

        var that = window.videoCallObject,
            options = {
                displayName : that.CONFIG.coachType,
                container : 'videoContainer',
                mode_parameter : 'plugin_webrtc',
                uiVersion : '1.4.5'
            };

        // Initialize the Main Object with App Identifier, Token, RtccType, options
        var rtcc = new Rtcc(that.CONFIG.appId, token, 'internal', options);

        // Call if the RtccDriver is not running on the client computer and if the browser is not WebRTC-capable
       rtcc.on("plugin.missing", function (downloadUrl) {
            var answer = confirm('Click OK to download and install the Rtcc client.');
            if (answer === true) {
                window.location = downloadUrl;
            }
        });

        rtcc.on('cloud.sip.ok', function() {
            document.getElementById('connecting').innerHTML = "Connected.";
            document.getElementById("divStatusCallButton").style.display = "block";
            //cal it here to get user status
            rtcc.getStatus(that.CONFIG.calleeId);
        });

        //This functions gets the current status of the callee we are calling
        rtcc.onGetHandler = function(name, obj){
            switch (name){
                case 'status':
                    var uid = obj.uid;

                    document.getElementById("refreshStatusLabel").style.display = "none";

                    if(obj.value == 0){
                        document.getElementById('call').className = "lbl-status offline";
                        document.getElementById('call').innerHTML = "offline";
                        if(VALIDATE_CALL){
                            alert("Member is offline, unable to call.");
                        }
                    }else{
                        document.getElementById('call').className = "lbl-status online";
                        document.getElementById('call').innerHTML = "online";
                        if(VALIDATE_CALL){
                            rtcc.createCall(that.CONFIG.calleeId, 'internal', 'Member');
                        }
                    }
                    break;
            }
        };

        //what to do when we are connected when we are connected with another user id
        //in plugin or driver mode
        rtcc.on('cloud.loggedasotheruser', function() {
            getToken(that.CONFIG.callerId, function (token){
                /*rtcc.setToken(token);
                rtcc.authenticate(1);*/
                rtcc.forceAuthenticate();
            });
        });

        //what do to when we are disconnected from the client: we reconnect
        rtcc.on('cloud.authenticate.error', function(number) {
            if (number === 15 || number === 29) {
                rtcc.setToken(that.CONFIG.TOKEN);
                rtcc.authenticate(1);
            }
        });

        var defineCallListeners = function(call) {
            VALIDATE_CALL = false;

            call.onAll(function() {
                if (window.console) {
                    console.log('Call: event "' + this.eventName + '"" with arguments: ' + JSON.stringify(arguments));
                }
            });

            call.on('create', function() {
                document.getElementById('connecting').innerHTML = 'Calling...';
            });

            call.on('active', function() {
                document.getElementById('connecting').innerHTML = 'Call active';
            });

            // for webrtc screen share
            call.on('chrome.screenshare.missing', function(url) {
                window.open(url);
            });

            call.on('terminate', function(reason) {
                document.getElementById('connecting').innerHTML = 'Call terminated';
                that.ALERT_MESSAGES.showAlertMessage($("#terminatedMessage"), "Call terminated", that.ALERT_MESSAGES.INFO);
            });
        };

        rtcc.initialize();

        //when a call has started
        rtcc.on('call.create', defineCallListeners);

        that.rtcc = rtcc;
    },

    createCall: function () {
        var that = window.videoCallObject;
        VALIDATE_CALL = true;
        that.rtcc.getStatus(that.CONFIG.calleeId);
    },

    refreshPatientStatus: function(showLabel){
        var that = window.videoCallObject;
        if(showLabel){
            document.getElementById("refreshStatusLabel").style.display = "inline";
        }
        VALIDATE_CALL = false;
        that.rtcc.getStatus(that.CONFIG.calleeId);
    }
};