if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

var LogBook  = {
				
	init: function(){
		var that = this;	
		that.cachePageControls();
		that.initDatepicker();
		that.bindNextPrevious();
		that.disableEnableButtons();
		return that;
	},
	
	cachePageControls: function(){
		var that = this;		
		that.$form = $("#formLogs");
		that.$btnNext = $("#btnNext");
		that.$btnPrev = $("#btnPrev");
		that.$dateLabel = $(".chooseDateText");		
		that.$navButtons = $(".nav-buttons");
		that.$selectedDate = $("#selectedDate");						
		that.dateFormatLess = "M/d/yyyy";		
		that.dateFormatDetailed = "dddd | MMM d, yyyy";
	},
	
	initDatepicker: function(){
		
		var that = this;
				
		that.$txtDate = $("#txtDate").pickadate({
			dateMin: [ 1970, 1, 1 ],
			clear: false,				
		    onClose: function(context) {		    			    	
				var dateString = that.picker.get("select", "mm/dd/yyyy");		    	
		    	that.$selectedDate.val(that.formatDate(dateString, that.dateFormatLess));		    	
		    	that.$form.submit();					    	
		    }
		});
		
		that.picker = that.$txtDate.pickadate("picker").set('max', new Date());
		
		that.$dateLabel.on("click", function(e){
			that.picker.open();
			return false;
		});
		
		if(Date.today()){
			that.$dateLabel.text("Today");			
		}
		
		return that;
	},
	
	bindNextPrevious: function(){
		
		var that = this;
		
		that.$navButtons.on("click", function(e){
			e.preventDefault();
			that.setDateAndSubmitForm($(this).data("nav"));
		});
		
		return that;
	},
	
	setDateAndSubmitForm: function(navType){
		
		var that = this,
			dateString = (that.$dateLabel.text() === "Today") ? Date.today() : that.$dateLabel.text();
			nowDate = new Date(dateString),					
			dateSubAdd = (navType == "next") ? 1 : -1;					
			
		nowDate.setDate(nowDate.getDate()+dateSubAdd);		
		
		var dateSelected = nowDate.toString(that.dateFormat);
		
		that.$selectedDate.val(that.formatDate(dateSelected, that.dateFormatLess));			
		that.setDate(that.formatDate(dateSelected, that.dateFormatDetailed));
						
		that.$form.submit();	
		
		return that;
	},
	
	setDate: function(dateString){
		var that = this;		
		that.$dateLabel.text(dateString);
		that.disableEnableButtons(dateString);
		return that;
	},
	
	formatDate : function(dateString, byFormat){			
		return new Date(dateString).toString(byFormat);
	},
	
	disableEnableButtons: function(dateString){
		var that = this;
		
		if(dateString){
			var dateServer = that.formatDate(dateString, that.dateFormatLess),
				dateToday = that.formatDate(Date.today().toString(), that.dateFormatLess);			
			if(dateServer === dateToday){
				that.setDate("Today");
			}			
		}
		
		that.$btnNext.removeClass("disabled").on("click", function(){
			that.setDateAndSubmitForm("next");
		});
		
		that.$btnPrev.removeClass("disabled").on("click", function(){
			that.setDateAndSubmitForm("prev");
		});
		
		if(that.$dateLabel.text() === "Today"){
			that.$btnNext.addClass("disabled").off("click");
		}
		
		if(that.$dateLabel.text().indexOf("1970") > -1){
			that.$btnPrev.addClass("disabled").off("click");
		}
		
		return that;
	}
};