var files = "",
	deleteKey = "",
	isWebpageToUpdate = false,
	jsonArticlesDiagrams = [],
	jsonWebpages = [],	
	SUCCESS = "success",
	ERROR = "error",
	FILE_EXTENSIONS = ["png", "jpg", "gif", "bmp", "log", "txt", "pdf", "doc", "docx"],
	FILE_EXTENSIONS_WITH_TYPE = [
			{"type" : "Document", "extension" : "doc, docx, txt, log"},
			{"type" : "Image", "extension" : "png, jpg, gif, bmp"},
			{"type" : "PDF", "extension" : "pdf"},
	],
	FILE_MAX_LENGTH = 5000000,
	selectedItem = 0,
	categoryIndex = -4,
	oldFileKey = "",
	readableJson = {
		"size" : "File Size: ",
		"lastModified" : "Upload Date: ",
		"owner" : "Owner: ",
		"key" : "Type: ",
		"url": "URL: "
	},
	$selectedGridItem = undefined,
	expandedAccordion = 0,
	accordionItem = -4,
	loggedUser = "",
	MAX_SPACE = 300,
	CURRENT_SPACE = 0,
	BUCKET_NAME = "",
	GOOGLE_DOCS_URL = "https://docs.google.com/gview?url=",
	FOLDER_ARTICLES = "Articles & Handouts",
	FOLDER_DIAGRAMS = "Diagrams",
	FOLDER_WEBPAGES = "Web Pages",
	AJAX_ERROR_MESSAGE = "Our server appears to be down. Please try again in a few minutes. If the problem persists please contact Customer Service at <a href='#' style='color:yellow'>info@healthslate.com</a> or 813-403-5400 extension 1.",
	FACILITY_SPACE = 0,
	UPLOAD_TEXT = "We're uploading your content!!!",
	DELETE_TEXT = "We're deleting your content!!!",
	RENAME_TEXT_FILE = "We're renaming your content!!!",
	SAVE_TEXT_WEB_PAGE = "We're saving web page!!!",
	EDIT_TEXT_WEB_PAGE = "We're updating web page!!!",
	ADD_TO_QUEUE_TEXT = "We're adding your content to queue!!!",
	filesArray = [];
		
function getFileTypeByExtension(fileName){
	var fileType = "webpages",
		ext = extractExtensionFromFile(fileName).toLowerCase();
			
	$.each(FILE_EXTENSIONS_WITH_TYPE, function(index, obj){
		 
		if(obj !== null && obj !== undefined){
			if(obj["extension"].indexOf(ext) > -1){
				fileType = obj["type"];
				return false;
			}
		}
	});

	return fileType;
}

function getVisibleItems(){

	return $("#itemGrid li:visible:gt(0)");
}

function sortByDate(a, b){		
  return new Date(a.lastModified) - new Date(b.lastModified);
}

function sortByName(a, b){

  var aName = extractFolderAndFileName(a.key, false).toLowerCase();
  var bName = extractFolderAndFileName(b.key, false).toLowerCase(); 

  return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}	

function scrollAndHighlight($element){				
	var $container = $('.content-manager');						
	var eTop = $element.offset().top - $container.offset().top + $container.scrollTop();
	
	$element.css({"background-color":"#dff0d8", "color":"#3c763d"});							
	
	if(eTop > $container.height()){
		$container.animate({
	        scrollTop: eTop
	    });	    		
	}
	
	setTimeout(function(){
		$element.removeAttr("style");
	}, 2000);	
}

function extractExtensionFromFile(file){	
	var extension = "";
	if(file !== undefined){
		var lastDot = file.lastIndexOf(".") + 1;
		extension = file.substring(lastDot, file.length);
	}	
	return extension;
}

function getFullCategoryFromFile(fileName, isSingular){
	
	var categoryType = "";
	
	if(fileName.indexOf(FOLDER_DIAGRAMS + "/") > -1){
		categoryType = FOLDER_DIAGRAMS;		
		if(isSingular){
			categoryType = "Diagram";
		}
	}else if(fileName.indexOf(FOLDER_ARTICLES +"/") > -1){
		categoryType = FOLDER_ARTICLES;
		if(isSingular){
			categoryType = "Article/Handout";
		}
	}else if(fileName.indexOf(FOLDER_WEBPAGES +"/") > -1){
		categoryType = FOLDER_WEBPAGES;
		if(isSingular){
			categoryType = "Web page";
		}
	}
	
	return categoryType;
}

function isValidFile(file){
	
	var fileName = file.name;
	
	var returnObject = {
		isExists: false,
		fileObject : undefined,
		message: ""
	};
	
	if(file !== undefined){
		var fileExtension = extractExtensionFromFile(fileName).toLowerCase(),			
			extension = FILE_EXTENSIONS.indexOf(fileExtension);			
		
		if(extension < 0){
			returnObject.isExists = true;				
			returnObject.message = "File extension (." + fileExtension + ") not supported for uploading";
			return returnObject;
		}
					
		if(file.size > FILE_MAX_LENGTH){
			returnObject.isExists = true;				
			returnObject.message = "File size should be less than 5MB";			
			return returnObject;
		}	
		
		$.each(jsonArticlesDiagrams, function(index, obj){				
			var _fileName = obj.key,
				fileCategory = getFullCategoryFromFile(_fileName, false),												
				selectedCategory = $("#categoryGroup a:not(.secondary)").data("tag");
			
			var isOfSameCategory = (fileCategory === selectedCategory);
			var isSameFileName = _fileName.search(new RegExp(fileName, "i")) > 0;
			
			if(isSameFileName && isOfSameCategory) {
				returnObject.isExists = true;
				returnObject.fileObject = $("div[data-key='"+_fileName+"']").parents("li");
				returnObject.message = "File already exists";	
				return false;
			}				
		});
	}			
	
	return returnObject;
}

function showLoading(actionType){
	var $loadingSpinner = $('#loading'),
		$progressTxt = $("#progressTxt"),
		progressText = UPLOAD_TEXT;
	
	switch(actionType){
		case "DELETE":
			progressText = DELETE_TEXT;
			break;
		case "RENAME":
			progressText = RENAME_TEXT_FILE;
			break;
		case "SAVE_WEB_PAGE":
			progressText = SAVE_TEXT_WEB_PAGE;
			break;
		case "EDIT_WEB_PAGE":
			progressText = EDIT_TEXT_WEB_PAGE;
			break;
		case "ADD_TO_QUEUE":
			progressText = ADD_TO_QUEUE_TEXT;
			break;
		default:				
			break;
	}
	
	$progressTxt.text(progressText);
	
	return $loadingSpinner.show();
}

function sendAjaxRequestWithData(_url, _data, successCallBack, actionType){		
	
  var $loadingSpinner = showLoading(actionType);
  
   $.ajax({
		type : "GET",
		url : _url,
		data : _data,				
		contentType : "application/javascript",				
		success : function(data) {											
			$loadingSpinner.hide();					
			
			try{
				data = JSON.parse(data);						
			}catch(err){
				console.log(err.message);
			}
			
			if(successCallBack !== undefined){				
				removeHighlightFromRow();
				successCallBack(data);
			}					
		},				
		error : function(data, status) {
			
			$loadingSpinner.hide();										
			showPermanentAlertMessage($("#message"), AJAX_ERROR_MESSAGE, ERROR);			
			console.log("ERROR");
			console.log(data);
		}
	});	    	  
}

function isDir(object) {
	if (object.key.substr(-1) === '/' && object.size == 0) {
		return true;
	}
	return false;
}

function showSearchedSideNav(){
	
	var $search = $("#sideNav li.search-files").show();
		
	$search.off("mouseover mouseout click").on("mouseover", function(){
		$(this).css("cursor", "default");
	});			
	
	$search.find("a").css("color", "white");
	
	var $accordionOpened = $("dd.active");
	if($accordionOpened.length > 0){
		$accordionOpened.children("a").click();
	}
}

function accordionItemHighlight($item){
	
	var $sideNavItems = $("#sideNav li.side-nav-item");
	
	$sideNavItems.removeAttr("style").filter(function(){
		$(this).find("a").css("color", "#2ba6cb");		
	});
		
	$sideNavItems.off("mouseover").on("mouseover", function(){
		$(this).css("background-color", "#4571DF");
		$(this).find("a").css("color", "#fff");
	}).off("mouseout").on("mouseout", function(){
		$(this).removeAttr("style");
		$(this).find("a").removeAttr("style");
	});
	
	$item.css("background-color", "#4571DF").find("a").css("color", "white");
	
	$item.off("mouseover mouseout");
}

function sideNavOnClickItem(){
	
	var $sideNavItems = $("#sideNav li.side-nav-item");
		
	$sideNavItems.off("click").on("click", function(){
		
		var liType = $(this).data("uploaded-type");
		
		$("#txtSearch").val("");
									
		accordionItem = $(this).data("cat-index");
		
		removeHighlightFromRow();
		
		accordionItemHighlight($(this));
		
		$("#sideNav").children("li.search-files").hide();
		
		var $span = $(this).find("span:last-child"),
			type = $span.data("type"),
			$emptyLi = $("li.empty-item").hide();
		
		if(type !== undefined){
			$("li.grid-row").hide();					
			var $itemsFound = $("li."+type+"."+liType);
			if($itemsFound.length > 0){
				$itemsFound.show();
			}else{
				$emptyLi.show();
			}				
		}
	});
}

function displaySideNavItemsCount(){

	var $sideNavItems = $("#sideNav li.side-nav-item"),
		$items = $sideNavItems.find("span:last-child");
		 
	$items.each(function(index, item){
		var type = $(this).data("type"),
			liType = $(this).parents(".side-nav-item").data("uploaded-type"),
			count = 0;
		
		var $lis = $("li."+type+"."+liType);
		
		if($lis.length > 0){
			count = $lis.length;
		}
		
	    $(this).children("span.file-count").text( "(" + count + ")");
	});

	sideNavOnClickItem();
}

function getAlertMessage($message, alertType){
	if(alertType == SUCCESS){
		$message.removeClass(ERROR).addClass(SUCCESS).show();
	}else{
		$message.removeClass(SUCCESS).addClass(ERROR).show();
	}

	return $message;
}

function showPermanentAlertMessage($message, errorMessage, alertType){		
	var $m = getAlertMessage($message, alertType);
	
	$m.html(errorMessage).append("<a href='#' class='close-alert'>&times;</a>");
	
	$("a.close-alert").off("click").on("click", function(e){
		e.preventDefault();
		$m.fadeOut(500);
	});
}

function showAlertMessage($message, errorMessage, alertType){
	
	var timer = null;
	
	clearTimeout(timer);
	
	var $m = getAlertMessage($message, alertType);
	$m.html(errorMessage);
	
	timer = setTimeout(function(){
		$m.fadeOut(500);
	}, 3000);	
}

function extractFolderAndFileName(strName, isFolder){
	
	var extracted = "";
	
	if(strName !== undefined){
		var lastIndex = strName.lastIndexOf("/")+1;
		
		extracted = strName.substring(lastIndex, strName.length);
		
		if(isFolder){				
			
			lastIndex = strName.lastIndexOf(".");
			extracted = strName.substring(0, lastIndex);
			
			//lastIndex = extracted.lastIndexOf("/")+1;
			//extracted = strName.substring(lastIndex, extracted.length);				
		}
	}
	
	return extracted;
}

function removeSideNavSelection(){    
    var $sideNavItems = $("#sideNav li.side-nav-item").removeClass("active selected-item").addClass("default-item").removeAttr("style");
    $sideNavItems.find("a").removeAttr("style").css("color","#1d5987");    
    $("#sideNav").find("li.search-files").hide();
}

function setSelectedTab(index){
    
	var $accordions = $("#sideNav > li:not(.search-files)"),	
		$accordion = $accordions.eq(index).find("dd > a").click(),	
		type = $accordion.data("uploaded-type"),
		$item = $accordion.parents("li").find("ul").children("li").eq(accordionItem);			
	
	//accordionItemHighlight($item);		
	//$item.click();
	
	$("li.grid-row").hide();	
	$("li."+type).show();
	
    $("#sideNav").find("li.search-files").hide();
}

function searchFilesInGrid(searchText)	{
	
	var filter = searchText, count = 0;
    var $empty = $("li.empty-item").hide();	 		        
    
    removeSideNavSelection();
    removeHighlightFromRow();
    
    $("#itemGrid li:gt(1)").each(function(){		 			
		var toSearch = $(this).find("div.file-name").text();
		
		showSearchedSideNav();
		
        if (toSearch.search(new RegExp(filter, "i")) < 0) {		                
            $(this).hide();		                			            
        } else {		            	
            $(this).show();	            
            count++;	   		                            
        }		  

        $empty.hide();
        if(count == 0){
        	$empty.show();
        }
    });				
}

function openUploadDialogInEditMode($webPage, fileKey){
	files = "";
	
	var $categoryGroup = $("#categoryGroup").hide();
	
	var $items = $categoryGroup.find("a").addClass("secondary");
	accordionItem = 2;
	$items.eq(accordionItem).click().removeClass("secondary");
	
	var webURL = $webPage.attr("title");
	var titleName = $webPage.text();
	
	oldFileKey = fileKey;
	$("#txtFileName").val(webURL);
	$("#txtTitleName").show().val(titleName);
	
	isWebpageToUpdate = true;
			
	$("#btnConfirmUpload").val("Update").text("Update");
	$("#uploadHeader").text("Edit web page URL");
	
	$("#emailCheckContainer").hide();
	
	$("#lblCategory").hide();
	$("#uploadBoxOpen").click();		
}

function removeWebpageUploadParameters(){
	isWebpageToUpdate = false;
	oldFileKey = "";
}

function bindSwitch($switch, manualSelect){		
	$switch.on("click", function (e){
		e.preventDefault();
		$switch.addClass("secondary").removeClass("category-selected");
		$(this).removeClass("secondary").addClass("category-selected");		
	});
	
	if(manualSelect){
		$switch.eq(0).click();
	}else{		
		if(accordionItem === 0 || accordionItem === 1){
			$switch.eq(accordionItem).click();
		}
	}
}

function bindCategories(){
	var $categoryGroup = $("#categoryGroup a");
	
	$categoryGroup.on("click", function (e){
		e.preventDefault();
		$categoryGroup.addClass("secondary").removeClass("category-selected");
		$(this).removeClass("secondary").addClass("category-selected");
		
		var $txtFileName = $("#txtFileName").attr({"readonly":"readonly", "placeholder":"Upload file name..."}),
			$btnBrowse = $("#btnBrowse").show(),
			$btnUpload = $("#btnConfirmUpload").val("Upload").text("Upload"),
			$header = $("#uploadHeader").html("Upload file <span class='small-light-text'>(Allowed file types: PDF, GIF, JPG, PNG, DOC, DOCX, TXT)</span>"),
			$txtTitle = $("#txtTitleName").hide(),
			$containerEmaiChk = $("#emailCheckContainer").show();
								
		accordionItem = $(this).parent("li").index();
		removeWebpageUploadParameters();
				
		var $txtFieldsDiv = $("#txtFieldsDiv").removeClass("medium-12 twelve").addClass("medium-10 ten");
		
		if($(this).data("tag") === FOLDER_WEBPAGES){
			files = "";
			$header.html("Enter web page URL");
			$txtFileName.val("").attr("placeholder","Enter web page URL...").removeAttr("readonly").focus();
			$txtTitle.val("").show();			
			$("#uploadFile").val("");			
			$btnBrowse.hide();
			$txtFieldsDiv.removeClass("medium-10 ten").addClass("medium-12 twelve");
			$btnUpload.val("Save").text("Save");			
			$containerEmaiChk.hide();
		}
	});
}

function bindSearching(){
	var $txtSearch = $("#txtSearch");	
	$txtSearch.on("keyup",function(e){
		var searchText = $(this).val();					
		searchFilesInGrid(searchText);		
    });	
}

function bindSorting(){
	$("*[data-sort]").off("click").on("click", function(){				
		var sorted = $(this).attr("data-sorted"),
			sortType = $(this).data("sort");

		if( sorted === "asc"){
			sorted = "desc";
			jsonArticlesDiagrams.reverse();			
		}else{			
			sorted = "asc";
			if(sortType === "name"){	
				jsonArticlesDiagrams.sort(sortByName);									
			}else{						
				jsonArticlesDiagrams.sort(sortByDate);				
			}					
		}

		$(this).attr("title","order-"+sorted).find("img")
			   .removeClass("asc desc").addClass(sorted)
			   .attr({"src":"../resources/css/images/sort_"+sorted+".png", "title":sorted});

		var combinedJson = {"data": jsonArticlesDiagrams, "facilitySpace" : FACILITY_SPACE};
		
		loadArticlesDiagramsWebpages(combinedJson, false);				
		$(this).attr("data-sorted",sorted);			
		return false;				
	});
}

function openInNewTab(url){

  var win = window.open(url, '_blank');
  if(win !== null){
	  win.focus();
  }
}

function getReadableNameByKey(key){
	return readableJson[key];   			
}

function removeWebExtension(fileName){	
	var webIndex = fileName.lastIndexOf(".");					
	fileName = fileName.substring(0, webIndex);
	
	return fileName;
}

function bindGridItemClick(){
	var $gridItems = $("#itemGrid li:gt(1)").addClass("grid-item-default");		
	$gridItems.off("click").on("click", onGridItemClick);
}

function onGridItemClick(){
	var $gridItems = $("#itemGrid li:gt(1)").addClass("grid-item-default").removeClass("grid-item-selected"),	
		$this = $(this).removeClass("grid-item-default").addClass("grid-item-selected"),		
		fileObject = $(this).data("file-object"),				
		_key = fileObject.key,					
		fileType = getFileTypeByExtension(_key),
		fileName = extractFolderAndFileName(_key, false),
		pos = $(this).offset(),
		$fileContainer = $("#fileDetails").empty(),
		$parent = $fileContainer.parents(".file-properties").hide(),
		$downloadBtn = $parent.find("a.download-button").show(),
		$viewBtn = $parent.find("a.view-btn").show();
	
	$("#sideNav").find("li.search-files").hide();
	
	bindNavButtonsOnClick($this, $parent, fileObject, fileType);
				
	if(fileType.indexOf("webpages") > -1){				
		fileName = removeWebExtension(fileName);						
	}
	$("#fileName").text(fileName);
	$("li#nav-file-name").text(fileName);	
}

function removeHighlightFromRow(){
	$(".rename-container").hide();
	$(".nav-tool-bar").hide();
	$("#itemGrid li:gt(1)").addClass("grid-item-default").removeClass("grid-item-selected");
}

function bindNavButtonsOnClick($gridItem, $parent, fileObject, fileType){
	var	$actionBtns = $("#navBarActions a:not(.view)"),
		_key = fileObject.key,
		fileName = extractFolderAndFileName(_key, false);
	
	$actionBtns.parents(".nav-tool-bar").show();
	
	$(".rename-container").hide();
		
	if($selectedGridItem !== undefined){	
   		$selectedGridItem.off("click").on("click", onGridItemClick);
   	}
	
	var url = GOOGLE_DOCS_URL + BUCKET_NAME + _key;		
	var $viewBtn = $("#nav-btn-view").find("a").off("click");
	
	if(fileType === "Image"){
		$viewBtn.on("click", function(e){
			e.preventDefault();
			$("#imageViewerOpen").click();
			var imgSrc = BUCKET_NAME + _key;
			var $img = sourceToImage(imgSrc);
			$("#imageTitle").text(fileName);
			$("#imgViewer").empty().append($img);
		});
	}else{		
		url = url.replace('&', '%26');
		if(fileType === "webpages"){
			url = fileObject.url;
		}
		$viewBtn.attr("href", encodeURI(url));		
	}
	
	setNavButtonsSecurity(fileObject);
		
	$actionBtns.off("click").on("click", function(e){
		e.preventDefault();		
		$parent.hide();
		var action = $(this).data("action");						
		
		switch (action) {
			case "download":															
				var $form = $("#formUpload").off("submit");				
				$form.attr("action","downloadFile1.action");
				$("#txtKey").val(_key);
				$form.submit();
			break;
			
			case "delete":							
				var $deleteHeader = $("#deleteFileHeader");							
				deleteKey = _key;		
				$deleteHeader.text("("+ extractFolderAndFileName(deleteKey, false) + ")");
				$selectedGridItem = $gridItem.off("click");
				if(fileType.indexOf("webpages") > -1){
					
					var fileName = extractFolderAndFileName(_key, false);			
					fileName = removeWebExtension(fileName);
					
					$deleteHeader.text("("+ fileName + ")");			
				}
						
				$("#dialogStatement").text("Are you sure you want to delete it?");
				$("#confirmBoxOpen").trigger("click");		
			break;
			
			case "edit":							
				$selectedGridItem = $gridItem.off("click");						
				var $file = $selectedGridItem.find("div.file-name").find("a");				
				openUploadDialogInEditMode($file, _key);
			break;
			
			case "rename":
				$selectedGridItem = $gridItem.off("click");
				$gridItem.find(".rename-container").show();				
			break;
			
			case "addtoqueue":							
				var data = { "key" : _key},
					url = "addContentToQueue.action";
				
				$selectedGridItem = $gridItem.off("click");
				sendAjaxRequestWithData(url, data, onSuccessQueueAdded, "ADD_TO_QUEUE");			
			break;
			
		default:
			break;
		}
				
		$gridItem.removeClass("grid-item-default").addClass("grid-item-selected");
		$actionBtns.parents(".nav-tool-bar").show();
	});
}

function onSuccessQueueAdded(dataResult){
	var $error = $("#message").hide();		
	if(dataResult.status === "success"){						
		showAlertMessage($error, "Item has been added to queue", SUCCESS);							
	}else{
		showAlertMessage($error, dataResult.data, ERROR);								
	}		
}

function setNavButtonsSecurity(fileObject){
	var _key = fileObject.key,			
		categoryType = getCategoryFromFile(_key), 
		fileType = getFileTypeByExtension(_key),	
		$btnDownload = $("#nav-btn-download").show(),
		$btnDel = $("#nav-btn-delete").hide(),
		$btnEdit = $("#nav-btn-edit").hide(),
		$btnRename = $("#nav-btn-rename").hide(),
		$btnView = $("#nav-btn-view").show(),
		$btnQueue = $("#nav-btn-queue").hide();
		
	if(categoryType === "webpages"){		
		$btnRename.hide();
		$btnQueue.hide();
		$btnDownload.hide();
	}
	
	if(fileObject.canEmail == "true" || fileObject.canEmail == true){
		$btnQueue.show();
	}
	
	if(loggedUser === fileObject.owner){
		$btnDel.show();	
		$btnRename.show();
		if(categoryType === "webpages"){
			$btnEdit.show();
			$btnRename.hide();
		}		
	}	
}

function bytesToMB(bytes){
	var sizeInMB = (bytes / (1024*1024));	
	return sizeInMB.toFixed(1);
}

function bytesToSize(bytes) {
   var k = 1000;
   var sizes = ['bytes', 'KB', 'MB', 'GB', 'TB'];
   
   if (bytes === 0) return undefined;
      
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(k)),10);
   return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
}

function removeHighlighted($accordionBtns, $accordionContent) {			     
	$accordionBtns.find("i").removeClass("glyphicon-chevron-up").addClass('glyphicon-chevron-down');				          
    $accordionContent.removeClass("active").children("div.content:visible").slideToggle("normal");	
}

function highlightSideNavAccordionItem(index){
	var $li = $("#sideNav > li:not(.search-files)"),
		$accordion = $li.find(".accordion"),
	    $accordionContent = $accordion.children("dd"),
		$accordionBtns = $accordionContent.children("a"),
		uploadedType = $accordionBtns.eq(index).data("uploaded-type");
			
	var $parent = $accordionBtns.eq(index).parent("dd");
	
	if(!$parent.hasClass("active")){
		$accordionBtns.eq(index).click();
	}
			
	filterGridItemsByType(uploadedType);
	
	var $items = $accordionBtns.eq(index).next("div.content").children("ul").children("li");	
	$items.eq(accordionItem).click();
}

function bindCategoriesAccordion(){
	var $li = $("#sideNav > li:not(.search-files)"),
		$accordion = $li.find(".accordion"),
	    $accordionContent = $accordion.children("dd"),
		$accordionBtns = $accordionContent.children("a"),
		$searched = $("li.search-files");
						
	$accordionContent.off("click").on("click", "a:eq(0)", function (event) {
        var $parent = $(this).parent(),
        	uploadedType = $(this).data("uploaded-type");
        
        removeSideNavSelection();
        removeHighlightFromRow();
        
        filterGridItemsByType(uploadedType);
        
        if($searched.is(":visible")){
        	$searched.hide();
        }
        
        expandedAccordion = $(this).parents("li").index();
        accordionItem = -4;
        
        if($parent.hasClass('active')){
	      
          $parent.removeClass("active");			          
          removeHighlighted($accordionBtns, $accordionContent);
	      
		}else{
		  
		  removeHighlighted($accordionBtns, $accordionContent);					  
		  $parent.addClass("active");					  
		  $parent.find(".content").slideToggle("normal");
		  
          $(this).find("i").removeClass("glyphicon-chevron-down").addClass('glyphicon-chevron-up');			          
	    }
        
        var $li = $("li.side-nav-item");  
        
        $li.find("a").removeAttr("style").filter(function(){
        	var style = $(this).attr("style");
        	if(style !== undefined){
        		$(this).find("a").css("color", "white");
        	}
        });                
    });
						
	$accordionBtns.eq(0).click();	
	filterGridItemsByType("me");
}

function filterGridItemsByType(type){
	$("li.grid-row").hide();					
	
	var $itemsFound = $("li."+type),
		$emptyLi = $("li.empty-item").hide();
	
	if($itemsFound.length > 0){
		$itemsFound.show();
	}else{
		$emptyLi.show();
	}
}

function getRenameDiv(key){
	
	var fileName = extractFolderAndFileName(key, false),
		$parent = $("<div/>", {"class":"rename-container"}),
		$input = $("<input/>", {"type":"text", "class":"rename-txt", "data-oldKey":key, "value": fileName.substring(0, fileName.lastIndexOf(".")) });	
				
	$parent.append($input);
		
	setInputSelection($input, 0, $input.val().lastIndexOf("."));
	
	return $parent.wrap("<p/>").parent().html();
}

function setInputSelection(input, startPos, endPos) {
	
	setTimeout(function(){
		input.focus();
		input.select();
	}, 500);
	
    //input.setSelectionRange(startPos, endPos);
}

function renameAFile($this){
	var oldKey = $this.data("oldkey"),
		newKey = $this.val(),			
		fileName = extractFolderAndFileName(oldKey, false),
		ext = extractExtensionFromFile(fileName).toLowerCase(),				
		data = { "key" : oldKey, "newKey": newKey+"."+ext},
		url = "renameFile.action";			
	
	sendAjaxRequestWithData(url, data, onSuccessRenameFile, "RENAME");
}

function bindRenamingFile(){
	var $renameInputs = $("input.rename-txt");	
	$renameInputs.on({
		"keypress": function(e){
			var keyCde = e.which || e.keyCode;
			if(keyCde === 13){
				renameAFile($(this));
				return false;
			}
		},		
		"blur": function(){
			renameAFile($(this));			
			return false;
		}
	});	
}

function bindUploadMultipleFiles(){
	var $error = $("#uploadFilesError");
	$("#btnUploadMultiple").on("click", function(){
		if(filesArray.length === 0){
			showAlertMessage($error, "No file(s) to upload", ERROR);
		}else{
			multipleFilesUpload();
		}
		return false;
	});	
}

function removeExtraSpan(){
	var $emailContainer = $("#emailCheckContainer");
	$("#chkEmail").on("change", function(){
		$emailContainer.find("span.checkbox").remove();	
	});		
	$emailContainer.find("span.checkbox").remove();		
}

function sourceToImage(imgSrc, optionalCallback){
	
	var newImg = document.createElement('img');
	
	if (typeof optionalCallback === "function" && optionalCallback !== undefined) { 
		newImg.onload = optionalCallback; 
	}
	
    newImg.src = imgSrc;

    return newImg;
}

function isAnyOrAllChecked($chkBoxes, isAny){
	var result = $chkBoxes.filter(function(){
		if(isAny){ // true means any checked, else means all checked
			return this.checked;
		}else{
			return !this.checked;
		}		
	});
						
	return (result.length === 0);
}

function rebindGridItemClick($gridItemRow){	
	if($gridItemRow !== undefined){			
		$gridItemRow.removeClass("grid-item-default").addClass("grid-item-selected")
				    .off("click").on("click", onGridItemClick);
		
   		$(".nav-tool-bar").show();
   	}
}

function getSwitchControl(values, groupClass){
	
	var $btnBar = $("<div/>", {"class":"button-bar position-rel top-10 small"}),
		$group = $("<ul/>",{"class":"button-group small " + groupClass});
	
	$.each(values, function(index, item){
		
		var $item = $("<li/>"),
			$itemChild = $("<a/>", {"href": "#", "data-tag":item, "class":"small button secondary", "text":item});
		
		$group.append($item.append($itemChild));
	});
			
	$btnBar.append($group);	
	
	return $btnBar;
}

//plugin to call before showing of div
$(function($) {

  var _oldShow = $.fn.show;

  $.fn.show = function(speed, oldCallback) {
    return $(this).each(function() {
      var obj = $(this),
          newCallback = function() {
            if ($.isFunction(oldCallback)) {
              oldCallback.apply(obj);
            }
            obj.trigger('afterShow');
          };

      // you can trigger a before show if you want
      obj.trigger('beforeShow');

      // now use the old function to show the element passing the new callback
      _oldShow.apply(obj, [speed, newCallback]);
    });
  }
});