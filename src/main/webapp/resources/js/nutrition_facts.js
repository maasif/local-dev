/**
 * Created by omursaleen on 2/18/2015.
 */

if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//==========================================
/* NutritionFacts JS Model*/
var NutritionFacts = {

    init: function(pagePath, mealNames, foodNames){
        var that = this;

        that.GLOBAL_FOOD_ID = "";
        that.FOOD_ID = "";
        that.FOOD_NAME = "";
        that.MEAL_NAMES = JSON.parse(mealNames);
        that.FOOD_NAMES = JSON.parse(foodNames);
        that.PAGE_PATH = pagePath;

        //alert message instance
        that.ALERT_MESSAGES = Object.create(AlertMessage).init();

        //ajaxing instance
        that.AJAXING = Object.create(Ajaxing);

        that.CONSTANTS = {
            "SAVE": "SAVE",
            "MORE": "MORE"
        };

        that.cachePageConstants();
        that.initSuggestionList();
        that.bindEventListeners();
        that.calculateTotals();

        window.nutritionFactsObject = that;
        return that;
    },

    cachePageConstants: function(){
        var that = this;

        that.$commonClass = $('.common-class');
        that.$foodModal = $("#foodModal");
        that.$newFoodName = $("#newFoodName");
        that.$btnCloseFoodDialog = $("#btnCloseFoodDialog");
        that.$btnAddFood = $("#btnAddFood");
        that.$btnCloseFoodDialog = $("#btnCloseFoodDialog");
        that.$btnCloseFoodDialogTop = $("#btnCloseFoodDialogTop");
        that.$btnSaveAndAddMore = $("#btnSaveAndAddMore");
        that.$btnSaveFood = $("#btnSaveFood");
        that.$btnConfirmDelete = $("#btnConfirmDelete");
        that.$logNameFromFood = $("#logNameFromFood");
        that.$logMealName = $("#logMealName");

        that.$carbs = $("#carbs");
        that.$fats = $("#fats");
        that.$protein = $("#protein");
        that.$servingSize = $("#servingSize");
    },

    initSuggestionList: function(){
        var that = this;

        that.buildFoodNamesTypeAhead(that.FOOD_NAME);
        that.buildMealNamesTypeAhead(that.MEAL_NAMES);
    },

    bindEventListeners: function(){
        var that = this;

        that.$btnAddFood.on("click", function(e){
            e.preventDefault();
            that.openAddFoodDialog();
        });

        that.$btnCloseFoodDialog.add(that.$btnCloseFoodDialogTop).on("click", function(e){
            e.preventDefault();
            that.closeDialog();
        });

        that.$btnSaveFood.on("click", function(e){
            e.preventDefault();
            that.saveFood(that.CONSTANTS.SAVE);
        });

        that.$btnSaveAndAddMore.on("click", function(e){
            e.preventDefault();
            that.saveFood(that.CONSTANTS.MORE);
        });

        that.$btnConfirmDelete.on("click", function(){
            that.confirmDeleteFood();
        });

        that.$carbs.on('click keyup', function (e) {
            var value = this.value;
            if (value.indexOf('.') != -1 && value.length > 4 ){
                this.value = value.slice(0, 4);
            } else if (value.indexOf('.') == -1 && value.length > 3){
                this.value = value.slice(0, 3);
            } else {
                that.calculateTotalCarbs();
            }
        });

        that.$fats.on('click keyup', function (e) {
            var value = this.value;
            if (value.indexOf('.') != -1 && value.length > 5 ){
                this.value = value.slice(0, 4);
            } else if (value.indexOf('.') == -1 && value.length > 3){
                this.value = value.slice(0, 3);
            } else {
                that.calculateTotalFats();
            }
        });

        that.$protein.on('click keyup', function (e) {
            var value = this.value;
            if (value.indexOf('.') != -1 && value.length > 5 ){
                this.value = value.slice(0, 4);
            } else if (value.indexOf('.') == -1 && value.length > 3){
                this.value = value.slice(0, 3);
            } else {
                that.calculateTotalProtein();
            }
        });

        that.$servingSize.on('change', function (e) {
            that.calculateTotalCarbs();
            that.calculateTotalFats();
            that.calculateTotalProtein();
        });

        $(".edit-icon").off("click").on("click", function(){
            window.nutritionFactsObject = that;
            that.editFood($(this).data("id"));
        });

        $(".del-icon").off("click").on("click", function(){
            that.deleteFood($(this).data("id"));
        });

       /* $("#logNameFromFood").on("blur", function(){

            window.nutritionFactsObject = that;

            var actionUrl = "updateMealName.action",
                dataToSend = {
                    "logId": $("#logIdval").val(),
                    "logName": $('#logNameFromFood').val()
                },
                postRequestActions = {
                    "requestType" : "GET",
                    "loading" : null
                };

            sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
        });*/
    },

    emptyAddFoodDialogFields: function(){
        var that = this;

        $("#mealId").val(null);

        that.FOOD_ID = "";
        $("#foodId").val(that.FOOD_ID);

        $("#mealOptions").val(null);

        that.$newFoodName.val("");

        $("#carbs").val("0");
        $("#fats").val("0");
        $("#protein").val("0");
        $("#servingSize").val("1");
        $("#servingUnit").val("");
        $("#totalFoodCarbs").text("0");
        $("#totalFoodFats").text("0");
        $("#totalFoodProtein").text("0");

        $(".new-food-success").hide();
    },

    openAddFoodDialog: function(){

        var that = this;

        that.emptyAddFoodDialogFields();
        that.showDialog();

        $("#servingUnit").removeAttr('disabled');
        $("#mealOptions").focus();
    },

    showDialog: function(){
        var that = this;

        $('.common-class').each(function() {
            $(this).addClass('low-opacity');
            $(this).prop("disabled", true );
        });

        $("#foodModal").addClass('display-dialog').show("slide", { direction: "right" }, 500);
        $("#newFoodName").focus();

        $('html, body').animate({
            scrollTop: $('#foodDetails').offset().top
        }, 'slow');
    },

    closeDialog: function(){
        var that = this;

        $("#foodModal").hide("slide", { direction: "right" }, 500);

        $('.common-class').each(function() {
            $(this).removeClass('low-opacity');
            $(this).prop("disabled", false );
        });

        $("body").css("overflow", "auto");
    },

    getFoodSuggestionArray: function(foodsMap){
        return $.map(foodsMap, function(index, obj){
            return {"name":index, "id": obj};
        });
    },

    buildFoodNamesTypeAhead: function(dataMap){
        var that = this;

        if(dataMap){

            var newArray = that.getFoodSuggestionArray(dataMap),
                $typeAhead = that.$newFoodName.typeahead();

            $typeAhead.data('typeahead').source = newArray;

        } else{

            var newArray = that.getFoodSuggestionArray(that.FOOD_NAMES);

            that.$newFoodName.val("").typeahead({
                source : newArray,
                itemSelected : function(item) {
                    that.GLOBAL_FOOD_ID = item.data("value");

                    that.FOOD_NAME = item.text();

                    if(that.FOOD_ID == ""){
                        that.FOOD_ID = item.data("value");
                    }

                    var actionUrl = "getFoodNutritionInfo.action"
                        , dataToSend = {'foodId': that.GLOBAL_FOOD_ID}
                        , postRequestActions = {
                            "requestType" : "GET",
                            "successCallBack" : that.populateNutritionInfo
                        };

                    sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
                }
            });
        }
    },

    buildMealNamesTypeAhead: function (dataMap){
        var that = this,
            newArray = that.getFoodSuggestionArray(that.MEAL_NAMES);

        $('#logNameFromFood').typeahead({
            source : newArray,
            itemSelected : function(item) {

                window.nutritionFactsObject = that;

                var mealSummaryId = item.data("value"),
                    mealNameSuggest = item.text(),
                    actionUrl = "getFoodLogSummaryDetail.action",
                    dataToSend = {
                        'mealSummaryId': mealSummaryId,
                        "logId": $("#logIdval").val()
                    },
                    postRequestActions = {
                        "requestType" : "GET",
                        "successCallBack" : that.populateCompleteSummary
                    };

                sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
                return false;
            }
        });
    },

    populateNutritionInfo: function(data){
        $("#carbs").val(data.CARBS);
        $("#fats").val(data.FATS);
        $("#protein").val(data.PROTEIN);
        $("#totalFoodCarbs").text(data.CARBS);
        $("#totalFoodFats").text(data.FATS);
        $("#totalFoodProtein").text(data.PROTEIN);
        $("#servingUnit").val(data.FOOD_UNIT);
        $("#servingSize").val("1");
    },

    removeZero: function(value){

        var that = this,
            temp = $.trim(value),
            res = temp.charAt(temp.length-1);

        if(temp && temp.indexOf(".") != -1 && res == "0"){
            temp = value.replace(/\.*0*$/g, "");
        }

        try{
            var floatV = parseFloat(temp);
            if(floatV < 0){
                temp = 0;
            }
        }catch (err){}

        return temp;
    },

    calculateTotalCarbs: function(){

        var that = this;

        if ($("#carbs").val() != ""){

            var carbs = parseFloat($("#carbs").val()),
                servingSize = parseFloat($("#servingSize").val());

           $("#totalFoodCarbs").text(that.removeZero((carbs * servingSize).toFixed(2)));

        } else if ($("#carbs").val() == ""){
            $("#totalFoodCarbs").text("0");
        } else {
            $("#carbs").val("0");
        }
    },

    calculateTotalFats: function(){
        var that = this;

        if($("#fats").val() != ""){

            var fats = parseFloat($("#fats").val()),
                servingSize = parseFloat($("#servingSize").val());

            $("#totalFoodFats").text(that.removeZero((fats * servingSize).toFixed(2)));

        } else if($("#fats").val() == ""){
            $("#totalFoodFats").text("0");
        } else{
            $("#fats").val("0");
        }
    },

    calculateTotalProtein: function(){
        var that = this;

        if($("#protein").val() != ""){

            var protein = parseFloat($("#protein").val()),
                servingSize = parseFloat($("#servingSize").val());

            $("#totalFoodProtein").text(that.removeZero((protein * servingSize).toFixed(2)));

        } else if($("#protein").val() == "") {
            $("#totalFoodProtein").text("0");
        } else{
            $("#protein").val("0");
        }
    },

    calculateTotals: function(){
        var that = this;

        $("#totalCarbs").text(that.removeZero(TOTALS_CFPC.carbs));
        $("#totalFats").text(that.removeZero(TOTALS_CFPC.fats));
        $("#totalProtein").text(that.removeZero(TOTALS_CFPC.proteins));
        $("#totalSum").text(that.removeZero(parseFloat(TOTALS_CFPC.calories).toFixed(2)));

        that.calPercentage(TOTALS_CFPC.calories, TOTALS_CFPC.carbs, TOTALS_CFPC.fats, TOTALS_CFPC.proteins);
    },

    calPercentage: function(totalCalories, totalCarbs, totalFats, totalProtein){

        var that = this,
            carbs,
            fats,
            protein,
            totalPercentage = 0;

        if(totalCalories != "0" && totalCalories != "0.0" && totalCalories != "0.00"){
            var totalCals = totalCalories;

            if(totalCarbs != "0"){
                carbs = totalCarbs;
                var percentage = (carbs*4/totalCals)*100;
                totalPercentage += percentage;
                percentage = (percentage).toFixed(0);
                $("#percentageOfCalsInCarbs").text(percentage+"%");
            }

            if(totalFats != "0"){
                fats = totalFats;
                var percentage = (fats*9/totalCals)*100;
                totalPercentage += percentage;
                percentage = (percentage).toFixed(0);
                $("#percentageOfCalsInFats").text(percentage+"%");
            }

            if(totalProtein != "0"){
                protein = totalProtein;
                var percentage = (protein*4/totalCals)*100;
                totalPercentage += percentage;
                percentage = (percentage).toFixed(0);
                $("#percentageOfCalsInProtein").text(percentage+"%");
            }
        }
    },

    populateMealNameField: function(){
        var that = this,
            foodName = that.$newFoodName.val(),
            rowCount = $('#nutTable tr').length,
            isNameFromSuggestion = false;

        if(rowCount == 0){
            if(!that.$logNameFromFood.val()){
                that.$logNameFromFood.val($.trim(foodName));
                that.$logMealName.val(replaceQuotes(that.$logNameFromFood.val()));
            }
        } else if (rowCount == 1){

            //check if name is from food list if so then add other wise don't add this
            var newArray = that.getFoodSuggestionArray(that.FOOD_NAMES);
            $.each(newArray, function (i, val) {
                if(val.name == that.$logNameFromFood.val()) {
                    isNameFromSuggestion = true;
                }
            });

            if(isNameFromSuggestion){

                var newFoodName = $.trim(foodName),
                    oldFoodName = that.$logNameFromFood.val();

                if(newFoodName != oldFoodName) {
                    that.$logNameFromFood.val(that.$logNameFromFood.val() + ", " + newFoodName);
                    that.$logMealName.val(replaceQuotes(that.$logNameFromFood.val()));
                }
            }
        }
    },

    saveFood: function(saveType){

        var that = this,
            foodNameVal = that.$newFoodName.val(),
            $ddServingUnit = $("#servingUnit");

        if($.trim(foodNameVal).length == 0){
            var $newFoodError = $(".new-food-error").text("Please enter food name").show();
            that.$newFoodName.val("");
            hideElement($newFoodError);
            return;
        }

        if($.trim($ddServingUnit.val()).length == 0){
            var $newFoodError = $(".new-food-error").text("Please select unit").show();
            hideElement($newFoodError);
            return;
        }

        $("#foodCarbs").val(that.$carbs.val());
        $("#foodFats").val(that.$fats.val());
        $("#foodProtein").val(that.$protein.val());
        $("#foodServingSize").val($("#servingSize").val());
        $("#foodServingUnit").val($("#servingUnit").val());
        $("#foodId").val(that.FOOD_ID);
        $("#foodName").val(replaceQuotes(that.$newFoodName.val()));

        that.populateMealNameField();

        if( saveType == that.CONSTANTS.SAVE){
            that.closeDialog();
        }

        window.nutritionFactsObject = that;

        var config = {
            "requestType":"GET",
            "successCallBack": that.onSuccessAddFood,
            "loading": $("#loading")
        };

        sendAjaxRequestWithData("addMealFood", $("#mealFoodForm").serialize(), config);

        if( saveType == that.CONSTANTS.MORE){
            //that.emptyAddFoodDialogFields();
        }
    },

    onSuccessAddFood: function(data){
        var that =  window.nutritionFactsObject;

        if(data){
            $(".foodDiv").show();
            $(".nutriFoodSpan").hide();
            var fMaster = $.parseJSON(data.CUSTOM_DATA);
            var selFoodId = $.parseJSON(data.DATA);
            that.addRow(fMaster, selFoodId);
        }
    },

    addRow: function(fMaster, data){
        var that = this;

        if($(".nutTableRow"+data).length){

            var carbs = parseFloat($(".foodServing"+data).text()) * $(".foodCarbs"+data).text(),
                fats = parseFloat($(".foodServing"+data).text()) * $(".foodFats"+data).text(),
                protein = parseFloat($(".foodServing"+data).text()) * $(".foodProtein"+data).text();

            $("#totalCarbs").text(parseFloat($("#totalCarbs").text()) - carbs);
            $("#totalFats").text(parseFloat($("#totalFats").text()) - fats);
            $("#totalProtein").text(parseFloat($("#totalProtein").text()) - protein);
            $("#totalSum").text(parseFloat($("#totalSum").text()) - (carbs*4 + fats*9 + protein*4));

            $(".nutTableRow"+data).remove();
        }

        var servingSize = that.removeZero(parseFloat($("#servingSize").val())),
            carbs = $("#servingSize").val() * $("#carbs").val(),
            fats = $("#servingSize").val() * $("#fats").val(),
            protein = $("#servingSize").val() * $("#protein").val(),
            totalCarbsVal = $("#totalCarbs").text(),
            totalFatsVal = $("#totalFats").text(),
            totalProteinVal = $("#totalProtein").text(),
            totalSumVal = $("#totalSum").text();

        if(!totalCarbsVal || totalCarbsVal == "NaN"){
            totalCarbsVal = "0";
        }

        if(!totalFatsVal || totalFatsVal == "NaN"){
            totalFatsVal = "0";
        }

        if(!totalProteinVal || totalProteinVal == "NaN"){
            totalProteinVal = "0";
        }

        if(!totalSumVal || totalSumVal == "NaN"){
            totalSumVal = "0";
        }

        var totalCarbs = that.removeZero((parseFloat(totalCarbsVal) + carbs).toFixed(2)),
            totalFats = that.removeZero((parseFloat(totalFatsVal) + fats).toFixed(2)),
            totalProtein = that.removeZero((parseFloat(totalProteinVal) + protein).toFixed(2)),
            totalSum = that.removeZero((parseFloat(totalSumVal) + (carbs*4 + fats*9 + protein*4)).toFixed(2));

        $("#totalCarbs").text(totalCarbs);
        $("#totalFats").text(totalFats);
        $("#totalProtein").text(totalProtein);
        $("#totalSum").text(totalSum);

        that.calPercentage(totalSum, totalCarbs, totalFats, totalProtein);

        carbs = that.removeZero(parseFloat($("#carbs").val()));
        fats = that.removeZero(parseFloat($("#fats").val()));
        protein = that.removeZero(parseFloat($("#protein").val()));

        window.nutritionFactsObject = that;

        that.saveFoodLogSummaryTotals(totalCarbs, totalFats, totalProtein, totalSum);

        var $imgEdit = $("<img/>", {"src": that.PAGE_PATH + "/resources/images/Edit.png", "data-id": data, "title": "Edit Food", "class": "nav-icons edit-icon"}),
            $imgDelete = $("<img/>", {"src": that.PAGE_PATH + "/resources/images/delete.png", "data-id": data, "title": "Delete Food", "class": "nav-icons del-icon"});

        var servingSizeUnitHTML = '<td class="second-col foodServing'+data+'">N/A</td>';

        if(fMaster && fMaster.servingSizeUnit){
            servingSizeUnitHTML = '<td class="second-col foodServing'+data+'">'+ fMaster.servingSizeUnit +'</td>';
        }

        $("#nutTable").append('<tr class="nutTableRow'+data+'">'+
                                '<td class="first-col newFoodName'+data+' capital">'+that.$newFoodName.val()+'</td>'+
                                '<td class="second-col foodServing'+data+'">'+servingSize+'</td>'+
                                servingSizeUnitHTML +
                                '<td class="third-col foodFats'+data+'">'+fats+'</td>'+
                                '<td class="forth-col foodCarbs'+data+'">'+carbs+'</td>'+
                                '<td class="fifth-col foodProtein'+data+'">'+protein+'</td>'+
                                '<td class="foodAction sixth-col">'+
                                '<div style="text-align:center;">'+
                                $imgEdit.wrap("<p/>").parent().html() + $imgDelete.wrap("<p/>").parent().html() +
                                '</div>'+
                                '</td>'+
                              '</tr>');

        $(".edit-icon").off("click").on("click", function(){
            window.nutritionFactsObject = that;
            that.editFood($(this).data("id"));
        });

        $(".del-icon").off("click").on("click", function(){
            that.deleteFood($(this).data("id"));
        });

        //refresh food names typeahead
        that.FOOD_NAMES[data] = that.$newFoodName.val();
        that.buildFoodNamesTypeAhead(that.FOOD_NAMES);

        //refresh meal names typeahead
        that.MEAL_NAMES[data] = that.$logNameFromFood.val();
        that.buildMealNamesTypeAhead(that.MEAL_NAMES);

        that.FOOD_ID = "";
        that.$newFoodName.val("");

        that.$carbs.val("");
        that.$fats.val("");
        that.$protein.val("");

        that.$servingSize.val(1);
        $("#servingUnit").val("");
        $("#totalFoodCarbs").text("0");
        $("#totalFoodProtein").text("0");
        $("#totalFoodFats").text("0");

        var $newFoodSuccess = $(".new-food-success").text("Food saved successfully").show(),
            $divFoodDetail = $('.div-1-food-detail');

        $("#newFoodName").focus();

        hideElement($newFoodSuccess);

        if($divFoodDetail && $divFoodDetail[0]) {
            $divFoodDetail.animate({scrollTop: $divFoodDetail[0].scrollHeight}, 1000);
        }

        //comment it if don't work perfectly :P
        //that.calculateTotalsForced();
    },

    saveFoodLogSummaryTotals: function(totalCarbs, totalFats, totalProteins, totalSum){
        var that = window.nutritionFactsObject,
            dataToSend = {
                "totalCarbs" : totalCarbs,
                "totalFats" : totalFats,
                "totalProtein" : totalProteins,
                "totalCalories" : totalSum,
                "logId": $("#logIdval").val()
            },
            postRequestActions = {
                "successCallBack": that.onSavingFoodLogSummaryTotals
            };

        that.AJAXING.sendPostRequest("saveFoodLogSummaryTotals.action", dataToSend, postRequestActions);
    },

    onSavingFoodLogSummaryTotals: function(data){
        var that = window.nutritionFactsObject;
        if(window.console){
            console.log(data);
        }
    },

    editFood: function(editFoodId){

        var that = this;

        that.FOOD_ID = editFoodId;

        window.nutritionFactsObject = that;

        var actionUrl = "getFoodNutritionInfoEdit.action"
            , dataToSend = {'foodId': that.FOOD_ID, "logId": $("#logIdval").val()}
            , postRequestActions = {
                "requestType" : "GET",
                "successCallBack" : that.populateNutritionInfoEdit
            };

        sendAjaxRequestWithData(actionUrl, dataToSend, postRequestActions);
    },

    populateNutritionInfoEdit: function(data){

        var that = window.nutritionFactsObject,
            foodName = $.trim($("td.newFoodName"+that.FOOD_ID).text());

        $("#newFoodName").val(foodName);
        $("#carbs").val(data.CARBS);
        $("#fats").val(data.FATS);
        $("#protein").val(data.PROTEIN);

        var $oldServingSize = $("td.foodServing"+that.FOOD_ID+":first"),
            servingSize = $.trim($("td.foodServing"+that.FOOD_ID).text()),
            $servingSize = $("#servingSize");

        $("#servingUnit").val(data.FOOD_UNIT);
        $servingSize.val(data.SERVING_SIZE);

        var totalCarbs = parseFloat($.trim(servingSize)) * data.CARBS,
            totalFats = parseFloat(servingSize) * data.FATS,
            totalProtein = parseFloat(servingSize) * data.PROTEIN;

        $("#totalFoodCarbs").text(totalCarbs);
        $("#totalFoodFats").text(totalFats);
        $("#totalFoodProtein").text(totalProtein);
        $(".new-food-success").hide();

        that.showDialog();
        $("#servingUnit").removeAttr('disabled');
    },

    deleteFood: function(foodId){
        $("#delFoodId").val(foodId);
        $("#confirmationAlert").foundation('reveal', 'open');
    },

    confirmDeleteFood: function(){

        var that = this;

        $("#confirmationAlert").foundation('reveal', 'close');

        window.nutritionFactsObject = that;

        var config = {
            "requestType":"GET",
            "successCallBack": that.onSuccessDeleteFood,
            "loading": $("#loading")
        };

        sendAjaxRequestWithData("deleteMealFood", $("#delFoodForm").serialize(), config);
    },

    onSuccessDeleteFood: function(data) {

        var that = window.nutritionFactsObject,
            carbs = parseFloat($(".foodServing" + data).text()) * $(".foodCarbs" + data).text(),
            fats = parseFloat($(".foodServing" + data).text()) * $(".foodFats" + data).text(),
            protein = parseFloat($(".foodServing" + data).text()) * $(".foodProtein" + data).text(),
            totalCarbs = 0, totalFats = 0, totalProtein = 0, totalSum = 0;

        if (parseFloat($("#totalCarbs").text()) - carbs !== 0) {
            var totalCarbs = (parseFloat($("#totalCarbs").text()) - carbs).toFixed(2);
        }

        if (parseFloat($("#totalProtein").text()) - protein !== 0) {
            var totalProtein = (parseFloat($("#totalProtein").text()) - protein).toFixed(2);
        }

        if (parseFloat($("#totalFats").text()) - fats !== 0) {
            var totalFats = (parseFloat($("#totalFats").text()) - fats).toFixed(2);
        }

        if (parseFloat($("#totalSum").text()) - (carbs * 4 + fats * 9 + protein * 4) !== 0) {
            var totalSum = (parseFloat($("#totalSum").text()) - (carbs * 4 + fats * 9 + protein * 4)).toFixed(2);
        }

        $("#totalCarbs").text(that.removeZero(totalCarbs));
        $("#totalFats").text(that.removeZero(totalFats));
        $("#totalProtein").text(that.removeZero(totalProtein));
        $("#totalSum").text(that.removeZero(totalSum));

        that.calPercentage($("#totalSum").text(), $("#totalCarbs").text(), $("#totalFats").text(), $("#totalProtein").text());

        $("#nutTable").find(".nutTableRow" + data).remove();

        var rowCount = $('#nutTable tr').length;

        if (rowCount == 0) {
            $("#totalCarbs").text("");
            $("#totalFats").text("");
            $("#totalProtein").text("");
            $("#totalSum").text("0");

            $("#percentageOfCalsInCarbs").text("");
            $("#percentageOfCalsInFats").text("");
            $("#percentageOfCalsInProtein").text("");

            $(".foodDiv").hide();
            $(".nutriFoodSpan").slideDown();
        }
    },

    populateCompleteSummary: function (data){
        var that = window.nutritionFactsObject;

        $(".foodDiv").show();
        $(".nutriFoodSpan").hide();

        var foodMasters = data;
        if(foodMasters && foodMasters.length > 0){
            $.each(foodMasters, function(index, fMaster){
                $("#carbs").val(fMaster.carbs);
                $("#fats").val(fMaster.fats);
                $("#protein").val(fMaster.protein);
                $("#servingSize").val(fMaster.numberOfServings);
                $("#servingUnit").val(fMaster.servingSizeUnit);
                $("#foodId").val(fMaster.foodMasterId);
                $("#newFoodName").val(fMaster.foodName);
                $(".nutriFoodSpan").hide();
                that.addRow(fMaster, fMaster.foodMasterId);
            });
        }
    },

    calculateTotalsForced: function(){

        var that = this, tFats = 0, tCarbs = 0, tProteins = 0, totalSum = 0;

        $("#nutTable tbody tr").each(function(){

              var $tr = $(this),
                  serving = $.trim($tr.children(".second-col").text()),
                  fats = $.trim($tr.children(".third-col").text()),
                  carbs = $.trim($tr.children(".forth-col").text()),
                  proteins = $.trim($tr.children(".fifth-col").text());

             tFats += that.removeZero(parseFloat(fats).toFixed(2));
             tCarbs += that.removeZero(parseFloat(carbs).toFixed(2));
             tProteins += that.removeZero(parseFloat(proteins).toFixed(2));
             totalSum += that.removeZero(parseFloat(carbs*4 + fats*9 + proteins*4)).toFixed(2);
        });

        $("#totalFats").text(tFats);
        $("#totalCarbs").text(tCarbs);
        $("#totalProtein").text(tProteins);
        $("#totalSum").text(totalSum);

        that.calPercentage(totalSum, tCarbs, tFats, tProteins);
    }
};