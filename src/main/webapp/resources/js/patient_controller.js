(function(){

    var app = angular.module("PatientModule", []);

    app.controller("PatientController", ["$scope", "$http", function($scope, $http){

        $scope.yearsList = [];

        var currentYear = new Date().getFullYear(),
            gobackTo = currentYear - 30; //30 years back from current year

        /*for(var i = currentYear; i >= gobackTo; i-- ){
            $scope.yearsList.push({ name: i, abbreviation: i.toString()});
        }*/

        $scope.yearsListDiag = ["Less than 2 years", "2-5 years", "6-10 years", "More than 10 years"];
        $scope.yearsListDiab = ["Less than 2 years", "2-5 years", "6-10 years", "More than 10 years", "Never had Diabetes Education"];

        $scope.stateList = [
            { name: 'AL', abbreviation: 'AL'},
            { name: 'AK', abbreviation: 'AK'},
            { name: 'AS', abbreviation: 'AS'},
            { name: 'AZ', abbreviation: 'AZ'},
            { name: 'AR', abbreviation: 'AR'},
            { name: 'CA', abbreviation: 'CA'},
            { name: 'CO', abbreviation: 'CO'},
            { name: 'CT', abbreviation: 'CT'},
            { name: 'DE', abbreviation: 'DE'},
            { name: 'DC', abbreviation: 'DC'},
            { name: 'FM', abbreviation: 'FM'},
            { name: 'FL', abbreviation: 'FL'},
            { name: 'GA', abbreviation: 'GA'},
            { name: 'GU', abbreviation: 'GU'},
            { name: 'HI', abbreviation: 'HI'},
            { name: 'ID', abbreviation: 'ID'},
            { name: 'IL', abbreviation: 'IL'},
            { name: 'IN', abbreviation: 'IN'},
            { name: 'IA', abbreviation: 'IA'},
            { name: 'KS', abbreviation: 'KS'},
            { name: 'KY', abbreviation: 'KY'},
            { name: 'LA', abbreviation: 'LA'},
            { name: 'ME', abbreviation: 'ME'},
            { name: 'MH', abbreviation: 'MH'},
            { name: 'MD', abbreviation: 'MD'},
            { name: 'MA', abbreviation: 'MA'},
            { name: 'MI', abbreviation: 'MI'},
            { name: 'MN', abbreviation: 'MN'},
            { name: 'MS', abbreviation: 'MS'},
            { name: 'MO', abbreviation: 'MO'},
            { name: 'MT', abbreviation: 'MT'},
            { name: 'NE', abbreviation: 'NE'},
            { name: 'NV', abbreviation: 'NV'},
            { name: 'NH', abbreviation: 'NH'},
            { name: 'NJ', abbreviation: 'NJ'},
            { name: 'NM', abbreviation: 'NM'},
            { name: 'NY', abbreviation: 'NY'},
            { name: 'NC', abbreviation: 'NC'},
            { name: 'ND', abbreviation: 'ND'},
            { name: 'MP', abbreviation: 'MP'},
            { name: 'OH', abbreviation: 'OH'},
            { name: 'OK', abbreviation: 'OK'},
            { name: 'OR', abbreviation: 'OR'},
            { name: 'PW', abbreviation: 'PW'},
            { name: 'PA', abbreviation: 'PA'},
            { name: 'PR', abbreviation: 'PR'},
            { name: 'RI', abbreviation: 'RI'},
            { name: 'SC', abbreviation: 'SC'},
            { name: 'SD', abbreviation: 'SD'},
            { name: 'TN', abbreviation: 'TN'},
            { name: 'TX', abbreviation: 'TX'},
            { name: 'UT', abbreviation: 'UT'},
            { name: 'VT', abbreviation: 'VT'},
            { name: 'VI', abbreviation: 'VI'},
            { name: 'VA', abbreviation: 'VA'},
            { name: 'WA', abbreviation: 'WA'},
            { name: 'WV', abbreviation: 'WV'},
            { name: 'WI', abbreviation: 'WI'},
            { name: 'WY', abbreviation: 'WY' }
        ];

        $scope.racesList = [
            { name: 'American Indian or Allaskan Native', abbreviation: 'American Indian or Allaskan Native'},
            { name: 'Black/African American', abbreviation: 'Black/African American'},
            { name: 'Hispanic/Latino', abbreviation: 'Hispanic/Latino'},
            { name: 'Native Hawaiin or other Pacific Islander', abbreviation: 'Native Hawaiin or other Pacific Islander'},
            { name: 'White/Caucasian', abbreviation: 'White/Caucasian'},
            { name: 'Mixed race/mutiracial', abbreviation: 'Mixed race/mutiracial'},
            { name: 'Do not know', abbreviation: 'Do not know'}
        ];

        $scope.diabetesList = [
            { name: 'Type 2', abbreviation: 'Type 2'},
            { name: 'Gestational', abbreviation: 'Gestational'},
            { name: 'Type 1', abbreviation: 'Type 1'}
        ];

        $scope.dietaryRestrictionsList = [
            { name: 'Dairy free', abbreviation: 'Dairy free'},
            { name: 'Egg Allergy', abbreviation: 'Egg Allergy'},
            { name: 'Fish Allergy', abbreviation: 'Fish Allergy'},
            { name: 'Gluten free', abbreviation: 'Gluten free'},
            { name: 'Kosher', abbreviation: 'Kosher'},
            { name: 'Lactose free', abbreviation: 'Lactose free'},
            { name: 'Liquid diet', abbreviation: 'Liquid diet'},
            { name: 'Low fat', abbreviation: 'Low fat'},
            { name: 'Low sodium', abbreviation: 'Low sodium'},
            { name: 'Milk Allergy', abbreviation: 'Milk Allergy'},
            { name: 'Muslim', abbreviation: 'Muslim'},
            { name: 'None', abbreviation: 'None'},
            { name: 'Other', abbreviation: 'Other'},
            { name: 'Paleo', abbreviation: 'Paleo'},
            { name: 'Peanut Allergy', abbreviation: 'Peanut Allergy'},
            { name: 'Shellfish Allergy', abbreviation: 'Shellfish Allergy'},
            { name: 'Soy Allergy', abbreviation: 'Soy Allergy'},
            { name: 'Treenut Allergy', abbreviation: 'Treenut Allergy'},
            { name: 'Vegetarian (Asian)', abbreviation: 'Vegetarian (Asian)'},
            { name: 'Vegetarian (Indian)', abbreviation: 'Vegetarian (Indian)'},
            { name: 'Vegetarian with dairy', abbreviation: 'Vegetarian with dairy'},
            { name: 'Vegetarian with fish ("pescatarian")', abbreviation: 'Vegetarian with fish ("pescatarian")'},
            { name: 'Vegetarian/vegan - no animal products', abbreviation: 'Vegetarian/vegan - no animal products'},
            { name: 'Wheat free', abbreviation: 'Wheat free'}
        ];

        $scope.mealPreparerList = [
            { name: 'Self', abbreviation: 'Self'},
            { name: 'Spouse/Partner', abbreviation: 'Spouse/Partner'},
            { name: 'Adult child', abbreviation: 'Adult child'},
            { name: 'Other caregiver', abbreviation: 'Other caregiver'},
            { name: 'Do not know', abbreviation: 'Do not know'}
        ];

        $scope.educationList = [
            { name: 'Elementary school', abbreviation: 'Elementary school'},
            { name: 'Some high school', abbreviation: 'Some high school'},
            { name: 'High school degree', abbreviation: 'High school degree'},
            { name: 'Some college', abbreviation: 'Some college'},
            { name: 'College degree', abbreviation: 'College degree'},
            { name: 'Graduate degree', abbreviation: 'Graduate degree'},
            { name: 'Do not know', abbreviation: 'Do not know'}
        ];

        $scope.languageList = [
            { name: 'English', abbreviation: 'English'},
            { name: 'Spanish', abbreviation: 'Spanish'},
            { name: 'Do not know', abbreviation: 'Do not know'}
        ];

        $scope.diabetesSupportList = [
            { name: 'Family', abbreviation: 'Family'},
            { name: 'Friend(s)', abbreviation: 'Friend(s)'},
            { name: 'Co-worker(s)', abbreviation: 'Co-worker(s)'},
            { name: 'Healthcare provider(s)', abbreviation: 'Healthcare provider(s)'},
            { name: 'Support group', abbreviation: 'Support group'},
            { name: 'No one', abbreviation: 'No one'}
        ];

        $scope.medicationList = [];

        //storing in page level object
        $scope.patient = PATIENT_DETAILS;

        //setting selected values in Dropdowns
        $scope.patient.state = {abbreviation: PATIENT_DETAILS.state};
        $scope.patient.race = {abbreviation: PATIENT_DETAILS.race};
        $scope.patient.diagnosis = {abbreviation: PATIENT_DETAILS.diagnosis};
        $scope.patient.education = {abbreviation: PATIENT_DETAILS.education};
        $scope.patient.language = {abbreviation: PATIENT_DETAILS.language};
        //$scope.patient.diagnosisDate = {abbreviation: PATIENT_DETAILS.diagnosisDate};

        if(!PATIENT_DETAILS.diagnosis.abbreviation){
            $scope.patient.diagnosis = {abbreviation: "Type 2"};
        }

        var $ddMeal = $("#ddMealPreparer"),
            $ddDiabSupport = $("#ddDiabetesSupport"),
            $ddDietRestrictions = $("#ddDietaryRestrictions"),
            $ddMedications = $("#ddMedications"),
            $txtDietaryRestrictionsOther = $("#txtDietaryRestrictionsOther").addClass("hide").val("");

        setTimeout(function(){

            if(MEDICATIONS_LISTING){
                $.each(MEDICATIONS_LISTING, function(index, medication){
                    $ddMedications.append($("<option/>", {value:medication.name, "html": medication.name}));
                });

                $ddMedications.chosen();
            }

            if(PATIENT_DETAILS.mealPreparer){
                var MEAL_PREPARER = PATIENT_DETAILS.mealPreparer.split(", ");
                $.each(MEAL_PREPARER, function(index, meal){
                    var $option = $ddMeal.children("option[value='"+meal+"']");
                    if($option.length > 0){
                        $option.prop("selected", "selected");
                    }
                });
            }

            if(PATIENT_DETAILS.diabetesSupport){
                var DIABETES_SUPPORT = PATIENT_DETAILS.diabetesSupport.split(", ");
                $.each(DIABETES_SUPPORT, function(index, diab){
                    var $option = $ddDiabSupport.children("option[value='"+diab+"']");
                    if($option.length > 0){
                        $option.prop("selected", "selected");
                    }
                });

            }

            if(PATIENT_DETAILS.dietaryRestrictions){
                var DIETARY_RESTRICTIONS = PATIENT_DETAILS.dietaryRestrictions.split(", ");

                if(DIETARY_RESTRICTIONS.indexOf("Other") > -1){
                    $txtDietaryRestrictionsOther.removeClass("hide").val(PATIENT_DETAILS.dietaryRestrictionsOther);
                }

                $.each(DIETARY_RESTRICTIONS, function(index, diet){
                    var $option = $ddDietRestrictions.children("option[value='"+diet+"']");
                    if($option.length > 0){
                        $option.prop("selected", "selected");
                    }
                });
            }

            if(PATIENT_DETAILS.medications){
                var MEDICATIONS = PATIENT_DETAILS.medications.split(", ");
                $.each(MEDICATIONS, function(index, diet){
                    var $option = $ddMedications.children("option[value='"+diet+"']");
                    if($option.length > 0){
                        $option.prop("selected", "selected");
                    }
                });
            }

            $ddMeal.trigger("chosen:updated");
            $ddDiabSupport.trigger("chosen:updated");
            $ddDietRestrictions.trigger("chosen:updated").on("change", function(){
                if($(this).val() && $(this).val().indexOf("Other") > -1){
                    $txtDietaryRestrictionsOther.removeClass("hide");
                }else{
                    $txtDietaryRestrictionsOther.addClass("hide").val("");
                }
            }).trigger("change");

            $ddMedications.trigger("chosen:updated");
        }, 200);

        if(PATIENT_DETAILS.imagePath){
            $scope.patient.imagePath = APP_PATH + PATIENT_DETAILS.imagePath;
        }else{
            $scope.patient.imagePath = APP_PATH + "/resources/css/images/default_profile_image.gif";
        }

        //storing for showing required fields label when user submits
        $scope.submitted = false;

        //submit form function
        $scope.submitForm = function(patient) {

            var $dob = $("#txtDOB"),
                $txtPhone = $("#txtPhone"),
                $txtMRN = $("#txtMRN"),
                $txtConfirmMRN = $("#txtConfirmMRN"),
                $errorDietaryRestrictionsOther = $("#errorDietaryRestrictionsOther").hide();

            $txtConfirmMRN.next("small").hide();

            if($.trim($dob.val()).length == 0 && $.trim($txtPhone.val()).length == 0){
                $dob.next("small").show();
                $txtPhone.next("small").show();
                return;
            }

            if($.trim($dob.val()).length == 0){
                $dob.next("small").show();
                return;
            }

            if($.trim($txtPhone.val()).length == 0){
                $txtPhone.next("small").show();
                return;
            }

            if($.trim($txtMRN.val()).length > 0 && $txtMRN.val() != $txtConfirmMRN.val()){
                $txtConfirmMRN.next("small").show();
                return;
            }

            if(!$txtDietaryRestrictionsOther.val() && $txtDietaryRestrictionsOther.is(":visible")){
                $errorDietaryRestrictionsOther.show();
                return;
            }

            $scope.submitted = true;

            //if form is valid then process
            if($scope.patientForm.$valid){

                var ajaxing = Object.create(Ajaxing),
                    $loading = $("#loading").show(),
                    $error = $("#errMessage"),
                    alertMessages = Object.create(AlertMessage).init();

                patient.state = patient.state.abbreviation;
                patient.race = patient.race.abbreviation;
                patient.diagnosis = patient.diagnosis.abbreviation;
                patient.education = patient.education.abbreviation;
                patient.language = patient.language.abbreviation;

                var mealPrepCommaSeparated = $ddMeal.val(),
                    diabetesSupportCommaSeparated = $ddDiabSupport.val(),
                    dietaryRestrictionsCommaSeparated = $ddDietRestrictions.val(),
                    medicationsCommaSeparated = $ddMedications.val();

                if(mealPrepCommaSeparated){
                    mealPrepCommaSeparated = mealPrepCommaSeparated.join(", ");
                }

                if(diabetesSupportCommaSeparated){
                    diabetesSupportCommaSeparated = diabetesSupportCommaSeparated.join(", ");
                }

                if(dietaryRestrictionsCommaSeparated){
                    dietaryRestrictionsCommaSeparated = dietaryRestrictionsCommaSeparated.join(", ");
                }

                if(medicationsCommaSeparated){
                    medicationsCommaSeparated = medicationsCommaSeparated.join(", ");
                }

                patient.mealPreparer =  mealPrepCommaSeparated;
                patient.diabetesSupport = diabetesSupportCommaSeparated;
                patient.dietaryRestrictions = dietaryRestrictionsCommaSeparated;
                patient.medications = medicationsCommaSeparated;
                patient.dob = $dob.val();
                patient.phone = "+1 " + $txtPhone.val();

                if(!patient.weight){
                    patient.weight = 0;
                }

                $http({
                    url: "saveUpdatePatientDetails.action",
                    method: 'POST',
                    data: { "patientData" : angular.toJson(patient)},
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    transformRequest: function(obj) {
                        var str = [];
                        for(var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    }
                }).success(function(data){
                    data = angular.fromJson(angular.fromJson(data));

                    $scope.patient.state = {abbreviation: patient.state};
                    $scope.patient.race = {abbreviation: patient.race};
                    $scope.patient.diagnosis = {abbreviation: patient.diagnosis};
                    $scope.patient.education = {abbreviation: patient.education};
                    $scope.patient.language = {abbreviation: patient.language};

                    if(data.STATUS == "SUCCESS"){
                        alertMessages.showAlertMessage($error, "Data saved successfully.", alertMessages.SUCCESS);
                        if(REDIRECT_TO){
                            window.location = REDIRECT_TO;
                        }else{
                            window.location = "dashboard.action?patientId="+PATIENT_DETAILS.patientId;
                        }
                    } else {
                        if(data.REASON == "MRN already assigned"){
                            $loading.hide();
                            alertMessages.showAlertMessage($error, "This MRN is already assigned.", alertMessages.ERROR);
                        } else {
                            $loading.hide();
                            ajaxing.showErrorOrRedirect(data, $error, AJAX_ERROR_MESSAGE);
                        }
                    }
                }).error(function(err){
                    $loading.hide();
                    ajaxing.showErrorOrRedirect(err, $error, AJAX_ERROR_MESSAGE);
                });
            }
        };
    }]);
})();