(function(){
        	
	var app = angular.module("PatientSignupModule", []);
	
	app.controller("PatientSignupController", ["$scope", "$http", function($scope, $http){

		$scope.facilityList = FACILITY_LIST;

		//storing in page level object
		$scope.signup = {};

		//storing for showing required fields label when user submits
		$scope.submitted = false;

		$scope.clearForm = function(){
			$scope.signup = {};
			$scope.submitted = false;
		};

		//submit form function
		$scope.submitForm = function(signup) {
			
			$scope.submitted = true;
			
			//if form is valid then process
			if($scope.signupForm.$valid){
				var ajaxing = Object.create(Ajaxing),
        			$loading = $("#loading").show(),
					$error = $("#errMessage"),
					alertMessages = Object.create(AlertMessage).init();

				$scope.signup.facility = $scope.signup.facility.name;

        		$http({

        	        url: BASE_URL+ "services/signUp/patientSignUp",
        	        method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					data: angular.toJson(signup)
        	    }).success(function(data){		        	    	
        	    	data = angular.fromJson(data);	
        	    	if(data.STATUS == "SUCCESS"){
						$loading.hide();
						alertMessages.showAlertMessage($error, "Member signed up successfully.", alertMessages.SUCCESS);
						$scope.clearForm();
        	    	}else{ 
        	    		$loading.hide();
        	    		alertMessages.showAlertMessage($error, data.REASON, alertMessages.ERROR);
        	    	}
        	    }).error(function(err){
        	    	$loading.hide();
        	    	alertMessages.showAlertMessage($error, AJAX_ERROR_MESSAGE, alertMessages.ERROR);
        	    });	   
			}        			        
		};
	}]);        	
})();