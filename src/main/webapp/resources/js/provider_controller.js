(function(){
        	
	var app = angular.module("ProviderModule", []);
	
	app.controller("ProviderController", ["$scope", "$http", function($scope, $http){

		$scope.typeList = ["Food Coach", "Coach"];

        $scope.sortTypes = {
            "Food Coach": 5,
            "Coach": 4,
			"Tech Support": 3
        };

		$scope.facilityList = FACILITY_LIST;
		$scope.providerList = PROVIDER_LIST;
        $scope.facilityIdFromPref = FACILITY_ID_PREF;

		//storing in page level object
		$scope.provider = PROVIDER_LIST;
		$scope.facilityId = -1;
		$scope.isEdited = false;
		$scope.confirmPassword = "";
		$scope.designation = "";

		//storing for showing required fields label when user submits
		$scope.submitted = false;

		//by default load Tech Support option in select list
		if($scope.provider && $scope.provider.facilityId == $scope.facilityIdFromPref){
			$scope.typeList = ["Food Coach", "Coach", "Tech Support"];
		}

		$scope.openProviderDialog = function(){
			$("#providerListDialog").foundation("reveal", "open");
		};

		//on change of facility refresh coach type list
		$scope.refreshCoachTypeList = function(facilityId){
			$scope.typeList = ["Food Coach", "Coach"];
			if($scope.facilityIdFromPref == facilityId){
				$scope.typeList = ["Food Coach", "Coach", "Tech Support"];
			}
		};

		$scope.clearForm = function(){
			$scope.submitted = false;
			$scope.provider = {};
			$scope.facilityId = -1;
			$scope.confirmPassword = "";
			$scope.isEdited = false;
			$scope.designation = "";
		};

		$scope.editProvider = function(provider){
			$scope.provider = provider;
			$scope.provider.userType = provider.type;
			$scope.provider.facility = {"facilityId": provider.facilityId };
			$scope.designation = {"designation": provider.designation};
			$scope.facilityId = provider.facilityId;
			$scope.submitted = false;
			$scope.isEdited = true;
			$scope.isSMSEnabled = {"isSMSEnabled": provider.isSMSEnabled};
			$scope.isEmailEnabled = {"isEmailEnabled": provider.isEmailEnabled};
			$("#providerListDialog").foundation("reveal", "close");

            $("#txtPhone").val(removeCountryCode($scope.provider.phone));

            //written in common.js
            setTimeout(function(){
                formatPhoneNumber($("#txtPhone"), $("#txtPhone").val());
            }, 20);
		};

        if($scope.provider){
            $scope.editProvider($scope.provider);
            setTimeout(bindRemoveCoach, 1000);
        }

		//submit form function
		$scope.submitForm = function(provider) {
			
			$scope.submitted = true;

			//validate both fields
			var $password = $("#txtPassword"),
				$repeatPassword = $("#txtRepeatPassword");

			if($password.val() != $repeatPassword.val()){
				return;
			}

			//if form is valid then process
			if($scope.providerForm.$valid){

				var providerId = provider.providerId,
        			actionName = "saveProvider.action",
        			ajaxing = Object.create(Ajaxing),
        			$loading = $("#loading").show(),
					$error = $("#errMessage"),
					alertMessages = Object.create(AlertMessage).init();

        		//if greater than '0' it means to update the form
        		if(providerId > 0){
        			actionName = "saveProvider.action?providerId="+providerId;
        		}

                var facId = "";
                if(provider.facility){
                    $scope.facilityId = provider.facility.facilityId;
                    facId = $scope.facilityId;
                }

                var providerType = provider.userType;

                $scope.provider.userType = providerType;
                $scope.designation = provider.designation;
                $scope.sortType = $scope.sortTypes[providerType];

                provider.userType = providerType;

        		$http({
        	        url: actionName,
        	        method: 'POST',
                    data: { "userData" : angular.toJson(provider), "facilityId": facId, "designation" :$scope.designation, "isSMSEnabled": provider.isSMSEnabled,"isEmailEnabled": provider.isEmailEnabled,"sortType": $scope.sortType, "isAccessAll": provider.isAccessAll },
        	        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        	        transformRequest: function(obj) {
        	            var str = [];
        	            for(var p in obj)
        	            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        	            return str.join("&");
        	        }
        	    }).success(function(data){		        	    	
        	    	data = angular.fromJson(angular.fromJson(data));
                    $loading.hide();
        	    	if(data.STATUS == "SUCCESS"){
						alertMessages.showAlertMessage($error, "Data saved successfully.", alertMessages.SUCCESS);
						window.location = "coaches.action";
        	    	}else if(data.STATUS == "ERROR"){
                        alertMessages.showAlertMessage($error, data.REASON, alertMessages.ERROR);
        	    	}else{
                        ajaxing.showErrorOrRedirect(data, $error, AJAX_ERROR_MESSAGE);
                    }
        	    }).error(function(err){
        	    	$loading.hide();
        	    	ajaxing.showErrorOrRedirect(err, $error, AJAX_ERROR_MESSAGE);
        	    });
			}        			        
		};
	}]);        	
})();