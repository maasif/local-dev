function addToOrder(description, price){

	var $tr = $("<tr/>")
	, $tdLeft = $("<td/>").text(description)
	, $tdRight = $("<td/>",{"class":"text-bold"}).text("$"+price);	
		
	$tr.append($tdLeft).append($tdRight);
	
	return $tr;	
}

function getProductScript(productObj){
	
var title = " "+ productObj.productTitle + " $" + productObj.productPrice + " " + productObj.productDescription;

var $parent = $("<div/>", { "class":"row padding-10" })
      , $leftDiv = $("<div/>", { "class":"large-3 medium-3 three columns" })
      , $rightDiv = $("<div/>", { "class":"large-9 medium-9 nine columns align-left" })
      , $a = $("<a/>", { "class": "th image-item", "href": "#"}).append($("<img/>", {"src": productObj.productSrc}))
      , $rightabel = $("<label/>", { "class":"inline", "html": title })
      , $rightInput = $("<input/>", { "type":"checkbox", "class":"accessories", "data-title" : productObj.productTitle , "data-price" : productObj.productPrice, "data-id" : productObj.productId });

  $a.on("click", function(e){
	 e.preventDefault();
	 return false;
  });
    
  if($.browser.msie && $.browser.version <= 9){
	  $a.children("img")
	  	.css({"height":"160px","padding-right":"3px"})
	  	.attr("height","160");	  
  }    
  
  $leftDiv.append($a);
  $rightDiv.append($rightabel.prepend($rightInput));

  $parent.append($leftDiv).append($rightDiv);
  
  return $parent;
}

function addProducts(productsArray){
    var $productsContainer = $("#productsContainer").empty();

    $.each(productsArray, function(index, product){    	
    	var $singleProduct = getProductScript(product);                  
        $productsContainer.append($singleProduct);        
        if(product.productTitle === "Healthslate Tablet"){
        	$("#productTablet").empty().append($singleProduct);
        	$singleProduct.find("input").hide();
        }
    });
}
