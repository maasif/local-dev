if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//==========================================
/* Settings JS Model*/
var Settings = {
	
	//init settings functionality
	init: function(){
		var that = this;
		
		//some array constants to use inside Settings modal
		that.MEAL_TIME_HEADINGS = ["Breakfast", "Lunch", "Snack", "Dinner"];
		that.CARB_CEILINGS_HEADINGS = ["Breakfast", "Lunch", "Snack", "Dinner"];
		that.GLUCOSE_TIME_STRINGS = ["Before breakfast","2 hr after breakfast","Before lunch","2 hr after lunch","Before dinner","2 hr after dinner","Before bed","Before all meals","2 hr after all meals", "Before and 2 hr after all meals", "Specific time of day", "Once/Week", "Three times/Week"];

        that.isOpenOnceWeekDialog = true;
        that.isOpenThreeTimesWeekDialog = true;

        that.loadOnceWeekDay = "";
        that.loadThreeTimesWeekDays = "";

		that.FREQUENCY = {
			"Before breakfast": 1,			
			"2 hr after breakfast": 1,
			"Before lunch": 1,
			"2 hr after lunch": 1,
			"Before dinner": 1,
			"2 hr after dinner": 1,
			"Before bed": 1,
			"Before all meals": 3,
			"2 hr after all meals": 3,
			"Before and 2 hr after all meals": 9,
			"Specific time of day": $("#glucoseTimesSelected li").length
		};
				
		//cache page controls on load 
		that.cachePageConstants();
		that.submitted = true;
		
		//render time/range controls dynamically
		that.renderHTML(that.$mealTimeContainer, that.MEAL_TIME_HEADINGS, true);
		that.renderHTML(that.$carbCeilingsContainer, that.CARB_CEILINGS_HEADINGS, false);
		
		that.validationKeys = {
			"Breakfast": ["Lunch", "Snack", "Dinner"],
			"Lunch": ["Snack", "Dinner"],
			"Snack": ["Dinner"]
		};
		
		//bind button event listeners
		that.bindEventListener();
		
		//load settings from server and populate in controls
		that.loadSettings();
		
		that.bindSwitch();
		
		return that;
	},
	
	//cache page controls on load 
	cachePageConstants: function(){
		
		var that = this;

        that.$chkGlucoseTargetConsent = $("#chkGlucoseTargetConsent").off("change");
		that.$mealTimeContainer = $("#mealTimeContainer").empty();
		that.$carbCeilingsContainer = $("#carbCeilingsContainer").empty();
		that.$glucoseTime = $("#glucoseTime").timepicker();
		that.$medicationTime = $("#medicationTime");//.timepicker({'step': 15});
        that.$medicationTimeDialog = $("#medicationTimeDialog").timepicker({'step': 15});
        that.$ddOnceWeek = $("#ddOnceWeek").chosen();
		that.$btnSaveSettings = $("#btnSaveSettings").off("click");
		that.$txtMealTargets = $("#txtMealTargets");
		that.$formSettings = $("#formSettings");
		
		that.$glucoseTimesSelected = $("#glucoseTimesSelected").empty();
		that.$medicationTimesSelected = $("#medicationTimesSelected").empty();
		
		that.$txtGlucoseTimes = $("#txtGlucoseTimes");
		that.$txtTargetSource = $("#txtTargetSource");
		that.$txtHiddenTargetSource = $("#txtHiddenTargetSource");
		that.$txtMedicationTimes = $("#txtMedicationTimes");
		that.$changedByLabel = $("#changedByLabel");
		that.$txtReminder = $("#txtReminder");

		that.$txtMealIsSigned = $("#txtMealIsSigned");
		that.$mealProcessedSwitch = $("#mealProcessedSwitch");

		that.glucoseTimeArray = [];
		that.medicationTimeArray = [];
        that.medicationTimeObjects = [];
		that.chkArray = [];
		that.glucoseTimeArrayForServer = [];

        that.$btnAddOnceWeekDialog = $("#btnAddOnceWeekDialog");
        that.$btnAddThreeTimesWeekDialog = $("#btnAddThreeTimesWeekDialog");

		//multiple select of Glucose Time, and all the functionality when check/uncheck option
		that.$ddGlucoseTimes = $("#ddGlucoseTimes").multipleSelect({
										placeholder: "-- Select --", 
										selectAll: false,	
										isOpen: true,
								        keepOpen: true,
								        multiple: true,
								        multipleWidth: 175,
										onOpen: function() {
							                $(".ms-choice").css("backhground-color", "#fff !important");							                
							            },
										onClick: function(view) {							                
											console.log(view, view.label, view.value);
							                that.disableEnableGlucosTimeOptions(view);
							            }
							   });
		
		//on change of Glucose Time, do following
		that.$glucoseTime.on("changeTime", function(e) {
			
			var $this = $(this),
				thisVal = $this.val(),
				$error = $("#errMessage"),
				alertMessage = Object.create(AlertMessage).init();
			
			if($.inArray(thisVal, that.glucoseTimeArray) == -1){
				that.glucoseTimeArray.push(thisVal);
				that.addOrRemoveGlucoseTimes();		
				$this.val("");
				that.FREQUENCY["Specific time of day"] = that.glucoseTimeArray.length;
			}else{
				alertMessage.showAlertMessage($error, "You cannot add duplicate time." , alertMessage.ERROR);
			}			
		});

        $("#btnAddMedicationDialog").on("click", function(e){
            e.preventDefault();
            var $this = that.$medicationTimeDialog,
                thisVal = $this.val(),
                medicationTotalTimes = that.medicationTimeArray.length,
                $error = $("#errMessageMedication"),
                alertMessage = Object.create(AlertMessage).init(),
                $ddWeekly = $("#ddWeekly");

            if($.trim(thisVal).length == 0){
                alertMessage.showAlertMessage($error, "Please select time." , alertMessage.ERROR);
                return;
            }

            if($.trim($ddWeekly.val()).length == 0 && $ddWeekly.parent("label").is(":visible")){
                alertMessage.showAlertMessage($error, "Please select day of week." , alertMessage.ERROR);
                return;
            }

            if(medicationTotalTimes < 4){
                if($.inArray(thisVal, that.medicationTimeArray) == -1){

                    var $medsOccurrence = $(".meds-occurrence:checked"),
                        medObject = {
                         "frequency": $medsOccurrence.val(),
                         "aka": $medsOccurrence.data("aka"),
                         "time": thisVal,
                         "day": ""
                        };

                    if($.trim($ddWeekly.val()).length > 0 && $ddWeekly.parent("label").is(":visible")){
                        medObject.day = $ddWeekly.val();
                    }

                    that.medicationTimeObjects.push(medObject);
                    that.medicationTimeArray.push(thisVal);
                    var alreadyAdded = that.getAlreadyAddedMedications();
                    that.addOrRemoveMedicationTimes(alreadyAdded);
                    $("#medicationAddDialog").foundation("reveal", "close");
                }else{
                    alertMessage.showAlertMessage($error, "You cannot add duplicate time." , alertMessage.ERROR);
                }
            }else{
                alertMessage.showAlertMessage($error, "You cannot enter more than four medication reminders." , alertMessage.ERROR);
            }

            $this.val("");
        });

        $(".meds-occurrence").on("change", function(){

            var $lblWeekly = $("#lblWeekly").hide(),
                $ddWeekly = $("#ddWeekly"),
                v = $(this).data("aka");

            if(v == "Weekly"){
                $lblWeekly.show();
            }
        });

		//on change of Medication Reminders Time, do following
		that.$medicationTime.on("click", function(e) {
            $(".meds-occurrence:eq(0)").prop("checked", "checked").change();
            $("#ddWeekly").val("");
			$("#medicationAddDialog").foundation("reveal", "open");
		});

        //on change of checkbox remove/add disable div
        that.$chkGlucoseTargetConsent.on("change", function(){
             $("#divOverlayGlucose").toggle();
        });
	},
	
	getAlreadyAddedMedications: function(){
		return $("#medicationTimesSelected").find("input.med-remind-chk").map(function(obj, index){
			return this.checked;
		}).get();
	},
	
	//disable/enable glucose time options
	disableEnableGlucosTimeOptions: function(selected){
		var that = this,
			selectedArrays = that.$ddGlucoseTimes.multipleSelect("getSelects"),
			$specific = $("#specificTimeOfDay").hide(),
	    	$before = $("li.before label").removeClass("disabled").children("input").removeAttr("disabled").end(),
	    	$2Hours = $("li.2hour label").removeClass("disabled").children("input").removeAttr("disabled").end(),
	    	$all = $("li.all label").removeClass("disabled").children("input").removeAttr("disabled").end(),
            $threeTimesWeek = $("li.three-times-weeks label").removeClass("disabled").children("input").removeAttr("disabled").end(),
            $onceWeek = $("li.once-week label").removeClass("disabled").children("input").removeAttr("disabled").end();

        if(!selected || selected.value == "Once/Week"){
            $onceWeek.find("span.selected-days").remove();
        }

        if(!selected || selected.value == "Three times/Week"){
            $threeTimesWeek.find("span.selected-days").remove();
        }

		$.each(selectedArrays, function(index, obj){
			var valSelected = obj;

			if(valSelected == "Before all meals"){
	        	$before.addClass("disabled").children("input").attr("disabled", "disabled").removeAttr("checked");							                	
	        }
	        
	        if(valSelected == "2 hr after all meals"){
	        	$2Hours.addClass("disabled").children("input").attr("disabled", "disabled").removeAttr("checked");							                	
	        }
	        
	        if(valSelected == "Before and 2 hr after all meals"){
	        	$all.addClass("disabled").children("input").attr("disabled", "disabled").removeAttr("checked");              	
	        }

	        if(valSelected == "Specific time of day"){
	        	$specific.show();
	        }

            if(valSelected == "Three times/Week"){

                if(that.loadThreeTimesWeekDays){
                    $threeTimesWeek.append("<span title='"+that.loadThreeTimesWeekDays+"' class='selected-days'> ("+that.loadThreeTimesWeekDays+") </span>");
                    that.loadThreeTimesWeekDays = "";
                }

                if(that.isOpenThreeTimesWeekDialog && selected.value == "Three times/Week"){
                    that.$ddOnceWeek.val("").trigger("chosen:updated");
                    $("#threeTimesWeekDialog").foundation("reveal", "open");
                }

                $onceWeek.addClass("disabled").children("input").attr("disabled", "disabled").removeAttr("checked");
            }

            if(valSelected == "Once/Week"){
                if(that.loadOnceWeekDay){
                    $onceWeek.append("<span title='"+that.loadOnceWeekDay+"' class='selected-days'> ("+that.loadOnceWeekDay+") </span>");
                    that.loadOnceWeekDay = "";
                }

                if(that.isOpenOnceWeekDialog && selected.value == "Once/Week"){
                    $("#ddOnceWeekDays").val("");
                    $("#onceWeekDialog").foundation("reveal", "open");
                }

                $threeTimesWeek.addClass("disabled").children("input").attr("disabled", "disabled").removeAttr("checked");
            }
		});
	},
	
	//add/remove Glucose Times functionality
	addOrRemoveGlucoseTimes: function(addOrRemove){
		var that = this,
			html = "";
		
		that.$glucoseTimesSelected.empty();
		
		if(addOrRemove){			
			var index = that.glucoseTimeArray.indexOf(addOrRemove);
			if (index > -1) {
				that.glucoseTimeArray.splice(index, 1);
			}
			
			that.FREQUENCY["Specific time of day"] = that.glucoseTimeArray.length;
		}
		
		$.each(that.glucoseTimeArray, function(index, obj){			
			html += "<li><a href='#'><span class='time-text'>"+ obj + "</span><span class='remove-time right' title='Delete' >&#8211;</span></a></li>";
		});
		
		that.$glucoseTimesSelected.append(html).show();
		
		//on click listener of delete Glucose Time
		$("span.remove-time").off("click").on("click", function(e){
			e.preventDefault();			
			that.addOrRemoveGlucoseTimes($(this).prev("span.time-text").text());
			if(that.glucoseTimeArray.length == 0){				
				that.$glucoseTimesSelected.hide();
				that.$glucoseTime.val("");
			}
		});
	},
	
	//add/remove Medication Reminders functionality
	addOrRemoveMedicationTimes: function(addOrRemove){
		var that = this,
			html = "",
			checkedArray = [];
		
		if(addOrRemove && $.isArray(addOrRemove)){
			checkedArray = addOrRemove;
		}else{
			if(addOrRemove){						
				var index = that.medicationTimeArray.indexOf(addOrRemove);
				
				checkedArray = that.getAlreadyAddedMedications();
				
				if (index > -1) {
					that.medicationTimeArray.splice(index, 1);
                    that.medicationTimeObjects.splice(index, 1);
				}				
			}			
		}
		
		that.$medicationTimesSelected.empty();
		
		$.each(that.medicationTimeArray, function(index, obj){	
			
			var checked = "checked = 'checked' ";
			
			//if checked array found then
			if(that.chkArray && that.chkArray.length > 0){
				var fromArray = that.chkArray[index];
				
				//condition to go into even if it is false
				if(fromArray != undefined || fromArray != null){
					checked = (fromArray == 'true' || fromArray == true) ? "checked = 'checked' " : "";	
				}				
			}
			
			//if checked array found then
			if(checkedArray && checkedArray.length > 0){
				var fromArray = checkedArray[index];
				
				//condition to go into even if it is false
				if(fromArray != undefined || fromArray != null){
					checked = (fromArray == 'true' || fromArray == true) ? "checked = 'checked' " : "";	
				}				
			}

            var medObject = that.medicationTimeObjects[index];

            medObject.aka = (medObject.frequency == "Once/Day" || !medObject.day) ? "Daily" : "Weekly";

            var medsHtml = medObject.aka + " <span class='italic'> at </span> <span class='time-text'>"+ medObject.time + "</span>";

            if(medObject.day){
                medsHtml = medObject.aka + " (" + medObject.day + ")" + "<span class='italic'> at </span> <span class='time-text'>"+ medObject.time + "</span>";
            }

			html += "<li><a href='#' class='position-relative'>" + medsHtml +
                    "<div><span class='remind-me'>Should remind:&nbsp;</span><input id='switchReminder"+index+"' type='checkbox' " + checked + " class='large-chk med-remind-chk'></div>" +
                    "<div class='remove-time remove-med-time right-minus128' title='Delete'>&#8211;</div></a></li>";
		});
		
		that.$medicationTimesSelected.append(html).show();
		
		//on click listener of delete Medication Reminders Time
		$("div.remove-med-time").off("click").on("click", function(e){
			e.preventDefault();			
			that.addOrRemoveMedicationTimes($(this).parents("li").find("span.time-text").text());
			if(that.medicationTimeArray.length == 0){				
				that.$medicationTimesSelected.hide();
				that.$medicationTime.val("");
			}
		});
	},
	
	//render time/range controls dynamically
	renderHTML: function($container, arr, timeOrNot){
		var that = this,
			html = "";
		
		$.each(arr, function(index, obj){
			
			var $timepicker = $("<input/>", { "type":"text", "class":"settings-picker margin-bottom-0 time-"+obj.toLowerCase(), "placeholder": "-- Select --", "data-mealtype" : obj});
			
			if(timeOrNot){
				html += "<div class='row page-section-row'>" +
							"<div class='columns large-3'><label class='inline label-bold'>" + obj + "</div>" + 
							"<div class='columns large-9'><label class='inline'>" + $timepicker.wrap("<p/>").parent().html() + "</label></div>" +
						"</div>";
			}else{
				
				$timepicker = $("<input/>", { "type":"number", "min": "0", "class":"margin-bottom-0 number-fields carbs-"+obj.toLowerCase(), "step": "any" });
				
				html += "<div class='row page-section-row'>" +
							"<div class='columns large-3'><label class='inline label-bold'>" + obj + "</div>" + 
							"<div class='columns large-9'><ul class='inline-list margin-bottom-0'><li><label class='inline margin-bottom-0'>" + $timepicker.wrap("<p/>").parent().html() + "</label></li><li class='margin-left-5'><label class='inline align-label'>g</label></li></div>" +							
						"</div>";
			}								
		});
		
		$container.append(html).find(".page-section-row:last-child").addClass("last-row");
		
		$(".settings-picker:not(.excluded)").timepicker({ 'timeFormat': 'h:i A', 'step': 15 }).on("keypress keydown", function(evt){
			return false;
		}).on("changeTime", function(e){
			e.preventDefault();
			
			//on change of time validations functionality
			/*var $this = $(this),
				thisVal = $this.val(),
				mealType = $this.data("mealtype"),
				validations = that.validationKeys[mealType],
				lessGreater = " should be less than ";
			
			if(validations){
				
				var summary = [];
				
				$.each(validations, function(index, obj){
					
					var $c = $(".time-"+obj);
					
					if($c){
						
						var firstTime = thisVal,
							secondTime = $c.val(),						
							isTrue = that.compareTwoTimes(firstTime, secondTime);
						
						var singleObj = {
							"from" : mealType,
							"to": validations.join(", ")
						};
						
						if(isTrue == "LESS"){							
							lessGreater = " should be less than ";
							summary.push(singleObj);
						}
						
						if (isTrue == "GREATER"){								
							lessGreater = " should be greater than ";
							summary.push(singleObj);
						}
					}
				});
				
				that.submitted = true;
				$this.removeAttr("style");
				
				if(summary.length > 0){
					
					that.submitted = false;
					
					var zeorth = summary[0],
						alertMessage = Object.create(AlertMessage).init(),
						$error = $("#errMessage");
					
					$this.css("border", "1px solid red");
					alertMessage.showAlertMessage($error, zeorth.from + lessGreater + zeorth.to , alertMessage.ERROR);
										
					//$this.val($this.attr("data-defaultval"));
				}
			}*/		
		});
		
		$(".number-fields").on("keypress", function(evt){
			return that.onlyAllowNumbers($(this), evt);
		});		
	},
	
	//bind button event listners
	bindEventListener: function(){
		var that = this,					
			ajaxing = Object.create(Ajaxing),
			alertMessage = Object.create(AlertMessage).init(),
			$error = $("#errMessage");
		
		that.$btnSaveSettings.on("click", function(e){
            e.preventDefault();
			if(that.submitted){
				
				that.saveTargets();
				that.mapReminders();

                $("#txtPatientId").val(patId);

				//send via ajax call
				var dataToSend = that.$formSettings.serialize(),
		    		postRequestActions = {    			
		    			"loading": $("#loading"),
		    			"error": $("#errMessage"),
		    			"successCallBack": that.onSaveSettings
		    		};    	
	    	        	
				ajaxing.sendPostRequest("saveSettings.action", dataToSend, postRequestActions);
			}else{
				alertMessage.showAlertMessage($error, "Please remove an error in the form to save 'Settings'.", alertMessage.ERROR);				
			}					
		});

        that.$btnAddOnceWeekDialog.on("click", function(e){
            e.preventDefault();

            var $err = $("#errMessageOnceWeek"),
                $dd = $("#ddOnceWeekDays"),
                $onceWeek = $("li.once-week label").find("span.selected-days").remove().end();

            if($.trim($dd.val()).length > 0){
                $onceWeek.append("<span title='"+$dd.val()+"' class='selected-days'> ("+$dd.val()+") </span>");
                $("#onceWeekDialog").foundation("reveal", "close");
            }else{
                alertMessage.showAlertMessage($err, "Please select day.", alertMessage.ERROR);
            }
        });

        window.settingsObject = that;

        that.$btnAddThreeTimesWeekDialog.on("click", function(e){
            e.preventDefault();
            that.addThreeDays();
        });
	},

    addThreeDays: function(){
        var that = window.settingsObject,
            $err = $("#errMessageThreeTimesWeek"),
            $threeTimesWeek = $("li.three-times-weeks label").find("span.selected-days").remove().end(),
            alertMessage = Object.create(AlertMessage).init(),
            concatenateThreeDays = that.$ddOnceWeek.val();

        if(concatenateThreeDays && concatenateThreeDays.length > 0){
            if(concatenateThreeDays.length == 3){
                $threeTimesWeek.append("<span title='"+that.$ddOnceWeek.val()+"' class='selected-days'> ("+that.$ddOnceWeek.val()+") </span>");
                $("#threeTimesWeekDialog").foundation("reveal", "close");
            }else{
                alertMessage.showAlertMessage($err, "Please select three days.", alertMessage.ERROR);
            }
        }else{
            alertMessage.showAlertMessage($err, "Please select three days.", alertMessage.ERROR);
        }
    },

	mapReminders: function() {
		var that = this,
			remindersObject = {};
				
		$(".reminder-switch").each(function(index, swtch){			
			var $this = $(swtch);					
			remindersObject[$this.data("key")] = ($this.next("span.reminder-status").text() == "REMINDER") ? true: false;			
		});
		
		that.$txtReminder.val(JSON.stringify(remindersObject));
	},
		
	//populate mealTargets, glucoseTimes and medicationTimes for sending to server
	saveTargets: function(){
		var that = this;
		
		that.$txtMealTargets.val(JSON.stringify(that.getMealTargets()));
		
		//get selected options from multiple select
		that.glucoseTimeArrayForServer = that.$ddGlucoseTimes.multipleSelect("getSelects");				
		
		//if not visible then empty the array
		if(!$("#specificTimeOfDay").is(":visible")){
			that.glucoseTimeArray = [];
		}
		
		var modifiedGlucoseArray = [],				
			mergedArrayGlucoseTimes = that.glucoseTimeArray.concat(that.glucoseTimeArrayForServer);
		
		//creating glucose times array for sending to server
		$.each(mergedArrayGlucoseTimes, function(index, obj){			
			if(obj != "Specific time of day"){				
				var frequency = that.FREQUENCY[obj],
                    gObject = {
                        "time" : obj,
                        "minTarget": 0,
                        "maxTarget": 0,
                        "frequency": (frequency) ? frequency : 1
                    };

                if(obj == "Once/Week"){
                    gObject.days = $("#ddOnceWeekDays").val();
                }

                if(obj == "Three times/Week"){
                    var threeWeekdays = $("#ddOnceWeek").val();
                    if(threeWeekdays){
                        gObject.days = $("#ddOnceWeek").val().join(",");
                    }else{
                        var excludedBrackets = $.trim($("li.three-times-weeks span.selected-days").text());
                        excludedBrackets = excludedBrackets.substring(1, excludedBrackets.length-1);
                        gObject.days = excludedBrackets;
                    }
                }

				modifiedGlucoseArray.push(gObject);
			}
		});
		
		that.$txtGlucoseTimes.val(JSON.stringify(modifiedGlucoseArray));
		
		//ready medication reminders to send
		var $availableMedicationReminders = that.$medicationTimesSelected.children("li"),
			medicationTimeArrayForServer = [];
		
		if($availableMedicationReminders && $availableMedicationReminders.length > 0){
			$.each($availableMedicationReminders, function(index, medCtrl){
                var medObject = that.medicationTimeObjects[index];
				medicationTimeArrayForServer.push({ 
					"time" : medObject.time,
                    "frequency": medObject.frequency,
                    "day": medObject.day,
					"shouldReminded" : $(medCtrl).find("input.med-remind-chk").prop("checked")
				});
			});
		}
		
		that.$txtMedicationTimes.val(JSON.stringify(medicationTimeArrayForServer));

		//save meal processed signature switch value
		that.$txtMealIsSigned.val(that.$mealProcessedSwitch.find("a:not(.secondary)").text());
	},
	
	//getting meal targets wrapped in object array
	getMealTargets: function(){
		var that = this,	
			$meals = $("[data-mealtype]"),
			finalArray = [];
		
		$meals.each(function(index, obj){
			var $this = $(obj),				
				$carbControl = $(".carbs-"+$this.data("mealtype").toLowerCase()),				
				singleMealObject = {
					"mealTime" : $this.val(),
					"mealType": $this.data("mealtype"),
					"carbTarget": ""
				};
		
			if($carbControl){				
				singleMealObject.carbTarget = $carbControl.val();							
			}
			
			finalArray.push(singleMealObject);
		});	
		
		return finalArray;
	},
	
	//onSave settings response from server
	onSaveSettings: function(data){		
		var alertMessage = Object.create(AlertMessage).init(),
			ajaxing = Object.create(Ajaxing),
			$error = $("#errMessage");
		
    	if(data.STATUS == "SUCCESS"){
    		alertMessage.showAlertMessage($error, "Settings saved successfully.", alertMessage.SUCCESS);
			setTimeout(function(){
				window.location.reload(true);
			}, 500);
    	}else{    		
    		ajaxing.showErrorOrRedirect(data, $error, "Unable to save settings. Please try again later.");
    	}    	
	},
	
	//load settings from server and populate in controls
	loadSettings: function(){
		var that = this;		
		that.settingsLoadedFromServer = TARGET_LOADED_OBJECT;
		if(that.settingsLoadedFromServer){

			//rebind switch here to trigger click while loading data
			that.bindSwitch();
			
			var loadedKeys = Object.keys(that.settingsLoadedFromServer), 			
				mealTargets = that.settingsLoadedFromServer["mealTargets"],			
				reminder = that.settingsLoadedFromServer["reminder"],
				mealProcessed = MEAL_IS_SIGNED;
			
			that.populateFields(loadedKeys, that.settingsLoadedFromServer);

			//load meal processed signature switch
			if(mealProcessed){
				if(mealProcessed == "Yes"){
					that.$mealProcessedSwitch.find("a").eq(1).click();
				}
			}

			if(reminder) {			
				
				//loading reminders toggle data
				$(".reminder-switch").each(function(index, obj) {
					
					var $singleSwitch = $(obj),
						dataKey = $singleSwitch.data("key"), 					
						trueOrFalse = reminder[dataKey];
					
					if(trueOrFalse){						
						$singleSwitch.find("a").eq(1).click();
					}
				});
			}

			//loading meal targets into their corresponding time and carbs
			if(mealTargets && mealTargets.length > 0){
                $.each(mealTargets, function(index, mealTarget){
                      var mealType = mealTarget.mealType.toLowerCase(),
                          $this = $(".time-"+mealType),
                          $carbControl = $(".carbs-"+mealType);

                    $this.attr("data-defaultval", mealTarget.mealTime).val(mealTarget.mealTime);

                    if($carbControl){
                        $carbControl.attr("data-defaultval", mealTarget.carbTarget).val(mealTarget.carbTarget);
                    }
                });
			}
			
			var glucoseTarget = that.settingsLoadedFromServer["glucoseTargets"];
			if(glucoseTarget && glucoseTarget.length > 0){
				var $glucoseTarget = $("[data-mealtype]"),
					multiselectArray = [],
					$specific = $("#specificTimeOfDay").hide();
				
				that.glucoseTimeArray = [];
				$.each(glucoseTarget, function(index, obj){
					var timeString = obj.time,
                        $threeTimesWeek = $("li.three-times-weeks label"),
                        $onceWeek = $("li.once-week label");

                    if(timeString == "Once/Week"){
                        that.isOpenOnceWeekDialog = false;
                        that.loadOnceWeekDay = obj.days;
                        $onceWeek.append("<span title='"+obj.days+"' class='selected-days'> ("+obj.days+") </span>");
                    }

                    if(timeString == "Three times/Week"){
                        that.isOpenThreeTimesWeekDialog = false;
                        that.loadThreeTimesWeekDays = obj.days;
                        $threeTimesWeek.append("<span title='"+obj.days+"' class='selected-days'> ("+obj.days+") </span>");
                    }

					if(that.GLUCOSE_TIME_STRINGS.indexOf(timeString) > -1){
						multiselectArray.push(timeString);
					}else{							
						that.glucoseTimeArray.push(timeString);									
					}
				});	
				
				if(that.glucoseTimeArray.length > 0){
					multiselectArray.push("Specific time of day");
					$specific.show();
				}else{					
					//if not selected 'Specific time of day' then clears its array
					if(!$specific.is(":visible")){
						that.glucoseTimeArray = [];
					}					
				}
				
				that.addOrRemoveGlucoseTimes();
				that.$ddGlucoseTimes.multipleSelect("setSelects", multiselectArray);
				that.disableEnableGlucosTimeOptions();

                that.isOpenOnceWeekDialog = true;
                that.isOpenThreeTimesWeekDialog = true;
			}	
			
			var medicationsReminders = that.settingsLoadedFromServer["medTargets"];
			if(medicationsReminders && medicationsReminders.length > 0){
				var $medicationsTarget = $("[data-mealtype]");
				that.medicationTimeArray = [];
                that.medicationTimeObjects = [];
				that.chkArray = [];
                that.medicationTimeObjects = medicationsReminders;
				$.each(medicationsReminders, function(index, obj){
					that.medicationTimeArray.push(obj.time);	
					that.chkArray.push(obj.shouldReminded)
				});					
				that.addOrRemoveMedicationTimes();
			}
			
			that.$changedByLabel.show();
			that.$txtTargetSource.text(that.$txtHiddenTargetSource.val());
		}		
	},
	
	//populate in controls
	populateFields: function(keys, data){
		$.each(keys, function(index, key) {			
			var $el = $('[name="target.' + key + '"]'),
				elVal = data[key];
			if ($el.is('select')) {						
				if ($el.attr('multiple') == 'multiple') {
					var values = elVal; // it has the multiple values to set, separated by comma
					var valuesArray = values.split(',');
					$el.val(valuesArray);
				} else {					
					$el.attr("data-defaultval", elVal).val(elVal);
				}
			} else {
				var type = $el.attr('type');				
				switch (type) {
				case 'checkbox':
					break;
				case 'radio':					
					break;
				default:
					$el.attr("data-defaultval", elVal).val(elVal);					
				}
			}
		});
	},
	
	//to allow entering only number inputs
	onlyAllowNumbers : function(ctrl, evt){
	    var charCode = (evt.which) ? evt.which : event.keyCode,
			$el = $(ctrl);

		//disable typing zero in the beginning
		if ($el.val().length == 0 && charCode == 48){
			return false;
		}

		//disable typing alphabets
	    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}

	    return true;
	},
	
	//compares two times
	compareTwoTimes: function(firstTime, secondTime){
		
		var dateFirstString = new Date().toLocaleDateString() + " " + firstTime,
			dateSecondString = new Date().toLocaleDateString() + " " + secondTime,		
			firstDate = new Date(dateFirstString),
			secondDate = new Date(dateSecondString);
		
		if(firstDate > secondDate){
			return "LESS";
		} 
		
		if(secondDate > firstDate){
			//return "GREATER";
		}
		
		return "";
	},
	
	bindSwitch: function(){	  
    	var that = this;
    	
    	$(".reminder-switch").each(function(){
    		var $reminderSwitch = $(this), 
    			$switchBtns = $reminderSwitch.find("a");
    		
    		$switchBtns.off("click").on("click", function(e){
        		e.preventDefault();		        		
        		$switchBtns.removeClass("button-green").addClass("secondary");		        		
        		$(this).removeClass("secondary").addClass("button-green").blur();
        		
        		that.selectedId = $(this).data("id");
        		
        		var reminderStatus = $(this).data("text");
        		if(reminderStatus == "REMINDER"){
        			reminderStatus = "NO REMINDER";
        		}else{
					if($(this).data("text") == "Yes"){
						reminderStatus = "No";
					}else if($(this).data("text") == "No"){
						reminderStatus = "Yes";
					}else{
						reminderStatus = "REMINDER";
					}
        		}
        		
        		$reminderSwitch.next("span.reminder-status").text(reminderStatus);
        		        		
        	});
    	});
    }
};