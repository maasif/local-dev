var jsonToSend = {
		 "ILLUSTRATION": [],
		 "RECIPE": [],
		 "VIDEO": [],
		 "MEDICATION": []		 				
	},
	tutorialList = "",
	selectedTutorial = undefined,
	readableJson = {
		 "ILLUSTRATION": "Illustrations",
		 "RECIPE": "Recipes",
		 "VIDEO": "Videos",
		 "MEDICATION": "Medications"		 
	};

function addRemoveFromArray(arr, value){
	var index = arr.indexOf(value);				
	if (index > -1) {
		arr.splice(index, 1);
	}else{
		arr.push(value);
	}	
	
	return arr;
}	

function sortDataByKey(key) {  
	
    if(key === "") {
        key = "title";
    }
    
   //will sort by date
    if(key.toLowerCase().indexOf("date") > -1 || key.toLowerCase().indexOf("modified") > -1){
    	return function (a,b) {
    		return new Date(a[key]) - new Date(b[key]);
       };    	
    }
    
    //will sort alphabetically
    return function (a,b) {
    	 var aName = a[key].toLowerCase();
    	 var bName = b[key].toLowerCase(); 
    	 return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
    };
}

function bindSorting(){
	$("*[data-sort]").off("click").on("click", function(){				
		var sorted = $(this).attr("data-sorted"),
			sortType = $(this).data("sort");

		if(sorted === "asc"){
			sorted = "desc";
			tutorialList.reverse();			
		}else{			
			sorted = "asc";
			tutorialList.sort(sortDataByKey(sortType));
		}

		$(this).attr("title","order-"+sorted).find("img")
			   .removeClass("asc desc").addClass(sorted)
			   .attr({"src":"../resources/css/images/sort_"+sorted+".png", "title":sorted});

		loadTutorials(tutorialList);				
		$(this).attr("data-sorted",sorted);			
		return false;				
	});
}

function getJsonObjectKeys(jsonObject){
	
	return Object.keys(jsonObject);
}

function getKeyIdIndex(keys){
	var idKey = "";
		
	$.each(keys, function(index, item){
		
		if(item.toLowerCase().indexOf("id") > -1){
			idKey =  item;
			return false;
		}
	});
	
	return idKey;
}

function loadTutorialItemsToDroppedArea(selectedResources){
	
	var tutorialItems = selectedResources,
		tutorialItemsJson = {
			 "VIDEO": [],
			 "RECIPE": [],
			 "ILLUSTRATION": [],
			 "MEDICATION": []				
		};		
	
	if(tutorialItems.length > 0){
		
		$.each(tutorialItems, function(index, item){
			var json = tutorialItemsJson[item.type];					
			if(json !== undefined){	
				if ($.inArray(item, json) == -1) {										
					json.push(item);
			    }												
			}						
		});
		
		var jsonKeys = getJsonObjectKeys(tutorialItemsJson),
			$ul = $("#droppingGrid").empty();	
		
		$.each(jsonKeys, function(index, item){							
			var items = tutorialItemsJson[item],			
			    $ulDraggingArea = $("#ul-"+item);
			if(items !== undefined && items.length > 0){				
				$.each(items, function(index, item2){
					var $item = $ulDraggingArea.children("li.grid-row[data-id='"+item2.id+"'][data-type='"+item2.type+"']");
					if($item.length > 0){
						var $tempItem = $item.draggable('disable').off("click");						
						$ul.append($tempItem);	
						$item.find("i.icon-ok").hide();
						$item.find("i.icon-remove").show();
						$ulDraggingArea.children("li.grid-row[data-id='"+item2.id+"'][data-type='"+item2.type+"']").remove();
					}
				});
				
				showDroppingAreaItems();
			}														
		});			
	}
}

function showDroppingAreaItems(){
	var $p = $("#droppedText").hide(),
		$ul = $("#droppingGrid").show(),
		$btnSave = $("#btnSaveAsTutorial").show(),
		$droppedContainer = $("#dropped").css("min-height","650px"),
		$navPanel = $("div.nav-panel").css({"height":"100px", "line-height":"100px"});
			
	if($ul.children("li").length == 0){
		$p.show();
		$btnSave.hide();
		$droppedContainer.removeAttr("style");
		$navPanel.removeAttr("style");
	}
}

function setTickToSelected($div){
	var $this = $div,		
		$icon = $this.find("i.icon-ok");
		
	$this.find("i.icon-remove").hide();
	
	if($icon.is(":visible")){
		$icon.hide();
	}else{
		$icon.show();
	}
	
	$this.toggleClass("drag-selected");
}

function readyDataToSend(){
	var $ul = $("#droppingGrid");
		
	$ul.children("li").each(function(index, item){
		var $this = $(this),				
			assetType = $this.data("type"),
			arr = jsonToSend[assetType];
		
		jsonToSend[assetType] = addRemoveFromArray(arr, $this.data("id"));
	});		
}

function bindTickSelection(e){
	e.preventDefault();		
	setTickToSelected($(this));					
}

function loadData(json, assetType, $container, defaultImgSrc){
	
	var $ul = $("<ul/>", {"class":"item-grid dragged", "id" : "ul-"+assetType}),
		$ulContainer = $("<div/>", {"class": "hs-tabs-content div-"+assetType}),
		$searchContainer = $("<div/>", {"class":"search-container"}),
		$search = $("<input/>", {"type": "text", "class":"search-asset", "placeholder":"Search " + assetType.toLowerCase() + "..."}),
		$empty = $("<li/>", {"class":"no-result center","text": "No items found to display"}),
		$iconRemove = $("<i/>", {"class":"icon icon-remove glyphicon glyphicon-remove pull-right"}).hide(),
		$iconOk = $("<i/>", {"class":"icon icon-ok glyphicon glyphicon-ok pull-right"}).hide();
	
	var html = "";		
	
	$ul.append($empty);
	
	if(json !== undefined){
	
		$.each(json, function(index, item){											
			var title = item.title.toLowerCase();			
			if(assetType === "MEDICATION"){
				title = item.name;
				//title = extractMedicationName(title); 
			}			
			var	idKey = getKeyIdIndex(Object.keys(item));						
			html += "<li class='capitalize grid-row " + assetType + "' data-id='"+ item[idKey] + "' data-type='" + assetType + "'><div class='row'><div class='small-12 twelve columns asset-name'><span class='grid-image'><img src='"+defaultImgSrc+"' /></span>" + title + $iconRemove.wrap("<p/>").parent().html() + $iconOk.wrap("<p/>").parent().html() + "</div></div></li>";			
		});	
		
		$ul.append(html);
		
		$searchContainer.append($search);
		$ulContainer.append($searchContainer).append($ul);					
		$container.append($ulContainer);
		initDragging($("li.grid-row"));				
	}		
}
	
function bindItemDragging($item, $items){
	var $dropPlace = $("#dropped");

	$item.draggable({		
		appendTo: 'body',
		containment: $("#mainPage"),
		scroll: false,
		helper:"clone",			
		zIndex: 200,
		scroll: false,
		revert:"invalid",
		start: function(e, ui) {
            ui.helper.addClass("drag-selected");
            $(this).addClass("drag-selected");            
        },
		drag:function (e, ui) {				
			$items.addClass("fade");
			$(this).removeClass("fade").addClass("drag-start");
			$dropPlace.css({"border-color":"red", "background-color":"#fff8c1"});			
		},		
		stop:function (e, ui) {
			$items.removeClass("fade");
			$(this).removeClass("drag-start");
			$dropPlace.css({"border-color":"#ccc", "background-color":"#fff"});	
			$(this).removeClass("drag-selected");
		}
	}).dblclick(function() {
		addItemToDroppingArea($(this));
	});
	
	$item.find("i.icon-remove").off("click").on("click", function(){		
		removeItemFromDroppedArea($(this).parents("li"));
		return false;
	});
	
	return $item;
}

function initDragging($items){
	
	var $items = $("ul.dragged li.grid-row");
	
	$items.each(function(){
		var $item = $(this);
		bindItemDragging($item, $items);
		$item.off("click").on("click", bindTickSelection);		
	});
}

function addItemToDroppingArea($item){
	
	$("#droppingGrid").append($item);	
	$item.find("i.icon-ok").hide();	
	$item.find("i.icon-remove").show();	
	$item.draggable('disable');		
	
	showDroppingAreaItems();
}

function removeItemFromDroppedArea($item){
	var $tempItem = bindItemDragging($item),
		$doppedContainer = $("#droppingGrid"),
		$draggedContainer = $("#ul-"+$item.data("type"));
	
	$tempItem.draggable("enable").off("click").on("click", bindTickSelection);
	
	$draggedContainer.append($tempItem);
		
	$doppedContainer.children("li[data-id='"+$item.data("id")+"']").remove();
	
	$tempItem.find("i.icon-remove").hide();
	
	showDroppingAreaItems();
}

function isAnyItemSelected(){
	
	var isSelected = false,	
		$ul = $("#droppingGrid");
	
	if($ul.children("li").length > 0){
		isSelected = true;
	}
	
	return isSelected;
}

function refreshPage(){
	jsonToSend = {
			 "ILLUSTRATION": [],
			 "RECIPE": [],
			 "VIDEO": [],
			 "MEDICATION": []			 			
		};
	
	$("li.grid-row i.icon-ok").hide();
}

function bindSearching($tab){
	
	var $txtSearch = $tab.find("input.search-asset").val(""),
		$items = $tab.find("li.grid-row"),
		$empty = $tab.find("li.no-result").hide();
	
	initDragging($items);
	
	$txtSearch.off("keyup").on("keyup", function(e){
		var $thisVal = $(this).val();				
		var filter = $thisVal, count = 0;		    
	    $items.each(function(){		 			
			var toSearch = $(this).find(".asset-name").text();				
	        if (toSearch.search(new RegExp(filter, "i")) < 0) {		                
	            $(this).hide();		                			            
	        } else {		            	
	            $(this).show();	            
	            count++;	   		                            
	        }
	        
	        $empty.hide();
	        if(count == 0){
	        	$empty.show();
	        }
	    });			
	});
	
	$txtSearch.trigger("keyup");
}

function extractMedicationName(title){
	var htmlIndex = title.lastIndexOf("."),	
		medName = title.substring(0, htmlIndex),			
		splitted = medName.split("_");
	
	return (splitted[0] + " " + splitted[1]);
}

function removeHighlighted($accordionBtns, $accordionContent) {			     
	$accordionBtns.find("i").removeClass("glyphicon-chevron-up").addClass('glyphicon-chevron-down');				          
    $accordionContent.removeClass("active").children("div.content:visible").slideToggle("normal");	
}

function buildAccordion(itemsJson, makeHyperLinks, addPersonalContent){
	
	var tutorialItemsJson = {
			 "VIDEO": [],
			 "RECIPE": [],
			 "ILLUSTRATION": [],
			 "MEDICATION": []			 
		},				
		tutorialItems = itemsJson;
	
	//add accordion item Educator's content 
	if(addPersonalContent){
		readableJson.PERSONAL_CONTENT = "Educator's Content";
		tutorialItemsJson.PERSONAL_CONTENT = [];
	}
		
	$.each(tutorialItems, function(index, item){
		var json = tutorialItemsJson[item.type];					
		if(json !== undefined){	
			if ($.inArray(item, json) == -1) {										
				json.push(item);
		    }												
		}						
	});
		
	var jsonKeys = getJsonObjectKeys(tutorialItemsJson),
		$accordion = $("#accordion").empty();
	
	$.each(jsonKeys, function(index, item){
		var accordionHeaderText = readableJson[item],
			$container = $("<dd/>"),
			$accordionButton = $("<a/>", {"href": "#panel"+index, "html":accordionHeaderText + "<i class='indicator glyphicon glyphicon-chevron-down pull-right'></i>"}),											
			$content = $("<div/>", {"id": "panel"+index, "class":"content"}).empty(),
			$ul = $("<ul/>", {"class":"tutorial-item"}).empty(),				
			items = tutorialItemsJson[item];
			
		if(items !== undefined && items.length > 0){
			
			$.each(items, function(index, item2){
				var title = item2.title,								
					id = (item2.tutorialItemId === undefined) ? item2.id : item2.tutorialItemId,				
					cssClass = "";
				
				// other than PersonalContent and Illustration capitalize the text
				if(accordionHeaderText !== "Educator's Content" && item !== "PERSONAL_CONTENT" && item !== "ILLUSTRATION"){										
					title = item2.title.toLowerCase();
					cssClass = "capitalize";							
				}
				
				var	$li = $("<li/>", {"class": cssClass, "data-id": id});
				
				if(makeHyperLinks){							
					var resourceLink = ((item2.link1 == undefined)? item2.path : item2.link1),
						$a = $("<a/>", {"href": resourceLink, "html": title, "target": "_blank", "class":"a-link"});
					
					//in case of personal content open resources in our custom viewer
					if(addPersonalContent && accordionHeaderText === "Educator's Content" && item === "PERSONAL_CONTENT"){						
						$a.on("click", function(e){
							openViewer($(this), resourceLink);
						});
					}					
					
					$li.append($a);
				}else{
					$li.text(title);
				}
				
				$ul.append($li);
			});
			
			$container.append($accordionButton).append($content.append($ul));
			
			$accordion.append($container);	
		}else{
			var $li = $("<li/>", {"text":"No items found to display", "class":"center empty"});
			$ul.append($li);			
		}																
	});	
	
	bindAccordion(addPersonalContent);									
}	

function bindAccordion(shouldExpand){
	var $accordion = $(".accordion"),
	    $accordionContent = $accordion.children("dd"),
		$accordionBtns = $accordionContent.children("a");
						
	$accordionContent.on("click", "a:eq(0)", function (event) {
        var $parent = $(this).parent();
	    
        if($parent.hasClass('active')){
	      
          $parent.removeClass("active");			          
          removeHighlighted($accordionBtns, $accordionContent);
	      
		}else{
		  
		  removeHighlighted($accordionBtns, $accordionContent);					  
		  $parent.addClass("active");					  
		  $parent.find(".content").slideToggle("normal");
		  
          $(this).find("i").removeClass("glyphicon-chevron-down").addClass('glyphicon-chevron-up');			          
	    }
    });
		
	if(!shouldExpand){
		$accordionBtns.eq(0).click();
	}
}

function openTutorialItemsDialog(){
	$("#tutorialItemsDialogOpen").click();
	$("#tutorialName").text(selectedTutorial.title);	
	buildAccordion(selectedTutorial.tutorialItems, true, false);
}

function bindGridItemsOnClick(){
	$("li.grid-row").off("click").on("click", function(){
		var index = $(this).data("object-index");					
		selectedTutorial = tutorialList[index];		
		openTutorialItemsDialog();
	});				
}