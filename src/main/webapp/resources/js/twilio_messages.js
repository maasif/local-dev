if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//bind when click on document outside of message dialog then close it
/*$(document).on("click", function (e){
    var container = $("#twilioMessageDialog");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
    	
    	//clearing refresh timer instance, once the dialog is closed
    	clearInterval(TWILIO_REFRESH_TIMER);

		if(!$("#txtTime").val()){
			container.foundation("reveal", "close");
		}
    }
});*/

// refresh timer instance
var TWILIO_REFRESH_TIMER,
	SHOULD_SHOW_LOADING = false;

//==========================================
/* TwilioMessages JS Model*/
var TwilioMessages = {
	
	init: function(patId, patPhoneNumber){
		var that = this;
		
		that.patientId = patId;
		that.phoneNumber = patPhoneNumber;
		that.shouldShowLoading = true;
		window.twilioObject = that;
		
		//alert message instance
		that.ALERT_MESSAGES = Object.create(AlertMessage).init();
		
		//ajaxing instance
		that.AJAXING = Object.create(Ajaxing);
		
		//cache page controls
		that.cachePageConstants();
		
		//set phone number to hidden text input
		that.$txtMessageTo.val(that.phoneNumber);
		
		//bind auto grow plugin to textarea
		//that.bindTextareaAutoGrow(that.$txtMessage);
		
		//bind event listener
		that.bindEventListener();
		
		SHOULD_SHOW_LOADING = true;
		
		//on load get sms from server
		that.getSmsListFromServer();
				
		//auto-refresh timer from server
		that.initRefreshTimer();

		that.$ddSmsTemplates.fillDropdown(SMS_TEMPLATES, "smsTemplateId", "template");

		return that;
	},
	
	//cache page controls
	cachePageConstants: function(){
		
		var that = this;
		
		that.$btnSendMessage = $("#btnSendMessage");
		that.$txtMessage = $("#txtMessage").val("").change();
		that.$txtMessageTo = $("#txtMessageTo");
		that.$threadedViewEmpty = $(".thread-empty").show();
		that.$dialogArea = $("#dialogArea");
		that.$threadViewList = $("#threadViewList").empty().hide();		
		that.$errMessage = $("#errMessageTwilio");
		that.$loading = $("#loadingTwilio");
		that.$shadow = $("#topShadow").hide();
		that.$btnRefreshMessages = $("#btnRefreshMessages").off("click");
		that.$ddSmsTemplates = $("#ddSmsTemplates");
		that.$twilioMessageDialog = $("#twilioMessageDialog");

		that.$txtTime = $("#txtTime").timepicker({ 'timeFormat': 'h:i A' }).on("keypress keydown", function(evt){
			return false;
		});

		that.$ddSmsContainer = $("#ddSmsContainer").removeClass("small-8").addClass("small-10");
		that.$ddTimeContainer = $("#ddTimeContainer").removeClass("small-2").hide();

		//when scroll dialog area div show shadow on the above, just to let user know there is data upside
		that.$dialogArea.on("scroll", function(){
		  var y = $(this).scrollTop();
		  if( y > 0 ){
			  that.$shadow.show();
		  } else {
			  that.$shadow.hide();
		  }
		});

		that.$ddSmsTemplates.on("change", function(){
			var $this = $(this).children(":selected"),
				smsTemplate = $this.data("json");

			that.$ddTimeContainer.removeClass("small-2").hide();
			that.$ddSmsContainer.addClass("small-10");
			that.$txtTime.val("");

			if(smsTemplate && smsTemplate.flag){
				that.$ddTimeContainer.addClass("small-2").show();
				that.$ddSmsContainer.removeClass("small-10").addClass("small-8");
			}
		});
	},
		
	//bind event listener
	bindEventListener: function(){
		var that = window.twilioObject;
		
		that.$btnSendMessage.off("click").on("click", function(e){
			e.preventDefault();
			
			that.shouldShowLoading = true;
			window.twilioObject = that;

			var	$selectedSms = $("#ddSmsTemplates option:selected"),
				smsTemplate = $selectedSms.data("json"),
				messageText = $selectedSms.text(),
				hyperlink = "";

			if(smsTemplate){
				hyperlink = (smsTemplate.flag) ? "" : smsTemplate.templateHyperlink;
			}

			if(that.$txtTime.is(":visible") && that.$txtTime.val() == ""){
				that.ALERT_MESSAGES.showAlertMessage(that.$errMessage, "Please select time to send.", that.ALERT_MESSAGES.ERROR);
				return;
			}

			if($.trim(messageText).length > 0 && $selectedSms.val() != ""){

				if(that.$txtTime.is(":visible")){
					messageText = messageText.replace("x:00 PM", that.$txtTime.val());
				}

				var dataToSend = { "messageTo" : that.phoneNumber, "messageBody" : messageText, "templateHyperlink": hyperlink, "patientId": that.patientId },
	    		postRequestActions = {    			
	    			"loading": that.$loading,
	    			"error": that.$errMessage,
	    			"successCallBack": that.onSentMessage
	    		};    	
    	        	
				that.AJAXING.sendPostRequest("sendMessageToPhone.action", dataToSend, postRequestActions);								
			}else{
				that.ALERT_MESSAGES.showAlertMessage(that.$errMessage, "Please select message to send.", that.ALERT_MESSAGES.ERROR);
			}						    		
		});
		
		that.$btnRefreshMessages.on("click", function(e){
			e.preventDefault();
			
			SHOULD_SHOW_LOADING = true;						
			that.getSmsListFromServer();
		});
	},
	
	//on success message sent, do following
	onSentMessage: function(data){
		var that = window.twilioObject;
    	
    	if(data.STATUS == "SUCCESS"){    		
    		that.$txtMessage.val("").change();
    		SHOULD_SHOW_LOADING = true;
    		//that.ALERT_MESSAGES.showAlertMessage(that.$errMessage, "Message sent successfully.", that.ALERT_MESSAGES.SUCCESS);
    		that.getSmsListFromServer();
			that.$ddSmsTemplates.fillDropdown(SMS_TEMPLATES, "smsTemplateId", "template").change();
    	}else{    		
    		//that.AJAXING.showErrorOrRedirect(data, that.$errMessage, "Unable to send message. Please try again later.");
    	}
    	
    	return that;
	},
	
	//bind auto grow plugin to textarea
	bindTextareaAutoGrow: function ($txtArea) {
        $txtArea.on("change keyup", function () {
            var value = $(this).val();
            if ($.trim(value).length > 0) {
                $(this).autosize().trigger("autosize.resize");
            } else {
                $(this).trigger("autosize.destroy");
            }
        }).trigger("change");
    },
    
    //gets sms list from server
    getSmsListFromServer: function(){
    	var that = window.twilioObject,
			$loadingInner = "";
    	    	
    	//if shouldShowLoading flag true then assign
    	if(SHOULD_SHOW_LOADING){
    		$loadingInner = that.$loading;
    	}
    	
    	var dataToSend = { "messageTo" : that.phoneNumber, "patientId": that.patientId },
			postRequestActions = {    			
				"loading": $loadingInner,
				"error": that.$errMessage,
				"successCallBack": that.onGetMessagesList
			};    	
        	
    	that.AJAXING.sendPostRequest("getAllMessages.action", dataToSend, postRequestActions); 
    },
    
    //on success get messages list, do following
    onGetMessagesList: function(data){
		var that = window.twilioObject;
    	
    	if(data){
    		that.buildThreadedView(data);
    	}else{    		    		    		    
    		if(data){    			
    			that.AJAXING.showErrorOrRedirect(data, that.$errMessage, "Unable to get messages. Please try again later.");
    		}    		
    	}
    	
    	return that;
	},
	
	//build threaded view of messages
	buildThreadedView: function(data){
		var that = window.twilioObject,
			html = "",
			outboundArray = ["outbound-api", "outbound", "outbound-reply"];
		
		that.$threadedViewEmpty.show();
		
		if(data && data.length > 0){
			
			that.$threadedViewEmpty.hide();
			
			$.each(data, function(index, twilio){
				
				var messageBubble = (outboundArray.indexOf(twilio.direction) > -1) ? {bubble: "bubble bubble-teal", direction: "left"} : {bubble: "bubble bubble-orange", direction: "right"};
				
				html += "<li class='"+ messageBubble.direction +"'><div class='bubble-wrapper'><div class='"+ messageBubble.bubble + "' >" + twilio.body + "</div><span class='time'>"+ new Date(twilio.dateSent).toString("M/d/yyyy h:mm tt") +"</span></div></li>";
			});
			
			that.$threadViewList.empty().append(html).show();
			
			that.scrollToBottom(that.$dialogArea);
		}		
	},
	
	//scroll to bottom of passing div
	scrollToBottom: function ($div) {
        $div.animate({ scrollTop: $div[0].scrollHeight }, 70);
    },
    
    //starts auto refresh timer for getting list from server, after each 1min
    initRefreshTimer: function(){
    	var that = window.twilioObject;
		SHOULD_SHOW_LOADING = false;
		clearInterval(TWILIO_REFRESH_TIMER);
    	TWILIO_REFRESH_TIMER = setInterval(that.getSmsListFromServer, 120000); //2mins
    }
};

/* A plugin to fill dropdown with passing array*/
$.fn.fillDropdown = function(values, valKey, textKey){
	var $dropdown = $(this).empty();
	$.each(values, function(val, obj) {
		var $option = $("<option/>", {"value": obj[valKey], "text": obj[textKey], "data-json": JSON.stringify(obj)});
		$dropdown.append($option);
	});

	$dropdown.children(":first").before("<option value='' selected='selected' disabled>(select)</option>");

	return $dropdown;
};