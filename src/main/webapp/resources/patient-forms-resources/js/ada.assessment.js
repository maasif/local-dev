if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//==========================================
/* AdaAssessment JS Model*/
var AdaAssessment  = {

	//initialize page functionality
    init: function(){
        var that = this;        
                
        that.reassessmentTopicList = [{"id": "1", "value": "Knowledge of diabetes disease process"}, {"id": "2", "value": "Nutritional Management"}, {"id": "3", "value": "Understanding of Physical Activity"}, {"id": "4", "value": "Medication Use"}, {"id": "5", "value": "Monitoring and using results"}, {"id": "6", "value": "Preventing Acute Complications"}, {"id": "7", "value": "Preventing Chronic Complications"}, {"id": "8", "value": "Psychosocial Adjustment"}, {"id": "9", "value": "Behavior Change Strategies"}, {"id": "10", "value": "Risk Reduction Strategies"}];
        that.reassessmentRatings = [{"value": "1", "text": "1 = needs instruction"}, {"value": "2", "text": "2 = needs review"}, {"value": "3", "text": "3 = comprehends key points"}, {"value": "4", "text": "4 = demonstrates competency"}, {"value": "N/A", "text": "N/A = not assessed"}];
        
        that.goalAchievementList = [{"id": "1", "value": "Healthy Eating"}, {"id": "2", "value": "Being Active"}, {"id": "3", "value": "Using Medications Safety"}, {"id": "4", "value": "Monitoring"}, {"id": "5", "value": "Reducing Risks: acute complication"}, {"id": "6", "value": "Reducing Risks: chronic complication"}, {"id": "7", "value": "Healthy Coping"}];
        that.goalAchievementRatings = [{"value": "0", "text": "0% = never"}, {"value": "25", "text": "25% = ocassionally"}, {"value": "50", "text": "50% = half the time"}, {"value": "75", "text": "75% = most of the time"}, {"value": "100", "text": "100% = always"}];
        
        that.$clinicianSignature = $("#divClinicianSignature").signaturePad({drawOnly:true});
        
        that.fillGrid("assessmentGrid");
        that.fillGrid("goalAchievementGrid");
        
        return that;
    },
    
    //fills Assessment/Goal Achievement Grid with static data
    fillGrid: function(container){
    	var that = this,
    		$container = $("#"+container).children("li:gt(1)").remove().end(),
    		listToPopulate = that.reassessmentTopicList,
    		ratings = that.reassessmentRatings,
    		html = "";
    		
    	if(container == "goalAchievementGrid"){
    		listToPopulate = that.goalAchievementList;
    		ratings = that.goalAchievementRatings;
    	}
    	
    	$.each(listToPopulate, function(index, item){
    		html += "<li class='grid-row'><div class='row'>" +
    				"<div class='medium-4 four columns'><label class='inline label-black'>" + item.value  + "</label></div>" +
    				"<div class='medium-4 four columns'>" + that.buildDropdown(ratings) + "</div>" +
    				"<div class='medium-4 four columns'><textarea class='margin-bottom-0'></textarea></div></div></li>";
    	});
    	
    	return $container.append(html);
    },
    
    //load dropdown with passing array
    buildDropdown: function(ratings){
    	var that = this,
    		$select = $("<select/>").fillDropdown(ratings, "value", "text");
    	
    	return $select.wrap("<p/>").parent().html();
    }
};

/* A plugin to fill dropdown with passing array*/
$.fn.fillDropdown = function(values, valKey, textKey){
    var $dropdown = $(this).empty();
    $.each(values, function(val, obj) {               
      var $option = $("<option/>", {"value": obj[valKey], "text": obj[textKey]});
      $dropdown.append($option);
    });
    
    //add new option with " --- Select --- " text at first in the dropdown
    var $optionEmpty = $("<option/>", {"value": "-1", "text": " --- Select --- ", "default": "default", "selected": "selected", "disabled": "disabled"});    
    $dropdown.children("option:first").before($optionEmpty);
    
    return $dropdown;
};
