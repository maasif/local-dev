if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//==========================================
/* Goal JS Model*/
var Goal  = {

	//initialize page functionality
    init: function(goalCategories, goalDetails, url){
        var that = this;
        
        /* load some constants in Goal object */
        that.url = url;
       
        that.atPage = 1;         
        that.json = goalCategories;
        that.loadData = goalDetails;
        that.currentPage = 1;
        that.selectedCategory = {};
        
        //alert message instance
		that.ALERT_MESSAGES = Object.create(AlertMessage).init();
		
		//ajaxing instance
		that.AJAXING = Object.create(Ajaxing);
		
        that.patientResourcesFolderPath = that.url+"resources/patient-forms-resources/";
        that.checkedImgPath = that.patientResourcesFolderPath + "css/images/checked.png";
        that.uncheckedImgPath = that.patientResourcesFolderPath + "css/images/unchecked.png";             
               
        that.halfCalendarFrequencyList = [{"value":"day", "text":"day"}, {"value":"week", "text":"week"}];
        that.calendarFrequencyList = [{"value":"hour", "text":"hour"},{"value":"day", "text":"day"}, {"value":"week", "text":"week"}, {"value":"month", "text":"month"}, {"value":"year", "text":"year"}];
        
        that.pageHeadings = {
        	"page1" : "",
        	"page2" : "Reaching my goal",
        	"page3" : "My Action Plan",
        	"page4" : "My Challenges",
        	"page5" : "Overcoming My Challenge",
        	"page6" : "Rewards"
        };
    
        /* calling functions */
        that.cachePageControls();
        that.renderScales();           
        that.loadMyGoals(); 
		that.loadActionPlans();	
		that.loadActionPlansParameters();
        that.bindEventListeners();
        
        //load formdata if opens in editing mode
        that.loadFormData();   
        that.bindSwitch();
    },
    
    //cache DOM objects at first load
    cachePageControls: function(){
    	
    	var that = this;
    	
    	that.$totalPages = $(".pages");
    	that.redirectOrNot = false;
    	that.filledFormId = -1;
    	 
    	that.$goalScale = $("#importantGoal");
        that.$actionPlanScale = $("#actionPlan");
        
        that.$ddGoalCategories = $("#ddGoalCategories");
        that.$ddMyGoals = $("#ddMyGoals");
        that.$ddActionPlans = $("#ddActionPlans");
        
        that.$ddFrequencyHowOften = $("#ddFrequencyHowOften");
		that.$ddCalendarFrequency = $("#ddCalendarFrequency");
		that.$ddMeal = $("#ddMeal");  
		that.$rcdResultLabel = $("#rcdResults");
		
		that.$btnNext = $("#btnNext");
		that.$btnPrev = $("#btnPrev");
		that.$btnSave = $("#btnSave");
		that.$btnCancel = $("#btnCancel");
		that.$btnSkip = $("#btnSkip");

		that.$btnNextLi = that.$btnNext.parents("li");
		that.$btnPrevLi = that.$btnPrev.parents("li");
		that.$btnSaveLi = that.$btnSave.parents("li");
		that.$btnCancelLi = that.$btnCancel.parents("li");
		
		that.$page1 = $("#page1");
		that.$page2 = $("#page2");
				
		that.$pageHeadingText = $("#pageHeadingText");
    },
    
    scrollPageToTop: function () {
        $("body,html").animate({
            scrollTop: 0
        }, 70);
    },
    
  //'Important' and 'Confident' scales html building
    getScalesHTML: function(className){
        var that = this,
        	$ul = $("<ul/>", {"class":"button-group scales create-scales"}).empty(),
        	html = "";

        for (var i = 1; i <= 5; i++) {                
        	
        	var notVeryText = "";
        	
        	if(i == 1){
        		notVeryText = "<label class='inline align-center label-align-left scale-text'>Not <br/>"+className+"</label>";
        	}else if(i == 5){
        		notVeryText = "<label class='inline align-center label-align-left scale-text'>Very <br/>"+className+"</label>";
        	}
        	
            html += "<li class='position-relative' data-scale='"+i+"'>" + notVeryText +
            		"<label class='inline'> <img src='"+that.url+"resources/patient-forms-resources/css/images/unchecked.png' " +
       				"class='"+className+"' data-ischecked='false' /><span>"+i+"</span></label></li>";
        }

        //html = "<li><label class='inline align-center label-align-left scale-text'>Not <br/>"+className+"</label></li>" + html + "<li><label class='inline align-center label-align-right scale-text'>Very <br/>"+className+"</label></li>";
        
        return $ul.append(html);
    },
    
    //render 'Important' and 'Confident' scales html into DOM
    renderScales: function(){
        var that = this,
        	$uncheckedGoals = $("#importantGoal img"),
            $uncheckedActionPlans = $("#actionPlan img");

        $uncheckedGoals.on("click", function(){  
        	
        	var scale = $(this).parents("li").data("scale");
        	$("#txtImportant").val(scale);
        	that.checkUncheckScale($(this), $uncheckedGoals, scale);            
        	
        	//show prompt here
        	if(scale < 4){
        		$("#confirmationImportantGoal").foundation("reveal", "open");
        	}        	
        });

        $uncheckedActionPlans.on("click", function(){        	
        	var scale = $(this).parents("li").data("scale");        	
        	$("#txtConfident").val(scale);
        	that.checkUncheckScale($(this), $uncheckedActionPlans, scale);            
        });
        
        /*that.$ddGoalCategories.fillDropdown(that.json, "goalCategoryId", "name").ddslick({                       
            imagePosition: "left",            
            onSelected: function (data) {
            	that.selectedCategory = data.selectedData;
            	$("#txtGoalCategory").val(that.selectedCategory.value);
            	that.loadMyGoals();
            }
        });*/
        
        that.$ddGoalCategories.fillDropdown(that.json, "goalCategoryId", "name");
    },
   
    //load values in MyGoals dropdown
    loadMyGoals: function(){
    	
    	var that = this,    	
        	goalCatId = that.$ddGoalCategories.val();//that.selectedCategory.value;
        
    	if(goalCatId){
    		that.$ddMyGoals.fillDropdown( that.getGoalByGoalCategoryId(goalCatId), "goalId", "name" );                                    		
    	}else{
    		that.$ddMyGoals.append("<option value='' selected='selected' disabled>(select)</option>");
    	}
    },
    
    //load values in ActionPlanse dropdown
    loadActionPlans: function(){
    	var that = this,	    	
	    	goalId = that.$ddMyGoals.val();
	    
    	that.$ddActionPlans.fillDropdown( that.getActionPlansByGoalId(goalId), "actionPlanId", "name", true); 
    },
    
    //based on selection in ActionPlan dropdown call child dropdowns(how often, meal etc)
    loadActionPlansParameters: function(){
    	var that = this;	    		    	    	
    	//that.showHideActionPlanParameters(that.$ddActionPlans.children("option:selected").data("json"));    	
    },
       
    //to change checked/unchecked image
    checkUncheckScale: function($item, $items, scale){

        /*var that = this;
        
        $items.attr({"src": that.uncheckedImgPath, "data-ischecked": false}); 
        
        $.each($items, function(index, img){
        	if(index < scale){
        		$(img).attr({"src": that.checkedImgPath, "data-ischecked": true});
        	}
        });  
        */
    	
        var that = this,
			src = $item.attr("src");
	            
	    $items.attr({"src": that.uncheckedImgPath, "data-ischecked": false}); 
	    
	    if(src === that.uncheckedImgPath){
	        $item.attr({"src": that.checkedImgPath, "data-ischecked": true});
	    }else{
	    	$item.attr({"src": that.uncheckedImgPath, "data-ischecked": false});            
	    }
    },
        
    //add/remove 'Other' textbox
    addRemoveOtherInputBox: function($parent, $container){     	
    	var category = $parent.children("option:selected").text().toLowerCase();    	
    	$container.hide();
    	
        if(category === "create my own"){
        	$container.show();
        }
    },
    
    //getting list of Goals by goalCategoryId
    getGoalByGoalCategoryId: function(goalCatId){
    	var that = this,
    		goalsArray = [];
    	
    	$.each(that.json, function(index, obj){    		
    		if(obj.goalCategoryId == goalCatId){    			
    			goalsArray = obj.goals;
    			return false;
    		}
    	});
    	
    	return goalsArray;
    },
    
    //getting list of ActionPlans by goalId
    getActionPlansByGoalId: function(goalId){
    	var that = this,
    		actionPlansArray = [];
    	
    	$.each(that.json, function(index, obj){
    		var goals = obj.goals;    		
    		if(goals){
    			$.each(goals, function(index, goal){
    				if(goal.goalId == goalId){
    					actionPlansArray = goal.actionPlans;
    					return false;
    				}
    			});
    		}    		
    	});
    	
    	return actionPlansArray;
    },
    
    validateDropdowns: function(ddList){
    	
    	var ddArray = [];
    	
    	$.each(ddList, function(index, dd){
    		var ddValue = $(dd).val();
    		
    		if(!ddValue){
    			ddArray.push($(dd));
    		}
    	});
    	
    	return ddArray;
    },
    
    validateForm: function(pageNumber){
    	
    	var that = this,
    		validated = {
	    		isValidate: true,
	    		$control: {},
	    		reason: ""
	    	};
    	
    	switch(pageNumber){
    	case 1:    		
    		
    		var $ddList = $("#ddGoalCategories,#ddMyGoals"),
				isValidatedDD = that.validateDropdowns($ddList);
		
			if(isValidatedDD.length == 0){
				validated.isValidate = that.validateScale("importantGoal");
	    		validated.$control = that.$goalScale;
	    		validated.reason = "Please choose 'How important is this to me?' ";    		    			
			}else{
				validated.isValidate = (isValidatedDD.length == 0) ? true : false;
				validated.$control = isValidatedDD;
				validated.reason = "Please choose values from dropdowns";
			}			
    		    		
    		break;
    	case 3:
    		
    		var $ddList = $("#ddActionPlans,#ddActionPlanFrequency,#ddCalendarFrequency"),
    			isValidatedDD = that.validateDropdowns($ddList);
    		
    		if(isValidatedDD.length == 0){
    			validated.isValidate = that.validateScale("actionPlan");
    			validated.$control = that.$actionPlanScale; 
    			validated.reason = "Please choose 'How often do I expect to stick to my Action Plan?'";    		    			
    		}else{
    			validated.isValidate = (isValidatedDD.length == 0) ? true : false;
    			validated.$control = isValidatedDD;
    			validated.reason = "Please choose values from dropdowns";
    		}
    		
    		break;
    	case 4:
    		var $ctrl = $("#txtStickFromActionPlan");
    		validated.isValidate = ($.trim($ctrl.val())) ? true: false;
    		validated.$control = $ctrl;
    		validated.reason = "Please fill the field";    		
    		break;
    	case 5:
    		var $ctrlOvercome = $("#txtOvercomeChallenge");
			validated.isValidate = ($.trim($ctrlOvercome.val())) ? true: false;
			validated.$control = $ctrlOvercome;
			validated.reason = "Please fill the field";
    		break;
    	/*case 6:
    		var $ctrlOvercome = $("#txtMyReward");
			validated.isValidate = ($.trim($ctrlOvercome.val())) ? true: false;
			validated.$control = $ctrlOvercome;
			validated.reason = "Please fill the field";
    		break;*/
    	default:    		
    		break;
    	}
    	
    	return validated;
    },
    
    getSelectedValueText: function($dd, value){
    	
    	return $dd.children("option:selected").text();
    },
    
    //binding event listeners
    bindEventListeners: function(){
    	var that = this;
    	
    	that.$ddGoalCategories.off("change").on("change", function(){     		
            that.loadMyGoals();
            
            $(this).addClass("cat-dd");
            
            var txtSelected = that.getSelectedValueText(that.$ddGoalCategories),            
            	catSelected = CATEGORY_COLOR[txtSelected];
            
            $("#catImage").show().attr("src", catSelected.image);
            
            that.$ddMyGoals.trigger("change");
        });
    	
    	that.$ddMyGoals.off("change").on("change", function(){               
        	that.loadActionPlans(); 
        	that.addRemoveOtherInputBox(that.$ddMyGoals, $("#sectionOtherGoals"));
        	that.$ddActionPlans.trigger("change");
        });
    	
    	that.$ddActionPlans.off("change").on("change", function(){                       	
    		that.loadActionPlansParameters();    		
    		that.addRemoveOtherInputBox(that.$ddActionPlans, $("#sectionOtherActionPlans"));        	
        });
    	
    	that.$btnNext.on("click", function(e){                
            e.preventDefault();              
            //validate             
            var validated = that.validateForm(that.currentPage);            
            if(validated.isValidate){
            	that.nextStep();  
            }else{
            	var alert = Object.create(AlertMessage).init(),            	
            		$controlArrays = validated.$control;
            	
            	if($controlArrays && $.isArray($controlArrays)){
            		$.each($controlArrays, function(index, dd){
            			$(dd).transition("shake");  
            		});          		
            	}else{
            		validated.$control.transition("shake");    
            	}
            	
            	alert.showAlertMessage($("#errMessage"), validated.reason, alert.ERROR);
            }   
        });

    	that.$btnPrev.on("click", function(e){                
            e.preventDefault();
            that.previousStep();
        });
		
    	that.$btnSave.on("click", function(e){
        	e.preventDefault();
        	
        	//redirect to goals listing page
        	that.redirectOrNot = true;    
        	
        	var validated = that.validateForm(that.currentPage);            
            if(validated.isValidate){
            	that.saveGoalForm();  
            }else{
            	var alert = Object.create(AlertMessage).init();
            	validated.$control.transition("shake");
            	alert.showAlertMessage($("#errMessage"), validated.reason, alert.ERROR);
            }             
        });

        that.$btnSkip.on("click", function(e){
            e.preventDefault();
            $(this).blur();
            that.currentPage++;
            that.loadStep(true, true);
        });
    },
                
    //on loading form set checked/unchecked images in Scales
    setSelectedScale: function(ctrl, valToSelect){
    	var that = this,
			$parent = $("div[data-classname='"+ctrl+"']");
    	
    	if($parent && valToSelect){
    		var $item = $parent.children("ul.scales").children("li[data-scale='"+valToSelect+"']");
    		if($item){    			
    			$item.find("img").attr({"src": that.checkedImgPath, "data-ischecked": true});
    		}
    	}
    },
    
    //validates atleast one is selected in Scales
    validateScale: function(ctrl){
    	var that = this,
    		$items = $("#"+ctrl).children("li[data-scale]");
    	
    	return ($items.find("img[data-ischecked='true']").length > 0);
    },
            
    nextStep: function() {

		var that = this,
			totalPages = that.$totalPages.length;
		
		that.currentPage++;
		if (that.currentPage >= totalPages) {
			that.currentPage = totalPages;
		}

		that.loadStep(true);
	},
	
    previousStep: function () {
    	var that = this;
    	
		if (that.currentPage <= 2) {
			that.$btnPrevLi.hide();
			that.$btnCancelLi.show();
		}

		that.loadStep(false);
	},
	    
    loadStep: function(isNextPrevious, isSkipped) {
		var that = this,
			previousPage = that.currentPage,
			totalPages = that.$totalPages.length,
			actionPlanScale = $("#txtConfident").val();

		that.scrollPageToTop();
		
        that.$btnSaveLi.hide();
        that.$btnCancelLi.hide();
        that.$btnPrevLi.show();
        that.$btnNextLi.show();

        //skip my challenge and overcome challenge section if scale is less that 4
		if( (actionPlanScale && actionPlanScale < 4) || isSkipped){
			if(isNextPrevious == true){	
				if(that.currentPage > 3){
                    if(isSkipped){
                        that.currentPage += 1;
                    }else{
                        that.currentPage += 2;
                    }
				}
			}else{				
				if(that.currentPage > 3){
					that.currentPage -=2;					
				}
			}
		}
		
		if (isNextPrevious == true) {
            previousPage = previousPage - 1;
		} else if (isNextPrevious == false) {
			that.currentPage = that.currentPage - 1;		
		}

		//set section/page heading
		var pageTxtKey = "page" + that.currentPage;     
		that.$pageHeadingText.text(that.pageHeadings[pageTxtKey]);
		
		if (that.currentPage <= 1) {
			that.$btnPrevLi.hide();
			that.$btnCancelLi.show();
		}
		
		if (that.currentPage >= totalPages) {			
            that.$btnNextLi.hide();
            that.$btnPrevLi.show();
            that.$btnSaveLi.show();
		}
		
		$(".button").blur();
		
		that.showHide("page" + previousPage, "page" + that.currentPage);			
	},
    
	showHide: function(divToHide, divToShow){

	  $("#"+divToHide).hide();
	  $("#"+divToShow).show();
	},

    //hide/show (how often, meals etc) dropdowns based on selection in ActionPlans dropdown
    showHideActionPlanParameters: function(json){
    	
    	var that = this,
    		jsonObj = JSON.parse(JSON.stringify(json));
    	
    	that.toggleControlVisibility(that.$ddFrequencyHowOften, false);
    	that.toggleControlVisibility(that.$ddCalendarFrequency, false);
    	that.toggleControlVisibility(that.$ddMeal, false);
    	that.toggleControlVisibility(that.$rcdResultLabel, false);
    	
    	if(jsonObj.showFrequencyTimes){
    		that.toggleControlVisibility(that.$ddFrequencyHowOften, true);    		
    	}
    	
    	if(jsonObj.showFullFrequencyCalendar == -1){    		
    		that.$ddCalendarFrequency.fillDropdown(that.halfCalendarFrequencyList, "value", "text");
    		that.toggleControlVisibility(that.$ddCalendarFrequency, true);
    	}else if(jsonObj.showFullFrequencyCalendar == 1){    		
    		that.$ddCalendarFrequency.fillDropdown(that.calendarFrequencyList, "value", "text");
    		that.toggleControlVisibility(that.$ddCalendarFrequency, true);    		    		
    	}
    	
    	if(jsonObj.showMeals){
    		that.toggleControlVisibility(that.$ddMeal, true);    		
    	}
    	
    	if(jsonObj.showRecordLabel){
    		that.toggleControlVisibility(that.$rcdResultLabel, true);
    	}
    },
    
    //hide/show control visibility
    toggleControlVisibility: function($ctrl, showHide){
    	
    	var $parent = $ctrl.parents("li");
    	
    	if(showHide){
    		$parent.show();
    	}else{
    		$parent.hide();
    	}
    },
    
    //saving goal form via ajax
    saveGoalForm: function(){
    	var that = this,
    		ajaxing = Object.create(Ajaxing),
    		alertMessage = Object.create(AlertMessage).init(),
    		$error = $("#errMessage"),
    		isScaleValidated = that.validateScale("actionPlan");
    		//isScaleValidated = that.validateScale("confidentSlider");
    	
    	//setting goalObject instance to window
    	window.goalObj = that;
    	
    	if(isScaleValidated){
    		
    		var fillFormId = that.filledFormId,
    			actionName = "saveForm.action?formType=GOAL";
    		
    		//if greater than '0' it means to update the form
    		if(fillFormId > 0){
    			actionName = "saveForm.action?formType=GOAL&filledFormId="+fillFormId;
    		}
    		
        	//ajax parimeters    	
        	var dataToSend = { "goalHabitsData" : JSON.stringify($("#goalForm").serializeObject()), "patientId": $("#txtPatientId").val() },
        		postRequestActions = {    			
        			"loading": $("#loading"),
        			"error": $("#errMessage"),
        			"successCallBack": that.onSaveGoalForm
        		};    	
        	        	
        	ajaxing.sendPostRequest(actionName, dataToSend, postRequestActions);
        	
    	}else{
    		
    		var messageSummary = [];
    		
    		if(!isScaleValidated){
    			messageSummary.push("Please choose 'How confident am I that I can keep to my action plan'");
    			$("#confidentSlider").parents(".medium-7").transition("shake");    			
    		}
    		
    		alertMessage.showAlertMessage($error, messageSummary.join("<br/>"), alertMessage.ERROR);
    	}    	    	
    },
    
    //on saving goal form further functionality to perform
    onSaveGoalForm: function(data){
    	var that = this;
    	    	
    	if(data.STATUS == "SUCCESS"){

            //call here to android method to sync goal from server
            try{
                Android.syncGoalPinnedMessage();
            }catch(err){ }

    		if(window.goalObj.redirectOrNot){
    			window.location = "goalsHabitsListing.action?AUTHORIZATION="+$("#txtAuthorization").val();
    		}  		
    	}else{
    		var ajaxing = Object.create(Ajaxing);
    		ajaxing.showErrorOrRedirect(data, $("#errMessage"), "Unable to save goal. Please try again later.");
    	}
    	
    	return that;
    },
    
    //loads form data into respective fields
    loadFormData: function(){
    	var that = this;
    	that.filledFormId = -1;    	
    	if(that.loadData){
    		
    		var formUtil = Object.create(FormsUtil);
    		formUtil.populateFormData(that.loadData);  
    		
    		//check scale checkbox
    		//that.checkUncheckScale($("#importantGoal img"), $("#txtImportant").val());
    		//that.checkUncheckScale($("#actionPlan img"), $("#txtConfident").val());
    		    		
    		//manually trigger dropdown change to select required value in linking dropdown
    		that.$ddGoalCategories.trigger("change");
    		
    		//call it after when dropdown load its values, then select in it
    		that.refreshDropdowns();
    		
    		setTimeout(function(){
    			
    			//manually trigger dropdown change to select required value in linking dropdown
        		that.$ddMyGoals.trigger("change");
        		
        		//call it after when dropdown load its values, then select in it
        		that.refreshDropdowns();
        		
        		//manually trigger dropdown change to select required value in linking dropdown
        		that.$ddActionPlans.trigger("change");
        		
        		//call it after when dropdown load its values, then select in it
        		that.refreshDropdowns();
        		
    		}, 50);       		
    	}
    },
    
    //refresh dropdowns after dynamically values inserted in dropdowns then select them
    refreshDropdowns: function(){
    	var that = this;    	
    	$.each(that.loadData, function(name, val) {
			var $el = $('[name="' + this.name + '"]');			
			if ($el && $el.is('select')) {										
				$el.val(this.value);				
			}			
			
			//setting filled form id while editing goal form
			if(this.name == "filledFormId"){
				that.filledFormId = this.value;
			}
			
			//'Other' fields that are hidden, once they are shown render data in them if available
			if(this.name == "actionPlanOther" || this.name == "myGoalsOther"){
				$el.val(this.value);
			}
		});
    },
    
    bindSwitch: function(){	 
    	
    	var that = this,
    		$reminderSwitch = $(".reminder-switch"),
    		$switchBtns = $reminderSwitch.find("a");
    	
    	$switchBtns.on("click", function(e){
    		e.preventDefault();		        		
    		$switchBtns.removeClass("button-green").addClass("secondary");		        		
    		$(this).removeClass("secondary").addClass("button-green").blur();
    		
    		var reminderStatus = $(this).data("text");
    		if(reminderStatus == "REMINDER"){
    			reminderStatus = "NO REMINDER";
    		}else{
    			reminderStatus = "REMINDER";
    		}
    		    		
    		$("#txtReminder").val(reminderStatus);
    		
    		$reminderSwitch.next("span.reminder-status").text(reminderStatus);    		    
    	});    	
    },
    
    loadReminderSwitch: function(isOn){
    	var that = this,
	    	$reminderSwitch = $(".reminder-switch"),
			$switchBtns = $reminderSwitch.find("a");
	    	
    	if(isOn){
    		that.isLoaded = false;
    		$switchBtns.eq(1).click();
    	}
    }  
};

//==========================================
/* A plugin to fill dropdown with passing array*/
$.fn.fillDropdown = function(values, valKey, textKey, setJson){
    var $dropdown = $(this).empty();
    $.each(values, function(val, obj) {      
      var json = "";
      if(setJson){
    	  json = JSON.stringify(obj);
      }  
      var $option = $("<option/>", {"value": obj[valKey], "text": obj[textKey], "data-json": json});
      
      if(valKey == "goalCategoryId"){
    	  var categoryText = obj[textKey],
    	  	  category = CATEGORY_COLOR[$.trim(categoryText)];
    	  
    	  if(category){
    		  $option = $("<option/>", {"value": obj[valKey], "text": obj[textKey], "data-imageSrc": category.image, "data-description": categoryText});
    	  }
      }
      
      $dropdown.append($option);
    });
        
    //if(valKey == "actionPlanId" || valKey == "actionPlanId"){
    	$dropdown.children(":first").before("<option value='' selected='selected' disabled>(select)</option>");
    //}
    
    return $dropdown;
};

//==========================================
/* A plugin to convert form into name/value pairs*/
$.fn.serializeObject = function(){
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
    	
    	//special case for these two to prevent saving if they aren't visible
    	if(this.name == "meals" || this.name == "calendarFrequency"){
    		var $el = $('[name="' + this.name + '"]'),
    			$elParent = $el.parents("li");
    		
    		//if the element is hidden then dont add in saving goal form json object
    		if($elParent.css("display") == "none"){    			
        		return;	
    		}    		
    	}
    	
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};