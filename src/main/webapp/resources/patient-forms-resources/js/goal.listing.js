(function(){

    var app = angular.module("Habits_Goals_Listing", []);
	
	//service to share data between controllers
	app.service("GoalDetailService", function($rootScope){
		this.goalDetail = GOAL_DETAILS; //GOAL_DETAILS is declared in goals_listing.jsp
		this.selectedCatName = ""; 	
		this.getGoalDetail = function(){
			return this.goalDetail;
		}
	});
	
	//rendering side nav goal categories and set selected category
	app.controller("GoalCategoriesController", function($scope, GoalDetailService){
		$scope.service = GoalDetailService;
		var that = this,
			ajaxing = Object.create(Ajaxing);
		
		that.goalDetailsList = $scope.service.goalDetail;
		this.categories = GOAL_CATEGORIES; //GOAL_CATEGORIES is declared in goals_listing.jsp		
		this.selected = 0; 	
		
		//for setting ng-model to select dropdown and set default 'All' in dropdown
		$scope.ddCategories = this.categories[0].goalCategoryId;
		
		this.onDropDownChange = function($this){				
			$scope.catCtrl.getGoalDetailsByCategoryId($this.ddCategories);
		};
		
		this.setSelected = function(index, catId){
			this.selected = index; 					
			$scope.catCtrl.getGoalDetailsByCategoryId(catId);
		}; 	  				
		
		this.getGoalDetailsByCategoryId = function(catId){	        
        	var dataToSend = {"goalCategory": catId},
	    		postRequestActions = {    			
	    			"loading": $("#loading"),
	    			"error": $("#errMessage"),
	    			"successCallBack": $scope.catCtrl.onLoadDetails
	    		};
    	        	
    		ajaxing.sendPostRequest("goalsByCategory.action", dataToSend, postRequestActions);
        };
		
		this.onLoadDetails = function(result){						
			that.goalDetailsList = result; 		
			$scope.$apply();
			$scope.catCtrl.bindRatingPlugin();			
		};
								
		this.openGoalForm = function(detail) {			
			if(detail){ 									
				var formId = detail.formId; 						 						
				window.location = "goalSummary.action?filledFormId="+formId;	
			}		    
		};
		
		this.bindRatingPlugin = function(){			
			loadRating(that.goalDetailsList);	 					
		};
	});
	
	//Habits controller
	app.controller("HabitsController", function($scope){
		var that = this;
		
		that.habitDetailsList = HABIT_DETAILS;
		this.openHabitForm = function(habit) {			
			if(habit){ 									
				var formId = habit.formId; 						 						
				window.location = "createHabit.action?formType=HABIT&filledFormId="+formId;	
			}		    
		};
	});
	
})();