if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//==========================================
/* GoalProgress JS Model*/
var GoalProgress = {

	//initialize page functionality
    init: function(goalProgress, actionPlans, url){
        var that = this;              
        
        that.goalProgress = goalProgress[0];
        that.actionPlans = actionPlans;        
        that.url = url;
        that.selectedId = -1;
        that.isLoaded = false;

        if(GOAL_RATINGS && GOAL_RATINGS.length > 0){
            $("#btnViewPreviousRatings").removeClass("hide");
        }

        //alert message instance
		that.ALERT_MESSAGES = Object.create(AlertMessage).init();
		
		//ajaxing instance
		that.AJAXING = Object.create(Ajaxing);
        
        that.CATEGORY_COLOR = {
    		"Healthy Eating": {color: "healthy-eating", image : (that.url + "resources/css/images/goal_icons/eating.png"), text: "HEALTHY"},
    		"Physical Activity": {color: "physical-activity", image : (that.url + "resources/css/images/goal_icons/physical-activity.png"), text: "ACTIVITY"},
    		"Medications & Health Mgmt": {color: "medication-monitoring", image : (that.url + "resources/css/images/goal_icons/medication-monitoring.png"), text: "MEDS"},
    		"Money, Work and Travel": {color: "work-money", image : (that.url + "resources/css/images/goal_icons/work.png"), text: "WORK"},
    		"Stress and Sleep": {color: "stress-sleep", image : (that.url + "resources/css/images/goal_icons/stress.png"), text: "STRESS"},
    		"Family & Relationships": {color: "family-relationship", image : (that.url + "resources/css/images/goal_icons/family.png"), text: "FAMILY"} 
        };
        
        that.patientResourcesFolderPath = that.url+"resources/patient-forms-resources/";
        that.checkedImgPath = that.patientResourcesFolderPath + "css/images/checked.png";
        that.uncheckedImgPath = that.patientResourcesFolderPath + "css/images/unchecked.png";
        
        window.goalProgressObject = that;
        
        //dialog delete GOAL when press 'Yes', do following
        $("#btnDeleteYesGoal").on("click", function(){
        	that.deleteGoal();
        	return false;
        });
        
       //dialog delete GOAL when press 'Yes', do following
        $("#btnDeleteGoal").on("click", function(){
        	that.selectedId = that.goalProgress.filledFormId;
        	$("#confirmationGoalDialog").foundation("reveal", "open");
        	return false;
        });

        $("#btnViewPreviousRatings").on("click", function(e){
            e.preventDefault();
            $("#goalRatingDialog").foundation("reveal", "open");
        });

        var $uncheckedGoals = $("#goalRatingList").find("img");
        
        //that.checkUncheckScale($uncheckedGoals, that.goalProgress.rating);
        
        $uncheckedGoals.on("click", function(){          	
        	var scale = $(this).parents("li").data("scale");        
        	that.checkUncheckScale($uncheckedGoals, scale); 
        	$("#txtGoalRating").val(scale);
        });

        that.bindSwitch();
        that.loadData();  
        
        return that;
    },
    
    loadData: function(){
    	var that = this,    	    
    		categoryColor = that.CATEGORY_COLOR[$.trim(that.goalProgress.categoryName)],    	    	
    		$ddActionPlans = $("#ddActionPlans").fillDropdown( that.actionPlans, "actionPlanId", "name", false),    	
    		goalDetails = that.buildGoalDetailsArray(that.goalProgress.formDataList);
    	
    	$("#divCategoryOuter").addClass(categoryColor.color);
    	
    	$("#divCategoryName").text(categoryColor.text);
    	
    	$("#imgCategory").attr("src", categoryColor.image);
    	
    	$("#lblGoalName").text(that.goalProgress.goalName);
        $("#h3GoalName").text(that.goalProgress.goalName + " rating history");
    	$("#lblCreatedDate").text("CREATED " + that.goalProgress.createdDate);
    	
    	$("#ddHowOften").val(goalDetails.planFrequency);
    	$("#ddDaysWeeks").val(goalDetails.calendarFrequency);
    	
    	$("#txtMyChallenges").val(goalDetails.myChallenge);
    	$("#txtOvercomingChallenges").val(goalDetails.overcomeChallenge);
    	$("#txtMyReward").val(goalDetails.myReward);    	
    	
    	$ddActionPlans.val(goalDetails.actionPlan);   
    	
    	that.isLoaded = true;
    	that.loadReminderSwitch(that.goalProgress.shouldRemind);
        that.loadGoalRatingHistory();
    },

    loadGoalRatingHistory: function(){
       var that = this,
            $ul = $("#dialogGoalRatingList").empty(),
            html = "";

        if(GOAL_RATINGS && GOAL_RATINGS.length > 0){

            $.each(GOAL_RATINGS, function(index, goalRating){

                html += "<li><label class='inline margin-bottom-0'><span class='font-bold'>Rated On: </span>"+ goalRating.ratedOn + "</label><div class='grey-section-data'> <ul class='large-block-grid-5 small-block-grid-5 scales goal-progress-rating' data-goalrating='"+goalRating.rating+"'>"
                + "<li class='position-relative'><label class='inline'><img src='"+that.url+"resources/patient-forms-resources/css/images/unchecked.png' /><span>1</span></label></li>"
                + "<li class='position-relative'><label class='inline'><img src='"+that.url+"resources/patient-forms-resources/css/images/unchecked.png' /><span>2</span></label></li>"
                + "<li class='position-relative'><label class='inline'><img src='"+that.url+"resources/patient-forms-resources/css/images/unchecked.png' /><span>3</span></label></li>"
                + "<li class='position-relative'><label class='inline'><img src='"+that.url+"resources/patient-forms-resources/css/images/unchecked.png' /><span>4</span></label></li>"
                + "<li class='position-relative'><label class='inline'><img src='"+that.url+"resources/patient-forms-resources/css/images/unchecked.png' /><span>5</span></label></li>"
                + "</ul></div></li>";
            });

            $ul.append(html);

            var $scales = $("[data-goalrating]");
            that.fillRating($scales, "goalrating");
        }
    },

    fillRating: function ($scales, key){
        var that = this;

        $scales.each(function(index, scale){
            var rating = $(scale).data(key),
                $children = $(scale).children("li");

            $children.each(function(index, child){

                if(index < rating){
                    $(child).find("img").attr("src", that.url + "resources/patient-forms-resources/css/images/checked.png");
                }
            });
        });
    },

    buildGoalDetailsArray: function(dataList){
    	var goalDetails = {};
    	    
		//converting fieldname and fieldValue to single goalDetailsObject
    	$.each(dataList, function(index, fd){	
			var fieldName = fd.fieldName;													
			goalDetails[fieldName] = fd.fieldValue;							        				
		});	
    	
    	return goalDetails;
    },

    saveRating: function(rating){
    	
    	var that = window.goalProgressObject,
    		dataToSend = { "formId" : that.selectedId, "rating": rating },
			postRequestActions = {    							
				"successCallBack": that.onSuccessSaveRating
			};    	
        	
		that.AJAXING.sendPostRequest("saveRating.action", dataToSend, postRequestActions);
    },
    
    onSuccessSaveRating: function(data){
    	var that = window.goalProgressObject;
    	
    	if(data.STATUS == "SUCCESS"){    		
    		//that.ALERT_MESSAGES.showAlertMessage($("#errMessage"), "Goal rating saved successfully.", that.ALERT_MESSAGES.SUCCESS);
    	}else{    		
    		//that.AJAXING.showErrorOrRedirect(data, $("#errMessage"), "Unable to save rating. Please try again later.");
    	}
    },
        
    deleteGoal: function(){
    	
    	var that = window.goalProgressObject,
    		dataToSend = { "filledFormId" : that.selectedId, "patientId" : $("#txtPatientId").val() },
			postRequestActions = {    			
				"loading": $("#loading"),
				"error": $("#errMessage"),
				"successCallBack": that.onSuccessDeleteGoal
			};    	
        	
		that.AJAXING.sendPostRequest("deleteGoal.action", dataToSend, postRequestActions);
    },
    
    onSuccessDeleteGoal: function(data){ 
    	var that = window.goalProgressObject;
    	
    	if(data.STATUS == "SUCCESS"){    		
    		$("#confirmationGoalDialog").foundation("reveal", "close");

            //call here to android method to sync goal from server
            try{
                Android.syncGoalPinnedMessage();
            }catch(err){ }

    		window.location = "goalsHabitsListing.action?AUTHORIZATION="+$("#txtAuthorization").val();
    	}else{    		
    		that.AJAXING.showErrorOrRedirect(data, $("#errMessage"), "Unable to delete Goal. Please try again later.");
    	}
    },

    saveReminder: function(reminderText){
    	
    	var that = window.goalProgressObject,
    		dataToSend = { "formId" : that.selectedId, "remindString" : reminderText },
			postRequestActions = {    							
				"successCallBack": that.onSuccessSaveReminder
			};    	
        	
		that.AJAXING.sendPostRequest("saveReminder.action", dataToSend, postRequestActions);
    },
    
    onSuccessSaveReminder: function(data){ 
    	var that = window.goalProgressObject;
    	
    	if(data.STATUS == "SUCCESS"){    		
    		//that.ALERT_MESSAGES.showAlertMessage($("#errMessage"), "Goal rating saved successfully.", that.ALERT_MESSAGES.SUCCESS);
    	}else{    		
    		//that.AJAXING.showErrorOrRedirect(data, $("#errMessage"), "Unable to delete Goal. Please try again later.");
    	}
    },
    
    //to change checked/unchecked image
    checkUncheckScale: function($items, scale){

        var that = this;
        
        $items.attr({"src": that.uncheckedImgPath, "data-ischecked": false}); 
        
        $.each($items, function(index, img){
        	if(index < scale){
        		$(img).attr({"src": that.checkedImgPath, "data-ischecked": true});
        	}
        });        
    },
    
    bindSwitch: function(){	 
    	
    	var that = this,
    		$reminderSwitch = $(".reminder-switch"),
    		$switchBtns = $reminderSwitch.find("a");
    	
    	$switchBtns.on("click", function(e){
    		e.preventDefault();		        		
    		$switchBtns.removeClass("button-green").addClass("secondary");		        		
    		$(this).removeClass("secondary").addClass("button-green").blur();
    		
    		that.selectedId = that.goalProgress.filledFormId;
    		
    		var reminderStatus = $(this).data("text");
    		if(reminderStatus == "REMINDER"){
    			reminderStatus = "NO REMINDER";
    		}else{
    			reminderStatus = "REMINDER";
    		}
    		 
    		$("#txtReminder").val(reminderStatus);
    		
    		$reminderSwitch.next("span.reminder-status").text(reminderStatus);
    		
    		if(that.isLoaded){
    			//that.saveReminder(reminderStatus);    		    			
    		}
    		that.isLoaded = true;
    	});    	
    },
    
    loadReminderSwitch: function(isOn){
    	var that = this,
	    	$reminderSwitch = $(".reminder-switch"),
			$switchBtns = $reminderSwitch.find("a");
	    	
    	if(isOn){
    		that.isLoaded = false;
    		$switchBtns.eq(1).click();
    	}
    }  
};
    
//==========================================
/* A plugin to fill dropdown with passing array*/
$.fn.fillDropdown = function(values, valKey, textKey, setJson){
    var $dropdown = $(this).empty();
    $.each(values, function(val, obj) {      
      var json = "";
      if(setJson){
    	  json = JSON.stringify(obj);
      }      
      var $option = $("<option/>", {"value": obj[valKey], "text": obj[textKey], "data-json": json});
      $dropdown.append($option);
    });
    
    return $dropdown;
};


(function(){
	
	var app = angular.module("GoalProgressModule", []);
	
	app.controller("GoalProgressController", ["$scope", "$http", function($scope, $http){        		
		var goalProgressess = (GOAL_PROGRESS) ? angular.fromJson(GOAL_PROGRESS) : [],
			singleLine = {},
			goalProgress = goalProgressess[0];
		
		var fromJson = goalProgress.formDataList;
		
		//convert jsonArray Object to single object
		angular.forEach(fromJson, function(obj, value){        			
			singleLine[obj.fieldName] = obj.fieldValue;        			
		});        	
		
		//storing in page level object
		$scope.goal = singleLine; 

		if($scope.goal.actionPlan != 'create my own'){
			$scope.goal.other = "";
		}

        $scope.actionPlanOnChange = function(){
            var $dd = $("#ddActionPlans"),
                $other = $("#lblOther").addClass("ng-hide"),
                selectedText = $dd.children("option:selected").text().toLowerCase();

            if(selectedText.toLowerCase() == "create my own"){
                $other.removeClass("ng-hide").css("display", "block");
            }
        }

        setTimeout(function () {
            var selectedText = $("#ddActionPlans").children("option:selected").text().toLowerCase();
            if (selectedText.toLowerCase() != "create my own") {
                $scope.goal.actionPlanOthers = "";
                $scope.actionPlanOnChange();
            }
        }, 10);

        //storing for showing required fields label when user submits
		$scope.submitted = false;

		//submit form function
		$scope.submitForm = function(goal) {
			
			$scope.submitted = true;
			
			//if form is valid then process
			if($scope.goalForm.$valid){
				
				var fillFormId = goalProgress.filledFormId,
        			actionName = "saveForm.action?formType=GOAL",
        			ajaxing = Object.create(Ajaxing),
        			$loading = $("#loading").show();
    		
        		//if greater than '0' it means to update the form
        		if(fillFormId > 0){
        			actionName = "saveForm.action?formType=GOAL&filledFormId="+fillFormId;
        		}
        		
        		goal.formType = "GOAL";
        		goal.REMINDER = $(".reminder-status").text();
        		goal.goalRating = $("#goalRatingList img[data-ischecked='true']").length;
        		        		        		
        		$http({
        	        url: actionName,
        	        method: 'POST',
        	        data: { "goalHabitsData" : angular.toJson(goal), "patientId": $("#txtPatientId").val() },
        	        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        	        transformRequest: function(obj) {
        	            var str = [];
        	            for(var p in obj)
        	            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        	            return str.join("&");
        	        }
        	    }).success(function(data){		        	    	
        	    	data = angular.fromJson(data);		        	    
        	    	if(data.STATUS == "SUCCESS" || data.indexOf("SUCCESS") > -1){

                        //call here to android method to sync goal from server
                        try{
                            Android.syncGoalPinnedMessage();
                        }catch(err){ }

        	    		window.location = "goalsHabitsListing.action?AUTHORIZATION="+$("#txtAuthorization").val();
        	    	}else{ 
        	    		$loading.hide();
        	    		ajaxing.showErrorOrRedirect(data, $("#errMessage"), AJAX_ERROR_MESSAGE);
        	    	}
        	    }).error(function(err){
        	    	$loading.hide();
        	    	ajaxing.showErrorOrRedirect(err, $("#errMessage"), AJAX_ERROR_MESSAGE);
        	    });
			}        			        
		};
	}]);        	
})();