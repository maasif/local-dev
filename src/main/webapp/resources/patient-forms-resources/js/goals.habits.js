if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//==========================================
/* GoalsHabits JS Model*/
var GoalsHabits = {

	//initialize page functionality
    init: function(goalsHabits, url){
        var that = this;              
        that.GOALS_AND_HABITS = goalsHabits;
        that.url = url;
        that.isLoaded = false;
        //alert message instance
		that.ALERT_MESSAGES = Object.create(AlertMessage).init();
		
		//ajaxing instance
		that.AJAXING = Object.create(Ajaxing);
        
        that.GOALS_JSON_KEYS = {
  			"goalOption" : "<b>My Goal</b>",
  			"actionPlan" : "<b>My Action Plan</b>",
  			"myChallenge" : "<b>My Challenge</b>",
  			"overcomeChallenge" : "To <b>Overcome</b> this	Challenge <b>I will</b>",
  			"myReward" : "<b>My Reward</b>",
  			"remindMe": "<b>Remind Me</b>"
  		};
      	
        that.CATEGORY_COLOR = {
    		"Healthy Eating": "healthy-eating",
    		"Physical Activity": "physical-activity",
    		"Medications & Health Mgmt": "medication-monitoring",
    		"Money, Work and Travel": "work-money",
    		"Stress and Sleep": "stress-sleep",
    		"Family & Relationships": "family-relationship"
        };
        
        that.patientResourcesFolderPath = that.url+"resources/patient-forms-resources/";
        that.checkedImgPath = that.patientResourcesFolderPath + "css/images/checked.png";
        that.uncheckedImgPath = that.patientResourcesFolderPath + "css/images/unchecked.png";
        
        that.renderGoals($("#goalList"));
        that.renderHabits($("#habitList"));
        
        that.bindSwitch();
        
        //load switch values
		that.loadReminderSwitch($(".goals-reminder"), that.goalsDataArray);
		that.loadReminderSwitch($(".habits-reminder"), that.habitsDataArray);
		
        window.goalsHabitsObject = that;
        
        //dialog delete HABIT when press 'Yes', do following
        $("#btnDeleteYesHabit").on("click", function(){
        	that.deleteHabit();
        	return false;
        });
        
        //dialog delete GOAL when press 'Yes', do following
        $("#btnDeleteYesGoal").on("click", function(){
        	that.deleteGoal();
        	return false;
        });
        
        $("#btnEditGoalDialog").on("click", function(){        	
        	window.location = "goalProgress.action?formType=GOAL&filledFormId="+that.selectedId;
        	return false;
        });
        
        var $uncheckedGoals = $("#goalRatingList").find("img");
        
        $uncheckedGoals.on("click", function(){          	
        	var scale = $(this).parents("li").data("scale");        
        	that.checkUncheckScale($(this), $uncheckedGoals, scale); 
        	that.saveRating(scale);
        });

        return that;
    },

    buildActionPlanString: function(goalDetails, actionPlan, planFrequency, calendarFrequency, meals, other){

    	var mealsString = "";
    	
    	if(meals){
    		mealsString =  " when " + meals;
    	}
    	    	
    	if(!calendarFrequency){
    		calendarFrequency = "";
    	}
    	
    	var actionPlanString = actionPlan;
    	
    	if(actionPlan.toLowerCase() == 'create my own'){
            actionPlanString = goalDetails.actionPlanOthers;
    	}
		var returnString = actionPlanString + " " + planFrequency;
		if(calendarFrequency != 'in the coming week') {
			returnString += " per " + calendarFrequency + mealsString;
		} else {
			returnString += " " + calendarFrequency + mealsString;
		}
    	return returnString;
    },
    
    buildGoalDetailsArray: function(dataList){
    	var goalDetails = {};
    	    
		//converting fieldname and fieldValue to single goalDetailsObject
    	$.each(dataList, function(index, fd){	
			var fieldName = fd.fieldName;													
			goalDetails[fieldName] = fd.fieldValue;							        				
		});	
    	
    	return goalDetails;
    },
    
    renderGoals: function($container, keysConstant){
    	
    	var that = this,
    		$ul = $container.empty(),
			html = "",
			dataArray = [];
			
    	if(!$.isEmptyObject(that.GOALS_AND_HABITS)){
			var goals = that.GOALS_AND_HABITS["GOALS"];
			if(goals){				
				dataArray = $.parseJSON(goals);
			}
		}
    	
    	that.goalsDataArray = dataArray;
    	
		if(dataArray && dataArray.length > 0){
			
			$.each(dataArray, function(index, goal){
				
				html += "<li>";
				
				var listHtml = "",
					dataList = goal.formDataList,
					goalDetails = {};
				
				if(dataList && dataList.length > 0){									
					goalDetails = that.buildGoalDetailsArray(dataList);																			
				}
				
				if(goalDetails){					
					html += that.buildGoalItem(goal, goalDetails);
				}
				
				html += "</li>";								
			});

			$ul.append(html);
			
			if(dataArray.length > 1){
	    		
				//hide all list-items except first one
	    		var $hiddenItems = $ul.children("li:not(:first)").hide();
	    		
	    		//adds load more item
	    		$ul.append("<li class='load-more other-goals'><a href='#'><img src='"+that.url+"resources/css/images/goal_icons/expand.png' width='22' /> <span class='collapse-text'>SEE ALL MY GOALS</span></a></li>");
	    		
	    		$(".other-goals").on("click", function(e){
	    			e.preventDefault();	        			
	    			
	    			var $this = $(this).find("span.collapse-text"),
	    				text = $this.text();
	    			
	    			if(text == "SEE ALL MY GOALS"){	    				
	    				$(this).find("img").attr("src", that.url+"resources/css/images/goal_icons/collapse.png");
	    				$hiddenItems.show("slow");
	    				$this.text("HIDE ALL MY GOALS");
	    			}else{
	    				$hiddenItems.hide("hide");
	    				$(this).find("img").attr("src", that.url+"resources/css/images/goal_icons/expand.png");
	    				$this.text("SEE ALL MY GOALS");
	    			}
	    		});	        			
			}
			
			$(".progress-btn").on("click", function(e){
				e.preventDefault();
				that.selectedId = $(this).data("id");
				
				//setting selected goal to Goal: <here>
				$("#spGoalName").text($(this).parents("li").find("div.goal-name").text());
				
				//setting selected goal to Action Plan: <here>
				$("#spActionPlan").text($(this).parents("li").find("div.action-plan").text());
				
				//load rating
				var $uncheckedGoals = $("#goalRatingList").find("img");
				that.checkUncheckScale($(this), $uncheckedGoals, $(this).data("rating")); 
				
				//skipping status dialog opening
				//$("#goalRatingDialog").foundation("reveal", "open");
				
				window.location = "goalProgress.action?formType=GOAL&filledFormId="+that.selectedId+"&authorizationCreds="+$("#txtAuthorization").val()+"&patientId="+$("#txtPatientId").val();
			});
				
			$(".delete-btn").on("click", function(){
				that.selectedId = $(this).data("id");
	        	$("#confirmationGoalDialog").foundation("reveal", "open");
	        	return false;
	        });
			
		}else{
			$ul.append("<li><div class='align-center padding-10'>No goals found</div></li>");
		}		
    },
    
    buildGoalItem: function(goal, goalDetails){
    	var that = this,
    		actionPlanString = that.buildActionPlanString(goalDetails, goal.actionPlan, goalDetails.planFrequency, goalDetails.calendarFrequency, goalDetails.meals, goalDetails.other),
    		bgColor = that.CATEGORY_COLOR[$.trim(goal.categoryName)],
    		foreColor = bgColor+"-text",
            goalName = goal.goalName;

        if(goalName.toLowerCase() == "create my own"){
            goalName = goalDetails.goalOthers;
        }
    	    	
    	return "<div class='goals-item'> "
					+ "<div class='category-outer "+ bgColor +"'> " 					
					+	"<div class='category-name'>"+goal.categoryName+"</div>"
					+ "</div> "						
					+ "<div class='goal-details'> "
						+ "<div class='goal-name "+ foreColor +"'>"+goalName+"</div> "
						+ "<div class='action-plan'>"+actionPlanString+"</div> "
						+ "<div class='grey-black-pair'> "
							+ "	<label class='inline margin-bottom-5 grey "+ foreColor +"'>Challenge</label> "
							+ "	<label class='inline margin-bottom-0 black'>"+(($.trim(goalDetails.myChallenge)) ? goalDetails.myChallenge : "N/A") +"</label> "
						+ "</div> "
						+ "<div class='grey-black-pair'> "
							+ "	<label class='inline margin-bottom-5 grey "+ foreColor +"'>Overcoming my Challenge</label> "
							+ "	<label class='inline margin-bottom-0 black'>"+(($.trim(goalDetails.overcomeChallenge)) ? goalDetails.overcomeChallenge : "N/A") +"</label> "
						+ "</div> "
						+ "<div class='grey-black-pair'> "
							+ "	<label class='inline margin-bottom-5 grey "+ foreColor +"'>Reward</label> "
							+ "	<label class='inline margin-bottom-0 black'>"+(($.trim(goalDetails.myReward)) ? goalDetails.myReward : "N/A") +"</label> "
						+ "</div> "
						+ "<div class='reminders hide'> "
						+ "	<ul class='button-group reminder-switch goals-reminder tiny round'> "
						+ "		<li><a href='#' title='No Reminder' class='button tiny margin-bottom-0 button-green' data-text='REMINDER' data-id="+goal.filledFormId+"><img src='"+that.url+"resources/css/images/goal_icons/no-reminder.png' title='No Reminder' /></a></li> "
						+ "		<li><a href='#' title='Reminder' class='button tiny secondary margin-bottom-0' data-text='NO REMINDER' data-id="+goal.filledFormId+"><img src='"+that.url+"resources/css/images/goal_icons/reminder.png' title='Reminder' /></a></li> "
						+ "	</ul> "
						+ "	<span class='reminder-status'>NO REMINDER</span> "
						+ "</div> "
					+ "</div> "
					+ "<div class='nav-btns'> "						
						+ "<a href='#' class='progress-btn' title='Progress' data-id="+goal.filledFormId+" data-rating="+goal.rating+"><img src='"+that.url+"resources/css/images/goal_icons/tick-selected.png' title='Progress' /></a> "
						+ "<a href='#' class='delete-btn' title='Delete' data-id="+goal.filledFormId+"><img src='"+that.url+"resources/css/images/goal_icons/delete-bin.png' title='Delete' /></a> "
					+ "</div> "
				+ "</div> ";
    },
    
    buildHabitItem: function(habit){
    	var that = this;    	    	
    	
    	return "<div class='goals-item'> "									
				+ "<div class='goal-details habit-details'> "
					+ "<div class='row'> "
						+ "<div class='grey-black-pair'> "
							+ "<div class='columns small-5'><label class='inline grey align-right grey-forced'>After I</label></div>"
							+ "<div class='columns small-7 padding-left-0'><label class='inline margin-bottom-0 margin-top-5 black'>"+(($.trim(habit.triggeringActivity)) ? habit.triggeringActivity : "N/A") +"</label></div>"
						+ "</div> "
					+ "</div> "
					+ "<div class='row'> "
						+ "<div class='grey-black-pair'> "
							+ "<div class='columns small-5'><label class='inline grey align-right grey-forced'>I Will</label></div> "
							+ "<div class='columns small-7 padding-left-0'><label class='inline margin-bottom-0 margin-top-5 black'>"+(($.trim(habit.healthierHabit)) ? habit.healthierHabit : "N/A") +"</label></div> "
						+ "</div> "
					+ "</div> "
					+ "<div class='row'> "
						+ "<div class='grey-black-pair'> "
							+ "<div class='columns small-5'><label class='inline grey align-right grey-forced celebrate-label'>And Celebrate</label></div> "
							+ "<div class='columns small-7 padding-left-0'><label class='inline margin-bottom-0 margin-top-5 black'>"+(($.trim(habit.celebrate)) ? habit.celebrate : "N/A") +"</label></div> "
						+ "</div> "
					+ "</div> "
					+ "<div class='row reminders' style='visibility:hidden'> "
					+ "<div class='columns small-5'>&nbsp;</div>"
					+ "<div class='columns small-7 padding-left-0'><ul class='button-group reminder-switch habits-reminder tiny round'> "
					+ "		<li><a href='#' title='No Reminder' class='button tiny margin-bottom-0 button-green' data-text='REMINDER' data-id="+habit.formId+"><img src='"+that.url+"resources/css/images/goal_icons/no-reminder.png' title='No Reminder' /></a></li> "
					+ "		<li><a href='#' title='Reminder' class='button tiny secondary margin-bottom-0' data-text='NO REMINDER' data-id="+habit.formId+"><img src='"+that.url+"resources/css/images/goal_icons/reminder.png' title='Reminder' /></a></li> "
					+ "	</ul> "
					+ "	<span class='reminder-status reminder-status-habit'>NO REMINDER</span></div> "
					+ "</div> "
				+ "</div> "	
				+ "<div class='nav-btns'> "	
					+ "<a href='#' class='progress-btn edit-habit habits-nav-btns' title='Edit' data-id="+habit.formId+"><img src='"+that.url+"resources/css/images/goal_icons/tick-selected.png' title='Edit' /></a> "
					+ "<a href='#' class='delete-btn delete-habit habits-nav-btns' title='Delete' data-id="+habit.formId+"><img src='"+that.url+"resources/css/images/goal_icons/delete-bin.png' title='Delete' /></a> "
				+ "</div> "
			+ "</div> ";
    },
    
    renderHabits: function($container){
    	var that = this,
    		$ul = $container.empty(),
			html = "",	        	
			dataArray = [];
		
		if(!$.isEmptyObject(that.GOALS_AND_HABITS)){
			var habits = that.GOALS_AND_HABITS["HABITS"];
			if(habits){				
				dataArray = $.parseJSON(habits);				
			}
		}
		
		that.habitsDataArray = dataArray;
		
		if(dataArray && dataArray.length > 0){
			
			$.each(dataArray, function(index, habit){
				
				html += "<li>" + that.buildHabitItem(habit) + "</li>";
			});
			
			$ul.append(html);
			
			if(dataArray.length > 1){
	    		
				//hide all list-items except first one
				var $hiddenItems = $ul.children("li:not(:first)").hide();
	    		
				//adds load more item
	    		$ul.append("<li class='load-more other-habits'><a href='#'><img src='"+that.url+"resources/css/images/goal_icons/expand.png' width='22' /> <span class='collapse-text'>SEE ALL MY HABITS</span></a></li>");
	    		
	    		$(".other-habits").on("click", function(e){
	    			e.preventDefault();	        			
	    			
	    			var $this = $(this).find("span.collapse-text"),
	    				text = $this.text();
	    			
	    			if(text == "SEE ALL MY HABITS"){	    				
	    				$(this).find("img").attr("src", that.url+"resources/css/images/goal_icons/collapse.png");
	    				$hiddenItems.show("slow");
	    				$this.text("HIDE ALL MY HABITS");
	    			}else{
	    				$hiddenItems.hide("hide");
	    				$(this).find("img").attr("src", that.url+"resources/css/images/goal_icons/expand.png");
	    				$this.text("SEE ALL MY HABITS");
	    			}
	    		});	  	    		       		
			}
			
			$(".delete-habit").on("click", function(e){
				e.preventDefault();
				that.selectedId = $(this).data("id");
				$("#confirmationHabitDialog").foundation("reveal", "open");
			});
			
			$(".edit-habit").on("click", function(e){
				e.preventDefault();
				that.selectedId = $(this).data("id");
				window.location = "createHabit.action?filledFormId="+that.selectedId+"&authorizationCreds="+$("#txtAuthorization").val()+"&patientId="+$("#txtPatientId").val();
			});
			
		}else{
			$ul.append("<li><div class='align-center padding-10'>No habits found</div></li>");
		}
    },
    
    saveRating: function(rating){
    	
    	var that = window.goalsHabitsObject,
    		dataToSend = { "formId" : that.selectedId, "rating": rating },
			postRequestActions = {    			
				"loading": $("#loading"),
				"error": $("#errMessageDialog"),
				"successCallBack": that.onSuccessSaveRating
			};    	
        	
		that.AJAXING.sendPostRequest("saveRating.action", dataToSend, postRequestActions);
    },
    
    onSuccessSaveRating: function(data){
    	var that = window.goalsHabitsObject;
    	
    	if(data.STATUS == "SUCCESS"){    		
    		that.ALERT_MESSAGES.showAlertMessage($("#errMessageDialog"), "Goal rating saved successfully.", that.ALERT_MESSAGES.SUCCESS);
    	}else{    		
    		that.AJAXING.showErrorOrRedirect(data, $("#errMessage"), "Unable to save rating. Please try again later.");
    	}
    },
    
    deleteHabit: function(){
    	
    	var that = window.goalsHabitsObject,
    		dataToSend = { "filledFormId" : that.selectedId, "patientId" : $("#txtPatientId").val() },
			postRequestActions = {    			
				"loading": $("#loading"),
				"error": $("#errMessage"),
				"successCallBack": that.onSuccessDeleteHabit
			};    	
        	
		that.AJAXING.sendPostRequest("deleteHabit.action", dataToSend, postRequestActions);
    },
    
    onSuccessDeleteHabit: function(data){
        $("#confirmationHabitDialog").foundation("reveal", "close");
    	if(data.STATUS == "SUCCESS"){    		
    		window.location.reload(true);
    	}else{    		
    		that.AJAXING.showErrorOrRedirect(data, $("#errMessage"), "Unable to delete Habit. Please try again later.");
    	}
    },
    
    deleteGoal: function(){
    	
    	var that = window.goalsHabitsObject,
    		dataToSend = { "filledFormId" : that.selectedId, "patientId" : $("#txtPatientId").val() },
			postRequestActions = {    			
				"loading": $("#loading"),
				"error": $("#errMessage"),
				"successCallBack": that.onSuccessDeleteGoal
			};    	
        	
		that.AJAXING.sendPostRequest("deleteGoal.action", dataToSend, postRequestActions);
    },
    
    onSuccessDeleteGoal: function(data){
    	var that = window.goalsHabitsObject;

        $("#confirmationGoalDialog").foundation("reveal", "close");

        if(data.STATUS == "SUCCESS"){

            //call here to android method to sync goal from server
            try{
                Android.syncGoalPinnedMessage();
            }catch(err){ }

    		window.location.reload(true);
    	}else{    		
    		that.AJAXING.showErrorOrRedirect(data, $("#errMessage"), "Unable to delete Goal. Please try again later.");
    	}
    },
    
    saveReminder: function(reminderText){
    	
    	var that = window.goalsHabitsObject,
    		dataToSend = { "formId" : that.selectedId, "remindString" : reminderText },
			postRequestActions = {    			
				"loading": $("#loading"),
				"error": $("#errMessage"),
				"successCallBack": that.onSuccessSaveReminder
			};    	
        	
		that.AJAXING.sendPostRequest("saveReminder.action", dataToSend, postRequestActions);
    },
    
    onSuccessSaveReminder: function(data){ 
    	var that = window.goalsHabitsObject;
    	
    	if(data.STATUS == "SUCCESS"){    		
    		that.ALERT_MESSAGES.showAlertMessage($("#errMessage"), "Reminder saved successfully.", that.ALERT_MESSAGES.SUCCESS);
    	}else{    		
    		that.AJAXING.showErrorOrRedirect(data, $("#errMessage"), "Unable to save reminder. Please try again later.");
    	}
    },
    
    bindSwitch: function(){	  
    	var that = this;
    	
    	$(".button-group").each(function(){
    		var $reminderSwitch = $(this), 
    			$switchBtns = $reminderSwitch.find("a");
    		
    		$switchBtns.on("click", function(e){
        		e.preventDefault();		        		
        		$switchBtns.removeClass("button-green").addClass("secondary");		        		
        		$(this).removeClass("secondary").addClass("button-green").blur();
        		
        		that.selectedId = $(this).data("id");
        		
        		var reminderStatus = $(this).data("text");
        		if(reminderStatus == "REMINDER"){
        			reminderStatus = "NO REMINDER";
        		}else{
        			reminderStatus = "REMINDER";
        		}
        		
        		$reminderSwitch.next("span.reminder-status").text(reminderStatus);
        		
        		if(that.isLoaded){
        			that.saveReminder(reminderStatus);    		    			
        		}
        		that.isLoaded = true;        		
        	});
    	});
    },
    
    //to change checked/unchecked image
    checkUncheckScale: function($item, $items, scale){

        var that = this,
    		src = $item.attr("src");
                
        $items.attr({"src": that.uncheckedImgPath, "data-ischecked": false}); 
        
        $.each($items, function(index, img){
        	if(index < scale){
        		$(img).attr({"src": that.checkedImgPath, "data-ischecked": true});
        	}
        });        
    },
    
    loadReminderSwitch: function($switch, array){
    	var that = this;
    	    	
    	$.each(array, function(index, obj){
    		
    		var $reminderSwitch = $switch.eq(index), 
				$switchBtns = $reminderSwitch.find("a");
    		
    		if(obj.shouldRemind){
        		that.isLoaded = false;
        		$switchBtns.eq(1).click();
        	}
        });    	
    }
}
    