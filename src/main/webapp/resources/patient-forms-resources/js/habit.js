(function(){
        	
	var app = angular.module("HabitsCreateModule", []);
	
	app.controller("HabitSaveController", ["$scope", "$http", function($scope, $http){        		
		var fromJson = (HABIT_FORM_DATA) ? angular.fromJson(HABIT_FORM_DATA) : [],
			singleLine = {}; 
		
		//convert jsonArray Object to single object
		angular.forEach(fromJson, function(obj, value){        			
			singleLine[obj.name] = obj.value;        			
		});        	
		
		//storing in page level object
		$scope.habit = singleLine; 
		
		//storing for showing required fields label when user submits
		$scope.submitted = false;
		
		//submit form function
		$scope.submitForm = function(habit) {
			
			$scope.submitted = true;
			
			//if form is valid then process
			if($scope.habitForm.$valid){
				
				var fillFormId = habit.filledFormId,
        			actionName = "saveForm.action?formType=HABIT",
        			ajaxing = Object.create(Ajaxing),
        			$loading = $("#loading").show();
    		
        		//if greater than '0' it means to update the form
        		if(fillFormId > 0){
        			actionName = "saveForm.action?formType=HABIT&filledFormId="+fillFormId;
        		}
        				
        		$http({
        	        url: actionName,
        	        method: 'POST',
        	        data: { "goalHabitsData" : angular.toJson(habit), "patientId": $("#txtPatientId").val() },
        	        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        	        transformRequest: function(obj) {
        	            var str = [];
        	            for(var p in obj)
        	            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        	            return str.join("&");
        	        },
        	    }).success(function(data){		        	    	
        	    	data = angular.fromJson(data);		        	    
        	    	if(data.STATUS == "SUCCESS" || data.indexOf("SUCCESS") > -1){
        	    		window.location = "goalsHabitsListing.action?AUTHORIZATION="+$("#txtAuthorization").val();
        	    	}else{ 
        	    		$loading.hide();
        	    		ajaxing.showErrorOrRedirect(data, $("#errMessage"), AJAX_ERROR_MESSAGE);
        	    	}
        	    }).error(function(err){
        	    	$loading.hide();
        	    	ajaxing.showErrorOrRedirect(err, $("#errMessage"), AJAX_ERROR_MESSAGE);
        	    });	   
			}        			        
		};
	}]);        	
})();