if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

//==========================================
/* Ajax send request JS Model*/
var Ajaxing = {

	//this file is used in: goal.js
    sendPostRequest: function(actionUrl, dataToSend, postRequestActions){       
        var that = this,
        	$loadingSpinner = "",
        	$error = "";
        
        if(postRequestActions){
          $loadingSpinner = postRequestActions.loading,
          $error = postRequestActions.error;                      
        }
        
        if($error){
            $error.hide();
        }
        
        if($loadingSpinner){
            $loadingSpinner.show();
        }
        
        $.post(actionUrl, dataToSend).done(function(data) {  			
  			if($loadingSpinner){
                $loadingSpinner.hide();
            }
            
            if($error){
                $error.hide();
            }
            
            try{
                data = JSON.parse(data);                        
            }catch(err){
                //console.log(err.message);
            }
            
            if(postRequestActions){
	            var callBack = postRequestActions.successCallBack;
	            if(callBack){             
	                callBack(data);
	            }
            }
  		}).fail(function(jqXHR, textStatus, errorThrown){
  			if($loadingSpinner){
                $loadingSpinner.hide();
            }

            //status 500: Server side error
            //status 404: not found
            console.log("ERROR", jqXHR);

            if($error){
                that.showErrorOrRedirect(jqXHR.responseText, $error, "Error code: " + jqXHR.status + "<br/>" + jqXHR.responseText);
            }
  		});          
    },
    
    sendRequest: function(actionUrl, dataToSend, postRequestActions){       
        var that = this,
        	$loadingSpinner = "",
            $error = "";
        
        if(postRequestActions){
	        $loadingSpinner = postRequestActions.loading,
	        $error = postRequestActions.error;	        
	    }
	    
        if($error){
            $error.hide();
        }
        
        if($loadingSpinner){
            $loadingSpinner.show();
        }
        
       $.ajax({
            type : postRequestActions.ajaxType, //GET or POST
            url : actionUrl,
            data : dataToSend,              
            contentType : "application/javascript",             
            success : function(data) {                                          
                
                if($loadingSpinner){
                    $loadingSpinner.hide();
                }
                
                if($error){
                    $error.hide();
                }
                
                try{
                    data = JSON.parse(data);                        
                }catch(err){
                    //console.log(err.message);
                }
                
                if(postRequestActions){
	                var callBack = postRequestActions.successCallBack;
	                if(callBack){             
	                    callBack(data);
	                }                   
                }
            },              
            error : function(jqXHR, textStatus, errorThrown) {
                            
                if($loadingSpinner){
                    $loadingSpinner.hide();
                }

                //status 500: Server side error
                //status 404: not found
                console.log("ERROR", jqXHR);
                
                if($error){
                	that.showErrorOrRedirect(jqXHR.responseText, $error, "Error code: " + jqXHR.status + "<br/>" + jqXHR.responseText);
                }
            }
        });
    },
    
    showErrorOrRedirect: function(data, $error, errorText){	    	
    	data = $.trim(data);    	
    	if(data.toLowerCase().indexOf("<!doctype") > -1){
    		window.location = "login.jsp";
    	}else{
    		var alertMessage = Object.create(AlertMessage);
        	alertMessage.showPermanentAlertMessage($error, errorText, alertMessage.ERROR);        	
    	}
    },

    loadExternalPage: function($div, url, customCallback){
        var $loading = $(".coach-section-loading,.consent-form-loading").show();
        $div.load(url, function(data){
            if(customCallback){
                customCallback();
            }
            $loading.hide();
        });
    }
};
