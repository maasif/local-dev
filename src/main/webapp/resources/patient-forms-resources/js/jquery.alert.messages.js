if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

var SUCCESS = "success",
    ERROR = "error",
    AJAX_ERROR_MESSAGE = "Unable to connect to server, please check your internet settings and try again. If the problem persists please contact Customer Service at <a href='info@healthslate.com' style='color:yellow'>info@healthslate.com</a> or (888) 291-7245 extension 1.";

//==========================================
/* AlertMessages JS Model*/
var AlertMessage = {

	//this file is used in: jquery.ajaxing.js, goal.js
		
	init: function(){
		var that = this;		
		that.SUCCESS = "success";
		that.ERROR = "alert";
		that.INFO = "info";
		that.AJAX_ERROR_MESSAGE = "Unable to connect to server, please check your internet settings and try again. If the problem persists please contact Customer Service at <a href='info@healthslate.com' style='color:yellow'>info@healthslate.com</a> or (888) 291-7245 extension 1.";
		return that;
	},
	
    getAlertMessage: function($message, alertType){
    	var that = this,    	
    		allClassess = that.SUCCESS + " " + that.ERROR + " " + that.INFO;
    	
    	return $message.removeClass(allClassess).addClass(alertType).show();    	
    },

    showAlertMessage: function($message, errorMessage, alertType, dontScroll){
    
        var that = this,
        	timer = null;

        if(!dontScroll){
            that.scrollPageToTop();
        }

        clearTimeout(timer);
        
        var $m = that.getAlertMessage($message, alertType);
        $m.html(errorMessage);
        
        timer = setTimeout(function(){
            $m.fadeOut(500);
        }, 3000);  
        
        return $message;
    },

    showPermanentAlertMessage: function($message, errorMessage, alertType, dontScroll){
    	var that = this,    	
    		$m = that.getAlertMessage($message, alertType);

        if(!dontScroll) {
            that.scrollPageToTop();
        }
    	
        $m.html(errorMessage).append("<a href='#' class='close-alert'>&times;</a>");
        
        $("a.close-alert").off("click").on("click", function(e){
            e.preventDefault();
            $m.fadeOut(500);
        });
        
        return $message;
    },
    
    scrollPageToTop: function(){
      $('body,html').animate({
            scrollTop : 0
        }, 70);
    },

    fadeOutAlertMessage: function($error){
        setTimeout(function(){
            $error.fadeOut(500);
        }, 3000);
    }
};