if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

// ================ LEGENDS ================
// Field-Type: DD: dropdown, CB: checkbox, TI: text-input, RB: radio, DP: datePicker, TA: textarea, LB: label, TICB: TextInput with checkbox
// Tag-Type: email, phone, zip, text etc
// Validations: CATA: Check all that apply, CO: Choose only one, RF: Required Field

//==========================================
/* FormBuilder JS Plugin */
// the procedural method
(function ($, window, document, undefined) {

    var FormBuilder  = {

    	//init all form builder functionality
        init: function(config, elem){
            var that = this;
            that.config = $.extend({}, $.fn.formBuilder.config, config);
            that.atPage = 1;         
            that.buildForm(elem);
            that.bindNextPrevious();
            $(".field-lb").boldLabels();
        },  

        //build form based on jsondata
        buildForm: function(elem){
            var that = this,
                jsonData = that.config.json,
                pageCount = 1,
                columnCount = 0,
                columns = that.getColumns(2),
                $page = $("<div/>", {"id": "page"+pageCount, "class": "pages"}),
                $navButtons = $(".navigation-buttons"),
                $row = that.getRow();                       
               
               $.each(jsonData, function(index, field){        
                    var fType = field.fieldType,                        
                        ctrlLabel = that.getFieldObjectByType("LB", field.fieldLabel),
                        ctrl = that.renderFormControls(field),
                        pageHeading = that.insertPageHeading(field),
                        $error = "";
                    
                    if(pageHeading){
                        $page.append(pageHeading);
                    }

                    if(field.fieldValidation){
                        $error = that.getFieldObjectByType("ERR", that.getValidationMessageByRule(field.fieldValidation)).hide();
                    }

                    //if next line
                    if(fType === "NL"){
                        columnCount = 0;
                        columns = that.getColumns(2);
                        $row = that.getRow();   
                        if(field.fieldClass == "full_width"){
                        	columns = that.getColumns(1);
                        }                        
                    }

                    //if next section or next page 
                    if(fType === "NS"){
                        pageCount++; 

                        columnCount = 0;
                        columns = that.getColumns(2);
                        $row = that.getRow();  

                        $page = $("<div/>", {"id": "page"+pageCount, "class": "pages", "style": "display:none"});
                        
                        $navButtons.show();
                    }
                    
                    if(ctrl){                        
                        
                        var $col = columns[columnCount];
                        
                        if($col){    
                        	
                        	var linkedField = "";
                        	
                        	if(field.fieldLinked){
                        		linkedField = field.fieldLinked;
                        	}
                        	                        	
                            $col.append(ctrlLabel.addClass(linkedField).append(ctrl)).append($error);                                                        
                            $page.append($row.append($col));
                            
                            //hide linking field by default
                            if(field.fieldLinked){
                        		linkedField = field.fieldLinked;                        		
                        		$("."+linkedField).hide();                        		                        		
                        	}
                        }  

                        columnCount++;                      
                    }   

                $(elem).append($page);   
                   
                that.bindImagesOnClick(field);
            });   
               
            //bind table delete click event              	
           	//that.bindTableDeleteOnClick();           	
        },

        //bind radio/checkbox image on click
        bindImagesOnClick: function(field){
                        
            if(field && field.fieldName){
                
                var that = this,
                	splitted = that.splitLinkingField(field.fieldName),    
                	$elems = $("."+((splitted) ? splitted[0] : field.fieldName));
                    
                $elems.off("click").on("click", function(){
                    
                    var $item = $(this),
                        src = $item.attr("src"),
                        yesNo = $item.data("value"),                    
                        $linkedCtrl = "";
                    
                    if(splitted){                    	
                    	$linkedCtrl = $("."+splitted[1]).hide();     
                    }
                    
                    if(field.fieldValidation && field.fieldValidation == "CATA"){                        
                        
                    }else{
                        $elems.attr("src", that.getImagePath(src, true));                        
                    }
                    
                    var newSrc = that.getImagePath(src);
                    
                	//show its linking field
                    if(yesNo == "Yes"){
                    	if($linkedCtrl){
                    		$linkedCtrl.show();
                    	}
                    }
                    
                    $item.attr("src", newSrc);                                        
                });    
            }
        },
        
        //bind table on delete button click
        bindTableDeleteOnClick: function(){
        	var that =this;
        	$(".delete-btn:not(:first)").off("click").on("click", function(e){
        		e.preventDefault();
        		$(this).closest("tr").remove();
        	});
        },
        
        //get control (radio, textarea, input) by type
        getFieldObjectByType: function(type, tagType, name)  {

            var that = this,
            	className = "field-"+type.toLowerCase(),
            	isTable = type.indexOf("TABLE-") > -1;

            if(isTable){
            	var $row = $("<div/>", {"class":"table-container"}),
            		$table = $("<table/>", {"class":"table-full"}).empty(),
            		splitted = type.split("-"),            		
            		cols = $.parseJSON(splitted[1]);
            	
            	var $tblHead = $("<thead/>"),
            		$tblBody = $("<tbody/>"),
            		$tr = $("<tr/>"),
            		tblHeadString = "",
            		tblBodyString = "";
            	         
            	$.each(cols, function(index, colText){
            		tblHeadString += "<th>"+colText+"</th>";
            		tblBodyString += "<td>"+that.getFieldObjectByType("TI", tagType).wrap("<div/>").parent().html()+"</td>";
            	});
            	
            	tblHeadString += "<th>Action</th>";
            	tblBodyString += "<td><a href='#' class='delete-btn' style='display:none'>Delete</a></td>";
            	
            	$tblBody.append($tr.clone().append(tblBodyString));
            	            	
            	var $aNew = $("<a/>", {"href":"#", "class": "right add-new-btn", "html": "<img src='resources/css/images/small_add.png' /> <span>Add New</span>"});
            	            	
            	$aNew.on("click", function(e){
            		e.preventDefault();
            		var $tr = $table.find("tr:last").clone().find("input").val("").end().find("a").show().end(),
            			$trLast = $table.find("tr:last");
            		
            		$trLast.after($tr);
            		
            		that.bindTableDeleteOnClick();
            	});
            	
            	$row.append($aNew).append($table.append($tblHead.append($tr.append(tblHeadString))).append($tblBody));
            	 
            	return $row;
            }else{
            	
	            switch(type){
	                case "DD":
	                    return $("<select/>", {"class": "large-dropdown font-24 " + className});
	                case "CB":
	                    //return $("<input/>", {"type": "checkbox", "class": "large-checkbox font-24 " + className, "value": tagType});
                        return $("<img/>", {"src":"resources/patient-forms-resources/css/images/unchecked.png", "class": className + " " + name, "data-value": tagType});
	                case "RB":
	                    //return $("<input/>", {"type": "radio", "name": name, "class": "large-radio font-24 " + className, "value": tagType});
                        return $("<img/>", {"src":"resources/patient-forms-resources/css/images/radio_unchecked.png", "class": className + " " + name, "data-value": tagType});
	                case "TI":
	                    return $("<input/>", {"type": tagType, "class": "large-input-1 font-24 " + className});
	                case "DP":                    
	                    return $("<input/>", {"type": "text", "class": "large-input-1 font-24 " + className, "readonly": "true"}).datepicker({                  
	                         changeMonth: false,
	                         changeYear: false,
	                         showAnim: 'slideDown',
	                         duration: 'fast'
	                    }).datepicker('setDate', new Date());
	                case "TA":
	                    return $("<textarea/>", {"cols": "5", "rows": "5", "class": className});
	                case "LB":
	                    return $("<label/>", {"class":"inline " + className}).append("<span>" + tagType + "</span>");
	                case "H3":
	                    return $("<h3/>", {"class": tagType}).html(name);
	                case "ERR":
	                    return $("<small/>", { "class":"error margin-top-negative-15"}).html(tagType);
	                case "TICB": //Text Input with CheckBox
	                	var $div = $("<div/>"),                	
	                		$input = that.getFieldObjectByType("TI", tagType),
	                		$label = that.getFieldObjectByType("LB", "&nbsp;Don't remember"),
	                		$chk = that.getFieldObjectByType("CB", tagType);
	                	
	                	$div.append($input).append($label.prepend($chk)).find("span").addClass("span-inline");
	                	                		
	                	return $div;               
	            }
            }                       
        },

        //render control based on their type
        renderFormControls: function(field){

            var that = this,
                ctrl = "",
                type = field.fieldType,
                tagType = field.fieldTagType,
                list = field.fieldOptions,
                validationClass = (field.fieldValidation) ? "validate-me" : "";
            
            if(type === "LB"){
            	return ctrl;
            }
            
            if(type === "DD"){
                ctrl = that.getFieldObjectByType(type, tagType).addClass(validationClass);
                ctrl.populateDropdown(list);
            }else if(type === "RB" || type === "CB"){
                var $child = $("<ul/>", {"class": "button-group multiple " + validationClass}).empty();                
                if(list){                                        
                	$.each(list, function(index, obj){
                		
                		var splitted = that.splitLinkingField(field.fieldName),
                        	label = that.getFieldObjectByType("LB", " " + obj.optionLabel),
                            ctrlRbCb = that.getFieldObjectByType(type, obj.optionValue, ((splitted) ? splitted[0] : field.fieldName) ),
                            $li = $("<li/>").append(label.prepend(ctrlRbCb));

                        $child.append($li);
                    });
                	
                	if(list.length > 4){
                		$child.addClass("vertical-items");
                	}
                	
                    ctrl = $child;                    
                }
            }else{
                ctrl = that.getFieldObjectByType(type, tagType);    
                if(ctrl){
                	ctrl.addClass(validationClass);
                }
            }
                  
            return ctrl;
        },

        //radio/checkbox images switching
        getImagePath: function(src, isDefault){
                        
            var imagePath = "";
            
            switch(src){
                case "resources/patient-forms-resources/css/images/radio_unchecked.png":
                    imagePath = "resources/patient-forms-resources/css/images/radio_checked.png";
                    if(isDefault){
                        imagePath = "resources/patient-forms-resources/css/images/radio_unchecked.png";
                    }
                break;
                case "resources/patient-forms-resources/css/images/radio_checked.png":
                    imagePath = "resources/patient-forms-resources/css/images/radio_unchecked.png";
                    if(isDefault){
                        imagePath = "resources/patient-forms-resources/css/images/radio_checked.png";
                    }
                break;
                case "resources/patient-forms-resources/css/images/unchecked.png":
                    imagePath = "resources/patient-forms-resources/css/images/checked.png";
                    if(isDefault){
                        imagePath = "resources/patient-forms-resources/css/images/unchecked.png";
                    }
                break;
                case "resources/patient-forms-resources/css/images/checked.png":
                    imagePath = "resources/patient-forms-resources/css/images/unchecked.png";
                    if(isDefault){
                        imagePath = "resources/patient-forms-resources/css/images/checked.png";
                    }
                break;
            }
            
            return imagePath;
        },
        
        //get full row for appending child containers in it
        getRow: function(){
            return $("<div/>", {"class":"row row-full"});
        },

        //get one/two/three column containers
        getColumns: function(colCount){
            var classes = "";

            switch(colCount){
                case 0:
                case 1:
                    classes = "large-12 medium-12 twelve columns";
                    return [$("<div/>", {"class": classes})];
                case 2:
                    classes = "large-6 medium-6 six columns";
                    return [$("<div/>", {"class": classes}), $("<div/>", {"class": classes})];
                case 3:
                    classes = "large-4 medium-4 four columns";
                    return [$("<div/>", {"class": classes}), $("<div/>", {"class": classes}), $("<div/>", {"class": classes})];
            }
        },

        //show/hide containers/div
        showHidePage: function(pageToHide, pageToShow){
            $("#page"+pageToHide).hide();
            $("#page"+pageToShow).show();
        },

        //bind next/prev on click events
        bindNextPrevious: function(){
            
            var that = this,
                $navButtons = $(".navigation-buttons"),
                $btnNext = $("#btnNext"),
                $btnPrevious = $("#btnPrev").hide(),
                totalPages = $(".pages").length;

            $btnNext.on("click", function(){
                //if(that.validateForm(that.atPage)){
                    that.atPage++;
                    if (that.atPage >= totalPages) {
                        that.atPage = totalPages;
                    }            
                    that.loadPage(true);
                //}                
            });

            $btnPrevious.on("click", function(){
                if (that.atPage <= 2) {
                    $(this).hide();
                }
                that.loadPage(false);
            });
            
            $("#btnNext").html("NEXT " + that.atPage + " OF " +  totalPages);
        },

        // load section on next/prev click
        loadPage: function(isNextPrevious){
            var that = this,
                currentPage = that.atPage,
                previousPage = currentPage,
                $btnPrevious = $("#btnPrev").show(),                
                totalPages = $(".pages").length,
                $btnNext = $("#btnNext").html("NEXT");
                
            that.scrollPageToTop();

            if (isNextPrevious == true) {
                previousPage = previousPage - 1;
            } else if (isNextPrevious == false) {
                that.atPage = that.atPage - 1;
            }

            if (that.atPage <= 1) {
                $btnPrevious.hide();
            }

            $btnNext.html("NEXT " + that.atPage + " OF " +  totalPages);
            
            if (that.atPage === totalPages){
                $btnNext.html("DONE");
            }

            that.showHidePage(previousPage, that.atPage);                        
        },

        //insert page/section heading on top
        insertPageHeading: function(field){
            var that = this,
                heading = "";

            if(field.fieldClass && field.fieldType === "LB"){
                heading = that.getFieldObjectByType("H3", field.fieldClass, field.fieldLabel)
                              .wrap(that.getRow())
                              .parent();
            }

            return heading;
        },

        //check string has number in it or not
        hasNumbers: function(t){
            return /\d/.test(t);
        },

        //validation of password, input etc
        validateFieldDataByType: function(fieldType, txtToValidate){
            var that = this,
                pattern = that.getRegexPattern(fieldType),            
                pTest = pattern.test(txtToValidate);
            
            // in case of password check is there a single number in it or not.
            if(pTest && fieldType === "password"){
                pTest = that.hasNumbers(txtToValidate);
            }
            
            return pTest;
        },

        // get regular expressions for validations of zip, phone etc
        getRegexPattern: function(regexType){
            
            switch (regexType) {
                case "zip":
                    return new RegExp(/^\d{5}(-\d{4})?$/);
                case "email":
                    return new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
                case "number":
                    return new RegExp(/^\d+$/);
                case "password":
                    return new RegExp(/^[A-Za-z0-9_]{8,}$/);
                case "phone":
                    return new RegExp(/^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/);
                default:
                    break;
            }
        },

        //validate section against the input fields etc
        validateForm: function(pageNumber){
          
          var that = this,
              $error = $("small.error").hide(),
              inputArray = new Array(),
              $inputs = $("#page"+pageNumber).find(".validate-me");
          
          $inputs.each(function(i, obj){

              var $input = $(obj), 
                  $secondInput = "",
                  $errorLabel = $input.parent("label"),
                  className = $input.attr("class"), 
                  matchCtrlId = $input.data("matchid"), //get matching control id like like email/repeat email, password/repeat password     
                  inputType = $input.data("inputtype"), // get for validation like zip/email
                  validateTxt = $input.data("validatetxt"), //get error array to show in error label                  
                  val1 = $input.val(),
                  val2 = "";

              //for two fields validation, like email/repeat email, password/repeat password
              if(matchCtrlId){
                  $secondInput = $("#"+matchCtrlId);
                  val2 = $secondInput.val();          
              }
                  
              that.bindFieldOnChange($input);
              
              if($.trim(val1).length === 0){                            
                 if(inputType){                 
                      if(inputType !== "phone"){
                          var oldTxt = validateTxt[0];
                          that.showHideError($errorLabel, true, false, oldTxt);
                          inputArray.push($input);                    
                      }
                  }else{
                      if(inputType !== "phone"){                               
                          if(className.indexOf("multiple") > -1){                
                              /*var $multiple = $input.children("li").find("input");
                              
                              if($multiple == undefined){
                                  $multiple = $input.children("li").find("img");
                              }
                              
                              var clsName = $multiple.attr("class"),
                                    extractClsName = $.trim(clsName.substring(0, clsName.indexOf("font-24")-1));
                                
                                var checkedLength = $("."+extractClsName+":checked").length;
                                if(checkedLength === 0){
                                    that.showHideError($errorLabel, true);
                                    inputArray.push($input);                                      
                                }*/
                         }else{           
                            that.showHideError($errorLabel, true);
                            inputArray.push($input);  
                         }
                      }
                  }                         
              }else{
                  
                  //if an input contains data-inputtype tag like zip, email to validate
                  if(inputType){               
                      var newTxt = validateTxt[1];                
                      if(!that.validateFieldDataByType(inputType, val1)){
                          that.showHideError($errorLabel, true, false, newTxt);
                          inputArray.push($input);  
                      }
                  }
              }    
              
             //for two fields validation, like email/repeat email, password/repeat password
              if(val2 !== val1){
                  if($secondInput.length > 0){
                      that.bindFieldOnChange($secondInput);
                      that.showHideError($secondInput.parent("label"), true);
                      inputArray.push($secondInput);             
                  }
              }                                  
          });

          var $firstElem = inputArray[0];
          if($firstElem){
            $firstElem.focus();    
          }                    
            
          return (inputArray.length == 0);
        },

        //get validation rule message by type
        getValidationMessageByRule: function(validationRule){
             switch(validationRule){
                case "CATA":
                    return "Please check all that apply";
                case "RF":
                    return "This field is required";
                case "CO":
                    return "Please choose only one";
            }
        },

        //show/hide validation error against each field
        showHideError: function(inputField, isShow, cssInlineBlock, customErrTxt){
        
          var $error = inputField.next("small.error").hide();
          
          if(customErrTxt){
              $error.text(customErrTxt);
          }

          if(isShow){
              
              var _display = "block";
              
              if(cssInlineBlock){
                  _display = "inline-block";          
              }
              
              $error.css("display", _display);
          }

          return $error;
        },

        //on change bind of input, dropdown and textarea
        bindFieldOnChange: function($input){

            var that = this,
                className = $input.attr("class");

            $input.off("keyup change").on("keyup change", function(){
                var value = $.trim($(this).val());                                
                if(value){
                  if(className && $input.hasClass("inline")){
                    that.showHideError($(this), false);
                  }else{
                    that.showHideError($(this).parent("label"), false);
                  }                                          
                }
            });
        },

        //scroll to top
        scrollPageToTop: function(){
          $('body,html').animate({
                scrollTop : 0
            }, 70);
        },
        
        splitLinkingField: function(fieldName){
        	var splitted = null;        	
        	if(fieldName.indexOf("|") > -1){        		
        		splitted = fieldName.split("|");
        	}        	
        	return splitted;
        }
    };

    $.fn.formBuilder = function(config){
        return this.each(function () {
            var frmBuilder = Object.create(FormBuilder);            
            frmBuilder.init(config, this);
        });
    }; 

    //default configuration of formBuilder
    $.fn.formBuilder.config = {
        "json":{}
    };

    //==========================================
    /* A plugin to fill dropdown with passing array*/
    $.fn.populateDropdown = function(values){
        var $dropdown = $(this).empty();
        return $.each(values, function(val, obj) {
          var text = (obj.text === undefined) ? obj.optionLabel : obj.text;
          var value = (obj.value === undefined) ? obj.optionValue : obj.value;          
          $dropdown.append($('<option></option>').val(value).html(text));
        });          
    };
    
   //==========================================
   /* A plugin to bold those labels that have span as a child */
    $.fn.boldLabels = function(){      
      return this.each(function(){
          var $elem = $(this).children().first();        
          if($elem.is('span')) {              
              $(this).addClass("font-bold");
          }
      });  
    };
})(jQuery, window, document);


