<!doctype html>
<%-- <%@ taglib prefix="s" uri="/struts-tags"%> --%>
<html>
  <head>        
    <title>Member Sign up</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="x-ua-compatible" content="IE=Edge"/>
	  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-3/css/bootstrap.min.css" />
    <!--[if (gte IE 9) | (!IE)]><!-->
	    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/foundation.min.css" />    
    	<script src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
	<!--<![endif]-->

	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/very.common.styles.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/settings.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/facility.css" />
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/login.style.css" />
	<script src="${pageContext.request.contextPath}/resources/js/angular.min.js"></script>
  </head>

  <body ng-app="PatientSignupModule">

	  <div id="loading" class="loader position-fixed">
		  <div class="image-loader">
			  <p class="progress-please-wait">Please wait...</p>
			  <div class="progress progress-striped active">
				  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
				  </div>
			  </div>
			  <p id="progressTxt" class="progress-text">We're saving data</p>
		  </div>
	  </div>

  	<div id="mainPage" class="main-page overflow-fix">
		<div class="logo">
			<img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" />
		</div>

		 <div class="padding-20">
			<h4 class="sign-in-text">Member Sign up</h4>
		 </div>

		<div id="errMessage" class="alert-box alert alert-message-abs">______________________</div>

		 <div class="content-login-form" ng-controller="PatientSignupController">
			<div class="padding-20" id="page1">
				<form id="signupForm" name="signupForm" method="POST" novalidate ng-submit="submitForm(signup)">
					<div class="row">
						<div class="small-12 twelve columns">
						  <label class="align-left font-18 padding-20 ie-error-style position-rel">
							Email address
							<input type="email" autofocus="autofocus" name="email" ng-model="signup.email" ng-pattern="/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i" class="validate-me login-input margin-bottom-0" required />
							<img alt="" class="img-credentials top-46" src="${pageContext.request.contextPath}/resources/css/images/username.png" />
							<small class="error" ng-show="submitted && signupForm.email.$error.pattern" data-alert>Invalid email address.</small>
							<small class="error" ng-show="submitted && signupForm.email.$error.required" data-alert>Required.</small>
						  </label>
						</div>
					  </div>

					<div class="row">
					  <div class="small-12 twelve columns">
						<label class="align-left font-18 padding-20 ie-error-style position-rel">
						  Facility
							<select id="ddFacility" class="large-dropdown margin-bottom-0 height-48" name="facility" ng-model="signup.facility"
									ng-options="item.name for item in facilityList track by item.facilityId" required>

								<option value="" selected disabled>(select)</option>

							</select>
							<small class="error" ng-show="submitted && signupForm.facility.$error.required">Required.</small>
						</label>
					  </div>
					</div>

					<div class="row">
						<div class="small-12 padding-lr-22 align-left twelve columns margin-bottom-0">
							<input type="submit" ng-click="submitted=true" class="small button radius btn-teal btn-sign-in margin-top-14 margin-bottom-0" value="SIGN UP" />
						 </div>
					</div>
				</form>
			</div>
		</div>
    </div>

	<div id="footer" class="footer">

		<div class="row row-full">

			<div class="columns medium-12">
				<div class="copyright-healtshlate">
					<img src="${pageContext.request.contextPath}/resources/css/images/logowhite.png" />
				</div>
			</div>

			<%--<div class="columns medium-6">

				<span class="grey-separator"></span>

				<div class="about-healthslate">
					<h6 class="color-green">About HealthSlate</h6>
					<p class="our-mission">Our mission is to help with chronic diseases maintain their health through highly indisclosed programs of education, behavior medication and clinical support.
						<br/><br/>
						Combining expertise in consumer behavior, mobile technology and evidenced-based medication. HealthSlate improves patients' quality of life and health outcomes. Learn more at <a href="http://www.healthslate.com" target="_blank">www.healthslate.com</a>
					</p>
				</div>
			</div>--%>
		</div>

	</div>

	<!--[if (gte IE 9) | (!IE)]><!-->
	    <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/js/foundation.min.js"></script>
	<!--<![endif]-->       
	
	<!-- Foundation 4 for IE 9 and later -->
	<!--[if gt IE 8]><!-->	    
	    <script src="${pageContext.request.contextPath}/resources/js/foundation4/foundation.min.js"></script>	    
	<!--<![endif]-->

  	<script src="${pageContext.request.contextPath}/resources/js/json2.js"></script>
  	<script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.alert.messages.js"></script>
  	<script src="${pageContext.request.contextPath}/resources/patient-forms-resources/js/jquery.ajaxing.js"></script>

    <script src="${pageContext.request.contextPath}/resources/js/jquery.migrate.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/common.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/hs.force-refresh-resources.js"></script>

     <script type="text/javascript">

		 var FACILITY_LIST = ("${facilitiesListString}") ? $.parseJSON("${facilitiesListString}") : undefined,
			BASE_URL = "${baseUrl}/";

	     $(function(){		    	 
	    	var browserVersion = $.browser.version;	 		
	 		if(!$.browser.msie || browserVersion >= 9){
	     		$(document).foundation();   	     		
	     	}

             HSForceRefreshResources.refreshResources();
	     });	         
	     
	  </script>

  	  <script src="${pageContext.request.contextPath}/resources/js/patient_signup_controller.js"></script>
  </body>
</html>