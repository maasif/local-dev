<!-- This file is used by the caller, used name is caller_uid, and it calls the callee named calle_uid -->
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Video Test Call</title>

  <script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
  <script src="https://f.vimeocdn.com/js/froogaloop2.min.js"></script>


</head>
<body class="hello">

<iframe id="player1" src="https://player.vimeo.com/video/60839513?api=1&player_id=player1&autoplay=1" width="100%" height="354" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<div>
  <p>Status: <span class="status">&hellip;</span></p>
</div>

</body>

<script type="text/javascript">

  $(function() {
    var iframe = $('#player1')[0];
    var player = $f(iframe);
    var status = $('.status');

    // When the player is ready, add listeners for pause, finish, and playProgress
    player.addEvent('ready', function() {
      status.text('ready');

      player.addEvent('pause', onPause);
      player.addEvent('finish', onFinish);
      player.addEvent('playProgress', onPlayProgress);
    });

    /*setTimeout(function(){
      player.api("play");
    }, 5000);*/
  });

  function onPause(id) {
    status.text('paused');
  }

  function onFinish(id) {
    status.text('finished');
  }

  function onPlayProgress(data, id) {
    status.text(data.seconds + 's played');
  }

</script>

</html>